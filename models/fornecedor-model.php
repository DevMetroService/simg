<?php
class FornecedorModel extends MainModel
{
    public $dados;
    public $time;

    private $fillable = [
      'optante_simples',
      'nome_fantasia',
      'razao_social',
      'insc_estadual',
      'insc_municipal',
      'descricao',
      'cnpjcpf',
      'avaliacao'
    ];

    private $fillableEndereco = [
      'municipio',
      'bairro',
      'logradouro',
      'cep',
      'numero_endereco',
      'complemento',
      'uf',
      'cod_fornecedor'
    ];

    private $fillableContato = [
      'fone_fornecedor',
      'email_fornecedor',
      'nome_contato',
      'site',
      'cod_fornecedor'
    ];

    private $fillableBanco = [
      'agencia',
      'conta_corrente',
      'banco',
      'num_transferencia',
      'cod_fornecedor'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->time = date('d-m-Y H:i:s', time());

        if(!empty($_POST))
            $this->dados = $_POST;
    }

    public function create(){
        $newMaterial = $this->medoo->insert("fornecedor",[
            array_intersect_key($this->dados, array_flip($this->fillable))
        ]);

        $this->dados += ["cod_fornecedor" => (int) $newMaterial];

        $this->medoo->insert("endereco_fornecedor",[
            array_intersect_key($this->dados, array_flip($this->fillableEndereco)),
        ]);

        $this->medoo->insert("dados_bancarios_fornecedor",[
            array_intersect_key($this->dados, array_flip($this->fillableBanco)),
        ]);

        $this->medoo->insert("contato_fornecedor",[
            array_intersect_key($this->dados, array_flip($this->fillableContato)),
        ]);

        return array_intersect_key($this->dados, array_flip($this->fillableEndereco));
    }

    public function update($codFornecedor){

        $this->medoo->update("fornecedor",
            array_intersect_key($this->dados, array_flip($this->fillable))
        ,[
            'cod_fornecedor' =>(int)$codFornecedor
        ]);

        $newMaterial = $this->medoo->insert("endereco_fornecedor",
            array_intersect_key($this->dados, array_flip($this->fillableEndereco))
        ,[
            "cod_fornecedor"  => (int)$codFornecedor
        ]);

        $newMaterial = $this->medoo->insert("dados_bancarios_fornecedor",
            array_intersect_key($this->dados, array_flip($this->fillableBanco))
        ,[
            "cod_fornecedor"  => (int)$codFornecedor
        ]);

        $newMaterial = $this->medoo->insert("contato_fornecedor",
            array_intersect_key($this->dados, array_flip($this->fillableContato))
        ,[
            "cod_fornecedor"  => (int)$codFornecedor
        ]);

    }

    public function disabled(){

    }

    public function getAll()
    {
        return $this->medoo->select("fornecedor", "*");
    }

    public function get($id)
    {
        return $this->medoo->select("fornecedor", "*", ["cof_fornecedor" => (int) $id])[0];
    }
}
 ?>
