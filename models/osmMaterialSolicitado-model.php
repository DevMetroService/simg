<?php

class osmMaterialSolicitadoModel extends MainModel
{
  private $dados;
  public $dadosUsuario;

  private $fillable = [
    'cod_osm',
    'cod_material',
    'quantidade'
  ];

  function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
  {
      $this->medoo  = $medoo;
      $this->phpass = $phpass;

      $this->dadosUsuario = $dadosUsuario;

      if(!empty($_POST))
        $this->dados = $_POST;

  }

  public function create()
  {
    $hasInsert = $this->medoo->insert("osm_material_solicitado",
      [
        array_intersect_key($this->dados, array_flip($this->fillable))
      ]
    );
    return $hasInsert;
  }

  public function delete($id)
  {
    $hasInsert = $this->medoo->delete("osm_material_solicitado",
      [
          "cod_osm_material_solicitado" => (int)$id
      ]
    );
    return $hasInsert;
  }

  public function deleteAllByOsm($id)
  {
    $hasInsert = $this->medoo->delete("osm_material_solicitado",
      [
          "cod_osm" => (int)$id
      ]
    );
    return $hasInsert;
  }

  public function listAll()
  {

  }

  public function listByOsm($id)
  {
      return $this->medoo->select("osm_material_solicitado",
        [
          '[><]material'=> 'cod_material'
        ], "*",
        [
          'cod_osm' => (int)$id
        ]
      );
  }
}
