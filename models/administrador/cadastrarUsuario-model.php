<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 18/04/2016
 * Time: 14:54
 */

class CadastrarUsuarioModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosUser = $_SESSION['dadosUser'];

        if( !empty($dadosUser['cod_usuario'])) {
            if(!empty($dadosUser['passwordReset'])){
                $upDateUsuario = $medoo->update('usuario',
                    [
                        "nome_completo"  => strtoupper($dadosUser['nome_completo']),
                        "usuario"       => strtolower($dadosUser['login_usuario']),
                        "email"         => strtolower($dadosUser['email']),
                        "nivel"         => $dadosUser['nivel_acesso'],
                        "senha"         => $dadosUser['senha'],
                        "permissao"     => $dadosUser['permissao']

                    ], [
                        "cod_usuario" => $dadosUser['cod_usuario']
                    ]
                );
            }
            else{
                $upDateUsuario = $medoo->update('usuario',
                    [
                        "nome_completo"  => strtoupper($dadosUser['nome_completo']),
                        "usuario"       => strtolower($dadosUser['login_usuario']),
                        "email"         => strtolower($dadosUser['email']),
                        "nivel"         => $dadosUser['nivel_acesso'],
                        "permissao"     => $dadosUser['permissao']

                    ], [
                        "cod_usuario" => $dadosUser['cod_usuario']
                    ]
                );
            }

            $deletou_dados_equipe = $medoo->delete("eq_us_unidade", [
                "AND" => [
                    "cod_usuario" => (int) $dadosUser['cod_usuario']
                ]
            ]);

            if (!empty( $dadosUser['equipes'] )){

                foreach($dadosUser['equipes'] as $key => $value){
                    $this->medoo->insert("eq_us_unidade",
                        [
                            "cod_usuario"       => (int)$dadosUser['cod_usuario'],
                            "cod_un_equipe"     => (int)$value
                        ]
                    );
                }

            }

        }
        else {
            $cod_newUsuario = $this->medoo->insert("usuario",
                [
                    "nome_completo"  => strtoupper($dadosUser['nome_completo']),
                    "usuario"       => strtolower($dadosUser['login_usuario']),
                    "email"         => strtolower($dadosUser['email']),
                    "nivel"         => $dadosUser['nivel_acesso'],
                    "senha"         => $dadosUser['senha'],
                    "permissao"     => $dadosUser['permissao']
                ]
            );

            foreach($dadosUser['equipes'] as $key => $value){
                $this->medoo->insert("eq_us_unidade",
                    [
                        "cod_usuario"       => (int)$cod_newUsuario,
                        "cod_un_equipe"     => (int)$value
                    ]
                );
            }
        }
    }
}
