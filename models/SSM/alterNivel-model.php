<?php
class AlterNivelModel extends MainModel
{
    private $dados;
    private $time;
    public $dadosUsuario;

    private $fillable = [
      'nivel_novo',
      'nivel_antigo',
      'cod_ssm'
    ];

    private $fillablePesquisa = [
      'nivel_novo',
      'nivel_antigo',
      'cod_ssm',
      'cod_status'
    ];

    private $fillableAtuante = [
    'cod_reg_alter_nivel_ssm',
    'cod_usuario',
    'cod_status',
    'observacao'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->time = date('d-m-Y H:i:s', time());

        if(!empty($_POST))
            $this->dados = $_POST;
    }

    public function create(){
        $solicitacao = $this->medoo->insert("reg_alter_nivel_ssm",[
            array_intersect_key($this->dados, array_flip($this->fillable))
        ]);

        $this->dados += ['cod_reg_alter_nivel_ssm' => $solicitacao];
        $this->dados += ['cod_status' => 44];

        $statusAlter = $this->medoo->insert("status_reg_alter_nivel_ssm",[
            array_intersect_key($this->dados, array_flip($this->fillableAtuante))
        ]);

        $this->medoo->update('reg_alter_nivel_ssm',
        [
          'cod_status_reg_alter_nivel_ssm' => $statusAlter
        ], [
          'cod_reg_alter_nivel_ssm' => $solicitacao
        ]);

    }

    public function aprove($status, $codReg){
        $aux = [];
        $aux += ['cod_usuario' => $this->dadosUsuario['cod_usuario']];
        $aux += ['cod_status' => $status];
        $aux += ['cod_reg_alter_nivel_ssm' => $codReg];

        $newStatus = $this->medoo->insert("status_reg_alter_nivel_ssm",[
          array_intersect_key($aux, array_flip($this->fillableAtuante))
        ]);

        $newStatus = $this->medoo->update("reg_alter_nivel_ssm",[
          'cod_status_reg_alter_nivel_ssm' => $newStatus
        ],[
          'cod_reg_alter_nivel_ssm' => $codReg
        ]);

        $solicitacao = $this->getView($codReg);

        $newStatus = $this->medoo->update("ssm",[
          'nivel' => $solicitacao['nivel_novo']
        ],[
          'cod_ssm' => $solicitacao['cod_ssm']
        ]);

        return array_intersect_key($aux, array_flip($this->fillableAtuante));
    }

    public function refuse($status, $codReg){
        $aux = [];
        $aux += ['cod_usuario' => $this->dadosUsuario['cod_usuario']];
        $aux += ['cod_status' => $status];
        $aux += ['cod_reg_alter_nivel_ssm' => $codReg];

        $newStatus = $this->medoo->insert("status_reg_alter_nivel_ssm",[
          array_intersect_key($aux, array_flip($this->fillableAtuante))
        ]);

        $newStatus = $this->medoo->update("reg_alter_nivel_ssm",[
          'cod_status_reg_alter_nivel_ssm' => $newStatus
        ],[
          'cod_reg_alter_nivel_ssm' => $codReg
        ]);

        return array_intersect_key($aux, array_flip($this->fillableAtuante));
    }

    public function getAll()
    {
        return $this->medoo->select('v_solicitacao_alter_nivel', '*');
    }

    public function getAllByStatus($status)
    {
        return $this->medoo->select('v_solicitacao_alter_nivel', '*', ['cod_status'=> $status]);
    }

    public function getAllByFilter($dados)
    {
      $dados = array_filter($dados);

      if(is_array($dados))
      {
        $dadosFiltrados = array_filter(array_intersect_key($dados, array_flip($this->fillablePesquisa)));

        if(count($dados) >1)
        {
          return $this->medoo->select('v_solicitacao_alter_nivel', '*', [ "AND" =>
              $dadosFiltrados
          ]);
        }
        else {
          if(count($dados) == 1)
            return $this->medoo->select('v_solicitacao_alter_nivel', '*',
                $dadosFiltrados
            );

          else
            return $this->getAll();
        }
      }

    }

    public function get($id)
    {
        return $this->medoo->select('reg_alter_nivel_ssm', '*', ['cod_reg_alter_nivel_ssm' => (int)$id])[0];
    }

    public function getView($id)
    {
        return $this->medoo->select('v_solicitacao_alter_nivel', '*', ['cod_reg_alter_nivel_ssm' => (int)$id])[0];
    }

    public function getBySsm($ssm, $status = null)
    {
        if($status != null)
        {
          $filter =[
            "AND" => [
              'cod_status' => (int)$status,
              'cod_ssm'=>$ssm
            ]
          ];
        }else {
          $filter = ['cod_ssm'=>$ssm];
        }

        return $this->medoo->select('v_solicitacao_alter_nivel', '*', $filter);
    }


}
 ?>
