<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class AmvModel extends MainModel{

    private $dados;

    private $time;

    private $usuario;

    private $fillable = [
        "cod_estacao",
        "nome_amv"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
            
        $this->time = date('d-m-Y H:i:s', time());

        //################# filtros e afins ##################
        $this->usuario = $this->getCodUsuario($this->dadosSaf['responsavelSaf']);
    }

    function create()
    {
        $new = $this->medoo->insert("amv",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        return $new;
    }

    function update($id)
    {
        $this->medoo->update("amv",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_amv" => $id
         ]);
    }

    public function all(){
        return $this->medoo->select("amv", [ 
            "[><]estacao" => ["cod_estacao"],
            "[><]linha" => ["cod_linha"]
            ], "*");
    }
}
