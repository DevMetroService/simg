<?php
class MaterialRodanteSafModel extends MainModel
{
    private $dados;
    private $time;

    private $fillable = [
        "cod_veiculo",
        "cod_carro",
        "cod_saf",
        "cod_prefixo",
        "cod_grupo",
        "odometro"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        if(!empty($_POST))
            $this->dados = $_POST;
            
        $this->time = date('d-m-Y H:i:s', time());
    }

    function create()
    {
        $newMRSAF = $this->medoo->insert("material_rodante_saf",
        [ array_intersect_key($this->dados, array_flip($this->fillable)) ]
        );
    }

    function update()
    {
        $newMRSAF = $this->medoo->update("material_rodante_saf",
         array_intersect_key($this->dados, array_flip($this->fillable)),
         [
             "cod_material_rodante_saf" => $this->dados['cod_material_rodante_saf']
         ]
        );
    }

}