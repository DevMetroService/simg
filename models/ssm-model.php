<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 13/04/2017
 * Time: 11:13
 */

class SsmModel extends MainModel{

    private $dadosSsm;
    private $time;
    private $usuario;
    
    
    private $fillable =
    [
        "cod_saf",
        "cod_servico",
        "cod_tipo_intervencao",
        "data_abertura",
        "cod_un_equipe",
        "cod_usuario",
        "nivel",
        "complemento",
        "cod_grupo",
        "cod_sistema",
        "cod_subsistema",
        "cod_local_grupo",
        "cod_sublocal_grupo",
        "cod_linha",
        "cod_trecho",
        "cod_ponto_notavel",
        "km_inicial",
        "km_final",
        "posicao",
        "cod_via",
        "complemento_local"
    ];

    private $fillableMR =
    [
        "cod_ssm",
        "cod_veiculo",
        "cod_carro",
        "odometro",
        "cod_prefixo",
        "cod_grupo"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->dadosSsm  = $_SESSION['dadosSsm'];
        unset($_SESSION['dadosSsm']);

        $this->time = date('d-m-Y H:i:s', time());

        //################# filtros e afins ##################
        $this->usuario = $this->getCodUsuario($this->dadosSsm['responsavelSsm']);
    }

    private function validarDados($dadosSsm){
        //Verificar se existe n�mero de SSM
        $hasSsm = $this->medoo->select("ssm", '*', ['cod_ssm' => (int) $this->dadosSsm['codigoSsm']]);
        if (!$hasSsm){
            return false;
        }

        //Verifica se existe sigla equipe e cod_unidade e busca o c�digo unEquipe;
        if(!empty($this->dadosSsm['localSsm']) && !empty($this->dadosSsm['equipeSsm'])){

            $codigoEquipe = $this->getCodEquipe($this->dadosSsm['equipeSsm']);

            $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe", [
                "AND" => [
                    "cod_equipe" => (int)$codigoEquipe,
                    "cod_unidade" => (int)$this->dadosSsm['localSsm']
                ]
            ]);
            $unEquipe = $unEquipe[0];
        }else{
            return false;
        }

        $arrSsm = Array();
        if(!empty($this->dadosSsm['tipoIntervencao']) && !empty($this->dadosSsm['servicoSsm']) && !empty($this->dadosSsm['nivelSsm']) && !empty($unEquipe) &&
        !empty($this->dadosSsm['linhaSsm']) && !empty($this->dadosSsm['trechoSsm']) && !empty($this->dadosSsm['grupoSistemaSsm']) && !empty($this->dadosSsm['sistemaSsm']) && !empty($this->dadosSsm['subSistemaSsm'])){
            $arrSsm['ssm'] = [
                "cod_tipo_intervencao"  => (int) $this->dadosSsm['tipoIntervencao'],
                "cod_servico"           => (int) $this->dadosSsm['servicoSsm'],
                "cod_un_equipe"         => (int) $unEquipe,
                "cod_usuario"           => (int) $this->dadosUsuario['cod_usuario'],
                "nivel"                 => (string) $this->dadosSsm['nivelSsm'],
                "complemento"           => (string) $this->dadosSsm['complementoServicoSsm'],
                "cod_linha"             => (int)$this->dadosSsm['linhaSsm'],
                "cod_trecho"            => (int)$this->dadosSsm['trechoSsm'],
                "cod_ponto_notavel"     => (int)$this->dadosSsm['pontoNotavelSsm'],
                "cod_grupo"             => (int)$this->dadosSsm['grupoSistemaSsm'],
                "cod_sistema"           => (int)$this->dadosSsm['sistemaSsm'],
                "cod_subsistema"        => (int)$this->dadosSsm['subSistemaSsm'],
                "cod_via"               => (int)$this->dadosSsm['viaSsm'],
                "km_inicial"            => (string)$this->dadosSsm['kmInicialSsm'],
                "km_final"              => (string)$this->dadosSsm['kmFinalSsm'],
                "posicao"               => (string)$this->dadosSsm['posicaoSsm'],
                "complemento_local"     => (string)$this->dadosSsm['complementoLocalSsm'],
                "cod_local_grupo" => (int)$this->dadosSsm['localGrupo'],
                "cod_sublocal_grupo" => (int)$this->dadosSsm['subLocalGrupo']
            ];
        }else{
            return false;
        }


        if(($this->dadosSsm['grupoSistemaSsm'] == '22' || $this->dadosSsm['grupoSistemaSsm'] == '23' || $this->dadosSsm['grupoSistemaSsm'] == '26')) {
            if (!empty($this->dadosSsm['veiculoMrSsm']) && !empty($this->dadosSsm['carroMrSsm']) && !empty($this->dadosSsm['odometroMrSsm'])) {
                $arrSsm['materialRodante'] = [
                    "cod_veiculo" => (int)$this->dadosSsm['veiculoMrSsm'],
                    "cod_carro" => (int)$this->dadosSsm['carroMrSsm'],
                    "odometro" => (double)$this->dadosSsm['odometroMrSsm'],
                    "cod_prefixo" => (int)$this->dadosSsm['prefixoMrSsm'],
                    "cod_grupo" => (int)$this->dadosSsm['grupoSistemaSsm']
                ];
            } else {
                return false;
            }
        }

        return $arrSsm;
    }

    public function gerar(){
        $saf = $this->medoo->select("v_saf", "*", ["cod_saf" => (int) $this->dadosSsm['cod_saf']])[0];
        $saf = $this->reorderArr($saf);
        
        $saf['data_abertura'] = $this->time;
        
        $ssm = $this->medoo->insert("ssm",[
            array_intersect_key($saf, array_flip($this->fillable))
        ]);

        $saf['cod_ssm'] = $ssm;

        //Verifica se pertence aos grupos de material rodante
        if(!in_array($saf['cod_grupo'], SISTEMAS_FIXOS)){
            $insertOsmMr = $this->medoo->insert("material_rodante_ssm", [
                array_intersect_key($saf, array_flip($this->fillableMR))
            ]);
        }

        //Altera o Status da SSM e SAF
        $this->alterarStatusSsm($ssm, 9, $this->time, $this->dadosUsuario);        
        $this->alterarStatusSaf($this->dadosSsm['cod_saf'], 1, $this->time, $this->dadosUsuario);

        //Mensagem para o alerta RealTime
        $msgNotificacao = "Saf n.{$this->dadosSsm['cod_saf']} autorizada.
                            <br />Ssm n.{$ssm} aberta.
                            <br />Por:  {$this->dadosUsuario['usuario']}.";

        $this->notificationGerar($saf, $this->dadosSsm['cod_saf'], $msgNotificacao, "Aberta");
    }

    public function update()
    {        
        $this->dadosSsm = $this->reorderArr($this->dadosSsm);
        
        $ssm = $this->medoo->update("ssm",
            array_intersect_key($this->dadosSsm, array_flip($this->fillable))
        ,[
            "cod_ssm" => $this->dadosSsm['cod_ssm']
        ]);

        //MATERIAL RODANTE
        $hasMR = $this->medoo->select("material_rodante_ssm", "*", ["cod_ssm"=>$this->dadosSsm['cod_ssm']]);
        if(!empty($hasMR))
        {
            if(in_array($this->dadosSsm['cod_grupo'],SISTEMAS_FIXOS))
            {
                $ssm = $this->medoo->delete("material_rodante_ssm",
                [
                    "cod_ssm" => (int)$this->dadosSsm['cod_ssm']
                ]);
            }else{
                $ssm = $this->medoo->update("material_rodante_ssm",
                array_intersect_key($this->dadosSsm, array_flip($this->fillableMR))
                ,[
                    "cod_ssm" => (int)$this->dadosSsm['cod_ssm']
                ]);
            }
            
        }
        else
        {
            $ssm = $this->medoo->insert("material_rodante_ssm",[
                array_intersect_key($this->dadosSsm, array_flip($this->fillableMR))
            ]);
        }
    }

    public function findBySaf($cod_saf){
        return $this->medoo->select("v_ssm", "*", ["cod_saf" => $cod_saf]);
    }

    public function encaminhar(){

        $dadosValidados = $this->validarDados($this->dadosSsm);
        if(!empty($dadosValidados)) {

            if(!empty($this->dadosSsm['justificativaAltNivel']))
                $justificativa = "Altera��o de n�vel => " . $this->dadosSsm['justificativaAltNivel']. ". POR: " . $this->dadosUsuario['usuario'];
            else
                $justificativa = "";

            //######### Update SSM ####################
            $this->medoo->update("ssm",
                $dadosValidados['ssm'],
                [
                    "cod_ssm" => (int)$this->dadosSsm['codigoSsm']
                ]
            );

            $this->alterarStatusSsm((int)$this->dadosSsm['codigoSsm'],12,$this->time,$this->dadosUsuario);

            $mr = $this->medoo->select('material_rodante_ssm', '*', ['cod_ssm' => (int)$this->dadosSsm['codigoSsm'] ]);

            if(!empty($mr)){
                //############## Update Material Rodante ###############
                $insertMaterialRodante = $this->medoo->update("material_rodante_ssm",
                    $dadosValidados['materialRodante'],
                    [
                        "cod_ssm" => (int)$this->dadosSsm['codigoSsm'],
                    ]
                );
            }else{
                //############## INSERT Material Rodante ###############
                $dadosValidados['materialRodante']['cod_ssm'] = $this->dadosSsm['codigoSsm'];

                $insertMaterialRodante = $this->medoo->insert("material_rodante_ssm",
                    [
                        $dadosValidados['materialRodante']
                    ]
                );
            }

            //######### Realtime Message
            $viewSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$this->dadosSsm['codigoSsm']]);
            $viewSsm = $viewSsm[0];

            $msgNotification = ("Ssm n. {$this->dadosSsm['codigoSsm']} encaminhada para {$viewSsm['sigla_equipe']}<br />Por:  {$this->dadosUsuario['usuario']}");

            $this->notificationEncaminhar($viewSsm, $dadosValidados['ssm']['cod_un_equipe'], $msgNotification, "Encaminhar");
        }else{

            return "error";
        }
    }

    public function aprovar(){
      if(!empty($this->dadosSsm['codigoSsm']))
        $this->alterarStatusSsm((int)$this->dadosSsm['codigoSsm'],16,$this->time,$this->dadosUsuario);
      else
        $this->alterarStatusSsm((int)$this->dadosSsm['cod_ssm'],16,$this->time,$this->dadosUsuario);

        $this->encerrarSaf($this->dadosSsm['cod_saf'], $this->time, $this->dadosUsuario, 14, $this->dadosSsm['observacaoValidar']);
    }

    public function aprovarReproveWithId($id, $cod_status, $justify=null){
        $this->alterarStatusSsm($id, $cod_status, $this->time, $this->dadosUsuario, $justify);

        if($cod_status == 16)
            $status_saf = 14;
        else
            $status_saf = 1;

        $cod_saf=$this->medoo->select("ssm", "cod_saf", ["cod_ssm"=>$id])[0];

        $this->encerrarSaf($cod_saf, $this->time, $this->dadosUsuario, $status_saf, $justify);
    }

    public function desaprovar(){
      if(!empty($this->dadosSsm['codigoSsm']))
        $this->alterarStatusSsm((int)$this->dadosSsm['codigoSsm'],36,$this->time,$this->dadosUsuario);
      else
        $this->alterarStatusSsm((int)$this->dadosSsm['cod_ssm'],36,$this->time,$this->dadosUsuario);


        $this->encerrarSaf($this->dadosSsm['cod_saf'], $this->time, $this->dadosUsuario, 1);
    }

    public function alterarStatus($codSsm, $status, $targetStatusCheck = true, $targetStatus, $descricao = null)
    {
      if($targetStatusCheck)
        if($this->checkIfHasStatus($codSsm, $targetStatus))
          $this->alterarStatusSsm($codSsm, $status, $this->time, $this->dadosUsuario, $descricao);
    }

    private function checkIfHasStatus($codSsm, $targetStatus)
    {
      $checkSsm = $this->medoo->select("v_ssm", 'cod_status', ["cod_ssm" => (int)$codSsm])[0];

      if($checkSsm == $targetStatus)
        return true;

      return false;
    }

    private function reorderArr($arr)
    {
        $keys = [
            "tipoIntervencao" => "cod_tipo_intervencao",
            "odometro_carro" => "odometro",
            "linhaSsm" => "cod_linha",
            "trechoSsm" => "cod_trecho",
            "grupoSistemaSsm" => "cod_grupo",
            "sistemaSsm" => "cod_sistema",
            "subSistemaSsm" => "cod_subsistema",
            "veiculoMrSsm" => "cod_veiculo",
            "carroMrSsm" => "cod_carro",
            "odometroMrSsm" => "odometro",
            "localGrupo" => "cod_local_grupo",
            "subLocalGrupo" => "cod_sublocal_grupo",
            "servicoSsm" => "cod_servico",
            "complementoServicoSsm" => "complemento",
            "nivelSsm" => "nivel",
            "codigoSsm" => "cod_ssm",
            "localSsm" => "cod_unidade",
            "equipeSsm" => "cod_equipe",
            "complementoLocalSsm" => "complemento_local",
            "posicaoSsm" => "posicao",
            "kmInicialSsm" => "km_inicial",
            "kmFinalSsm" => "km_final",
            "viaSsm" => "cod_via"
        ];

        foreach($keys as $key => $value)
        {
            if(array_key_exists($key, $arr))
                $arr[$value] = $arr[$key];
        }

        return array_filter($arr);
    }

    private function notificationGerar($selectVSaf, $numSsm, $msgNotificacao="", $tipo, $alertaAcao = false){
        //Alerta Especifico para o Usuario
        if($alertaAcao)
            $_SESSION['alertaAcao'] = $alertaAcao;

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = $msgNotificacao;
        $_SESSION['notificacao']['nomeUsuario'] = $this->usuario['usuario'];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']    = $tipo;
        $_SESSION['notificacao']['tabela'] = "Ssm";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero']          = $numSsm;
        $_SESSION['notificacao']['numeroSaf']       = $selectVSaf['cod_saf'];
        $_SESSION['notificacao']['dataAbertura']    = $this->time;
        $_SESSION['notificacao']['nivel']           = $selectVSaf['nivel'];
    }

    private function notificationEncaminhar($viewSsm, $unEquipe, $msgNotificacao="", $tipo){
        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = $msgNotificacao ;

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']                = $tipo;
        $_SESSION['notificacao']['statusAntigo']        = $this->dadosSsm['statusAntigo'];
        $_SESSION['notificacao']['tabela']              = "Ssm";

        //Informa��o para alimentar qualquer  da tabela Ssm
        $_SESSION['notificacao']['numero']              = $this->dadosSsm['codigoSsm'];
        $_SESSION['notificacao']['numeroSaf']           = $viewSsm['cod_saf'];
        $_SESSION['notificacao']['dataAbertura']        = $this->parse_timestamp($viewSsm["data_abertura"]);
        $_SESSION['notificacao']['nivel']               = $this->dadosSsm['nivelSsm'];
        $_SESSION['notificacao']['encaminhamento']      = $this->parse_timestamp($viewSsm["data_status"]);
        $_SESSION['notificacao']['sistema']             = $viewSsm["nome_sistema"];

        $_SESSION['notificacao']['veiculo']             = $viewSsm["nome_veiculo"];

        $_SESSION['notificacao']['servico']             = $viewSsm["nome_servico"];
        $_SESSION['notificacao']['complementoServico']  = $viewSsm["complemento_servico"];

        $_SESSION['notificacao']['nomeEquipe']          = $viewSsm['nome_equipe'];
        $_SESSION['notificacao']['nomeUnidade']         = $viewSsm['nome_unidade'];
        $_SESSION['notificacao']['siglaEquipe']         = $viewSsm['sigla_equipe'];
        $_SESSION['notificacao']['siglaUnidade']        = $viewSsm['sigla_unidade'];

        $_SESSION['notificacao']['nomeLinha']           = $viewSsm['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']          = $viewSsm['descricao_trecho'];

        $_SESSION['notificacao']['subChannelEquipe']    = $unEquipe;
        $_SESSION['notificacao']['nomeUsuario']         = $this->usuario['usuario'];

        $_SESSION['notificacao']['avaria']          = $viewSsm['nome_avaria'];
    }
}
