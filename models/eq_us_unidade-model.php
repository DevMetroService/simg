<?php
class EqUsUnidadeModel extends MainModel
{
    private $fillable = [
      'cod_un_equipe',
      'cod_usuario'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
    }

    public function create(){
        $newMaterial = $this->medoo->insert("eq_us_unidade",[
            array_intersect_key($_POST, array_flip($this->fillable))
        ]);
    }

    public function delete($user)
    {
        $this->medoo->delete("eq_us_unidade", [
            "AND" => [
                "cod_usuario" => (int) $user
            ]
        ]);
    }

    public function getAll()
    {
        return $this->medoo->select("usuario", "*");
    }

    public function get($id)
    {
        return $this->medoo->select("usuario", "*", ["cod_usuario" => (int) $id])[0];
    }
}
 ?>
