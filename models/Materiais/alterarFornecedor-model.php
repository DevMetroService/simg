<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/02/2017
 * Time: 17:47
 */

class AlterarFornecedorModel extends MainModel {

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dados = $_SESSION['dadosFornecedor'];
        


        $status = [];
        $status ['table_fornecedor'] = '';
        $status ['table_contato_fornecedor'] = '';
        $status ['table_dados_bancarios_fornecedor'] = '';
        $status ['table_endereco_fornecedor'] = '';

        
        $status['table_fornecedor'] = $this->medoo->update("fornecedor",
            [
                "nome_fantasia"         => $dados['nomeFornecedor'],
                "razao_social"          => $dados['razaoSocial'],
                "cnpjcpf"               => $dados['hiddenCnpj'],
                "avaliacao"             => (int)$dados['avalicaoFornecedor'],
                "descricao"             => $dados['descricaoFornecedor'],
                "optante_simples"       => $dados['simplesNacionalFornecedor'],
                "insc_estadual"         => (int)$dados['inscricaoEstadualFornecedor'],
                "insc_municipal"        => (int)$dados['inscricaoMunicipalFornecedor'],
            ],
            [
                "cod_fornecedor"        => (int)$dados['hidden_codFornecedor']
            ]
        );


        $status['table_contato_fornecedor'] = $this->medoo->update("contato_fornecedor",
            [
                "nome_contato"          => $dados['nomeContatoFornecedor'],
                "fone_fornecedor"       => $dados['hiddenTelefone'],
                "email_fornecedor"      => $dados['emailContatoFornecedor'],
                "site"                  => $dados['siteFornecedor'],
            ],
            [
                "cod_fornecedor"        => (int)$dados['hidden_codFornecedor']
            ]
        );


        $status['table_dados_bancarios_fornecedor'] = $this->medoo->update("dados_bancarios_fornecedor",
            [
                "agencia"               => $dados['agenciaBancoFornecedor'],
                "conta_corrente"        => $dados['ccBancoFornecedor'],
                "banco"                 => $dados['nomeBancoFornecedor'],
                "num_transferencia"     => $dados['numeroBancoFornecedor'],
            ],
            [
                "cod_fornecedor"        => (int)$dados['hidden_codFornecedor']
            ]
        );


        $status['table_endereco_fornecedor'] = $this->medoo->update("endereco_fornecedor",
            [
                "municipio"             => $dados['municipioEnderecoFornecedor'],
                "bairro"                => $dados['bairroEnderecoFornecedor'],
                "logradouro"            => $dados['logradouroEnderecoFornecedor'],
                "cep"                   => (int)$dados['hiddenCep'],
                "numero_endereco"       => (int)$dados['numeroEnderecoFornecedor'],
                "complemento"           => $dados['complementoEnderecoFornecedor'],
                "uf"                    => $dados['ufEnderecoFornecedor'],
            ],
            [
            "cod_fornecedor"            => (int)$dados['hidden_codFornecedor']
            ]
        );



        //Conta a quantidade de updates com exito

        $c = 0;
        foreach ($status as $key => $valor) {
            if ($valor == 1) {
                $c++;
            }
        }

        // Verifica se o total de exitos em update � ou n�o igual a quantidade de tabelas (= 4)

        if ($c != 4) {
            $_SESSION['statusUpdate'] = "HOUVE UM ERRO AO TENTAR REALIZAR ALTERA��ES. \n Fornecedor n�o alterado!";
        } else {
            $_SESSION['statusUpdate'] = "ALTERA��ES REALIZADAS COM SUCESSO!";
        }





        unset($_SESSION['dadosFornecedor']);
    }

}