<?php

    class EntradaEstoqueModel extends MainModel
    {

        function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
        {
            $this->medoo = $medoo;
            $this->phpass = $phpass;

            $material = (int) $_SESSION['entradaMaterial']['material'];

            $codForMat = $medoo->select("for_material", "cod_fm",
                [
                    "cod_fornecedor"    => (int) $_SESSION['entradaMaterial']['fornecedor'],
                    "cod_material"      => (int) $material,
                ]

            );
            echo($codForMat);
            echo($codForMat[0]);
            echo("testeAqui");
            if(!empty($codForMat[0]) ){
                $updateFornecedorMaterial = $medoo->update("for_material",
                    [
                        "preco_uni"     => 0,
                        "prazo_entrega" => (int) $_SESSION['entradaMaterial']['prazoEntrega'],
                    ],
                    [
                        "cod_fm"        => $codForMat[0],
                    ]
                );

            }else{
                $insertFornecedorMaterial = $medoo->insert("for_material",
                    [
                        "cod_material"      => (int) $material,
                        "cod_fornecedor"    => (int) $_SESSION['entradaMaterial']['fornecedor'],
                        "preco_uni"         => (int) $_SESSION['entradaMaterial']['quantidade'],
                        "prazo_entrega"     => (int) $_SESSION['entradaMaterial']['prazoEntrega']

                    ]
                );
            }

            $quantAtual = $medoo->select("estoque_material", "quant",
                [
                    "cod_material"      => $material,
                ]
            );

            if(!empty($quantAtual[0])){
                $quantAtual[0] = $quantAtual[0] + $_SESSION['entradaMaterial']['quantidade'];
                $updateEstoque = $medoo->update("estoque_material",
                    [
                        "quant"         => (int) $quantAtual[0],
                    ],
                    [
                        "cod_material"  => (int) $material,
                    ]
                );

            }else{
                $insertEstoque = $medoo->insert("estoque_material",
                    [
                        "cod_unidade"       => 1,
                        "cod_material"      => (int) $material,
                        "quant"             => (int) $_SESSION['entradaMaterial']['quantidade'],
                    ]
                );

            }


        }


        function getCodMaterial($nomeMaterial){
            $codMaterial = $this->medoo->select("material", "cod_material",
                [
                    "nome_material"     =>  $nomeMaterial,
                ]
            );

            return $codMaterial;
        }

    }
