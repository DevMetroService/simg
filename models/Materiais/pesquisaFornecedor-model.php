<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: abner.andrade
 * Date: 09/01/2018
 * Time: 14:30
 */
class PesquisaFornecedorModel extends MainModel
{
    private $dadosReturn;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo   = $medoo;
        $dadosPesquisa = $_POST;

        //Verifica se h� valores e acrescenta na variavel where


        if (!empty($dadosPesquisa['cnpj'])) {
            $where[] = "cnpjcpf = '{$dadosPesquisa['cnpj']}'";
        }

        if (!empty($dadosPesquisa['uf'])) {
            $where[] = "uf = '{$dadosPesquisa['uf']}'";
        }

        if (!empty($dadosPesquisa['avaliacao'])) {
            $where[] = "avaliacao = '{$dadosPesquisa['avaliacao']}'";
        }




        //SINTAX SQL para pesquisa no PostGres

        $sql = "SELECT {$dadosPesquisa['whereSql']}
                FROM endereco_fornecedor e
                FULL JOIN fornecedor f ON e.cod_fornecedor = f.cod_fornecedor";
        

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $this->dadosReturn = (boolval(false));
        } else {
            $this->dadosReturn = $resultado;
        }
    }

    function getDados(){
        return $this->dadosReturn;
    }
}