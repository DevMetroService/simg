<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/02/2017
 * Time: 17:47
 */

class AlterarMaterialModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dados = $_SESSION['dadosMaterial'];

        
        $inserir = $this->medoo->update("material", [
            "nome_material"     => $dados['nomeMaterial'],
            "descricao"         => $dados['descricao'],
            "quant_min"         => (float) $dados['qtdMinima'],

            "cod_categoria"     => $dados['categoria'],
            "cod_marca"         => $dados['marcaMaterial'],
            "cod_uni_medida"    => $dados['unidadeMedida'],
        ],[
            "cod_material"      => $dados['codMaterial']
        ]);

        
        
        if($inserir == 1) {            
            $_SESSION['statusUpdate'] = "Alterações realizadas com Sucesso!";            

        } else {
            $_SESSION['statusUpdate'] = "Erro ao Tentar Alterar!";            
        }




        unset($_SESSION['dadosMaterial']);
    }

}