<?php

class CadastroFornecedorModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        //--------- Dados do fornecedor ----------------


        $nomeFantasia               = (string)$_SESSION['dadosFornecedor']['nomeFornecedor'];
        $razaoSocial                = (string)$_SESSION['dadosFornecedor']['razaoSocial'];
        $cnpj                       = (string)$_SESSION['dadosFornecedor']['cnpjFornecedor'];
        $simplesNacinal             = (string)$_SESSION['dadosFornecedor']['simplesNacionalFornecedor'];
        $inscricaoMunicipal         = (int) $_SESSION['dadosFornecedor']['inscricaoMunicipalFornecedor'];
        $inscricaoEstadual          = (int) $_SESSION['dadosFornecedor']['inscricaoEstadualFornecedor'];
        $cep                        = (string)$_SESSION['dadosFornecedor']['cepFornecedor'];
        $logradouro                 = (string)$_SESSION['dadosFornecedor']['logradouroEnderecoFornecedor'];
        $numeroEndereco             = (int) $_SESSION['dadosFornecedor']['numeroEnderecoFornecedor'];
        $complementoEndereco        = (string)$_SESSION['dadosFornecedor']['complementoEnderecoFornecedor'];
        $bairroEndereco             = (string)$_SESSION['dadosFornecedor']['bairroEnderecoFornecedor'];
        $municipioEndereco          = (string)$_SESSION['dadosFornecedor']['municipioEnderecoFornecedor'];
        $ufEndereco                 = (string)$_SESSION['dadosFornecedor']['ufEnderecoFornecedor'];
        $nomeContato                = (string)$_SESSION['dadosFornecedor']['nomeContatoFornecedor'];
        $telefoneContato            = (string)$_SESSION['dadosFornecedor']['telefoneContatoFornecedor'];
        $email                      = (string)$_SESSION['dadosFornecedor']['emailContatoFornecedor'];
        $site                       = (string)$_SESSION['dadosFornecedor']['siteFornecedor'];
        $descricaoFornecedor        = (string)$_SESSION['dadosFornecedor']['descricaoFornecedor'];
        $agenciaBanco               = (string)$_SESSION['dadosFornecedor']['agenciaBancoFornecedor'];
        $contaCorrente              = (string)$_SESSION['dadosFornecedor']['ccBancoFornecedor'];
        $nomeBanco                  = (string)$_SESSION['dadosFornecedor']['nomeBancoFornecedor'];
        $numeroBanco                = (string)$_SESSION['dadosFornecedor']['numeroBancoFornecedor'];

        $medoo->insert("fornecedor",
            [
                "optante_simples"       => $simplesNacinal,
                "nome_fantasia"         => $nomeFantasia,
                "razao_social"          => $razaoSocial,
                "cnpjcpf"               => preg_replace("/\D+/", "", $cnpj),
                "insc_estadual"         => $inscricaoEstadual,
                "insc_municipal"        => $inscricaoMunicipal,
                "descricao"             => $descricaoFornecedor,
            ]
        );


        $fornecedorID = $this->medoo->select("fornecedor", "cod_fornecedor");
        $contador = count($fornecedorID);
        if($contador == 0){
            $lastFornecedor = (int) $fornecedorID[contador];
        }
        if($contador >  0){
            $lastFornecedor = (int) $fornecedorID[$contador -1];
        }

        $medoo->insert("contato_fornecedor",
            [
                "fone_fornecedor"       => $telefoneContato,
                "email_fornecedor"      => $email,
                "site"                  => $site,
                "nome_contato"          => $nomeContato,
                "cod_fornecedor"        => $lastFornecedor,
            ]
        );

        $medoo->insert("dados_bancarios_fornecedor",
            [
                "agencia"               => $agenciaBanco,
                "conta_corrente"        => $contaCorrente,
                "banco"                 => $nomeBanco,
                "num_transferencia"     => $numeroBanco,
                "cod_fornecedor"        => $lastFornecedor,
            ]
        );

        $medoo->insert("endereco_fornecedor",
            [
                "municipio"             => $municipioEndereco,
                "bairro"                => $bairroEndereco,
                "logradouro"            => $logradouro,
                "cep"                   => preg_replace("/\D+/", "", $cep),
                "numero_endereco"       => $numeroEndereco,
                "complemento"           => $complementoEndereco,
                "uf"                    => $ufEndereco,
                "cod_fornecedor"        => $lastFornecedor,
            ]
        );
    }

}