<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: abner.andrade
 * Date: 09/01/2018
 * Time: 14:30
 */
class PesquisaMaterialModel extends MainModel
{
    private $dadosReturn;
    
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo   = $medoo;
        $dadosPesquisa = $_POST;

        //Verifica se h� valores e acrescenta na variavel where

        //#########Dados Gerais
        if (!empty($dadosPesquisa['categoria'])) {
            $where[] = "m.cod_categoria = {$dadosPesquisa['categoria']}";
        }

        if (!empty($dadosPesquisa['pesquisaMaterial'])) {
            $where[] = "m.cod_material = {$dadosPesquisa['pesquisaMaterial']}";
        }

        //#########Status
        if (!empty($dadosPesquisa['unidade'])) {
            $where[] = "m.cod_uni_medida = {$dadosPesquisa['unidade']}";
        }



        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT {$dadosPesquisa['whereSql']} FROM material m 
INNER JOIN categoria_material c ON m.cod_categoria = c.cod_categoria INNER JOIN unidade_medida u
ON u.cod_uni_medida = m.cod_uni_medida";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        $sql = $sql . " ORDER BY nome_material ASC";

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $this->dadosReturn = (boolval(false));
        } else {
            $this->dadosReturn = $resultado;
        }
    }

    function getDados(){
        return $this->dadosReturn;
    }
}