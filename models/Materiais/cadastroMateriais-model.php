<?php

class CadastroMateriaisModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;


        $cod_material = (int) $_SESSION['dadosMaterial']['cod_material'];

        if (!empty($cod_material)) {
            // Faz um UPDATE

            try {
                if(!empty($_SESSION['dadosMaterial']['descricaoResumidaMaterial'])){
                    $descricao = $_SESSION['dadosMaterial']['descricaoResumidaMaterial'];
                }else{
                    $descricao = $_SESSION['dadosMaterial']['nomeMaterial'];
                }

                $insertMaterial = $medoo->update('material',
                    [
                        "cod_uni_medida"    => (integer) $_SESSION['dadosMaterial']['unidadeMedidaMaterial'],
                        "cod_categoria"     => (integer) $_SESSION['dadosMaterial']['categoriaMaterial'],
                        "cod_marca"         => (integer) $_SESSION['dadosMaterial']['marcaMaterial'],
                        "nome_material"     => (string) strtoupper($_SESSION['dadosMaterial']['nomeMaterial']),
                        "ean"               => (string) $_SESSION['dadosMaterial']['codigoBarrasMaterial'],
                        "descricao"         => (string) strtoupper($descricao),
                        "quant_min"         => (integer) $_SESSION['dadosMaterial']['qtdMinimaMaterial'],
                        "cod_grupo"         => (integer) $_SESSION['dadosMaterial']['grupoSistema'],

                    ], [
                        "cod_material" => $cod_material
                    ]
                );


                if (!empty($_SESSION['dadosMaterial']['estoqueAtual'])) {

                    $insertEstoque = $this->medoo->update("estoque",
                        [
                            "quantidade" => $_SESSION['dadosMaterial']['estoqueAtual']
                        ], [
                            "cod_material" => $cod_material
                        ]
                    );
                }
            }
            catch (Exception $erro) {
                var_dump($erro);
            }

        } else {

            if(!empty($_SESSION['dadosMaterial']['descricaoResumidaMaterial'])){
                $descricao = $_SESSION['dadosMaterial']['descricaoResumidaMaterial'];
            }else{
                $descricao = $_SESSION['dadosMaterial']['nomeMaterial'];
            }

            $insertMaterial = $medoo->insert('material',
                [
                    "cod_uni_medida"    => (integer) $_SESSION['dadosMaterial']['unidadeMedidaMaterial'],
                    "cod_categoria"     => (integer) $_SESSION['dadosMaterial']['categoriaMaterial'],
                    "cod_marca"         => (integer) $_SESSION['dadosMaterial']['marcaMaterial'],
                    "nome_material"     => (string) strtoupper($_SESSION['dadosMaterial']['nomeMaterial']),
                    "ean"               => (string) $_SESSION['dadosMaterial']['codigoBarrasMaterial'],
                    "descricao"         => (string) strtoupper($descricao),
                    "quant_min"         => (float) $_SESSION['dadosMaterial']['qtdMinimaMaterial'],
                    "cod_grupo"         => (integer) $_SESSION['dadosMaterial']['grupoSistema'],
                ]
            );

            if (!empty($_SESSION['dadosMaterial']['estoqueAtual'])) {

                $insertEstoque = $this->medoo->update("estoque",
                    [
                        "quantidade" => $_SESSION['dadosMaterial']['estoqueAtual']
                    ], [
                        "cod_material" => $cod_material
                    ]
                );
            }
        }
        unset($_SESSION['dadosMaterial']);
    }


    function getCodCategoria($nomeCategoria){
        $codCategoria = $this->medoo->select("categoria_material", "cod_categoria",
            [
               "nome_categoria"     =>  $nomeCategoria,
            ]
        );

        return $codCategoria;
    }

    function getCodMarca($nomeMarca){
        $codMarca = $this->medoo->select("marca_material", "cod_marca",
            [
                "nome_marca"     =>  $nomeMarca,
            ]
        );

        return $codMarca;
    }

}
