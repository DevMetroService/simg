<?php
/**
 * Created by VS Code.
 * User: josue.marques
 * Date: 03/12/2021
 * Time: 09:29
 */

class LocalModel extends MainModel{

    private $fillable = [
        "nome_local",
        "cod_linha",
        "cod_estacao",
        "grupo",
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
    }

    function create()
    {
        $new = $this->medoo->insert("local",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        return $new;
    }

    function update($id)
    {
        $this->medoo->update("local",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_local" => $id
         ]);
    }

    public function all(){
        return $this->medoo->select("local", 
        [
            "[><]linha" => "cod_linha",
            "[><]estacao" => "cod_estacao",
        ],"*");
    }
}
