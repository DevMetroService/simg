<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 27/08/2020
 * Time: 14:56
 */


class FuncionarioModel extends MainModel
{

    private $time;
    private $usuario;

    private $fillableFuncionario=[
      'nome_funcionario',
      'data_nasc',
      'cpf_cnpj',
      'ativo',
      'cod_funcionario_empresa'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->time = date('d-m-Y H:i:s', time());
    }

    public function getList()
    {
        return $this->medoo->select("funcionario", "*");
    }

    public function create()
    {
        return $this->medoo->insert('funcionario', [
            array_intersect_key($_POST, array_flip($this->fillableFuncionario))
        ]);
    }

    public function update($codFuncionario)
    {
        //Atualiza os dados relacionados a tabela FUNCIONARIO
        $funcionario = $this->medoo->update('funcionario',
            array_intersect_key($_POST, array_flip($this->fillableFuncionario))
        , [
            "cod_funcionario" => (int)$codFuncionario
        ]);
        
        return array_intersect_key($_POST, array_flip($this->fillableFuncionario));
    }

    public function getFuncionario($id)
    {
        return $dados = $this->medoo->select("funcionario_empresa",["[><]funcionario(f)"=>"cod_funcionario_empresa"],
            [
                "funcionario_empresa.matricula(matriculaF)",
                "f.nome_funcionario",
                "f.cod_funcionario",
                "f.cpf_cnpj",
                "f.data_nasc",
                "f.ativo"
            ], [
                "f.cod_funcionario" => (int)$id
            ]
        )[0];
    }

    public function getFuncionarioByOsm($codOsm)
    {
      return $dados = $this->medoo->select("funcionario_empresa(fe)",
          [
              "[><]funcionario"=>"cod_funcionario_empresa",
              "[><]osm_mao_de_obra(omo)"=>["fe.cod_funcionario" => "cod_funcionario"]
          ],[
              "fe.matricula",
              "funcionario.nome_funcionario",
              "funcionario.cod_funcionario",
              "funcionario.cpf_cnpj",
              "funcionario.data_nasc",
              "omo.data_termino",
              "omo.data_inicio",
              "omo.total_hrs",
              "omo.cod_osmao"
          ], [
              "cod_osm" => (int)$codOsm
          ]
      );
    }

    public function getFuncionarioByOsp($codOs)
    {
      return $dados = $this->medoo->select("funcionario",
          [
              "[><]funcionario_empresa(fe)"=>"cod_funcionario_empresa",
              "[><]osp_mao_de_obra(omo)"=>["fe.cod_funcionario" => "cod_funcionario"]
          ],[
              "fe.matricula",
              "funcionario.nome_funcionario",
              "funcionario.cod_funcionario",
              "funcionario.cpf_cnpj",
              "funcionario.data_nasc",
              "omo.data_termino",
              "omo.data_inicio",
              "omo.total_hrs",
              "omo.cod_ospmao"
          ], [
              "cod_osp" => (int)$codOs
          ]
      );
    }

    public function getFuncionarioByOsmp($codOs)
    {
      return $dados = $this->medoo->select("funcionario_empresa(fe)",
          [
              "[><]funcionario"=>"cod_funcionario_empresa",
              "[><]osmp_mao_de_obra(omo)"=>["fe.cod_funcionario" => "cod_funcionario"]
          ],[
              "fe.matricula",
              "funcionario.nome_funcionario",
              "funcionario.cod_funcionario",
              "funcionario.cpf_cnpj",
              "funcionario.data_nasc",
              "omo.data_termino",
              "omo.data_inicio",
              "omo.total_hrs",
              "omo.cod_osmp_mao_de_obra"
          ], [
              "cod_osmp" => (int)$codOs
          ]
      );
    }

    public function getFuncionarioByTipoEmpresa($tipo)
    {
        return $this->medoo->select(
            "funcionario(f)",
            [
                "[>]dados_empresa_funcionario" => "cod_funcionario",
                "[>]centro_resultado(cr)"      => "cod_centro_resultado",
                "[><]funcionario_empresa(fe)"  => "cod_funcionario_empresa",
                "[>]cargos_funcionarios(cg)"   => ["fe.cod_cargos" => "cod_cargos"]
            ], [
                "fe.matricula",
                "f.cod_funcionario",
                "f.nome_funcionario",
                "cg.descricao(cargo)",
                "cr.descricao(centro_resultado)"
            ],[
                "fe.cod_tipo_funcionario"  => $tipo
            ]
        );
    }

    public function getDadosEmpresa($id)
    {
        return $this->medoo->select("dados_empresa_funcionario", "*", ["cod_funcionario" => (int)$id])[0];
    }

    public function getEmpresa($id)
    {
        $sql = "SELECT * FROM funcionario_empresa WHERE cod_funcionario_empresa = (SELECT cod_funcionario_empresa FROM funcionario WHERE cod_funcionario = $id)";
        return $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC)[0];
    }

    public function getContato($id)
    {
        return $this->medoo->select("contato_funcionario", "*", ["cod_funcionario" => (int)$id])[0];
    }

    public function getEscolaridade($id)
    {
        return $this->medoo->select("escolaridade_funcionario", "*", ["cod_funcionario" => (int)$id])[0];
    }
}
