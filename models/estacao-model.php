<?php
/**
 * Created by VS Code.
 * User: josue.marques
 * Date: 03/12/2021
 * Time: 09:29
 */

class EstacaoModel extends MainModel{

    private $fillable = [
        "nome_estacao",
        "cod_linha",
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
    }

    function create()
    {
        $new = $this->medoo->insert("estacao",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        return $new;
    }

    function update($id)
    {
        $this->medoo->update("estacao",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_estacao" => $id
         ]);
    }

    public function all(){
        return $this->medoo->select("estacao", 
        [
            "[><]linha" => "cod_linha"
        ],"*");
    }
}
