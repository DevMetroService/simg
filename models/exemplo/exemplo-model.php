<?php
/**
 * 
 * @author Valmar
 *
 */
class ExemploModel extends MainModel {
	/**
	 */
	public function __construct($bancoDados = true, $controller = null, $medoo = true) {
		// Configurando PDO
		$this->bancoDados = $bancoDados;
		
		//
		$this->controller = $controller;
		
		// Parametros
		$this->parametros = $this->controller->parametros;
		
		
		$this->medoo = $medoo->info();
		
		//
		 $dadosUsuario= $this->controller->dadosUsuario;
		
		echo 'Modelo carregado... <br>';
	}
}

?>