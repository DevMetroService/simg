<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class PmpModel extends MainModel{

    private $dadosPmp;

    private $time;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
        
        $this->dadosPmp  = $_SESSION['dadosPmp'];
        unset($_SESSION['dadosPmp']);
        
        $this->time = date('d-m-Y H:i:s', time());
    }

    public function cadastroProcedimento(){
        for ($i = 1; $i < $this->dadosPmp['counter']; $i++) {
            $servicoPmpSubSistema = $this->medoo->select('servico_pmp_sub_sistema', ['[><]sub_sistema' => 'cod_sub_sistema'], 'cod_servico_pmp_sub_sistema',
                [
                    "AND" => [
                        'sub_sistema.cod_sistema'       => $this->dadosPmp['sistema' . $i],
                        'sub_sistema.cod_subsistema'    => $this->dadosPmp['subSistema' . $i],
                        'cod_servico_pmp'               => $this->dadosPmp['servicoPmp' . $i]
                    ]
                ]);
            $servicoPmpSubSistema = $servicoPmpSubSistema[0];

            $this->medoo->insert('servico_pmp_periodicidade', [
                "cod_servico_pmp_sub_sistema"   => $servicoPmpSubSistema,
                "cod_tipo_periodicidade"        => (int)$this->dadosPmp['periodicidade' . $i],
                "cod_procedimento"              => (int)$this->dadosPmp['procedimento' . $i],
            ]);
        }

        for ($i = 1; $i < $this->dadosPmp['counterExec']; $i++) {
            if(!empty($this->dadosPmp["removeRow{$i}"])){
                $flagPmp = $this->medoo->select('pmp', 'cod_servico_pmp_periodicidade', ['cod_servico_pmp_periodicidade' => (int)$this->dadosPmp["codServPer{$i}"]]);

                if(!empty($flagPmp)) {
                    $_SESSION['erroProcedimento'][] = "O Servi�o/Procedimento cod {$this->dadosPmp["codServPer{$i}"]} n�o poder� ser excluido pois j� est� interligado a algum item da pmp.";
                }else {
                    $this->medoo->delete('servico_pmp_periodicidade',
                        [
                            "cod_servico_pmp_periodicidade" => (int)$this->dadosPmp["codServPer{$i}"]
                        ]
                    );
                }
            }
        }
    }

    public function insertListaPmp(){
        for ($i = 1; $i < $this->dadosPmp['counter']; $i++) {
            $servicoPmpPeriodicidade = $this->medoo->select('servico_pmp_periodicidade',
                [
                    '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                    '[><]sub_sistema' => 'cod_sub_sistema'
                ],
                'cod_servico_pmp_periodicidade',
                [
                    "AND" => [
                        'cod_tipo_periodicidade'                    => $this->dadosPmp['periodicidade' . $i],
                        'sub_sistema.cod_sistema'                   => $this->dadosPmp['sistema' . $i],
                        'sub_sistema.cod_subsistema'                => $this->dadosPmp['subSistema' . $i],
                        'servico_pmp_sub_sistema.cod_servico_pmp'   => $this->dadosPmp['servicoPmp' . $i]
                    ]
                ]);
            $servicoPmpPeriodicidade = $servicoPmpPeriodicidade[0];

            $insertLinhaPmp = $this->medoo->insert('pmp', [
                "cod_servico_pmp_periodicidade" => $servicoPmpPeriodicidade,
                "mao_obra"      => (int)$this->dadosPmp['maoObra' . $i],
                "horas_uteis"   => (int)$this->dadosPmp['horasUteis' . $i],
                "homem_hora"    => (int)$this->dadosPmp['homemHora' . $i],
                "turno"         => $this->dadosPmp['turno' . $i],
                "quinzena"      => (int)$this->dadosPmp['quinzena' . $i],
                "cod_grupo"     => (int)$this->dadosPmp['grupo' . $i],
                "ativo"         => "E" // Item Pmp Editavel
            ]);

            switch ($this->dadosPmp['grupo' . $i]){
                case 20: // Rede A�rea
                    $this->medoo->insert('pmp_rede_aerea', [
                        "cod_pmp"   => $insertLinhaPmp,
                        "cod_local" => (int)$this->dadosPmp['local' . $i],
                        "cod_poste" => (int)$this->dadosPmp['poste' . $i]
                    ]);
                    break;
                case 21: // Edifica��es
                    $this->medoo->insert('pmp_edificacao', [
                        "cod_pmp"   => $insertLinhaPmp,
                        "cod_local" => (int)$this->dadosPmp['local' . $i]
                    ]);
                    break;
                case 24: // Via Permanente
                    $this->medoo->insert('pmp_via_permanente', [
                        "cod_pmp"               => $insertLinhaPmp,
                        "cod_via"               => (int) $this->dadosPmp['via' . $i],
                        "cod_estacao_inicial"   => (int) $this->dadosPmp['trechoInicial' . $i],
                        "cod_estacao_final"     => (int) $this->dadosPmp['trechoFinal' . $i],
                        "posicao"               => (int) $this->dadosPmp['posicao' . $i],
                        "km_inicial"            => (string) $this->dadosPmp['kmInicial' . $i],
                        "km_final"              => (string) $this->dadosPmp['kmFinal' . $i],
                        "cod_amv"               => (int) $this->dadosPmp['amv' . $i],
                    ]);
                    break;
                case 25: // Subesta��o
                    $this->medoo->insert('pmp_subestacao', [
                        "cod_pmp"   => $insertLinhaPmp,
                        "cod_local" => (int)$this->dadosPmp['local' . $i]
                    ]);
                    break;
                case 22: // Material Rodante VLT
                    $this->medoo->insert('pmp_vlt', [
                        "cod_pmp"       => $insertLinhaPmp,
                        "cod_veiculo"   => (int)$this->dadosPmp['veiculo' . $i]
                    ]);
                    break;
                case 27: // Telecom
                    $this->medoo->insert('pmp_telecom', [
                        "cod_pmp"       => $insertLinhaPmp,
                        "cod_local"   => (int)$this->dadosPmp['local' . $i]
                    ]);
                    break;
                case 28: // Bilhetagem
                    $this->medoo->insert('pmp_bilhetagem', [
                        "cod_pmp"   => $insertLinhaPmp,
                        "cod_estacao" => (int)$this->dadosPmp['estacao' . $i]
                    ]);
                    break;
                case 30: // Transportes Verticais
                    $this->medoo->insert('pmp_transportes_verticais', [
                        "cod_pmp"   => $insertLinhaPmp,
                        "cod_local" => (int)$this->dadosPmp['local' . $i]
                    ]);
                    break;
                case 31: // Jardins e �reas Verdes
                    $this->medoo->insert('pmp_jardins', [
                        "cod_pmp"   => $insertLinhaPmp,
                        "cod_estacao" => (int)$this->dadosPmp['estacao' . $i]
                    ]);
                    break;
            }
        }

        for ($i = 1; $i < $this->dadosPmp['counterExec']; $i++) {
            if (!empty($this->dadosPmp['excluirLinhaPmpBanco' . $i])) {
                $flagPmp = $this->medoo->select('pmp', 'ativo', ['cod_pmp' => (int) $this->dadosPmp['excluirLinhaPmpBanco' . $i]]);
                $flagPmp = $flagPmp[0];

                if($flagPmp == "E"){
                    $this->medoo->delete('pmp',
                        [
                            "cod_pmp" => (int)$this->dadosPmp['excluirLinhaPmpBanco' . $i]
                        ]
                    );
                }else{
                    $this->medoo->update('pmp', ['ativo' => "D"],
                        [
                            "cod_pmp" => (int)$this->dadosPmp['excluirLinhaPmpBanco' . $i]
                        ]
                    );
                }
            }
        }
    }

    public function depreciarAtivarDeletar(){
        $codPmp = $this->dadosPmp['codPmp'];

        $flagPmp = $this->medoo->select('pmp', 'ativo', ['cod_pmp' => (int) $codPmp]);
        $flagPmp = $flagPmp[0];

        if($flagPmp == "E"){ // Excluir
            $this->medoo->delete('pmp',
                [
                    "cod_pmp" => (int)$codPmp
                ]
            );
        }else if($flagPmp == "D"){ // Ativar
            $this->medoo->update('pmp', ['ativo' => "A"],
                [
                    "cod_pmp" => (int)$codPmp
                ]
            );
        }else{ // Depreciar
            $this->medoo->update('pmp', ['ativo' => "D"],
                [
                    "cod_pmp" => (int)$codPmp
                ]
            );
        }
    }

    public function gerarPmpAnual($grupos = ['20','21','24','25','27','28', '31'], $ano = null, $ativo = ['A', 'E']){
        if($ano == null) {
            //N�O MEXER, Olhe no CadastroGeral.gerarPmpAnual()
            //N�O MEXER
            //N�O MEXER
            $proxAno = date('Y') + 1;
        }else {
            $proxAno = $ano;
        }

        // Seleciona todos os PMP's Ativos e Abertos (Edit�veis)
        // Ordenado pela periodicidade, para garantir a hierarquia
        $pmp = $this->medoo->select("pmp", [
            '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade'
        ], '*', [
            'ORDER' => [
                "cod_tipo_periodicidade" => "DESC",
            ], 'AND' => [
                'ativo' => $ativo,
                'cod_grupo' => $grupos
            ]
        ]);

        // Loop em todas as quinzenas do ano
        for ($quinzena = 1; $quinzena <= 24; $quinzena++) {


            //Verifica se h� cronograma com a quinzena/mes e ano corretos para a sequ�ncia.
            $selectCronograma = $this->medoo->select('cronograma','*',["AND" => ["quinzena" => (int) $quinzena, "ano" => (int) $proxAno]]);
            $selectCronograma = $selectCronograma[0];

            // Se n�o existir adiciona a linha no cronograma
            if(!empty($selectCronograma)){
                $cronograma = $selectCronograma['cod_cronograma'];
            }else{
                // Guarda o m�s
                if(($quinzena % 2) == 0) {
                    $mes = $quinzena / 2;
                }else{
                    $mes = ($quinzena + 1) / 2;
                }
                $cronograma = $this->medoo->insert("cronograma",[
                    "quinzena"          => (int) $quinzena,
                    "mes"               => (int) $mes,
                    "ano"               => (int) $proxAno,
                    "data_abertura"     => $this->time
                ]);
            }
            //Fim da verifica��o/cria��o do item em cronograma.

            if (!empty($pmp)) {
                // Seleciona apenas o PMP que aparecem nessa quinzena
                $dadosQzn = $this->filtrarPorQzn($pmp, $quinzena);

                // Variavel para evitar duplicidade de servi�os e garantir a Hieraquia das periodicidades
                $dadosArmazenados = array(
                    20 => array(), // Rede Aerea
                    21 => array(), // Edifica��es
                    24 => array(), // Via Permanente
                    25 => array(), // Subesta��o
                    27 => array(), // Telecom
                    28 => array(), // Bilhetagem
                    31 => array(), // Jardins e �reas Verdes
                    22 => array()  // Material Rodante Vlt
                );

                if (!empty($dadosQzn)) {
                    foreach ($dadosQzn as $dado) {
                        // Barra todos os PMP's que uma periodicidade superior j� foi cadastrada
                        $arrayController = null;

                        switch ($dado['cod_grupo']){
                            case 20: // Rede A�rea
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_local'], $dado['cod_poste']);
                                break;
                            case 21: // Edifica��es
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_local']);
                                break;
                            case 24: // Via Permanente
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_estacao_inicial'], $dado['cod_estacao_final'], $dado['km_inicial'], $dado['km_final'], $dado['cod_amv']);
                                break;
                            case 25: // Subesta��o
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_local']);
                                break;
                            case 22: // Material Rodante Vlt
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_veiculo']);
                                break;
                            case 27: // Telecom
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_local']);
                                break;
                            case 28: // Bilhetagem
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_estacao']);
                                break;
                            case 31: // Jardins e �reas Verdes
                                $arrayController = array($dado['cod_servico_pmp_sub_sistema'], $dado['cod_estacao']);
                                break;
                        }
                        if (!in_array($arrayController, $dadosArmazenados[$dado['cod_grupo']])) {

                            $cronogramaPmp = $this->medoo->insert('cronograma_pmp', [
                                'cod_cronograma' => $cronograma,
                                'cod_pmp'        => $dado['cod_pmp']
                            ]);

                            $inserirStatusCronograma = $this->medoo->insert("status_cronograma_pmp",[
                                "cod_status"            => 32, //Aberta
                                "cod_cronograma_pmp"    => (int) $cronogramaPmp,
                                "data_status"           => $this->time,
                                "usuario"               => (int) $this->dadosUsuario['cod_usuario']
                            ]);

                            $this->medoo->update("cronograma_pmp",[
                                "cod_status_cronograma_pmp" => (int) $inserirStatusCronograma
                            ], [
                                "cod_cronograma_pmp"        => (int)$cronogramaPmp
                            ]);

                            $this->medoo->update("pmp",[
                                "ativo" => "A"
                            ], [
                                'cod_pmp' => $dado['cod_pmp']
                            ]);


                            if ($dado['cod_tipo_periodicidade'] == 2) { // Se a periodicidade for Semanal

                                $cronogramaPmp = $this->medoo->insert('cronograma_pmp', [
                                    'cod_cronograma' => $cronograma,
                                    'cod_pmp'        => $dado['cod_pmp']
                                ]);

                                $inserirStatusCronograma = $this->medoo->insert("status_cronograma_pmp",[
                                    "cod_status"            => 32, //Aberta
                                    "cod_cronograma_pmp"    => (int) $cronogramaPmp,
                                    "data_status"           => $this->time,
                                    "usuario"               => (int) $this->dadosUsuario['cod_usuario']
                                ]);

                                $this->medoo->update("cronograma_pmp",[
                                    "cod_status_cronograma_pmp" => (int) $inserirStatusCronograma
                                ], [
                                    "cod_cronograma_pmp"        => (int)$cronogramaPmp
                                ]);
                            }

                            // Adiciona o servi�o cadastrado para n�o ser cadastrado o mesmo servi�o em hierarquias inferiores
                            $dadosArmazenados[$dado['cod_grupo']][] = $arrayController;
                        }
                    }
                }
            }
        }
    }

    function filtrarPorQzn($dados, $quinzena) {
        foreach ($dados as $dado) {
            $qznInicial = $dado['quinzena'];
            $perio = $dado['cod_tipo_periodicidade'];

            while ($qznInicial <= $quinzena) { // Enquanto a qzn for inferior a Quinzena pedida

                if ($quinzena == $qznInicial) {
                    $dadosQzn[] = $this->getDadoPmp($dado); // Adicionado os dados da pmp para ser adicionano ao cronograma desta quinzena
                }

                switch ($perio) { // Ajuste conforme a periodicidade
                    case 9: // Anual // a cada 12 meses
                        $qznInicial += 24;
                        break;
                    case 8: // Semestral // a cada 6 meses
                        $qznInicial += 12;
                        break;
                    case 7: // Quadrimestral // a cada 4 meses
                        $qznInicial += 8;
                        break;
                    case 6: // Trimestral // a cada 3 meses
                        $qznInicial += 6;
                        break;
                    case 5: // Bimestral // a cada 2 meses
                        $qznInicial += 4;
                        break;
                    case 4: // Mensal // a cada m�s
                        $qznInicial += 2;
                        break;
                    case 3: // Quinzenal // uma vez na quinzena
                        $qznInicial += 1;
                        break;
                    case 2: // Semanal // duas vezes na quinzena
                        $qznInicial += 1;
                        break;
                    case 1: // Diario // uma vez na quinzena com quantidade de dias de servi�o em 15
                        $qznInicial += 1;
                        break;
                }
            }
        }

        return $dadosQzn;
    }

    function getDadoPmp($pmp){
        $dado = null;
        switch ($pmp['cod_grupo']){
            case 20: // Rede A�rea
                $tabelaGrupo = 'rede_aerea';
                break;
            case 21: // Edifica��es
                $tabelaGrupo = 'edificacao';
                break;
            case 24: // Via Permanente
                $tabelaGrupo = 'via_permanente';
                break;
            case 25: // Subesta��o
                $tabelaGrupo = 'subestacao';
                break;
            case 22: // Material Rodante Vlt
                $tabelaGrupo = 'vlt';
                break;
            case 27: // Telecom
                $tabelaGrupo = 'telecom';
                break;
            case 28: // Bilhetagem
                $tabelaGrupo = 'bilhetagem';
                break;
            case 31: // Jardins e �reas Verdes
                $tabelaGrupo = 'jardins';
                break;
        }
        $dado = $this->medoo->select("pmp", [
            '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
            '[><]pmp_'.$tabelaGrupo => 'cod_pmp'
        ], '*', [
            'cod_pmp' => $pmp['cod_pmp']
        ]);

        $dado = $dado[0];
        return $dado;
    }
}