<?php 
class DadosEmpresaModel extends MainModel
{

    private $dados;
    private $time;
    private $usuario;

    private $fillable=[
        'cod_cargos',
        'cod_centro_resultado',
        'cod_unidade',
        'cod_funcionario'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->time = date('d-m-Y H:i:s', time());
    }
    
    public function create()
    {
        $dadosCorp = $this->medoo->insert('dados_empresa_funcionario', [
            array_intersect_key($_POST, array_flip($this->fillable))
        ]);

        return $dadosCorp;
    }

    public function update($id)
    {
        $dadosCorp = $this->medoo->update('dados_empresa_funcionario', 
            array_intersect_key($_POST, array_flip($this->fillable))
        ,[
            "cod_funcionario" => $id
        ]);
    }

    public function getByEmployer($id)
    {
        return $this->medoo->select("dados_empresa_funcionario", "*", ["cod_funcionario" => $id]);
    }
}