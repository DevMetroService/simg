<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class ProcedimentoModel extends MainModel{

    private $fillable = [
        "cod_grupo",
        "nome_procedimento"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
    }

    function create()
    {
        $new = $this->medoo->insert("procedimento",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        return $new;
    }

    function update($id)
    {
        $this->medoo->update("procedimento",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_procedimento" => $id
         ]);
    }

    public function all(){
        return $this->medoo->select("procedimento", ["[><]grupo" => "cod_grupo"], "*");
    }
}
