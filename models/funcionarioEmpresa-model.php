<?php 
class FuncionarioEmpresaModel extends MainModel
{

    private $dados;
    private $time;
    private $usuario;

    private $fillable=[
        'matricula',
        'cod_cargos',
        'cod_tipo_funcionario',
        'cod_funcionario'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->time = date('d-m-Y H:i:s', time());
    }
    
    public function create()
    {
        return $this->medoo->insert('funcionario_empresa',[
            array_intersect_key($_POST, array_flip($this->fillable))
        ]);
        return $this->medoo->error();
    }

    public function update($id)
    {
        $insertContatoSimples = $this->medoo->update('funcionario_empresa',
            array_intersect_key($_POST, array_flip($this->fillable))
        , [
            "cod_funcionario_empresa" => (int)$id
        ]);
        return $this->medoo->error();
    }

    public function getActualEmployer($id)
    {
        return $this->medoo->query("select fe.cod_tipo_funcionario, fe.cod_cargos, cod_funcionario_empresa, fe.matricula
        from funcionario f
        JOIN funcionario_empresa fe USING(cod_funcionario_empresa)
        WHERE f.cod_funcionario = {$id}")->fetchAll(PDO::FETCH_ASSOC)[0];

    }
}