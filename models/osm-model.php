<?php

class OsmModel extends MainModel
{

    private $dados;
    private $time;

    private $fillable =
    [
        "cod_ssm",
        "grupo_atuado",
        "sistema_atuado",
        "trecho_atuado",
        "usuario_responsavel",
        "complemento_local",
        "km_inicial",
        "km_final",
        "cod_via",
        "cod_ostatus",
        "cod_ponto_notavel_atuado",
        "cod_linha_atuado",
        "posicao_atuado",
        "data_abertura",
        "subsistema_atuado",
        "cod_local_grupo",
        "cod_sublocal_grupo",
        "nivel"
    ];

    private $fillableServico = 
    [
        "cod_osm",
        "cod_servico",
        "unidade_medida",
        "unidade_tempo",
        "complemento",
        "qtd_pessoas",
        "total_servico",
        "tempo"
    ];

    private $fillableMR =
    [
        "cod_osm",
        "cod_veiculo",
        "cod_carro",
        "odometro",
        "cod_prefixo",
        "cod_grupo"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->dados = $_POST;

        $this->time = date('d-m-Y H:i:s', time());
    }


    public function create()
    {
        //Recebe a informa��es da SSM origem
        $ssm = $this->medoo->select("v_ssm", "*",["cod_ssm" => $this->dados['codigoSsm']])[0];

        if($this->checkIfHasOsmOpen($ssm['cod_ssm']) )
            return;

        $ssm = $this->reorderArray($ssm); //Adequa as colunas para o padr�o de OSM

        //Insere as informa��es
        $insertOsm = $this->medoo->insert("osm_falha", [
            array_intersect_key($ssm, array_flip($this->fillable))
        ]);
        $ssm['cod_osm'] = $insertOsm; //recebe o novo codigo da OSM
        
        //Verifica se pertecen aos grupos de material rodante
        if(!in_array($ssm['cod_grupo'], SISTEMAS_FIXOS)){
            $insertOsmMr = $this->medoo->insert("material_rodante_osm", [
                array_intersect_key($ssm, array_flip($this->fillableMR))
            ]);

            //Verifica se o status da ssm � 12 --Encaminhada
            if($ssm['cod_status'] == 12)
                $this->medoo->update('veiculo', ["disponibilidade" => 'n'], ["cod_veiculo" => (int)$this->dados['cod_veiculo']]);
        }

        //Altera status e atualiza o formul�rio da SSM e OSM
        $this->alterarStatusOsm($insertOsm, 10, $this->time, $this->dadosUsuario);
        $this->alterarStatusSsm($this->dados['codigoSsm'], 5, $this->time, $this->dadosUsuario);         

        //Insere as informa��es de servi�o
        $insertServicoOsm = $this->medoo->insert("osm_servico", [
            array_intersect_key($ssm, array_flip($this->fillableServico))
        ]);

        $this->notificationCreate($ssm, $insertOsm);
    }

    private function reorderArray($arr)
    {
        $keys = [
            "cod_grupo"=>"grupo_atuado",
            "cod_sistema"=>"sistema_atuado",
            "cod_subsistema"=>"subsistema_atuado",
            "cod_linha"=>"cod_linha_atuado",
            "cod_trecho"=>"trecho_atuado",
            "cod_ponto_notavel"=>"cod_ponto_notavel_atuado",
            "posicao" => "posicao_atuado",
            "complemento_servico" => "complemento",
            "odometro_ssm" => "odometro"
        ];

        $arr['data_abertura'] = $this->parse_timestamp($this->time, 'Y-m-d H:i:s');

        foreach($keys as $key => $value)
        {
            if(array_key_exists($key, $arr))
                $arr[$value] = $arr[$key];
        }

        return array_filter($arr);
    }

    public function findBySsm($cod_ssm) {
        return $this->medoo->select("v_osm", "*", ["cod_ssm" => $cod_ssm]);
    }

    private function checkIfHasOsmOpen($cod_ssm)
    {
        $hasOsm = $this->medoo->select("v_osm", "*", 
        [
            "AND" =>
            [
                "cod_ssm" => $cod_ssm,
                "cod_status" => 10
            ]
        ])[0];

        if( !empty($hasOsm) )
            return true;
        else
            return false;
    }

    private function notificationCreate($ssm, $insertOsm)
    {
        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = "Ssm n. {$ssm['cod_ssm']} autorizada.
                                        <br />Osm n. {$insertOsm} aberta.
                                        <br />Por: {$this->dadosUsuario['usuario']}.";

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']         = "Aberta";
        $_SESSION['notificacao']['tabela']       = "Osm";
        $_SESSION['notificacao']['statusAntigo'] = $ssm['nome_status'];

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero']           = $insertOsm;
        $_SESSION['notificacao']['numeroSsm']        = $ssm['cod_ssm'];
        $_SESSION['notificacao']['dataAbertura']     = $this->time;
        $_SESSION['notificacao']['nivel']            = $ssm['nivel'];

        $_SESSION['notificacao']['veiculo']          = $ssm['nome_veiculo'];

        $_SESSION['notificacao']['nomeEquipe']       = $ssm['nome_equipe'];
        $_SESSION['notificacao']['nomeUnidade']      = $ssm['nome_unidade'];
        $_SESSION['notificacao']['siglaEquipe']      = $ssm['sigla_equipe'];
        $_SESSION['notificacao']['siglaUnidade']     = $ssm['sigla_unidade'];

        $_SESSION['notificacao']['nomeLinha']        = $ssm['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']       = $ssm['descricao_trecho'];

        $_SESSION['notificacao']['subChannelEquipe'] = $ssm['cod_un_equipe'];
        $_SESSION['notificacao']['nomeUsuario'] = $this->dadosUsuario['usuario'];
    }
}