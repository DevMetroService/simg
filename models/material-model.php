<?php
class MaterialModel extends MainModel
{
    public $dados;
    public $time;

    private $fillableMaterial = [
      'cod_categoria',
      'cod_marca',
      'cod_uni_medida',
      'nome_material',
      'ean',
      'descricao',
      'quant_min',
      'cod_marcador',
      'tipo_origem',
      'cod_grupo',
      'ativo',
      'valor_unitario',
      'cod_tipo_material'
    ];

    private $fillableQuantidade = [
    'quantidade'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->time = date('d-m-Y H:i:s', time());

        if(!empty($_POST))
            $this->dados = $_POST;
    }

    public function create(){
        $newMaterial = $this->medoo->insert("material",[
            array_intersect_key($this->dados, array_flip($this->fillableMaterial))
        ]);

        $this->medoo->insert("estoque",[
            "quantidade" => $this->dados['quantidade'],
            "cod_material" => (int)$newMaterial
        ]);
    }

    public function update($codMaterial){

        $this->medoo->update("material",
            array_intersect_key($this->dados, array_flip($this->fillableMaterial))
        ,[
            'cod_material' =>(int)$codMaterial
        ]);

        $hasEstoque = $this->medoo->select("estoque", "*", ['cod_material' => $codMaterial])[0];

        if(!empty($hasEstoque))
            $this->medoo->update("estoque",[
                "quantidade" => $this->dados['quantidade']
            ],[
                'cod_material' =>(int)$codMaterial
            ]);
        else
            $this->medoo->insert("estoque",[
                "quantidade" => $this->dados['quantidade'],
                "cod_material" => (int)$codMaterial
            ]);
    }

    public function disabled(){
        $this->medoo->update("material",[
            "ativo" => $this->dados['ativo'],
        ],[
          $this->dados['codMaterial']
        ]);
    }

    public function getAll($whereActive = null)
    {
        if($whereActive == null)
            return $this->medoo->select("v_material", "*");
        else            
            return $this->medoo->select("v_material", "*", ["AND" => $whereActive]);
    }

    public function get($id)
    {
        return $this->medoo->select("v_material", "*", ["cod_material" => (int) $id])[0];
    }

    public function getByGroup($codGrupo)
    {
        return $this->medoo->select("v_material", "*", ["cod_grupo" => [$codGrupo] ])[0];
    }

}
 ?>
