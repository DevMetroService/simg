<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class LinhaModel extends MainModel{

    private $fillable = [
        "nome_linha"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
    }

    function create()
    {
        $new = $this->medoo->insert("linha",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        return $new;
    }

    function update($id)
    {
        $this->medoo->update("linha",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_linha" => $id
         ]);
    }

    public function all(){
        return $this->medoo->select("linha", "*");
    }
}
