<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class PontoNotavelModel extends MainModel{

    private $fillable = [
        "cod_trecho",
        "nome_ponto",
        "km"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
    }

    function create()
    {
        $new = $this->medoo->insert("ponto_notavel",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        return $new;
    }

    function update($id)
    {
        $this->medoo->update("ponto_notavel",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_ponto_notavel" => $id
         ]);
    }

    public function all(){
        return $this->medoo->select("ponto_notavel", 
        [
            "[><]trecho" => "cod_trecho"
        ],"*");
    }
}
