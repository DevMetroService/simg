<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 25/02/2019
 * Time: 11:32
 */

class CrudCamposModel extends MainModel
{
    public $dados;
    public $time;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->dados = $_POST;

        $this->time = date('d-m-Y H:i:s', time());
    }

    public function createAvaria(){
            $this->medoo->insert("avaria",[
                "nome_avaria"           => $this->dados['nome_avaria'],
                "sugestao_nivel"        => $this->dados['sugestao_nivel'],
                "cod_grupo"             => (int) $this->dados['cod_grupo'],
                "ativo"                 => $this->dados['ativo'],
            ]);
    }

    public function updateAvaria(){
        $this->medoo->update("avaria",[
            "nome_avaria"           => $this->dados['nome_avaria'],
            "sugestao_nivel"        => $this->dados['sugestao_nivel'],
            "cod_grupo"             => (int) $this->dados['cod_grupo'],
            "ativo"                 => $this->dados['ativo'],
        ],[
            "cod_avaria"            => (int) $this->dados['cod_avaria'],
        ]);
    }

    public function createLocalGrupo(){
            $this->medoo->insert("avaria",[
                "nome_avaria"           => $this->dados['nome_local_grupo'],
                "cod_grupo"             => (int) $this->dados['cod_grupo'],
                "ativo"                 => $this->dados['ativo'],
            ]);
    }

    public function updateLocalGrupo(){
        $this->medoo->update("avaria",[
            "nome_avaria"           => $this->dados['nome_local_grupo'],
            "cod_grupo"             => (int) $this->dados['cod_grupo'],
            "ativo"                 => $this->dados['ativo'],
        ],[
            "cod_avaria"            => (int) $this->dados['cod_local_grupo'],
        ]);
    }
}


