<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class SafModel extends MainModel{

    private $dados;

    private $time;

    private $usuario;

    private $fillable = [
        "cod_usuario",
        "data_abertura",
        "cod_grupo",
        "cod_sistema",
        "cod_subsistema",
        "cod_avaria",
        "nivel",
        "complemento_falha",
        "cod_linha",
        "cod_trecho",
        "cod_ponto_notavel",
        "complemento_local",
        "km_inicial",
        "km_final",
        "cod_via",
        "posicao",
        "tipo_orisaf",
        "cod_tipo_funcionario",
        "matricula",
        "nome",
        "cpf",
        "contato",
        "cod_local_grupo",
        "cod_sublocal_grupo"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        if(!empty($_POST))
            $this->dados = $_POST;
            
        $this->time = date('d-m-Y H:i:s', time());

        //################# filtros e afins ##################
        $this->usuario = $this->getCodUsuario($this->dadosSaf['responsavelSaf']);
    }

    function create()
    {
        $this->dados['data_abertura'] = $this->time;
        $this->dados['cod_usuario'] = $this->dadosUsuario['cod_usuario'];

        $newSaf = $this->medoo->insert("saf",
        [ array_intersect_key($this->dados, array_flip($this->fillable)) ]
        );

        $this->alterarStatusSaf($newSaf, 4, $this->time, $this->dadosUsuario);

        //Notification
        $msgNotificacao = "Saf n. {$newSaf} aberta.<br />Por: {$this->dados['responsavelSaf']}.";
        $tipo = "Inserir";
        $selectVSaf = $this->medoo->select('v_saf', "*", ['cod_saf' => (int) $newSaf])[0];
        if($this->dadosUsuario['nivel'] == "2.3")
            $alertaAcao = "Solicita��o de Abertura de Falha n. {$newSaf} realizada com sucesso.";
        $this->notification($selectVSaf, $newSaf, $msgNotificacao, $tipo, $alertaAcao);

        return $newSaf;
    }

    function update($id)
    {
        $this->medoo->update("saf",
         array_intersect_key($this->dados, array_flip($this->fillable)),
         [
            "cod_saf" => $id
         ]);
    }

    private function validarDados($dadosSaf)
    {
        //origemSaf//
        $arrOrigem = Array();
        if (empty($dadosSaf['pontoNotavelSaf']))
            $dadosSaf['pontoNotavelSaf'] = null;

        if (empty($dadosSaf['via']))
            $dadosSaf['via'] = null;
            
        if (
            !empty($dadosSaf['codTipoFuncionario']) && !empty($dadosSaf['nomeSolicitante']) && !empty($dadosSaf['cpfSolicitante']) && !empty($dadosSaf['contatoSolicitante']) &&
            !empty($dadosSaf['trechoSaf']) && !empty($dadosSaf['linhaSaf']) &&
            !empty($dadosSaf['gSistema']) && !empty($dadosSaf['sistema']) && !empty($dadosSaf['subSistemaSaf']) && !empty($dadosSaf['avaria']) && !empty($dadosSaf['nivelSaf'])
        ){
            $arrOrigem['dados'] = [
                "cod_usuario" => (int)$this->usuario,
                "data_abertura" => $this->inverteData($this->time),
                "cod_grupo" => (int)$dadosSaf['gSistema'],
                "cod_sistema" => (int)$dadosSaf['sistema'],
                "cod_subsistema" => (int)$dadosSaf['subSistemaSaf'],
                "cod_avaria" => (int)$dadosSaf['avaria'],
                "nivel" => (String)$dadosSaf['nivelSaf'],
                "complemento_falha" => (String)$dadosSaf['complementoAvaria'],
                "cod_linha" => (int)$dadosSaf['linhaSaf'],
                "km_inicial" => (String)$dadosSaf['kmInicial'],
                "km_final" => (String)$dadosSaf['kmFinal'],
                "cod_trecho" => (int)$dadosSaf['trechoSaf'],
                "cod_ponto_notavel" => (int)$dadosSaf['pontoNotavelSaf'],
                "complemento_local" => (String)$dadosSaf['complementoLocal'],
                "cod_via" => (int)$dadosSaf['via'],
                "posicao" => (String)$dadosSaf['posicaoLocal'],
                "cod_tipo_funcionario" => (int)$dadosSaf['codTipoFuncionario'],
                "nome" => (String)$dadosSaf['nomeSolicitante'],
                "cpf" => (String)$dadosSaf['cpfSolicitante'],
                "contato" => (String)$dadosSaf['contatoSolicitante'],
                "matricula" => (int)$dadosSaf['matriculaSolicitante'],
                "cod_local_grupo" => (int)$dadosSaf['localGrupo'],
                "cod_sublocal_grupo" => (int)$dadosSaf['subLocalGrupo']
            ];
        }

        if(($dadosSaf['gSistema'] == '22' || $dadosSaf['gSistema'] == '23' || $dadosSaf['gSistema'] == '26')){
            if(!empty($dadosSaf['veiculoMrSaf']) && !empty($dadosSaf['carroMrSaf']) && !empty($dadosSaf['odometroMrSaf'])){
                $arrOrigem['materialRodante'] = [
                    "cod_veiculo" => (int) $this->dadosSaf['veiculoMrSaf'],
                    "cod_carro"   => (int) $this->dadosSaf['carroMrSaf'],
                    "odometro"    => (double) $this->dadosSaf['odometroMrSaf'],
                    "cod_prefixo" => (int) $this->dadosSaf['prefixoMrSaf'],
                    "cod_grupo"   => (int) $dadosSaf['gSistema']
                ];
            }else{
                return false;
            }
        }
        return $arrOrigem;
    }

    public function cadastro(){
        //Etapa de valida��o dos dados de cadastro da SAF

        $dadosValidados = $this->validarDados($this->dadosSaf);

        
        //Cadastro da SAF
        if(!empty($dadosValidados)) {
            //Se todos os dados forem validos, ser� criado uma nova SAF

            $insertSaf = $this->medoo->insert('saf', [
                $dadosValidados['dados']
            ]);
            
            //############## Status_SAF #####################
            $insertStatusSaf = $this->medoo->insert('status_saf',[
                "cod_saf"           => (int) $insertSaf,
                "cod_status"        => (int) 4, //aberta
                "data_status"       => $this->time,
                "usuario"           => (int) $this->usuario
            ]);

            $updateSaf = $this->medoo->update('saf',["cod_ssaf" => (int) $insertStatusSaf, "data_abertura" => $this->inverteData($this->time)], ["cod_saf" => (int) $insertSaf]);

            //############## Material Rodante ###############
            if(!empty($dadosValidados['materialRodante'])){

                $dadosValidados['materialRodante']['cod_saf'] = $insertSaf;

            }

            $insertMaterialRodante = $this->medoo->insert("material_rodante_saf",
                [
                    $dadosValidados['materialRodante']
                ]
            );

            //$updateSaf = $this->medoo->update('saf',["cod_ssaf" => (int) $insertStatusSaf], ["cod_saf" => (int) $insertSaf]);

            $msgNotificacao = "Saf n. {$insertSaf} aberta.<br />Por: {$this->dadosSaf['responsavelSaf']}.";

            $selectVSaf = $this->medoo->select('v_saf', "*", ['cod_saf' => (int) $insertSaf]);
            $selectVSaf = $selectVSaf[0];

            $tipo = "Inserir";

            if($this->dadosUsuario['nivel'] == "2.3")
                $alertaAcao = "Solicita��o de Abertura de Falha n. {$insertSaf} realizada com sucesso.";

            $this->notification($selectVSaf, $insertSaf, $msgNotificacao, $tipo, $alertaAcao);
        }else{

        }
    }

    public function alterar(){

        $dadosValidados = $this->validarDados($this->dadosSaf);

        if(!empty($dadosValidados)) {
            $updateOrigem = $this->medoo->update('saf',
                $dadosValidados['dados']
            , [
                "cod_saf" => (int)$this->dadosSaf['codigoSaf']
            ]);
            
            //############## Material Rodante ###############
            $hasMr = $this->medoo->select('material_rodante_saf', "*", ["cod_saf" => (int)$this->dadosSaf['codigoSaf']]);
            if(!empty($hasMr))
                $insertMaterialRodante = $this->medoo->update("material_rodante_saf",
                    $dadosValidados['materialRodante'],
                    [
                        "cod_saf" => (int)$this->dadosSaf['codigoSaf']
                    ]);
            else{
                $dadosValidados['materialRodante']['cod_saf'] = (int)$this->dadosSaf['codigoSaf'];
                $insertMaterialRodante = $this->medoo->insert("material_rodante_saf",
                    [
                        $dadosValidados['materialRodante']
                    ]);
            }
            //############## Status_SAF #####################
            //############## Status_SAF #####################
            //############## Status_SAF #####################
            if (!empty($this->dadosSaf['reenviar'])) {

                $this->alterarStatusSaf($this->dadosSaf['codigoSaf'],4,$this->time, $this->dadosUsuario);

                //Alerta Especifico para o Usuario
                $alertaAcao = "Solicita��o de Abertura de Falha n." . $this->dadosSaf['codigoSaf'] . " reenviada";

                //Mensagem para o alerta RealTime
                $msgNotificacao = "Saf n. {$this->dadosSaf['codigoSaf']} retornada.<br />Por:  {$this->dadosUsuario['usuario']}";

                //Tipo de a��o
                $tipo = "Inserir";
            } else {
                $tipo = "Alterar";
            }

            $selectVSaf = $this->medoo->select('v_saf', "*", ['cod_saf' => (int)$this->dadosSaf['codigoSaf']]);
            $selectVSaf = $selectVSaf[0];


            // Salvar altera��es
            $this->medoo->insert('alteracao_saf',[
                "cod_saf"           => (int) $this->dadosSaf['codigoSaf'],
                "data_alteracao"    => $this->time,
                "cod_usuario"       => (int)$this->dadosUsuario['cod_usuario']
            ]);

            $this->notification($selectVSaf, $this->dadosSaf['codigoSaf'], $msgNotificacao, $tipo, $alertaAcao);
        }else{

        }
    }

    public function get(){
        return $this->medoo->select('v_saf', '*', ["cod_saf"=>$this->dadosSaf['codigoSaf'] ]);
    }

    public function getAll(){
        return $this->medoo->select('v_saf', '*');
    }

    private function notification($selectVSaf, $insertSaf, $msgNotificacao="", $tipo, $alertaAcao = false){
        //Alerta Especifico para o Usuario
        if($alertaAcao)
            $_SESSION['alertaAcao'] = $alertaAcao;

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = $msgNotificacao;
        $_SESSION['notificacao']['nomeUsuario'] = $this->usuario['usuario'];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']    = $tipo;
        $_SESSION['notificacao']['tabela']  = "Saf";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero']          = $insertSaf;
        $_SESSION['notificacao']['dataAbertura']    = $this->parse_timestamp($selectVSaf['data_abertura']);
        $_SESSION['notificacao']['nivel']           = $selectVSaf['nivel'];

        $_SESSION['notificacao']['nomeLinha']       = $selectVSaf['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']      = $selectVSaf['descricao_trecho'];
    }
}
