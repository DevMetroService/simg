<?php
class UsuarioModel extends MainModel
{
    public $time;

    private $fillable = [
      'usuario',
      'permissao',
      'validater',
      'senha',
      'nivel',
      'nome_completo',
      'email'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->time = date('d-m-Y H:i:s', time());
    }

    public function create(){
        $newUser = $this->medoo->insert("usuario",[
            array_intersect_key($_POST, array_flip($this->fillable))
        ]);
        return $newUser;
    }

    public function update(){
        $this->medoo->update("usuario",
            array_intersect_key($_POST, array_flip($this->fillable))
        ,[
            'cod_usuario' =>(int)$_POST['cod_usuario']
        ]);
    }

    public function resetSenha($user, $newPass)
    {
        $this->medoo->update("usuario",
        [
            "senha" => $newPass
        ]
        ,[
            'cod_usuario' =>(int)$user
        ]);

        return $this->medoo->error();
    }

    public function getAll()
    {
        return $this->medoo->select("usuario", "*");
    }

    public function get($id)
    {
        return $this->medoo->select("usuario", "*", ["cod_usuario" => (int) $id])[0];
    }
}
 ?>
