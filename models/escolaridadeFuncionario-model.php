<?php 
class EscolaridadeFuncionarioModel extends MainModel
{

    private $dados;
    private $time;
    private $usuario;

    private $fillable=[
        'cod_tipo_escolaridade',
        'ef_andamento',
        'ef_curso_tecnico',
        'ef_nome_curso_tecnico',
        'cod_funcionario'
      ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->time = date('d-m-Y H:i:s', time());

    }
    
    public function create()
    {
        return $this->medoo->insert('escolaridade_funcionario',[
            array_intersect_key($_POST, array_flip($this->fillable))
        ]);
    }

    public function update($id)
    {
        $insertContatoSimples = $this->medoo->update('escolaridade_funcionario',
              array_intersect_key($_POST, array_flip($this->fillable))
          , [
              "cod_funcionario" => (int)$id
          ]);
    }

    public function getByEmployer($id)
    {
        return $this->medoo->select("escolaridade_funcionario", "*", ["cod_funcionario" => $id]);
    }
}