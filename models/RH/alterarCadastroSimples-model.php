<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 10/05/2016
 * Time: 12:49
 */

class AlterarCadastroSimplesModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosFuncionario = $_SESSION['dadosFuncionario'];

        $time = date('d-m-Y H:i:s', time());

        $updateFuncSimples = $this->medoo->update('funcionario',[
            "matricula"         => $dadosFuncionario['matricula'],
            "nome_funcionario"  => $dadosFuncionario['nome'],
            "cpf"               => $dadosFuncionario['cpfCnpj'],
            "data_nasc"         =>  $dadosFuncionario['dataNascimento']
        ],[
            "cod_funcionario"   => (int)$dadosFuncionario['codFuncionario']
        ]);

        $selectDados = $this->medoo->select('contato_funcionario','*',['cod_funcionario' => (int)$dadosFuncionario['codFuncionario']]);

        if(!empty($selectDados)){
            $alterarContatoSimples = $this->medoo->update('contato_funcionario',[
                "cf_email"          => $dadosFuncionario['emailCorp'],
                "celular"           => $dadosFuncionario['telefoneCorp'],
                "email_pessoal"     => $dadosFuncionario['emailPes'],
                "cf_fone_res"       => $dadosFuncionario['telefonePes'],
            ],[
                "cod_funcionario"   => (int)$dadosFuncionario['codFuncionario']
            ]);
        }else{
            $alterarContatoSimples = $this->medoo->insert('contato_funcionario',[
                "cf_email"          => $dadosFuncionario['emailCorp'],
                "celular"           => $dadosFuncionario['telefoneCorp'],
                "email_pessoal"     => $dadosFuncionario['emailPes'],
                "cf_fone_res"       => $dadosFuncionario['telefonePes'],
                "cod_funcionario"   => (int)$dadosFuncionario['codFuncionario']
            ]);
        }

        $selectDados = $this->medoo->select('dados_empresa_funcionario','*',['cod_funcionario' => (int)$dadosFuncionario['codFuncionario']]);

        if(!empty($selectDados)){
            $dadosCorp = $this->medoo->update('dados_empresa_funcionario',[
                'cod_cargos'                =>(int)$dadosFuncionario['funcao'],
                'cod_centro_resultado'      =>(int)$dadosFuncionario['centroResultado'],
                'cod_unidade'               =>(int)$dadosFuncionario['unidade'],
            ],[
                "cod_funcionario"           => (int)$dadosFuncionario['codFuncionario']
            ]);
        }else{
            $dadosCorp = $this->medoo->insert('dados_empresa_funcionario',[
                'cod_cargos'                =>(int)$dadosFuncionario['funcao'],
                'cod_centro_resultado'      =>(int)$dadosFuncionario['centroResultado'],
                'cod_unidade'               =>(int)$dadosFuncionario['unidade'],
                "cod_funcionario"           => (int)$dadosFuncionario['codFuncionario']
            ]);
        }

        if(!empty($dadosFuncionario['emAndamento'])){
            $andamento = 's';
        }else{
            $andamento = 'n';
        }

        if(!empty($dadosFuncionario['cursoTecnicoSimples'])){
            $hasCurso = 's';
        }else{
            $hasCurso = 'n';
        }

        $selectEscolaridade = $this->medoo->select("escolariade_funcionario", "*", ["cod_funcionario" => (int)$dadosFuncionario['codFuncionario'] ]);

        if(!empty($selectEscolaridade)) {
            $insertEscolaridade = $this->medoo->update('escolaridade_funcionario',[
                'cod_tipo_escolaridade' =>$dadosFuncionario['escolaridade'],
                'ef_andamento'          =>$andamento,
                'ef_curso_tecnico'      =>$hasCurso,
                'ef_nome_curso_tecnico' =>$dadosFuncionario['nomeCursoTecnico'],
            ],[
                "cod_funcionario"       => (int)$dadosFuncionario['codFuncionario']
            ]);
        }else{
            $insertEscolaridade = $this->medoo->insert('escolaridade_funcionario',[
                'cod_tipo_escolaridade' =>$dadosFuncionario['escolaridade'],
                'ef_andamento'          =>$andamento,
                'ef_curso_tecnico'      =>$hasCurso,
                'ef_nome_curso_tecnico' =>$dadosFuncionario['nomeCursoTecnico'],
                "cod_funcionario"       => (int)$dadosFuncionario['codFuncionario']
            ]);
        }

    }
}