<?php

class pesquisaFuncionarioModel extends MainModel{
	
	
	function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true)
	{
		$this->medoo = $medoo;
	
        $pesquisaMatricula = $_SESSION['consultaFuncionario']['pesquisaMatricula'];
		$pesquisaNome = $_SESSION['consultaFuncionario']['pesquisaNome'];
        $pesquisaCpf = $_SESSION['consultaFuncionario']['pesquisaCpf'];
        $pesquisaRg = $_SESSION['consultaFuncionario']['pesquisaRg'];
        $pesquisaPis = $_SESSION['consultaFuncionario']['pesquisaPis'];
		$pesquisaDataNascimento = $_SESSION['consultaFuncionario']['pesquisaDataNascimento'];
        $pesquisaDataAdmissao = $_SESSION['consultaFuncionario']['pesquisaDataAdmissao'];
        $pesquisaDataDemissao = $_SESSION['consultaFuncionario']['pesquisaDataDemissao'];
        $pesquisaCentroCusto = $_SESSION['consultaFuncionario']['pesquisaCentroCusto'];

        //Verifica se h� valores e acrescenta na variavel where
        if (!empty($pesquisaMatricula)) {
           	$where[] = "funcionario.matricula_funcionario = '{$pesquisaMatricula}'";
        }
        
        if (!empty($pesquisaNome)) {
        	$where[] = " funcionario.nome_funcionario = '{$pesquisaNome}'";
        }

        if (!empty($pesquisaCpf)) {
           	$where[] = " funcionario.cpf = '{$pesquisaCpf}'";
        }

        if (!empty($pesquisaRg)) {
         	$where[] = " rg_funcionario.num_rg = '{$pesquisaRg}'";
        }

        if (!empty($pesquisaPis)) {
           	$where[] = " pis.num_pis = '{$pesquisaPis}'";
        }

        if (!empty($pesquisaDataNascimento)) {
         	$where[] = " funcionario.data_nasc = '{$pesquisaDataNascimento}'";
        }

        if (!empty($pesquisaDataAdmissao)) {
           	$where[] = " `dados_empresa_funcionario.def_data_admissao` = '{$pesquisaDataAdmissao}'";
        }

        if (!empty($pesquisaDataDemissao)) {
           	$where[] = " `dados_empresa_funcionario.def_data_demissao` = '{$pesquisaDataDemissao}'";
        }

        if (!empty($pesquisaCentroCusto)) {
          	$where[] = " `gerencia.centro_custo` = '{$pesquisaCentroCusto}'";
        }
                    
        //SINTAX SQL para pesquisa no PostGres
        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
                           
                            //                             	JOIN gerencia USING (cod_gerencia)
        $sql = 'SELECT *
	           		FROM funcionario
           			JOIN pis USING (cod_funcionario)
       				JOIN rg_funcionario USING (cod_funcionario)
               		JOIN contato_funcionario USING (cod_funcionario)
            		JOIN dados_empresa_funcionario USING (cod_funcionario)';

        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
    	    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

        //matricula_funcionario, nome_funcionario, cpf, centro_custo, cf_fone_res
        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	
		if (empty($resultado))
		{
			$_SESSION['resultadoPesquisaFuncionario'] = (boolval(false));
			//unset($_SESSION['consultaFuncionario']);
		} else{
			$_SESSION['resultadoPesquisaFuncionario'] = $resultado;
			//unset($_SESSION['consultaFuncionario']);
		}
	
	
	}
}