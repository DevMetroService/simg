<?php
/**
 * 
 */
class CadastroFuncionarioModel extends MainModel
{


    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->recebeDadosCadastrais();

    }

    function recebeDadosCadastrais()
    {
        $dadosPessoais 		= $_SESSION['dadosPessoais'];
        $documentacao		= $_SESSION['documentacaoFuncionario'];
        $filiacao 			= $_SESSION['filiacaoFuncionario'];
        $dadosEmpresariais 	= $_SESSION['empresaFuncionario'];

        # ------------------------------------------------------------------------------------------------------- #
        # --------------------------------------- 	DADOS PESSOAIS 	--------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $nomeCompleto 				    = ( string )$dadosPessoais['nomeCompleto'];
        $sexoFuncionario 			    = ( string )$dadosPessoais['sexoFuncionario'];
        $cpf 						    = (string)$dadosPessoais['cpfFuncionario'];
        $estrangeiro                    = ( boolean )$dadosPessoais['estrangeiro'];
        $naturalidade  				    = ( boolean )$dadosPessoais['naturalidade'];
        $dataNascimentoFuncionario      = ( string )$dadosPessoais['dataNascimentoFuncionario'];
        $localNascimentoFuncionario     = ( string )$dadosPessoais['localNascimentoFuncionario'];
        $estadoCivilFuncionario 	    = ( string )$dadosPessoais['estadoCivilFuncionario'];
        $nacionalidade  			    = ( string )$dadosPessoais['nacionalidadeFuncionario'];
        $tipoVistoFuncionario  		    = ( string )$dadosPessoais['tipoVistoFuncionario'];
        $dataValidadeVistoFuncionario   = ( string )$dadosPessoais['dataValidadeVistoFuncionario'];
        $numeroPassaporte               = ( string )$dadosPessoais['numeroPassaporteFuncionario'];
        $dataNaturalizacao              = ( string )$dadosPessoais['dataNaturalizacaoFuncionario'];
        $cepFuncionario  			    = ( string )$dadosPessoais['cepFuncionario'];
        $logradouroFuncionario  	    = ( string )$dadosPessoais['logradouroFuncionario'];
        $numeroEnderecoFuncionario 	    = ( int )$dadosPessoais['numeroEnderecoFuncionario'];
        $complementoFuncionario  	    = ( string )$dadosPessoais['complementoFuncionario'];
        $bairroFuncionario  		    = ( string )$dadosPessoais['bairroFuncionario'];
        $municipioFuncionario  		    = ( string )$dadosPessoais['municipioFuncionario'];
        $estadoFuncionario 			    = ( string )$dadosPessoais['estadoFuncionario'];
        $telefoneResidencialFuncionario = ( string )$dadosPessoais['residencialFuncionario'];
        $telefoneCelularFuncionario     = ( string )$dadosPessoais['celularFuncionario'];
        $nomeContatoFuncionario 	    = ( string )$dadosPessoais['nomeContatoFuncionario'];
        $telefoneContatoFuncionario     = ( string )$dadosPessoais['contatoFuncionario'];
        $escolaridadeFuncionario 	    = ( int )$dadosPessoais['escolaridadeFuncionario'];
        if (!empty($dadosPessoais['emCurso'])) {
            $estaCursando = 's';
        } else {
            $estaCursando = 'n';
        }
        $cursoSuperiorFuncionario 	    = ( string )$dadosPessoais['cursoSuperiorFuncionario'];
        $semestreSuperiorFuncionario    = ( int )$dadosPessoais['semestreFuncionario'];
        $cursoTecnico 				    = ( string )$dadosPessoais['cursoTecnico'];
        $nomeCursoTecnicoFuncionario    = ( string )$dadosPessoais['nomeCursoTecnicoFuncionario'];
        $corOlhosFuncionario 		    = ( string )$dadosPessoais['corOlhosFuncionario'];
        $corCabeloFuncionario 		    = ( string )$dadosPessoais['corCabeloFuncionario'];
        $corPeleFuncionario 		    = ( string )$dadosPessoais['corPeleFuncionario'];
        $tamanhoCalcaFuncionario 	    = ( string )$dadosPessoais['tamanhoCalcaFuncionario'];
        $tamanhoCamisaFuncionario 	    = ( string )$dadosPessoais['tamanhoCamisaFuncionario'];
        $numeroCalcadoFuncionario 	    = ( int )$dadosPessoais['numeroCalcadoFuncionario'];

        # ------------------------------------------------------------------------------------------------------- #
        # ---------------------------------------- DOCUMENTACAO	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $carteiraProfissional 		= (string)$documentacao['carteiraProfissionalFuncionario'];
        $serieCarteiraProfissional 	= (string)$documentacao['serieCarteiraProfissionalFuncionario'];
        $emissaoCarteiraProfissional= (string)$documentacao['emissaoCarteiraProfissionalFuncionario'];
        $UfCarteiraProfissional 	= (string)$documentacao['ufCarteiraProfissionalFuncionario'];
        $pis 						= (string)$documentacao['pisFuncionario'];
        $rg 						= (string)$documentacao['rgFuncionario'];
        $ufRG 						= (string)$documentacao['ufRGFuncionario'];
        $orgaoEmissorRG 			= (string)$documentacao['orgaoEmissorFuncionario'];
        $dataExpedicao 				= (string)$documentacao['dataExpedicaoFuncionario'];
        $reservista 				= (string)$documentacao['reservistaFuncionario'];
        $cnh 						= (string)$documentacao['cnhFuncionario'];
        $categoriaCnh 				= (string)$documentacao['categoriaCnhFuncionario'];
        $primeiraCnh 				= (string)$documentacao['primeiraCnhFuncionario'];
        $vencimentoCnh 				= (string)$documentacao['vencimentoCnhFuncionario'];
        $tituloEleitor 				= (string)$documentacao['tituloEleitorFuncionario'];
        $zonaEleitoral 				= (string)$documentacao['zonaEleitoralFuncionario'];
        $secaoEleitoral 			= (string)$documentacao['secaoEleitoralFuncionario'];
        $municipioEleitoral 		= (string)$documentacao['municipioEleitoralFuncionario'];
        $ufEleitoral 				= (string)$documentacao['ufEleitoralFuncionario'];
        $dataEmissaoEleitoral 		= (string)$documentacao['dataEmissaoEleitoralFuncionario'];
        $documentoClasse 			= (string)$documentacao['documentoClasseFuncionario'];
        $conselhoClasse				= (string)$documentacao['conselhoClasseFuncionario'];
        $numeroClasse 				= (string)$documentacao['numeroClasseFuncionario'];
        $regiaoClasse 				= (string)$documentacao['regiaoClasseFuncionario'];
        $banco 						= (string)$documentacao['bancoFuncionario'];
        $agenciaNumero 				= (string)$documentacao['agenciaNumeroFuncionario'];
        $ccNumero 					= (string)$documentacao['ccNumeroFuncionario'];

        # ------------------------------------------------------------------------------------------------------- #
        # ---------------------------------------- FILIACAO - PARTE I ------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $nomePaiCompleto 			    = (string)$filiacao ['nomePaiCompleto'];
        $nascimentoPaiFuncionario 	    = (string)$filiacao ['nascimentoPaiFuncionario'];
        $irrfPaiFuncionario 		    = (string)$filiacao ['irrfPaiFuncionario'];
        $nomeMaeCompleto 			    = (string)$filiacao ['nomeMaeCompleto'];
        $nascimentoMaeFuncionario 	    = (string)$filiacao ['nascimentoMaeFuncionario'];
        $irrfMaeFuncionario 		    = (string)$filiacao ['irrfMaeFuncionario'];
        $nomeConjugeCompleto 		    = (string)$filiacao ['nomeConjugeCompleto'];
        $nascimentoConjugeFuncionario 	= (string)$filiacao ['nascimentoConjugeFuncionario'];
        $irrfConjugeFuncionario 		= (string)$filiacao ['irrfConjugeFuncionario'];
        $sexoConjugeFuncionario 	    = (string)$filiacao ['sexoConjugeFuncionario'];

        # ------------------------------------------------------------------------------------------------------- #
        # ---------------------------------------- DADOS EMPRESA ------------------------------------------------ #
        # ------------------------------------------------------------------------------------------------------- #

        $cargoFuncionario 			= (string)$dadosEmpresariais ['cargoFuncionario'];
        $dataAdmissaoFuncionario 	= (string)$dadosEmpresariais ['dataAdmissaoFuncionario'];
        $salario 					= (string)$dadosEmpresariais ['salarioFuncionario'];
        $categoriaSalario 			= (string)$dadosEmpresariais ['categoriaSalarioFuncionario'];
        $cargaHoraria 				= (string)$dadosEmpresariais ['cargaHorariaFuncionario'];
        $centroCusto 				= (string)$dadosEmpresariais ['centroCustoFuncionario'];
        if (!empty($dadosEmpresariais['insalubridadeFuncionario'])) {
            $insalubridade = 's';
        } else {
            $insalubridade = 'n';
        }

        if (!empty($dadosEmpresariais['periculosidadeFuncionario '])) {
            $periculosidade = 's';
        } else {
            $periculosidade = 'n';
        }
        $dataAtestadoSaudeOperacional 	= (string)$dadosEmpresariais ['dataAtestadoSaudeOperacionalFuncionario'];
        $matricula 						= (string)$dadosEmpresariais ['matriculaFuncionario'];
        $sindicato 						= (string)$dadosEmpresariais ['sindicatoFuncionario'];
        $situacaoSindical 				= (string)$dadosEmpresariais ['situacaoSindicalFuncionario'];
        $registroSindicato 				= (string)$dadosEmpresariais ['registroSindicatoFuncionario'];
        $chapaSindicato 				= (string)$dadosEmpresariais ['chapaSindicatoFuncionario'];
        $vinculoEmpresa                 = (string)$dadosEmpresariais ['vinculoEmpresaFuncionario'];
        $email 							= (string)$dadosEmpresariais ['emailFuncionario'];
        $usuario 						= (string)$dadosEmpresariais ['usuarioFuncionario'];
        $senha 							= (string)$dadosEmpresariais ['senhaFuncionario'];
        $codigoUnidade 					= (int)$dadosEmpresariais ['unidadeFuncionario'];

        # ------------------------------------------------------------------------------------------------------- #
        # -------------------------------------------- Filtros e afins ------------------------------------------ #
        # ------------------------------------------------------------------------------------------------------- #
        if($dataNaturalizacao === ''){
            $dataNaturalizacao = null;
        }
        if($tipoVistoFuncionario === ''){
            $tipoVistoFuncionario = null;
        }
        if($dataValidadeVistoFuncionario === ''){
            $dataValidadeVistoFuncionario = null;
        }
        if($numeroPassaporte === ''){
            $numeroPassaporte = null;
        }

        if($nomePaiCompleto === ''){
            $nomePaiCompleto = null;
            $irrfPaiFuncionario = null;
            $nascimentoPaiFuncionario = null;
        }
        if($nomeConjugeCompleto === ''){
            $nomeConjugeCompleto = null;
            $irrfConjugeFuncionario = null;
            $nascimentoConjugeFuncionario = null;
            $sexoConjugeFuncionario = null;
        }

        if($cnh === ''){
            $cnh=null;
            $categoriaCnh = null;
            $primeiraCnh = null;
            $vencimentoCnh = null;
        }

        $salarioSemMascara 					= explode('R$ ', $salario, 2);
        $salarioFormatado 					= $this->moeda($salarioSemMascara[1]);
        $estadoCivil 						= $this->medoo->select("estado_civil", "cod_estadocivil", ["descricao" => $estadoCivilFuncionario]);
        $ufResidencialFuncionario           = (int) $this->getUF($estadoFuncionario);
        $unidadeFederativaRG 				= (int) $this->getUF($ufRG);
        $unidadeFederativaTE 				= (int) $this->getUF($ufEleitoral);
        $unidadeFederativaCT                = (int) $this->getUF($UfCarteiraProfissional);
        $fotoID	 							= $this->medoo->select('fotostemp', "nome");
        $contadorFoto 						= count($fotoID);
        $lastIDBeneficiario 				= $fotoID[$contadorFoto - 1];
        $fotoLastID 						= $this->medoo->select('fotostemp_cod_fotostemp_seq', "last_value");
        $foto 								= $this->medoo->select("fotostemp", "nome", ["cod_fotostemp" => $fotoLastID]);

        if(!empty($estrangeiro)){
            if(!empty($naturalidade)){
                $naturalidade = "s";
                $nacionalidade = 'Brasileiro';
            }else{
                $naturalidade = "n";
            }

            $estrangeiro = "s";
        }else{
            $estrangeiro = "n";
            $nacionalidade = "Brasileiro";
        }

        # ------------------------------------------------------------------------------------------------------- #
        # -------------------------------------------- Funcionario ---------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertFuncionario = $this->medoo->insert
        ("funcionario",
            [
                "nome_funcionario" 		=> $nomeCompleto,
                "cpf" 					=> preg_replace("/\D+/", "", $cpf),
                "sexo" 					=> $sexoFuncionario,
                "cod_estadocivil"		=> $estadoCivil[0],
                "data_nasc" 			=> $this->inverteData($dataNascimentoFuncionario),
                "local_nasc" 			=> $localNascimentoFuncionario,
                "estrangeiro"           => $estrangeiro,
                "naturalidade" 			=> $naturalidade,
                "nacionalidade" 		=> $nacionalidade,
                "data_naturalizacao" 	=> $dataNaturalizacao,
                "tipo_visto" 			=> $tipoVistoFuncionario,
                "data_val_visto"        => $dataValidadeVistoFuncionario,
                "num_passaporte"        => $numeroPassaporte,
                "foto" 					=> $foto[0],
                "num_reservista" 		=> $reservista,
                "num_pis"               => $pis,
            ]
        );

        $funcionarioID = $this->medoo->select('funcionario', "cod_funcionario", ["ORDER" => "cod_funcionario cresc"]);
        $contador = count($funcionarioID);
        if ($contador == 0) {
            $lastIDFuncionario = $funcionarioID[$contador];
        }
        if ($contador > 0) {
            $lastIDFuncionario = $funcionarioID[$contador - 1];
        }

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Dependente Funcionario -------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertDependente = $this->medoo->insert
        ("dependente_funcionario",
            [
                "cod_funcionario" 		=> $lastIDFuncionario,
                "df_nome_pai" 			=> $nomePaiCompleto,
                "df_irrf_pai" 			=> $irrfPaiFuncionario,
                "df_data_nasc_pai" 		=> $this->inverteData($nascimentoPaiFuncionario),
                "df_nome_mae" 			=> $nomeMaeCompleto,
                "df_irrf_mae" 			=> $irrfMaeFuncionario,
                "df_data_nasc_mae" 		=> $this->inverteData($nascimentoMaeFuncionario),
                "df_nome_conjuge"       => $nomeConjugeCompleto,
                "df_irrf_conjuge"       => $irrfConjugeFuncionario,
                "df_datanasc_conjuge"   => $this->inverteData($nascimentoConjugeFuncionario),
                "df_sexo_conjuge"       => $sexoConjugeFuncionario,
            ]
        );

        $beneficienteID = $this->medoo->select('dependente_funcionario', "cod_dependente");
        $contadorBeneficiario = count($beneficienteID);

        if ($contadorBeneficiario > 0)
        {
            $lastIDBeneficiario = $beneficienteID[$contadorBeneficiario - 1];
        }

        for ($contador = 1; $contador < 16; $contador++)
        {
            if(! empty ($filiacao['nomeBeneficiarioFuncionario'.$contador]) )
            {
                $nomeBeneficiario				= (string) $filiacao['nomeBeneficiarioFuncionario'.$contador];
                $sexoBeneficiario 				= (string) $filiacao['sexoBeneficiarioFuncionario'.$contador];
                $parentescoBeneficiario			= (string) $filiacao['parentescoBeneficiarioFuncionario'.$contador];
                $nascimentoBeneficiario			= (string) $filiacao['nascimentoBeneficiarioFuncionario'.$contador];
                $irrfBeneficiario				= (string) $filiacao['irrfBeneficiarioFuncionario'.$contador];

                $insertBeneficiario = $this->medoo->insert
                ("beneficiario",
                    [
                        "cod_funcionario"  			=> $lastIDFuncionario,
                        "nome_beneficiario" 		=> $nomeBeneficiario,
                        "sexo_beneficiario" 		=> $sexoBeneficiario,
                        "parentesco_beneficiario"  	=> $parentescoBeneficiario,
                        "data_nasc_beneficiario" 	=> $this->inverteData($nascimentoBeneficiario),
                        "irrf_beneficiario"  		=> $irrfBeneficiario,
                    ]
                );
            }

        }

        # ------------------------------------------------------------------------------------------------------- #
        # --------------------------------- Escolaridade Funcionario -------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #
        if ( ($escolaridadeFuncionario == 6) || ($escolaridadeFuncionario ==7)){
            $cursoSuperior = "s";
        } else {
            $cursoSuperior = "n";
        }


        if(!empty ($semestreSuperiorFuncionario)){
            $insertEscolaridade = $this->medoo->insert
            ("escolaridade_funcionario",
                [
                    "cod_funcionario" 			=> $lastIDFuncionario,
                    "cod_tipo_escolaridade"  	=> $escolaridadeFuncionario,
                    "ef_semestre" 				=> $semestreSuperiorFuncionario,
                    "ef_curso_tecnico" 			=> $cursoTecnico,
                    "ef_curso_superior" 		=> $cursoSuperior,
                    "ef_andamento" 				=> $estaCursando,
                    "ef_nome_do_curso_superior" => $cursoSuperiorFuncionario,
                    "ef_nome_curso_tecnico" 	=> $nomeCursoTecnicoFuncionario
                ]
            );
        }else{
            $insertEscolaridade = $this->medoo->insert
            ("escolaridade_funcionario",
                [
                    "cod_funcionario" 			=> $lastIDFuncionario,
                    "cod_tipo_escolaridade"  	=> $escolaridadeFuncionario,
                    "ef_curso_tecnico" 			=> $cursoTecnico,
                    "ef_curso_superior" 		=> $cursoSuperior,
                    "ef_andamento" 				=> $estaCursando,
                    "ef_nome_do_curso_superior" => $cursoSuperiorFuncionario,
                    "ef_nome_curso_tecnico" 	=> $nomeCursoTecnicoFuncionario
                ]
            );
        }

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Endereco Funcionario ---------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertEndereco = $this->medoo->insert
        ("endereco_funcionario",
            [
                "cod_uf" 				=> $ufResidencialFuncionario,
                "ef_municipio" 			=> $municipioFuncionario,
                "ef_bairro" 			=> $bairroFuncionario,
                "logradouro" 			=> $logradouroFuncionario,
                "ef_cep" 				=> preg_replace("/\D+/", "", $cepFuncionario),
                "ef_numero"	            => $numeroEnderecoFuncionario,
                "ef_complemento" 		=> $complementoFuncionario,
                "cod_funcionario" 		=> $lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------ Classe Funcionario ----------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        if(!empty($documentacao['possuiDocumentoClasse'])){
            $insertClasse = $this->medoo->insert
            ("classe_funcionario",
                [
                    "num_classe" 				=> $numeroClasse,
                    "conselho_classe" 			=> $conselhoClasse,
                    "documento_classe" 			=> $documentoClasse,
                    "regiao_classe" 			=> $regiaoClasse,
                    "cod_funcionario" 			=> $lastIDFuncionario,
                ]
            );
        }


        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Dados Banc�rios Funcionario --------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #
        $insertDadoBancario = $this->medoo->insert
        ("dados_bancarios_funcionario",
            [
                "dbf_num_agencia" 			=> $agenciaNumero,
                "dbf_nomebanco" 			=> $banco,
                "dbf_num_conta_corrente" 	=> $ccNumero,
                "cod_funcionario" 			=> $lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Contato Funcionario ----------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertContato = $this->medoo->insert
        ("contato_funcionario",
            [
                "cf_email" 				=> $email,
                "cf_fone_res" 			=> $telefoneResidencialFuncionario,
                "cf_fone_recado" 		=> $telefoneContatoFuncionario,
                "cf_falar_com" 			=> $nomeContatoFuncionario,
                "cf_celular" 			=> $telefoneCelularFuncionario,
                "cod_funcionario" 		=> $lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------ Carteira Trabalho Funcionario ------------------------------------ #
        # ------------------------------------------------------------------------------------------------------- #

        $insertCarteiraTrabalho = $this->medoo->insert
        ("ctps_funcionario",
            [
                "cod_uf" 				=> $unidadeFederativaCT,
                "data_emissao_ctps"		=> $this->inverteData($emissaoCarteiraProfissional),
                "num_ctps" 				=> $carteiraProfissional,
                "num_ctps_serie"		=> $serieCarteiraProfissional,
                "cod_funcionario" 		=> $lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Rg Funcionario ---------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertRg = $this->medoo->insert
        ("rg_funcionario",
            [
                "data_emissao_rg" 	        => $this->inverteData($dataExpedicao),
                "cod_uf" 					=> (int) $unidadeFederativaRG,
                "orgao_emissor" 			=> (string)$orgaoEmissorRG,
                "num_rg" 					=> (string)$rg,
                "cod_funcionario" 			=> (int)$lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- CNH Funcionario --------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertCnh = $this->medoo->insert
        ("cnh_funcionario",
            [
                "num_cnh" 				=> $cnh,
                "data_emissao_cnh" 		=> $this->inverteData($primeiraCnh),
                "categoria_cnh" 		=> $categoriaCnh,
                "vencimento_cnh" 		=> $this->inverteData($vencimentoCnh),
                "cod_funcionario"		=> $lastIDFuncionario,
            ]
        );


        # ------------------------------------------------------------------------------------------------------- #
        # -------------------------------- T�tulo Eleitoral Funcionario ----------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertTituloEleitor = $this->medoo->insert
        ("te_funcionario",
            [
                "num_te"                => (string)$tituloEleitor,
                "data_emissao_te"       => $this->inverteData($dataEmissaoEleitoral),
                "zona" 				    => (int)$zonaEleitoral,
                "secao" 			    => (int)$secaoEleitoral,
                "municipio"             => (string)$municipioEleitoral,
                "cod_uf" 				=> (int)$unidadeFederativaTE,
                "cod_funcionario"		=> (int)$lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Dados da Empresa Funcionario -------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertDadosEmpresa = $this->medoo->insert
        ("dados_empresa_funcionario",
            [
                "cod_cargos"                    => $cargoFuncionario,
                "cod_centro_resultado"          => $centroCusto,
                "cod_unidade"                   => $codigoUnidade,
                "def_data_admissao" 			=> $this->inverteData($dataAdmissaoFuncionario),
                "def_salario" 					=> $salarioFormatado,
                "def_categoria" 				=> $categoriaSalario,
                "def_periculosidade" 			=> $periculosidade,
                "def_insalubridade" 			=> $insalubridade,
                "def_carga_horario_mensal" 		=> $cargaHoraria,
                "def_sindicato" 				=> $sindicato,
                "def_situacao_sindical_anual" 	=> $situacaoSindical,
                "def_registro" 					=> $registroSindicato,
                "data_aso" 						=> $this->inverteData($dataAtestadoSaudeOperacional) ,
                "def_chapa" 					=> $chapaSindicato,
                "def_vinculo"                   => $vinculoEmpresa,
                "cod_funcionario" 				=> $lastIDFuncionario,
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Tra�os F�sicos Funcionario ---------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertTracoFisico = $this->medoo->insert
        ("tracos_fisicos_funcionario",
            [
                "cod_funcionario"   => $lastIDFuncionario,
                "tff_cor_olhos"     => $corOlhosFuncionario,
                "tff_cabelos"       => $corCabeloFuncionario,
                "tff_pele"          => $corPeleFuncionario,
                "tff_num_calca"     => $tamanhoCalcaFuncionario,
                "tff_num_camisa"    => $tamanhoCamisaFuncionario,
                "tff_num_calcado"   => $numeroCalcadoFuncionario
            ]
        );

        # ------------------------------------------------------------------------------------------------------- #
        # ----------------------------------- Insert Usu�rio Funcionario ---------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $insertUsuario = $this->medoo->insert
        ("usuario",
            [
                "cod_funcionario" => $lastIDFuncionario,
                "matricula_funcionario" => $matricula,
                "nome" => $nomeCompleto,
                "usuario" => $usuario,
                "senha" => $this->phpass->HashPassword($senha)
            ]
        );

        #---------------------------------------------------------------------------------------------------------#
        #------------------------------------- Verifica��o de erros ----------------------------------------------#
        #---------------------------------------------------------------------------------------------------------#

        unset($_SESSION['dadosPessoais']);
        unset($_SESSION['documentacaoFuncionario']);
        unset($_SESSION['filiacaoFuncionario']);
        unset($_SESSION['empresaFuncionario']);

    }

    function moeda($get_valor) {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor;
    }

    function getUF($uf){

        $unidadeFederativa = $this->medoo->select("uf", "cod_uf", ["sigla_uf" => $uf]);
        return $unidadeFederativa[0];
    }




}