<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 04/05/2016
 * Time: 09:49
 */
class CadastroSimplesModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $insertFuncSimples = $this->medoo->insert('funcionario', [
            "matricula" => $_SESSION['dadosNovoFuncionario']['matricula'],
            "nome_funcionario" => $_SESSION['dadosNovoFuncionario']['nome'],
            "data_nasc" => $_SESSION['dadosNovoFuncionario']['dataNascimento'],
            "cpf_cnpj" => $_SESSION['dadosNovoFuncionario']['cpfCnpj'],
            "cod_tipo_funcionario" => $_SESSION['dadosNovoFuncionario']['tipoFuncionario']
        ]);

        $insertContatoSimples = $this->medoo->insert('contato_funcionario', [
            "cf_email" => $_SESSION['dadosNovoFuncionario']['emailCorp'],
            "celular" => $_SESSION['dadosNovoFuncionario']['telefoneCorp'],
            "email_pessoal" => $_SESSION['dadosNovoFuncionario']['emailPes'],
            "cf_fone_res" => $_SESSION['dadosNovoFuncionario']['telefonePes'],
            "cod_funcionario" => (int)$insertFuncSimples
        ]);

        $dadosCorp = $this->medoo->insert('dados_empresa_funcionario', [
            'cod_cargos' => $_SESSION['dadosNovoFuncionario']['funcao'],
            'cod_centro_resultado' => $_SESSION['dadosNovoFuncionario']['centroResultado'],
            'cod_unidade' => $_SESSION['dadosNovoFuncionario']['unidade'],
            'cod_funcionario' => (int)$insertFuncSimples
        ]);

        if (!empty($_SESSION['dadosNovoFuncionario']['emAndamento'])) {
            $andamento = 's';
        } else {
            $andamento = 'n';
        }

        if (!empty($_SESSION['dadosNovoFuncionario']['cursoTecnicoSimples'])) {
            $hasCurso = 's';
        } else {
            $hasCurso = 'n';
        }

        $insertEscolaridade = $this->medoo->insert('escolaridade_funcionario', [
            'cod_tipo_escolaridade' => $_SESSION['dadosNovoFuncionario']['escolaridade'],
            'ef_andamento' => $andamento,
            'ef_curso_tecnico' => $hasCurso,
            'ef_nome_curso_tecnico' => $_SESSION['dadosNovoFuncionario']['nomeCursoTecnico'],
            'cod_funcionario' => (int)$insertFuncSimples
        ]);

        if (!empty($insertContatoSimples) && !empty($insertFuncSimples) && !empty($dadosCorp) && !empty($insertEscolaridade)) {
            $_SESSION['feedback_banco']['msg'] = "Inserido com sucesso.<br />{$_SESSION['dadosNovoFuncionario']['matricula']} - {$_SESSION['dadosNovoFuncionario']['nome']}";
            $_SESSION['feedback_banco']['alert'] = ',';

            unset($_SESSION['dadosNovoFuncionario']);
        } else {
            $_SESSION['feedback_banco']['msg'] = "Houve um erro no cadastro. Contato:{$insertContatoSimples} - Funcionario:{$insertFuncSimples} - dadosCorp:{$dadosCorp} - dadosEscolaridade{$insertEscolaridade} ";
//                $_SESSION['feedback_banco'] = var_dump($_SESSION['dadosNovoFuncionario']);
            $_SESSION['feedback_banco']['alert'] = 'danger';

//                unset($_SESSION['dadosNovoFuncionario']);
            return false;
        }
    }
}