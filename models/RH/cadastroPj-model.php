<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 13/08/2015
 * Time: 11:29
 */

class CadastroPjModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosPj = $_SESSION['dadosPj'];

        $this->medoo->insert("funcionario", [
            "matricula"             => (int) $dadosPj['matriculaPj'],
            "cod_tipo_funcionario"  => (int) $dadosPj['tipoPj'],
            "nome_funcionario"      => (string) $dadosPj['nomePj'],
            "cpf"                   => (string) $dadosPj['cpfCnpj']
        ]);

       /* switch($dadosPj['tipoPj']){
            case "5":
                $this->medoo->insert("funcionario_pj", [
                    "matricula"             => (int) $dadosPj['matriculaPj'],
                    "cod_cargo"             => (int) $dadosPj['funcaoPj'],
                    "unidade"               => (int) $dadosPj['unidadePj'],
                    "cod_centro_resultado"  => (int) $dadosPj['centroResultadoPj'],
                    "nome_funcionario_pj"   => (string) $dadosPj['nomePj'],
                    "cnpj_cpf"              => (string) $dadosPj['cpfCnpj'],
                    "email_corporativo"     => (string) $dadosPj['emailCorpPj'],
                    "email_pessoal"         => (string) $dadosPj['emailPesPj'],
                    "celular_corporativo"   => (string) $dadosPj['telefoneCorpPj'],
                    "celular_pessoal"       => (string) $dadosPj['telefonePesPj'],
                ]);
                break;
            case "4":
                $this->medoo->insert("funcionario_crj", [
                    "matricula"             => (int) $dadosPj['matriculaPj'],
                    "cod_cargo"             => (int) $dadosPj['funcaoPj'],
                    "unidade"               => (int) $dadosPj['unidadePj'],
                    "cod_centro_resultado"  => (int) $dadosPj['centroResultadoPj'],
                    "nome_funcionario_crj"  => (string) $dadosPj['nomePj'],
                    "cnpj_cpf"              => (string) $dadosPj['cpfCnpj'],
                    "email_corporativo"     => (string) $dadosPj['emailCorpPj'],
                    "email_pessoal"         => (string) $dadosPj['emailPesPj'],
                    "celular_corporativo"   => (string) $dadosPj['telefoneCorpPj'],
                    "celular_pessoal"       => (string) $dadosPj['telefonePesPj'],
                ]);
                break;
            case "3":
                $this->medoo->insert("funcionario_mpe", [
                    "matricula"             => (int) $dadosPj['matriculaPj'],
                    "cod_cargo"             => (int) $dadosPj['funcaoPj'],
                    "unidade"               => (int) $dadosPj['unidadePj'],
                    "cod_centro_resultado"  => (int) $dadosPj['centroResultadoPj'],
                    "nome_funcionario_mpe"  => (string) $dadosPj['nomePj'],
                    "cnpj_cpf"              => (string) $dadosPj['cpfCnpj'],
                    "email_corporativo"     => (string) $dadosPj['emailCorpPj'],
                    "email_pessoal"         => (string) $dadosPj['emailPesPj'],
                    "celular_corporativo"   => (string) $dadosPj['telefoneCorpPj'],
                    "celular_pessoal"       => (string) $dadosPj['telefonePesPj'],
                ]);
        }*/

        unset($_SESSION['dadosPj']);

        $_SESSION['feedback_banco']['msg'] = "Inserido com sucesso.<br />{$dadosPj['matriculaPj']} - {$dadosPj['nomePj']}";
        $_SESSION['feedback_banco']['alert'] = 'success';
    }
}