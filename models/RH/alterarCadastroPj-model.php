<?php
/**
 * Created by PhpStorm.
 * User: Rycker
 * Date: 22/06/2016
 * Time: 11:26
 */

class AlterarCadastroPjModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosPj = $_SESSION['dadosPj'];

        $this->medoo->update("funcionario", [
            "matricula"             => (int) $dadosPj['matriculaPj'],
            "cod_tipo_funcionario"  => (int) $dadosPj['tipoPj'],
            "nome_funcionario"      => (string) $dadosPj['nomePj'],
            "cpf"                   => (string) $dadosPj['cpfCnpj']
        ],[
            "matricula"             => (int) $dadosPj['matriculaAntiga']
        ]);

        switch($dadosPj['tipoPj']){
            case '5':
                $this->medoo->update("funcionario_pj", [
                    "matricula"             => (int) $dadosPj['matriculaPj'],
                    "cod_cargo"             => (int) $dadosPj['funcaoPj'],
                    "unidade"               => (int) $dadosPj['unidadePj'],
                    "cod_centro_resultado"  => (int) $dadosPj['centroResultadoPj'],
                    "nome_funcionario_pj"   => (string) $dadosPj['nomePj'],
                    "cnpj_cpf"              => (string) $dadosPj['cpfCnpj'],
                    "email_corporativo"     => (string) $dadosPj['emailCorpPj'],
                    "email_pessoal"         => (string) $dadosPj['emailPesPj'],
                    "celular_corporativo"   => (string) $dadosPj['telefoneCorpPj'],
                    "celular_pessoal"       => (string) $dadosPj['telefonePesPj'],
                ],[
                    "cod_funcionario_pj"    => (int) $dadosPj['codFuncionario']
                ]);
                break;
            case "4":
                $this->medoo->update("funcionario_crj", [
                    "matricula"             => (int) $dadosPj['matriculaPj'],
                    "cod_cargo"             => (int) $dadosPj['funcaoPj'],
                    "unidade"               => (int) $dadosPj['unidadePj'],
                    "cod_centro_resultado"  => (int) $dadosPj['centroResultadoPj'],
                    "nome_funcionario_crj"  => (string) $dadosPj['nomePj'],
                    "cnpj_cpf"              => (string) $dadosPj['cpfCnpj'],
                    "email_corporativo"     => (string) $dadosPj['emailCorpPj'],
                    "email_pessoal"         => (string) $dadosPj['emailPesPj'],
                    "celular_corporativo"   => (string) $dadosPj['telefoneCorpPj'],
                    "celular_pessoal"       => (string) $dadosPj['telefonePesPj'],
                ],[
                    "cod_funcionario_crj"   => (int) $dadosPj['codFuncionario'],
                ]);
                break;
            case "3":
                $this->medoo->update("funcionario_mpe", [
                    "matricula"             => (int) $dadosPj['matriculaPj'],
                    "cod_cargo"             => (int) $dadosPj['funcaoPj'],
                    "unidade"               => (int) $dadosPj['unidadePj'],
                    "cod_centro_resultado"  => (int) $dadosPj['centroResultadoPj'],
                    "nome_funcionario_mpe"  => (string) $dadosPj['nomePj'],
                    "cnpj_cpf"              => (string) $dadosPj['cpfCnpj'],
                    "email_corporativo"     => (string) $dadosPj['emailCorpPj'],
                    "email_pessoal"         => (string) $dadosPj['emailPesPj'],
                    "celular_corporativo"   => (string) $dadosPj['telefoneCorpPj'],
                    "celular_pessoal"       => (string) $dadosPj['telefonePesPj'],
                ],[
                    "cod_funcionario_mpe"   => (int) $dadosPj['codFuncionario'],
                ]);
                break;
        }

        unset($_SESSION['dadosPj']);
    }
}