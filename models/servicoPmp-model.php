<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class ServicoPmpModel extends MainModel{

    private $dados;

    private $time;

    private $usuario;

    private $fillable = [
        "nome_servico_pmp",
        "cod_grupo"
    ];

    private $fillableSubSistema = [
        "cod_sub_sistema",
        "cod_servico_pmp"
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;
            
        $this->time = date('d-m-Y H:i:s', time());

        //################# filtros e afins ##################
        $this->usuario = $this->getCodUsuario($this->dadosSaf['responsavelSaf']);
    }

    function create()
    {
        $new = $this->medoo->insert("servico_pmp",
        [ array_intersect_key($_POST, array_flip($this->fillable)) ]
        );

        $_POST['cod_servico_pmp'] = $new;

        $new = $this->medoo->insert("servico_pmp_sub_sistema",
        [ array_intersect_key($_POST, array_flip($this->fillableSubSistema)) ]
        );

        return $new;
    }

    function update($id, $sub_sistema)
    {
        $this->medoo->update("servico_pmp",
         array_intersect_key($_POST, array_flip($this->fillable)),
         [
            "cod_servico_pmp" => $id
         ]);

         $this->medoo->insert("servico_pmp_sub_sistema",
        [ array_intersect_key($_POST, array_flip($this->fillableSubSistema)) ]
        );
    }

    public function all(){
        return $this->medoo->select("servico_pmp_sub_sistema", [ 
            "[><]sub_sistema" => ["cod_sub_sistema"],
            "[><]servico_pmp" => ["cod_servico_pmp"],
            "[><]grupo" => ["cod_grupo"],
            "[><]sistema" => ["cod_sistema"],
            "[><]subsistema" => ["cod_subsistema"]
            ], "*");
    }
}
