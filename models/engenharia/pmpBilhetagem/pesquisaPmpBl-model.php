<?php
ini_set('memory_limit', '1024M'); // or you could use 1G

class PesquisaPmpBlModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_SESSION['dadosPesquisaPmpBl'];

        //Verifica se h� valores e acrescenta na variavel where

        //#########Identifica��o
        if (!empty($dadosPesquisa['codPmp'])) {
            $where[] = "bl.cod_pmp = {$dadosPesquisa['codPmp']}";
        }

        if (!empty($dadosPesquisa['status'])) {
            $where[] = "bl.ativo = '{$dadosPesquisa['status']}'";
        }

        //#########Informa��es Gerais
        if (!empty($dadosPesquisa['sistema'])) {
            $where[] = "sb.cod_sistema = {$dadosPesquisa['sistema']}";
        }

        if (!empty($dadosPesquisa['subSistema'])) {
            $where[] = "sb.cod_subsistema = {$dadosPesquisa['subSistema']}";
        }

        if (!empty($dadosPesquisa['servicoPmp'])) {
            $where[] = "pb.cod_servico_pmp = {$dadosPesquisa['servicoPmp']}";
        }

        //###########Local
        if (!empty($dadosPesquisa['estacao'])) {
            $where[] = "cod_estacao = {$dadosPesquisa['estacao']}";
        }

        //############Per�odo
        if (!empty($dadosPesquisa['periodicidade'])) {
            $where[] = "sp.cod_tipo_periodicidade = {$dadosPesquisa['periodicidade']}";
        }

        if (!empty($dadosPesquisa['quinzena'])) {
            $where[] = "bl.quinzena = {$dadosPesquisa['quinzena']}";
        }

        //#############Falha
        if (!empty($dadosPesquisa['homemHora'])) {
            $where[] = "bl.homem_hora = {$dadosPesquisa['homemHora']}";
        }

        if (!empty($dadosPesquisa['maoObra'])) {
            $where[] = "bl.mao_obra = {$dadosPesquisa['maoObra']}";
        }

        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT
                  cod_pmp,
                  cod_estacao,
                  cod_sistema,
                  cod_subsistema,
                  cod_servico_pmp,
                  cod_tipo_periodicidade,
                  cod_procedimento,
                  quinzena,
                  turno,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  ativo
                FROM pmp bl
                  JOIN pmp_bilhetagem USING(cod_pmp)
                  JOIN servico_pmp_periodicidade sp ON bl.cod_servico_pmp_periodicidade = sp.cod_servico_pmp_periodicidade
                  JOIN servico_pmp_sub_sistema pb ON sp.cod_servico_pmp_sub_sistema = pb.cod_servico_pmp_sub_sistema
                  JOIN sub_sistema sb ON pb.cod_sub_sistema = sb.cod_sub_sistema";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        $sql = $sql . " ORDER BY cod_pmp";

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $_SESSION['resultadoPesquisaPmpBl'] = (boolval(false));
        } else {
            $_SESSION['resultadoPesquisaPmpBl'] = $resultado;
        }
    }
}