<?php
ini_set('memory_limit', '1024M'); // or you could use 1G

class PesquisaPmpTlModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_SESSION['dadosPesquisaPmpTl'];

        //Verifica se h� valores e acrescenta na variavel where

        //#########Identifica��o
        if (!empty($dadosPesquisa['codPmp'])) {
            $where[] = "tl.cod_pmp = {$dadosPesquisa['codPmp']}";
        }

        if (!empty($dadosPesquisa['status'])) {
            $where[] = "tl.ativo = '{$dadosPesquisa['status']}'";
        }

        //#########Informa��es Gerais
        if (!empty($dadosPesquisa['sistema'])) {
            $where[] = "sb.cod_sistema = {$dadosPesquisa['sistema']}";
        }

        if (!empty($dadosPesquisa['subSistema'])) {
            $where[] = "sb.cod_subsistema = {$dadosPesquisa['subSistema']}";
        }

        if (!empty($dadosPesquisa['servicoPmp'])) {
            $where[] = "pb.cod_servico_pmp = {$dadosPesquisa['servicoPmp']}";
        }

        //###########Local
        if (!empty($dadosPesquisa['local'])) {
            $where[] = "cod_local = {$dadosPesquisa['local']}";
        }

        //############Per�odo
        if (!empty($dadosPesquisa['periodicidade'])) {
            $where[] = "sp.cod_tipo_periodicidade = {$dadosPesquisa['periodicidade']}";
        }

        if (!empty($dadosPesquisa['quinzena'])) {
            $where[] = "tl.quinzena = {$dadosPesquisa['quinzena']}";
        }

        //#############Falha
        if (!empty($dadosPesquisa['homemHora'])) {
            $where[] = "tl.homem_hora = {$dadosPesquisa['homemHora']}";
        }

        if (!empty($dadosPesquisa['maoObra'])) {
            $where[] = "tl.mao_obra = {$dadosPesquisa['maoObra']}";
        }

        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT
                  cod_pmp,
                  cod_local,
                  cod_sistema,
                  cod_subsistema,
                  cod_servico_pmp,
                  cod_tipo_periodicidade,
                  cod_procedimento,
                  quinzena,
                  turno,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  ativo
                FROM pmp tl
                  JOIN pmp_telecom USING(cod_pmp)
                  JOIN servico_pmp_periodicidade sp ON tl.cod_servico_pmp_periodicidade = sp.cod_servico_pmp_periodicidade
                  JOIN servico_pmp_sub_sistema pb ON sp.cod_servico_pmp_sub_sistema = pb.cod_servico_pmp_sub_sistema
                  JOIN sub_sistema sb ON pb.cod_sub_sistema = sb.cod_sub_sistema";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        $sql = $sql . " ORDER BY cod_pmp";

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $_SESSION['resultadoPesquisaPmpTl'] = (boolval(false));
        } else {
            $_SESSION['resultadoPesquisaPmpTl'] = $resultado;
        }
    }
}