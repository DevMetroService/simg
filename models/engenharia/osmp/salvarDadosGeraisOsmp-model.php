<?php

/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 21/12/2016
 * Time: 17:28
 */
class SalvarDadosGeraisOsmpModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosOsmp = $_SESSION['dadosGeraisOsmp'];

        $time = date('d-m-Y H:i:s', time());

        $this->osResponsavel($dadosUsuario['cod_usuario'], $dadosOsmp['codigoOs'], "osmp");

        $updateOsmp = $this->medoo->update("osmp", [
            "descricao"           => $dadosOsmp['complemento']
        ], [
            "cod_osmp" => (int)$dadosOsmp['codigoOs']
        ]);
        
        if(!empty($dadosOsmp['km_inicial'])){
            $updateOsmp = $this->medoo->update("osmp_vp", [
                "km_inicial"    => (string) $dadosOsmp['km_inicial'],
                "km_final"      => (string) $dadosOsmp['km_final']
            ], [
                "cod_osmp" => (int)$dadosOsmp['codigoOs']
            ]);
        }

        unset($_SESSION['dadosGeraisOsmp']);
    }
}


