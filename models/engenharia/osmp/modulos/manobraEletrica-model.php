<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 31/07/2015
 * Time: 11:14
 */
class ManobraEletricaModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosManobra = $_SESSION['dadosManobra'];

        for ($i = 0; $i < 20; $i++) {
            if (!empty($dadosManobra['retirarManobra' . $i])) {
                $deleteManobra = $this->medoo->delete("osmp_manobra_eletrica", [
                    "AND" =>
                        [
                            "cod_osmp"       => (int)$dadosManobra['codigoOs'],
                            "pedido_manobra" => (int)$dadosManobra['pm' . $i],
                        ]
                ]);
            } else {
                if (!empty($dadosManobra['pm' . $i])) {

                    $selectMan = $this->medoo->select("osmp_manobra_eletrica", "*",
                        [
                            "AND" =>
                                [
                                    "cod_osmp"       => (int)$dadosManobra['codigoOs'],
                                    "pedido_manobra" => (int)$dadosManobra['pm' . $i]
                                ]
                        ]
                    );

                    if (empty($selectMan)) {
                        $insertManobra = $this->medoo->insert("osmp_manobra_eletrica", [
                            "cod_osmp"          => (int)$dadosManobra['codigoOs'],
                            "chave"             => (string)$dadosManobra['chaveManobra' . $i],
                            "local"             => (string)$dadosManobra['localManobra' . $i],
                            "pedido_manobra"    => (int)$dadosManobra['pm' . $i],
                            "data_desl"         => (string)$dadosManobra['dataDesligamento' . $i],
                            "hora_desl"         => (string)$dadosManobra['horaDesligamento' . $i],
                            "data_lig"          => (string)$dadosManobra['dataLigamento' . $i],
                            "hora_lig"          => (string)$dadosManobra['horaLigamento' . $i],
                            "matricula_desl"    => (int)$dadosManobra['solicitadoDesligamento' . $i],
                            "atendido_por_desl" => (string)$dadosManobra['atendidoDesligamento' . $i],
                            "matricula_lig"     => (int)$dadosManobra['solicitadoLigamento' . $i],
                            "atendido_por_lig"  => (string)$dadosManobra['atendidoLigamento' . $i],
                        ]);

                    } else {
                        $selectMan = null;
                    }
                }
            }
        }

        unset($_SESSION['dadosManobra']);
    }
}