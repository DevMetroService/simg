<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 05/08/2015
 * Time: 15:46
 */

class EncerramentoModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosEncerramento = $_SESSION['dadosEncerramento'];

        //Pega c�digo ssmp para altera��o de status
        //Pega c�digo ssmp para altera��o de status
        //Pega c�digo ssmp para altera��o de status
        $codSsmp = $this->medoo->select("osmp", "cod_ssmp", ["cod_osmp" => $dadosEncerramento['codigoOs']]);
        $codSsmp = $codSsmp[0];

        //Pega tempo atual de encerramento
        $time = date('d-m-Y H:i:s', time());

        //verifica se h� informa��es na tabela desta OSMP
        $selectEncerramento = $this->medoo->select("osmp_encerramento", "*", ["cod_osmp" => $dadosEncerramento['codigoOs']]);

        //Se, selectEncerramento for vazio, insert, do contrario, update
        if (empty($selectEncerramento)) {

            //Insere o status encerrada para OSMP independete da sua condi��o
            //Insere o status encerrada para OSMP independete da sua condi��o
            //Insere o status encerrada para OSMP independete da sua condi��o
            $insertStatusOsmp = $this->medoo->insert("status_osmp", [
                "cod_osmp"    => (int)$dadosEncerramento['codigoOs'],
                "cod_status"  => (int)11,    //Encerrado
                "data_status" => $time,
                "usuario"     => (int) $dadosUsuario['cod_usuario']
            ]);

            $updateOsmp = $this->medoo->update("osmp", [
                "cod_status_osmp" => (int)$insertStatusOsmp
            ], [
                "cod_osmp"        => (int)$dadosEncerramento['codigoOs']
            ]);

            //Realizando altera��o no status da SSMP
            if ($dadosEncerramento['descricaoTipoFechamento'] == "1") {      //Se for encerrado sem pend�ncia

                if($dadosEncerramento['liberacaoTrafego'] == 's'){
                    $this->alterarStatusSsmp($codSsmp, 18, 18, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Encerrada //Cronograma - Encerrada
                }else{
                    $this->alterarStatusSsmp($codSsmp, 18, 22, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Encerrada //Cronograma - Pendente
                }

                $insertEncerramento = $this->medoo->insert("osmp_encerramento", [
                    "cod_osmp"              => (int)$dadosEncerramento['codigoOs'],
                    "data_encerramento"     => $dadosEncerramento['dataHoraFechamento'],
                    "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                    //TODO: Alterar nome do elemento HTML
                    "cod_funcionario"       => (int) $dadosEncerramento['codFuncionario'],
                    "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego']
                ]);
            }else{
                if($dadosEncerramento['descricaoTipoFechamento'] == "3"){//Quando for n�o executado.
                    $this->alterarStatusSsmp($codSsmp, 18, 18, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Encerrada -- Cronograma � encerrado
                }else{
                    $this->alterarStatusSsmp($codSsmp, 22, null, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Pendente //Cronograma continua em Execu��o
                }


                $insertEncerramento = $this->medoo->insert("osmp_encerramento", [
                    "cod_osmp"              => (int)$dadosEncerramento['codigoOs'],
                    "data_encerramento"     => $dadosEncerramento['dataHoraFechamento'],
                    "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                    //TODO: Alterar nome do elemento HTML
                    "cod_funcionario"       => (int) $dadosEncerramento['codFuncionario'],
                    "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego'],
                    "cod_pendencia"         => (int)$dadosEncerramento['pendenciaFechamento'],
                    "descricao"             => (string)$dadosEncerramento['motivoNaoExecucao'],
                ]);
            }
        }else{
            $selectEncerramento = $selectEncerramento[0];

            //Realizando altera��o no status da SSMP
            if ($dadosEncerramento['descricaoTipoFechamento'] == "1") {      //Se for encerrado sem pend�ncia

                if(($selectEncerramento["cod_tipo_fechamento"] != $dadosEncerramento['descricaoTipoFechamento']) || ($selectEncerramento["liberacao"] != $dadosEncerramento['liberacaoTrafego'])) {
                    if ($dadosEncerramento['liberacaoTrafego'] == 's') {
                        $this->alterarStatusSsmp($codSsmp, 18, 18, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Encerrada //Cronograma - Encerrada
                    } else {
                        $this->alterarStatusSsmp($codSsmp, 18, 22, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Encerrada //Cronograma - Pendente
                    }
                }

                $insertEncerramento = $this->medoo->update("osmp_encerramento", [
                    "data_encerramento"     => $dadosEncerramento['dataHoraFechamento'],
                    "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                    //TODO: Alterar nome do elemento HTML
                    "cod_funcionario"       => (int) $dadosEncerramento['codFuncionario'],
                    "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego']
                ],[
                    "cod_osmp"              => (int)$dadosEncerramento['codigoOs']
                ]);
            }else{
                if($selectEncerramento["cod_tipo_fechamento"] != $dadosEncerramento['descricaoTipoFechamento']){
                    if($dadosEncerramento['descricaoTipoFechamento'] == "3"){//Quando for n�o executado.
                        $this->alterarStatusSsmp($codSsmp, 18, 18, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Encerrada -- Cronograma � encerrado
                    }else{
                        $this->alterarStatusSsmp($codSsmp, 22, null, $time, $dadosUsuario, $dadosEncerramento['form']); //SSMP - Pendente //Cronograma continua em Execu��o
                    }
                }

                $insertEncerramento = $this->medoo->update("osmp_encerramento", [
                    "data_encerramento"     => $dadosEncerramento['dataHoraFechamento'],
                    "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                    //TODO: Alterar nome do elemento HTML
                    "cod_funcionario"       => (int) $dadosEncerramento['codFuncionario'],
                    "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego'],
                    "cod_pendencia"         => (int)$dadosEncerramento['pendenciaFechamento'],
                    "descricao"             => (string)$dadosEncerramento['motivoNaoExecucao'],
                ],[
                    "cod_osmp"               => (int)$dadosEncerramento['codigoOs']
                ]);
            }
        }

        unset($_SESSION['dadosEncerramento']);
    }

    function alterarStatusSsmp($codSsmp, $statusSsmp, $statusCronograma, $time, $dadosUsuario, $form){
        $insertStatusSsmp = $this->medoo->insert("status_ssmp", [
            "cod_ssmp"    => (int)$codSsmp,
            "cod_status"  => (int)$statusSsmp,
            "data_status" => $this->inverteData($time),
            "usuario"     => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateSsp = $this->medoo->update("ssmp", [
            "cod_status_ssmp" => (int)$insertStatusSsmp
        ], [
            "cod_ssmp"     => (int)$codSsmp
        ]);

        /////////////////////////// Alterar Status Cronograma
        /////////////////////////// Alterar Status Cronograma
        /////////////////////////// Alterar Status Cronograma
        if(!empty($statusCronograma)) {
            $codCronograma = $this->medoo->select("ssmp", "cod_cronograma_pmp", ["cod_ssmp" => (int)$codSsmp]);
            $codCronograma = $codCronograma[0];

            $insertStatusCronograma = $this->medoo->insert('status_cronograma_pmp', [
                "cod_cronograma_pmp" => (int)$codCronograma,
                "cod_status" => (int)$statusCronograma,
                "data_status" => $this->inverteData($time),
                "usuario" => (int)$dadosUsuario['cod_usuario']
            ]);

            $updateCronograma = $this->medoo->update('cronograma_pmp', ["cod_status_cronograma_pmp" => (int)$insertStatusCronograma], ["cod_cronograma_pmp" => (int)$codCronograma]);
        }
    }
}
