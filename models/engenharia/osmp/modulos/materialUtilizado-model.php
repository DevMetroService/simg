<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/07/2015
 * Time: 09:42
 */

class MaterialUtilizadoModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosMateriais = $_SESSION['dadosMateriais'];

        if(!empty($_SESSION['dadosMaterialOs']['check']) && $_SESSION['dadosMaterialOs']['codigoOs'] == $dadosMateriais['codigoOs'])
            return;

        for($i = 0; $i<500; $i++){
            if(!empty($dadosMateriais['retirarMaterial'.$i])){
                $deleteMaterial = $this->medoo->delete("osmp_material",[
                    "AND" =>
                        [
                            "cod_osmp"       => (int) $dadosMateriais['codigoOs'],
                            "cod_material"   => (int) $dadosMateriais['codigoMaterialDelete'.$i]
                        ]
                ]);
            }else{
                if(!empty($dadosMateriais['codigoMaterial'.$i])){

                    $selectMat = $this->medoo->select("osmp_material", "*",
                        [
                            "AND" =>
                                [
                                    "cod_osmp"      => (int) $dadosMateriais['codigoOs'],
                                    "cod_material"  => (int) $dadosMateriais['codigoMaterial'.$i]
                                ]
                        ]
                    );

                    if(empty($selectMat)){
                        $insertMaterial = $this->medoo->insert("osmp_material",[
                            "cod_osmp"          => (int) $dadosMateriais['codigoOs'],
                            "cod_material"      => (int) $dadosMateriais['codigoMaterial'.$i],
                            "utilizado"         => (double) $dadosMateriais['qtdUtilizado'.$i],
                            "unidade"           => (int) $dadosMateriais['unidadeMedidaOs'.$i],
                            "estado"            => (string) $dadosMateriais['estadoMaterial'.$i],
                            "origem"            => (string) $dadosMateriais['origemMaterial'.$i],
                            "descricao_origem"  => (string) $dadosMateriais['outroOrigem'.$i]
                        ]);
                    }else{
                        $updateMaterial = $this->medoo->update("osmp_material",[
                            "utilizado"  => (int) $dadosMateriais['qtdUtilizado'.$i]
                        ],[
                            "AND" =>
                                [
                                    "cod_osmp"      => (int) $dadosMateriais['codigoOs'],
                                    "cod_material"  => (int) $dadosMateriais['codigoMaterial'.$i]
                                ]
                        ]);
                    }
                }
            }
        }

        $_SESSION['dadosCheck']['material'] = $this->medoo->select("osmp_material", "*", ["cod_osmp" => (int)$dadosMateriais['codigoOs']]);
        unset($_SESSION['dadosMateriais']);
    }
}