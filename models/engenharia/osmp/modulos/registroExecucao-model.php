<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 27/07/2015
 * Time: 13:29
 */

class RegistroExecucaoModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $registroExecucao = $_SESSION['registroExecucao'];

        $this->osResponsavel($dadosUsuario['cod_usuario'], $registroExecucao['codigoOs'], "osmp");

        $codEquipe = $this->getCodEquipe($registroExecucao['equipeExecucao']);

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
            "AND" =>[
                "cod_equipe"  => (int) $codEquipe,
                "cod_unidade" => (int) $registroExecucao['unidadeExecucao']
            ]
        ]);
        $unEquipe = $unEquipe[0];

        if(!empty($registroExecucao["cautelaExecucao"])){
            $cautela ="s";
        }else{
            $cautela ="n";
        }

        $registroExistente = $this->medoo->select("osmp_registro","cod_osmp_registro",["cod_osmp" =>(int)$registroExecucao['codigoOs'] ]);

        if(!empty($registroExistente)){
            $updateOsmpRegistro = $this->medoo->update("osmp_registro",[
                "cod_un_equipe"         => (int) $unEquipe,
                "cod_atuacao"           => (int) $registroExecucao['atuacaoExecucao'],
                "cod_veiculo"           => (int) $registroExecucao['transporteExecucao'],
                "clima"                 => (string) $registroExecucao['climaExecucao'],
                "desc_atuacao"          => (string) $registroExecucao['obsAtuacao'],
                "dados_complementar"    => (string) $registroExecucao['dadosComplementares'],
                "temperatura"           => (string) $registroExecucao['temperaturaExecucao'],
                "cautela"               => (string) $cautela,
                "km_inicial_cautela"    => (string) $registroExecucao['kmIniCautela'],
                "km_final_cautela"      => (string) $registroExecucao['kmFimCautela'],
                "restricao_velocidade"  => (string) $registroExecucao['restringirVelocidadeExecucao']
            ],[
                "cod_osmp"              => (int) $registroExecucao['codigoOs']
            ]);
        }else{
            $inserirOsmpRegistro = $this->medoo->insert("osmp_registro",[
                "cod_osmp"               => (int) $registroExecucao['codigoOs'],

                "cod_un_equipe"         => (int) $unEquipe,
                "cod_atuacao"           => (int) $registroExecucao['atuacaoExecucao'],
                "cod_veiculo"           => (int) $registroExecucao['transporteExecucao'],
                "clima"                 => (string) $registroExecucao['climaExecucao'],
                "desc_atuacao"          => (string) $registroExecucao['obsAtuacao'],
                "dados_complementar"    => (string) $registroExecucao['dadosComplementares'],
                "temperatura"           => (string) $registroExecucao['temperaturaExecucao'],
                "cautela"               => (string) $cautela,
                "km_inicial_cautela"    => (string) $registroExecucao['kmIniCautela'],
                "km_final_cautela"      => (string) $registroExecucao['kmFimCautela'],
                "restricao_velocidade"  => (string) $registroExecucao['restringirVelocidadeExecucao']
            ]);
        }

        unset($_SESSION['registroExecucao']);
    }
}