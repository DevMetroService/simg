<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/08/2015
 * Time: 15:12
 */

class TemposTotaisModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosTemposTotais = $_SESSION['dadosTemposTotais'];

        $selectTempo = $this->medoo->select("osmp_tempo", "*", ["cod_osmp" => $dadosTemposTotais['codigoOs']]);

        if(!empty($selectTempo)){
            $updateTempo = $this->medoo->update("osmp_tempo",
                [
                    "atraso_recebimento"        => (int) $dadosTemposTotais['atrasoRecebimento'],
                    "data_preparacao"           => $dadosTemposTotais['preparacaoInicio'],
                    "atraso_preparacao"         => (int) $dadosTemposTotais['atrasoIniPreparacao'],
                    "data_ace_saida_autorizada" => $dadosTemposTotais['acessoSaidaAutorizada'],
                    "data_ace_saida"            => $dadosTemposTotais['acessoSaida'],
                    "atraso_ace_saida"          => (int) $dadosTemposTotais['atrasoAcessoSaida'],
                    "data_ace_chegada"          => $dadosTemposTotais['acessoChegada'],
                    "atraso_ace_chegada"        => (int) $dadosTemposTotais['atrasoAcessoChegada'],
                    "data_ace_pronto"           => $dadosTemposTotais['acessoProntoSair'],
                    "atraso_ace_pronto"         => (int) $dadosTemposTotais['atrasoAcessoProntoSair'],
                    "data_exec_inicio"          => $dadosTemposTotais['execucaoInicio'],
                    "atraso_exec_inicio"        => (int) $dadosTemposTotais['atrasoExecInicio'],
                    "data_exec_termino"         => $dadosTemposTotais['execucaoTermino'],
                    "atraso_exec_termino"       => (int) $dadosTemposTotais['atrasoExecTermino'],
                    "data_reg_pronto"           => $dadosTemposTotais['regressoProntoSair'],
                    "atraso_reg_pronto"         => (int) $dadosTemposTotais['atrasoRegressoProntoSair'],
                    "data_reg_saida_autorizada" => $dadosTemposTotais['regressoSaidaAutorizada'],
                    "data_reg_chegada"          => $dadosTemposTotais['regressoChegada'],
                    "atraso_reg_chegada"        => (int) $dadosTemposTotais['atrasoRegressoChegada'],
                    "data_desmobilizacao"       => $dadosTemposTotais['regressoDesmobilizacao'],
                    "descricao"                 => $dadosTemposTotais['descricaoTempo'],
                ],[
                    "cod_osmp"                  => (int) $dadosTemposTotais['codigoOs']
                ]
            );
        }else{

            $this->osResponsavel($dadosUsuario['cod_usuario'], $dadosTemposTotais['codigoOs'], "osmp");

            $insertTempo = $this->medoo->insert("osmp_tempo",
                [
                    "cod_osmp"                  => (int) $dadosTemposTotais['codigoOs'],

                    "data_recebimento"          => $dadosTemposTotais['dataRecebimento'],
                    "atraso_recebimento"        => (int) $dadosTemposTotais['atrasoRecebimento'],
                    "data_preparacao"           => $dadosTemposTotais['preparacaoInicio'],
                    "atraso_preparacao"         => (int) $dadosTemposTotais['atrasoIniPreparacao'],
                    "data_ace_saida_autorizada" => $dadosTemposTotais['acessoSaidaAutorizada'],
                    "data_ace_saida"            => $dadosTemposTotais['acessoSaida'],
                    "atraso_ace_saida"          => (int) $dadosTemposTotais['atrasoAcessoSaida'],
                    "data_ace_chegada"          => $dadosTemposTotais['acessoChegada'],
                    "atraso_ace_chegada"        => (int) $dadosTemposTotais['atrasoAcessoChegada'],
                    "data_ace_pronto"           => $dadosTemposTotais['acessoProntoSair'],
                    "atraso_ace_pronto"         => (int) $dadosTemposTotais['atrasoAcessoProntoSair'],
                    "data_exec_inicio"          => $dadosTemposTotais['execucaoInicio'],
                    "atraso_exec_inicio"        => (int) $dadosTemposTotais['atrasoExecInicio'],
                    "data_exec_termino"         => $dadosTemposTotais['execucaoTermino'],
                    "atraso_exec_termino"       => (int) $dadosTemposTotais['atrasoExecTermino'],
                    "data_reg_pronto"           => $dadosTemposTotais['regressoProntoSair'],
                    "atraso_reg_pronto"         => (int) $dadosTemposTotais['atrasoRegressoProntoSair'],
                    "data_reg_saida_autorizada" => $dadosTemposTotais['regressoSaidaAutorizada'],
                    "data_reg_chegada"          => $dadosTemposTotais['regressoChegada'],
                    "atraso_reg_chegada"        => (int) $dadosTemposTotais['atrasoRegressoChegada'],
                    "data_desmobilizacao"       => $dadosTemposTotais['regressoDesmobilizacao'],
                    "descricao"                 => $dadosTemposTotais['descricaoTempo']
                ]
            );
        }
        unset($_SESSION['dadosTemposTotais']);
    }
}