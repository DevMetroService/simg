<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/08/2015
 * Time: 14:29
 */
class PesquisaOsmpModel extends MainModel
{


    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_POST;

        $sql = '';
        $join = '';

        //Verifica se h� valores e acrescenta na variavel where

        //#########Dados Gerais
        if (!empty($dadosPesquisa['numeroPesquisaOsmp'])) {
            $where[] = "o.cod_osmp = {$dadosPesquisa['numeroPesquisaOsmp']}";
        }

        if (!empty($dadosPesquisa['codigoSsmp'])) {
            $where[] = "o.cod_ssmp = {$dadosPesquisa['codigoSsmp']}";
        }

        //#########Datas
        if (!empty($dadosPesquisa['situacaoPesquisaOsmp'])) {
            $where[] = "st.cod_status = {$dadosPesquisa['situacaoPesquisaOsmp']}";
        }

        if (!empty($dadosPesquisa['dataPartirOsmp']) || !empty($dadosPesquisa['dataAteOsmp'])) {
            $dataPartir = $this->inverteData($dadosPesquisa['dataPartirOsmp']);
            $dataRetroceder = $this->inverteData($dadosPesquisa['dataAteOsmp']);

            if ($dataPartir == $dataRetroceder && !empty($dataPartir)) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "so.data_status >= '{$dataPartir}'";
                $where[] = "so.data_status <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "so.data_status >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "so.data_status <= '{$dataRetroceder}'";
                }
            }
        }

        if (!empty($dadosPesquisa['dataPartirOsmpAbertura']) || !empty($dadosPesquisa['dataAteOsmpAbertura'])) {
            $dataPartirA = $this->inverteData($dadosPesquisa['dataPartirOsmpAbertura']);
            $dataRetrocederA = $this->inverteData($dadosPesquisa['dataAteOsmpAbertura']);

            if ($dataPartirA == $dataRetrocederA && !empty($dataPartirA)) {
                $dataPartirA = $dataPartirA . " 00:00:00";
                $dataRetrocederA = $dataRetrocederA . " 23:59:59";

                $where[] = "o.data_abertura >= '{$dataPartirA}'";
                $where[] = "o.data_abertura <= '{$dataRetrocederA}'";

            } else {
                if (!empty($dataPartirA)) {
                    $dataPartirA = $dataPartirA . " 00:00:00";
                    $where[] = "o.data_abertura >= '{$dataPartirA}'";
                }

                if (!empty($dataRetrocederA)) {
                    $dataRetrocederA = $dataRetrocederA . " 23:59:59";
                    $where[] = "o.data_abertura < '{$dataRetrocederA}'";
                }
            }
        }

        if (!empty($dadosPesquisa['dataPartirOsmpEncerramento']) || !empty($dadosPesquisa['dataAteOsmpEncerramento'])) {
            $dataPartirE = $this->inverteData($dadosPesquisa['dataPartirOsmpEncerramento']);
            $dataRetrocederE = $this->inverteData($dadosPesquisa['dataAteOsmpEncerramento']);

            if ($dataPartirE == $dataRetrocederE && !empty($dataPartirE)) {
                $dataPartirE = $dataPartirE . " 00:00:00";
                $dataRetrocederE = $dataRetrocederE . " 23:59:59";

                $where[] = "oe.data_encerramento >= '{$dataPartirE}'";
                $where[] = "oe.data_encerramento <= '{$dataRetrocederE}'";

            } else {
                if (!empty($dataPartirE)) {
                    $dataPartirE = $dataPartirE . " 00:00:00";
                    $where[] = "oe.data_encerramento >= '{$dataPartirE}'";
                }

                if (!empty($dataRetrocederE)) {
                    $dataRetrocederE = $dataRetrocederE . " 23:59:59";
                    $where[] = "oe.data_encerramento < '{$dataRetrocederE}'";
                }
            }
        }

        //#########M�quinas e Equipamentos
        if (!empty($dadosPesquisa['maquinasEquipamentosPesquisaOsmp'])) {
            $where[] = "omq.cod_material = '{$dadosPesquisa['maquinasEquipamentosPesquisaOsmp']}'";
            $join .= ' LEFT JOIN osmp_maquina omq ON o.cod_osmp = omq.cod_osmp ';
        }

        if (!empty($dadosPesquisa['materiaisPesquisaOsmp']) || !empty($dadosPesquisa['estadoPesquisaOsmp']) || !empty($dadosPesquisa['origemPesquisaOsmp'])) {
            $join .= ' LEFT JOIN osmp_material oma ON o.cod_osmp = oma.cod_osmp ';
        }

        if (!empty($dadosPesquisa['materiaisPesquisaOsmp'])) {
            $where[] = "oma.cod_material = '{$dadosPesquisa['materiaisPesquisaOsmp']}'";
        }

        if (!empty($dadosPesquisa['estadoPesquisaOsmp'])) {
            $where[] = "estado = '{$dadosPesquisa['estadoPesquisaOsmp']}'";
        }

        if (!empty($dadosPesquisa['origemPesquisaOsmp'])) {
            $where[] = "origem = '{$dadosPesquisa['origemPesquisaOsmp']}'";
        }

        //#########Servi�o
        if (!empty($dadosPesquisa['sistemaPesquisaOsmp'])) {
            $where[] = "cod_sistema = {$dadosPesquisa['sistemaPesquisaOsmp']}";
        }

        if (!empty($dadosPesquisa['subSistemaPesquisaOsmp'])) {
            $where[] = "cod_subsistema = {$dadosPesquisa['subSistemaPesquisaOsmp']}";
        }

        //#########Local
        if (!empty($dadosPesquisa['linhaPesquisaOsmp'])) {
            $where[] = "linha.cod_linha = {$dadosPesquisa['linhaPesquisaOsmp']}";
        }

        ////SINTAX SQL para pesquisa no PostGres
        switch ($dadosPesquisa['grupoPesquisaOsmp']) {
            //Edifica��es
            case '21':
                $sql = "SELECT
                            'ed' AS form,
                            o.cod_osmp,
                            o.cod_ssmp,
                            nome_linha,
                            nome_local AS local,
                            o.data_abertura,
                            nome_servico_pmp,
                            st.nome_status,
                            tf.nome_fechamento
                        
                            FROM osmp o
                            JOIN osmp_ed USING (cod_osmp)
                  JOIN local lo USING (cod_local)
                  JOIN linha USING (cod_linha)
                            JOIN status_osmp so USING(cod_status_osmp)
                            JOIN status st USING(cod_status)
                            LEFT JOIN osmp_encerramento oe ON oe.cod_osmp = o.cod_osmp
                            LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                        
                            JOIN ssmp_ed s ON s.cod_ssmp = o.cod_ssmp
                  JOIN pmp_edificacao USING (cod_pmp_edificacao)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp) " . $join;

                if (!empty($dadosPesquisa['localPesquisaOsmp'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaOsmp']}";
                }
                break;
            //Via Permanente
            case '24':
                $sql = "SELECT
                            'vp' AS form,
                            o.cod_osmp,
                            o.cod_ssmp,
                            nome_linha,
                            ei.nome_estacao || ' - ' || ef.nome_estacao AS local,
                            o.data_abertura,
                            nome_servico_pmp,
                            st.nome_status,
                            tf.nome_fechamento
                        
                            FROM osmp o
                            JOIN osmp_vp USING (cod_osmp)
                  JOIN estacao ei ON osmp_vp.cod_estacao_inicial = ei.cod_estacao
                  JOIN linha USING (cod_linha)
                  JOIN estacao ef ON osmp_vp.cod_estacao_final = ef.cod_estacao
                            JOIN status_osmp so USING(cod_status_osmp)
                            JOIN status st USING(cod_status)
                            LEFT JOIN osmp_encerramento oe ON oe.cod_osmp = o.cod_osmp
                            LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                        
                            JOIN ssmp_vp s ON o.cod_ssmp = s.cod_ssmp
                  JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp) " . $join;

                if (!empty($dadosPesquisa['estacaoInicialPesquisaOsmp'])) {
                    $where[] = "osmp_vp.cod_estacao_inicial = {$dadosPesquisa['estacaoInicialPesquisaOsmp']}";
                }

                if (!empty($dadosPesquisa['estacaoFinalPesquisaOsmp'])) {
                    $where[] = "osmp_vp.cod_estacao_final = {$dadosPesquisa['estacaoFinalPesquisaOsmp']}";
                }
                break;
            //Subesta��o
            case '25':
                $sql = "SELECT
                            'su' AS form,
                            o.cod_osmp,
                            o.cod_ssmp,
                            nome_linha,
                            nome_local AS local,
                            o.data_abertura,
                            nome_servico_pmp,
                            st.nome_status,
                            tf.nome_fechamento
                        
                            FROM osmp o
                            JOIN osmp_su USING (cod_osmp)
                  JOIN local lo USING (cod_local)
                  JOIN linha USING (cod_linha)
                            JOIN status_osmp so USING(cod_status_osmp)
                            JOIN status st USING(cod_status)
                            LEFT JOIN osmp_encerramento oe ON oe.cod_osmp = o.cod_osmp
                            LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                        
                            JOIN ssmp_su v ON o.cod_ssmp = v.cod_ssmp
                  JOIN pmp_subestacao USING (cod_pmp_subestacao)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN sub_sistema USING(cod_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp) " . $join;

                if (!empty($dadosPesquisa['localPesquisaOsmp'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaOsmp']}";
                }
                break;
            //Rede A�rea
            case '20':
                $sql = "SELECT
                              'ra' AS form,
                              o.cod_osmp,
                              o.cod_ssmp,
                              nome_local AS local,
                              nome_linha,
                              o.data_abertura,
                              nome_servico_pmp,
                              st.nome_status,
                              tf.nome_fechamento
                        
                              FROM osmp o
                            JOIN osmp_ra USING (cod_osmp)
                  JOIN local lo USING(cod_local)
                  JOIN linha USING(cod_linha)
                              JOIN status_osmp so USING (cod_status_osmp)
                              JOIN status st USING (cod_status)
                              LEFT JOIN osmp_encerramento oe ON oe.cod_osmp = o.cod_osmp
                              LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                              JOIN ssmp_ra v ON o.cod_ssmp = v.cod_ssmp
                  JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp) " . $join;
                
                if (!empty($dadosPesquisa['localPesquisaOsmp'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaOsmp']}";
                }
                break;
            //Telecom
            case '27':
                $sql = "SELECT
                            'tl' AS form,
                            o.cod_osmp,
                            o.cod_ssmp,
                            nome_linha,
                            nome_local AS local,
                            o.data_abertura,
                            nome_servico_pmp,
                            st.nome_status,
                            tf.nome_fechamento
                        
                            FROM osmp o
                            JOIN osmp_te USING (cod_osmp)
                  JOIN local lo USING (cod_local)
                  JOIN linha USING (cod_linha)
                            JOIN status_osmp so USING(cod_status_osmp)
                            JOIN status st USING(cod_status)
                            LEFT JOIN osmp_encerramento oe ON oe.cod_osmp = o.cod_osmp
                            LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                        
                            JOIN ssmp_te s ON s.cod_ssmp = o.cod_ssmp
                  JOIN pmp_telecom USING (cod_pmp_telecom)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp) " . $join;

                if (!empty($dadosPesquisa['localPesquisaOsmp'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaOsmp']}";
                }
                break;
            //Bilhetagem
            case '28':
                $sql = "SELECT
                            'bl' AS form,
                            o.cod_osmp,
                            o.cod_ssmp,
                            nome_linha,
                            nome_estacao AS estacao,
                            o.data_abertura,
                            nome_servico_pmp,
                            st.nome_status,
                            tf.nome_fechamento
                        
                            FROM osmp o
                            JOIN osmp_bi USING (cod_osmp)
                  JOIN estacao lo USING (cod_estacao)
                  JOIN linha USING (cod_linha)
                            JOIN status_osmp so USING(cod_status_osmp)
                            JOIN status st USING(cod_status)
                            LEFT JOIN osmp_encerramento oe ON oe.cod_osmp = o.cod_osmp
                            LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                        
                            JOIN ssmp_bi s ON s.cod_ssmp = o.cod_ssmp
                  JOIN pmp_bilhetagem USING (cod_pmp_bilhetagem)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp) " . $join;

                if (!empty($dadosPesquisa['estacaoPesquisaOsmp'])) {
                    $where[] = "lo.cod_estacao = {$dadosPesquisa['estacaoPesquisaOsmp']}";
                }
                break;
        }

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

        //Executa o select a partir do medoo
        $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $_SESSION['resultadoPesquisaOsmp'] = (boolval(false));
        } else {
            $_SESSION['resultadoPesquisaOsmp'] = $resultado;
        }
    }
}