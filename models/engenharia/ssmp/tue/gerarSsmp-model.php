<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/11/2016
 * Time: 09:04
 */

class gerarSsmpModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $dadosTue = $_SESSION['dadosTue'];

        $codSsmp = $this->medoo->insert('ssmp', [
            "cod_usuario"           => (int)$dadosUsuario['cod_usuario'],
            "data_abertura"         => $this->inverteData($time),
        ]);

        $insertSsmpTue = $this->medoo->insert('ssmp_tue', [
            "cod_ssmp"          => (int)$codSsmp,
            "cod_servico_pmp"   => $dadosTue['servico'],
            "cod_veiculo"       => $dadosTue['veiculo'],
            "odometro"          => $dadosTue['odometro']
        ]);

        /////////////////////////// Inserir Status
        /////////////////////////// Inserir Status
        /////////////////////////// Inserir Status

        $insertStatusSsmp = $this->medoo->insert('status_ssmp', [
            "cod_ssmp"      => (int)$codSsmp,
            "cod_status"    => (int)32, //Aberta
            "data_status"   => $this->inverteData($time),
            "usuario"       => (int)$dadosUsuario['cod_usuario']
        ]);

        $updateSsmp = $this->medoo->update('ssmp', ["cod_status_ssmp" => (int)$insertStatusSsmp], ["cod_ssmp" => (int)$codSsmp]);

        unset($_SESSION['dadosTue']);
    }
}