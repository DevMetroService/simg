<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/11/2016
 * Time: 09:04
 */

class programarSsmpModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $dadosSsmp = $_SESSION['dadosSsmp'];

        $time = date('d-m-Y H:i:s', time());

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe", [
            "AND" => [
                "cod_equipe"  => 38, // MATERIAL RODANTE TUE
                "cod_unidade" => (int) $dadosSsmp['unidade']
            ]
        ]);
        $unEquipe = $unEquipe[0];

        $updateSsmp = $this->medoo->update('ssmp',
            [
                "solicitacao_acesso"    => $dadosSsmp['numeroSolicitacaoAcesso'],
                "dias_servico"          => $dadosSsmp['diasServico'],
                "data_programada"       => $dadosSsmp['dataHoraProgramada'],
                "complemento"           => $dadosSsmp['complemento'],
                "cod_un_equipe"         => $unEquipe
            ], [
                "cod_ssmp" => (int) $dadosSsmp['codigoSsmp']
            ]
        );

        /////////////////////////// Alterar Status Ssmp
        /////////////////////////// Alterar Status Ssmp
        /////////////////////////// Alterar Status Ssmp

        $insertStatusSsmp = $this->medoo->insert('status_ssmp',[
            "cod_ssmp"       => (int) $dadosSsmp['codigoSsmp'],
            "cod_status"     => (int) 19, // Programado
            "data_status"    => $this->inverteData($time),
            "usuario"        => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateSsmp = $this->medoo->update('ssmp',["cod_status_ssmp" => (int) $insertStatusSsmp], ["cod_ssmp" => (int) $dadosSsmp['codigoSsmp']]);

        unset($_SESSION['dadosSsmp']);
    }
}