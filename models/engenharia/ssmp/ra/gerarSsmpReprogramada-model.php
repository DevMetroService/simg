<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/11/2016
 * Time: 09:04
 */

class gerarSsmpReprogramadaModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $codCronograma = $_SESSION['codCronograma'];

        $dadosCronograma = $this->medoo->select('cronograma_pmp',[
            '[><]pmp_rede_aerea' => 'cod_pmp'
        ], [
            'cod_pmp_rede_aerea',
            'cod_local',
            'cod_via',
            'posicao',
            'cod_poste'
        ], ['cod_cronograma_pmp' => (int)$codCronograma]);
        $dadosCronograma = $dadosCronograma[0];

        $codSsmp = $this->medoo->insert('ssmp' ,[
            "cod_usuario"           => (int)$dadosUsuario['cod_usuario'],
            "data_abertura"         => $time,
            "cod_cronograma_pmp" => (int) $dadosCronograma['cod_cronograma_pmp']
        ]);

        $insertSsmp = $this->medoo->insert('ssmp_ra' ,[
            "cod_ssmp"              => (int)$codSsmp,
            "cod_pmp_rede_aerea"    => (int)$dadosCronograma['cod_pmp_rede_aerea'],
            "cod_local"             => (int)$dadosCronograma['estacao_final'],
            "cod_via"               => (int)$dadosCronograma['cod_via'],
            "posicao"               => $dadosCronograma['posicao'],
            "cod_poste"             => (int)$dadosCronograma['poste_final']
        ]);

        /////////////////////////// Inserir Status
        /////////////////////////// Inserir Status
        /////////////////////////// Inserir Status

        $insertStatusSsmp = $this->medoo->insert('status_ssmp',[
            "cod_ssmp"       => (int) $codSsmp,
            "cod_status"     => (int) 32, //Aberta
            "data_status"    => $time,
            "usuario"        => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateSsmp = $this->medoo->update('ssmp',["cod_status_ssmp" => (int) $insertStatusSsmp], ["cod_ssmp" => (int) $codSsmp]);

        /////////////////////////// Alterar Status Cronograma
        /////////////////////////// Alterar Status Cronograma
        /////////////////////////// Alterar Status Cronograma

        $insertStatusCronograma = $this->medoo->insert('status_cronograma_pmp',[
            "cod_cronograma_pmp" => (int) $codCronograma,
            "cod_status"            => (int) 33, //Reprogramada
            "data_status"           => $this->inverteData($time),
            "usuario"               => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateCronograma = $this->medoo->update('cronograma_pmp',["cod_status_cronograma_pmp" => (int) $insertStatusCronograma], ["cod_cronograma_pmp" => (int) $codCronograma]);


        unset($_SESSION['codCronograma']);
    }
}