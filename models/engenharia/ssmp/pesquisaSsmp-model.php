<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 02/02/2017
 * Time: 17:55
 */

class PesquisaSsmpModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_SESSION['dadosPesquisaSsmp'];

        //Verifica se h� valores e acrescenta na variavel where

        $grupo = $dadosPesquisa['grupoSistemaPesquisa'];
        $sql = '';

        //#########Dados Gerais
        if (!empty($dadosPesquisa['numeroSsmp'])) {
            $where[] = "s.cod_ssmp = {$dadosPesquisa['numeroSsmp']}";
        }

        if (!empty($dadosPesquisa['sistemaPesquisa'])) {
            $where[] = "su.cod_sistema = {$dadosPesquisa['sistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['subSistemaPesquisa'])) {
            $where[] = "su.cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['servico'])) {
            $where[] = "sp.cod_servico_pmp = {$dadosPesquisa['servico']}";
        }

        if (!empty($dadosPesquisa['periodicidade'])) {
            $where[] = "spp.cod_tipo_periodicidade = {$dadosPesquisa['periodicidade']}";
        }

        //######### Status / Per�odo
        if (!empty($dadosPesquisa['statusSsmpPesquisa'])) {
            $where[] = "st.cod_status = '{$dadosPesquisa['statusSsmpPesquisa']}'";
        }

        if (!empty($dadosPesquisa['quinzenaPesquisaSsmp'])) {
            $where[] = "c.quinzena = '{$dadosPesquisa['quinzenaPesquisaSsmp']}'";
        }

        if (!empty($dadosPesquisa['anoPesquisaSsmp'])) {
            $where[] = "c.ano = '{$dadosPesquisa['anoPesquisaSsmp']}'";
        }

        $dataPartir = $this->inverteData($dadosPesquisa['dataPartir']);
        $dataRetroceder = $this->inverteData($dadosPesquisa['dataRetroceder']);

        if ($dataPartir == $dataRetroceder && !empty($dataPartir)) {
            $dataPartir = $dataPartir . " 00:00:00";
            $dataRetroceder = $dataRetroceder . " 23:59:59";

            $where[] = "data_programada >= '{$dataPartir}'";
            $where[] = "data_programada <= '{$dataRetroceder}'";

        } else {
            if (!empty($dataPartir)) {
                $dataPartir = $dataPartir . " 00:00:00";
                $where[] = "data_programada >= '{$dataPartir}'";
            }

            if (!empty($dataRetroceder)) {
                $dataRetroceder = $dataRetroceder . " 23:59:59";
                $where[] = "data_programada < '{$dataRetroceder}'";
            }
        }

        //###########Local
        if (!empty($dadosPesquisa['linha'])) {
            $where[] = "l.cod_linha = {$dadosPesquisa['linha']}";
        }

        switch ($grupo) {
            //Edifica��es
            case '21':
                if (!empty($dadosPesquisa['localPesquisa'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisa']}";
                }
                
                $sql = "SELECT    s.cod_ssmp, 'ed' AS form,
                                  cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                                  ssmp.data_programada,
                                  st.nome_status,
                                  l.nome_linha,
                                  lo.nome_local,
                                  c.quinzena,
                                  sp.nome_servico_pmp
                                FROM ssmp_ed s
                                  JOIN ssmp USING(cod_ssmp)
                                  JOIN status_ssmp se USING(cod_status_ssmp)
                                  JOIN status st USING(cod_status)
                                  JOIN local lo USING(cod_local)
                                  JOIN linha l USING(cod_linha)
                                  JOIN pmp_edificacao pe USING(cod_pmp_edificacao)
                                  JOIN pmp USING(cod_pmp)
                                  JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema ss USING(cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp USING(cod_servico_pmp)
                                  JOIN sub_sistema su USING(cod_sub_sistema)
                                  JOIN cronograma_pmp cron USING(cod_cronograma_pmp)
                                  JOIN cronograma c USING(cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
            //Via Permanente
            case '24':
                if (!empty($dadosPesquisa['estacaoInicialPesquisa'])) {
                    $where[] = "pe.cod_estacao_inicial = {$dadosPesquisa['estacaoInicialPesquisa']}";
                }

                if (!empty($dadosPesquisa['estacaoFinalPesquisa'])) {
                    $where[] = "pe.cod_estacao_final = {$dadosPesquisa['estacaoFinalPesquisa']}";
                }

                $sql = "SELECT    s.cod_ssmp, 'vp' AS form,
                                  cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                                  data_programada,
                                  st.nome_status,
                                  l.nome_linha,
                                  ei.nome_estacao || ' - ' || ef.nome_estacao AS nome_local,
                                  c.quinzena,
                                  sp.nome_servico_pmp
                                FROM ssmp_vp s
                                  JOIN ssmp USING(cod_ssmp)
                                  JOIN status_ssmp se USING(cod_status_ssmp)
                                  JOIN status st USING(cod_status)
                                  JOIN estacao ei ON ei.cod_estacao = cod_estacao_inicial
                                  JOIN linha l USING(cod_linha)
                                  JOIN estacao ef ON ef.cod_estacao = cod_estacao_final
                                  JOIN pmp_via_permanente pe USING(cod_pmp_via_permanente)
                                  JOIN pmp USING(cod_pmp)
                                  JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema ss USING(cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp USING(cod_servico_pmp)
                                  JOIN sub_sistema su USING(cod_sub_sistema)
                                  JOIN cronograma_pmp cron USING(cod_cronograma_pmp)
                                  JOIN cronograma c USING(cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
            //Subesta��o
            case '25':
                if (!empty($dadosPesquisa['localPesquisa'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisa']}";
                }

                $sql = "SELECT    s.cod_ssmp, 'su' AS form,
                                  cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                                  data_programada,
                                  st.nome_status,
                                  l.nome_linha,
                                  lo.nome_local,
                                  c.quinzena,
                                  sp.nome_servico_pmp
                                FROM ssmp_su s
                                  JOIN ssmp USING(cod_ssmp)
                                  JOIN status_ssmp se USING(cod_status_ssmp)
                                  JOIN status st USING(cod_status)
                                  JOIN local lo USING(cod_local)
                                  JOIN linha l USING(cod_linha)
                                  JOIN pmp_subestacao pe USING(cod_pmp_subestacao)
                                  JOIN pmp USING(cod_pmp)
                                  JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema ss USING(cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp USING(cod_servico_pmp)
                                  JOIN sub_sistema su USING(cod_sub_sistema)
                                  JOIN cronograma_pmp cron USING(cod_cronograma_pmp)
                                  JOIN cronograma c USING(cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
            //Rede A�rea
            case '20':
                if (!empty($dadosPesquisa['estacaoInicialPesquisaCronograma'])) {
                    $where[] = "pe.estacao_inicial = {$dadosPesquisa['estacaoInicialPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['estacaoFinalPesquisaCronograma'])) {
                    $where[] = "pe.estacao_final = {$dadosPesquisa['estacaoFinalPesquisaCronograma']}";
                }

                $sql = "SELECT
                          s.cod_ssmp,
                          'ra' AS form,
                          cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                          data_programada,
                          st.nome_status,
                          l.nome_linha,
                          lo.nome_local AS nome_local,
                          c.quinzena,
                          sp.nome_servico_pmp
                        FROM ssmp_ra s
                          JOIN ssmp USING (cod_ssmp)
                          JOIN status_ssmp se USING (cod_status_ssmp)
                          JOIN status st USING (cod_status)
                          JOIN local lo ON s.cod_local = lo.cod_local
                          JOIN linha l USING (cod_linha)
                          JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                  JOIN pmp USING(cod_pmp)
                          JOIN servico_pmp_periodicidade spp USING (cod_servico_pmp_periodicidade)
                          JOIN servico_pmp_sub_sistema ss USING (cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING (cod_servico_pmp)
                          JOIN sub_sistema su USING (cod_sub_sistema)
                          JOIN cronograma_pmp cron USING (cod_cronograma_pmp)
                          JOIN cronograma c USING (cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
            //Telecom
            case '27':
                if (!empty($dadosPesquisa['localPesquisa'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisa']}";
                }

                $sql = "SELECT    s.cod_ssmp, 'tl' AS form,
                                  cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                                  ssmp.data_programada,
                                  st.nome_status,
                                  l.nome_linha,
                                  lo.nome_local,
                                  c.quinzena,
                                  sp.nome_servico_pmp
                                FROM ssmp_te s
                                  JOIN ssmp USING(cod_ssmp)
                                  JOIN status_ssmp se USING(cod_status_ssmp)
                                  JOIN status st USING(cod_status)
                                  JOIN local lo USING(cod_local)
                                  JOIN linha l USING(cod_linha)
                                  JOIN pmp_telecom pe USING(cod_pmp_telecom)
                                  JOIN pmp USING(cod_pmp)
                                  JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema ss USING(cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp USING(cod_servico_pmp)
                                  JOIN sub_sistema su USING(cod_sub_sistema)
                                  JOIN cronograma_pmp cron USING(cod_cronograma_pmp)
                                  JOIN cronograma c USING(cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
            //Bilhetagem
            case '28':
                if (!empty($dadosPesquisa['estacaoPesquisa'])) {
                    $where[] = "lo.cod_estacao = {$dadosPesquisa['estacaoPesquisa']}";
                }

                $sql = "SELECT    s.cod_ssmp, 'bl' AS form,
                                  cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                                  ssmp.data_programada,
                                  st.nome_status,
                                  l.nome_linha,
                                  lo.nome_estacao,
                                  c.quinzena,
                                  sp.nome_servico_pmp
                                FROM ssmp_bi s
                                  JOIN ssmp USING(cod_ssmp)
                                  JOIN status_ssmp se USING(cod_status_ssmp)
                                  JOIN status st USING(cod_status)
                                  JOIN estacao lo USING(cod_estacao)
                                  JOIN linha l USING(cod_linha)
                                  JOIN pmp_bilhetagem pe USING(cod_pmp_bilhetagem)
                                  JOIN pmp USING(cod_pmp)
                                  JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema ss USING(cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp USING(cod_servico_pmp)
                                  JOIN sub_sistema su USING(cod_sub_sistema)
                                  JOIN cronograma_pmp cron USING(cod_cronograma_pmp)
                                  JOIN cronograma c USING(cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
            //Material Rodante VLT
            case '22':
                if (!empty($dadosPesquisa['VeiculoPesquisa'])) {
                    $where[] = "cod_veiculo = {$dadosPesquisa['VeiculoPesquisa']}";
                }

                $sql = "SELECT    s.cod_ssmp, 'vlt' AS form,
                                  cron.cod_cronograma_pmp AS cod_cronograma_pmp,
                                  data_programada,
                                  st.nome_status,
                                  l.nome_linha,
                                  nome_veiculo,
                                  c.quinzena,
                                  sp.nome_servico_pmp
                                FROM ssmp_vlt s
                                  JOIN ssmp USING(cod_ssmp)
                                  JOIN status_ssmp se USING(cod_status_ssmp)
                                  JOIN status st USING(cod_status)
                                  JOIN veiculo USING(cod_veiculo)
                                  JOIN linha l USING(cod_linha)
                                  JOIN pmp_vlt pe USING(cod_pmp_vlt)
                                  JOIN pmp USING(cod_pmp)
                                  JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema ss USING(cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp USING(cod_servico_pmp)
                                  JOIN sub_sistema su USING(cod_sub_sistema)
                                  JOIN cronograma_pmp cron USING(cod_cronograma_pmp)
                                  JOIN cronograma c USING(cod_cronograma)";
                if (!empty($where)) {
                    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
                }

                //Executa o select a partir do medoo
                $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                break;
        }



        if (empty($resultado)) {
            $_SESSION['resultadoPesquisaSsmp'] = (boolval(false));
        } else {
            $_SESSION['resultadoPesquisaSsmp'] = $resultado;
        }
    }
}