<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/11/2016
 * Time: 09:04
 */

class salvarSsmpModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $dadosSsmp = $_SESSION['dadosSsmp'];

        switch ($dadosSsmp['form']){
            case 'ed':
                $codEquipe = 12;
                break;
            case 'vp':
                $codEquipe = 32;
                break;
            case 'su':
                $codEquipe = 33;
                break;
            case 'ra':
                $codEquipe = 34;
                break;
            case 'vlt':
                $codEquipe = 37;
                break;
            case 'tue':
                $codEquipe = 38;
                break;
        }

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe", [
            "AND" => [
                "cod_equipe"  => $codEquipe,
                "cod_unidade" => (int) $dadosSsmp['unidade']
            ]
        ]);
        $unEquipe = $unEquipe[0];

        $updateSsmp = $this->medoo->update('ssmp',
            [
                "solicitacao_acesso"    => $dadosSsmp['numeroSolicitacaoAcesso'],
                "dias_servico"          => $dadosSsmp['diasServico'],
                "data_programada"       => $dadosSsmp['dataHoraProgramada'],
                "complemento"           => $dadosSsmp['complemento'],
                "cod_un_equipe"         => $unEquipe
            ], [
                "cod_ssmp" => (int) $dadosSsmp['codigoSsmp']
            ]
        );

        unset($_SESSION['dadosSsmp']);
    }
}