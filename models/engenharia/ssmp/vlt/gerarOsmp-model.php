<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/11/2016
 * Time: 09:04
 */

class gerarOsmpModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $codSsmp = $_SESSION['dadosSsmp']['codigoSsmp'];

        $dadosSsmp = $this->medoo->select('ssmp_vlt',['[><]ssmp' => 'cod_ssmp'],[
            'cod_ssmp',
            'cod_ssmp_vlt',
            'complemento',
            'cod_veiculo'
        ], ['cod_ssmp' => (int) $codSsmp]);
        $dadosSsmp = $dadosSsmp[0];

        $insertOsmp = $this->medoo->insert('osmp' ,[
            "cod_ssmp"         => $dadosSsmp['cod_ssmp'],
            "data_abertura"    => $this->inverteData($time),
            "descricao"        => $dadosSsmp['complemento']
        ]);

        $insertOsmpVlt = $this->medoo->insert('osmp_vlt' ,[
            "cod_osmp"         => $insertOsmp,
            "cod_veiculo"      => $dadosSsmp['cod_veiculo']
        ]);

        /////////////////////////// Inserir Status Osmp
        /////////////////////////// Inserir Status Osmp
        /////////////////////////// Inserir Status Osmp

        $insertStatusOsmp = $this->medoo->insert('status_osmp',[
            "cod_osmp"    => (int) $insertOsmp,
            "cod_status"  => (int) 10, //Execu��o
            "data_status" => $this->inverteData($time),
            "usuario"     => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateOsmp = $this->medoo->update('osmp',["cod_status_osmp" => (int) $insertStatusOsmp], ["cod_osmp" => (int) $insertOsmp]);

        /////////////////////////// Alterar Status Ssmp
        /////////////////////////// Alterar Status Ssmp
        /////////////////////////// Alterar Status Ssmp

        $insertStatusSsmp = $this->medoo->insert('status_ssmp',[
            "cod_ssmp"       => (int) $dadosSsmp['cod_ssmp'],
            "cod_status"     => (int) 21, // Autorizada
            "data_status"    => $this->inverteData($time),
            "usuario"        => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateSsmp = $this->medoo->update('ssmp',["cod_status_ssmp" => (int) $insertStatusSsmp], ["cod_ssmp" => (int) $codSsmp]);

        /////////////////////////// Alterar Status Cronograma
        /////////////////////////// Alterar Status Cronograma
        /////////////////////////// Alterar Status Cronograma

        $codCronograma = $this->medoo->select("cronograma_ssmp", "cod_cronograma_pmp", [ "cod_ssmp" => $dadosSsmp['cod_ssmp'] ]);
        $codCronograma = $codCronograma[0];

        $insertStatusCronograma = $this->medoo->insert('status_cronograma_pmp',[
            "cod_cronograma_pmp" => (int) $codCronograma,
            "cod_status"            => (int) 34, //Execu��o
            "data_status"           => $this->inverteData($time),
            "usuario"               => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateCronograma = $this->medoo->update('cronograma_pmp',["cod_status_cronograma_pmp" => (int) $insertStatusCronograma], ["cod_cronograma_pmp" => (int) $codCronograma]);

        unset($_SESSION['dadosSsmp']);
    }
}