<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/11/2016
 * Time: 09:04
 */

class gerarSsmpModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $ListaDeCronogramas = $_SESSION['codCronograma'];

        foreach ($ListaDeCronogramas as $codCronograma) {
            $dadosCronograma = $this->medoo->select('cronograma_pmp', [
                '[><]pmp_vlt' => 'cod_pmp'
            ], [

                'cod_pmp_vlt',
                'cod_veiculo'

            ], ['cod_cronograma_pmp' => (int)$codCronograma]);
            $dadosCronograma = $dadosCronograma[0];

            $codSsmp = $this->medoo->insert('ssmp', [
                "cod_usuario"           => (int)$dadosUsuario['cod_usuario'],
                "data_abertura"         => $this->inverteData($time),
            ]);

            $insertSsmp = $this->medoo->insert('ssmp_vlt', [
                "cod_ssmp"       => (int)$codSsmp,
                "cod_pmp_vlt"    => $dadosCronograma['cod_pmp_vlt'],
                "cod_veiculo"    => $dadosCronograma['cod_veiculo']
            ]);

            /////////////////////////// Inserir Status
            /////////////////////////// Inserir Status
            /////////////////////////// Inserir Status

            $insertStatusSsmp = $this->medoo->insert('status_ssmp', [
                "cod_ssmp"      => (int)$codSsmp,
                "cod_status"    => (int)32, //Aberta
                "data_status"   => $this->inverteData($time),
                "usuario"       => (int)$dadosUsuario['cod_usuario']
            ]);

            $updateSsmp = $this->medoo->update('ssmp', ["cod_status_ssmp" => (int)$insertStatusSsmp], ["cod_ssmp" => (int)$codSsmp]);

            /////////////////////////// Alterar Status Cronograma
            /////////////////////////// Alterar Status Cronograma
            /////////////////////////// Alterar Status Cronograma

            $insertStatusCronograma = $this->medoo->insert('status_cronograma_pmp', [
                "cod_cronograma_pmp"    => (int)$codCronograma,
                "cod_status"            => (int)21, //Autorizada
                "data_status"           => $this->inverteData($time),
                "usuario"               => (int)$dadosUsuario['cod_usuario']
            ]);

            $updateCronograma = $this->medoo->update('cronograma_pmp', ["cod_status_cronograma_pmp" => (int)$insertStatusCronograma], ["cod_cronograma_pmp" => (int)$codCronograma]);

            /////////////////////////// Inserir Rela��o Cronograma Ssmp
            /////////////////////////// Inserir Rela��o Cronograma Ssmp
            /////////////////////////// Inserir Rela��o Cronograma Ssmp

            $insertCronSsmp = $this->medoo->insert('cronograma_ssmp', [
                "cod_ssmp"           => (int)$codSsmp,
                "cod_cronograma_pmp" => (int)$codCronograma
            ]);
        }

        unset($_SESSION['codCronograma']);
    }
}