<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 06/02/2017
 * Time: 10:07
 */

class PesquisaCronogramaModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_SESSION['dadosPesquisaCronograma'];

        //Verifica se h� valores e acrescenta na variavel where

        $grupo = $dadosPesquisa['grupoPesquisaCronograma'];
        $sql = "";

        //######### Dados Gerais
        if (!empty($dadosPesquisa['sistemaPesquisaCronograma'])) {
            $where[] = "ss.cod_sistema = {$dadosPesquisa['sistemaPesquisaCronograma']}";
        }

        if (!empty($dadosPesquisa['subsistemaPesquisa'])) {
            $where[] = "ss.cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['servicoPesquisaCronograma'])) {
            $where[] = "sp.cod_servico_pmp = {$dadosPesquisa['servicoPesquisaCronograma']}";
        }

        if (!empty($dadosPesquisa['periodicidadePesquisaCronograma'])) {
            $where[] = "spp.cod_tipo_periodicidade = {$dadosPesquisa['periodicidadePesquisaCronograma']}";
        }

        //######### Status / Per�odo
        if (!empty($dadosPesquisa['statusPesquisaCronograma'])) {
            $where[] = "st.cod_status = '{$dadosPesquisa['statusPesquisaCronograma']}'";
        }

        if (!empty($dadosPesquisa['quinzenaPesquisaCronograma'])) {
            $where[] = "c.quinzena = '{$dadosPesquisa['quinzenaPesquisaCronograma']}'";
        }

        if (!empty($dadosPesquisa['mesPesquisaCronograma'])) {
            $where[] = "c.mes = '{$dadosPesquisa['mesPesquisaCronograma']}'";
        }

        if (!empty($dadosPesquisa['anoPesquisaCronograma'])) {
            $where[] = "c.ano = '{$dadosPesquisa['anoPesquisaCronograma']}'";
        }

        //###########Local
        if (!empty($dadosPesquisa['linhaPesquisaCronograma'])) {
            $where[] = "l.cod_linha = {$dadosPesquisa['linhaPesquisaCronograma']}";
        }

        if (!empty($dadosPesquisa['numeroPesquisaCronograma'])) {
            $where[] = "cp.cod_cronograma_pmp = {$dadosPesquisa['numeroPesquisaCronograma']}";
        }

        switch ($grupo) {
            //Edifica��es
            case '21':
                if (!empty($dadosPesquisa['localPesquisaCronograma'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaCronograma']}";
                }
                $sql = "SELECT 'Edifica��o'         AS nome_grupo,
                          cp.cod_cronograma_pmp     AS cod_cronograma,
                          l.nome_linha              AS linha,
                          sp.nome_servico_pmp       AS servico,
                          c.quinzena                AS quinzena,
                          st.nome_status            AS status,
                          lo.nome_local             AS local
                          
                        FROM cronograma c
                          JOIN cronograma_pmp cp USING (cod_cronograma)
                          JOIN status_cronograma_pmp scpe USING(cod_status_cronograma_pmp)
                          JOIN status st USING(cod_status)
                          JOIN pmp USING(cod_pmp)
                          JOIN pmp_edificacao pe USING(cod_pmp)
                          JOIN local lo USING(cod_local)
                          JOIN linha l USING(cod_linha)
                          JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                          JOIN servico_pmp_sub_sistema spss USING(cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING(cod_servico_pmp)
                          JOIN sub_sistema ss USING(cod_sub_sistema)";
                break;
            //Via Permanente
            case '24':
                if (!empty($dadosPesquisa['estacaoInicialPesquisaCronograma'])) {
                    $where[] = "pe.cod_estacao_inicial = {$dadosPesquisa['estacaoInicialPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['estacaoFinalPesquisaCronograma'])) {
                    $where[] = "pe.cod_estacao_final = {$dadosPesquisa['estacaoFinalPesquisaCronograma']}";
                }

                $sql = "SELECT 'Via Permanente'     AS nome_grupo,
                          cp.cod_cronograma_pmp     AS cod_cronograma,
                          l.nome_linha              AS linha,
                          sp.nome_servico_pmp       AS servico,
                          c.quinzena                AS quinzena,
                          st.nome_status            AS status,
                          ei.nome_estacao || ' - ' || ef.nome_estacao AS local
                        FROM cronograma c
                          JOIN cronograma_pmp cp USING (cod_cronograma)
                          JOIN status_cronograma_pmp scpe USING(cod_status_cronograma_pmp)
                          JOIN status st USING(cod_status)
                          JOIN pmp p USING(cod_pmp)
                          JOIN pmp_via_permanente pe USING(cod_pmp)
                          JOIN estacao ei ON ei.cod_estacao = pe.cod_estacao_inicial
                          JOIN linha l USING(cod_linha)
                          JOIN estacao ef ON ef.cod_estacao = pe.cod_estacao_final
                          JOIN servico_pmp_periodicidade spp ON p.cod_servico_pmp_periodicidade = spp.cod_servico_pmp_periodicidade
                          JOIN servico_pmp_sub_sistema spss USING(cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING(cod_servico_pmp)
                          JOIN sub_sistema ss USING(cod_sub_sistema)";
                break;
            //Subesta��o
            case '25':
                if (!empty($dadosPesquisa['localPesquisaCronograma'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaCronograma']}";
                }

                $sql = "SELECT 'Subesta��o' AS nome_grupo,
                          cp.cod_cronograma_pmp     AS cod_cronograma,
                          l.nome_linha              AS linha,
                          sp.nome_servico_pmp       AS servico,
                          c.quinzena                AS quinzena,
                          st.nome_status            AS status,
                          lo.nome_local             AS local
                          
                        FROM cronograma c
                          JOIN cronograma_pmp cp USING (cod_cronograma)
                          JOIN status_cronograma_pmp scpe USING(cod_status_cronograma_pmp)
                          JOIN status st USING(cod_status)
                          JOIN pmp p USING(cod_pmp)
                          JOIN pmp_subestacao pe USING(cod_pmp)
                          JOIN local lo USING(cod_local)
                          JOIN linha l USING(cod_linha)
                          JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                          JOIN servico_pmp_sub_sistema spss USING(cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING(cod_servico_pmp)
                          JOIN sub_sistema ss USING(cod_sub_sistema)";
                break;
            //Rede A�rea
            case '20':
                if (!empty($dadosPesquisa['localPesquisaCronograma'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaCronograma']}";
                }

                $sql = "SELECT 'Rede Aerea' AS nome_grupo,
                               cp.cod_cronograma_pmp  AS cod_cronograma,
                               l.nome_linha              AS linha,
                               sp.nome_servico_pmp       AS servico,
                               c.quinzena                AS quinzena,
                               st.nome_status            AS status,
                               lo.nome_local             AS local
                        FROM cronograma c
                          JOIN cronograma_pmp cp USING (cod_cronograma)
                          JOIN status_cronograma_pmp scpe USING(cod_status_cronograma_pmp)
                          JOIN status st USING(cod_status)
                          JOIN pmp p USING(cod_pmp)
                          JOIN pmp_rede_aerea pe USING(cod_pmp)
                          JOIN local lo ON pe.cod_local = lo.cod_local
                          JOIN linha l USING(cod_linha)
                          JOIN servico_pmp_periodicidade spp ON p.cod_servico_pmp_periodicidade = spp.cod_servico_pmp_periodicidade
                          JOIN servico_pmp_sub_sistema spss USING(cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING(cod_servico_pmp)
                          JOIN sub_sistema ss USING(cod_sub_sistema)";
                break;
            //Telecom
            case '27':
                if (!empty($dadosPesquisa['localPesquisaCronograma'])) {
                    $where[] = "lo.cod_local = {$dadosPesquisa['localPesquisaCronograma']}";
                }
                $sql = "SELECT 'Telecom'            AS nome_grupo,
                          cp.cod_cronograma_pmp     AS cod_cronograma,
                          l.nome_linha              AS linha,
                          sp.nome_servico_pmp       AS servico,
                          c.quinzena                AS quinzena,
                          st.nome_status            AS status,
                          lo.nome_local             AS local
                          
                        FROM cronograma c
                          JOIN cronograma_pmp cp USING (cod_cronograma)
                          JOIN status_cronograma_pmp scpe USING(cod_status_cronograma_pmp)
                          JOIN status st USING(cod_status)
                          JOIN pmp USING(cod_pmp)
                          JOIN pmp_telecom pe USING(cod_pmp)
                          JOIN local lo USING(cod_local)
                          JOIN linha l USING(cod_linha)
                          JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                          JOIN servico_pmp_sub_sistema spss USING(cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING(cod_servico_pmp)
                          JOIN sub_sistema ss USING(cod_sub_sistema)";
                break;
            //Bilhetagem
            case '28':
                if (!empty($dadosPesquisa['estacaoInicialPesquisaCronograma'])) {
                    $where[] = "lo.cod_estacao = {$dadosPesquisa['estacaoInicialPesquisaCronograma']}";
                }
                $sql = "SELECT 'Bilhetagem'         AS nome_grupo,
                          cp.cod_cronograma_pmp     AS cod_cronograma,
                          l.nome_linha              AS linha,
                          sp.nome_servico_pmp       AS servico,
                          c.quinzena                AS quinzena,
                          st.nome_status            AS status,
                          lo.nome_estacao             AS local
                          
                        FROM cronograma c
                          JOIN cronograma_pmp cp USING (cod_cronograma)
                          JOIN status_cronograma_pmp scpe USING(cod_status_cronograma_pmp)
                          JOIN status st USING(cod_status)
                          JOIN pmp USING(cod_pmp)
                          JOIN pmp_bilhetagem pe USING(cod_pmp)
                          JOIN estacao lo USING(cod_estacao)
                          JOIN linha l USING(cod_linha)
                          JOIN servico_pmp_periodicidade spp USING(cod_servico_pmp_periodicidade)
                          JOIN servico_pmp_sub_sistema spss USING(cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp sp USING(cod_servico_pmp)
                          JOIN sub_sistema ss USING(cod_sub_sistema)";
                break;
        }
        
        if($sql == ""){
            $_SESSION['resultadoPesquisaCronograma'] = (boolval(false));
        }else {
            if (!empty($where)) {
                $sql = $sql . ' WHERE ' . implode(' AND ', $where);
            }


            print_r($sql);
            //Executa o select a partir do medoo
            $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            if (empty($resultado)) {
                $_SESSION['resultadoPesquisaCronograma'] = (boolval(false));
            } else {
                $_SESSION['resultadoPesquisaCronograma'] = $resultado;
            }
        }
    }
}