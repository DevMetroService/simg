<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 06/10/2016
 * Time: 14:37
 */

class CadastroProcedimentoModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosProcedimento = $_SESSION['procedimento'];

        for ($i = 1; $i < $dadosProcedimento['counter']; $i++) {
            $servicoPmpSubSistema = $this->medoo->select('servico_pmp_sub_sistema', ['[><]sub_sistema' => 'cod_sub_sistema'], 'cod_servico_pmp_sub_sistema',
                [
                    "AND" => [
                        'sub_sistema.cod_sistema'       => $dadosProcedimento['sistema' . $i],
                        'sub_sistema.cod_subsistema'    => $dadosProcedimento['subSistema' . $i],
                        'cod_servico_pmp'               => $dadosProcedimento['servicoPmp' . $i]
                    ]
                ]);
            $servicoPmpSubSistema = $servicoPmpSubSistema[0];
            
            $insertLinhaPmp = $this->medoo->insert('servico_pmp_periodicidade', [
                "cod_servico_pmp_sub_sistema"   => $servicoPmpSubSistema,
                "cod_tipo_periodicidade"        => (int)$dadosProcedimento['periodicidade' . $i],
                "cod_procedimento"              => (int)$dadosProcedimento['procedimento' . $i],
            ]);
        }

        for ($i = 1; $i < $dadosProcedimento['counterExec']; $i++) {
            if(!empty($dadosProcedimento["removeRow{$i}"])){
                $flagPmp = $this->medoo->select('pmp', 'cod_servico_pmp_periodicidade', ['cod_servico_pmp_periodicidade' => (int)$dadosProcedimento["codServPer{$i}"]]);

                if(!empty($flagPmp)) {
                    $_SESSION['erroProcedimento'][] = "O Servi�o/Procedimento cod {$dadosProcedimento["codServPer{$i}"]} n�o poder� ser excluido pois j� est� interligado a algum item da pmp.";
                }else {
                    $deleteLinhaPmp = $this->medoo->delete('servico_pmp_periodicidade',
                        [
                            "cod_servico_pmp_periodicidade" => (int)$dadosProcedimento["codServPer{$i}"]
                        ]
                    );
                }
            }
        }

        unset($_SESSION['procedimento']);
    }
}