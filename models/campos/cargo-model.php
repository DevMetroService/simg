<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/07/2015
 * Time: 14:22
 */

class CargoModel extends MainModel
{
    public $dados;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->dados = $_POST;

    }

    function insert()
    {
        $this->medoo->insert("cargos_funcionarios",[

            "descricao"=> $this->dados['descricao'],

        ]);
        }

    function update()
    {
        $this->medoo->update("cargos_funcionarios", [

            "descricao" => $this->dados['descricao'],

        ], [
            "cod_cargos" => (int)$this->dados['cod_cargos'],
        ]);
    }
}

