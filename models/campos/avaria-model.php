<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/07/2015
 * Time: 14:22
 */

class AvariaModel extends MainModel
{
    public $dadosAvaria;
    public $time;

    private $fillable = 
    [
        'nome_avaria',
        'sugestao_nivel',
        'cod_grupo',
        'ativo'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dados = $_POST;
        $this->dadosAvaria = $_POST;

        $this->time = date('d-m-Y H:i:s', time());

        if(empty($dados['cod_avaria'])){
            $this->medoo->insert("avaria",[
                "nome_avaria"           => $dados['nome_avaria'],
                "sugestao_nivel"        => $dados['sugestao_nivel'],
                "cod_grupo"             => (int) $dados['cod_grupo'],
                "ativo"                 => $dados['ativo'],
            ]);

        }else{
            $this->medoo->update("avaria",[
                "nome_avaria"           => $dados['nome_avaria'],
                "sugestao_nivel"        => $dados['sugestao_nivel'],
                "cod_grupo"             => (int) $dados['cod_grupo'],
                "ativo"                 => $dados['ativo'],
            ],[
                "cod_avaria"            => (int) $dados['cod_avaria'],
            ]);
        }

    }

    public function getAvaria($id)
    {
        return $this->medoo->select('avaria', '*', ['cod_avaria' => $id])[0];
    }
}

