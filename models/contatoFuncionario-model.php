<?php 
class ContatoFuncionarioModel extends MainModel
{
    private $time;
    private $usuario;

    private $fillable=[
        'cf_email',
        'celular',
        'cf_fone_res',
        'email_pessoal',
        'cod_funcionario'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->time = date('d-m-Y H:i:s', time());
    }
    
    public function create()
    {
        return $this->medoo->insert('contato_funcionario',[
            array_intersect_key($_POST, array_flip($this->fillable))
        ]);
    }

    public function update($id)
    {
        $insertContatoSimples = $this->medoo->update('contato_funcionario',
              array_intersect_key($_POST, array_flip($this->fillable))
          , [
              "cod_funcionario" => (int)$id
          ]);
    }

    public function getByEmployer($id)
    {
        return $this->medoo->select("contato_funcionario", "*", ["cod_funcionario" => $id]);
    }
}