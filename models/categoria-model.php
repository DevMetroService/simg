<?php
class CategoriaModel extends MainModel
{
    public $dados;
    public $time;

    private $fillable = [
      'nome_categoria',
      'descricao'
    ];

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $this->time = date('d-m-Y H:i:s', time());

        if(!empty($_POST))
            $this->dados = $_POST;
    }

    public function create(){
        $newMaterial = $this->medoo->insert("categoria_material",[
            array_intersect_key($this->dados, array_flip($this->fillable))
        ]);

    }

    public function update($codCategoria){

        $this->medoo->update("categoria_material",
            array_intersect_key($this->dados, array_flip($this->fillable))
        ,[
            'cod_categoria' =>(int)$codCategoria
        ]);

    }

    public function getAll()
    {
        return $this->medoo->select("categoria_material", "*");
    }

    public function get($id)
    {
        return $this->medoo->select("categoria_material", "*", ["cod_categoria" => (int) $id])[0];
    }
}
 ?>
