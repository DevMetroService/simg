<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 11/04/2017
 * Time: 11:19
 */

class MrModel extends MainModel{

    private $dadosMr;

    private $time;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;
        $this->dadosUsuario = $dadosUsuario;

        $this->dadosMr  = $_SESSION['dadosMr'];
        unset($_SESSION['dadosMr']);
        
        $this->time = date('d-m-Y H:i:s', time());
    }

    private function validarDadosVeiculo($dadosMr){
        $arrDados = Array();

        if(!empty($dadosMr['nomeVeiculo']) && !empty($dadosMr['tipoVeiculo'])){
            $selectNome = $this->medoo->select('veiculo', 'nome_veiculo',
                [
                    "nome_veiculo" => (String) strtoupper($dadosMr['nomeVeiculo']),
                    "cod_grupo"    => (int) $dadosMr['tipoVeiculo']
                ]);


            if(!empty($selectNome))
                return false;

            $arrDados['veiculo'] = [
                "nome_veiculo"   => (String) strtoupper($dadosMr['nomeVeiculo']),
                "cod_composicao" => (int) $dadosMr['composicao'],
                "cod_grupo"      => (int) $dadosMr['tipoVeiculo']
            ];
        }else{
            return false;
        }

        return $arrDados;
    }

    private function validarDadosCarro($dadosMr){
        $arrDados = Array();
        if(!empty($dadosMr['nomeCarro']) && !empty($dadosMr['tipoCarro']) && !empty($dadosMr['grupo'])){
            $selectNome = $this->medoo->select('carro', 'nome_carro',
                [
                    "nome_carro" => (String) strtoupper($dadosMr['nomeCarro']),
                    "cod_grupo"  => (int) $dadosMr['tipoVeiculo']
                ]);

            if(!empty($selectNome))
                return false;

            $arrDados['carro'] = [
                "nome_carro"     => (String) strtoupper($dadosMr['nomeCarro']),
                "cod_veiculo"    => (int) $dadosMr['veiculo'],
                "cod_tipo_carro" => (int) $dadosMr['tipoCarro'],
                "cod_grupo"      => (int) $dadosMr['grupo']
            ];
        }else{
            return false;
        }

        return $arrDados;
    }

    private function nomeGrupo($grupo){
        switch ($grupo){
            case 22:
                return 'VLT';
                break;
            case 23:
                return 'TUE';
                break;
            case 26:
                return 'Locomotiva';
                break;
        }
    }

    public function cadastroVeiculo(){
        $dadosValidados = $this->validarDadosVeiculo($this->dadosMr);

        if(!empty($dadosValidados)) {
            $insertVeiculo = $this->medoo->insert('veiculo', [
                $dadosValidados['veiculo']
            ]);

            $this->feedBack(
                'info',
                'Novo Ve�culo',
                'Foi cadastrado com sucesso o Ve�culo ' . $this->nomeGrupo($dadosValidados['veiculo']['cod_grupo']) . ' de nome ' . $dadosValidados['veiculo']['nome_veiculo']
            );
        }else{
            $this->feedBack(
                'error',
                'Ve�culo',
                'O nome do Ve�culo ' . strtoupper($this->dadosMr['nomeVeiculo']) . ' j� existe!'
            );
        }
    }

    public function cadastroCarro(){
        $dadosValidados = $this->validarDadosCarro($this->dadosMr);

        if(!empty($dadosValidados)) {
            $insertCarro = $this->medoo->insert('carro', [
                $dadosValidados['carro']
            ]);

            $this->feedBack(
                'info',
                'Novo Carro',
                'Foi cadastrado com sucesso o Carro ' . $this->nomeGrupo($dadosValidados['carro']['cod_grupo']) . ' n�mero ' . $dadosValidados['carro']['nome_carro']
            );
        }else{
            $this->feedBack(
                'error',
                'Carro',
                'O nome do Carro ' . strtoupper($this->dadosMr['nomeCarro']) . ' j� existe!'
            );
        }
    }

    public function cadastroComposicao(){
        $selectComp = $this->medoo->select('composicao', 'numero_composicao', ["cod_grupo" => $this->dadosMr]);
        $numeroComp = count($selectComp) + 1;

        $insertComp = $this->medoo->insert('composicao', [
            "numero_composicao" => (int) $numeroComp,
            "cod_grupo"         => (int) $this->dadosMr
        ]);

        $this->feedBack(
            'info',
            'Nova Composi��o',
            'Foi cadastrada com sucesso a Composi��o ' . $this->nomeGrupo($this->dadosMr) . ' n�mero ' . $numeroComp
        );
    }

    public function configComposicao(){
        if ($this->dadosMr['composicao'] != '') {
            $selectVeiculosComposicao = $this->medoo->select('veiculo', 'cod_veiculo', ["cod_composicao" => (int)$this->dadosMr['composicao']]);
        }

        for($i = 1; $this->dadosMr['quantVeiculoComposicao'] >= $i; $i++) {
            if (!empty($this->dadosMr['veiculo-' . $i])) {
                if ($this->dadosMr['composicao'] != '') {
                    $keyArray = array_search($this->dadosMr['veiculo-' . $i], $selectVeiculosComposicao);
                    if($keyArray !== false){
                        unset($selectVeiculosComposicao[ $keyArray ]);
                    }

                    $selectVeiculo = $this->medoo->select('veiculo', 'cod_composicao', ["cod_veiculo" => (int)$this->dadosMr['veiculo-' . $i]]);
                    $selectVeiculo = $selectVeiculo[0];

                    if ($selectVeiculo != $this->dadosMr['composicao']) {
                        $updateVeiculo = $this->medoo->update('veiculo', [
                            "cod_composicao" => (int)$this->dadosMr['composicao']
                        ], [
                            "cod_veiculo" => (int)$this->dadosMr['veiculo-' . $i]
                        ]);

                        $insertCompVeiculo = $this->medoo->insert('composicao_veiculo', [
                            "cod_veiculo" => (int)$this->dadosMr['veiculo-' . $i],
                            "cod_composicao" => (int)$this->dadosMr['composicao']
                        ]);
                    }
                }

                $selectCarroVeiculos = $this->medoo->select('carro', 'cod_carro', ["cod_veiculo" => (int)$this->dadosMr['veiculo-' . $i]]);

                if (!empty($this->dadosMr['carro' . $i])) {
                    $carrosRetirados = array_diff($selectCarroVeiculos, $this->dadosMr['carro' . $i]);
                }else{
                    $carrosRetirados = $selectCarroVeiculos;
                }

                for ($y = 0; count($carrosRetirados) > $y; $y++) {
                    $deletarCarroVeiculo = $this->medoo->update('carro', [
                        "cod_veiculo" => null
                    ], [
                        "cod_carro" => (int)$carrosRetirados[$y]
                    ]);
                }

                for ($w = 0; count($this->dadosMr['carro' . $i]) > $w; $w++) {
                    $selectCarro = $this->medoo->select('carro', 'cod_veiculo', ["cod_carro" => $this->dadosMr['carro' . $i][$w]]);
                    $selectCarro = $selectCarro[0];

                    if ($selectCarro != $this->dadosMr['veiculo-' . $i]) {
                        $updateCarro = $this->medoo->update('carro', [
                            "cod_veiculo" => (int)$this->dadosMr['veiculo-' . $i]
                        ], [
                            "cod_carro" => (int)$this->dadosMr['carro' . $i][$w]
                        ]);

                        $insertCarroVeiculo = $this->medoo->insert('carro_veiculo', [
                            "cod_veiculo" => (int)$this->dadosMr['veiculo-' . $i],
                            "cod_carro" => (int)$this->dadosMr['carro' . $i][$w]
                        ]);
                    }
                }
            }
        }

        function deletarVeiculoComposicao($value, $key, $medoo){
            $deletarVeiculoComposicao = $medoo->update('veiculo', [
                "cod_composicao" => null
            ], [
                "cod_veiculo" => (int) $value
            ]);
        }

        if(!empty($selectVeiculosComposicao)){
            // array_walk executa uma fun��o em todos os itens do array
            array_walk($selectVeiculosComposicao, 'deletarVeiculoComposicao', $this->medoo);
        }
    }

    public function disponibilidadeTUE(){
        $selectVeiculo = $this->medoo->select("veiculo", ["[><]carro" => "cod_veiculo"],
            ["cod_veiculo", "cod_carro", "odometro", "disponibilidade"], ["AND" => ["veiculo.cod_grupo" => (int) 23, "carro.carro_lider" => "s"]]);

        if (!empty($selectVeiculo)) {
            foreach ($selectVeiculo as $dados) {
                if ($this->dadosMr['disponibilidade/'.$dados['cod_veiculo']] != $dados['disponibilidade']) {
                    $updateVeiculo = $this->medoo->update('veiculo', [
                        "disponibilidade" => $this->dadosMr['disponibilidade/' . $dados['cod_veiculo']]
                    ], [
                        "cod_veiculo" => $dados['cod_veiculo']
                    ]);
                    $this->updateHistorioDisponibilidade($this->dadosUsuario['cod_usuario'], $dados['cod_veiculo'], $this->dadosMr['disponibilidade/' . $dados['cod_veiculo']], $this->time);
                }

                if ($this->dadosMr['odometro/' . $dados['cod_veiculo']] != $dados['odometro']){
                    $updateCarro = $this->medoo->update('carro', [
                        "odometro" => $this->dadosMr['odometro/'.$dados['cod_veiculo']]
                    ], [
                        "cod_carro" => $dados['cod_carro']
                    ]);

                    //$kmRodado = $dados['odometro'] - $this->dadosMr['odometro/' . $dados['cod_veiculo']];
                }
            }
        }
    }

    public function odometroTUE(){
        $selectVeiculo = $this->medoo->select("carro", ["[><]veiculo" => "cod_veiculo"],
            ["cod_veiculo", "cod_carro", "odometro", "disponibilidade"], ["AND" => ["veiculo.cod_grupo" => (int) 23, "carro.carro_lider" => "s"]]);

        if (!empty($selectVeiculo)) {
            foreach ($selectVeiculo as $dados) {

                $odometroTUE = $this->dadosMr["odometro{$dados['cod_veiculo']}"];

                if (!empty($odometroTUE) AND $odometroTUE != $dados['odometro']){
                    $updateCarro = $this->medoo->update('carro', [
                        "odometro" => (int) $odometroTUE
                    ], [
                        "AND" => [
                            "cod_veiculo" => $dados['cod_veiculo'],
                            "carro_lider" => 's'
                        ]
                    ]);
                    $this->registroHistoricoTUE ($this->dadosUsuario['cod_usuario'], $dados['cod_veiculo'], $odometroTUE);
                }
            }
        }
    }

    public function odometroVLT(){
        $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo"], ["cod_grupo" => (int) 22]);

        if (!empty($selectVeiculo)) {
            foreach ($selectVeiculo as $dados) {
                $valorVLT = [];
                $valorVLT['ma'] = [];
                $valorVLT['mb'] = [];

                if( !empty($this->dadosMr['odometroMA' . $dados['cod_veiculo']]))
                    $valorVLT['ma'] += [ "odometro" => (double) $this->dadosMr['odometroMA'.$dados['cod_veiculo']]];
                if($this->dadosMr['hori_tracaoMA'.$dados['cod_veiculo']])
                    $valorVLT['ma'] += [ "horimetro_tracao" => $this->dadosMr['hori_tracaoMA'.$dados['cod_veiculo']] ];
                if($this->dadosMr['hori_geradorMA'.$dados['cod_veiculo']])
                    $valorVLT['ma'] += [ "horimetro_gerador" => $this->dadosMr['hori_geradorMA'.$dados['cod_veiculo']] ];

                if($this->dadosMr['odometroMB'.$dados['cod_veiculo']])
                    $valorVLT['mb'] += [ "odometro" => (double) $this->dadosMr['odometroMB'.$dados['cod_veiculo']] ];
                if($this->dadosMr['hori_tracaoMB'.$dados['cod_veiculo']])
                    $valorVLT['mb'] += [ "horimetro_tracao" => $this->dadosMr['hori_tracaoMB'.$dados['cod_veiculo']] ];
                if($this->dadosMr['hori_geradorMB'.$dados['cod_veiculo']])
                    $valorVLT['mb'] += [ "horimetro_gerador" => $this->dadosMr['hori_geradorMB'.$dados['cod_veiculo']] ];

                if($valorVLT['ma']) {
                    $updateCarro = $this->medoo->update('carro',
                        $valorVLT['ma']
                    , [
                        "cod_carro" => (int) $this->dadosMr['carroMA' . $dados['cod_veiculo']]
                    ]);
                }

                if($valorVLT['mb']) {
                    $updateCarro = $this->medoo->update('carro',
                        $valorVLT['mb']
                    , [
                        "cod_carro" => $this->dadosMr['carroMB' . $dados['cod_veiculo']]
                    ]);
                }

                if($valorVLT['ma'] || $valorVLT['mb'] ) {
                    $this->registroHistoricoVLT(
                        $this->dadosUsuario['cod_usuario'], $dados['cod_veiculo'],
                        $this->dadosMr['odometroMA' . $dados['cod_veiculo']],
                        $this->dadosMr['hori_tracaoMA' . $dados['cod_veiculo']],
                        $this->dadosMr['hori_geradorMA' . $dados['cod_veiculo']],
                        $this->dadosMr['odometroMB' . $dados['cod_veiculo']],
                        $this->dadosMr['hori_tracaoMB' . $dados['cod_veiculo']],
                        $this->dadosMr['hori_geradorMB' . $dados['cod_veiculo']]
                    );
                }
            }
        }
    }

    public function disponibilidadeVLT(){
        $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo", "disponibilidade"],
            ["AND" => ["veiculo.cod_grupo" => (int) 22], "ORDER" => "nome_veiculo"]);

        if (!empty($selectVeiculo)) {
            foreach ($selectVeiculo as $dados) {
                if($this->dadosMr['disponibilidade/'.$dados['cod_veiculo']] != $dados['disponibilidade']){
                    $updateVeiculo = $this->medoo->update('veiculo', [
                        "disponibilidade" => $this->dadosMr['disponibilidade/'.$dados['cod_veiculo']]
                    ], [
                        "cod_veiculo" => $dados['cod_veiculo']
                    ]);

                    $this->updateHistorioDisponibilidade($this->dadosUsuario['cod_usuario'], $dados['cod_veiculo'], $this->dadosMr['disponibilidade/'.$dados['cod_veiculo']], $this->time);
                }

                $updateCarro = $this->medoo->update('carro', [
                    "odometro"          => $this->dadosMr['odometroMA/'.$dados['cod_veiculo']],
                    "horimetro_tracao"  => $this->dadosMr['hori_tracaoMA/'.$dados['cod_veiculo']],
                    "horimetro_gerador" => $this->dadosMr['hori_geradorMA/'.$dados['cod_veiculo']]
                ], [
                    "cod_carro" => $this->dadosMr['carroMA/'.$dados['cod_veiculo']]
                ]);

                $updateCarro = $this->medoo->update('carro', [
                    "odometro"          => $this->dadosMr['odometroMB/'.$dados['cod_veiculo']],
                    "horimetro_tracao"  => $this->dadosMr['hori_tracaoMB/'.$dados['cod_veiculo']],
                    "horimetro_gerador" => $this->dadosMr['hori_geradorMB/'.$dados['cod_veiculo']]
                ], [
                    "cod_carro" => $this->dadosMr['carroMB/'.$dados['cod_veiculo']]
                ]);
            }
        }
    }

    public function procedimento(){
        if($this->dadosMr['ativo'] =="on"){
            $setAtivo = 's';
        }else{
            $setAtivo = 'n';
        }

        if($this->dadosMr['codigo']){
            $this->medoo->update('servico_material_rodante',
                [
                    'cod_grupo' => (int)$this->dadosMr['grupo'],
                    'nome_servico_material_rodante' => $this->dadosMr['servico'],
                    'procedimento' => $this->dadosMr['procedimento'],
                    'ativo' => $setAtivo
                ],[
                    'cod_servico_material_rodante' => $this->dadosMr['codigo']
                ]
            );
        }else{
            $this->medoo->insert('servico_material_rodante',
                [
                    'cod_grupo' => (int)$this->dadosMr['grupo'],
                    'nome_servico_material_rodante' => $this->dadosMr['servico'],
                    'procedimento' => $this->dadosMr['procedimento'],
                    'ativo' => $setAtivo
                ]);
        }
    }

    public function ficha(){
        $sub_sistema = $this->medoo->select('sub_sistema','cod_sub_sistema',
            [
                "AND" => [
                    'cod_sistema' => (int)$this->dadosMr['sistema'],
                    'cod_subsistema' => (int)$this->dadosMr['subsistema'],
                ]
            ]
        );

        if($this->dadosMr['codigo']){
            $this->medoo->update('ficha_material_rodante',
                [
                    'cod_servico_material_rodante' => (int)$this->dadosMr['servico'],
                    'cod_sub_sistema' => (int)$sub_sistema[0],
                    'denominacao' => $this->dadosMr['denominacao'],
                    'numero_ficha' => $this->dadosMr['ficha'],
                ],[
                    'cod_ficha_material_rodante' => $this->dadosMr['codigo']
                ]
            );
        }else{
            $this->medoo->insert('ficha_material_rodante',
                [
                    'cod_servico_material_rodante' => (int)$this->dadosMr['servico'],
                    'cod_sub_sistema' => (int)$sub_sistema[0],
                    'denominacao' => $this->dadosMr['denominacao'],
                    'numero_ficha' => $this->dadosMr['ficha'],
                ]);
        }
    }

    public function calculoDisponibilidade(){
        $disponibilidade = $this->medoo->select('calc_disponibilidade','cod_calc_disponibilidade',
            [
                "AND" => [
                    'dia'       => $this->dadosMr['dia'],
                    'mes'       => $this->dadosMr['mes'],
                    'ano'       => $this->dadosMr['ano'],
                    'cod_linha' => $this->dadosMr['linha']
                ]
            ]
        );
        $disponibilidade = $disponibilidade[0];

        $infoDecode = utf8_decode($this->dadosMr['info']);

        if(empty($disponibilidade)){
            $this->medoo->insert('calc_disponibilidade',
                [
                    'info'              => $infoDecode,
                    'cod_usuario'       => $this->dadosUsuario['cod_usuario'],
                    'data_alteracao'    => $this->time,
                    'dia'               => $this->dadosMr['dia'],
                    'mes'               => $this->dadosMr['mes'],
                    'ano'               => $this->dadosMr['ano'],
                    'resultado'         => $this->dadosMr['resultado'],
                    'cod_linha'         => (int)$this->dadosMr['linha']
                ]
            );
        }else{
            $this->medoo->update('calc_disponibilidade',
                [
                    'info'              => $infoDecode,
                    'cod_usuario'       => $this->dadosUsuario['cod_usuario'],
                    'data_alteracao'    => $this->time,
                    'dia'               => $this->dadosMr['dia'],
                    'mes'               => $this->dadosMr['mes'],
                    'ano'               => $this->dadosMr['ano'],
                    'resultado'         => $this->dadosMr['resultado'],
                ],[
                    'cod_calc_disponibilidade' => (int)$disponibilidade
                ]
            );
        }
    }

    public function cadastroItemPmp(){
        //Verifica se ja existe um registro para este servico/mes/ano
        $duplicidade = $this->medoo->select('pmp_mr',
            "*",
            [   "AND" =>
                [
                    "cod_grupo" => (int) $this->dadosMr["grupo"],
                    "cod_servico_material_rodante" => (int) $this->dadosMr["cod_servico_mr"],
                    "cod_linha" => (int) $this->dadosMr['cod_linha'],
                    "mes"=> (int) $this->dadosMr["mes"],
                    "ano"=> (int) $this->dadosMr["ano"]
                ]
            ]
        );

        //Cria o novo registro se nao houver duplicidade
        if (empty($duplicidade)) {

            $this->medoo->insert('pmp_mr', [
                "cod_grupo" => (int) $this->dadosMr['grupo'],
                "cod_linha" => (int) $this->dadosMr['cod_linha'],
                "cod_servico_material_rodante" => (int) $this->dadosMr['cod_servico_mr'],
                "mes" => (int) $this->dadosMr['mes'],
                "ano" => (int) $this->dadosMr['ano'],
                "cod_usuario" => $this->dadosUsuario['cod_usuario'],
                "data_cadastro" => $this->time,
                "qtd_manutencao" => (int) $this->dadosMr['qtd_manutencao']
            ]);

        }
    }

    public function atualizarItemPmp(){

        $this->medoo->update('pmp_mr',
            [
                "cod_grupo" => (int) $this->dadosMr['grupo'],
                "cod_servico_material_rodante" => (int) $this->dadosMr['cod_servico_mr'],
                "cod_linha" => (int) $this->dadosMr['cod_linha'],
                "mes" => (int) $this->dadosMr['mes'],
                "ano" => (int) $this->dadosMr['ano'],
                "qtd_manutencao" => (int) $this->dadosMr['qtd_manutencao']
            ],
            [
                "cod_pmp_mr" => $this->dadosMr["cod_pmp_mr"]
            ]
        );
    }

    private function registroHistoricoTUE($usuario, $veiculo, $odometro){
        $historicoTue = $this->medoo->query("SELECT date_part('DAY',data_cadastro) as dia, mes, ano, cod_odometro_tue
                                              FROM odometro_tue WHERE cod_veiculo = {$veiculo}")->fetchAll(PDO::FETCH_ASSOC);

        foreach($historicoTue as $dados){
            if($dados['dia'] == date('j') &&
                $dados['mes'] == date('n') &&
                $dados['ano'] == date('Y'))
            {
                $this->medoo->update('odometro_tue', [
                    "cod_usuario"    => (int) $usuario,
                    "cod_veiculo"    => (int) $veiculo,
                    "odometro"       => $odometro,
                    "data_cadastro"  => $this->time,
                    "mes"            => date('m'),
                    "ano"            => date('Y')
                ],[
                    "cod_odometro_tue" => (int)$dados['cod_odometro_tue']
                ]);

                return;
            }
        }

        $this->medoo->insert('odometro_tue', [
            "cod_usuario"    => (int) $usuario,
            "cod_veiculo"    => (int) $veiculo,
            "odometro"       => $odometro,
            "data_cadastro"  => $this->time,
            "mes"            => date('m'),
            "ano"            => date('Y')
        ]);

    }

    private function registroHistoricoVLT($usuario, $veiculo, $odometroMA, $tracaoMA, $geradorMA, $odometroMB, $trachaMB, $geradorMB){
        $historicoVlt = $this->medoo->query("SELECT date_part('DAY',data_cadastro) as dia, mes, ano, cod_odometro_vlt
                                              FROM odometro_vlt WHERE cod_veiculo = {$veiculo}")->fetchAll(PDO::FETCH_ASSOC);


        foreach($historicoVlt as $dados){
            if($dados['dia'] == date('j') &&
                $dados['mes'] == date('n') &&
                $dados['ano'] == date('Y'))
            {
                $this->medoo->update('odometro_vlt', [
                    "cod_usuario"    => (int) $usuario,
                    "cod_veiculo"    => (int) $veiculo,
                    "odometro_ma"    => $odometroMA,
                    "tracao_ma"      => $tracaoMA,
                    "gerador_ma"     => $geradorMA,
                    "odometro_mb"    => $odometroMB,
                    "tracao_mb"      => $trachaMB,
                    "gerador_mb"     => $geradorMB,
                    "data_cadastro"  => $this->time,
                    "mes"            => date('m'),
                    "ano"            => date('Y')
                ],[
                    "cod_odometro_vlt" => (int)$dados['cod_odometro_vlt']
                ]);

                return;
            }
        }

        $this->medoo->insert('odometro_vlt', [
            "cod_usuario"    => (int) $usuario,
            "cod_veiculo"    => (int) $veiculo,
            "odometro_ma"    => $odometroMA,
            "tracao_ma"      => $tracaoMA,
            "gerador_ma"     => $geradorMA,
            "odometro_mb"    => $odometroMB,
            "tracao_mb"      => $trachaMB,
            "gerador_mb"     => $geradorMB,
            "data_cadastro"  => $this->time,
            "mes"            => date('m'),
            "ano"            => date('Y')
        ]);
    }

    private function updateHistorioDisponibilidade($usuario, $veiculo, $status, $data){
        switch ($status){
            case 's':
                $status = 35;
                break;
            case 'n':
                $status = 36;
                break;
            case 'i':
                $status = 37;
                break;
        }

//        $hasInfo = $this->medoo->select('disponiblidade', '*', ['cod_veiculo' => (int)$veiculo]);
//        if($hasInfo){
//            $this->medoo->update('disponibilidade', [
//                "cod_usuario"    => (int) $usuario,
//                "cod_veiculo"    => (int) $veiculo,
//                "cod_status"     => (int) $status,
//                "data_alteracao" => $data
//            ],[
//                "cod-Veiculo"    => (int) $veiculo
//            ]);
//        }else{
//            $this->medoo->insert('disponibilidade', [
//                "cod_usuario"    => (int) $usuario,
//                "cod_veiculo"    => (int) $veiculo,
//                "cod_status"     => (int) $status,
//                "data_alteracao" => $data
//            ]);
//        }

            $this->medoo->insert('disponibilidade', [
                "cod_usuario"    => (int) $usuario,
                "cod_veiculo"    => (int) $veiculo,
                "cod_status"     => (int) $status,
                "data_alteracao" => $data
            ]);

    }

    #====================== GERADOR DE EXCEL ======================#

    public function csvKmRodadosPorVeiculoTUE() {

        /* Dados do Solicitante */
        if (!empty($_POST['codUsuario'])) {
            $where[] = "o.cod_usuario = {$_POST['codUsuario']}";
        }


        /* Data ( Apartir / At� ) */
        if(!empty($_POST['dataPartir']))
            $dataPartir = $this->inverteData($_POST['dataPartir']);

        if(!empty($_POST['dataRetroceder']))
            $dataRetroceder = $this->inverteData($_POST['dataRetroceder']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "o.data_cadastro >= '{$dataPartir}'";
                $where[] = "o.data_cadastro <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "o.data_cadastro >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "o.data_cadastro < '{$dataRetroceder}'";
                }
            }
        }


        /* Veiculos */
        if (!empty($_POST['veiculos'])) {
            $veiculos =  "( v.cod_veiculo = ". implode(' OR v.cod_veiculo = ', $_POST['veiculos']) . ")";
            $where[] = $veiculos;
        }


        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT
                o.cod_odometro_tue AS cod_odometro,
                v.cod_veiculo,
                v.nome_veiculo,
                v.cod_grupo,
                o.data_cadastro,
                o.odometro,
                o.cod_usuario,
                u.usuario AS nome_usuario,
                o.ano AS ano,
                o.mes AS mes
                FROM odometro_tue AS o 
                JOIN veiculo v ON o.cod_veiculo = v.cod_veiculo
                JOIN usuario u ON u.cod_usuario = o.cod_usuario";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        $sql = $sql . " ORDER BY o.data_cadastro, v.cod_veiculo";

        //Executa o select a partir do medoo
        $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);


        return $resultado;
    }

    public function csvKmRodadosPorVeiculoVLT() {

        /* Dados do Solicitante */
        if (!empty($_POST['codUsuario'])) {
            $where[] = "o.cod_usuario = {$_POST['codUsuario']}";
        }


        /* Data ( Apartir / At� ) */
        if(!empty($_POST['dataPartir']))
            $dataPartir = $this->inverteData($_POST['dataPartir']);

        if(!empty($_POST['dataRetroceder']))
            $dataRetroceder = $this->inverteData($_POST['dataRetroceder']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "o.data_cadastro >= '{$dataPartir}'";
                $where[] = "o.data_cadastro <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "o.data_cadastro >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "o.data_cadastro < '{$dataRetroceder}'";
                }
            }
        }


        /* Linha */
        if (!empty($_POST['linha'])) {
            $where[] = "v.cod_linha = {$_POST['linha']}";
        }


        /* Veiculos */
        if (!empty($_POST['veiculos'])) {
            $veiculos =  "( v.cod_veiculo = ". implode(' OR v.cod_veiculo = ', $_POST['veiculos']) . ")";
            $where[] = $veiculos;
        }


        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT
                o.cod_odometro_vlt AS cod_odometro,
                v.cod_veiculo,
                v.nome_veiculo,
                v.cod_grupo,
                l.cod_linha,
                l.nome_linha,
                o.data_cadastro,
                o.odometro_ma,
                o.tracao_ma,
                o.gerador_ma,
                o.odometro_mb,
                o.tracao_mb,
                o.gerador_mb,
                o.cod_usuario,
                u.usuario AS nome_usuario,
                o.ano AS ano,
                o.mes AS mes
                FROM odometro_vlt AS o 
                JOIN veiculo v ON o.cod_veiculo = v.cod_veiculo
                JOIN usuario u ON u.cod_usuario = o.cod_usuario
                JOIN linha as l ON v.cod_linha = l.cod_linha";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        $sql = $sql . " ORDER BY o.data_cadastro, v.cod_veiculo, l.cod_linha";

        //Executa o select a partir do medoo
        $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);


        return $resultado;
    }
}