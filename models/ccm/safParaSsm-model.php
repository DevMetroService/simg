<?php

class SafParaSsmModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSaf = $_SESSION['SafToSsm'];

        $time = date('d-m-Y H:i:s', time());

        //Cadastro SSM a partir da SAF
        $viewSaf = $this->medoo->select("v_saf", "nivel", ["cod_saf" => (int) $dadosSaf['codigoSaf']]);

        //Criar SSM
        $createSsm = $this->medoo->insert("ssm",[
            "cod_saf"       => (int) $dadosSaf['codigoSaf'],
            "data_abertura" => $this->inverteData($time),
            "nivel"         => (string) $viewSaf[0]
        ]);

        $inserirStatusSsm = $this->medoo->insert("status_ssm",[
            "cod_status"    => 9, //Aberta
            "cod_ssm"       => (int) $createSsm,
            "data_status"   => $this->inverteData($time),
            "usuario"       => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateSsm = $this->medoo->update("ssm",["cod_mstatus" => (int) $inserirStatusSsm], ["cod_ssm" => (int)$createSsm]);

        //Alterar Status Saf
        $insertStatusSaf = $this->medoo->insert('status_saf',[
            "cod_saf"       => (int) $dadosSaf['codigoSaf'],
            "cod_status"    => (int) 1, // Autorizada
            "data_status"   => $this->inverteData($time),
            "usuario"       => (int)  $dadosUsuario['cod_usuario']
        ]);

        $updateSaf = $this->medoo->update("saf",["cod_ssaf" => (int) $insertStatusSaf], ["cod_saf" => (int) $dadosSaf['codigoSaf']]);

        unset($_SESSION['SafToSsm']);

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = "Saf n.{$dadosSaf['codigoSaf']} autorizada.
                                        <br />Ssm n.{$createSsm} aberta.
                                        <br />Por:  {$dadosUsuario['usuario']}.";

        //Tipo de a��o
        $_SESSION['notificacao']['tipo'] = "Aberta";
        $_SESSION['notificacao']['tabela'] = "Ssm";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero']          = $createSsm;
        $_SESSION['notificacao']['numeroSaf']       = $dadosSaf['codigoSaf'];
        $_SESSION['notificacao']['dataAbertura']    = $time;
        $_SESSION['notificacao']['nivel']           = $viewSaf[0];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}
