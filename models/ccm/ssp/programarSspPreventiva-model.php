<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/11/2015
 * Time: 10:58
 */
class ProgramarSspPreventivaModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $_SESSION['dadosSsp']['time'] = $time;
        $_SESSION['dadosSsp']['dadosUsuario'] = $dadosUsuario;

        $codigoEquipe = $this->getCodEquipe($_SESSION['dadoSsp']['equipeSsp']);

        $_SESSION['unEquipe'] = $this->medoo->select("un_equipe", "cod_un_equipe", [
            "AND" => [
                "cod_equipe" => (int)$codigoEquipe,
                "cod_unidade" => (int)$_SESSION['dadoSsp']['localSsp']
            ]
        ]);
        $_SESSION['unEquipe'] = $_SESSION['unEquipe'][0];

        $updateSsp = $this->medoo->update('ssp', [
            'cod_tipo_intervencao' => (int)32,
            'data_programada' => $_SESSION['dadoSsp']['dataHoraSsp'],
            'dias_servico' => (int)$_SESSION['dadoSsp']['diasServico'],
            'solicitacao_acesso' => (string)$_SESSION['dadoSsp']['numeroSolicitacaoAcesso'],
            'complemento' => (string)$_SESSION['dadoSsp']['complementoServicoSsp'],
            'cod_un_equipe' => (int)$_SESSION['unEquipe']
        ], [
            'cod_ssp' => (int)$_SESSION['dadoSsp']['codigoSsp']
        ]);

        $insertStatusAtual = $this->medoo->insert("status_ssp", [
            "cod_status" => (int)19, // status programado para equipe
            "cod_ssp" => (int)$_SESSION['dadoSsp']['codigoSsp'],
            "data_status" => $this->inverteData($_SESSION['dadosSsp']['time']),
            "usuario" => (int)$_SESSION['dadosSsp']['dadosUsuario']['cod_usuario']
        ]);

        $updateStatusInSsp = $this->medoo->update('ssp', [
            'cod_pstatus' => (int)$insertStatusAtual
        ], [
            'cod_ssp' => (int)$_SESSION['dadoSsp']['codigoSsp']
        ]);

        $updatetLocalSsp = $this->medoo->update('local_ssp', [
            'cod_ponto_notavel' => (int)$_SESSION['dadoSsp']['pontoNotavelSsp'],
            'km_inicial' => (string)$_SESSION['dadoSsp']['kmInicialSsp'],
            'km_final' => (string)$_SESSION['dadoSsp']['kmFinalSsp'],
            'posicao' => (string)$_SESSION['dadoSsp']['posicaoSsp'],
            'cod_via' => (int)$_SESSION['dadoSsp']['viaSsp'],
            'complemento' => (string)$_SESSION['dadoSsp']['complementoLocalSsp']
        ], [
            'cod_ssp' => (int)$_SESSION['dadoSsp']['codigoSsp']
        ]);

        if (empty($updateSsp) || empty($insertStatusAtual) || empty($updatetLocalSsp)) {
            $_SESSION['feedback_banco'] = $this->medoo->error();
        }

        unset($_SESSION['dadoSsp']);
        unset($_SESSION['unEquipe']);
    }
}