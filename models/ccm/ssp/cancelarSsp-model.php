<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 29/06/2015
 * Time: 08:11
 */

class CancelarSspModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSsp = $_SESSION['dados'];

        $statusAntigo = $this->medoo->select("v_ssp", "nome_status", ["cod_ssp" => (int) $dadosSsp['codigo']]);
        $statusAntigo = $statusAntigo[0];

        //############## Status_Ssp #####################
        //############## Status_Ssp #####################
        //############## Status_Ssp #####################

        $time = date('d-m-Y H:i:s', time());

        if ($dadosSsp['acaoModal'] == "encerrar") {
            $insertStatusSsp = $this->medoo->insert('status_ssp', [
                "cod_ssp"       => (int)$dadosSsp['codigo'],
                "cod_status"    => (int)18, // Encerrar
                "descricao"     => (string)$dadosSsp['motivoAcao'],
                "data_status"   => $time,
                "usuario"       => (int)$dadosUsuario['cod_usuario']
            ]);

            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Ssp n. {$dadosSsp['codigo']} encerrada.";

        } else {
            $insertStatusSsp = $this->medoo->insert('status_ssp', [
                "cod_ssp"       => (int)$dadosSsp['codigo'],
                "cod_status"    => (int)17, // Cancelada
                "descricao"     => (string)$dadosSsp['motivoAcao'],
                "data_status"   => $time,
                "usuario"       => (int)$dadosUsuario['cod_usuario']
            ]);

            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Ssp n. {$dadosSsp['codigo']} cancelada.";
        }

        $updateSsp = $this->medoo->update('ssp', ["cod_pstatus" => (int)$insertStatusSsp], ["cod_ssp" => (int)$dadosSsp['codigo']]);

        $this->alterarStatusSsmReferente($dadosSsp['codigo'], $time, $dadosUsuario);

        unset($_SESSION['dados']);

        $selectVSsp = $this->medoo->select('v_ssp', "*", ['cod_ssp' => (int) $dadosSsp['codigo']]);
        $selectVSsp = $selectVSsp[0];

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";
        $_SESSION['notificacao']['nomeUsuario']     = $dadosUsuario['usuario'];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']            = "Cancelar";
        $_SESSION['notificacao']['statusAntigo']    = $statusAntigo;
        $_SESSION['notificacao']['tabela']          = "Ssp";

        //Informa��o para alimentar qualquer  da tabela Ssm
        $_SESSION['notificacao']['numero']          = $dadosSsp['codigo'];

        $_SESSION['notificacao']['subChannelEquipe'] = $selectVSsp['cod_un_equipe'];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}