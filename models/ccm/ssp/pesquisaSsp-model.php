<?php
    ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/08/2015
 * Time: 14:29
 */

class PesquisaSspModel extends MainModel
{
    private $dadosReturn;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_POST;

        //Verifica se h� valores e acrescenta na variavel where

        //#########Dados Gerais
        if (!empty($dadosPesquisa['numeroSsp'])) {
            $where[] = "cod_ssp = {$dadosPesquisa['numeroSsp']}";
        }

        if (!empty($dadosPesquisa['solicitanteUsuario'])) {
            $where[] = "codigo_usuario = {$dadosPesquisa['solicitanteUsuario']}";
        }

        if (!empty($dadosPesquisa['origemProgramacao'])) {
            $where[] = "tipo_orissp = {$dadosPesquisa['origemProgramacao']}";
        }

        if (!empty($dadosPesquisa['numeroSA'])) {
            $where[] = "solicitacao_acesso = '{$dadosPesquisa['numeroSA']}'";
        }

        //#########Status
        if(!empty($dadosPesquisa['statusPesquisa'])){
            $where[] = "cod_status = {$dadosPesquisa['statusPesquisa']}";
        }

        //=====DATA STATUS
        if(!empty($dadosPesquisa['dataPartir']))
            $dataPartir = $this->inverteData($dadosPesquisa['dataPartir']);

        if(!empty($dadosPesquisa['dataRetroceder']))
            $dataRetroceder = $this->inverteData($dadosPesquisa['dataRetroceder']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "data_status >= '{$dataPartir}'";
                $where[] = "data_status <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "data_status >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "data_status < '{$dataRetroceder}'";
                }
            }
        }

        //=====DATA ABERTURA
        if(!empty($dadosPesquisa['dataPartirAbertura']))
            $dataPartirAbertura = $this->inverteData($dadosPesquisa['dataPartirAbertura']);

        if(!empty($dadosPesquisa['dataAteAbertura']))
            $dataRetrocederAbertura = $this->inverteData($dadosPesquisa['dataAteAbertura']);

        if ($dataPartirAbertura != "" || $dataRetrocederAbertura != "") {
            if ($dataPartirAbertura == $dataRetrocederAbertura) {
                $dataPartirAbertura = $dataPartirAbertura . " 00:00:00";
                $dataRetrocederAbertura = $dataRetrocederAbertura . " 23:59:59";

                $where[] = "data_abertura_ssp >= '{$dataPartirAbertura}'";
                $where[] = "data_abertura_ssp <= '{$dataRetrocederAbertura}'";

            } else {
                if (!empty($dataPartirAbertura)) {
                    $dataPartirAbertura = $dataPartirAbertura . " 00:00:00";
                    $where[] = "data_abertura_ssp >= '{$dataPartirAbertura}'";
                }

                if (!empty($dataRetrocederAbertura)) {
                    $dataRetrocederAbertura = $dataRetrocederAbertura . " 23:59:59";
                    $where[] = "data_abertura_ssp < '{$dataRetrocederAbertura}'";
                }
            }
        }

        //###########Local
        if (!empty($dadosPesquisa['linhaPesquisaSsp'])) {
            $where[] = "cod_linha = {$dadosPesquisa['linhaPesquisaSsp']}";
        }

        if (!empty($dadosPesquisa['trechoPesquisaSsp'])) {
            $where[] = "cod_trecho = {$dadosPesquisa['trechoPesquisaSsp']}";
        }

        if (!empty($dadosPesquisa['pnPesquisaSsp'])) {
            $where[] = "cod_ponto_notavel = {$dadosPesquisa['pnPesquisaSsp']}";
        }

        //###########Solicitante
        if (!empty($dadosPesquisa['matriculaSolicitante'])) {
            $where[] = "matricula = {$dadosPesquisa['matriculaSolicitante']}";
        }

        if (!empty($dadosPesquisa['cpfSolicitante'])) {
            $where[] = "cpf_origem = '{$dadosPesquisa['cpfSolicitante']}'";
        }

        ///############Servi�o
        if (!empty($dadosPesquisa['grupoSistemaPesquisa'])) {
            $where[] = "cod_grupo = {$dadosPesquisa['grupoSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['sistemaPesquisa'])) {
            $where[] = "cod_sistema = {$dadosPesquisa['sistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['subSistemaPesquisa'])) {
            $where[] = "cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['servicoPesquisa'])) {
            $where[] = "cod_servico = {$dadosPesquisa['servicoPesquisa']}";
        }

        if (!empty($dadosPesquisa['tipoIntervencaoPesquisa'])) {
            $where[] = "cod_tipo_intervencao = {$dadosPesquisa['tipoIntervencaoPesquisa']}";
        }

        //#############Encaminhamento
        if (!empty($dadosPesquisa['localPesquisa'])) {
            $where[] = "cod_unidade = {$dadosPesquisa['localPesquisa']}";
        }

        if (!empty($dadosPesquisa['equipePesquisa'])) {
            $where[] = "sigla_equipe = '{$dadosPesquisa['equipePesquisa']}'";
        }

        if (!empty($dadosPesquisa['veiculoPesquisa'])) {
            $where[] = "cod_veiculo = '{$dadosPesquisa['veiculoPesquisa']}'";
        }

        if (!empty($dadosPesquisa['carroAvariadoPesquisa'])) {
            $where[] = "cod_carro = '{$dadosPesquisa['carroAvariadoPesquisa']}'";
        }

        if (!empty($dadosPesquisa['odometroPartirPesquisa'])) {
            $where[] = "odometro > '{$dadosPesquisa['odometroPartirPesquisa']}'";
        }

        if (!empty($dadosPesquisa['odometroAtePesquisa'])) {
            $where[] = "odometro < '{$dadosPesquisa['odometroAtePesquisa']}'";
        }

        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT {$dadosPesquisa['whereSql']} FROM v_ssp";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $this->dadosReturn = (boolval(false));
        } else {
            $this->dadosReturn = $resultado;
        }
    }

    function getDados(){
        return $this->dadosReturn;
    }
}
