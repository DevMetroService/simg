<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/11/2015
 * Time: 10:58
 */

class AlterarSspModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadoSsp = $_SESSION['dadoSsp'];

        $time = date('d-m-Y H:i:s', time());

        $codigoUsuario = $this->getCodUsuario($dadoSsp['responsavelSsp']);
        $codigoEquipe  = $this->getCodEquipe($dadoSsp['equipeSsp']);

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
            "AND" =>[
                "cod_equipe"        => (int) $codigoEquipe,
                "cod_unidade"       => (int) $dadoSsp['localSsp']
            ]
        ]);

        $updateInSsp = $this->medoo->update('ssp',[
            'cod_servico'                   => (int)$dadoSsp['servicoSsp'],
            'cod_tipo_intervencao'          => (int)$dadoSsp['tipoIntervencao'],
            'data_abertura'                 => $time,
            'cod_un_equipe'                 => (int)$unEquipe[0],
            'cod_usuario'                   => (int)$codigoUsuario,
            'complemento'                   => (string)$dadoSsp['complementoServicoSsp'],
            'data_programada'               => $dadoSsp['dataHoraSsp'],
            'dias_servico'                  => (int)$dadoSsp['diasServico'],
            'solicitacao_acesso'            => (string)$dadoSsp['numeroSolicitacaoAcesso'],

            'matricula'                     =>(int) $dadoSsp['matriculaSolicitante'],
            'nome'                          =>(string) $dadoSsp['nomeSolicitante'],
            'cpf'                           =>(string) $dadoSsp['cpfSolicitante'],
            'contato'                       =>(string) $dadoSsp['contatoSolicitante'],

            'cod_grupo'                     =>(int) $dadoSsp['grupoSistemaSsp'],
            'cod_sistema'                   =>(int) $dadoSsp['sistemaSsp'],
            'cod_subsistema'                =>(int) $dadoSsp['subSistemaSsp'],
            'cod_linha'                     =>(int) $dadoSsp['linhaSsp'],
            'km_inicial'                    =>(string) $dadoSsp['kmInicialSsp'],
            'km_final'                      =>(string) $dadoSsp['kmFinalSsp'],
            'cod_trecho'                    =>(int) $dadoSsp['trechoSsp'],
            'cod_ponto_notavel'             =>(int) $dadoSsp['pontoNotavelSsp'],
            'posicao'                       =>(string) $dadoSsp['posicaoSsp'],
            'cod_via'                       =>(int) $dadoSsp['viaSsp'],
            'complemento_local'             =>(string) $dadoSsp['complementoLocalSsp']
        ],[
            'cod_ssp'                       => $dadoSsp['codigoSsp']
        ]);

        if($dadoSsp['grupoSistemaSsp'] == '22' || $dadoSsp['grupoSistemaSsp'] == '23' || $dadoSsp['grupoSistemaSsp'] == '26'){
            $hasDadosMr = $this->medoo->select("material_rodante_ssp", "*", ["cod_ssp" => (int)$dadoSsp['codigoSsp']]);

            if(!empty($hasDadosMr)){
                $insertMr = $this->medoo->update("material_rodante_ssp",
                    [
                        "cod_veiculo"   => (int)$dadoSsp['veiculoMrSsp'],
                        "cod_carro"     => (int)$dadoSsp['carroMrSsp'],
                        "odometro"      => (double)$dadoSsp['odometroMrSsp'],
                        "cod_grupo"     => (int)$dadoSsp['grupoSistemaSsp']
                    ],[
                        'cod_ssp'       => $dadoSsp['codigoSsp']
                    ]);
            }else{
                $insertMr = $this->medoo->insert("material_rodante_ssp",
                    [
                        "cod_veiculo"   => (int)$dadoSsp['veiculoMrSsp'],
                        "cod_carro"     => (int)$dadoSsp['carroMrSsp'],
                        "odometro"      => (double)$dadoSsp['odometroMrSsp'],
                        "cod_grupo"     => (int)$dadoSsp['grupoSistemaSsp'],
                        "cod_ssp"       => (int)$dadoSsp['codigoSsp']
                    ]);
            }

        }

        unset($_SESSION['dadoSsp']);

        $viewSsp = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int) $dadoSsp['codigoSsp']]);
        $viewSsp = $viewSsp[0];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']   = "Alterar";
        $_SESSION['notificacao']['tabela'] = "Ssp";

        //Informa��o para alimentar qualquer  da tabela Ssm
        $_SESSION['notificacao']['numero']             = $dadoSsp['codigoSsp'];
        $_SESSION['notificacao']['dataProgramada']     = $this->parse_timestamp($viewSsp["data_programada"]);
        $_SESSION['notificacao']['qtdDias']            = $viewSsp["dias_servico"];
        $_SESSION['notificacao']['status']             = $viewSsp["nome_status"];

        $_SESSION['notificacao']['veiculo']            = $viewSsp["nome_veiculo"];

        $_SESSION['notificacao']['servico']            = $viewSsp["nome_servico"];
        $_SESSION['notificacao']['complementoServico'] = $viewSsp["complemento"];

        $_SESSION['notificacao']['subChannelEquipe']   = $unEquipe[0];
        $_SESSION['notificacao']['nomeUsuario']        = $dadosUsuario['usuario'];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}