<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/11/2015
 * Time: 10:58
 */
class CadastroSspModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $_SESSION['dadosSsp']['time'] = $time;
        $_SESSION['dadosSsp']['dadosUsuario'] = $dadosUsuario;

        $codigoUsuario = $this->getCodUsuario($_SESSION['dadoSsp']['responsavelSsp']);
        $codigoEquipe = $this->getCodEquipe($_SESSION['dadoSsp']['equipeSsp']);

        $_SESSION['unEquipe'] = $this->medoo->select("un_equipe", "cod_un_equipe", [
            "AND" => [
                "cod_equipe" => (int)$codigoEquipe,
                "cod_unidade" => (int)$_SESSION['dadoSsp']['localSsp']
            ]
        ]);
        $_SESSION['unEquipe'] = $_SESSION['unEquipe'][0];

        $_SESSION['codSsp'] = $codigoSsp = $this->medoo->insert('ssp', [
            'cod_servico' => (int)$_SESSION['dadoSsp']['servicoSsp'],
            'cod_tipo_intervencao' => (int)$_SESSION['dadoSsp']['tipoIntervencao'],
            'data_abertura' => $_SESSION['dadosSsp']['time'],
            'cod_un_equipe' => (int)$_SESSION['unEquipe'],
            'cod_usuario' => (int)$codigoUsuario,
            'complemento' => (string)$_SESSION['dadoSsp']['complementoServicoSsp'],
            'data_programada' => $_SESSION['dadoSsp']['dataHoraSsp'],
            'dias_servico' => (int)$_SESSION['dadoSsp']['diasServico'],
            'solicitacao_acesso' => (string)$_SESSION['dadoSsp']['numeroSolicitacaoAcesso'],

            'tipo_orissp' => (int)$_SESSION['dadoSsp']['origemProgramacao'],
            'matricula' => (int)$_SESSION['dadoSsp']['matriculaSolicitante'],
            'nome' => (string)$_SESSION['dadoSsp']['nomeSolicitante'],
            'contato' => (string)$_SESSION['dadoSsp']['contatoSolicitante'],

            'cod_grupo' => (int)$_SESSION['dadoSsp']['grupoSistemaSsp'],
            'cod_sistema' => (int)$_SESSION['dadoSsp']['sistemaSsp'],
            'cod_subsistema' => (int)$_SESSION['dadoSsp']['subSistemaSsp'],
            'cod_linha' => (int)$_SESSION['dadoSsp']['linhaSsp'],
            'km_inicial' => (string)$_SESSION['dadoSsp']['kmInicialSsp'],
            'km_final' => (string)$_SESSION['dadoSsp']['kmFinalSsp'],
            'cod_trecho' => (int)$_SESSION['dadoSsp']['trechoSsp'],
            'cod_ponto_notavel' => (int)$_SESSION['dadoSsp']['pontoNotavelSsp'],
            'posicao' => (string)$_SESSION['dadoSsp']['posicaoSsp'],
            'cod_via' => (int)$_SESSION['dadoSsp']['viaSsp'],
            'complemento_local' => (string)$_SESSION['dadoSsp']['complementoLocalSsp']
        ]);

        if($_SESSION['dadoSsp']['grupoSistemaSsp'] == '22' || $_SESSION['dadoSsp']['grupoSistemaSsp'] == '23' || $_SESSION['dadoSsp']['grupoSistemaSsp'] == '26'){
             $insertMr = $this->medoo->insert("material_rodante_ssp",
                 [
                     "cod_ssp"  => $codigoSsp,
                     "cod_veiculo"  => (int)$_SESSION['dadoSsp']['veiculoMrSsp'],
                     "odometro"  => (double)$_SESSION['dadoSsp']['odometroMrSsp'],
                     "cod_grupo"=> (int)$_SESSION['dadoSsp']['grupoSistemaSsp'],


                     "cod_carro"=> (int)$_SESSION['dadoSsp']['carroMrSsp']
                 ]
             );
        }

        $insertStatusAtual = $this->medoo->insert("status_ssp", [
            "cod_status" => (int)19, // status aberta/programado para equipe
            "cod_ssp" => (int)$codigoSsp,
            "data_status" => $this->inverteData($_SESSION['dadosSsp']['time']),
            "usuario" => (int)$_SESSION['dadosSsp']['dadosUsuario']['cod_usuario']
        ]);

        $updateStatusInSsp = $this->medoo->update('ssp', [
            'cod_pstatus' => (int)$insertStatusAtual
        ], [
            'cod_ssp' => (int)$_SESSION['codSsp']
        ]);

        if (!empty($codigoSsp) && !empty($insertStatusAtual)) {
            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Ssp n. {$codigoSsp} criada.";

            if (!empty($insertStatusAtualSsm)) {
                $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. {$_SESSION['dadoSsp']['numeroSsmPendente']} agendada.";
            }

            $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$_SESSION['dadosSsp']['dadosUsuario']['usuario']}";
            $_SESSION['notificacao']['nomeUsuario'] = $_SESSION['dadosSsp']['dadosUsuario']['usuario'];

            $viewSsp = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$_SESSION['codSsp']]);
            $viewSsp = $viewSsp[0];

            //Tipo de a��o
            $_SESSION['notificacao']['tipo'] = "Inserir";
            $_SESSION['notificacao']['tabela'] = "Ssp";

            //Informa��o para alimentar qualquer  da tabela Ssm
            $_SESSION['notificacao']['numero'] = $_SESSION['codSsp'];
            $_SESSION['notificacao']['dataProgramada'] = $this->parse_timestamp($viewSsp["data_programada"]);
            $_SESSION['notificacao']['qtdDias'] = $viewSsp["dias_servico"];
            $_SESSION['notificacao']['status'] = $viewSsp["nome_status"];

            $_SESSION['notificacao']['veiculo'] = $viewSsp["nome_veiculo"];

            $_SESSION['notificacao']['servico'] = $viewSsp["nome_servico"];
            $_SESSION['notificacao']['complementoServico'] = $viewSsp["complemento"];

            $_SESSION['notificacao']['subChannelEquipe'] = $_SESSION['unEquipe'];

            //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];

            $title = $viewSsp['cod_ssp'] . " - " . $viewSsp['nome_servico'];
            $descr = '<div class="row">
								<div class="col-md-4">
									<label>Codigo Ssp</label>
									<input disabled type="text" name="codigoSspCallendar" value="' . $viewSsp['cod_ssp'] . '" class="form-control">
								</div>
								<div class="col-md-8">
									<label>Status</label>
									<input disabled type="text" value="' . $viewSsp['nome_status'] . '" class="form-control">
								</div>
							</div>
							<label>Tipo Interven��o</label>
							<input disabled type="text" value="' . $viewSsp['nome_tipo_intervencao'] . '" class="form-control">
							<label>Local</label>
							<input disabled type="text" value="' . $viewSsp['nome_trecho'] . ' - ' . $viewSsp['descricao_trecho'] . '" class="form-control">
							<label>Equipe</label>
							<input disabled type="text" value="' . $viewSsp['nome_equipe'] . '" class="form-control">
							<label>Servi�o</label>
							<input disabled type="text" value="' . $viewSsp['nome_servico'] . '" class="form-control">';

            $dataProgramada = $viewSsp['data_programada'];
            $diasServico = $viewSsp['dias_servico'];

            $timestamp = strtotime($dataProgramada);

            $start = date('Y/m/d', $timestamp);
            $end = date('Y/m/d', strtotime('+' . $diasServico . ' days', strtotime($dataProgramada)));

            if ($viewSsp['nome_status'] == "Encerrada" || $viewSsp['nome_status'] == "Cancelada") {
                $backcolor = "gray";
            } else if ($viewSsp['tipo_orissp'] == "2") {
                $backcolor = "red";
            } else {
                $backcolor = "default";
            }

            $_SESSION['notificacao']['title'] = $title;
            $_SESSION['notificacao']['description'] = $descr;
            $_SESSION['notificacao']['start'] = $start;
            $_SESSION['notificacao']['end'] = $end;
            $_SESSION['notificacao']['backgroundColor'] = $backcolor;

        } else {
            $_SESSION['feedback_banco'] = $this->medoo->error();
            return false;
        }

        unset($_SESSION['dadoSsp']);
        unset($_SESSION['unEquipe']);
        unset($_SESSION['codSsp']);
    }
}