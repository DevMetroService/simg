<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/11/2015
 * Time: 10:58
 */
class CadastroSspPreventivaModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;

        $time = date('d-m-Y H:i:s', time());

        $_SESSION['dadosSsp']['time'] = $time;
        $_SESSION['dadosSsp']['dadosUsuario'] = $dadosUsuario;

        $pmp = $this->medoo->select("pmp_edificacao",["[><]servico_pmp" => "cod_servico_pmp","[><]local" => "cod_local","[><]sub_sistema" => "cod_subsistema"], "*", ["cod_pmp_edificacao" => (int)$_SESSION['codPMP']]);
        $pmp = $pmp[0];

        $_SESSION['dadosSsp']['pmp'] = $pmp;


        $medooBegin = $this->medoo;
        $bol = $medooBegin->action(function ($medooBegin) {

            $pmp = $this->medoo->select("pmp", "*", ["cod_pmp" => (int)$_SESSION['codPMP']]);
            $pmp = $pmp[0];

            $codigoSsp = $medooBegin->insert('ssp', [
                'cod_servico'   => 1,// Alterar aqui         (int)$pmp['servico'], // Alterar aqui
                'data_abertura' => $_SESSION['dadosSsp']['time']
            ]);

            $insertStatusAtual = $medooBegin->insert("status_ssp", [
                "cod_status"    => (int)29, //ABERTA status aberta
                "cod_ssp"       => (int)$codigoSsp,
                "data_status"   => $this->inverteData($_SESSION['dadosSsp']['time']),
                "usuario"       => (int)$_SESSION['dadosSsp']['dadosUsuario']['cod_usuario']
            ]);

            $updateStatusInSsp = $medooBegin->update('ssp', [
                'cod_pstatus'   => (int)$insertStatusAtual
            ], [
                'cod_ssp'       => (int)$codigoSsp
            ]);

            $insertOrigemSsp = $medooBegin->insert('origem_ssp', [
                'cod_ssp'       => (int)$codigoSsp,
                'tipo_orissp'   => (int)1 // Preventiva
            ]);

            $insertLocalSsp = $medooBegin->insert('local_ssp', [
                'cod_ssp'           => (int)$codigoSsp,             // NOT NULL
                'cod_grupo'         => 21, // Edifica��es   (  int)$pmp['grupoSistema'],   // NOT NULL
                'cod_sistema'       => (int)$pmp['cod_sistema'],        // NOT NULL
                'cod_subsistema'    => (int)$pmp['cod_subsistema'],
                'cod_linha'         => (int)$pmp['linha'],          // NOT NULL
                'cod_trecho'        => 1// Alterar aqui               (int)$pmp['trecho'],  // Alterar aqui        // NOT NULL
                //'cod_ponto_notavel' => (int)$pmp['pontoNotavel'],
                //'km_inicial'        => (string)$pmp['kmInicial'],
                //'km_final'          => (string)$pmp['kmFinal'],
                //'posicao'           => (string)$pmp['posicao']
            ]);

            if (!empty($codigoSsp) && !empty($insertStatusAtual) && !empty($insertOrigemSsp) && !empty($insertLocalSsp)) {
//                //Mensagem para o alerta RealTime
//                $_SESSION['notificacao']['mensag'] = "Ssp n. {$codigoSsp} Aberta.";
//                $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$_SESSION['dadosSsp']['dadosUsuario']['usuario']}";
//                $_SESSION['notificacao']['nomeUsuario'] = $_SESSION['dadosSsp']['dadosUsuario']['usuario'];
//
//                $viewSsp = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$_SESSION['codSsp']]);
//                $viewSsp = $viewSsp[0];
//
//                //Tipo de a��o
//                $_SESSION['notificacao']['tipo']    = "Inserir";
//                $_SESSION['notificacao']['tabela']  = "Ssp";
//
//                //Informa��o para alimentar qualquer  da tabela Ssm
//                $_SESSION['notificacao']['numero']  = $_SESSION['codSsp'];
//                $_SESSION['notificacao']['status']  = $viewSsp["nome_status"];
//                $_SESSION['notificacao']['servico'] = $viewSsp["nome_servico"];
//
//                //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];

            } else {
                $_SESSION['feedback_banco'] = $medooBegin->error();
                return false;
            }
        });

        if(!$bol){
            $resultado = $this->medoo->query("SELECT setval('ssp_cod_ssp_seq', nextval('ssp_cod_ssp_seq')-2);")->fetchAll(PDO::FETCH_ASSOC);
        }
    }
}