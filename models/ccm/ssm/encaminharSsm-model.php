<?php

class EncaminharSsmModel extends MainModel{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario){

        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSsm = $_SESSION['dadosSsm'];

        $time = date('d-m-Y H:i:s', time());

        if($dadosSsm['statusAntigo'] == "Autorizada"){

            //######### Insert no status_ssm ###########
            $insertStatusAtual = $this->medoo->insert("status_ssm",[
                "cod_status"    => (int) 12, //Encaminhado
                "cod_ssm"       => (int) $dadosSsm['codigoSsm'],
                "data_status"   => $this->inverteData($time),
                "usuario"       => (int) $dadosUsuario['cod_usuario']
            ]);

            //######### Update SSM ####################
            $updateSsm = $this->medoo->update("ssm",["cod_mstatus" => (int) $insertStatusAtual],["cod_ssm" => (int) $dadosSsm['codigoSsm']]);

        }else{
            //####### Filtro e Afins ##############

            $codigoUsuario = $this->getCodUsuario($dadosSsm['responsavelSsm']);
            $codigoEquipe  = $this->getCodEquipe($dadosSsm['equipeSsm']);

            //######### Insert no status_ssm ###########
            //######### Insert no status_ssm ###########
            //######### Insert no status_ssm ###########

            $insertStatusAtual = $this->medoo->insert("status_ssm",[
                "cod_status"    =>(int) 12, //Encaminhado
                "cod_ssm"       =>(int) $dadosSsm['codigoSsm'],
                "data_status"   => $this->inverteData($time),
                "usuario"       =>(int) $dadosUsuario['cod_usuario']
            ]);


            //######### GET UNIDADE_EQUIPE #################
            //######### GET UNIDADE_EQUIPE #################
            //######### GET UNIDADE_EQUIPE #################

            $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
                "AND" =>[
                    "cod_equipe"        => (int) $codigoEquipe,
                    "cod_unidade"       => (int) $dadosSsm['localSsm']
                    ]
            ]);
            $unEquipe = $unEquipe[0];

            //######### Update SSM ####################
            //######### Update SSM ####################
            //######### Update SSM ####################

            $updateSsm = $this->medoo->update("ssm",[
                "cod_tipo_intervencao"  => (int) $dadosSsm['tipoIntervencao'],
                "cod_servico"           => (int) $dadosSsm['servicoSsm'],
                "cod_un_equipe"         => (int) $unEquipe,
                "cod_usuario"           => (int) $codigoUsuario,
                "cod_mstatus"           => (int) $insertStatusAtual,
                "nivel"                 => (string) $dadosSsm['nivelSsm'],
                "complemento"           => (string) $dadosSsm['complementoServicoSsm']
            ],[
                "cod_ssm"               => (int) $dadosSsm['codigoSsm']
            ]);

            //######### Insert/UPDATE no local_ssm ##########
            //######### Insert/UPDATE no local_ssm ##########
            //######### Insert/UPDATE no local_ssm ##########

            $haveLocal = $this->medoo->select("local_ssm", "cod_ssm",[
                "cod_ssm" => (int) $dadosSsm['codigoSsm']
            ]);

            if(!empty($haveLocal)){
                $localSsm = $this->medoo->update("local_ssm",[
                    "cod_linha"         => (int) $dadosSsm['linhaSsm'],
                    "cod_trecho"        => (int) $dadosSsm['trechoSsm'],
                    "cod_ponto_notavel" => (int) $dadosSsm['pontoNotavelSsm'],
                    "cod_grupo"         => (int) $dadosSsm['grupoSistemaSsm'],
                    "cod_sistema"       => (int) $dadosSsm['sistemaSsm'],
                    "cod_subsistema"    => (int)$dadosSsm['subSistemaSsm'],
                    "cod_via"           => (int) $dadosSsm['viaSsm'],
                    "km_inicial"        => (string) $dadosSsm['kmInicialSsm'],
                    "km_final"          => (string) $dadosSsm['kmFinalSsm'],
                    "posicao"           => (string) $dadosSsm['posicaoSsm'],
                    "complemento"       => (string) $dadosSsm['complementoLocalSsm']
                ],[
                    "cod_ssm"           => (int) $dadosSsm['codigoSsm'],
                ]);
            }else{
                $localSsm = $this->medoo->insert("local_ssm",[
                    "cod_ssm"           => (int) $dadosSsm['codigoSsm'],
                    "cod_linha"         => (int) $dadosSsm['linhaSsm'],
                    "cod_trecho"        => (int) $dadosSsm['trechoSsm'],
                    "cod_ponto_notavel" => (int) $dadosSsm['pontoNotavelSsm'],
                    "cod_grupo"         => (int) $dadosSsm['grupoSistemaSsm'],
                    "cod_sistema"       => (int) $dadosSsm['sistemaSsm'],
                    "cod_subsistema"    => (int)$dadosSsm['subSistemaSsm'],
                    "cod_via"           => (int) $dadosSsm['viaSsm'],
                    "km_inicial"        => (string) $dadosSsm['kmInicialSsm'],
                    "km_final"          => (string) $dadosSsm['kmFinalSsm'],
                    "posicao"           => (string) $dadosSsm['posicaoSsm'],
                    "complemento"       => (string) $dadosSsm['complementoLocalSsm']
                ]);
            }
        }

        $viewSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int) $dadosSsm['codigoSsm']]);
        $viewSsm = $viewSsm[0];

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = "Ssm n. {$dadosSsm['codigoSsm']} encaminhada para {$viewSsm['sigla_equipe']}<br />Por:  {$dadosUsuario['usuario']}";

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']                = "Encaminhar";
        $_SESSION['notificacao']['statusAntigo']        = $dadosSsm['statusAntigo'];
        $_SESSION['notificacao']['tabela']              = "Ssm";

        //Informa��o para alimentar qualquer  da tabela Ssm
        $_SESSION['notificacao']['numero']              = $dadosSsm['codigoSsm'];
        $_SESSION['notificacao']['numeroSaf']           = $viewSsm['cod_saf'];
        $_SESSION['notificacao']['dataAbertura']        = $this->parse_timestamp($viewSsm["data_abertura"]);
        $_SESSION['notificacao']['nivel']               = $dadosSsm['nivelSsm'];
        $_SESSION['notificacao']['encaminhamento']      = $this->parse_timestamp($viewSsm["data_status"]);

        $_SESSION['notificacao']['servico']             = $viewSsm["nome_servico"];
        $_SESSION['notificacao']['complementoServico']  = $viewSsm["complemento_servico"];

        $_SESSION['notificacao']['nomeEquipe']          = $viewSsm['nome_equipe'];
        $_SESSION['notificacao']['nomeUnidade']         = $viewSsm['nome_unidade'];
        $_SESSION['notificacao']['siglaEquipe']         = $viewSsm['sigla_equipe'];
        $_SESSION['notificacao']['siglaUnidade']        = $viewSsm['sigla_unidade'];

        $_SESSION['notificacao']['nomeLinha']           = $viewSsm['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']          = $viewSsm['descricao_trecho'];

        $_SESSION['notificacao']['subChannelEquipe']    = $unEquipe;
        $_SESSION['notificacao']['nomeUsuario']         = $dadosUsuario['usuario'];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}