<?php

/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 21/01/2016
 * Time: 12:09
 */
class alterarSsmModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSsm = $_SESSION['alterarSsm'];

        $codigoEquipe = $this->getCodEquipe($dadosSsm['equipeSsm']);

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
            "AND" =>[
                "cod_equipe"        => (int) $codigoEquipe,
                "cod_unidade"       => (int) $dadosSsm['localSsm']
            ]
        ]);
        $unEquipe = $unEquipe[0];


        $updateSsm = $this->medoo->update("ssm", [
            "cod_tipo_intervencao" => (int) $dadosSsm['tipoIntervencao'],
            "cod_servico"          => (int) $dadosSsm['servicoSsm'],
            "cod_un_equipe"        => (int) $unEquipe,
            "nivel"                => (string) $dadosSsm['nivelSsm'],
            "complemento"          => (string) $dadosSsm['complementoServicoSsm'],
            "cod_local_grupo"      => (int) $dadosSsm['localGrupo'],
            "cod_sublocal_grupo"   => (int) $dadosSsm['subLocalGrupo'],
            "cod_linha"         => (int) $dadosSsm['linhaSsm'],
            "cod_trecho"        => (int) $dadosSsm['trechoSsm'],
            "cod_ponto_notavel" => (int) $dadosSsm['pontoNotavelSsm'],
            "cod_grupo"         => (int) $dadosSsm['grupoSistemaSsm'],
            "cod_sistema"       => (int) $dadosSsm['sistemaSsm'],
            "cod_subsistema"    => (int) $dadosSsm['subSistemaSsm'],
            "cod_via"           => (int) $dadosSsm['viaSsm'],
            "km_inicial"        => (string) $dadosSsm['kmInicialSsm'],
            "km_final"          => (string) $dadosSsm['kmFinalSsm'],
            "posicao"           => (string) $dadosSsm['posicaoSsm'],
            "complemento_local"       => (string) $dadosSsm['complementoLocalSsm']
        ], [
            "cod_ssm" => (int) $dadosSsm['codigoSsm']
        ]);

        if($dadosSsm['grupoSistemaSsm'] == '22' || $dadosSsm['grupoSistemaSsm'] == '23' || $dadosSsm['grupoSistemaSsm'] == '26'){
            $this->medoo->update("material_rodante_ssm", [
                "cod_veiculo"   => (int)$dadosSsm['veiculoMrSsm'],
                "odometro"      => (double)$dadosSsm['odometroMrSsm'],
                "cod_grupo"     => (int)$dadosSsm['grupoSistemaSsm']
            ], [
                "cod_ssm" => (int) $dadosSsm['codigoSsm']
            ]);
        }

        $viewSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int) $dadosSsm['codigoSsm']]);
        $viewSsm = $viewSsm[0];

        // Salvar altera��es
        $time = date('d-m-Y H:i:s', time());
        $this->medoo->insert('alteracao_ssm',[
            "cod_ssm"           => (int) $dadosSsm['codigoSsm'],
            "data_alteracao"    => $time,
            "cod_usuario"       => (int) $dadosUsuario['cod_usuario']
        ]);


        //Tipo de a��o
        $_SESSION['notificacao']['tipo']   = "Alterar";
        $_SESSION['notificacao']['tabela'] = "Ssm";

        //Informa��o para alimentar qualquer uma das tabelas Ssm
        //Informa��o para alimentar qualquer uma das tabelas Ssm
        //Informa��o para alimentar qualquer uma das tabelas Ssm
        $_SESSION['notificacao']['numero']              = $dadosSsm['codigoSsm'];
        $_SESSION['notificacao']['numeroSaf']           = $viewSsm['cod_saf'];
        $_SESSION['notificacao']['dataAbertura']        = $this->parse_timestamp($viewSsm["data_abertura"]);
        $_SESSION['notificacao']['nivel']               = $dadosSsm['nivelSsm'];
        $_SESSION['notificacao']['encaminhamento']      = $this->parse_timestamp($viewSsm["data_status"]);

        $_SESSION['notificacao']['status']              = $viewSsm["nome_status"];
        $_SESSION['notificacao']['motivo']              = $viewSsm["descricao_status"];

        $_SESSION['notificacao']['sistema']             = $viewSsm["nome_sistema"];
        $_SESSION['notificacao']['veiculo']             = $viewSsm["nome_veiculo"];

        $_SESSION['notificacao']['servico']             = $viewSsm["nome_servico"];
        $_SESSION['notificacao']['complementoServico']  = $viewSsm["complemento_servico"];

        $_SESSION['notificacao']['nomeEquipe']          = $viewSsm['nome_equipe'];
        $_SESSION['notificacao']['nomeUnidade']         = $viewSsm['nome_unidade'];
        $_SESSION['notificacao']['siglaEquipe']         = $viewSsm['sigla_equipe'];
        $_SESSION['notificacao']['siglaUnidade']        = $viewSsm['sigla_unidade'];

        $_SESSION['notificacao']['nomeLinha']           = $viewSsm['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']          = $viewSsm['descricao_trecho'];

        $_SESSION['notificacao']['nomeUsuario']         = $dadosUsuario['usuario'];

        $_SESSION['notificacao']['avaria']              = $viewSsm['nome_avaria'];

        if ($viewSsm["nome_status"] != "Aberta"){
            $_SESSION['notificacao']['subChannelEquipe'] = $unEquipe;
        }
    }
}