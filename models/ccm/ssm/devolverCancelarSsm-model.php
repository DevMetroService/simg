<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 29/06/2015
 * Time: 08:11
 */

class DevolverCancelarSsmModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSsm = $_SESSION['dados'];

        //############## Status_SSM #####################
        //############## Status_SSM #####################
        //############## Status_SSM #####################

        $time = date('d-m-Y H:i:s', time());

        $View = $this->medoo->select("v_ssm", ["nome_status","cod_saf"], ["cod_ssm" => (int) $dadosSsm['codigo']]);
        $View = $View[0];

        $statusAntigo   = $View["nome_status"];
        $codSaf         = $View["cod_saf"];

        //Se a a��o for de devolu��o...
        if ($dadosSsm['acaoModal'] == "devolver") {
            $motivo = $dadosSsm['motivoAcao'] . ' => Devolvido por: ' . $dadosUsuario['usuario'] . ' - '.$time;

            $this->alterarStatusSsm($dadosSsm['codigo'], 6, $time, $dadosUsuario, $motivo);

            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Ssm n. {$dadosSsm['codigo']} devolvida.";
            $_SESSION['notificacao']['tipo'] = "Devolver";
        }
        else {
            if($dadosSsm['acaoModal'] == "encerrar"){
                $statusSSM = 35; // Aguardando Valida��o
                $statusSAF = 37; // Aguardando Valida��o

                $motivo = $dadosSsm['motivoAcao'] . ' => Enviado para valida��o por: ' . $dadosUsuario['usuario'] . ' - '.$time;
                $motivoSaf = $motivo;

                //Mensagem para o alerta RealTime
                $_SESSION['notificacao']['mensag'] = "Ssm n. {$dadosSsm['codigo']} Aguardando Valida��o.";
            }
            else {
                if ($dadosSsm['motivo'] == "2" || $dadosSsm['motivo'] == "3") {

                    if ($dadosSsm['motivo'] == "2") {
                        $tipossp = "Programa��o";
                    } else {
                        $tipossp = "Servi�o";
                    }

                    $statusSSM = 26;  // Programada
                    $statusSAF = 27;  // Programada

                    $insertSmp = $this->medoo->insert("ssm_ssp_cancelada",
                        [
                            "cod_ssm" => (int)$dadosSsm['codigo'],
                            "cod_ssp" => (int)$dadosSsm['nSsp']
                        ]
                    );

                    $motivo = "{$dadosSsm['motivoAcao']} => Programada por: {$dadosUsuario['usuario']} Referente a SSP {$tipossp} n. {$dadosSsm['nSsp']} - {$time}";
                    $motivoSaf = $motivo;

                    //Mensagem para o alerta RealTime
                    $_SESSION['notificacao']['mensag'] = "Ssm n.{$dadosSsm['codigo']} tratada como {$tipossp}.";
                } else {
                    $statusSSM = 7; // Cancelada
                    $statusSAF = 2; // Cancelada

                    $motivo = $dadosSsm['motivoAcao'] . ' => Cancelada por: ' . $dadosUsuario['usuario'] . ' - ' . $time;
                    $motivoSaf = "Ssm descontinuada.  Motivo: " . $motivo;

                    //Mensagem para o alerta RealTime
                    $_SESSION['notificacao']['mensag'] = "Ssm n.{$dadosSsm['codigo']} cancelada.";
                }
            }


            $this->alterarStatusSsm($dadosSsm['codigo'], $statusSSM, $time, $dadosUsuario, $motivo);

            $_SESSION['notificacao']['tipo'] = "Cancelar";

            //Verifica a SAF para encerramento.
            $this->encerrarSaf($codSaf, $time, $dadosUsuario, $statusSAF, $motivoSaf);

            if($this->verificarSsm($codSaf)){
                //Mensagem para o alerta RealTime
                if($dadosSsm['acaoModal'] == "encerrar")
                    $_SESSION['notificacao']['mensag'] .= "<br />Saf n. {$codSaf} encerrada.";
                else if($dadosSsm['motivo'] == "2")
                    $_SESSION['notificacao']['mensag'] .= "<br />Saf n. {$codSaf} tratada como programa��o.";
                else if($dadosSsm['motivo'] == "3")
                    $_SESSION['notificacao']['mensag'] .= "<br />Saf n. {$codSaf} tratada como servi�o.";
                else
                    $_SESSION['notificacao']['mensag'] .= "<br />Saf n. {$codSaf} encerrada, servi�o descontinuado.";
            }
        }

        $_SESSION['notificacao']['nomeUsuario'] = $dadosUsuario['usuario'];
        $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";

        $viewSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int) $dadosSsm['codigo']]);
        $viewSsm = $viewSsm[0];

        //Tipo de a��o
        $_SESSION['notificacao']['tabela']          = "Ssm";
        $_SESSION['notificacao']['statusAntigo']    = $statusAntigo;

        //Informa��o para alimentar qualquer  da tabela Ssm
        $_SESSION['notificacao']['numero']          = $dadosSsm['codigo'];
        $_SESSION['notificacao']['nivel']           = $viewSsm['nivel'];

        if ($dadosSsm['acaoModal'] == "devolver"){
            $_SESSION['notificacao']['numeroSaf']    = $viewSsm["cod_saf"];
            $_SESSION['notificacao']['dataAbertura'] = $this->parse_timestamp($viewSsm["data_abertura"]);
            $_SESSION['notificacao']['status']       = "Devolvida";
            $_SESSION['notificacao']['motivo']       = $viewSsm["descricao_status"];
        }

        if ($statusAntigo != "Aberta"){
            $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
                "AND" =>[
                    "cod_equipe"  => (int) $viewSsm['cod_equipe'],
                    "cod_unidade" => (int) $viewSsm['cod_unidade']
                ]
            ]);
            $unEquipe = $unEquipe[0];

            $_SESSION['notificacao']['subChannelEquipe'] = $unEquipe;
        }

        unset($_SESSION['dados']);

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}
