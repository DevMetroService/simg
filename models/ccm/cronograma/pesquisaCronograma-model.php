<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 06/02/2017
 * Time: 10:07
 */
class PesquisaCronogramaModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_SESSION['dadosPesquisaCronograma'];

        //Verifica se h� valores e acrescenta na variavel where

        $grupo = $dadosPesquisa['grupoPesquisaCronograma'];
        $sql = "";

        switch ($grupo) {
            //Edifica��es
            case '21':

                if (!empty($dadosPesquisa['numeroPesquisaCronograma'])) {
                    $where[] = "ce.cod_cronograma_pmp = {$dadosPesquisa['numeroPesquisaCronograma']}";
                }

                //#########Dados Gerais

                if (!empty($dadosPesquisa['sistemaPesquisaCronograma'])) {
                    $where[] = "sis.cod_sistema = {$dadosPesquisa['sistemaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['subsistemaPesquisa'])) {
                    $where[] = "sub.cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
                }

                if (!empty($dadosPesquisa['servicoPesquisaCronograma'])){
                    $where[] = "sp.cod_servico_pmp = {$dadosPesquisa['servicoPesquisaCronograma']}";
                }

                //#########Datas
                if (!empty($dadosPesquisa['statusPesquisaCronograma'])) {
                    $where[] = "scpe.cod_status = '{$dadosPesquisa['statusPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['quinzenaPesquisaCronograma'])) {
                    $where[] = "c.quinzena = '{$dadosPesquisa['quinzenaPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['mesPesquisaCronograma'])) {
                    $where[] = "c.mes = '{$dadosPesquisa['mesPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['anoPesquisaCronograma'])) {
                    $where[] = "c.ano = '{$dadosPesquisa['anoPesquisaCronograma']}'";
                }

                //###########Local
                if (!empty($dadosPesquisa['linhaPesquisaCronograma'])) {
                    $where[] = "li.cod_linha = {$dadosPesquisa['linhaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['localPesquisaCronograma'])) {
                    $where[] = "pe.cod_local = {$dadosPesquisa['localPesquisaCronograma']}";
                }

                $sql = 'SELECT
                          ce.cod_cronograma_pmp    AS cod_cronograma,
                          c.ano                    AS ano,
                          c.data_abertura          AS data_abertura,
                          c.mes                    AS mes,
                          c.quinzena               AS quinzena,
                          scpe.cod_status          AS cod_status,
                          s.nome_status            AS status,
                          data_status              AS data_status,
                          pe.cod_local             AS cod_local,
                          lo.nome_local            AS local,
                          li.cod_linha             AS cod_linha,
                          li.nome_linha            AS linha,
                          sp.nome_servico_pmp      AS servico,
                          sis.nome_sistema         AS sistema,
                          sub.nome_subsistema      AS subsistema
                        FROM cronograma c
                          INNER JOIN cronograma_pmp ce ON c.cod_cronograma = ce.cod_cronograma
                          INNER JOIN status_cronograma_pmp scpe ON ce.cod_status_cronograma_pmp = scpe.cod_status_cronograma_pmp
                          INNER JOIN status s ON scpe.cod_status = s.cod_status
                          INNER JOIN pmp p ON ce.cod_pmp = p.cod_pmp
                          INNER JOIN pmp_edificacao pe ON p.cod_pmp = pe.cod_pmp
                          INNER JOIN local lo ON pe.cod_local = lo.cod_local
                          INNER JOIN linha li ON lo.cod_linha = li.cod_linha
                          INNER JOIN servico_pmp_periodicidade spp ON p.cod_servico_pmp_periodicidade = spp.cod_servico_pmp_periodicidade
                          INNER JOIN servico_pmp_sub_sistema spss ON spp.cod_servico_pmp_sub_sistema = spss.cod_servico_pmp_sub_sistema
                          INNER JOIN servico_pmp sp ON spss.cod_servico_pmp = sp.cod_servico_pmp
                          INNER JOIN sub_sistema ss ON spss.cod_sub_sistema = ss.cod_sub_sistema
                          INNER JOIN sistema sis ON ss.cod_sistema = sis.cod_sistema
                          INNER JOIN subsistema sub ON ss.cod_subsistema = sub.cod_subsistema';
                break;
            //Via Permanente
            case '24':

                if (!empty($dadosPesquisa['numeroPesquisaCronograma'])) {
                    $where[] = "ce.cod_cronograma_pmp = {$dadosPesquisa['numeroPesquisaCronograma']}";
                }

                //#########Dados Gerais

                if (!empty($dadosPesquisa['sistemaPesquisaCronograma'])) {
                    $where[] = "sis.cod_sistema = {$dadosPesquisa['sistemaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['subsistemaPesquisa'])) {
                    $where[] = "sub.cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
                }

                if (!empty($dadosPesquisa['servicoPesquisaCronograma'])) {
                    $where[] = "sp.cod_servico_pmp = {$dadosPesquisa['servicoPesquisaCronograma']}";
                }

                //#########Datas
                if (!empty($dadosPesquisa['statusPesquisaCronograma'])) {
                    $where[] = "scpe.cod_status = '{$dadosPesquisa['statusPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['quinzenaPesquisaCronograma'])) {
                    $where[] = "c.quinzena = '{$dadosPesquisa['quinzenaPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['mesPesquisaCronograma'])) {
                    $where[] = "c.mes = '{$dadosPesquisa['mesPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['anoPesquisaCronograma'])) {
                    $where[] = "c.ano = '{$dadosPesquisa['anoPesquisaCronograma']}'";
                }

                //###########Local
                if (!empty($dadosPesquisa['linhaPesquisaCronograma'])) {
                    $where[] = "li.cod_linha = {$dadosPesquisa['linhaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['trechoPesquisaCronograma'])) {
                    $where[] = "tre.cod_trecho = {$dadosPesquisa['trechoPesquisaCronograma']}";
                }

                $sql = 'SELECT
                          ce.cod_cronograma_pmp    AS cod_cronograma,
                          c.ano                    AS ano,
                          c.data_abertura          AS data_abertura,
                          c.mes                    AS mes,
                          c.quinzena               AS quinzena,
                          scpe.cod_status          AS cod_status,
                          s.nome_status            AS status,
                          data_status              AS data_status,
                          li.cod_linha             AS cod_linha,
                          li.nome_linha            AS linha,
                          tre.cod_trecho           AS cod_trecho,
                          tre.nome_trecho          AS trecho,
                          sp.nome_servico_pmp      AS servico,
                          sis.nome_sistema         AS sistema,
                          sub.nome_subsistema      AS subsistema
                        FROM cronograma c
                          INNER JOIN cronograma_pmp ce ON c.cod_cronograma = ce.cod_cronograma
                          INNER JOIN status_cronograma_pmp scpe ON ce.cod_status_cronograma_pmp = scpe.cod_status_cronograma_pmp
                          INNER JOIN status s ON scpe.cod_status = s.cod_status
                          INNER JOIN pmp p ON ce.cod_pmp = p.cod_pmp
                          INNER JOIN pmp_via_permanente pe ON p.cod_pmp = pe.cod_pmp
                          INNER JOIN via v ON pe.cod_via = v.cod_via
                          INNER JOIN linha li ON v.cod_linha = li.cod_linha
                          INNER JOIN trecho tre ON li.cod_linha = tre.cod_linha
                          INNER JOIN servico_pmp_periodicidade spp ON p.cod_servico_pmp_periodicidade = spp.cod_servico_pmp_periodicidade
                          INNER JOIN servico_pmp_sub_sistema spss ON spp.cod_servico_pmp_sub_sistema = spss.cod_servico_pmp_sub_sistema
                          INNER JOIN servico_pmp sp ON spss.cod_servico_pmp = sp.cod_servico_pmp
                          INNER JOIN sub_sistema ss ON spss.cod_sub_sistema = ss.cod_sub_sistema
                          INNER JOIN sistema sis ON ss.cod_sistema = sis.cod_sistema
                          INNER JOIN subsistema sub ON ss.cod_subsistema = sub.cod_subsistema';
                break;
            //Subesta��o
            case '25':

                if (!empty($dadosPesquisa['numeroPesquisaCronograma'])) {
                    $where[] = "ce.cod_cronograma_pmp = {$dadosPesquisa['numeroPesquisaCronograma']}";
                }

                //#########Dados Gerais

                if (!empty($dadosPesquisa['sistemaPesquisaCronograma'])) {
                    $where[] = "sis.cod_sistema = {$dadosPesquisa['sistemaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['subsistemaPesquisa'])) {
                    $where[] = "sub.cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
                }

                if (!empty($dadosPesquisa['servicoPesquisaCronograma'])) {
                    $where[] = "sp.cod_servico_pmp = {$dadosPesquisa['servicoPesquisaCronograma']}";
                }

                //#########Datas
                if (!empty($dadosPesquisa['statusPesquisaCronograma'])) {
                    $where[] = "scpe.cod_status = '{$dadosPesquisa['statusPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['quinzenaPesquisaCronograma'])) {
                    $where[] = "c.quinzena = '{$dadosPesquisa['quinzenaPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['mesPesquisaCronograma'])) {
                    $where[] = "c.mes = '{$dadosPesquisa['mesPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['anoPesquisaCronograma'])) {
                    $where[] = "c.ano = '{$dadosPesquisa['anoPesquisaCronograma']}'";
                }

                //###########Local
                if (!empty($dadosPesquisa['linhaPesquisaCronograma'])) {
                    $where[] = "li.cod_linha = {$dadosPesquisa['linhaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['localPesquisaCronograma'])) {
                    $where[] = "pe.cod_local = {$dadosPesquisa['localPesquisaCronograma']}";
                }

                $sql = 'SELECT
                          ce.cod_cronograma_pmp    AS cod_cronograma,
                          c.ano                    AS ano,
                          c.data_abertura          AS data_abertura,
                          c.mes                    AS mes,
                          c.quinzena               AS quinzena,
                          scpe.cod_status          AS cod_status,
                          s.nome_status            AS status,
                          data_status              AS data_status,
                          pe.cod_local             AS cod_local,
                          lo.nome_local            AS local,
                          li.cod_linha             AS cod_linha,
                          li.nome_linha            AS linha,
                          sp.cod_servico_pmp       AS cod_servico,
                          sp.nome_servico_pmp      AS servico,
                          sis.cod_sistema          AS cod_sistema,
                          sis.nome_sistema         AS sistema,
                          sub.cod_subsistema       AS cod_subsistema,
                          sub.nome_subsistema      AS subsistema
                        FROM cronograma c
                          INNER JOIN cronograma_pmp ce ON c.cod_cronograma = ce.cod_cronograma
                          INNER JOIN status_cronograma_pmp scpe ON ce.cod_status_cronograma_pmp = scpe.cod_status_cronograma_pmp
                          INNER JOIN status s ON scpe.cod_status = s.cod_status
                          INNER JOIN pmp p ON ce.cod_pmp = p.cod_pmp
                          INNER JOIN pmp_subestacao pe ON p.cod_pmp = pe.cod_pmp
                          INNER JOIN local lo ON pe.cod_local = lo.cod_local
                          INNER JOIN linha li ON lo.cod_linha = li.cod_linha
                          INNER JOIN servico_pmp_periodicidade spp ON p.cod_servico_pmp_periodicidade = spp.cod_servico_pmp_periodicidade
                          INNER JOIN servico_pmp_sub_sistema spss ON spp.cod_servico_pmp_sub_sistema = spss.cod_servico_pmp_sub_sistema
                          INNER JOIN servico_pmp sp ON spss.cod_servico_pmp = sp.cod_servico_pmp
                          INNER JOIN sub_sistema ss ON spss.cod_sub_sistema = ss.cod_sub_sistema
                          INNER JOIN sistema sis ON ss.cod_sistema = sis.cod_sistema
                          INNER JOIN subsistema sub ON ss.cod_subsistema = sub.cod_subsistema';
                break;
            //Rede A�rea
            case '20':

                if (!empty($dadosPesquisa['numeroPesquisaCronograma'])) {
                    $where[] = "ce.cod_cronograma_pmp = {$dadosPesquisa['numeroPesquisaCronograma']}";
                }

                //#########Dados Gerais

                if (!empty($dadosPesquisa['sistemaPesquisaCronograma'])) {
                    $where[] = "sis.cod_sistema = {$dadosPesquisa['sistemaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['subsistemaPesquisa'])) {
                    $where[] = "sub.cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
                }

                if (!empty($dadosPesquisa['servicoPesquisaCronograma'])) {
                    $where[] = "sp.cod_servico_pmp = {$dadosPesquisa['servicoPesquisaCronograma']}";
                }

                //#########Datas
                if (!empty($dadosPesquisa['statusPesquisaCronograma'])) {
                    $where[] = "scpe.cod_status = '{$dadosPesquisa['statusPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['quinzenaPesquisaCronograma'])) {
                    $where[] = "c.quinzena = '{$dadosPesquisa['quinzenaPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['mesPesquisaCronograma'])) {
                    $where[] = "c.mes = '{$dadosPesquisa['mesPesquisaCronograma']}'";
                }

                if (!empty($dadosPesquisa['anoPesquisaCronograma'])) {
                    $where[] = "c.ano = '{$dadosPesquisa['anoPesquisaCronograma']}'";
                }

                //###########Local
                if (!empty($dadosPesquisa['linhaPesquisaCronograma'])) {
                    $where[] = "li.cod_linha = {$dadosPesquisa['linhaPesquisaCronograma']}";
                }

                if (!empty($dadosPesquisa['estacaoPesquisaCronograma'])) {
                    $where[] = "esta.cod_estacao = {$dadosPesquisa['estacaoPesquisaCronograma']}";
                }

                $sql = 'SELECT
                          ce.cod_cronograma_pmp    AS cod_cronograma,
                          c.ano                    AS ano,
                          c.data_abertura          AS data_abertura,
                          c.mes                    AS mes,
                          c.quinzena               AS quinzena,
                          scpe.cod_status          AS cod_status,
                          s.nome_status            AS status,
                          data_status              AS data_status,
                          li.cod_linha             AS cod_linha,
                          li.nome_linha            AS linha,
                          esta.cod_estacao         AS cod_estacao,
                          esta.nome_estacao        AS estacao,
                          sp.nome_servico_pmp      AS servico,
                          sis.nome_sistema         AS sistema,
                          sub.nome_subsistema      AS subsistema
                        FROM cronograma c
                          INNER JOIN cronograma_pmp ce ON c.cod_cronograma = ce.cod_cronograma
                          INNER JOIN status_cronograma_pmp scpe ON ce.cod_status_cronograma_pmp = scpe.cod_status_cronograma_pmp
                          INNER JOIN status s ON scpe.cod_status = s.cod_status
                          INNER JOIN pmp p ON ce.cod_pmp = p.cod_pmp
                          INNER JOIN pmp_rede_aerea pe ON p.cod_pmp = pe.cod_pmp
                          INNER JOIN via v ON pe.cod_via = v.cod_via
                          INNER JOIN linha li ON v.cod_linha = li.cod_linha
                          INNER JOIN estacao esta ON li.cod_linha = esta.cod_linha
                          INNER JOIN servico_pmp_periodicidade spp ON p.cod_servico_pmp_periodicidade = spp.cod_servico_pmp_periodicidade
                          INNER JOIN servico_pmp_sub_sistema spss ON spp.cod_servico_pmp_sub_sistema = spss.cod_servico_pmp_sub_sistema
                          INNER JOIN servico_pmp sp ON spss.cod_servico_pmp = sp.cod_servico_pmp
                          INNER JOIN sub_sistema ss ON spss.cod_sub_sistema = ss.cod_sub_sistema
                          INNER JOIN sistema sis ON ss.cod_sistema = sis.cod_sistema
                          INNER JOIN subsistema sub ON ss.cod_subsistema = sub.cod_subsistema';
                break;
        }

        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

//        var_dump($dadosPesquisa);
//        echo($sql);

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $_SESSION['resultadoPesquisaCronograma'] = (boolval(false));
        } else {
            $_SESSION['resultadoPesquisaCronograma'] = $resultado;
        }
    }
}