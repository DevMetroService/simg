<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/06/2015
 * Time: 15:44
 */
class gerarOspModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSsp = $_SESSION['gerarOsp'];

        $time = date('d-m-Y H:i:s', time());

        $hasSsp = $this->medoo->select("v_ssp", "*",["cod_ssp" => $dadosSsp['codigoSsp']]);
        $hasSsp = $hasSsp[0];

        if($hasSsp['nome_status'] == "Pendente"){
            //Update status da Ssp
            $insertNovoStatusSsp = $this->medoo->insert("status_ssp", [
                "cod_ssp"       => (int)$dadosSsp['codigoSsp'],
                "data_status"   => $this->inverteData($time),
                "cod_status"    => (int)21, // Autorizada
                "usuario"       => (int)$dadosUsuario['cod_usuario']
            ]);

            $updateSsp = $this->medoo->update("ssp", [
                "cod_pstatus" => (int)$insertNovoStatusSsp
            ], [
                "cod_ssp" => (int)$dadosSsp['codigoSsp']
            ]);

            //######################## Gerar Osp ######################
            //######################## Gerar Osp ######################
            //######################## Gerar Osp ######################
            $insertOsp = $this->medoo->insert("osp", [
                "cod_ssp"                   => (int)$dadosSsp['codigoSsp'],
                "grupo_atuado"              => (int)$hasSsp['cod_grupo'],
                "sistema_atuado"            => (int)$hasSsp['cod_sistema'],
                "subsistema_atuado"         => (int)$hasSsp['cod_subsistema'],
                "trecho_atuado"             => (int)$hasSsp['cod_trecho'],
                "cod_linha_atuado"          => (int)$hasSsp['cod_linha'],
                "cod_ponto_notavel_atuado"  => (int)$hasSsp['cod_ponto_notavel'],
                "cod_via"                   => (int)$hasSsp['cod_via'],
                "complemento"               => (string)$hasSsp['complemento_local'],
                "posicao_atuado"            => (string)$hasSsp['posicao'],
                "km_inicial"                => (string)$hasSsp['km_inicial'],
                "km_final"                  => (string)$hasSsp['km_final'],
                "data_abertura"             => (string)$time
            ]);

            if($hasSsp['cod_grupo'] == 22 || $hasSsp['cod_grupo'] == 23 || $hasSsp['cod_grupo'] == 26) {
                $insertOsmMr = $this->medoo->insert("material_rodante_osp", [
                    "cod_osp" => (int)$insertOsp,
                    "cod_veiculo" => (int)$hasSsp['cod_veiculo'],
                    "cod_carro" => (int)$hasSsp['cod_carro'],
                    "odometro" => (int)$hasSsp['odometro'],
                    "cod_prefixo" => (int)$hasSsp['cod_prefixo'],
                    "cod_grupo" => (int)$hasSsp['cod_grupo']
                ]);
            }

            // ####################### Insert Osp_SERVICO ###########################
            // ####################### Insert Osp_SERVICO ###########################
            // ####################### Insert Osp_SERVICO ###########################

            $insertServicoOsp = $this->medoo->insert("osp_servico", [
                "cod_servico"   => (int)$hasSsp['cod_servico'],
                "cod_osp"       => (int)$insertOsp,
                "complemento"   => (string)$hasSsp['complemento']
            ]);

            //######################## Inserir Status Osp ######################
            //######################## Inserir Status Osp ######################
            //######################## Inserir Status Osp ######################

            $insertStatusOsp = $this->medoo->insert("status_osp", [
                "cod_osp"       => (int)$insertOsp,
                "cod_status"    => (int)10, // Execu��o
                "data_status"   => $this->inverteData($time),
                "usuario"       => (int)$dadosUsuario['cod_usuario']
            ]);

            //######################## Update Osp c/ ultimo status ######################
            $updateOspStatus = $this->medoo->update("osp", [
                "cod_ospstatus" => (int)$insertStatusOsp
            ], [
                "cod_osp"       => (int)$insertOsp
            ]);

        }else {
            $codigoEquipe   = $this->getCodEquipe($dadosSsp['equipeSsp']);
            $codigoUsuario  = $this->getCodUsuario($dadosSsp['responsavelSsp']);

            //Update status da Ssp
            $insertNovoStatusSsp = $this->medoo->insert("status_ssp", [
                "cod_ssp"       => (int) $dadosSsp['codigoSsp'],
                "data_status"   => $this->inverteData($time),
                "cod_status"    => (int) 21, // Autorizada
                "usuario"       => (int) $dadosUsuario['cod_usuario']
            ]);

            $updateSsp = $this->medoo->update("ssp", [
                "cod_pstatus"   => (int) $insertNovoStatusSsp
            ], [
                "cod_ssp"       => (int) $dadosSsp['codigoSsp']
            ]);

            //######### GET UNIDADE_EQUIPE #################
            //######### GET UNIDADE_EQUIPE #################
            //######### GET UNIDADE_EQUIPE #################

            $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe", [
                "AND" => [
                    "cod_equipe"    => (int) $codigoEquipe,
                    "cod_unidade"   => (int) $dadosSsp['localSsp']
                ]
            ]);
            $unEquipe = $unEquipe[0];

            //######### Update Ssp ####################
            //######### Update Ssp ####################
            //######### Update Ssp ####################

            $updateSsp = $this->medoo->update("ssp", [
                'cod_servico'                   => (int) $dadosSsp['servicoSsp'],
                'cod_tipo_intervencao'          => (int) $dadosSsp['tipoIntervencao'],
                'complemento'                   => (string) $dadosSsp['complementoServicoSsp'],
                'data_programada'               => (string) $dadosSsp['dataHoraSsp'],
                'dias_servico'                  => (int) $dadosSsp['diasServico'],
                'solicitacao_acesso'            => (string) $dadosSsp['numeroSolicitacaoAcesso'],
                'matricula'                     =>(int) $dadosSsp['matriculaSolicitante'],
                'nome'                          =>(string) $dadosSsp['nomeSolicitante'],
                'cpf'                           =>(string) $dadosSsp['cpfSolicitante'],
                'contato'                       =>(string) $dadosSsp['contatoSolicitante'],
                "cod_un_equipe"                 => (int) $unEquipe,
                "cod_usuario"                   => (int) $codigoUsuario,
                "cod_pstatus"                   => (int) $insertNovoStatusSsp,
                "cod_linha"         => (int) $dadosSsp['linhaSsp'],
                "cod_trecho"        => (int) $dadosSsp['trechoSsp'],
                "cod_ponto_notavel" => (int) $dadosSsp['pontoNotavelSsp'],
                "cod_grupo"         => (int) $dadosSsp['grupoSistemaSsp'],
                "cod_sistema"       => (int) $dadosSsp['sistemaSsp'],
                "cod_subsistema"    => (int) $dadosSsp['subSistemaSsp'],
                "cod_via"           => (int) $dadosSsp['viaSsp'],
                "km_inicial"        => (string) $dadosSsp['kmInicialSsp'],
                "km_final"          => (string) $dadosSsp['kmFinalSsp'],
                "posicao"           => (string) $dadosSsp['posicaoSsp'],
                "complemento_local"       => (string) $dadosSsp['complementoLocalSsp']
            ],[
                "cod_ssp"                       => (int) $dadosSsp['codigoSsp']
            ]);

            //######################## Gerar Osp ######################
            //######################## Gerar Osp ######################
            //######################## Gerar Osp ######################
            $insertOsp = $this->medoo->insert("osp", [
                "cod_ssp"                   => (int) $dadosSsp['codigoSsp'],
                "grupo_atuado"              => (int) $dadosSsp['grupoSistemaSsp'],
                "sistema_atuado"            => (int) $dadosSsp['sistemaSsp'],
                "subsistema_atuado"         => (int) $dadosSsp['subSistemaSsp'],
                "trecho_atuado"             => (int) $dadosSsp['trechoSsp'],
                "cod_linha_atuado"          => (int) $dadosSsp['linhaSsp'],
                "cod_ponto_notavel_atuado"  => (int) $dadosSsp['pontoNotavelSsp'],
                "cod_via"                   => (int) $dadosSsp['viaSsp'],
                "complemento"               => (string) $dadosSsp['complementoLocalSsp'],
                "posicao_atuado"            => (string) $dadosSsp['posicaoSsp'],
                "km_inicial"                => (string) $dadosSsp['kmInicialSsp'],
                "km_final"                  => (string) $dadosSsp['kmFinalSsp'],
                "data_abertura"             => (string) $time
            ]);
            // ####################### Insert Osp_SERVICO ###########################
            // ####################### Insert Osp_SERVICO ###########################
            // ####################### Insert Osp_SERVICO ###########################

            $insertServicoOsp = $this->medoo->insert("osp_servico", [
                "cod_servico"   => (int) $dadosSsp['servicoSsp'],
                "cod_osp"       => (int) $insertOsp,
                "complemento"   => (string) $dadosSsp['complementoServicoSsp']
            ]);

            //######################## Inserir Material Rodante Osp ######################
            //######################## Inserir Material Rodante Osp ######################
            //######################## Inserir Material Rodante Osp ######################
            if($dadosSsp['grupoSistemaSsp'] == 22 || $dadosSsp['grupoSistemaSsp'] == 23 || $dadosSsp['grupoSistemaSsp'] == 26) {
                $insertOsmMr = $this->medoo->insert("material_rodante_osp", [
                    "cod_osp" => (int)$insertOsp,
                    "cod_veiculo" => (int)$dadosSsp['veiculoMrSsp'],
                    "cod_carro" => (int)$dadosSsp['carroMrSsp'],
                    "odometro" => (int)$dadosSsp['odometroMrSsp'],
                    "cod_prefixo" => (int)$dadosSsp['prefixoMrSsp'],
                    "cod_grupo" => (int)$dadosSsp['grupoSistemaSsp']
                ]);
            }

            //######################## Inserir Status Osp ######################
            //######################## Inserir Status Osp ######################
            //######################## Inserir Status Osp ######################

            $insertStatusOsp = $this->medoo->insert("status_osp", [
                "cod_osp"       => (int) $insertOsp,
                "cod_status"    => (int) 10, // Execu��o
                "data_status"   => $this->inverteData($time),
                "usuario"       => (int) $dadosUsuario['cod_usuario']
            ]);

            //######################## Update Osp c/ ultimo status ######################
            $updateOspStatus = $this->medoo->update("osp", [
                "cod_ospstatus" => (int) $insertStatusOsp
            ], [
                "cod_osp"       => (int) $insertOsp
            ]);
        }

        unset($_SESSION['gerarOsp']);

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = "Ssp n.{$dadosSsp['codigoSsp']} autorizada.
                                        <br />Osp n.{$insertOsp} aberta.
                                        <br />Por:  {$dadosUsuario['usuario']}.";


        $selectVSsp = $this->medoo->select('v_ssp', "*", ['cod_ssp' => (int) $dadosSsp['codigoSsp']]);
        $selectVSsp = $selectVSsp[0];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']             = "Aberta";
        $_SESSION['notificacao']['tabela']           = "Osp";
        $_SESSION['notificacao']['statusAntigo']     = $hasSsp['nome_status'];

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero']           = $insertOsp;
        $_SESSION['notificacao']['numeroSsp']        = $dadosSsp['codigoSsp'];
        $_SESSION['notificacao']['dataAbertura']     = $time;

        $_SESSION['notificacao']['veiculo']       = $selectVSsp['nome_veiculo'];

        $_SESSION['notificacao']['nomeEquipe']       = $selectVSsp['nome_equipe'];
        $_SESSION['notificacao']['nomeUnidade']      = $selectVSsp['nome_unidade'];
        $_SESSION['notificacao']['siglaEquipe']      = $selectVSsp['sigla_equipe'];
        $_SESSION['notificacao']['siglaUnidade']     = $selectVSsp['sigla_unidade'];

        $_SESSION['notificacao']['nomeLinha']        = $selectVSsp['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']       = $selectVSsp['descricao_trecho'];

        $_SESSION['notificacao']['subChannelEquipe'] = $selectVSsp['cod_un_equipe'];
        $_SESSION['notificacao']['nomeUsuario']      = $dadosUsuario['usuario'];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}
