<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/08/2015
 * Time: 14:29
 */
class PesquisaOsmModel extends MainModel
{
    private $dadosReturn;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_POST;

        $join = '';

        //Verifica se h� valores e acrescenta na variavel where

        //#########Dados Gerais
        if (!empty($dadosPesquisa['numeroPesquisaOsm'])) {
            $where[] = "osm.cod_osm = {$dadosPesquisa['numeroPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['codigoSsm'])) {
            $where[] = "osm.cod_ssm = {$dadosPesquisa['codigoSsm']}";
        }

        if (!empty($dadosPesquisa['responsavelPreenchimentoPesquisaOsm'])) {
            $where[] = "osm.usuario_responsavel = {$dadosPesquisa['responsavelPreenchimentoPesquisaOsm']}";
        }

        //#########Datas
        if (!empty($dadosPesquisa['situacaoPesquisaOsm'])) {
            $where[] = "status.cod_status = '{$dadosPesquisa['situacaoPesquisaOsm']}'";
        }

        if (!empty($dadosPesquisa['dataPartirOsm']) || !empty($dadosPesquisa['dataAteOsm'])) {
            $dataPartir = $this->inverteData($dadosPesquisa['dataPartirOsm']);
            $dataRetroceder = $this->inverteData($dadosPesquisa['dataAteOsm']);

            if ($dataPartir == $dataRetroceder && !empty($dataPartir)) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "sto.data_status >= '{$dataPartir}'";
                $where[] = "sto.data_status <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "sto.data_status >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "sto.data_status < '{$dataRetroceder}'";
                }
            }
        }

        if (!empty($dadosPesquisa['dataPartirOsmAbertura']) || !empty($dadosPesquisa['dataAteOsmAbertura'])) {
            $dataPartirA = $this->inverteData($dadosPesquisa['dataPartirOsmAbertura']);
            $dataRetrocederA = $this->inverteData($dadosPesquisa['dataAteOsmAbertura']);

            if ($dataPartirA == $dataRetrocederA && !empty($dataPartirA)) {
                $dataPartirA = $dataPartirA . " 00:00:00";
                $dataRetrocederA = $dataRetrocederA . " 23:59:59";

                $where[] = "osm.data_abertura >= '{$dataPartirA}'";
                $where[] = "osm.data_abertura <= '{$dataRetrocederA}'";

            } else {
                if (!empty($dataPartirA)) {
                    $dataPartirA = $dataPartirA . " 00:00:00";
                    $where[] = "osm.data_abertura >= '{$dataPartirA}'";
                }

                if (!empty($dataRetrocederA)) {
                    $dataRetrocederA = $dataRetrocederA . " 23:59:59";
                    $where[] = "osm.data_abertura < '{$dataRetrocederA}'";
                }
            }
        }

        if (!empty($dadosPesquisa['dataPartirOsmEncerramento']) || !empty($dadosPesquisa['dataAteOsmEncerramento'])) {
            $dataPartirE = $this->inverteData($dadosPesquisa['dataPartirOsmEncerramento']);
            $dataRetrocederE = $this->inverteData($dadosPesquisa['dataAteOsmEncerramento']);

            if ($dataPartirE == $dataRetrocederE && !empty($dataPartirE)) {
                $dataPartirE = $dataPartirE . " 00:00:00";
                $dataRetrocederE = $dataRetrocederE . " 23:59:59";

                $where[] = "oe.data_encerramento >= '{$dataPartirE}'";
                $where[] = "oe.data_encerramento <= '{$dataRetrocederE}'";

            } else {
                if (!empty($dataPartirE)) {
                    $dataPartirE = $dataPartirE . " 00:00:00";
                    $where[] = "oe.data_encerramento >= '{$dataPartirE}'";
                }

                if (!empty($dataRetrocederE)) {
                    $dataRetrocederE = $dataRetrocederE . " 23:59:59";
                    $where[] = "oe.data_encerramento < '{$dataRetrocederE}'";
                }
            }
        }

        //###########Local
        if (!empty($dadosPesquisa['linhaPesquisaOsm'])) {
            $where[] = "osm.cod_linha_atuado = {$dadosPesquisa['linhaPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['trechoPesquisaOsm'])) {
            $where[] = "osm.trecho_atuado = {$dadosPesquisa['trechoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['pnPesquisaOsm'])) {
            $where[] = "osm.cod_ponto_notavel_atuado = {$dadosPesquisa['pnPesquisaOsm']}";
        }

        //############Servi�o
        if (!empty($dadosPesquisa['grupoPesquisaOsm'])) {
            $where[] = "osm.grupo_atuado = {$dadosPesquisa['grupoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['sistemaPesquisaOsm'])) {
            $where[] = "osm.sistema_atuado = {$dadosPesquisa['sistemaPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['subSistemaPesquisa'])) {
            $where[] = "osm.subsistema_atuado = {$dadosPesquisa['subSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['servicoPesquisaOsm'])) {
            $where[] = "os.cod_servico = {$dadosPesquisa['servicoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['nivelPesquisaOsm'])) {
            $where[] = "ssm.nivel = '{$dadosPesquisa['nivelPesquisaOsm']}'";
        }

        if (!empty($dadosPesquisa['localGrupoOsm'])) {
            $where[] = "osm.cod_local_grupo = {$dadosPesquisa['localGrupoOsm']}";
        }

        if (!empty($dadosPesquisa['subLocalGrupoOsm'])) {
            $where[] = "osm.cod_sublocal_grupo = {$dadosPesquisa['subLocalGrupoOsm']}";
        }

        //############Registro de Execucao
        if (!empty($dadosPesquisa['unidadePesquisaOsm'])) {
            $where[] = "un_equipe.cod_unidade = {$dadosPesquisa['unidadePesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['equipePesquisaOsm'])) {
            $where[] = "un_equipe.cod_equipe = {$this->getCodEquipe($dadosPesquisa['equipePesquisaOsm'])}";
        }

        if (!empty($dadosPesquisa['causaPesquisaOsm'])) {
            $where[] = "oreg.cod_causa = {$dadosPesquisa['causaPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['agCausadorPesquisaOsm'])) {
            $where[] = "oreg.cod_ag_causador = {$dadosPesquisa['agCausadorPesquisaOsm']}";
        }

        //############Fechamento Pesquisa
        if (!empty($dadosPesquisa['tipoFechamentoPesquisaOsm'])) {
            $where[] = "oe.cod_tipo_fechamento = {$dadosPesquisa['tipoFechamentoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['tipoPendenciaPesquisaOsm'])) {
            $where[] = "oe.cod_pendencia = {$dadosPesquisa['tipoPendenciaPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['liberacaoPesquisaOsm'])) {
            $where[] = "oe.liberacao = '{$dadosPesquisa['liberacaoPesquisaOsm']}'";
        }
    
        if (!empty($dadosPesquisa['transferenciaPesquisaOsm'])) {
            $where[] = "oe.transferida = '{$dadosPesquisa['transferenciaPesquisaOsm']}'";
        }

        if (!empty($dadosPesquisa['maquinasEquipamentosPesquisaOsm'])) {
            $where[] = "omq.cod_material = '{$dadosPesquisa['maquinasEquipamentosPesquisaOsm']}'";
            $join .= ' LEFT JOIN osm_maquina omq ON osm.cod_osm = omq.cod_osm ';
        }

        if (!empty($dadosPesquisa['materiaisPesquisaOsm']) || !empty($dadosPesquisa['estadoPesquisaOsm']) || !empty($dadosPesquisa['origemPesquisaOsm'])) {
            $join .= ' LEFT JOIN osm_material oma ON osm.cod_osm = oma.cod_osm ';
        }

        if (!empty($dadosPesquisa['materiaisPesquisaOsm'])) {
            $where[] = "oma.cod_material = '{$dadosPesquisa['materiaisPesquisaOsm']}'";
        }

        if (!empty($dadosPesquisa['estadoPesquisaOsm'])) {
            $where[] = "estado = '{$dadosPesquisa['estadoPesquisaOsm']}'";
        }

        if (!empty($dadosPesquisa['origemPesquisaOsm'])) {
            $where[] = "origem = '{$dadosPesquisa['origemPesquisaOsm']}'";
        }

        //#########Material Rodante
        if(
            (!empty($dadosPesquisa['veiculoPesquisaOsm'])) ||
            (!empty($dadosPesquisa['carroAvariadoPesquisaOsm'])) ||
            (!empty($dadosPesquisa['prefixoPesquisaOsm'])) ||
            (!empty($dadosPesquisa['odomentroAtePesquisaOsm'])) ||
            (
            $dadosPesquisa['grupoPesquisaOsm'] == '22' ||
            $dadosPesquisa['grupoPesquisaOsm'] == '23' ||
            $dadosPesquisa['grupoPesquisaOsm'] == '26'
            )
        ) {
            $joinColuna = 'v.nome_veiculo,';
            $join .= ' 
                  LEFT JOIN material_rodante_osm mro ON osm.cod_osm = mro.cod_osm
                  LEFT JOIN veiculo v ON mro.cod_veiculo = v.cod_veiculo
                  LEFT JOIN carro c USING (cod_carro)
                  LEFT JOIN prefixo USING (cod_prefixo) ';
        }

        if (!empty($dadosPesquisa['veiculoPesquisaOsm'])) {
            $where[] = "mro.cod_veiculo = {$dadosPesquisa['veiculoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['carroAvariadoPesquisaOsm'])) {
            $where[] = "cod_carro = {$dadosPesquisa['carroAvariadoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['prefixoPesquisaOsm'])) {
            $where[] = "cod_prefixo = {$dadosPesquisa['prefixoPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['odometroPartirPesquisaOsm'])) {
            $where[] = "odometro <= {$dadosPesquisa['odometroPartirPesquisaOsm']}";
        }

        if (!empty($dadosPesquisa['odomentroAtePesquisaOsm'])) {
            $where[] = "odometro >= {$dadosPesquisa['odomentroAtePesquisaOsm']}";
        }

        ////SINTAX SQL para pesquisa no PostGres
        $sql = 'SELECT
                  osm.cod_osm,
                  osm.cod_ssm,
                  linha.nome_linha,
                  trecho.descricao_trecho,
                  osm.data_abertura AS data_abertura_osm,
                  servico.nome_servico,
                  os.complemento    AS complemento_servico,
                  '.$joinColuna.'
                  status.nome_status,
                  
                  nome_fechamento
                FROM osm_falha osm
                  JOIN status_osm sto USING (cod_ostatus)
                  JOIN status USING (cod_status)
                  JOIN linha ON cod_linha = cod_linha_atuado
                  JOIN trecho ON cod_trecho = trecho_atuado
                  LEFT JOIN osm_encerramento oe ON oe.cod_osm = osm.cod_osm
                  LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                  LEFT JOIN osm_registro oreg ON oreg.cod_osm = osm.cod_osm
                  JOIN osm_servico os ON os.cod_osm = osm.cod_osm
                  JOIN servico USING (cod_servico)
                  JOIN ssm USING (cod_ssm)
                  LEFT JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe ' . $join;
//        mro.cod_veiculo   AS cod_veiculo,
//                  cod_carro         AS cod_carro,
//                  odometro,
//                  cod_prefixo,

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $this->dadosReturn = (boolval(false));
        } else {
            $this->dadosReturn = $resultado;
        }
    }

    function getDados(){
        return $this->dadosReturn;
    }
}