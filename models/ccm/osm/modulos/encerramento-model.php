<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 05/08/2015
 * Time: 15:46
 */

class EncerramentoModel extends MainModel
{
    private $time;
    private $dadosEncerramento;
    public $dadosUsuario;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo  = $medoo;
        $this->phpass = $phpass;
        //Pega tempo atual de encerramento
        $this->time = date('d-m-Y H:i:s', time());
        $this->dadosUsuario = $dadosUsuario;

        if(!empty($_POST))
          $this->dadosEncerramento = $_POST;

        unset($_SESSION['dadosGeraisOsm']);
        unset($_SESSION['dadosEncerramento']);
    }

    public function store()
    {

      $osm = $this->medoo->select("osm_falha", ["cod_ssm", "grupo_atuado"], ["cod_osm" => $this->dadosEncerramento['codigoOs']])[0];
      $codSsm = $osm['cod_ssm'];

      $codSaf = $this->medoo->select("ssm", "cod_saf", ["cod_ssm" => (int)$codSsm]);
      $codSaf = $codSaf[0];

      $selectSaf = $this->medoo->select("v_saf", "*", ["cod_saf" => (int)$codSaf]);
      $selectSaf = $selectSaf[0];

      $codMr = [22,23];

      //Pega c�digo un_equipe caso seja transferida
      if ($this->dadosEncerramento['selectTranferencia'] == "s") {
          $codigoEquipe  = $this->getCodEquipe($this->dadosEncerramento['equipeContinuidade']);

          $codigoUnEquipe = $this->medoo->select('un_equipe', 'cod_un_equipe', [
              "AND" => [
                  "cod_equipe"  => (int)$codigoEquipe,
                  "cod_unidade" => (int)$this->dadosEncerramento['unidadeContinuidade']
              ]
          ]);
          $codigoUnEquipe = $codigoUnEquipe[0];
      }

      $this->alterarStatusOsm($this->dadosEncerramento['codigoOs'],11, $this->time, $this->dadosUsuario);

      //Mensagem para o alerta RealTime
      $_SESSION['notificacao']['mensag'] = "Osm n.{$this->dadosEncerramento['codigoOs']} encerrada";

      //Realizando altera��o no status da SSM
      switch ($this->dadosEncerramento['descricaoTipoFechamento'])
      {
          //Sem Pend�ncia
          case "1":
              if(in_array($selectSaf['cod_grupo'], $codMr))
              {
                  $this->alterarStatusSsm($codSsm, 35, $this->time, $this->dadosUsuario); //Status da SSM fica em Aguardando Valida��o
                  $this->encerrarSaf($codSaf, $this->time, $this->dadosUsuario, 37); //Status da SAF fica Aguardando Valida��o SE esta for a �ltima SSM.
              }
              else
                  $this->alterarStatusSsm($codSsm, 39, $this->time, $this->dadosUsuario); //Status da SSM fica em An�lise

              $insertEncerramento = $this->medoo->insert("osm_encerramento", [
                  "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
                  "cod_tipo_fechamento"   => (int)$this->dadosEncerramento['descricaoTipoFechamento'],
                  "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                  "descricao"             => (string)$this->dadosEncerramento['recomendacoesContinuidade'],
                  "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                  "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                  "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
              ]);

              $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. " . $codSsm . " Aguardando Valida��o";

              break;

          // Com Pend�ncia
          case "2":
              $this->alterarStatusSsm($codSsm, 15, $this->time, $this->dadosUsuario); //Pendente
              $_SESSION['notificacao']['mensag'] .= "<br/>Ssm n. " . $codSsm . " pendente";

              $insertEncerramento = $this->medoo->insert("osm_encerramento", [
                  "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
                  "cod_tipo_fechamento"   => (int)$this->dadosEncerramento['descricaoTipoFechamento'],
                  "cod_pendencia"         => (int)$this->dadosEncerramento['pendenciaFechamento'],
                  "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                  "descricao"             => (string)$this->dadosEncerramento['recomendacoesContinuidade'],
                  "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                  "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                  "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
              ]);

              //Mensagem para o alerta RealTime
              $_SESSION['notificacao']['ssmPendente'] = true;
              break;

          // N�o Executada
          case "3":
              $this->alterarStatusSsm($codSsm, 35, $this->time, $this->dadosUsuario); //Status da SSM fica Aguardando Valida��o
              $this->encerrarSaf($codSaf, $this->time, $this->dadosUsuario, 37);

              $insertEncerramento = $this->medoo->insert("osm_encerramento", [
                  "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
                  "cod_tipo_fechamento"   => (int)$this->dadosEncerramento['descricaoTipoFechamento'],
                  "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                  "descricao"             => (string)$this->dadosEncerramento['motivoNaoExecucao'],
                  "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                  "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                  "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
              ]);

              $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. " . $codSsm . " Aguardando Valida��o";

              break;

          // Com Pend�ncia por falta de material contratual
          case "4":
              $this->alterarStatusSsm($codSsm, 41, $this->time, $this->dadosUsuario); //Pendente Por Falta de Material N�o Contratual
              //Envio de e-mail de notifica��o
              $sql = "SELECT distinct on (email) email FROM usuario WHERE nivel = '6.1'";
              $emails = $this->medoo->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0);
              Mailer::sendEmail($emails, 'newPendencyMaterialService', ['cod_ssm' => $codSsm]);

              $_SESSION['notificacao']['mensag'] .= "<br/>Ssm n. " . $codSsm . " pendente por falta de material contratual";

              $insertEncerramento = $this->medoo->insert("osm_encerramento", [
                  "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
                  "cod_tipo_fechamento"   => (int)4,
                  "cod_pendencia"         => (int)67,
                  "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                  "descricao"             => (string)$this->dadosEncerramento['recomendacoesContinuidade'],
                  "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                  "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                  "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
              ]);

              //Mensagem para o alerta RealTime
              $_SESSION['notificacao']['ssmPendente'] = true;
              break;
      }

      if($this->dadosEncerramento['selectTranferencia'] == "s"){
          $this->inserirUnEquipe($insertEncerramento, $codigoUnEquipe);
          $ssm = $this->transferenciaOsm($selectSaf, $this->time, $codigoUnEquipe, $this->dadosEncerramento['recomendacoesContinuidade'], $this->dadosUsuario);

          //Mensagem para o alerta RealTime
          $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. " . $ssm . " criada para transferencia";
      }

      $this->createNotification($codSsm);

    }

    public function update()
    {
      $osm = $this->medoo->select("osm_falha", ["cod_ssm", "grupo_atuado"], ["cod_osm" => $this->dadosEncerramento['codigoOs']])[0];
      $codSsm = $osm['cod_ssm'];

      $codSaf = $this->medoo->select("ssm", "cod_saf", ["cod_ssm" => (int)$codSsm]);
      $codSaf = $codSaf[0];

      $selectSaf = $this->medoo->select("v_saf", "*", ["cod_saf" => (int)$codSaf]);
      $selectSaf = $selectSaf[0];

      $codMr = [22,23];

      switch($this->dadosEncerramento['descricaoTipoFechamento'])
      {
         //Sem Pend�ncia
         case "1":
             $this->alterarStatusSsm($codSsm, 35, $this->time, $this->dadosUsuario); //Status da SSM fica Aguardando Valida��o
             $this->encerrarSaf($codSaf, $this->time, $this->dadosUsuario, 37);

             $insertEncerramento = $this->medoo->update("osm_encerramento", [
                 "cod_tipo_fechamento"   => (int)$this->dadosEncerramento['descricaoTipoFechamento'],
                 "cod_pendencia"         => null,
                 "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                 "descricao"             => (string)$this->dadosEncerramento['recomendacoesContinuidade'],
                 "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                 "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                 "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
             ],[
                 "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
             ]);
             $_SESSION['notificacao']['mensag'] = "Ssm n. " . $codSsm . " Aguardando Valida��o";
             break;

         //Com Pend�ncia
         case"2":
             if(in_array($osm['grupo_atuado'], SISTEMAS_FIXOS) && $this->dadosEncerramento['pendenciaFechamento'] == 67){
                 $this->alterarStatusSsm($codSsm, 41, $this->time, $this->dadosUsuario); //Pendente Por Falta de Material N�o Contratual
                 $emails = $this->medoo->select('usuario', 'email', ['nivel' => '6.1']);
                 Mailer::sendEmail($emails, 'newPendencyMaterialService', ['cod_ssm' => $codSsm]);

             }
             else
                 $this->alterarStatusSsm($codSsm, 15, $this->time, $this->dadosUsuario); //Pendente

             $insertEncerramento = $this->medoo->update("osm_encerramento", [
                 "cod_tipo_fechamento"   => (int)$this->dadosEncerramento['descricaoTipoFechamento'],
                 "cod_pendencia"         => (int)$this->dadosEncerramento['pendenciaFechamento'],
                 "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                 "descricao"             => (string)$this->dadosEncerramento['recomendacoesContinuidade'],
                 "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                 "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                 "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
             ],[
                 "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
             ]);

             //Mensagem para o alerta RealTime
             $_SESSION['notificacao']['mensag'] = "Ssm n. " . $codSsm . " pendente";
             $_SESSION['notificacao']['ssmPendente'] = true;

             //Verifica��o de status SAF
             if($selectSaf['nome_status'] != "Autorizada"){
                 $autorizaStatusSaf = $this->medoo->insert("status_saf",[
                     "cod_status"        => (int) 1, // Autorizada
                     "cod_saf"           => (int) $codSaf,
                     "data_status"       => $this->time,
                     "usuario"       => (int)  $this->dadosUsuario['cod_usuario']
                 ]);

                 $autorizaSaf = $this->medoo->update("saf",[
                     "cod_ssaf"  => (int) $autorizaStatusSaf
                 ],[
                     "cod_saf"   => (int) $codSaf
                 ]);
             }
             break;

         //N�o Executado
         case "3":
             $this->alterarStatusSsm($codSsm, 35, $this->time, $this->dadosUsuario); //Status da SSM fica Aguardando Valida��o
             $this->encerrarSaf($codSaf, $this->time, $this->dadosUsuario, 37);

             $insertEncerramento = $this->medoo->update("osm_encerramento", [
                 "cod_tipo_fechamento"   => (int)$this->dadosEncerramento['descricaoTipoFechamento'],
                 "cod_pendencia"         => null,
                 //TODO: Alterar nome do elemento HTML
                 "cod_funcionario"       => (int) $this->dadosEncerramento['codFuncionario'],
                 "descricao"             => (string)$this->dadosEncerramento['motivoNaoExecucao'],
                 "liberacao"             => (string)$this->dadosEncerramento['liberacaoTrafego'],
                 "transferida"           => (string)$this->dadosEncerramento['selectTranferencia'],
                 "data_encerramento"     => $this->dadosEncerramento['dataHoraFechamento']
             ],[
                 "cod_osm"               => (int)$this->dadosEncerramento['codigoOs'],
             ]);
             $_SESSION['notificacao']['mensag'] = "Ssm n. " . $codSsm . " Aguardando Valida��o";
             break;
     }

      if($this->dadosEncerramento['selectTranferencia'] == "s"){
         $this->inserirUnEquipe($insertEncerramento, $codigoUnEquipe);
         $ssm = $this->transferenciaOsm($selectSaf, $this->time, $codigoUnEquipe, $this->dadosEncerramento['recomendacoesContinuidade'], $this->dadosUsuario);

         //Mensagem para o alerta RealTime
         $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. " . $ssm . " criada pela transferencia";
     }

      $this->createNotification($codSsm);
    }

    private function inserirUnEquipe($insertEncerramento, $codigoUnEquipe){
        $insertEncerramento = $this->medoo->update("osm_encerramento", [
            "cod_un_equipe"         => (int)$codigoUnEquipe
        ],[
            "cod_osmence" => (int)$insertEncerramento
        ]);
    }

    private function transferenciaOsm($saf, $codEquipeTrans, $recomendacao){

        $insertSsm = $this->medoo->insert("ssm",[
            "cod_saf"       => (int) $saf['cod_saf'],
            "data_abertura" => $this->time,
            "nivel"         => (string) $saf['nivel'],
            "cod_grupo"     => $saf['cod_grupo'],
            "cod_sistema"   => $saf['cod_sistema'],
            "cod_subsistema"=> $saf['cod_subsistema'],
            "cod_linha"     => $saf['cod_linha'],
            "cod_trecho"    => $saf['cod_trecho'],
            "cod_un_equipe" => (int) $codEquipeTrans,
            "complemento"   => (string) $recomendacao
        ]);

        $inserirStatusSsm = $this->medoo->insert("status_ssm",[
            "cod_status"    => 8, // Transferida
            "cod_ssm"       => (int) $insertSsm,
            "data_status"   => $this->time,
            "descricao"     => (string) "REC.:" . $recomendacao,
            "usuario"       => (int)  $this->dadosUsuario['cod_usuario']
        ]);

        $updateSsmStatus = $this->medoo->update("ssm",[
            "cod_mstatus" => (int) $inserirStatusSsm
        ], [
            "cod_ssm" => (int)$insertSsm
        ]);

        $_SESSION['notificacao']['transferencia']       = $insertSsm;
        $_SESSION['notificacao']['transferenciaSaf']    = $saf['cod_saf'];
        $_SESSION['notificacao']['transferenciaData']   = $this->time;
        $_SESSION['notificacao']['transferenciaNivel']  = $saf['nivel'];

        $_SESSION['notificacao']['transferenciaStatus'] = "Transferida";
        $_SESSION['notificacao']['transferenciaMotivo'] = "REC.:" . $recomendacao;

        return $insertSsm;
    }

    public function createNotification($codSsm)
    {
      // Mensagem do respons�vel pela a��o.
      $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$this->dadosUsuario['usuario']}";

      //Tipo de a��o
      $_SESSION['notificacao']['tipo']   = "Deletar";
      $_SESSION['notificacao']['tabela'] = "Osm";
      //Informa��o para a alimenta��o da tabela
      $_SESSION['notificacao']['numero'] = $this->dadosEncerramento['codigoOs'];

      if(!empty($_SESSION['notificacao']['ssmPendente'])){
          $viewSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$codSsm]);
          $viewSsm = $viewSsm[0];

          $_SESSION['notificacao']['ssmPendenteNumero']          = $codSsm;
          $_SESSION['notificacao']['ssmPendenteNumeroSaf']       = $viewSsm['cod_saf'];
          $_SESSION['notificacao']['ssmPendenteDataAbertura']    = $this->parse_timestamp($viewSsm["data_abertura"]);
          $_SESSION['notificacao']['ssmPendenteEncaminhamento']  = $this->parse_timestamp($viewSsm["data_status"]);
          $_SESSION['notificacao']['ssmPendenteNivel']           = $viewSsm['nivel'];

          $_SESSION['notificacao']['ssmPendenteVeiculo'] = $viewSsm['nome_veiculo'];

          $_SESSION['notificacao']['ssmPendenteNomeLinha']       = $viewSsm['nome_linha'];
          $_SESSION['notificacao']['ssmPendenteNomeTrecho']      = $viewSsm['descricao_trecho'];
      }

      $sql = "SELECT un_equipe.cod_un_equipe FROM osm_registro osmr
                JOIN un_equipe USING(cod_un_equipe)
                WHERE osmr.cod_osm = {$this->dadosEncerramento['codigoOs']}";


      $codigoUniEquipe = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
      $codigoUniEquipe = $codigoUniEquipe[0]['cod_un_equipe'];

      $_SESSION['notificacao']['subChannelEquipe'] = $codigoUniEquipe;
      $_SESSION['notificacao']['nomeUsuario'] = $this->dadosUsuario['usuario'];
    }
}
