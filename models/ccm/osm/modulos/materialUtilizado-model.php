<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/07/2015
 * Time: 09:42
 */

class MaterialUtilizadoModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosMateriais = $_SESSION['dadosMateriais'];

        if(!empty($_SESSION['dadosMaterialOs']['check']) && $_SESSION['dadosMaterialOs']['codigoOs'] == $dadosMateriais['codigoOs'])
            return;

        for($i = 0; $i<500; $i++){
            if(!empty($dadosMateriais['retirarMaterial'.$i])){
                $deleteMaterial = $this->medoo->delete("osm_material",[
                    "AND" =>
                        [
                            "cod_osm"           => (int) $dadosMateriais['codigoOs'],
                            "cod_material"   => (int) $dadosMateriais['codigoMaterialDelete'.$i]
                        ]
                ]);
            }else{
                if(!empty($dadosMateriais['codigoMaterial'.$i])){
                    $insertMaterial = $this->medoo->insert("osm_material",[
                        "cod_osm"           => (int) $dadosMateriais['codigoOs'],
                        "cod_material"      => (int) $dadosMateriais['codigoMaterial'.$i],
                        "utilizado"         => (double) $dadosMateriais['qtdUtilizado'.$i],
                        "unidade"           => (int) $dadosMateriais['unidadeMedidaOs'.$i],
                        "estado"            => (string) $dadosMateriais['estadoMaterial'.$i],
                        "origem"            => (string) $dadosMateriais['origemMaterial'.$i],
                        "descricao_origem"  => (string) $dadosMateriais['outroOrigem'.$i]
                    ]);
                }
            }
        }

        $_SESSION['dadosCheck']['material'] = $this->medoo->select("osm_material", "*", ["cod_osm" => (int)$dadosMateriais['codigoOs']]);
        unset($_SESSION['dadosMateriais']);

    }
}
