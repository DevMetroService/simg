<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 23/07/2015
 * Time: 15:48
 */

class MaquinaUtilizadaModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosMaquina = $_SESSION['dadosMaquina'];

        if(!empty($_SESSION['dadosMaquinaOs']['check']) && $_SESSION['dadosMaquinaOs']['codigoOs'] == $dadosMaquina['codigoOs'])
            return;

        for($i = 0; $i<20; $i++){
            if(!empty($dadosMaquina['retirarMaquina'.$i])){
                $deleteMaquina = $this->medoo->delete("osm_maquina",[
                    "AND" =>
                        [
                            "cod_osm"           => (int) $dadosMaquina['codigoOs'],
                            "cod_material"      => (int) $dadosMaquina['codigoMaquina'.$i],
                        ]
                ]);
            }else{
                if(!empty($dadosMaquina['codigoMaquina'.$i])){

                    $selectMaq = $this->medoo->select("osm_maquina", "*",
                        [
                            "AND" =>
                                [
                                    "cod_osm"       => (int) $dadosMaquina['codigoOs'],
                                    "cod_material"  => (int) $dadosMaquina['codigoMaquina'.$i]
                                ]
                        ]
                    );

                    if(empty($selectMaq)){
                        $insertMaquina = $this->medoo->insert("osm_maquina",[
                            "cod_osm"           => (int) $dadosMaquina['codigoOs'],
                            "cod_material"      => (int) $dadosMaquina['codigoMaquina'.$i],
                            "qtd"               => (int) $dadosMaquina['qtdMaquina'.$i],
                            "descricao"         => (string) $dadosMaquina['descricaoMaquina'.$i],
                            "num_serie"         => (string) $dadosMaquina['numeroSerieMaquina'.$i],
                            "total_hrs"         => (string) $dadosMaquina['totalHorasMaquina'.$i],
                        ]);
                    }else{
                        $updateMaterial = $this->medoo->update("osm_maquina",[
                            "qtd"           => (int) $dadosMaquina['qtdMaquina'.$i],
                            "total_hrs"     => (string) $dadosMaquina['totalHorasMaquina'.$i],
                            "descricao"     => (string) $dadosMaquina['descricaoMaquina'.$i],
                        ],[
                            "AND" =>
                                [
                                    "cod_osm"       => (int) $dadosMaquina['codigoOs'],
                                    "cod_material"  => (int) $dadosMaquina['codigoMaquina'.$i]
                                ]
                        ]);
                    }
                }
            }
        }

        
        $_SESSION['dadosCheck']['maquina'] = $this->medoo->select("osm_maquina", "*", ["cod_osm" => (int)$dadosMaquina['codigoOs']]);
        unset($_SESSION['dadosMaoObra']);

    }
}