<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/07/2015
 * Time: 14:22
 */

class SalvarDadosGeraisOsmModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosOsm = $_SESSION['dadosGeraisOsm'];

        $codUsuario = $this->getCodUsuario($dadosOsm['responsavelPreenchimento']);

        $time = date('d-m-Y H:i:s', time());

        $this->osResponsavel($codUsuario, $dadosOsm['codigoOs'], "osm_falha");

        //Atualizando dados gerais da OSM na tabela osm_falha
        //Atualizando dados gerais da OSM na tabela osm_falha
        //Atualizando dados gerais da OSM na tabela osm_falha
        $updateOsm = $this->medoo->update("osm_falha",[
            "grupo_atuado"              => (int) $dadosOsm['grupoOsAtuado'],
            "sistema_atuado"            => (int) $dadosOsm['sistemaOsAtuado'],
            "subsistema_atuado"         => (int) $dadosOsm['subSistemaOsAtuado'],
            "cod_linha_atuado"          => (int) $dadosOsm['linhaOsAtuado'],
            "trecho_atuado"             => (int) $dadosOsm['trechoOsAtuado'],
            "complemento_local"         => (string) $dadosOsm['complementoLocalOsAtuado'],
            "km_inicial"                => (string) $dadosOsm['kmInicialOs'],
            "km_final"                  => (string) $dadosOsm['kmFinalOs'],
            "posicao_atuado"            => (string) $dadosOsm['posicaoAtuado'],
            "cod_via"                   => (int) $dadosOsm['viaOsAtuada'],
            "cod_ponto_notavel_atuado"  => (int) $dadosOsm['pontoNotavelOsAtuado'],
            "nivel"                     => (string) $dadosOsm['nivelAtuado'],
        ],[
            "cod_osm"                   => (int) $dadosOsm['codigoOs']
        ]);


        //Atualizando dados gerais da OSM na tabela osm_servico
        //Atualizando dados gerais da OSM na tabela osm_servico
        //Atualizando dados gerais da OSM na tabela osm_servico
        $servicoOsm = $this->medoo->select("osm_servico","*",["cod_osm" => $dadosOsm['codigoOs']]);
        $servicoOsm = $servicoOsm[0];

        if(empty($servicoOsm)){
            $insertOsmServico = $this->medoo->insert("osm_servico",[
                "cod_osm"               => (int) $dadosOsm['codigoOs'],
                "cod_servico"           => (int) $dadosOsm['servicoExecutadoOs'],
                "unidade_medida"        => (int) $dadosOsm['unidadeTotalServico'],
                "unidade_tempo"         => (int) $dadosOsm['unidadeTempoServico'],
                "qtd_pessoas"           => (int) $dadosOsm['efetivoServico'],
                "total_servico"         => (int) $dadosOsm['totalServico'],
                "tempo"                 => (int) $dadosOsm['tempoServico'],
                "complemento"           => (string) $dadosOsm['complementoServico']
            ]);
        }else{
            $updateOsmServico = $this->medoo->update("osm_servico",[
                "cod_servico"           => (int) $dadosOsm['servicoExecutadoOs'],
                "unidade_medida"        => (int) $dadosOsm['unidadeTotalServico'],
                "unidade_tempo"         => (int) $dadosOsm['unidadeTempoServico'],
                "qtd_pessoas"           => (int) $dadosOsm['efetivoServico'],
                "total_servico"         => (int) $dadosOsm['totalServico'],
                "tempo"                 => (int) $dadosOsm['tempoServico'],
                "complemento"           => (string) $dadosOsm['complementoServico']
            ],[
                "cod_osm"               => (int) $dadosOsm['codigoOs'],
            ]);
        }

        //Atualizando dados gerais da OSM na tabela material_rodante_osm
        //Atualizando dados gerais da OSM na tabela material_rodante_osm
        //Atualizando dados gerais da OSM na tabela material_rodante_osm
        $mrOsm = $this->medoo->select("material_rodante_osm","*",["cod_osm" => $dadosOsm['codigoOs']]);
        $mrOsm = $mrOsm[0];

        if(empty($mrOsm)){
            $insertOsmMr = $this->medoo->insert("material_rodante_osm",[
                "cod_osm"          => (int) $dadosOsm['codigoOs'],
                "cod_veiculo"      => (int) $dadosOsm['veiculoMrOsm'],
                "cod_carro"        => (int) $dadosOsm['carroMrOs'],
                "odometro"         => (int) $dadosOsm['odometroMrOsm'],
                "cod_prefixo"      => (int) $dadosOsm['prefixoMrOsm'],
                "cod_grupo"        => (int) $dadosOsm['grupoOsAtuado']
            ]);
        }else{
            $updateOsmMr = $this->medoo->update("material_rodante_osm",[
                "cod_veiculo"      => (int) $dadosOsm['veiculoMrOsm'],
                "cod_carro"        => (int) $dadosOsm['carroMrOs'],
                "odometro"         => (int) $dadosOsm['odometroMrOsm'],
                "cod_prefixo"      => (int) $dadosOsm['prefixoMrOsm'],
                "cod_grupo"        => (int) $dadosOsm['grupoOsAtuado']
            ],[
                "cod_osm"               => (int) $dadosOsm['codigoOs'],
            ]);
        }
    }
}

