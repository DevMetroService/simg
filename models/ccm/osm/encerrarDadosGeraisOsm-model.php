<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 12/06/2018
 * Time: 09:49
 */

class EncerrarDadosGeraisOsmModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosOsm = $_SESSION['dadosGeraisOsm'];
        unset($_SESSION['dadosGeraisOsm']);

        $codUsuario = $this->getCodUsuario($dadosOsm['responsavelPreenchimento']);

        $time = date('d-m-Y H:i:s', time());

        $this->osResponsavel($codUsuario, $dadosOsm['codigoOs'], "osm_falha");

        //Atualizando dados gerais da OSM na tabela osm_falha
        $updateOsm = $this->medoo->update("osm_falha", [
            "grupo_atuado" => (int)$dadosOsm['grupoOsAtuado'],
            "sistema_atuado" => (int)$dadosOsm['sistemaOsAtuado'],
            "subsistema_atuado" => (int)$dadosOsm['subSistemaOsAtuado'],
            "cod_linha_atuado" => (int)$dadosOsm['linhaOsAtuado'],
            "trecho_atuado" => (int)$dadosOsm['trechoOsAtuado'],
            "complemento_local" => (string)$dadosOsm['complementoLocalOsAtuado'],
            "km_inicial" => (string)$dadosOsm['kmInicialOs'],
            "km_final" => (string)$dadosOsm['kmFinalOs'],
            "posicao_atuado" => (string)$dadosOsm['posicaoAtuado'],
            "cod_via" => (int)$dadosOsm['viaOsAtuada'],
            "cod_ponto_notavel_atuado" => (int)$dadosOsm['pontoNotavelOsAtuado'],
            "nivel" => (string)$dadosOsm['nivelAtuado'],
        ], [
            "cod_osm" => (int)$dadosOsm['codigoOs']
        ]);


        //Atualizando dados gerais da OSM na tabela osm_servico
        $servicoOsm = $this->medoo->select("osm_servico", "*", ["cod_osm" => $dadosOsm['codigoOs']]);
        $servicoOsm = $servicoOsm[0];

        if (empty($servicoOsm)) {
            $insertOsmServico = $this->medoo->insert("osm_servico", [
                "cod_osm" => (int)$dadosOsm['codigoOs'],
                "cod_servico" => (int)$dadosOsm['servicoExecutadoOs'],
                "unidade_medida" => (int)$dadosOsm['unidadeTotalServico'],
                "unidade_tempo" => (int)$dadosOsm['unidadeTempoServico'],
                "qtd_pessoas" => (int)$dadosOsm['efetivoServico'],
                "total_servico" => (int)$dadosOsm['totalServico'],
                "tempo" => (int)$dadosOsm['tempoServico'],
                "complemento" => (string)$dadosOsm['complementoServico']
            ]);
        } else {
            $updateOsmServico = $this->medoo->update("osm_servico", [
                "cod_servico" => (int)$dadosOsm['servicoExecutadoOs'],
                "unidade_medida" => (int)$dadosOsm['unidadeTotalServico'],
                "unidade_tempo" => (int)$dadosOsm['unidadeTempoServico'],
                "qtd_pessoas" => (int)$dadosOsm['efetivoServico'],
                "total_servico" => (int)$dadosOsm['totalServico'],
                "tempo" => (int)$dadosOsm['tempoServico'],
                "complemento" => (string)$dadosOsm['complementoServico']
            ], [
                "cod_osm" => (int)$dadosOsm['codigoOs'],
            ]);
        }

        //Atualizando dados gerais da OSM na tabela material_rodante_osm
        $mrOsm = $this->medoo->select("material_rodante_osm", "*", ["cod_osm" => $dadosOsm['codigoOs']]);
        $mrOsm = $mrOsm[0];

        if (empty($mrOsm)) {
            $insertOsmMr = $this->medoo->insert("material_rodante_osm", [
                "cod_osm" => (int)$dadosOsm['codigoOs'],
                "cod_veiculo" => (int)$dadosOsm['veiculoMrOsm'],
                "cod_carro" => (int)$dadosOsm['carroMrOsm'],
                "odometro" => (int)$dadosOsm['odometroMrOsm'],
                "cod_prefixo" => (int)$dadosOsm['prefixoMrOsm'],
                "cod_grupo" => (int)$dadosOsm['grupoOsAtuado']
            ]);
        } else {
            $updateOsmMr = $this->medoo->update("material_rodante_osm", [
                "cod_veiculo" => (int)$dadosOsm['veiculoMrOsm'],
                "cod_carro" => (int)$dadosOsm['carroMrOsm'],
                "odometro" => (int)$dadosOsm['odometroMrOsm'],
                "cod_prefixo" => (int)$dadosOsm['prefixoMrOsm'],
                "cod_grupo" => (int)$dadosOsm['grupoOsAtuado']
            ], [
                "cod_osm" => (int)$dadosOsm['codigoOs'],
            ]);
        }

        //Encerrando OSM
        $codSsm = $this->medoo->select("osm_falha", "cod_ssm", ["cod_osm" => $dadosOsm['codigoOs']]);
        $codSsm = $codSsm[0];

        $this->alterarStatusOsm($dadosOsm['codigoOs'], 11, $time, $dadosUsuario);

        //Verifica se existe tabela encerramento.
        $selectEncerramento = $this->medoo->select("osm_encerramento", "*", ["cod_osm" => $dadosOsm['codigoOs']]);

        //Altero Status para Encerrado.
        if (empty($selectEncerramento)) {
            // Manobra de Entrada ou CheckList Reprovado
            if($dadosOsm['servicoExecutadoOs'] == '307' || $dadosOsm['servicoExecutadoOs'] == '362'){
                $this->alterarStatusSsm($codSsm, 15, $time, $dadosUsuario); //Pendente

                $insertEncerramento = $this->medoo->insert("osm_encerramento", [
                    "cod_osm" => (int)$dadosOsm['codigoOs'],
                    "cod_tipo_fechamento" => (int)2,
                    "cod_pendencia" => (int)56,
                    "liberacao" => (string)'n',
                    "transferida" => (string)'n',
                    "data_encerramento" => $time
                ]);

                //Mensagem para o alerta RealTime
                $_SESSION['notificacao']['mensag'] = "Osm de Manobra de Entrada n.{$dadosOsm['codigoOs']} registrada.";
                $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. " . $codSsm . " pendente";
                $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";
                $_SESSION['notificacao']['ssmPendente'] = true;
            }else{ // Manobra de Sa�da ou Checklist Aprovado
                $this->alterarStatusSsm($codSsm, 16, $time, $dadosUsuario); //Encerrado

                $insertEncerramento = $this->medoo->insert("osm_encerramento", [
                    "cod_osm" => (int)$dadosOsm['codigoOs'],
                    "cod_tipo_fechamento" => (int)1,
                    "liberacao" => (string)'s',
                    "transferida" => (string)'n',
                    "data_encerramento" => $time
                ]);


                //Mensagem para o alerta RealTime
                $_SESSION['notificacao']['mensag'] = "Osm de Manobra de Saida n.{$dadosOsm['codigoOs']} registrada.";
                $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. " . $codSsm . " finalizada.";

                $codSaf = $this->medoo->select("ssm", "cod_saf", ["cod_ssm" => (int)$codSsm]);
                $codSaf = $codSaf[0];

                $this->encerrarSaf($codSaf, $this->inverteData($time), $dadosUsuario);

                $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";
            }
        }

        $viewSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$codSsm]);
        $viewSsm = $viewSsm[0];

        $_SESSION['notificacao']['subChannelEquipe'] = $viewSsm['cod_un_equipe'];
        $_SESSION['notificacao']['nomeUsuario'] = $dadosUsuario['usuario'];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo'] = "Deletar";
        $_SESSION['notificacao']['tabela'] = "Osm";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero'] = $dadosOsm['codigoOs'];

        if (!empty($_SESSION['notificacao']['ssmPendente'])) {
            $_SESSION['notificacao']['ssmPendenteNumero'] = $codSsm;
            $_SESSION['notificacao']['ssmPendenteNumeroSaf'] = $viewSsm['cod_saf'];
            $_SESSION['notificacao']['ssmPendenteDataAbertura'] = $this->parse_timestamp($viewSsm["data_abertura"]);
            $_SESSION['notificacao']['ssmPendenteEncaminhamento'] = $this->parse_timestamp($viewSsm["data_status"]);
            $_SESSION['notificacao']['ssmPendenteNivel'] = $viewSsm['nivel'];

            $_SESSION['notificacao']['ssmPendenteVeiculo'] = $viewSsm['nome_veiculo'];

            $_SESSION['notificacao']['ssmPendenteServico'] = $viewSsm['nome_servico'];

            $_SESSION['notificacao']['ssmPendenteNomeLinha'] = $viewSsm['nome_linha'];
            $_SESSION['notificacao']['ssmPendenteNomeTrecho'] = $viewSsm['descricao_trecho'];
        }
    }
}
