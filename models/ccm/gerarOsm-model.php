<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/06/2015
 * Time: 15:44
 */
class gerarOsmModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $grupo_mr = [22, 23, 26];

        $dadosSsm = $_SESSION['gerarOsm'];

        $time = date('d-m-Y H:i:s', time());

        $hasSsm = $this->medoo->select("v_ssm", "*",["cod_ssm" => $dadosSsm['codigoSsm']]);
        $hasSsm = $hasSsm[0];

        $alterDispo = $hasSsm['cod_status']; //Para altera��o de disponibilidade de trem;

        if(!empty($dadosSsm['justificativaAltNivel']))
            $justificativa = "Altera��o de n�vel => " . $dadosSsm['justificativaAltNivel']. ". POR: " . $dadosUsuario['usuario'];
        else
            $justificativa = "";

        // Se n�o estiver pendente � permitido a altera��o da ssm
        if($hasSsm['cod_status'] == 15 || $hasSsm['cod_status'] == 36 || $hasSsm['cod_status'] == 42 || $hasSsm['cod_status'] == 43 || $dadosSsm['viewSsm']){

            $this->alterarStatusSsm($dadosSsm['codigoSsm'], 5, $time, $dadosUsuario, $justificativa);

            //######################## Gerar Osm ######################
            //######################## Gerar Osm ######################
            //######################## Gerar Osm ######################

            $insertOsm = $this->medoo->insert("osm_falha", [
                "cod_ssm" => (int)$hasSsm['cod_ssm'],
                "grupo_atuado" => (int)$hasSsm['cod_grupo'],
                "sistema_atuado" => (int)$hasSsm['cod_sistema'],
                "subsistema_atuado" => (int)$hasSsm['cod_subsistema'],
                "trecho_atuado" => (int)$hasSsm['cod_trecho'],
                "cod_local_grupo" => (int)$hasSsm['cod_local_grupo'],
                "cod_sublocal_grupo" => (int)$hasSsm['cod_sublocal_grupo'],
                "cod_linha_atuado" => (int)$hasSsm['cod_linha'],
                "cod_ponto_notavel_atuado" => (int)$hasSsm['cod_ponto_notavel'],
                "cod_via" => (int)$hasSsm['cod_via'],
                "complemento_local" => (string)$hasSsm['complemento'],
                "posicao_atuado" => (string)$hasSsm['posicao'],
                "km_inicial" => (string)$hasSsm['km_inicial'],
                "km_final" => (string)$hasSsm['km_final'],
                "nivel" => (string)$hasSsm['nivel'],
                "data_abertura" => $time
            ]);
            // ####################### Insert OSM_SERVICO ###########################
            // ####################### Insert OSM_SERVICO ###########################
            // ####################### Insert OSM_SERVICO ###########################

            $insertServicoOsm = $this->medoo->insert("osm_servico", [
                "cod_servico" => (int)$hasSsm['cod_servico'],
                "cod_osm" => (int)$insertOsm,
                "complemento" => (string)$hasSsm['complemento_servico']
            ]);

            //Atualizando dados gerais da OSM na tabela material_rodante_osm
            //Atualizando dados gerais da OSM na tabela material_rodante_osm
            //Atualizando dados gerais da OSM na tabela material_rodante_osm

            if($hasSsm['cod_grupo'] == 22 || $hasSsm['cod_grupo'] == 23 || $hasSsm['cod_grupo'] == 26){
                $insertOsmMr = $this->medoo->insert("material_rodante_osm", [
                    "cod_osm" => (int)$insertOsm,
                    "cod_veiculo" => (int)$hasSsm['cod_veiculo'],
                    "cod_carro" => (int)$hasSsm['cod_carro'],
                    "odometro" => (int)$hasSsm['odometro'],
                    "cod_prefixo" => (int)$hasSsm['cod_prefixo'],
                    "cod_grupo" => (int)$hasSsm['cod_grupo']
                ]);

                if($alterDispo == 12)
                  $this->medoo->update('veiculo', ["disponibilidade" => 'n'], ["cod_veiculo" => (int)$hasSsm['cod_veiculo']]);
            }

            //######################## Inserir Status Osm ######################
            //######################## Inserir Status Osm ######################
            //######################## Inserir Status Osm ######################

            $insertStatusOsm = $this->medoo->insert("status_osm", [
                "cod_osm" => (int)$insertOsm,
                "cod_status" => (int)10, // Execu��o
                "data_status" => $time,
                "usuario" => (int)$dadosUsuario['cod_usuario']
            ]);

            //######################## Update Osm c/ ultimo status ######################

            $updateOsmStatus = $this->medoo->update("osm_falha", [
                "cod_ostatus" => (int)$insertStatusOsm
            ], [
                "cod_osm" => (int)$insertOsm
            ]);
        }else {

            $codigoUsuario = $this->getCodUsuario($dadosSsm['responsavelSsm']);
            $codigoEquipe = $this->getCodEquipe($dadosSsm['equipeSsm']);

            $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe", [
                "AND" => [
                    "cod_equipe"  => (int)$codigoEquipe,
                    "cod_unidade" => (int)$dadosSsm['localSsm']
                ]
            ]);
            $unEquipe = $unEquipe[0];

            $this->alterarStatusSsm($dadosSsm['codigoSsm'], 5, $time, $dadosUsuario, $justificativa);

            $updateSsm = $this->medoo->update("ssm", [
                "cod_tipo_intervencao"  => (int) $dadosSsm['tipoIntervencao'],
                "cod_servico"           => (int) $dadosSsm['servicoSsm'],
                "cod_un_equipe"         => (int) $unEquipe,
                "nivel"                 => (string) $dadosSsm['nivelSsm'],
                "complemento"           => (string) $dadosSsm['complementoServicoSsm'],
                "cod_linha"             => (int)$dadosSsm['linhaSsm'],
                "cod_trecho"            => (int)$dadosSsm['trechoSsm'],
                "cod_ponto_notavel"     => (int)$dadosSsm['pontoNotavelSsm'],
                "cod_grupo"             => (int)$dadosSsm['grupoSistemaSsm'],
                "cod_sistema"           => (int)$dadosSsm['sistemaSsm'],
                "cod_subsistema"        => (int)$dadosSsm['subSistemaSsm'],
                "cod_via"               => (int)$dadosSsm['viaSsm'],
                "km_inicial"            => (string)$dadosSsm['kmInicialSsm'],
                "km_final"              => (string)$dadosSsm['kmFinalSsm'],
                "posicao"               => (string)$dadosSsm['posicaoSsm'],
                "complemento_local"     => (string)$dadosSsm['complementoLocalSsm'],
                "cod_local_grupo" => (int)$dadosSsm['localgrupo'],
                "cod_sublocal_grupo" => (int)$dadosSsm['sublocalgrupo']
            ], [
                "cod_ssm"               => (int)$dadosSsm['codigoSsm']
            ]);

            //######################## Gerar Osm ######################
            //######################## Gerar Osm ######################
            //######################## Gerar Osm ######################
                $insertOsm = $this->medoo->insert("osm_falha", [
                    "cod_ssm" => (int)$dadosSsm['codigoSsm'],
                    "grupo_atuado" => (int)$dadosSsm['grupoSistemaSsm'],
                    "sistema_atuado" => (int)$dadosSsm['sistemaSsm'],
                    "subsistema_atuado" => (int)$dadosSsm['subSistemaSsm'],
                    "trecho_atuado" => (int)$dadosSsm['trechoSsm'],
                    "cod_local_grupo" => (int)$dadosSsm['localgrupo'],
                    "cod_sublocal_grupo" => (int)$dadosSsm['sublocalgrupo'],
                    "cod_linha_atuado" => (int)$dadosSsm['linhaSsm'],
                    "cod_ponto_notavel_atuado" => (int)$dadosSsm['pontoNotavelSsm'],
                    "cod_via" => (int)$dadosSsm['viaSsm'],
                    "complemento_local" => (string)$dadosSsm['complementoLocalSsm'],
                    "posicao_atuado" => (string)$dadosSsm['posicaoSsm'],
                    "posicao_atuado" => (string)$dadosSsm['posicaoSsm'],
                    "km_inicial" => (string)$dadosSsm['kmInicialSsm'],
                    "km_final" => (string)$dadosSsm['kmFinalSsm'],
                    "data_abertura" => $time
                ]);

                // ####################### Insert OSM_SERVICO ###########################
                // ####################### Insert OSM_SERVICO ###########################
                // ####################### Insert OSM_SERVICO ###########################

                $insertServicoOsm = $this->medoo->insert("osm_servico", [
                    "cod_servico" => (int)$dadosSsm['servicoSsm'],
                    "cod_osm" => (int)$insertOsm,
                    "complemento" => (string)$dadosSsm['complementoServicoSsm']
                ]);

                //Atualizando dados gerais da OSM na tabela material_rodante_osm
                //Atualizando dados gerais da OSM na tabela material_rodante_osm
                //Atualizando dados gerais da OSM na tabela material_rodante_osm

            if($dadosSsm['grupoSistemaSsm'] == 22 || $dadosSsm['grupoSistemaSsm'] == 23 || $dadosSsm['grupoSistemaSsm'] == 26) {
                $insertOsmMr = $this->medoo->insert("material_rodante_osm", [
                    "cod_osm" => (int)$insertOsm,
                    "cod_veiculo" => (int)$dadosSsm['veiculoMrSsm'],
                    "cod_carro" => (int)$dadosSsm['carroMrSsm'],
                    "odometro" => (int)$dadosSsm['odometroMrSsm'],
                    "cod_prefixo" => (int)$dadosSsm['prefixoMrSsm'],
                    "cod_grupo" => (int)$dadosSsm['grupoSistemaSsm']
                ]);

                if($alterDispo == '12')
                  $this->medoo->update('veiculo', ["disponibilidade" => 'n'], ["cod_veiculo" => (int)$dadosSsm['veiculoMrSsm']]);
            }

                //######################## Inserir Status Osm ######################
                //######################## Inserir Status Osm ######################
                //######################## Inserir Status Osm ######################

                $insertStatusOsm = $this->medoo->insert("status_osm", [
                    "cod_osm" => (int)$insertOsm,
                    "cod_status" => (int)10, // Execu��o
                    "data_status" => $time,
                    "usuario" => (int)$dadosUsuario['cod_usuario']
                ]);

                //######################## Update Osm c/ ultimo status ######################

                $updateOsmStatus = $this->medoo->update("osm_falha", [
                    "cod_ostatus" => (int)$insertStatusOsm
                ], [
                    "cod_osm" => (int)$insertOsm
                ]);
//            }
        }

        unset($_SESSION['gerarOsm']);

        //Mensagem para o alerta RealTime
        $_SESSION['notificacao']['mensag'] = "Ssm n. {$dadosSsm['codigoSsm']} autorizada.
                                        <br />Osm n. {$insertOsm} aberta.
                                        <br />Por: {$dadosUsuario['usuario']}.";

        $selectVSsm = $this->medoo->select('v_ssm', "*", ['cod_ssm' => (int) $dadosSsm['codigoSsm']]);
        $selectVSsm = $selectVSsm[0];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']         = "Aberta";
        $_SESSION['notificacao']['tabela']       = "Osm";
        $_SESSION['notificacao']['statusAntigo'] = $hasSsm['nome_status'];

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero']           = $insertOsm;
        $_SESSION['notificacao']['numeroSsm']        = $dadosSsm['codigoSsm'];
        $_SESSION['notificacao']['dataAbertura']     = $time;
        $_SESSION['notificacao']['nivel']            = $selectVSsm['nivel'];

        $_SESSION['notificacao']['veiculo']          = $selectVSsm['nome_veiculo'];

        $_SESSION['notificacao']['nomeEquipe']       = $selectVSsm['nome_equipe'];
        $_SESSION['notificacao']['nomeUnidade']      = $selectVSsm['nome_unidade'];
        $_SESSION['notificacao']['siglaEquipe']      = $selectVSsm['sigla_equipe'];
        $_SESSION['notificacao']['siglaUnidade']     = $selectVSsm['sigla_unidade'];

        $_SESSION['notificacao']['nomeLinha']        = $selectVSsm['nome_linha'];
        $_SESSION['notificacao']['nomeTrecho']       = $selectVSsm['descricao_trecho'];

        $_SESSION['notificacao']['subChannelEquipe'] = $selectVSsm['cod_un_equipe'];
        $_SESSION['notificacao']['nomeUsuario'] = $dadosUsuario['usuario'];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}
