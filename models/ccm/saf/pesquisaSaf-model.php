<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 20/08/2015
 * Time: 16:30
 */
class PesquisaSafModel extends MainModel
{
    private $dadosReturn;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo   = $medoo;
        $dadosPesquisa = $_POST;

        //Verifica se h� valores e acrescenta na variavel where

        //#########Dados Gerais
        if (!empty($dadosPesquisa['codigoSaf'])) {
            $where[] = "cod_saf = {$dadosPesquisa['codigoSaf']}";
        }

        if (!empty($dadosPesquisa['solicitanteUsuarioPesquisa'])) {
            $where[] = "cod_usuario = {$dadosPesquisa['solicitanteUsuarioPesquisa']}";
        }

        //#########Status
        if (!empty($dadosPesquisa['statusPesquisa'])) {
            $where[] = "cod_status = {$dadosPesquisa['statusPesquisa']}";
        }

        //=====DATA STATUS
        if(!empty($dadosPesquisa['dataPartir']))
            $dataPartir = $this->inverteData($dadosPesquisa['dataPartir']);

        if(!empty($dadosPesquisa['dataRetroceder']))
            $dataRetroceder = $this->inverteData($dadosPesquisa['dataRetroceder']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "data_status >= '{$dataPartir}'";
                $where[] = "data_status <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "data_status >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "data_status < '{$dataRetroceder}'";
                }
            }
        }

        //=====DATA ABERTURA
        if(!empty($dadosPesquisa['dataPartirAbertura']))
            $dataPartirAbertura = $this->inverteData($dadosPesquisa['dataPartirAbertura']);

        if(!empty($dadosPesquisa['dataAteAbertura']))
            $dataRetrocederAbertura = $this->inverteData($dadosPesquisa['dataAteAbertura']);

        if ($dataPartirAbertura != "" || $dataRetrocederAbertura != "") {
            if ($dataPartirAbertura == $dataRetrocederAbertura) {
                $dataPartirAbertura = $dataPartirAbertura . " 00:00:00";
                $dataRetrocederAbertura = $dataRetrocederAbertura . " 23:59:59";

                $where[] = "data_abertura >= '{$dataPartirAbertura}'";
                $where[] = "data_abertura <= '{$dataRetrocederAbertura}'";

            } else {
                if (!empty($dataPartirAbertura)) {
                    $dataPartirAbertura = $dataPartirAbertura . " 00:00:00";
                    $where[] = "data_abertura >= '{$dataPartirAbertura}'";
                }

                if (!empty($dataRetrocederAbertura)) {
                    $dataRetrocederAbertura = $dataRetrocederAbertura . " 23:59:59";
                    $where[] = "data_abertura < '{$dataRetrocederAbertura}'";
                }
            }
        }


        //###########Local
        if (!empty($dadosPesquisa['linhaPesquisaSaf'])) {
            $where[] = "cod_linha = {$dadosPesquisa['linhaPesquisaSaf']}";
        }

        if (!empty($dadosPesquisa['trechoPesquisaSaf'])) {
            $where[] = "cod_trecho = {$dadosPesquisa['trechoPesquisaSaf']}";
        }

        if (!empty($dadosPesquisa['pnPesquisaSaf'])) {
            $where[] = "cod_ponto_notavel = {$dadosPesquisa['pnPesquisaSaf']}";
        }

        if (!empty($dadosPesquisa['localGrupo'])) {
            $where[] = "cod_local_grupo = {$dadosPesquisa['localGrupo']}";
        }

        if (!empty($dadosPesquisa['subLocalGrupo'])) {
            $where[] = "cod_sublocal_grupo = {$dadosPesquisa['subLocalGrupo']}";
        }

        //############Solicitante
        if (!empty($dadosPesquisa['solicitanteOrigemPesquisaSaf'])) {
            $where[] = "cod_tipo_funcionario = {$dadosPesquisa['solicitanteOrigemPesquisaSaf']}";
        }

        if (!empty($dadosPesquisa['solicitanteMatriculaPesquisa'])) {
            $where[] = "matricula = '{$dadosPesquisa['solicitanteMatriculaPesquisa']}'";
        }

        if (!empty($dadosPesquisa['solicitanteCpfPesquisa'])) {
            $where[] = "cpf = '{$dadosPesquisa['solicitanteCpfPesquisa']}'";
        }

        //#############Falha
        if (!empty($dadosPesquisa['grupoSistemaPesquisa'])) {
            $where[] = "cod_grupo = {$dadosPesquisa['grupoSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['sistemaPesquisa'])) {
            $where[] = "cod_sistema = {$dadosPesquisa['sistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['subSistemaPesquisa'])) {
            $where[] = "cod_subsistema = {$dadosPesquisa['subSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['avariaPesquisa'])) {
            $where[] = "cod_avaria = {$dadosPesquisa['avariaPesquisa']}";
        }

        if (!empty($dadosPesquisa['nivelPesquisa'])) {
            $where[] = "nivel = '{$dadosPesquisa['nivelPesquisa']}'";
        }

        //#########Material Rodante
        if (!empty($dadosPesquisa['veiculoPesquisa'])) {
            $where[] = "cod_veiculo = {$dadosPesquisa['veiculoPesquisa']}";
        }

        if (!empty($dadosPesquisa['carroAvariadoPesquisa'])) {
            $where[] = "cod_carro = {$dadosPesquisa['carroAvariadoPesquisa']}";
        }

        if (!empty($dadosPesquisa['prefixoPesquisa'])) {
            $where[] = "cod_prefixo = {$dadosPesquisa['prefixoPesquisa']}";
        }

        if (!empty($dadosPesquisa['odometroPartirPesquisa'])) {
            $where[] = "odometro <= {$dadosPesquisa['odometroPartirPesquisa']}";
        }

        if (!empty($dadosPesquisa['odomentroAtePesquisa'])) {
            $where[] = "odometro >= {$dadosPesquisa['odomentroAtePesquisa']}";
        }

        //SINTAX SQL para pesquisa no PostGres
        $sql = "SELECT {$dadosPesquisa['whereSql']} FROM v_saf";

        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }
        $sql = $sql . " ORDER BY cod_saf";

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $this->dadosReturn = (boolval(false));
        } else {
            $this->dadosReturn = $resultado;
        }
    }

    function getDados(){
        return $this->dadosReturn;
    }
}
