<?php

class DevolverCancelarSafModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosSaf = $_SESSION['dados'];

        //############## Status_SAF #####################
        //############## Status_SAF #####################
        //############## Status_SAF #####################
        $time = date('d-m-Y H:i:s', time());
        if ($dadosSaf['acaoModal'] == "devolver") {
            $motivo = $dadosSaf['motivoAcao'] . ' => Devolvido por: ' . $dadosUsuario['usuario'] . ' - ' . $time;
            $cod_status = 3; //Devolvido
            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Saf n. {$dadosSaf['codigo']} devolvida.<br />Por: {$dadosUsuario['usuario']}";
        } else {
            $motivo = $dadosSaf['motivoAcao'] . ' => Cancelada por: ' . $dadosUsuario['usuario'] . ' - ' . $time;

            $cod_status = 2; //cancelada

            //Alerta Especifico para o Usuario
            if($dadosUsuario['nivel'] == "2.3")
                $_SESSION['alertaAcao'] = "Solicita��o de Abertura de Falha n. {$dadosSaf['codigo']} cancelada";

            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Saf n. {$dadosSaf['codigo']} cancelada.<br />Por:  {$dadosUsuario['usuario']}";
        }

        $this->alterarStatusSaf($dadosSaf['codigo'], $cod_status, $time, $dadosUsuario, $motivo);

        unset($_SESSION['dados']);

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']   = "Deletar";
        $_SESSION['notificacao']['tabela'] = "Saf";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero'] = $dadosSaf['codigo'];

    }
}
