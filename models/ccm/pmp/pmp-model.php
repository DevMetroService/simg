<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 02/12/2015
 * Time: 09:01
 */


class SspModel extends MainModel{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario) {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadoSsp = $_SESSION['dadosSsp'];

        $codigoSsp = 0;

        $isSsp = $this->medoo->select('ssp','*', ['cod_ss_programacao' => $dadoSsp['codigoSsp'] ]);
        $time = date('d-m-Y H:i:s', time());

        $codigoEquipe = $this->medoo->select("equipe", "cod_equipe", [ "sigla" => $dadoSsp['equipeSsm']]);
        $codigoEquipe = $codigoEquipe[0];
        $codigoUsuario = $this->medoo->select("usuario", "cod_usuario", ["usuario" => $dadoSsp['responsavelSsm']]);
        $codigoUsuario = $codigoUsuario[0];

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
            "AND" =>[
                "cod_equipe"        => (int) $codigoEquipe,
                "cod_unidade"       => (int) $dadoSsp['localSsp']
            ]
        ]);

        if(!empty($isSsp)){
            $insertInSsp = $this->medoo->update('ssp',[
                'cod_servico'                   =>(int)$dadoSsp['servicoSsp'],
                'cod_tipo_intervencao'          =>(int)$dadoSsp['tipoIntervencao'],
                'data_abertura'                 =>$time,
                'cod_un_equipe'                 =>(int)$unEquipe,
                'cod_usuario'                   =>(int)$codigoUsuario,
                'complemento'                   =>(string)$dadoSsp['complementoServicoSsp']
            ],[
                'cod_ss_programacao'            =>(int)$dadoSsp['numeroSsp']
            ]);

            $codigoSsp = (int)$dadoSsp['numeroSsp'];

        }else{
            $insertInSsp = $this->medoo->insert('ssp',[
                'cod_servico'                   =>(int)$dadoSsp['servicoSsp'],
                'cod_tipo_intervencao'          =>(int)$dadoSsp['tipoIntervencao'],
                'data_abertura'                 => $time,
                'cod_un_equipe'                 =>(int)$unEquipe,
                'cod_usuario'                   =>(int)$codigoUsuario,
                'complemento'                   =>(string)$dadoSsp['complementoServicoSsp']
            ]);

            $codigoSsp = (int)$insertInSsp;

        }

        $insertStatusAtual = $this->medoo->insert("status_ssm",[
            "cod_status"        =>(int) 12,
            "cod_ssm"           =>(int)$codigoSsp,
            "data_status"       => $this->inverteData($time),
            "usuario"           => (int)  $dadosUsuario['cod_usuario']
        ]);

        $updateStatusInSsp = $this->medoo->update('ssp',[
            'cod_ss_pstatus'        => (int) $insertStatusAtual
        ],[
            'cod_ss_programacao'    => (int) $codigoSsp
        ]);

        unset($_SESSION['dadoSsp']);

    }


}