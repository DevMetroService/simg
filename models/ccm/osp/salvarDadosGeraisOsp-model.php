<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/07/2015
 * Time: 14:22
 */

class SalvarDadosGeraisOspModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosOsp = $_SESSION['dadosGeraisOsp'];

        $codUsuario = $this->getCodUsuario($dadosOsp['responsavelPreenchimento']);

        $time = date('d-m-Y H:i:s', time());

        $this->osResponsavel($codUsuario, $dadosOsp['codigoOs'], "osp");

        //Atualizando dados gerais da Osp na tabela Osp_falha
        //Atualizando dados gerais da Osp na tabela Osp_falha
        //Atualizando dados gerais da Osp na tabela Osp_falha
        $updateOsp = $this->medoo->update("osp",[
            "grupo_atuado"              => (int) $dadosOsp['grupoOsAtuado'],
            "sistema_atuado"            => (int) $dadosOsp['sistemaOsAtuado'],
            "subsistema_atuado"         => (int) $dadosOsp['subSistemaOsAtuado'],
            "cod_linha_atuado"          => (int) $dadosOsp['linhaOsAtuado'],
            "trecho_atuado"             => (int) $dadosOsp['trechoOsAtuado'],
            "complemento"               => (string) $dadosOsp['complementoLocalOsAtuado'],
            "km_inicial"                => (string) $dadosOsp['kmInicialOs'],
            "km_final"                  => (string) $dadosOsp['kmFinalOs'],
            "posicao_atuado"            => (string) $dadosOsp['posicaoAtuado'],
            "cod_via"                   => (int) $dadosOsp['viaOsAtuada'],
            "cod_ponto_notavel_atuado"  => (int) $dadosOsp['pontoNotavelOsAtuado'],
        ],[
            "cod_osp"                   => (int) $dadosOsp['codigoOs']
        ]);


        //Atualizando dados gerais da Osp na tabela Osp_servico
        //Atualizando dados gerais da Osp na tabela Osp_servico
        //Atualizando dados gerais da Osp na tabela Osp_servico
        $servicoOsp = $this->medoo->select("osp_servico","*",["cod_osp" => $dadosOsp['codigoOs']]);
        $servicoOsp = $servicoOsp[0];

        if(empty($servicoOsp)){
            $insertOspServico = $this->medoo->insert("osp_servico",[
                "cod_osp"               => (int) $dadosOsp['codigoOs'],
                "cod_servico"           => (int) $dadosOsp['servicoExecutadoOs'],
                "unidade_medida"        => (int) $dadosOsp['unidadeTotalServico'],
                "unidade_tempo"         => (int) $dadosOsp['unidadeTempoServico'],
                "qtd_pessoas"           => (int) $dadosOsp['efetivoServico'],
                "total_servico"         => (int) $dadosOsp['totalServico'],
                "tempo"                 => (int) $dadosOsp['tempoServico'],
                "complemento"           => (string) $dadosOsp['complementoServico']
            ]);
        }else{
            $updateOspServico = $this->medoo->update("osp_servico",[
                "cod_servico"           => (int) $dadosOsp['servicoExecutadoOs'],
                "unidade_medida"        => (int) $dadosOsp['unidadeTotalServico'],
                "unidade_tempo"         => (int) $dadosOsp['unidadeTempoServico'],
                "qtd_pessoas"           => (int) $dadosOsp['efetivoServico'],
                "total_servico"         => (int) $dadosOsp['totalServico'],
                "tempo"                 => (int) $dadosOsp['tempoServico'],
                "complemento"           => (string) $dadosOsp['complementoServico']
            ],[
                "cod_osp"               => (int) $dadosOsp['codigoOs'],
            ]);
        }

        $mrOsp = $this->medoo->select("material_rodante_osp","*",["cod_osp" => $dadosOsp['codigoOs']]);
        $mrOsp = $mrOsp[0];

        if(empty($mrOsp)){
            $insertOspMr = $this->medoo->insert("material_rodante_osp",[
                "cod_osp"          => (int) $dadosOsp['codigoOs'],
                "cod_veiculo"      => (int) $dadosOsp['veiculoMrOsm'],
                "cod_carro"        => (int) $dadosOsp['carroMrOs'],
                "odometro"         => (int) $dadosOsp['odometroMrOsm'],
//                "cod_prefixo"      => (int) $dadosOsp['prefixoMrOsm'],
                "cod_grupo"        => (int) $dadosOsp['grupoOsAtuado']
            ]);
        }else{
            $updateOspMr = $this->medoo->update("material_rodante_osp",[
                "cod_veiculo"      => (int) $dadosOsp['veiculoMrOsm'],
                "cod_carro"        => (int) $dadosOsp['carroMrOs'],
                "odometro"         => (int) $dadosOsp['odometroMrOsm'],
//                "cod_prefixo"      => (int) $dadosOsp['prefixoMrOsm'],
                "cod_grupo"        => (int) $dadosOsp['grupoOsAtuado']
            ],[
                "cod_osp"               => (int) $dadosOsp['codigoOs'],
            ]);
        }
    }
}