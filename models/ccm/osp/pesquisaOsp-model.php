<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 24/08/2015
 * Time: 14:29
 */

class PesquisaOspModel extends MainModel
{
    private $dadosReturn;

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $dadosPesquisa = $_POST;

        $join = '';

        //Verifica se h� valores e acrescenta na variavel where

        //#########Dados Gerais
        if (!empty($dadosPesquisa['numeroPesquisaOsp'])) {
            $where[] = "osp.cod_osp = {$dadosPesquisa['numeroPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['codigoSsp'])) {
            $where[] = "osp.cod_ssp = {$dadosPesquisa['codigoSsp']}";
        }

        if (!empty($dadosPesquisa['responsavelPreenchimentoPesquisaOsp'])) {
            $where[] = "osp.usuario_responsavel = {$dadosPesquisa['responsavelPreenchimentoPesquisaOsp']}";
        }

        //#########Datas
        if (!empty($dadosPesquisa['situacaoPesquisaOsp'])) {
            $where[] = "status.cod_status = '{$dadosPesquisa['situacaoPesquisaOsp']}'";
        }

        if(!empty($dadosPesquisa['dataPartirOsp']) || !empty($dadosPesquisa['dataAteOsp'])){
            $dataPartir     = $this->inverteData($dadosPesquisa['dataPartirOsp']);
            $dataRetroceder = $this->inverteData($dadosPesquisa['dataAteOsp']);

            if($dataPartir == $dataRetroceder && !empty($dataPartir)){
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "sto.data_status >= '{$dataPartir}'";
                $where[] = "sto.data_status <= '{$dataRetroceder}'";

            }else{
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "sto.data_status >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "sto.data_status < '{$dataRetroceder}'";
                }
            }
        }

        if(!empty($dadosPesquisa['dataPartirOspAbertura']) || !empty($dadosPesquisa['dataAteOspAbertura'])){
            $dataPartirA     = $this->inverteData($dadosPesquisa['dataPartirOspAbertura']);
            $dataRetrocederA = $this->inverteData($dadosPesquisa['dataAteOspAbertura']);

            if($dataPartirA == $dataRetrocederA && !empty($dataPartirA)){
                $dataPartirA = $dataPartirA . " 00:00:00";
                $dataRetrocederA = $dataRetrocederA . " 23:59:59";

                $where[] = "osp.data_abertura >= '{$dataPartirA}'";
                $where[] = "osp.data_abertura <= '{$dataRetrocederA}'";

            }else{
                if (!empty($dataPartirA)) {
                    $dataPartirA = $dataPartirA . " 00:00:00";
                    $where[] = "osp.data_abertura >= '{$dataPartirA}'";
                }

                if (!empty($dataRetrocederA)) {
                    $dataRetrocederA = $dataRetrocederA . " 23:59:59";
                    $where[] = "osp.data_abertura < '{$dataRetrocederA}'";
                }
            }
        }

        if(!empty($dadosPesquisa['dataPartirOspEncerramento']) || !empty($dadosPesquisa['dataAteOspEncerramento'])){
            $dataPartirE     = $this->inverteData($dadosPesquisa['dataPartirOspEncerramento']);
            $dataRetrocederE = $this->inverteData($dadosPesquisa['dataAteOspEncerramento']);

            if($dataPartirE == $dataRetrocederE && !empty($dataPartirE)){
                $dataPartirE = $dataPartirE . " 00:00:00";
                $dataRetrocederE = $dataRetrocederE . " 23:59:59";

                $where[] = "oe.data_encerramento >= '{$dataPartirE}'";
                $where[] = "oe.data_encerramento <= '{$dataRetrocederE}'";

            }else{
                if (!empty($dataPartirE)) {
                    $dataPartirE = $dataPartirE . " 00:00:00";
                    $where[] = "oe.data_encerramento >= '{$dataPartirE}'";
                }

                if (!empty($dataRetrocederE)) {
                    $dataRetrocederE = $dataRetrocederE . " 23:59:59";
                    $where[] = "oe.data_encerramento < '{$dataRetrocederE}'";
                }
            }
        }

        //###########Local
        if (!empty($dadosPesquisa['linhaPesquisaOsp'])) {
            $where[] = "osp.cod_linha_atuado = {$dadosPesquisa['linhaPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['trechoPesquisaOsp'])) {
            $where[] = "osp.trecho_atuado = {$dadosPesquisa['trechoPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['pnPesquisaOsp'])) {
            $where[] = "osp.cod_ponto_notavel_atuado = {$dadosPesquisa['pnPesquisaOsp']}";
        }

        //############Servi�o
        if (!empty($dadosPesquisa['grupoPesquisaOsp'])) {
            $where[] = "osp.grupo_atuado = {$dadosPesquisa['grupoPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['sistemaPesquisaOsp'])) {
            $where[] = "osp.sistema_atuado = {$dadosPesquisa['sistemaPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['subSistemaPesquisa'])) {
            $where[] = "osp.subsistema_atuado = {$dadosPesquisa['subSistemaPesquisa']}";
        }

        if (!empty($dadosPesquisa['servicoPesquisaOsp'])) {
            $where[] = "os.cod_servico = {$dadosPesquisa['servicoPesquisaOsp']}";
        }

        //############Registro de Execucao
        if (!empty($dadosPesquisa['unidadePesquisaOsp'])) {
            $where[] = "un_equipe.cod_unidade = {$dadosPesquisa['unidadePesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['equipePesquisaOsp'])) {
            $where[] = "un_equipe.cod_equipe = {$this->getCodEquipe($dadosPesquisa['equipePesquisaOsp'])}";
        }

        if (!empty($dadosPesquisa['causaPesquisaOsp'])) {
            $where[] = "oreg.cod_causa = {$dadosPesquisa['causaPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['agCausadorPesquisaOsp'])) {
            $where[] = "oreg.cod_ag_causador = {$dadosPesquisa['agCausadorPesquisaOsp']}";
        }

        //############Fechamento Pesquisa
        if (!empty($dadosPesquisa['tipoFechamentoPesquisaOsp'])) {
            $where[] = "oe.cod_tipo_fechamento = {$dadosPesquisa['tipoFechamentoPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['tipoPendenciaPesquisaOsp'])) {
            $where[] = "oe.cod_pendencia = {$dadosPesquisa['tipoPendenciaPesquisaOsp']}";
        }

        if (!empty($dadosPesquisa['liberacaoPesquisaOsp'])) {
            $where[] = "oe.liberacao = '{$dadosPesquisa['liberacaoPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['maquinasEquipamentosPesquisaOsp'])) {
            $where[] = "omq.cod_material = '{$dadosPesquisa['maquinasEquipamentosPesquisaOsp']}'";
            $join .= ' LEFT JOIN osp_maquina omq ON osp.cod_osp = omq.cod_osp ';
        }

        if (!empty($dadosPesquisa['materiaisPesquisaOsp']) || !empty($dadosPesquisa['estadoPesquisaOsp']) || !empty($dadosPesquisa['origemPesquisaOsp'])) {
            $join .= ' LEFT JOIN osp_material oma ON osp.cod_osp = oma.cod_osp ';
        }

        if (!empty($dadosPesquisa['materiaisPesquisaOsp'])) {
            $where[] = "oma.cod_material = '{$dadosPesquisa['materiaisPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['estadoPesquisaOsp'])) {
            $where[] = "estado = '{$dadosPesquisa['estadoPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['origemPesquisaOsp'])) {
            $where[] = "origem = '{$dadosPesquisa['origemPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['veiculoPesquisaOsp'])) {
            $where[] = "mro.cod_veiculo = '{$dadosPesquisa['veiculoPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['carroAvariadoPesquisaOsp'])) {
            $where[] = "mro.cod_carro = '{$dadosPesquisa['carroAvariadoPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['odometroPartirPesquisaOsp'])) {
            $where[] = "odometro > '{$dadosPesquisa['odometroPartirPesquisaOsp']}'";
        }

        if (!empty($dadosPesquisa['odometroAtePesquisaOsp'])) {
            $where[] = "odometro < '{$dadosPesquisa['odometroAtePesquisaOsp']}'";
        }

        ////SINTAX SQL para pesquisa no PostGres
        $sql = 'SELECT osp.cod_osp, osp.cod_ssp,
                      linha.nome_linha, trecho.descricao_trecho, 
                      osp.data_abertura AS data_abertura_osp, 
                      os.complemento AS complemento_servico, servico.nome_servico,
                      status.nome_status, tf.nome_fechamento, grupo_atuado
                      
    
                            FROM osp
                            JOIN status_osp sto USING (cod_ospstatus)
                            JOIN status USING (cod_status)
                            JOIN linha ON cod_linha = cod_linha_atuado
                            JOIN trecho ON cod_trecho = trecho_atuado
                            LEFT JOIN material_rodante_osp mro ON(osp.cod_osp = mro.cod_osp)
                            LEFT JOIN osp_encerramento oe ON oe.cod_osp = osp.cod_osp
                            LEFT JOIN tipo_fechamento tf USING(cod_tipo_fechamento)
                            LEFT JOIN osp_registro oreg ON oreg.cod_osp = osp.cod_osp
                            JOIN osp_servico os ON os.cod_osp = osp.cod_osp
                            JOIN servico USING (cod_servico)
                            LEFT JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe' . $join;



        //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
        //Verifica se h� dados e divide os campos adicionando 'AND'
        if (!empty($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

        //Executa o select a partir do medoo
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $this->dadosReturn = (boolval(false));
        } else {
            $this->dadosReturn = $resultado;
        }
    }

    function getDados(){
        return $this->dadosReturn;
    }
}