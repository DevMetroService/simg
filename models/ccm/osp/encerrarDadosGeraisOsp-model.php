<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 12/06/2018
 * Time: 09:49
 */

class EncerrarDadosGeraisOspModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosOsp = $_SESSION['dadosGeraisOsp'];
        unset($_SESSION['dadosGeraisOsp']);

        $codUsuario = $this->getCodUsuario($dadosOsp['responsavelPreenchimento']);

        $time = date('d-m-Y H:i:s', time());

        $this->osResponsavel($codUsuario, $dadosOsp['codigoOs'], "osp");

        //Atualizando dados gerais da OSM na tabela osp_falha
        $updateOsp = $this->medoo->update("osp", [
            "grupo_atuado" => (int)$dadosOsp['grupoOsAtuado'],
            "sistema_atuado" => (int)$dadosOsp['sistemaOsAtuado'],
            "subsistema_atuado" => (int)$dadosOsp['subSistemaOsAtuado'],
            "cod_linha_atuado" => (int)$dadosOsp['linhaOsAtuado'],
            "trecho_atuado" => (int)$dadosOsp['trechoOsAtuado'],
            "complemento" => (string)$dadosOsp['complementoLocalOsAtuado'],
            "km_inicial" => (string)$dadosOsp['kmInicialOs'],
            "km_final" => (string)$dadosOsp['kmFinalOs'],
            "posicao_atuado" => (string)$dadosOsp['posicaoAtuado'],
            "cod_via" => (int)$dadosOsp['viaOsAtuada'],
            "cod_ponto_notavel_atuado" => (int)$dadosOsp['pontoNotavelOsAtuado']
        ], [
            "cod_osp" => (int)$dadosOsp['codigoOs']
        ]);


        //Atualizando dados gerais da OSM na tabela osp_servico
        $servicoOsp = $this->medoo->select("osp_servico", "*", ["cod_osp" => $dadosOsp['codigoOs']]);
        $servicoOsp = $servicoOsp[0];

        if (empty($servicoOsp)) {
            $insertOspServico = $this->medoo->insert("osp_servico",[
                "cod_osp"               => (int) $dadosOsp['codigoOs'],
                "cod_servico"           => (int) $dadosOsp['servicoExecutadoOs'],
                "unidade_medida"        => (int) $dadosOsp['unidadeTotalServico'],
                "unidade_tempo"         => (int) $dadosOsp['unidadeTempoServico'],
                "qtd_pessoas"           => (int) $dadosOsp['efetivoServico'],
                "total_servico"         => (int) $dadosOsp['totalServico'],
                "tempo"                 => (int) $dadosOsp['tempoServico'],
                "complemento"           => (string) $dadosOsp['complementoServico']
            ]);
        } else {
            $updateOspServico = $this->medoo->update("osp_servico",[
                "cod_servico"           => (int) $dadosOsp['servicoExecutadoOs'],
                "unidade_medida"        => (int) $dadosOsp['unidadeTotalServico'],
                "unidade_tempo"         => (int) $dadosOsp['unidadeTempoServico'],
                "qtd_pessoas"           => (int) $dadosOsp['efetivoServico'],
                "total_servico"         => (int) $dadosOsp['totalServico'],
                "tempo"                 => (int) $dadosOsp['tempoServico'],
                "complemento"           => (string) $dadosOsp['complementoServico']
            ],[
                "cod_osp"               => (int) $dadosOsp['codigoOs'],
            ]);
        }

        //Atualizando dados gerais da OSM na tabela material_rodante_osp
        $mrOsp = $this->medoo->select("material_rodante_osp","*",["cod_osp" => $dadosOsp['codigoOs']]);
        $mrOsp = $mrOsp[0];

        if(empty($mrOsp)){
            $insertOspMr = $this->medoo->insert("material_rodante_osp",[
                "cod_osp"          => (int) $dadosOsp['codigoOs'],
                "cod_veiculo"      => (int) $dadosOsp['veiculoMrOsm'],
                "cod_carro"        => (int) $dadosOsp['carroMrOsm'],
                "odometro"         => (int) $dadosOsp['odometroMrOsm'],
                "cod_prefixo"      => (int) $dadosOsp['prefixoMrOsm'],
                "cod_grupo"        => (int) $dadosOsp['grupoOsAtuado']
            ]);
        }else{
            $updateOspMr = $this->medoo->update("material_rodante_osp",[
                "cod_veiculo"      => (int) $dadosOsp['veiculoMrOsm'],
                "cod_carro"        => (int) $dadosOsp['carroMrOsm'],
                "odometro"         => (int) $dadosOsp['odometroMrOsm'],
                "cod_prefixo"      => (int) $dadosOsp['prefixoMrOsm'],
                "cod_grupo"        => (int) $dadosOsp['grupoOsAtuado']
            ],[
                "cod_osp"               => (int) $dadosOsp['codigoOs'],
            ]);
        }

        //Encerrando OSM
        $codSsp = $this->medoo->select("osp", "cod_ssp", ["cod_osp" => $dadosOsp['codigoOs']]);
        $codSsp = $codSsp[0];

        //Insere o status encerrada para OSM independete da sua condi��o
        $insertStatusOsp = $this->medoo->insert("status_osp", [
            "cod_osp" => (int)$dadosOsp['codigoOs'],
            "cod_status" => (int)11,    //Encerrado
            "data_status" => $time,
            "usuario" => (int)$dadosUsuario['cod_usuario']
        ]);

        $updateOsp = $this->medoo->update("osp", [
            "cod_ospstatus" => (int)$insertStatusOsp
        ], [
            "cod_osp" => (int)$dadosOsp['codigoOs']
        ]);

        //Verifica se existe tabela encerramento.
        $selectEncerramento = $this->medoo->select("osp_encerramento", "*", ["cod_osp" => $dadosOsp['codigoOs']]);

        //Altero Status para Encerrado.
        if (empty($selectEncerramento)) {
            if($dadosOsp['servicoExecutadoOs'] == '307' || $dadosOsp['servicoExecutadoOs'] == '362'){ // Manobra de Entrada
                $this->alterarStatusSsp($codSsp, 22, $time, $dadosUsuario); //Pendente

                $insertEncerramento = $this->medoo->insert("osp_encerramento", [
                    "cod_osp" => (int)$dadosOsp['codigoOs'],
                    "cod_tipo_fechamento" => (int)2,
                    "cod_pendencia" => (int)56,
                    "liberacao" => (string)'n',
                    "data_encerramento" => $time
                ]);

                //Mensagem para o alerta RealTime
                $_SESSION['notificacao']['mensag'] = "Osp n.{$dadosOsp['codigoOs']} encerrada.";
                $_SESSION['notificacao']['mensag'] .= "<br />Ssp n. " . $codSsp . " pendente";
                $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";
                $_SESSION['notificacao']['sspPendente'] = true;
            }else{ // Manobra de Sa�da
                $this->alterarStatusSsp($codSsp, 18, $time, $dadosUsuario); //Encerrado

                $insertEncerramento = $this->medoo->insert("osp_encerramento", [
                    "cod_osp" => (int)$dadosOsp['codigoOs'],
                    "cod_tipo_fechamento" => (int)1,
                    "liberacao" => (string)'s',
                    "data_encerramento" => $time
                ]);

                //Mensagem para o alerta RealTime
                $_SESSION['notificacao']['mensag'] = "Osp n.{$dadosOsp['codigoOs']} encerrada.";
                $_SESSION['notificacao']['mensag'] .= "<br />Ssp n. " . $codSsp . " finalizada.";
                $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";
            }
        }

        $viewSsp = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$codSsp]);
        $viewSsp = $viewSsp[0];

        if(!empty($_SESSION['notificacao']['sspPendente'])){
            $_SESSION['notificacao']['sspPendenteNumero']             = $codSsp;
            $_SESSION['notificacao']['sspPendenteDataProgramada']     = $this->parse_timestamp($viewSsp["data_programada"]);
            $_SESSION['notificacao']['sspPendenteQtdDias']            = $viewSsp["dias_servico"];

            $_SESSION['notificacao']['sspPendenteVeiculo']            = $viewSsp["nome_veiculo"];

            $_SESSION['notificacao']['sspPendenteServico']            = $viewSsp["nome_servico"];
            $_SESSION['notificacao']['sspPendenteComplementoServico'] = $viewSsp["complemento"];
        }

        $_SESSION['notificacao']['subChannelEquipe'] = $viewSsp['cod_un_equipe'];;
        $_SESSION['notificacao']['nomeUsuario'] = $dadosUsuario['usuario'];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']   = "Deletar";
        $_SESSION['notificacao']['tabela'] = "Osp";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero'] = $dadosOsp['codigoOs'];


    }
}

