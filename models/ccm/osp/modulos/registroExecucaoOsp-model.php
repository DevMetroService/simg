<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 27/07/2015
 * Time: 13:29
 */

class RegistroExecucaoOspModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $registroExecucao = $_SESSION['registroExecucao'];

        $this->osResponsavel($dadosUsuario['cod_usuario'], $registroExecucao['codigoOs'], "osp");

        $codEquipe = $this->getCodEquipe($registroExecucao['equipeExecucao']);

        $unEquipe = $this->medoo->select("un_equipe", "cod_un_equipe",[
            "AND" =>[
                "cod_equipe" => (int) $codEquipe,
                "cod_unidade" => (int) $registroExecucao['unidadeExecucao']
            ]
        ]);
        $unEquipe = $unEquipe[0];

        if(!empty($registroExecucao["cautelaExecucao"])){
            $cautela ="s";
        }else{
            $cautela ="n";
        }

        $registroExistente = $this->medoo->select("osp_registro","*",["cod_osp" =>(int)$registroExecucao['codigoOs'] ]);

        if(!empty($registroExistente)){
            $updateOsmRegistro = $this->medoo->update('osp_registro',[
                "cod_un_equipe"         => (int) $unEquipe,
                "cod_atuacao"           => (int) $registroExecucao['atuacaoExecucao'],
                "cod_veiculo"           => (int) $registroExecucao['transporteExecucao'],
                "clima"                 => (string) $registroExecucao['climaExecucao'],
                "desc_atuacao"          => (string) $registroExecucao['obsAtuacao'],
                "dados_complementar"    => (string) $registroExecucao['dadosComplementares'],
                "temperatura"           => (string) $registroExecucao['temperaturaExecucao'],
                "cautela"               => (string) $cautela,
                "km_inicial_cautela"    => (string) $registroExecucao['kmIniCautela'],
                "km_final_cautela"      => (string) $registroExecucao['kmFimCautela'],
                "restricao_veloc"       => (string) $registroExecucao['restringirVelocidadeExecucao']
            ],[
                "cod_osp"               => (int) $registroExecucao['codigoOs']
            ]);
        }else{
            $inserirOsmRegistro = $this->medoo->insert('osp_registro',[
                "cod_osp"               => (int) $registroExecucao['codigoOs'],
                "cod_ag_causador"       => (int) 22, //Inexistente
                "cod_un_equipe"         => (int) $unEquipe,
                "cod_causa"             => (int) 136, //Inexistente
                "cod_atuacao"           => (int) $registroExecucao['atuacaoExecucao'],
                "cod_veiculo"           => (int) $registroExecucao['transporteExecucao'],
                "clima"                 => (string) $registroExecucao['climaExecucao'],
                "desc_atuacao"          => (string) $registroExecucao['obsAtuacao'],
                "dados_complementar"    => (string) $registroExecucao['dadosComplementares'],
                "temperatura"           => (string) $registroExecucao['temperaturaExecucao'],
                "cautela"               => (string) $cautela,
                "km_inicial_cautela"    => (string) $registroExecucao['kmIniCautela'],
                "km_final_cautela"      => (string) $registroExecucao['kmFimCautela'],
                "restricao_veloc"       => (string) $registroExecucao['restringirVelocidadeExecucao']
            ]);
        }

        $this->medoo->update('material_rodante_osp',
            [
                "horimetro_tracao_ma" => $registroExecucao['horimetroTracaoMA'],
                "horimetro_gerador_ma" => $registroExecucao['horimetroGeradorMA'],
                "horimetro_tracao_mb" => $registroExecucao['horimetroTracaoMB'],
                "horimetro_gerador_mb" => $registroExecucao['horimetroGeradorMB'],
            ],
            [
                "cod_osm"  => (int) $registroExecucao['codigoOs'],
            ]
        );

        unset($_SESSION['registroExecucao']);
    }
}