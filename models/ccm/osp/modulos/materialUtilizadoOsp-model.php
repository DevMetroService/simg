<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/07/2015
 * Time: 09:42
 */

class MaterialUtilizadoOspModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosMateriais = $_SESSION['dadosMateriais'];

        if (!empty($_SESSION['dadosMaterialOs']['check']) && $_SESSION['dadosMaterialOs']['codigoOs'] == $dadosMateriais['codigoOs'])
            return;

        for ($i = 0; $i < 500; $i++) {
            if (!empty($dadosMateriais['retirarMaterial' . $i])) {
                $deleteMaterial = $this->medoo->delete("osp_material", [
                    "AND" =>
                        [
                            "cod_osp" => (int)$dadosMateriais['codigoOs'],
                            "cod_material" => (int)$dadosMateriais['codigoMaterialDelete' . $i]
                        ]
                ]);
            } else {
                if (!empty($dadosMateriais['codigoMaterial' . $i])) {
                    $insertMaterial = $this->medoo->insert("osp_material", [
                        "cod_osp" => (int)$dadosMateriais['codigoOs'],
                        "utilizado" => (double)$dadosMateriais['qtdUtilizado' . $i],
                        "cod_material" => (int)$dadosMateriais['codigoMaterial' . $i],
                        "unidade" => (int)$dadosMateriais['unidadeMedidaOs' . $i],
                        "estado" => (string)$dadosMateriais['estadoMaterial' . $i],
                        "origem" => (string)$dadosMateriais['origemMaterial' . $i],
                    ]);
                }
            }

            $_SESSION['dadosCheck']['material'] = $this->medoo->select("osp_material", "*", ["cod_osp" => (int)$dadosMateriais['codigoOs']]);
            unset($_SESSION['dadosMaoObra']);
        }
    }
}

