<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 09/07/2015
 * Time: 15:24
 */

class MaoObraOspModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosMaoObra = $_SESSION['dadosMaoObra'];

        $this->osResponsavel($dadosUsuario['cod_usuario'], $dadosMaoObra['codigoOs'], "osp");

        for($i = 1; $i<100; $i++){
            if(!empty($dadosMaoObra['retirarFuncionario'.$i])){
                $deleteMaoObra = $this->medoo->delete("osp_mao_de_obra",[
                    "cod_ospmao"            => (int) $dadosMaoObra['codMaoObraOs'.$i]
                ]);
            }else{
                if(!empty($dadosMaoObra['InicioServicoMaoObraOs'.$i])){
                    $insertMaoObra = $this->medoo->insert("osp_mao_de_obra",[
                        "cod_osp"           => (int) $dadosMaoObra['codigoOs'],
                        //TODO: Alterar nome do elemento HTML
                        "cod_funcionario"   => (int) $dadosMaoObra['matriculaFuncionarioMaoObraOs'.$i],
                        "data_inicio"       => (string) $dadosMaoObra['InicioServicoMaoObraOs'.$i],
                        "data_termino"      => (string) $dadosMaoObra['TerminoServicoMaoObraOs'.$i],
                        "total_hrs"         => (string) $dadosMaoObra['totalHoraMaoObraOs'.$i],
                    ]);
                }
            }

            unset($_SESSION['dadosMaoObra']);
        }
    }
}
