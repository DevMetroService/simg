<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 05/08/2015
 * Time: 15:46
 */

class EncerramentoOspModel extends MainModel
{
    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosEncerramento = $_SESSION['dadosEncerramento'];

        //Pega c�digo ssp para altera��o de status
        //Pega c�digo ssp para altera��o de status
        //Pega c�digo ssp para altera��o de status
        $codSsp = $this->medoo->select("osp", "cod_ssp", ["cod_osp" => $dadosEncerramento['codigoOs']]);
        $codSsp = $codSsp[0];

        //Pega tempo atual de encerramento
        $time = date('d-m-Y H:i:s', time());

        //verifica se h� informa��es na tabela desta OSP
        $selectEncerramento = $this->medoo->select("osp_encerramento", "*", ["cod_osp" => $dadosEncerramento['codigoOs']]);

        //Se, selectEncerramento for vazio, insert, do contrario, update
        if (empty($selectEncerramento)) {
            //Insere o status encerrada para OSP independete da sua condi��o
            //Insere o status encerrada para OSP independete da sua condi��o
            //Insere o status encerrada para OSP independete da sua condi��o
            $insertStatusOsp = $this->medoo->insert("status_osp", [
                "cod_osp"     => (int)$dadosEncerramento['codigoOs'],
                "cod_status"  => (int)11,    //Encerrado
                "data_status" => $time,
                "usuario"     => (int)  $dadosUsuario['cod_usuario']
            ]);

            $updateOsp = $this->medoo->update("osp", [
                "cod_ospstatus" => (int)$insertStatusOsp
            ], [
                "cod_osp"     => (int)$dadosEncerramento['codigoOs']
            ]);

            //Mensagem para o alerta RealTime
            $_SESSION['notificacao']['mensag'] = "Osp n. " . $dadosEncerramento['codigoOs'] . " encerrada.";


            switch ($dadosEncerramento['descricaoTipoFechamento'])
            {
                case "1":
                case "3":
                    $this->alterarStatusSsp($codSsp, 18, $time, $dadosUsuario); //Status da Ssp fica Encerrada

                    $insertEncerramento = $this->medoo->insert("osp_encerramento", [
                        "cod_osp"               => (int)$dadosEncerramento['codigoOs'],
                        "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                        "cod_funcionario"   => (int) $dadosEncerramento['codFuncionario'],
                        "descricao"             => (string)$dadosEncerramento['motivoNaoExecucao'],
                        "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego'],
                        "data_encerramento"     => $dadosEncerramento['dataHoraFechamento']
                    ]);

                    //Mensagem para o alerta RealTime
                    $_SESSION['notificacao']['mensag'] .= "<br />Ssp n. {$codSsp} encerrada.";
                    $this->alterarStatusSsmReferente($codSsp, $time, $dadosUsuario);
                    break;

                case "2":
                    $this->alterarStatusSsp($codSsp, 22, $time, $dadosUsuario); //Pendente

                    $insertEncerramento = $this->medoo->insert("osp_encerramento", [
                        "cod_osp"               => (int)$dadosEncerramento['codigoOs'],
                        "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                        "cod_pendencia"         => (int)$dadosEncerramento['pendenciaFechamento'],
                        "cod_funcionario"   => (int) $dadosEncerramento['codFuncionario'],
                        "descricao"             => (string)$dadosEncerramento['recomendacoesContinuidade'],
                        "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego'],
                        "data_encerramento"     => $dadosEncerramento['dataHoraFechamento']
                    ]);

                    //Mensagem para o alerta RealTime
                    $_SESSION['notificacao']['mensag'] .= "<br />Ssp n. {$codSsp} pendente.";
                    $_SESSION['notificacao']['sspPendente'] = true;
                    break;
            }

        }else{
            switch ($dadosEncerramento['descricaoTipoFechamento'])
            {
                //N�o executada ou Sem Pend�ncia
                case "1":
                case "3":
                $this->alterarStatusSsp($codSsp, 18, $time, $dadosUsuario); //Status da Ssp fica Encerrada

                $insertEncerramento = $this->medoo->update("osp_encerramento", [
                    "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                    "cod_funcionario"       => (int) $dadosEncerramento['codFuncionario'],
                    "descricao"             => (string)$dadosEncerramento['motivoNaoExecucao'],
                    "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego'],
                    "data_encerramento"     => $dadosEncerramento['dataHoraFechamento']
                ],[
                    "cod_osp"               => (int)$dadosEncerramento['codigoOs']
                ]);

                //Mensagem para o alerta RealTime
                $_SESSION['notificacao']['mensag'] = "Ssp n. {$codSsp} encerrada.";
                $this->alterarStatusSsmReferente($codSsp, $time, $dadosUsuario);
                    break;

                //Com Pend�ncia
                case "2":
                    $this->alterarStatusSsp($codSsp, 22, $time, $dadosUsuario); //Pendente

                    $insertEncerramento = $this->medoo->update("osp_encerramento", [
                        "cod_tipo_fechamento"   => (int)$dadosEncerramento['descricaoTipoFechamento'],
                        "cod_pendencia"         => (int)$dadosEncerramento['pendenciaFechamento'],
                        "cod_funcionario"   => (int) $dadosEncerramento['codFuncionario'],
                        "descricao"             => (string)$dadosEncerramento['recomendacoesContinuidade'],
                        "liberacao"             => (string)$dadosEncerramento['liberacaoTrafego'],
                        "data_encerramento"     => $dadosEncerramento['dataHoraFechamento']
                    ],[
                        "cod_osp"               => (int)$dadosEncerramento['codigoOs']
                    ]);

                    //Mensagem para o alerta RealTime
                    $_SESSION['notificacao']['mensag'] = "Ssp n. {$codSsp} pendente.";
                    $_SESSION['notificacao']['sspPendente'] = true;
                    break;
            }
        }

        unset($_SESSION['dadosEncerramento']);

        if(!empty($_SESSION['notificacao']['sspPendente'])){
            $viewSsp = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$codSsp]);
            $viewSsp = $viewSsp[0];

            $_SESSION['notificacao']['sspPendenteNumero']             = $codSsp;
            $_SESSION['notificacao']['sspPendenteDataProgramada']     = $this->parse_timestamp($viewSsp["data_programada"]);
            $_SESSION['notificacao']['sspPendenteQtdDias']            = $viewSsp["dias_servico"];

            $_SESSION['notificacao']['sspPendenteVeiculo']            = $viewSsp["nome_veiculo"];

            $_SESSION['notificacao']['sspPendenteServico']            = $viewSsp["nome_servico"];
            $_SESSION['notificacao']['sspPendenteComplementoServico'] = $viewSsp["complemento"];
        }

        $sql = "SELECT un_equipe.cod_un_equipe FROM osp_registro ospr
                  JOIN un_equipe USING(cod_un_equipe)
                  WHERE ospr.cod_osp = {$dadosEncerramento['codigoOs']}";

        $codigoUniEquipe = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $codigoUniEquipe = $codigoUniEquipe[0]['cod_un_equipe'];

        $_SESSION['notificacao']['subChannelEquipe'] = $codigoUniEquipe;

        $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";
        $_SESSION['notificacao']['nomeUsuario'] = $dadosUsuario['usuario'];

        //Tipo de a��o
        $_SESSION['notificacao']['tipo']   = "Deletar";
        $_SESSION['notificacao']['tabela'] = "Osp";

        //Informa��o para a alimenta��o da tabela
        $_SESSION['notificacao']['numero'] = $dadosEncerramento['codigoOs'];

        //$_SESSION['alertaAcao'] = $_SESSION['notificacao']['mensag'];
    }
}
