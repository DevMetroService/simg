<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 23/05/2016
 * Time: 13:07
 */

class cancelarOsModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $dadosOs = $_SESSION['dados'];

        $time = date('d-m-Y H:i:s', time());

        $motivo = $dadosOs['motivoAcao'] . ' => por: ' . $dadosUsuario['usuario'] . ' - '.$time;

        switch ($dadosOs['tipoOs']){
            case "Osm":
                $this->alterarStatusOsm($dadosOs['codigo'], $dadosOs['motivo'], $time, $dadosUsuario, $motivo, $dadosOs['codOsDuplicado']);

                //Prepara a notifica��o a ser exibida
                $_SESSION['notificacao']['mensag'] = "Osm n. {$dadosOs['codigo']} cancelada.";

                //Pega c�digo ssm para altera��o de status
                $codSsm = $this->medoo->select("osm_falha", "cod_ssm", ["cod_osm" => $dadosOs['codigo']]);
                $codSsm = $codSsm[0];

                $this->encerrarSsm($codSsm, $time, $dadosUsuario, 7, $motivo);

                $sql = "SELECT un_equipe.cod_un_equipe FROM ssm
                          JOIN un_equipe USING(cod_un_equipe)
                          WHERE ssm.cod_ssm = {$codSsm}";
                break;

            case "Osp":
                //Insere novo status
                $insertStatusOs = $this->medoo->insert("status_osp",
                    [
                        "cod_osp"           => (int)$dadosOs['codigo'],
                        "cod_status"        => (int)$dadosOs['motivo'],
                        "cod_osp_duplicado" => (int)$dadosOs['codOsDuplicado'],
                        "descricao"         => $motivo,
                        "data_status"       => $time,
                        "usuario"           => $dadosUsuario['cod_usuario']
                    ]
                );

                //Atualiza statusAtual
                $updateOsm = $this->medoo->update("osp",
                    [
                        "cod_ospstatus"       => (int) $insertStatusOs
                    ],[
                        "cod_osp"           => (int)$dadosOs['codigo']
                    ]
                );

                //Prepara notifica��o
                $_SESSION['notificacao']['mensag'] = "Osp n. {$dadosOs['codigo']} cancelada.";

                //Pega c�digo ssp para altera��o de status
                $codSsp = $this->medoo->select("osp", "cod_ssp", ["cod_osp" => $dadosOs['codigo']]);
                $codSsp = $codSsp[0];

                $this->cancelarSsp($codSsp, $time, $dadosUsuario, $motivo);

                //Informa a equipe para exibi��o na notifi��o.
                $sql = "SELECT un_equipe.cod_un_equipe FROM ssp
                          JOIN un_equipe USING(cod_un_equipe)
                          WHERE ssp.cod_ssp = {$codSsp}";
                break;

            case "Osmp":
                //Insere novo status
                $insertStatusOs = $this->medoo->insert("status_osmp",
                    [
                        "cod_osmp"          => (int)$dadosOs['codigo'],
                        "cod_status"        => (int)$dadosOs['motivo'],
                        "descricao"         => $motivo,
                        "data_status"       => $time,
                        "usuario"           => $dadosUsuario['cod_usuario']
                    ]
                );

                //Atualiza statusAtual
                $updateOsmp = $this->medoo->update("osmp",
                    [
                        "cod_status_osmp"    => (int) $insertStatusOs
                    ],[
                        "cod_osmp"           => (int)$dadosOs['codigo']
                    ]
                );

                //Prepara notifica��o
                $_SESSION['notificacao']['mensag'] = "Osmp n. {$dadosOs['codigo']} cancelada.";

                //Pega c�digo ssp para altera��o de status
                $codSsmp = $this->medoo->select("osmp", "cod_ssmp", ["cod_osmp" => $dadosOs['codigo']]);
                $codSsmp = $codSsmp[0];

                $this->cancelarSsmp($codSsmp, $time, $dadosUsuario, $motivo);

                //Informa a equipe para exibi��o na notifi��o.
                $sql = "SELECT un_equipe.cod_un_equipe FROM ssmp
                          JOIN un_equipe USING(cod_un_equipe)
                          WHERE ssmp.cod_ssmp = {$codSsmp}";
                break;

                break;
        }

        $_SESSION['notificacao']['mensag'] .= "<br />Por:  {$dadosUsuario['usuario']}";

        $_SESSION['notificacao']['tabela'] = $dadosOs['tipoOs'];
        $_SESSION['notificacao']['tipo']   = "Deletar";
        $_SESSION['notificacao']['numero'] = $dadosOs['codigo'];

        $codigoUniEquipe = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $codigoUniEquipe = $codigoUniEquipe[0]['cod_un_equipe'];

        $_SESSION['notificacao']['subChannelEquipe'] = $codigoUniEquipe;
        $_SESSION['notificacao']['nomeUsuario'] = $dadosUsuario['usuario'];
    }

    function cancelarSsp($codSsp, $time, $dadosUsuario, $motivo){
        $osps = $this->medoo->select("osp",["[><]status_osp" => 'cod_ospstatus'],"*",["cod_ssp" => $codSsp]);

        $status =17; // Status Cancelada da SSP;
        foreach($osps as $dados=>$value){
            switch ($value['cod_status']){
                case 11:
                    $status = 18;  //SSP est� Encerrada
                    break;
                case 23:
                case 24:
                case 25:
                    break;

                default:  //Sai da fun��o sem alterar ou cancelar a SSP se n�o for nenhuma das op��es anteriores
                    return;
            }
        }
        if($status == 18){
            $_SESSION['notificacao']['mensag'] .= "<br />Ssp n.{$codSsp} encerrada.";
        }else{
            $_SESSION['notificacao']['mensag'] .= "<br />Ssp n.{$codSsp} cancelada.";
        }

        $updateStatusSsp = $this->medoo->insert("status_ssp", [
            "cod_ssp"     => (int) $codSsp,
            "cod_status"  => (int) $status,
            "data_status" => $time,
            "descricao"   => $motivo,
            "usuario"     => (int) $dadosUsuario['cod_usuario']
        ]);

        $updateSsp = $this->medoo->update("ssp", [
            "cod_pstatus" => (int)$updateStatusSsp
        ], [
            "cod_ssp"     => (int)$codSsp
        ]);

        $this->alterarStatusSsmReferente($codSsp, $time, $dadosUsuario); // se for Corretiva
    }

    function cancelarSsmp($codSsmp, $time, $dadosUsuario, $motivo){
        $osps = $this->medoo->select("osmp",["[><]status_osmp" => 'cod_status_osmp'],"*",["cod_ssmp" => $codSsmp]);

        $status =17; // Status Cancelada da SSMP;
        foreach($osps as $dados=>$value){
            switch ($value['cod_status']){
                case 11:
                    $status = 18;  //SSP est� Encerrada
                    break;
                case 21:
                case 23:
                case 24:
                case 25:
                    break;

                default:  //Sai da fun��o sem alterar ou cancelar a SSP se n�o for nenhuma das op��es anteriores
                    return;
            }
        }
        if($status == 18){
            $_SESSION['notificacao']['mensag'] .= "<br />Ssmp n.{$codSsmp} encerrada.";
        }else{
            $_SESSION['notificacao']['mensag'] .= "<br />Ssmp n.{$codSsmp} cancelada.";
        }

        //Cria uma variavel com o nome da tabela de status exata da ssmp.

        //Insere o status
        $updateStatusSsmp = $this->medoo->insert("status_ssmp", [
            "cod_ssmp"     => (int) $codSsmp,
            "cod_status"  => (int) $status,
            "data_status" => $time,
            "descricao"   => $motivo,
            "usuario"     => (int) $dadosUsuario['cod_usuario']
        ]);

        //Cria uma variavel com o nome da tabela da ssmp.

        //Atualiza a tabela com o novo status
        $updateSsp = $this->medoo->update("ssmp", [
            "cod_status_ssmp"   => (int)$updateStatusSsmp
        ], [
            "cod_ssmp"                      => (int)$codSsmp
        ]);
    }
}
