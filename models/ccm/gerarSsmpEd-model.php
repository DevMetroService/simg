<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 05/12/2016
 * Time: 11:20
 */
class gerarSsmpEdModel extends MainModel
{

    function __construct($bancoDados = true, $controller = null, $medoo = true, $phpass = true, $dadosUsuario)
    {
        $this->medoo = $medoo;
        $this->phpass = $phpass;

        $medooBegin = $this->medoo;

        $_SESSION['dados_usuario_ssmp'] = $dadosUsuario;

        $time = date('d-m-Y H:i:s', time());

        $itemCronograma = $this->medoo->select("cronograma_pmp", [
            '[><]pmp' => 'cod_pmp',
            '[><]pmp_edificacao' => 'cod_pmp',
            '[><]cronograma' => 'cod_cronograma',
            '[><]local' => "cod_local",
            '[><]tipo_periodicidade' => "cod_tipo_periodicidade",
            '[><]status_cronograma_pmp' => "cod_status_cronograma_pmp",
            "[><]status" => 'cod_status'
        ], [
            'cronograma_pmp.cod_cronograma_pmp',
            'cod_local',
            'nome_local',
            'cod_pmp_edificacao',
            'nome_servico_pmp',
            'cronograma.quinzena',
            'nome_periodicidade',
            'nome_status'
        ], [
            'cod_cronograma_pmp' => $_SESSION['cronogra_ed']
        ]);
        $itemCronograma = $itemCronograma[0];

        $selectUnEquipe = $this->medoo->select("eq_us_unidade",
            [
                "[><]un_equipe" => "cod_un_equipe"
            ], [
                'cod_un_equipe'
            ], [
                "AND" => [
                    "cod_equipe" => 12,
                    "cod_usuario" => $_SESSION['dados_usuario_ssmp']['cod_usuario']
                ]
            ]);
        $selectUnEquipe = $selectUnEquipe[0];

        $insertSSMP = $this->medoo->insert('ssmp', [
            "data_abertura" => $time,
            "cod_un_equipe" => $selectUnEquipe
        ]);

        $insertSSMPED = $this->medoo->insert('ssmp_ed', [
            "cod_ssmp" => $insertSSMP,
            "cod_local" => $itemCronograma['cod_local'],
            "cod_pmp_edificacao" => $itemCronograma['cod_pmp_edificacao']
        ]);

        $insertStatusSsmp = $this->medoo->insert("status_ssmp", [
            "cod_ssmp" => $insertSSMP,
            "data_status" => $time,
            "cod_status" => 32,
            "usuario" => $_SESSION['dados_usuario_ssmp']['cod_usuario']
        ]);

        $updateSsmp = $this->medoo->update('ssmp',["cod_status_ssmp" => (int) $insertStatusSsmp], ["cod_ssmp" => (int) $insertSSMP]);

        if (empty($insertSSMP) || empty($insertStatusSsmp)) {
            $_SESSION['feedback_banco'] = $this->medoo->error();
        }

        unset($_SESSION['dados_usuario_ssmp']);
        unset($_SESSION['cronogra_ed']);

    }
}