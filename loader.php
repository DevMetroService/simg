<?php

define("ERROR_REPORT_LEVEL", E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED);

ini_set('default_charset', DB_CHARSET);

if (! defined ( 'ABSPATH' )) { 								# Evitamos que esse arquivo seja acessado diretamente pelos usuários
	exit ();
}

session_start (); 											# Iniciamos a sessão

if (! defined ( 'DEBUG' ) 									# Verificamos se o Debug está setado em true
	|| DEBUG === false) { 	

    error_reporting ( 0 ); 									# Esconde todos os erros
	ini_set ( "display_errors", 0 );
} else {
	error_reporting ( ERROR_REPORT_LEVEL );						 		# Mostra todos os erros
	ini_set ( "display_errors", 1 );
}

require_once ABSPATH . '/functions/global-functions.php'; 	# Funções globais
$simg_mvc = new SimgMVC (); 								# Carregamos a aplicação