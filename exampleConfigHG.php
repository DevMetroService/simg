<?php
/**
 * Configura??o geral do sistema
 */
define ( 'GESIV', [4] ); 			                                      # Caminho para a raiz
define ( 'ABSPATH', dirname ( __FILE__ ) ); 			              # Caminho para a raiz
define ( 'UP_ABSPATH', ABSPATH . '/views/_images' ); 	                  # Caminho para a pasta rh
define ( 'TEMP_PATH', ABSPATH . '/includes/temp/');

define ( 'SISTEMAS_FIXOS', [20,21,24,25,27,28,29]);

define ( 'DASHBOARDS', ['gesiv','gemof','material','Rh','astig']);

define ( 'HOME_URI', 'http://'.$_SERVER['HTTP_HOST']);                    # URL da home
define ( 'UP_FOTOFUNCIONARIO_ABSPATH',
    ABSPATH  . 'views/_uploads/fotosFuncionario/' );                      # Caminho para a pasta rh

define ( 'PORTNUMBER', '5432');

/** Servidor Homologação **/
define ( 'HOSTNAME', '172.25.19.56' );
define ( 'DB_NAME', 'HGSIMG' );
define ( 'DB_USER', 'user_simg' );
define ( 'DB_SENHA', 'Pro@2020_1' );
define ( 'SCHEMA', 'public' );

define ( 'DB_CHARSET', 'ISO-8859-1' );
define ( 'DEBUG', true ); 								                  # Se voc? estiver desenvolvendo, modifique o valor para true.


//Defini��es de configura��o de envio de e-mail
define ( 'MAIL_ACTIVE', false);
define ( 'SMTP_HOST', '172.27.20.10');
define ( 'SMTP_PORT', '25');
define ( 'MAIL_USERNAME', 'envio.sistemas@metrofor.ce.gov.br');
define ( 'MAIL_PASSWORD', 'sistemas!@#');
define ( 'SMTP_CONF', array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
    ))
);
define ('MAIL_DEBUG', false);

//Seta timezone padr�o do sistema em PHP
date_default_timezone_set ("America/Fortaleza");

/**
 * ATENÇÃO! Não edite daqui em diante
 */

require_once ABSPATH . '/loader.php';					                  # Carrega o loader, que vai carregar a aplica??o inteira
