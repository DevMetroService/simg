<?php
$this->dashboard->cabecalho('Preventiva');

$qznNextMes = $this->getQznNextMes();

// -- Controle de PMP - Edificação
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Edificação", 2);

$this->dashboard->painelCronograma('Ed', false, false, null, null, true, false, 'default');

$this->dashboard->painelSsmpAbertas('Ed', '', false);
$this->dashboard->painelSsmpProgramadas('Ed', '', false);
$this->dashboard->painelSsmpPendentes('Ed', '', false);
$this->dashboard->painelOsmp('Ed', '', false);

// -- Controle de PMP - Via Permanente
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Via Permanente", 2);

$this->dashboard->painelCronograma('Vp', false, false, null, null, true, false, 'default');

$this->dashboard->painelSsmpAbertas('Vp', '', false);
$this->dashboard->painelSsmpProgramadas('Vp', '', false);
$this->dashboard->painelSsmpPendentes('Vp', '', false);
$this->dashboard->painelOsmp('Vp', '', false);

// -- Controle de PMP - Subestação
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Subestação", 2);

$this->dashboard->painelCronograma('Su', false, false, null, null, true, false, 'default');

$this->dashboard->painelSsmpAbertas('Su', '', false);
$this->dashboard->painelSsmpProgramadas('Su', '', false);
$this->dashboard->painelSsmpPendentes('Su', '', false);
$this->dashboard->painelOsmp('Su', '', false);

// -- Controle de PMP - Rede Aerea
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Rede Aerea", 2);

$this->dashboard->painelCronograma('Ra', false, false, null, null, true, false, 'default');

$this->dashboard->painelSsmpAbertas('Ra', '', false);
$this->dashboard->painelSsmpProgramadas('Ra', '', false);
$this->dashboard->painelSsmpPendentes('Ra', '', false);
$this->dashboard->painelOsmp('Ra', '', false);

// -- Controle de PMP - Telecom
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Telecom", 2);

$this->dashboard->painelCronograma('Tl', false, false, null, null, true, false, 'default');

$this->dashboard->painelSsmpAbertas('Tl', '', false);
$this->dashboard->painelSsmpProgramadas('Tl', '', false);
$this->dashboard->painelSsmpPendentes('Tl', '', false);
$this->dashboard->painelOsmp('Tl', '', false);

// -- Controle de PMP - Bilhetagem
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Bilhetagem", 2);

$this->dashboard->painelCronograma('Bl', false, false, null, null, true, false, 'default');

$this->dashboard->painelSsmpAbertas('Bl', '', false);
$this->dashboard->painelSsmpProgramadas('Bl', '', false);
$this->dashboard->painelSsmpPendentes('Bl', '', false);
$this->dashboard->painelOsmp('Bl', '', false);


$this->dashboard->modalExibirCronograma();