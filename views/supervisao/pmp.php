<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 23/02/2016
 * Time: 15:47
 */
//echo var_dump($_SESSION);
?>
<div class="page-header">
    <h2>Plano de Manuten��o Preventiva - Edifica��es</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>Linha Sul</h3>
            </div>
            <div class="panel-body">
                <table id="" class="table table-striped table-bordered table-hover dataTable no-footer text-center">
                    <thead>
                    <tr role="row">
                        <th>Local</th>
                        <th>Servi�o de Manuten��o</th>
                        <th>Periodicidade</th>
                        <th>Procedimento</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $selectPmp = $this->medoo->query("SELECT DISTINCT ON(serv_prev.nome_servico_prev) nome_servico_prev, quant_intervalos, nome_procedimento
                                                        FROM pmp
                                                        JOIN servico_preventivo serv_prev USING(cod_servico_prev)
                                                        JOIN procedimento USING(cod_procedimento)
                                                        JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                                        WHERE cod_sistema = {$_SESSION['filtroPmpTabela']['sistema']}")->fetchAll(PDO::FETCH_ASSOC);
                    $countRowPmp = count($selectPmp) +1;
                    $selectTrecho = $this->medoo->select('estacao', '*', ['cod_linha' => 5]);
                    $countRowTrecho = count($selectTrecho);
                    $nomesLocais = "";
                    if($countRowPmp < $countRowTrecho){
                        $countRowPmp = $countRowTrecho;
                    }
                    foreach ($selectTrecho as $dados => $value){
                        $nomesLocais .= "<a href='#' class='local'>".$value['nome_estacao']."</a></br>";
                    }
                    echo ('<tr>');
                    echo ('<td rowspan="'.$countRowPmp.'">');
                    echo ($nomesLocais);
                    echo ('</td>');
                    echo ('</tr>');


                    if($selectPmp)
                        foreach ($selectPmp as $dados=>$value){
                            echo('<tr>');
                            echo('<td><a class="servico" href="#">'.$value['nome_servico_prev'].'</a></td>');
                            echo('<td><a class="periodicidade" href="'.$value['quant_intervalos'].'">'.getPeriodo($value['quant_intervalos']).'</a></td>');
                            echo('<td><a href="#" class="procedimento">'.$value['nome_procedimento'].'</a></td>');
                            echo('</tr>');
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
function getPeriodo($intervalo){
    switch($intervalo){
        case "1":
            return "Quinzenal";

        case "2":
            return "Mensal";

        case "4":
            return "Bimestral";

        case "6":
            return "Trimestral";

        case "12":
            return "Semestral";

        case "24":
            return "Anual";

        default:
            return"Dados n�o encontrado";

    }
}

?>
