<div class="modal fade modal-historico" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="fa fa-close"></i></button>
                <h4 class="modal-title">Hist�rico da SAF</h4>
            </div>
            <div class="modal-body">

                <?php
                if(!empty($saf)) {
                    $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_saf s
                JOIN usuario u on s.usuario = u.cod_usuario
                JOIN status USING(cod_status)
                WHERE cod_saf = {$saf['cod_saf']} ORDER BY data_status";

                    if ($sql != "")
                        $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                }
                ?>

                <div class = "row">
                    <div class="col-md-12">
                        <table class="tableRel table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Responsavel</th>
                                <th>Data da altera��o</th>
                                <th>Complemento</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($result)){
                                foreach ($result as $dados){
                                    echo("<tr>");
                                    echo("<td>{$dados['nome_status']}</td>");
                                    echo("<td>{$dados['usuario']}</td>");
                                    echo("<td>{$this->parse_timestamp($dados['data_status'])}</td>");
                                    echo("<td>{$dados['descricao']}</td>");
                                    echo("</tr>");
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>