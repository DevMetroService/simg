<?php

echo <<<HTML
<div class="modal fade modalCancelSaf" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="fa fa-close"></i></button>
                <h4 class="modal-title">Cancelamento da SAF</h4>
            </div>
            <div class="modal-body">
                <form id="cancelForm" action="{$this->home_uri}/saf/cancel/{$saf['cod_saf']}" method="post" class="form-group">
                    <div class = "row">
                        <div class="col-md-3">
                            <label>SAF</label>
                            <input class="form-control" readonly value="{$saf['cod_saf']}">
                        </div>

                        <div class="col-md-8">
                            <label>Motivo do Cancelamento</label>
                            <textarea id="motivoCancelamento" class="form-control" name="descricao"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="submitCancel" type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

HTML;
?>
<script>
$(document).ready(function()
{
    $('#submitCancel').click(function()
    {
        if($('#motivoCancelamento').val() == "")
            alertaJquery('Campo Obrigatório', "O campo de motivo da devolução deve ser informado.", "warning");
        else
        {
            $('#cancelForm').submit();
            $('.modalCancelSaf').modal('hide');
        }
            
    });
});
</script>