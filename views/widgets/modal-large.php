<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 18/09/2020
 * Time: 13:03
 */

class modalLarge
{
    public function __invoke($idTarget, $title, $content)
    {?>
        <div class='modal fade' id='<?=$idTarget?>' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'><i class='fa fa-close'></i></span></button>
                        <h4 class='modal-title' id='myModalLabel'><?=$title?></h4>
                    </div>
                    <div class='modal-body'>
                        <?= $content ?>
                    </div>
                    <div class='modal-footer'>
                        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                        <button type='button' class='btn btn-primary'>Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}