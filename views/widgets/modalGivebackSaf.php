<?php

echo <<<HTML
<div class="modal fade modalGivebackSaf" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="fa fa-close"></i></button>
                <h4 class="modal-title">Devolu��o da SAF</h4>
            </div>
            <div class="modal-body">
                <form id="givebackForm" action="{$this->home_uri}/saf/giveback/{$saf['cod_saf']}" method="post" class="form-group">
                    <div class = "row">
                        <div class="col-md-3">
                            <label>SAF</label>
                            <input class="form-control" readonly value="{$saf['cod_saf']}">
                        </div>

                        <div class="col-md-8">
                            <label>Motivo da Devolu��o</label>
                            <textarea id="motivoDevolucao" class="form-control" name="descricao"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="submitGiveback" type="button" class="btn btn-primary">OK</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

HTML;
?>
<script>
$(document).ready(function()
{
    $('#submitGiveback').click(function()
    {
        if($('#motivoDevolucao').val() == "")
            alertaJquery('Campo Obrigat�rio', "O campo de motivo da devolu��o deve ser informado.", "warning");
        else
        {
            $('#givebackForm').submit();
            $('.modalGivebackSaf').modal('hide');
        }
    });
});
</script>