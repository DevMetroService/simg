<html lang="br">
<head>

    <?php header('Content-type: text/html; charset=iso-8859-1'); ?>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <link rel="icon" type="image/jpg" href="<?php echo HOME_URI ?>/views/_images/mini_metroservice_logo.png"/>
    <title><?php echo $this->titulo ?></title>

    <!-- Bloco de CSS -->
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/stylePrint.css">
    <!-- Chamada do css do font-Awesome -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/font-awesome/css/font-awesome.min.css">

    <!-- Entrada de Script -->
    <script type="text/javascript" src="<?php echo HOME_URI ?>/views/_js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo HOME_URI ?>views/_js/config.js"></script>

    <script type="text/javascript" src="<?php echo HOME_URI ?>/views/_js/plugins/highcharts.js"></script>

</head>

<body>
<div class="tela"></div>

<div class="container-button">
<?php
if(empty($button)){
    echo '<button class="btn-azul" onclick="print()">Imprimir</button>';
}else{
    echo $button;
    echo '<script type="text/javascript" src="' . HOME_URI . 'views/_js/scriptBtnView.js"></script>';
}
?>
</div>

<div class="modal-container">
    <div class="modal">
        <div class="header-modal">
            <h2>Motivo da A��o<span class="close-modal-container">&times;</span></h2>
        </div>
        <div class="body-modal">
            <strong>Informe o Motivo</strong>
            <textarea placeholder="Informe aqui"></textarea>
        </div>
        <button class="btn-verde ok-modal-container">&#10004;</button>
        <button class="btn-vermelho close-modal-container">&#10006;</button>
    </div>
</div>