<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 25/02/2016
 * Time: 16:26
 */
$arquivo = $_GET['arquivo'];
$extensao = pathinfo($arquivo);

$bloquados = array('php', 'html', 'htm', 'asp');

if (!in_array($extensao['extension'], $bloquados)) {
    if (isset($arquivo) && file_exists($arquivo)) { // faz o teste se a variavel n�o esta vazia e se o arquivo realmente existe
        switch (strtolower(substr(strrchr(basename($arquivo), "."), 1))) { // verifica a extens�o do arquivo para pegar o tipo
            case "pdf":
                $tipo = "application/pdf";
                break;
            case "exe":
                $tipo = "application/octet-stream";
                break;
            case "zip":
                $tipo = "application/zip";
                break;
            case "doc":
                $tipo = "application/msword";
                break;
            case "xls":
                $tipo = "application/vnd.ms-excel";
                break;
            case "xlsx":
                $tipo = "application/vnd.ms-excel";
                break;
            case "ppt":
                $tipo = "application/vnd.ms-powerpoint";
                break;
            case "gif":
                $tipo = "image/gif";
                break;
            case "png":
                $tipo = "image/png";
                break;
            case "jpg":
                $tipo = "image/jpg";
                break;
            case "mp3":
                $tipo = "audio/mpeg";
                break;
        }
        header("Content-Type: " . $tipo); // informa o tipo do arquivo ao navegador
        header("Content-Length: " . filesize($arquivo)); // informa o tamanho do arquivo ao navegador
        header("Content-Disposition: attachment; filename=" . basename($arquivo)); // informa ao navegador que � tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
        readfile($arquivo); // l� o arquivo
        exit; // aborta p�s-a��es
    }
} else {
    echo "Erro!";
    exit;
}