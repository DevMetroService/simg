<?php
if ($relatorio) {
    echo("<input id='rel' type='hidden' value='{$relatorio}'>");
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4><i class="fa fa-pie-chart fa-fw"></i> Relat�rios de Controle</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formRel" action="<?= $this->home_uri ?>/dashboardGeral/gerarRelatorio/PizzaStatus" target="_blank" method="post">
                            <div class="page-header">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-6">
                                        <h3 class="text-center">Selecione o relat�rio</h3>
                                        <select name="grafico" class="form-control">
                                            <option value="PizzaStatus">
                                                Status
                                            </option>
                                            <option value="LancadasAtendidas">
                                                Lan�adas, Atendidas e em Atendimento
                                            </option>
                                            <option value="MateriaisUtilizados">
                                                Materiais Utilizados
                                            </option>
                                            <option value="MaquinasEquipamentosUtilizados">
                                                Maquinas e Equipamentos Utilizados
                                            </option>
                                            <option value="HomemHora">
                                                Homem Hora
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Descri��o:</label>
                                        <div id="descricaoRel"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name="form" value="saf">
                                <?php
                                function echoButtonForm($form){
                                    $formName = strtoupper($form);
                                    echo   "<div class='col-md-1 {$form}DivRel'>
                                                <div class='panel'>
                                                    <button type='button' name='{$form}' class='btn btn-default btn-block'>{$formName}</button>
                                                </div>
                                            </div>";
                                }
                                $arrayform = array('saf','ssm','osm','ssp','osp','ssmp','osmp','todos');
                                array_walk($arrayform, "echoButtonForm");
                                ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4>Filtros</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row funcionarioDivRel">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Funcion�rios</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <select id='selectFuncionario' name="funcionario[]"
                                                                            class="form-control multiSelect"
                                                                            multiple="multiple">
                                                                        <?php
                                                                        $selectFunc = $this->medoo->select("funcionario",
                                                                            [
                                                                                '[><]dados_empresa_funcionario' => 'cod_funcionario',
                                                                                '[><]centro_resultado' => 'cod_centro_resultado'
                                                                            ], ['cod_funcionario', 'nome_funcionario', 'descricao', 'cod_centro_resultado'],
                                                                            ["ORDER" => "descricao"]);

                                                                        $txtOption = array();

                                                                        foreach ($selectFunc as $dados => $value) {
                                                                            $txtOption[$value['cod_centro_resultado']] .= "<option value='{$value['cod_funcionario']}'>{$value["nome_funcionario"]}</option>";
                                                                        }
                                                                        ?>
                                                                        <optgroup label="Energia" id="Energia">
                                                                            <?php echo $txtOption['13'] ?>
                                                                            <?php echo $txtOption['14'] ?>
                                                                            <?php echo $txtOption['15'] ?>
                                                                        </optgroup>
                                                                        <optgroup id="viaPermanente"
                                                                                  label="Via Permanente">
                                                                            <?php echo $txtOption['17'] ?>
                                                                            <?php echo $txtOption['18'] ?>
                                                                            <?php echo $txtOption['19'] ?>
                                                                            <?php echo $txtOption['20'] ?>
                                                                            <?php echo $txtOption['21'] ?>
                                                                            <?php echo $txtOption['22'] ?>
                                                                            <?php echo $txtOption['23'] ?>
                                                                            <?php echo $txtOption['24'] ?>
                                                                            <?php echo $txtOption['25'] ?>
                                                                        </optgroup>
                                                                        <optgroup id="edificacao" label="Edifica��es">
                                                                            <?php
                                                                            echo $txtOption['16'];
                                                                            echo $txtOption['26'];
                                                                            echo $txtOption['27'];
                                                                            echo $txtOption['28'];
                                                                            ?>
                                                                        </optgroup>
                                                                        <optgroup label="Bilhetagem/Telecom" id="telecom">
                                                                            <?php
                                                                            echo $txtOption['30'];
                                                                            ?>
                                                                        </optgroup>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 periodo">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Per�odo</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row periodoMesAno">
                                                                <div class="col-md-3">
                                                                    <label>M�s</label>
                                                                    <select class="form-control" name="mes">
                                                                        <?php
                                                                        foreach (MainController::$monthComplete as $key => $value) {
                                                                            echo("<option value='{$key}'>$value</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label>Ano</label>
                                                                    <select class="form-control" name="ano">
                                                                        <?php
                                                                        echo "<option>" . (date('Y') - 5) . "</option>";
                                                                        echo "<option>" . (date('Y') - 4) . "</option>";
                                                                        echo "<option>" . (date('Y') - 3) . "</option>";
                                                                        echo "<option>" . (date('Y') - 2) . "</option>";
                                                                        echo "<option>" . (date('Y') - 1) . "</option>";
                                                                        echo "<option selected>" . date('Y') . "</option>";
                                                                        echo "<option>" . (date('Y') + 1) . "</option>";
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row periodoAPartirAte">
                                                                <div class="col-md-6">
                                                                    <label>Per�odo (a Partir)</label>
                                                                    <input class="form-control data add-on validaData"
                                                                           name="dataPartirPeriodo"
                                                                           data-format="dd/MM/yyyy"/>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>At�</label>
                                                                    <input class="form-control data add-on validaData"
                                                                           name="dataRetrocederPeriodo"
                                                                           data-format="dd/MM/yyyy"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dataAbertura">
                                                    <div class="col-md-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading datAbert">
                                                                <label>Data de Abertura</label>
                                                            </div>
                                                            <div class="panel-heading datProg">
                                                                <label>Data Programada</label>
                                                            </div>
                                                            <div class="panel-heading datFiltro">
                                                                <label>Datas</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label class="datAbert">Data (a Partir)</label>
                                                                        <label class="datProg">Data (a Partir)</label>
                                                                        <label class="datFiltro">Data Abertura (A Partir)</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataPartirDatAber"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>At�</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataRetrocederDatAber"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row dataEncerrada">
                                                                    <div class="col-md-6">
                                                                        <label>Data Encerramento (a Partir)</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataPartirDatEnc"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>At�</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataRetrocederDatEnc"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 local">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Local</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label>Linha</label>
                                                                    <select class="form-control" name="linha">
                                                                        <option value="">Todos</option>
                                                                        <?php
                                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                                        foreach ($selectLinha as $dados => $value) {
                                                                            echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4 trecho">
                                                                    <label>Trecho</label>
                                                                    <select class="form-control" name="trecho">
                                                                        <option value="">Todos</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4 pn">
                                                                    <label>Ponto Not�vel</label>
                                                                    <select class="form-control" name="pn">
                                                                        <option value="">Todos</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Dados Gerais</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label>Grupo de Sistema</label>
                                                                    <select name="grupoSistema" class="form-control">
                                                                        <option value="">Grupos de Sistemas</option>
                                                                        <?php
                                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);

                                                                        foreach ($grupoSistema as $dados => $value) {
                                                                            echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4 sistema">
                                                                    <label>Sistema</label>
                                                                    <select name="sistema" class="form-control">
                                                                        <option value="">Sistemas</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4 subSistema">
                                                                    <label>Sub-Sistema</label>
                                                                    <select name="subSistema" class="form-control">
                                                                        <option value="">Sub-Sistemas</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3 solicitanteOrigem">
                                                                    <label>Origem</label>
                                                                    <select name="solicitanteOrigem"
                                                                            class="form-control">
                                                                        <option value="">Todos</option>
                                                                        <?php
                                                                        $selectop = array(
                                                                            "1" => "MetroService",
                                                                            "2" => "MetroFor",
                                                                            "3" => "Terceiros"
                                                                        );

                                                                        foreach ($selectop as $dados => $value) {
                                                                            echo("<option value='{$dados}' >{$value}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3 origemProgRel">
                                                                    <label>Origem</label>
                                                                    <select name="origemProgRel" class="form-control">
                                                                        <option value="">Todos</option>
                                                                        <?php
                                                                        $selectop = array(
                                                                            "1" => "Preventiva",
                                                                            "2" => "Corretiva",
                                                                            "3" => "Programa��o",
                                                                            "4" => "Servi�o"
                                                                        );
                                                                        foreach ($selectop as $dados => $value) {
                                                                            echo("<option value='" . $dados . "' ");

                                                                            if (!empty($dadosRefill['origemProgramacao']) && $dadosRefill['origemProgramacao'] == $dados) {
                                                                                echo("selected");
                                                                            }
                                                                            echo(">" . $value . "</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6 avariaRelDiv">
                                                                    <label>Avaria</label>
                                                                    <input type="hidden" name="avaria" value=""/>
                                                                    <input name="avariaInput" list="avariaDataList"
                                                                           class="form-control"
                                                                           placeholder="Clique na Avaria"
                                                                           autocomplete="off" rel="tooltip-wrapper"
                                                                           data-title="Para Limpar este campo clique em Resetar Filtros"/>

                                                                    <datalist id="avariaDataList">
                                                                        <option value=""></option>
                                                                        <?php
                                                                        $avaria = $this->medoo->select("avaria", ['cod_avaria', 'nome_avaria'], ['ORDER' => 'nome_avaria']);

                                                                        foreach ($avaria as $dados => $value) {
                                                                            echo('<option value="' . $value['cod_avaria'] . '">' . $value['nome_avaria'] . '</option>');
                                                                        }
                                                                        ?>
                                                                    </datalist>
                                                                </div>
                                                                <div class="col-md-3 nivelDiv">
                                                                    <label>Nivel</label>
                                                                    <select name="nivel" class="form-control">
                                                                        <option value="">Todos</option>
                                                                        <option value="A">A</option>
                                                                        <option value="B">B</option>
                                                                        <option value="C">C</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row servicoDiv">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Servi�o</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6 tipoIntervencaoDiv">
                                                                    <label>Tipo de Interven��o</label>
                                                                    <select name="tipoIntervencao" class="form-control">
                                                                        <option value="">Sistemas</option>
                                                                        <?php
                                                                        $selectIntevercao = $this->medoo->select("tipo_intervencao", ['cod_tipo_intervencao', "nome_tipo_intervencao"], ["ORDER" => "nome_tipo_intervencao"]);

                                                                        foreach ($selectIntevercao as $dados => $value) {
                                                                            echo("<option value='{$value['cod_tipo_intervencao']}'>{$value["nome_tipo_intervencao"]}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Servi�o</label>
                                                                    <select name="servico" class="form-control">
                                                                        <option value="">Grupos de Sistemas</option>
                                                                        <?php
                                                                        $selectServico = $this->medoo->select("servico", ['cod_servico', "nome_servico"], ["ORDER" => "nome_servico"]);

                                                                        foreach ($selectServico as $dados => $value) {
                                                                            echo("<option value='{$value['cod_servico']}'>{$value["nome_servico"]}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row encaminhamnetoDiv">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Encaminhamento</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Unidade</label>
                                                                    <select name="unidade" class="form-control">
                                                                        <option value="">Selecione a Unidade</option>
                                                                        <?php
                                                                        $selectUnidade = $this->medoo->select("unidade", ['cod_unidade', "nome_unidade"]);

                                                                        foreach ($selectUnidade as $dados => $value) {
                                                                            echo("<option value='{$value['cod_unidade']}'>{$value["nome_unidade"]}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Equipe</label>
                                                                    <select name="equipe" class="form-control">
                                                                        <option value="">Selecione a Equipe</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row maquinasEquipamentosDiv">
                                                <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>M�quinas e Materiais</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="col-md-12 maquinaEquipamentoDiv">
                                                                <label>M�quinas e Equipamentos</label>
                                                                <select class="form-control"
                                                                        name="maquinasEquipamentos">
                                                                    <option value="">Todos</option>
                                                                    <?php
                                                                    $selectMaquinasEquipamentos = $this->medoo->select("v_material", ['nome_material', 'cod_material'], [
                                                                        "cod_categoria" => 10, "ORDER" => "nome_material"]);
                                                                    foreach ($selectMaquinasEquipamentos as $dados => $value) {
                                                                        echo('<option value="' . $value['cod_material'] . '">' . $value['nome_material'] . '</option>');
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-12 materiaisDiv">
                                                                <label>Materiais</label>
                                                                <select class="form-control" name="materiais">
                                                                    <option value="">Todos</option>
                                                                    <?php
                                                                    $selectMateriais = $this->medoo->select("v_material", ['nome_material', 'cod_material'], ["ORDER" => "nome_material", "cod_categoria" => "32"]);

                                                                    foreach ($selectMateriais as $dados => $value) {
                                                                        echo('<option value="' . $value['cod_material'] . '">' . $value['nome_material'] . '</option>');
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row materialRodanteDiv" hidden>
                                                <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Material Rodante</label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label>Ve�culo</label>
                                                                    <select name="veiculo" class="form-control">
                                                                        <option value="">Ve�culo</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label>Carro</label>
                                                                    <select name="carro" class="form-control">
                                                                        <option value="">Carro</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label>Prefixo</label>
                                                                    <select name="prefixo" class="form-control">
                                                                        <option value="">Prefixo</option>
                                                                        <?php
                                                                        $prefixo = $this->medoo->select("prefixo", ['cod_prefixo', 'nome_prefixo']);
                                                                        foreach ($prefixo as $dados => $value) {
                                                                            echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label>Od�metro a partir</label>
                                                                    <input name='odometroPartir' class='form-control'/>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label>Od�metro at�</label>
                                                                    <input name="odometroAte" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-lg gerarRel">
                                        <i class="fa fa-cogs fa-fw"></i>
                                        Gerar Relat�rio
                                    </button>
                                    <button type="reset" class="btn btn-default btn-lg resetarRel">
                                        <i class="fa fa-repeat fa-fw"></i>
                                        Resetar Filtros
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
