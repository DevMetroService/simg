<?php
require_once ( ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);

//############         Periodo
$dataPartirPeriodo     = inverteData($_POST['dataPartirPeriodo']);
$dataRetrocederPeriodo = inverteData($_POST['dataRetrocederPeriodo']);

$wherePeriodoSaf = "";
$wherePeriodoSsp = "";

if($dataPartirPeriodo != "" || $dataRetrocederPeriodo !=""){
    $dat_ab_saf = "data_abertura";
    $dat_ab_ssp = "data_programada";

    if($dataPartirPeriodo == $dataRetrocederPeriodo){
        $dataPartirPeriodo     = $dataPartirPeriodo . " 00:00:00";
        $dataRetrocederPeriodo = $dataRetrocederPeriodo . " 23:59:59";

        $whereSaf[] = "$dat_ab_saf >= '{$dataPartirPeriodo}'";
        $whereSaf[] = "$dat_ab_saf <= '{$dataRetrocederPeriodo}'";

        $whereSsp[] = "$dat_ab_ssp >= '{$dataPartirPeriodo}'";
        $whereSsp[] = "$dat_ab_ssp <= '{$dataRetrocederPeriodo}'";

    }else{
        if ($dataPartirPeriodo) {
            $dataPartirPeriodo = $dataPartirPeriodo . " 00:00:00";

            $whereSaf[]    = "$dat_ab_saf >= '{$dataPartirPeriodo}'";
            $whereSsp[]    = "$dat_ab_ssp >= '{$dataPartirPeriodo}'";
        }

        if ($dataRetrocederPeriodo) {
            $dataRetrocederPeriodo = $dataRetrocederPeriodo . " 00:00:00";

            $whereSaf[] = "$dat_ab_saf < '{$dataRetrocederPeriodo}'";
            $whereSsp[] = "$dat_ab_ssp < '{$dataRetrocederPeriodo}'";
        }
    }
}

//#########################################         SAF          #############################################
//#########################################         SAF          #############################################
//#########################################         SAF          #############################################

$sqlSaf = "SELECT COUNT(*) FROM v_saf";

if (count($whereSaf)) {
    if($where != '')
        $sqlSaf = $sqlSaf . ' WHERE ' . $where . ' AND ' . implode(' AND ', $whereSaf) . ' AND ';
    else
        $sqlSaf = $sqlSaf . ' WHERE ' . implode(' AND ', $whereSaf) . ' AND ';
}else{
    if($where != '')
        $sqlSaf = $sqlSaf . ' WHERE ' . $where . ' AND ';
    else
        $sqlSaf = $sqlSaf . ' WHERE ';
}

$sqlSafEncerrada  = $sqlSaf . " cod_status = 14";
$sqlSafProgramada = $sqlSaf . " cod_status = 27";

$resultSafEncerrada  = $this->medoo->query($sqlSafEncerrada)->fetchAll(PDO::FETCH_ASSOC);
$resultSafEncerrada  = $resultSafEncerrada[0]['count'];

$resultSafProgramada = $this->medoo->query($sqlSafProgramada)->fetchAll(PDO::FETCH_ASSOC);
$resultSafProgramada = $resultSafProgramada[0]['count'];

//########################################        SSP     ##############################################
//########################################        SSP     ##############################################
//########################################        SSP     ##############################################

$sqlSsp = "SELECT COUNT(*) FROM v_ssp";

if (count($whereSsp)) {
    if($where != '')
        $sqlSsp = $sqlSsp . ' WHERE ' . $where . ' AND ' . implode(' AND ', $whereSsp) . ' AND ';
    else
        $sqlSsp = $sqlSsp . ' WHERE ' . implode(' AND ', $whereSsp) . ' AND ';
}else{
    if($where != '')
        $sqlSsp = $sqlSsp . ' WHERE ' . $where . ' AND ';
    else
        $sqlSsp = $sqlSsp . ' WHERE ';
}
$sqlSsp = $sqlSsp . " tipo_orissp IN(1, 3)";

$resultSsp = $this->medoo->query($sqlSsp)->fetchAll(PDO::FETCH_ASSOC);
$resultSsp = $resultSsp[0]['count'];

$corretivas  = $resultSafEncerrada;
$preventivas = $resultSsp - $resultSafProgramada;

$porcentagemCor = $corretivas + $preventivas;
$porcentagemCor = ($corretivas * 100) / $porcentagemCor;

$porcentagemPre = $corretivas + $preventivas;
$porcentagemPre = ($preventivas * 100) / $porcentagemPre;
?>
<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> Imprimir relatório</button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo2.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="geralPdfRel">
    <h3 class="tituloRel">
       <?php echo $_POST['titulo']?>
    </h3>

    <div class="chartRel">
        <canvas name="chart">
            <input value="<?php echo "{$corretivas}" ?>" name="corretivas" type="hidden"/>
            <input value="<?php echo "{$preventivas}" ?>" name="preventivas" type="hidden"/>
        </canvas>
    </div>

    <div>
        <table class="table">
            <tr>
                <th></th>
                <th>Quant.</th>
                <th>%</th>
            </tr>
            <tr>
                <th>Corretivas</th>
                <td><?php echo $corretivas ?></h3></td>
                <td><?php echo $porcentagemCor ?></td>
            </tr>
            <tr>
                <th>Preventivas</th>
                <td><?php echo $preventivas ?></h3></td>
                <td><?php echo $porcentagemPre ?></td>
            </tr>
        </table>
    </div>
</div>