<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4><i class="fa fa-pie-chart fa-fw"></i> Relat�rio Gerencial de Usuarios</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php

                        $sql = 'SELECT
                                  
                                  cod_usuario, usuario, ultimo_login,
                                  nome_equipe,
                                  nome_unidade
                                  
                                  FROM eq_us_unidade 
                                    JOIN usuario USING (cod_usuario)
                                    JOIN un_equipe USING (cod_un_equipe)
                                    JOIN equipe USING (cod_equipe)
                                    JOIN unidade USING (cod_unidade)
                                    ORDER BY cod_usuario';

                        $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                        ?>

                        <table class="tableRel table table-bordered table-responsive">
                            <thead>
                            <th>Usu�rio</th>
                            <th>Nome Equipe</th>
                            <th>Nome Unidade</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($result as $dados) {
                                echo("<tr>");
                                echo("<td>{$dados['usuario']}</td>");
                                echo("<td>{$dados['nome_equipe']}</td>");
                                echo("<td>{$dados['nome_unidade']}</td>");
                                echo("</tr>");
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>