<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4><i class="fa fa-pie-chart fa-fw"></i> Gerador de Relat�rio</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formRel" target="_blank" method="post">
                            <div class="page-header">
                                <div class="row">
                                    <h1>Relat�rio Din�mico</h1>
                                </div>
                            </div>

                            <input type="hidden" name="_table" />

                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Selecione o Tipo de Gr�fico</h4>
                                    <select id="select_tipo_grafico" class="form-control">
                                        <option value="" disabled selected>Selecione</option>
                                        <option value="column">Gr�fico de Colunas</option>
                                        <option value="pie">Gr�fico de Pizza</option>
                                        <!--<option value="line">Gr�fico de Linhas</option>-->
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <h4>Limite de Dados para Gr�fico</h4>
                                    <select id="select_limit_grafico" class="form-control">
                                        <option value="">Todos</option>
                                        <option>5</option>
                                        <option>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                    </select>
                                </div>
                            </div>

                            <br />

                            <div class="row select_form">
                                <div class="col-md-12">
                                    <div class="row">
                                        <?php
                                        function echoButtonForm($form){
                                            $formName = $form;
                                            $form = str_replace(" ","_",strtolower(preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($formName), ENT_QUOTES, 'ISO-8859-1'))));
                                            echo   "<div class='col-md-4 {$form}DivRel'>
                                                        <div class='panel'>
                                                            <button type='button' name='{$form}' class='btn btn-default btn-block _btnForm'>{$formName}</button>
                                                        </div>
                                                    </div>";
                                        }
                                        // $arrayform = array('saf','ssm','osm','ssp','osp','ssmp','osmp','todos');
                                        $arrayform = array('CORRETIVA');//, 'PROGRAMA��O', 'PLANO DE MANUTEN��O');
                                        array_walk($arrayform, "echoButtonForm");
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <!--  AREA DOS FILTROS -->
                            <div class="row filtros all_">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" role="tab" id="headingFiltro">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <a class="collapsed" role="button" data-toggle="collapse" href="#filtroGraf" aria-expanded="false" aria-controls="collapseFiltro">
                                                        <button class="btn btn-default"><label>Filtros</label></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="filtroGraf" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="filtroGraf">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12 dados_gerais all_">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Dados Gerais</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-4 grupoSistema">
                                                                        <label>Grupo de Sistema</label>
                                                                        <select name="grupoSistema" class="form-control">
                                                                            <option value="">Grupos de Sistemas</option>
                                                                            <?php
                                                                            $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);

                                                                            foreach ($grupoSistema as $dados => $value) {
                                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 sistema">
                                                                        <label>Sistema</label>
                                                                        <select name="sistema" class="form-control">
                                                                            <option value="">Sistemas</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 subSistema">
                                                                        <label>Sub-Sistema</label>
                                                                        <select name="subSistema" class="form-control">
                                                                            <option value="">Sub-Sistemas</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3 origemProgRel all_">
                                                                        <label>Origem</label>
                                                                        <select name="origemProgramacao" class="form-control">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            $selectop = array(
                                                                                "1" => "Preventiva",
                                                                                "2" => "Corretiva",
                                                                                "3" => "Programa��o",
                                                                                "4" => "Servi�o",
                                                                                "5" => "Preditiva"
                                                                            );
                                                                            foreach ($selectop as $dados => $value) {
                                                                                echo("<option value='{$dados}'>{$value}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6 avariaRelDiv all_">
                                                                        <label for="avaria">Op��o de Avaria</label>
                                                                        <input type="hidden" name="cod_avaria" value="">
                                                                        <input id="avaria" name="avaria" class="form-control"
                                                                               placeholder="Selecione uma Avaria"
                                                                               list="avariaDataList" autocomplete="off">

                                                                        <datalist id="avariaDataList">
                                                                            <option value=""></option>
                                                                            <?php
                                                                            $avaria = $this->medoo->select("avaria", ['cod_avaria', 'nome_avaria'], ['ORDER' => 'nome_avaria']);

                                                                            foreach ($avaria as $dados) {
                                                                                echo strtoupper("<option id='{$dados['cod_avaria']}' value='{$dados['nome_avaria']}'/>");
                                                                            }
                                                                            ?>
                                                                        </datalist>
                                                                    </div>
                                                                    <div class="col-md-3 nivelDiv all_">
                                                                        <label>Nivel</label>
                                                                        <select name="nivel" class="form-control">
                                                                            <option value="">Todos</option>
                                                                            <option value="A">A</option>
                                                                            <option value="B">B</option>
                                                                            <option value="C">C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 periodo all_">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Per�odo</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row periodoMesAno">

                                                                    <div class="col-md-3 periodicidade all_">
                                                                        <label>Periodicidade</label>
                                                                        <select class="form-control" name="periodicidadePesquisaCronograma">
                                                                            <option value="">Todos</option>
                                                                            <option value="1">Di�rio</option>
                                                                            <option value="2">Semanal</option>
                                                                            <option value="3">Quinzenal</option>
                                                                            <option value="4">Mensal</option>
                                                                            <option value="6">Trimestral</option>
                                                                            <option value="5">Bimestral</option>
                                                                            <option value="9">Anual</option>
                                                                            <option value="8">Semestral</option>
                                                                            <option value="7">Quadrimestral</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-md-3 quinzena">
                                                                        <label>Quinzena</label>
                                                                        <input class="form-control number" name="quinzenaCronograma" value="" type="text">
                                                                    </div>

                                                                    <div class="col-md-3 mes">
                                                                        <label>M�s</label>
                                                                        <select class="form-control" name="mes">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            foreach (MainController::$monthComplete as $key => $value) {
                                                                                echo("<option value='{$key}'>$value</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-3 ano">
                                                                        <label>Ano</label>
                                                                        <select class="form-control" name="ano">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            echo "<option>" . (date('Y') - 5) . "</option>";
                                                                            echo "<option>" . (date('Y') - 4) . "</option>";
                                                                            echo "<option>" . (date('Y') - 3) . "</option>";
                                                                            echo "<option>" . (date('Y') - 2) . "</option>";
                                                                            echo "<option>" . (date('Y') - 1) . "</option>";
                                                                            echo "<option>" . date('Y') . "</option>";
                                                                            echo "<option>" . (date('Y') + 1) . "</option>";
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="row periodoAPartirAte all_">
                                                                    <div class="col-md-6">
                                                                        <label>Per�odo (a Partir)</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataPartirPeriodo"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label>At�</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataRetrocederPeriodo"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 dataAbertura all_">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading datAbert all_">
                                                                <label>Data de Abertura</label>
                                                            </div>
                                                            <div class="panel-heading datProg all_">
                                                                <label>Data Programada</label>
                                                            </div>
                                                            <div class="panel-body apartirAte">
                                                                <div class="row">
                                                                    <div class="col-md-6 dt_apartir">
                                                                        <label>Data (a Partir)</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataPartirDatAber"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                    <div class="col-md-6 dt_ate">
                                                                        <label>At�</label>
                                                                        <input class="form-control data add-on validaData"
                                                                               name="dataRetrocederDatAber"
                                                                               data-format="dd/MM/yyyy"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 local all_">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Local</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-6 linha">
                                                                        <label>Linha</label>
                                                                        <select class="form-control" name="linha">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                                            foreach ($selectLinha as $dados => $value) {
                                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6 trecho">
                                                                        <label>Trecho</label>
                                                                        <select class="form-control" name="trecho">
                                                                            <option value="">Todos</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 servicoDiv all_">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Servi�o</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-6 tipoIntervencao">
                                                                        <label>Tipo de Interven��o</label>
                                                                        <select name="tipoIntervencao" class="form-control">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            $selectIntevercao = $this->medoo->select("tipo_intervencao", ['cod_tipo_intervencao', "nome_tipo_intervencao"], ["ORDER" => "nome_tipo_intervencao"]);

                                                                            foreach ($selectIntevercao as $dados => $value) {
                                                                                echo("<option value='{$value['cod_tipo_intervencao']}'>{$value["nome_tipo_intervencao"]}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6 tipoServico">
                                                                        <label>Servi�o</label>
                                                                        <select name="servico" class="form-control">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            $selectServico = $this->medoo->select("servico", ['cod_servico', "nome_servico"], ["ORDER" => "nome_servico"]);

                                                                            foreach ($selectServico as $dados => $value) {
                                                                                echo("<option value='{$value['cod_servico']}'>{$value["nome_servico"]}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row ">
                                                    <div class="col-md-12 encaminhamnetoDiv all_">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Encaminhamento</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-6 unidade">
                                                                        <label>Unidade</label>
                                                                        <select name="unidade" class="form-control">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            $selectUnidade = $this->medoo->select("unidade", ['cod_unidade', "nome_unidade"]);

                                                                            foreach ($selectUnidade as $dados => $value) {
                                                                                echo("<option value='{$value['cod_unidade']}'>{$value["nome_unidade"]}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6 equipe">
                                                                        <label>Equipe</label>
                                                                        <select name="equipe" class="form-control">
                                                                            <option value="">Todos</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row materialRodanteDiv all_" hidden>
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Material Rodante</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-4 veiculo">
                                                                        <label>Ve�culo</label>
                                                                        <select name="veiculo" class="form-control">
                                                                            <option value="">Ve�culo</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 carro">
                                                                        <label>Carro</label>
                                                                        <select name="carro" class="form-control">
                                                                            <option value="">Carro</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row bilhetagemTelecomDiv all_" hidden>
                                                    <div class="col-md-12">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <label>Detalhamento de Local</label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-4 veiculo">
                                                                        <label>Local</label>
                                                                        <select name="localGrupo" class="form-control">
                                                                            <option value="">Todos</option>
                                                                            <?php
                                                                            $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                                                            foreach($local as $dados=>$value){
                                                                                echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4 carro">
                                                                        <label>SubLocal</label>
                                                                        <select name="subLocalGrupo" class="form-control">
                                                                            <option value="">Todos</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-offset-3 col-md-6 select_opcoes_grafico all_">
                                    <h4 class="text-center">Op��es do Gr�fico</h4>
                                    <select id="select_opcoes_grafico" name="ocorrencia" class="form-control"></select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-offset-3 col-md-6 select_opcoes_grafico all_">
                                    <h5 class="text-center">T�tulo do Gr�fico</h5>
                                    <input name="titulo_grafico" class="form-control"/>
                                </div>
                            </div>


                            <br />
                            <br />

                            <!-- BOTOES DE ACAO -->
                            <div class="row">
                                <div class="col-md-8">
                                    <button id="btn_gerar_relatorio" type="button" class="btn btn-primary btn-lg all_ botoes_acao">
                                        <i class="fa fa-cogs fa-fw"></i>
                                        Gerar Relat�rio
                                    </button>
                                    <button type="reset" class="btn btn-default btn-lg resetarRel">
                                        <i class="fa fa-repeat fa-fw"></i>
                                        Resetar Filtros
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <!-- SECAO DO RELATORIO (GRAFICOS E TABELAS) -->
                <!-- SECAO DO RELATORIO (GRAFICOS E TABELAS) -->
                <!-- SECAO DO RELATORIO (GRAFICOS E TABELAS) -->


                <!-- AREA DOS GRAFICOS -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div id="area_grafico"></div>
                    </div>
                </div>

                <!-- AREA DAS TABELAS -->
                <div class="row area_tabela_relatorio all_" style="padding-top: 20px">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label class="area_tabela_titulo"></label></div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="area_tabela" class="table table-striped table-bordered no-footer">
                                            <thead><tr><th></th></tr></thead><tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>