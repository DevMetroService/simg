<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 26/01/2017
 * Time: 11:47
 */

$dados = $_POST;
$sql = "";

$form = array(
    1  => "SAF",
    2  => "SSM",
    3  => "OSM",
    4  => "SSP",
    5  => "OSP",
    6  => "OSMP",
    7  => "SSMP-Edificações",
    8  => "SSMP-Via Permanente",
    9  => "SSMP-Rede Aerea",
    10 => "SSMP-Subestação",
    11 => "Cronograma-Edificações",
    12 => "Cronograma-Via Permanente",
    13 => "Cronograma-Rede Aerea",
    14 => "Cronograma-Subestação"
);

switch ($dados['form']) {
    case 1:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_saf s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_saf = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = $form[$dados['form']];
        break;
    case 2:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssm s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_ssm = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = $form[$dados['form']];
        break;
    case 3:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_osm s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_osm = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = $form[$dados['form']];
        break;
    case 4:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_ssp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = $form[$dados['form']];
        break;
    case 5:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_osp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_osp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = $form[$dados['form']];
        break;
    case 6:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_osmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_osmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = false;
        break;
    case 7:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_ssmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = "Ssmp/Ed";
        break;
    case 8:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_ssmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = "Ssmp/Vp";
        break;
    case 9:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_ssmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = "Ssmp/Ra";
        break;
    case 10:
        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_ssmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = "Ssmp/Su";
        break;
    case 11:
        $sql = "SELECT nome_status, data_status, u.usuario, '' AS descricao FROM status_cronograma_pmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_cronograma_pmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = false;
        break;
    case 12:
        $sql = "SELECT nome_status, data_status, u.usuario, '' AS descricao FROM status_cronograma_pmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_cronograma_pmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = false;
        break;
    case 13:
        $sql = "SELECT nome_status, data_status, u.usuario, '' AS descricao FROM status_cronograma_pmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_cronograma_pmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = false;
        break;
    case 14:
        $sql = "SELECT nome_status, data_status, u.usuario, '' AS descricao FROM status_cronograma_pmp s 
                  JOIN usuario u on s.usuario = u.cod_usuario
                  JOIN status USING(cod_status)
                  WHERE cod_cronograma_pmp = {$dados['cod']} ORDER BY data_status";
        $dados['link'] = false;
        break;
}

if($sql != "")
    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Histórico de Formulário</strong></h3>
            </div>
            <div class="panel-body hidden-print">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <form method="post">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Código</label>
                                        <input required type="text" class="form-control number" name="cod" value="<?php echo $dados['cod'] ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Formulário</label>
                                        <select required class="form-control" name="form">
                                            <?php
                                            foreach ($form as $key => $value) {
                                                if ($dados['form'] == $key) {
                                                    echo("<option value='{$key}' selected >{$value}</option>");
                                                } else {
                                                    echo("<option value='{$key}' >{$value}</option>");
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn btn-primary btn-large btn-block"><i class="fa fa-refresh"></i> Pesquisar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="col-md-12">
                    <?php
                    if(!empty($dados['form'])){
                        echo "<h3>Registros {$form[$dados['form']]} {$dados['cod']}";
                        if($dados['link']){
                            echo ("  <button class='btn btn-primary btn-circle botaoVerForm' type='button' 
                            title='Visualizar formulário pesquisado' data-codigo = '{$dados['cod']}' data-form = '{$dados['link']}'>
                                <i class='fa fa-eye fa-fw'></i>
                            </button>");
                        }
                        echo('</h3>');
                    }
                    ?>
                </div>
                <div class="col-md-12">
                    <table class="tableRel table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Responsavel</th>
                                <th>Data da alteração</th>
                                <th>Complemento</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(!empty($result)){
                                foreach ($result as $dados){
                                    echo("<tr>");
                                    echo("<td>{$dados['nome_status']}</td>");
                                    echo("<td>{$dados['usuario']}</td>");
                                    echo("<td>{$this->parse_timestamp($dados['data_status'])}</td>");
                                    echo("<td>{$dados['descricao']}</td>");
                                    echo("</tr>");
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            if(!empty($result)) {
                echo '<div class="row hidden-print" style="margin-top: 5%; margin-bottom: 5%">
                                <div class="col-md-offset-4 col-md-4">
                                    <button class="btn btn-primary btn-large btn-block" onclick="window.print()"><i class="fa fa-print"></i> Imprimir</button>
                                </div>
                            </div>';
            }
            ?>
        </div>
    </div>
</div>