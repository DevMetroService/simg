<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 14/02/2019
 * Time: 11:37
 */



if($_POST){

    if($_POST['mes'] == 12){
        $proxMes = 1;
        $proxAno = $_POST['ano'] +1;
    }else{
        $proxMes = $_POST['mes'] + 1;
        $proxAno = $_POST['ano'];
    }

    $sql = "
    SELECT DISTINCT ON(nome_veiculo) nome_veiculo, v.mes, v.ano,
        CASE 
            WHEN odometro_ma<odometro_mb THEN odometro_mb 
            ELSE  odometro_ma 
        END as odometro,
        CASE 
            WHEN tracao_ma<tracao_mb THEN tracao_mb 
            ELSE  tracao_ma 
        END as tracao,
        o_final.odometro_final, o_final.tracao_final
    FROM odometro_vlt
    JOIN veiculo v USING(cod_veiculo)
    JOIN 
    (
        SELECT DISTINCT ON(nome_veiculo) nome_veiculo as veiculo, 
            CASE 
                WHEN odometro_ma<odometro_mb THEN odometro_mb 
                ELSE  odometro_ma 
            END as odometro_final,
            CASE 
                WHEN tracao_ma<tracao_mb THEN tracao_mb 
                ELSE  tracao_ma 
            END as tracao_final
        FROM odometro_vlt
        JOIN veiculo USING(cod_veiculo)
    
        WHERE data_cadastro <= '{$proxAno}-{$proxMes}-01 00:00:00' AND frota = '{$_POST['frota']}'
        ORDER BY nome_veiculo ASC, tracao_final DESC
    )as o_final ON nome_veiculo = veiculo
    WHERE data_cadastro <= '{$_POST['ano']}-{$_POST['mes']}-01 00:00:00' AND frota = '{$_POST['frota']}'
    ORDER BY nome_veiculo ASC, tracao DESC
    ";

    $veiculos = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);


    $manutencoes = ['Mensal', 'Bimensal', 'Trimestral', 'Semestral', 'Anual', '2 Anos', '3 Anos', '4 Anos', '5 Anos'];
    $qtdManutencoes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
}

?>

<div class="page-header">
    <h1> Relat�rio APP MR </h1>
    <small>Aproveitamento de Programa��o Preventivas TUE</small>
</div>

<div class="row">

    <form action="" method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Dados de Filtro</label>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label>M�s</label>
                            <select id="mes" name="mes" class="form-control">
                            <?php
                            $mes = Date('n');
                            foreach (MainController::$monthComplete as $key => $value) {
                                if ($key == $_POST['mes'])
                                    echo("<option value='{$key}' selected>{$value}</option>");
                                else
                                    if($mes == $key)
                                        echo("<option value='{$key}' selected>{$value}</option>");
                                    else
                                        echo("<option value='{$key}'>{$value}</option>");
                            }
                            ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Ano</label>
                            <select id="ano" name="ano" class="form-control">
                                <?php
                                $ano = date('Y');
                                echo ($_POST['ano'] == $ano) ? "<option selected value='{$ano}'>$ano</option>" : "<option value='{$ano}'>$ano</option>";
                                $op5 = $ano - 1;
                                echo ($_POST['ano'] == $op5) ? "<option selected value='{$op5}'>$op5</option>" : "<option value='{$op5}'>$op5</option>";
                                $op4 = $ano - 2;
                                echo ($_POST['ano'] == $op4) ? "<option selected value='{$op4}'>$op4</option>" : "<option value='{$op4}'>$op4</option>";
                                $op3 = $ano - 3;
                                echo ($_POST['ano'] == $op3) ? "<option selected value='{$op3}'>$op3</option>" : "<option value='{$op3}'>$op3</option>";
                                $op2 = $ano - 4;
                                echo ($_POST['ano'] == $op2) ? "<option selected value='{$op2}'>$op2</option>" : "<option value='{$op2}'>$op2</option>";
                                $op1 = $ano - 5;
                                echo ($_POST['ano'] == $op1) ? "<option selected value='{$op1}'>$op1</option>" : "<option value='{$op1}'>$op1</option>";

                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Frota</label>
                            <select id="frota" class="form-control" name="frota" required>
                                <option value="">Selecione a frota</option>
                                <?php
                                $frota = $this->medoo->query("SELECT distinct(frota) FROM veiculo WHERE cod_grupo = 22")->fetchAll();
                                foreach($frota as $key=>$value){
                                    echo "<option value='{$value['frota']}'";
                                    echo $_POST['frota'] == $value['frota'] ? ' selected ': ' ';
                                    echo ">{$value['frota']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-4">
                <button type="submit" class="btn btn-primary btn-large btn-block"><i
                        class="fa fa-refresh"></i> Atualizar Gr�fico
                </button>
            </div>
            <div class="col-md-4">
                <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                        class="fa fa-reply-all"></i> Limpar Filtros
                </button>
            </div>
        </div>

    </form>

    <div class="row" style="margin-top: 25px;">
        <div class="col-md-12">
            <?php
            if($_POST)
            {
            ?>
                <table class='table table-bordered table-striped table-responsive'>
                    <thead>
                    <tr>
                        <th>Ve�culo</th>
                        <th>Od�metro Atual</th>
                        <th>Od�metro Previsto</th>
                        <th>Hor�metro Atual</th>
                        <th>Hor�metro Previsto</th>
                        <th>Ciclo</th>
                        <th>Manuten��o</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach($veiculos as $dados=>$value)
                    {
                        //Verifica se os valores s�o v�lidos a partir do marco zero da manuten��o
                        if( ($_POST['ano'] == 2019 && $_POST['mes'] >= 4) || ($_POST['ano'] > 2019) ) {

                            $mesesCorridos = 0;
                            $ciclosCompletos = 0;
                            $mesManutencao = 0;

                            //Recebe a diferen�a de ano da pesquisa
                            $diferencaAno = ($_POST['ano'] - $value['ano']);

                            //Recebe diferen�a de m�s da pesquisa
                            $diferencaMes = ($_POST['mes'] - $value['mes']);

                            //Calcula os meses corridos
                            $mesesCorridos = $diferencaAno*12 + $diferencaMes;
                            //Calcula Qtd de Ciclos completos
                            $ciclosCompletos = floor($mesesCorridos/12);

                            //Verifica se � menos de um ciclo
                            if($ciclosCompletos == 0){
                                $mesManutencao = $mesesCorridos;
                            }else{
                                $mesManutencao = $mesesCorridos % 12;
                            }

                            switch ($mesManutencao) {
                                case 1:
                                case 5:
                                case 7:
                                case 11:
                                    $manutencao = 0;
                                    $qtdManutencoes[0]++;
                                    break;

                                case 2:
                                case 4:
                                case 8:
                                case 10:
                                    $manutencao = 1;
                                    $qtdManutencoes[1]++;
                                    break;

                                case 3:
                                case 9:
                                    $manutencao = 2;
                                    $qtdManutencoes[2]++;
                                    break;

                                case 6:
                                    $manutencao = 3;
                                    $qtdManutencoes[3]++;
                                    break;

                                default:
                                    switch($mesesCorridos){
                                        case 12:
                                        case 84:
                                            $manutencao = 4;
                                            $qtdManutencoes[4]++;
                                            break;

                                        case 24:
                                            $manutencao = 5;
                                            $qtdManutencoes[5]++;
                                            break;

                                        case 36:
                                        case 72:
                                            $manutencao = 6;
                                            $qtdManutencoes[6]++;
                                            break;

                                        case 48:
                                            $manutencao = 7;
                                            $qtdManutencoes[7]++;
                                            break;

                                        case 60:
                                            $manutencao = 8;
                                            $qtdManutencoes[8]++;
                                            break;

                                    }
                                    break;
                            }
                            echo("<tr>");
                            echo("<td>{$value['nome_veiculo']}</td>");
                            echo("<td>{$value['odometro']}</td>");
                            echo("<td>{$value['odometro_final']}</td>");
                            echo("<td>{$value['tracao']}</td>");
                            echo("<td>{$value['tracao_final']}</td>");
                            echo("<td>{$mesesCorridos}</td>");
                            echo("<td>{$manutencoes[$manutencao]}</td>");
                            echo("</tr>");
                        }
                    }
                    ?>
                    </tbody>
                </table>
        <?php
        }
        ?>
        </div>
    </div>
    <div class="row" style="margin-top: 25px;">
        <div class="col-md-6">
            <table class='table table-bordered table-striped table-responsive'>
                <thead>
                <tr>
                    <th>Manuten��es</th>
                    <th>Total Previstas</th>
                    <th>Total Realizadas</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($_POST) {
                    $totalPrevistas = 0;
                    $totalRealizadas = 0;
                    
                    foreach ($manutencoes as $key => $value) {
                        echo("<tr>");
                        echo("<td>$value</td>");
                        echo("<td>$qtdManutencoes[$key]</td>");

                        $totalPrevistas += $qtdManutencoes[$key];

                        switch ($key) {
                            case 0:
                                $aux = 336;
                                break;
                            case 1:
                                $aux = 337;
                                break;
                            case 2:
                                $aux = 338;
                                break;
                            case 3:
                                $aux = 339;
                                break;
                            case 4:
                                $aux = 340;
                                break;
                            case 5:
                                $aux = 341;
                                break;
                        }
                        $mes = $_POST['mes'];
                        $ano = $_POST['ano'];

                        if($mes ==12){
                            $proxMes = 1;
                            $proxAno = $ano+1;
                        }else{
                            $proxMes = $mes+1;
                            $proxAno = $ano;
                        }

                        $sql = "SELECT count(*) 
                            FROM v_ssp 
                            WHERE cod_grupo = 22 
                            AND data_abertura_ssp BETWEEN '{$_POST['ano']}-{$_POST['mes']}-01 00:00:00' 
                            AND '{$proxAno}-{$proxMes}-01 00:00:00'
                            AND cod_status IN (21, 18)
                            AND cod_servico = $aux";

                        $realizadas = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                        echo("<td>{$realizadas[0]['count']}</td>");

                        $totalRealizadas += $realizadas[0]['count'];
                        echo("</tr>");
                    }
                }

                echo("<input id='totalPrevistas' type='hidden' value={$totalPrevistas}>");
                echo("<input id='totalRealizadas' type='hidden' value={$totalRealizadas}>");
                ?>
                </tbody>
            </table>

        </div>
        <div class="col-md-6">
            <div id="container"></div>
        </div>
    </div>

</div>


