<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>MTBF - M�dia de Tempo Entre Falhas</strong></h3>
            </div>
            <div class="panel-body">
                <form id="formRelMTBF" method="post" action="">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="formMTBF" action="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/MTBF" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div id="descricaoRel">Tempo m�dia de falhas em per�odo determinado.</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>A partir</label>
                                                        <input class="form-control data" value="" name="aPartir">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>At�</label>
                                                        <input class="form-control data" value="" name="ate">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Linha</label>
                                                        <?php
                                                        $linha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);
                                                        $this->form->getSelectLinha($_POST['linha'], $linha);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Trecho</label>
                                                        <select class="form-control" name="trecho">
                                                            <option value="">TODOS</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Ponto Not�vel</label>
                                                        <select class="form-control" name="pn">
                                                            <option value="">TODOS</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Grupo de Sistema</label>
                                                        <?php
                                                        $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'],["ORDER" => "nome_grupo"]);
                                                        $this->form->getSelectGrupo($_POST['grupo'], $grupo);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Sistema</label>
                                                        <?php
                                                        $sistema = null;
                                                        if (!empty($_POST['grupo'])) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupo']]);
                                                        }
                                                        $this->form->getSelectSistema($_POST['sistema'], $sistema);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Subsistema</label>
                                                        <?php
                                                        $subsistema = null;
                                                        if (!empty($_POST['sistema']))
                                                            $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);
                                                        $this->form->getSelectSubsistema($_POST['subsistema'], $subsistema);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row materialRodanteDiv"
                                    <?php
                                    if ($_POST['grupo'] != 22 || $_POST['grupo'] != 23 || $_POST['grupo'] != 26)
                                        echo 'hidden'; ?>>
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Ve�culo</label>
                                                        <?php
                                                        if (!empty($_POST['grupo']))
                                                            $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo']);
                                                        $this->form->getSelectVeiculo($_POST['veiculo'], $veiculo);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Carro</label>
                                                        <?php
                                                        if (!empty($_POST['veiculo']))
                                                            $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_veiculo' => $_POST['veiculo']]);
                                                        $this->form->getSelectCarro($_POST['carro'], $carro);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Prefixo</label>
                                                        <?php
                                                        $prefixo = $this->medoo->select("prefixo", ['cod_prefixo']);
                                                        $this->form->getSelectPrefixo($_POST['prefixo'], $prefixo);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">

                                                    <input type="hidden" name="avaria" readonly
                                                           value="<?php if (!empty($_POST['avaria'])) echo $_POST['avaria']; ?>"/>

                                                    <div class="col-md-12 col-xs-12">
                                                        <label for="avarias">Falha</label>
                                                        <input id="avaria" name="avarias" class="form-control"
                                                               placeholder="Selecione uma Falha"
                                                               value="<?php if (!empty($_POST['avarias'])) echo $_POST['avarias']; ?>"
                                                               list="avariaDataList" autocomplete="off">

                                                        <datalist id="avariaDataList">
                                                            <option value=""></option>
                                                            <?php
                                                            $avaria = $this->medoo->select('avaria', ['cod_avaria', 'nome_avaria'], ['ORDER' => 'nome_avaria']);
                                                            foreach ($avaria as $dados) {
                                                                echo strtoupper("<option value=" . $dados['cod_avaria'] . ">" . $dados['nome_avaria'] . "</option>");
                                                            }
                                                            ?>
                                                        </datalist>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-offset-2 col-md-4">
                                        <button type="button" id="gerarRelatorio" class="atualizar btn btn-primary btn-large btn-block"><i
                                                    class="fa fa-refresh"></i> Atualizar Tabela
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                                    class="fa fa-reply-all"></i> Limpar Filtros
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <table id="tableMtbf" class="tableRel table table-bordered table-striped table-responsive">
                                    <thead>
                                    <tr>
                                        <td colspan="4"><b>Total de Falhas</b></td>
                                        <td colspan="4"><b>MTBF</b> (horas)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" id="total"></td>
                                        <td colspan="4" id="mtbf"></td>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>