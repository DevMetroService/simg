<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

$result = null;

if ($_POST) {
    $where = [];

    if ($_POST['linha']) {
        $where[] = "s.cod_linha = {$_POST['linha']}";
    }

    if ($_POST['trecho']) {
        $where[] = "s.cod_trecho = {$_POST['trecho']}";
    }

    if ($_POST['grupoSistema']) {
        $where[] = "s.cod_grupo = {$_POST['grupoSistema']}";
    }

    if ($_POST['sistema']) {
        $where[] = "s.cod_sistema = {$_POST['sistema']}";
    }

    if ($_POST['subSistema']) {
        $where[] = "s.cod_subsistema = {$_POST['subSistema']}";
    }

    $sqlWhere = implode(' AND ', $where);

    $sql = "SELECT 
            s.cod_saf,
            s.data_abertura,
            s.nome_grupo,
            s.nome_linha || ' - ' || s.descricao_trecho AS local,
            s.nome_veiculo,
            s.nome_avaria,
            s.nivel,
            
            (
                CASE
                    WHEN 
                        s.data_abertura + ('07 days') :: INTERVAL < CURRENT_TIMESTAMP
                        THEN 
                            '1'
                    WHEN 
                        s.data_abertura + ('04 days') :: INTERVAL < CURRENT_TIMESTAMP 
                        THEN 
                            '2'
                    ELSE 
                        '3'
                END
            ) AS categoria
            
            FROM v_saf s
            LEFT JOIN ssm USING (cod_saf)
            
            WHERE 
            cod_ssm NOT IN (SELECT cod_ssm FROM osm_falha)
            AND nome_status NOT IN ('Encerrada', 'Cancelada', 'Programada') ";

    $sql .= !empty($sqlWhere) ?  " AND ". $sqlWhere . " ORDER BY data_abertura" : $sqlWhere . " ORDER BY data_abertura";

    $result = $this->medoo->query($sql);
}

$cat0 = 0;
$cat4 = 0;
$cat7 = 0;
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>
                    <i class="fa fa-pie-chart fa-fw"></i>
                    <strong>
                        SAF's sem OS a mais de 7 dias
                    </strong>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if ($result)
                            echo '<div id="container"></div>';
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 hidden-print">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if ($_POST['linha'] == $value['cod_linha']) {
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Trecho</label>
                                                    <select class="form-control" name="trecho">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        if ($_POST['linha']) {
                                                            $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$_POST['linha']]);

                                                            foreach ($selectTrecho as $dados => $value) {
                                                                if ($_POST['trecho'] == $value['cod_trecho'])
                                                                    echo("<option value='{$value['cod_trecho']}' selected>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_trecho']}'>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupoSistema" class="form-control">
                                                        <option value="">Grupos de Sistemas</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($_POST['grupoSistema'] == $value['cod_grupo']) {
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if ($_POST['grupoSistema']) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if ($_POST['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if ($_POST['sistema']) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-large btn-block">
                                        <i class="fa fa-refresh"></i> Atualizar Gr�fico
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block">
                                        <i class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
<!--                                <div class="col-md-4">-->
<!--                                    <button type="button" onclick="print()" class="btn btn-default btn-large btn-block">-->
<!--                                        <i class="fa fa-print"></i> Imprimir-->
<!--                                    </button>-->
<!--                                </div>-->
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6">
                        <table class="table table-bordered text-center">
                            <tr>
                                <td id="tabCat0"></td>
                                <td style="text-align: left">
                                    <i class="fa fa-area-chart fa-2x" style="color: #0b97c4; "></i> - de 0 a 03 dias
                                </td>
                            </tr>
                            <tr>
                                <td id="tabCat4"></td>
                                <td style="text-align: left">
                                    <i class="fa fa-area-chart fa-2x" style="color: #FF5722; "></i> - de 04 a 07 dias
                                </td>
                            </tr>
                            <tr>
                                <td id="tabCat7"></td>
                                <td style="text-align: left">
                                    <i class="fa fa-area-chart fa-2x" style="color: #D50000; "></i> - Acima de 07 dias
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold" id="total"></td>
                                <td style="text-align: left">
                                    <i class="fa fa-area-chart fa-2x" style="color: #B0BEC5; "></i> - <strong>Total</strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="panel-body hidden-print">
                <div class="col-md-12">
                    <table class="tableRel table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Cod SAF</th>
                            <th>Data de Abertura</th>
                            <th>Grupo</th>
                            <th>Local</th>
                            <th>Ve�culo</th>
                            <th>Avaria</th>
                            <th>N�vel</th>
                            <th>Dias em Aberto</th>
                            <th>Categoria</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (!empty($result)){
                            foreach ($result as $dados) {
                                switch ($dados['categoria']) {
                                    case '1':
                                        $categoria = "Acima de 07 dias";
                                        $cat7++;
                                        break;
                                    case '2':
                                        $categoria = "de 04 a 07 dias";
                                        $cat4++;
                                        break;
                                    case '3':
                                        $categoria = "de 0 a 03 dias";
                                        $cat0++;
                                        break;
                                }

                                $dataAbertura = MainController::parse_timestamp_date($dados['data_abertura']);

                                $interval = date_diff(new DateTime($dataAbertura), new DateTime())->days;

                                echo("<tr>");
                                echo("<td>{$dados['cod_saf']}</td>");
                                echo("<td>{$dataAbertura}</td>");
                                echo("<td>{$dados['nome_grupo']}</td>");
                                echo("<td>{$dados['local']}</td>");
                                echo("<td>{$dados['nome_veiculo']}</td>");
                                echo("<td>{$dados['nome_avaria']}</td>");
                                echo("<td>{$dados['nivel']}</td>");
                                echo("<td>{$interval}</td>");
                                echo("<td>{$categoria}</td>");
                                echo("<td>");
                                echo("<button type='button' class='abrirModalSaf btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirSaf'><i class='fa fa-eye'></i></button>");
                                echo("</td>");
                                echo("</tr>");
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

echo "<input type='hidden' name='cat0' value='{$cat0}'/>";
echo "<input type='hidden' name='cat4' value='{$cat4}'/>";
echo "<input type='hidden' name='cat7' value='{$cat7}'/>";

$this->dashboard->modalExibirSaf();
?>