<style>
    * {
        margin: 0;
        padding: 0;
        list-style: none;
        vertical-align: baseline;
    }

    #loading {
        width: 100%;
        height: 100%;
        position: absolute;
        display: block;
        background: rgba(0, 0, 0, 0.6);
        z-index: 99999;
    }

    #imgLoading {
        position: absolute;
        top: 50%;
        left: 50%;
        height: 100px;
        margin-top: -50px;
    }
</style>

<div id="loading">
    <img id="imgLoading" src="<?php echo HOME_URI ?>/views/_images/loader/spin.gif"/>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 16/03/2017
 * Time: 11:13
 */
$medooLocal = $this->medoo;

function getValueGrafCorretivo($medoo)
{
    $arr = Array();

    if ($_POST['grupo']) {
        $where[] = "cod_grupo = {$_POST['grupo']}";
        $whereOs[] = "grupo_atuado = {$_POST['grupo']}";
    }

    if ($_POST['mes'] == 12) {
        $nextMes = 1;
        $ano = $_POST['ano'] + 1;
    } else {
        $nextMes = $_POST['mes'] + 1;
        $ano = $_POST['ano'];
    }

    if ($_POST['linha']) {
        $where[] = "cod_linha = {$_POST['linha']}";
        $whereOs[] = "cod_linha_atuado = {$_POST['linha']}";
    }

    if ($_POST['sistema']) {
        $where[] = "cod_sistema = {$_POST['sistema']}";
        $whereOs[] = "sistema_atuado = {$_POST['sistema']}";
    }

    if ($_POST['subSistema']) {
        $where[] = "cod_subsistema = {$_POST['subSistema']}";
        $whereOs[] = "subsistema_atuado = {$_POST['subSistema']}";
    }


    if ($where) {
        $where = " AND " . implode(' AND ', $where);
        $whereOs = " AND " . implode(' AND ', $whereOs);
    }


    if (strlen($_POST['mes']) == 1) {
        $tempoInicial = "01/0{$_POST['mes']}/{$ano} 00:00:00";
        $tempoFinal = "01/0{$nextMes}/{$ano} 00:00:00";
    } else {
        $tempoInicial = "01/{$_POST['mes']}/{$ano} 00:00:00";
        $tempoFinal = "01/{$nextMes}/{$ano} 00:00:00";
    }

    //Total Abertas
    $arr['totalAbertas'] = 0;

    $sql = ("SELECT
            (SELECT count(*) FROM osm_falha osm JOIN v_ssm USING(cod_ssm) WHERE osm.data_abertura >= '{$tempoInicial}' AND osm.data_abertura < '{$tempoFinal}' {$whereOs}) +
            (SELECT count(*) FROM v_ssm WHERE cod_ssm NOT IN (SELECT cod_ssm FROM osm_falha ) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$where}) +
            (SELECT count(*) FROM v_saf WHERE cod_status IN(4,3) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$where})+
            (SELECT count(*) FROM v_ssp WHERE tipo_orissp = 2 AND nome_status='Programado'  AND data_programada >= '{$tempoInicial}' AND data_programada < '{$tempoFinal}' {$where})+
            (SELECT count(*) FROM osp JOIN v_ssp USING(cod_ssp) WHERE tipo_orissp =2 AND osp.data_abertura >= '{$tempoInicial}' AND osp.data_abertura < '{$tempoFinal}' {$whereOs}) AS corretivas_abertas");
    $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $resultado = $resultado[0];

    $arr['totalAbertas'] += $resultado['corretivas_abertas'];

    //Total Executadas

    $arr['totalEncerrada'] = 0;

    $sql = ("SELECT 
            (SELECT count(*) FROM osm_falha JOIN status_osm USING(cod_ostatus) WHERE cod_status = 11 AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$whereOs})+
	        (SELECT count(*) FROM osp JOIN v_ssp USING(cod_ssp) JOIN status_osp sosp USING(cod_ospstatus) WHERE tipo_orissp = 2 AND sosp.cod_status = 11 AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$where}) AS corretivas_encerradas");
    $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $resultado = $resultado[0];

    $arr['totalEncerrada'] += $resultado['corretivas_encerradas'];

    //Canceladas

    $arr['totalCancelada'] = 0;

    $sql = ("SELECT 
            (SELECT count(*) FROM osm_falha JOIN status_osm USING(cod_ostatus) WHERE cod_status IN(23,24,25) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$whereOs})+
            (SELECT count(*) FROM v_ssp WHERE tipo_orissp =2 AND nome_status = 'Programado' AND data_programada >= '{$tempoInicial}' AND data_programada < '{$tempoFinal}' {$where})+
            (SELECT count(*) FROM osp JOIN v_ssp USING(cod_ssp) JOIN status_osp sosp USING(cod_ospstatus) WHERE tipo_orissp = 2 AND sosp.cod_status IN(23,24,25) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$whereOs})+
            (SELECT count(*) FROM v_ssm WHERE nome_status IN ('Cancelada', 'Programada') AND cod_ssm NOT IN(SELECT cod_ssm FROM osm_falha) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$where})+
            (SELECT count(*) FROM v_saf WHERE nome_status IN ('Cancelada') AND cod_saf NOT IN(SELECT cod_saf FROM ssm) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$where}) AS corretivas_canceladas");
    $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $resultado = $resultado[0];

    $arr['totalCancelada'] += $resultado['corretivas_canceladas'];

    $arr['totalAtendimento'] = $arr['totalAbertas'] - ($arr['totalCancelada'] + $arr['totalEncerrada']);

    return ($arr);
}

function getValueGrafPreventivo($medoo)
{

    $arr = Array();

    //Verifica��o de virada de ano.
    if ($_POST['mes'] == 12) {
        $nextMes = 1;
        $ano = $_POST['ano'] + 1;
    } else {
        $nextMes = $_POST['mes'] + 1;
        $ano = $_POST['ano'];
    }

    //Verificando a necessidade de se adicionar 0 em m�s e adi��o do hor�rio.
    if (strlen($_POST['mes']) == 1) {
        $tempoInicial = "01/0{$_POST['mes']}/{$ano} 00:00:00";
        $tempoFinal = "01/0{$nextMes}/{$ano} 00:00:00";
    } else {
        $tempoInicial = "01/{$_POST['mes']}/{$ano} 00:00:00";
        $tempoFinal = "01/{$nextMes}/{$ano} 00:00:00";
    }

    if ($_POST['linha']) {
        $where[] = "cod_linha = {$_POST['linha']}";
        $wherePmp[] = "cod_linha = {$_POST['linha']}";
        $whereOs[] = "cod_linha_atuado = {$_POST['linha']}";
    }

    if ($_POST['sistema']) {
        $where[] = "cod_sistema = {$_POST['sistema']}";
        $wherePmp[] = "cod_sistema = {$_POST['sistema']}";
        $whereOs[] = "sistema_atuado = {$_POST['sistema']}";
    }

    if ($_POST['subSistema']) {
        $where[] = "cod_subsistema = {$_POST['subSistema']}";
        $wherePmp[] = "cod_subsistema = {$_POST['subSistema']}";
        $whereOs[] = "subsistema_atuado = {$_POST['subSistema']}";
    }

    if ($_POST['grupo']) {
        $where[] = "cod_grupo = {$_POST['grupo']}";
        $whereOs[] = "grupo_atuado = {$_POST['grupo']}";
    }

    if ($wherePmp) {
        $where = " AND " . implode(' AND ', $where);
        $wherePmp = " AND " . implode(' AND ', $wherePmp);
        $whereOs = " AND " . implode(' AND ', $whereOs);
    } else {
        if ($where) {
            $where = " AND " . implode(' AND ', $where);
            $whereOs = " AND " . implode(' AND ', $whereOs);
        }
    }


    $selectSsmp = "";
    $selectOsmp = "";

    switch ($_POST['grupo']) {
        case 21:
            $selectPmpTotal = "SELECT (SELECT count(*) FROM osmp 
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp USING (cod_ssmp)
                                JOIN ssmp_ed USING (cod_ssmp)
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp})+
                                  (SELECT count(*) FROM ssmp_ed 
                                JOIN ssmp USING (cod_ssmp) 
                                JOIN status_ssmp USING (cod_status_ssmp)  
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)) AS preventivas_abertas";
            $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp 
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_ed USING (cod_ssmp)
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11 ) AS preventivas_encerradas";
            $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp 
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_ed USING (cod_ssmp)
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23) ) AS preventivas_canceladas";
            $arr['tituloFiltroGrupo'] = "GRUPO EDIFICACOES";
            break;
        case 24:
            $selectPmpTotal = "SELECT (SELECT count(*) FROM osmp 
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} )+
                                 (SELECT count(*) FROM ssmp_vp 
                                 JOIN ssmp USING (cod_ssmp)  
                                 JOIN status_ssmp USING (cod_status_ssmp)  
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' AND cod_status IN(19) {$wherePmp} ) AS preventivas_abertas";
            $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp 
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11) AS preventivas_encerradas";
            $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp 
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)) AS preventivas_canceladas";
            $arr['tituloFiltroGrupo'] = "GRUPO VIA PERMANENTE";
            break;
        case 25:
            $selectPmpTotal = "SELECT (SELECT count(*) FROM osmp
                                  JOIN status_osmp USING (cod_status_osmp)
                                  JOIN ssmp_su USING (cod_ssmp)
                                  JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                JOIN pmp USING (cod_pmp)
                                  JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                  JOIN sub_sistema USING(cod_sub_sistema)
                                  JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp})+
                                  (SELECT count(*) FROM ssmp_su
                                  JOIN ssmp USING (cod_ssmp)
                                  JOIN status_ssmp USING (cod_status_ssmp)
                                  JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                JOIN pmp USING (cod_pmp)
                                  JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                  JOIN sub_sistema USING(cod_sub_sistema)
                                  JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)) AS preventivas_abertas";
            $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_su USING (cod_ssmp)
                                          JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                JOIN pmp USING (cod_pmp)
                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)
                                         WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11 ) AS preventivas_encerradas";
            $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_su USING (cod_ssmp)
                                          JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                JOIN pmp USING (cod_pmp)
                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)
                                       WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)) AS preventivas_canceladas";
            $arr['tituloFiltroGrupo'] = "GRUPO SUBESTACAO";
            break;
        case 20:
            $selectPmpTotal = "SELECT (SELECT count(*)
                                FROM osmp
                                  JOIN status_osmp USING (cod_status_osmp)
                                  JOIN ssmp_ra USING (cod_ssmp)
                                  JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                  JOIN sub_sistema USING (cod_sub_sistema)
                                  JOIN local ON (pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp})+
                                  (SELECT count(*)
                                FROM ssmp_ra
                                  JOIN ssmp USING (cod_ssmp)
                                  JOIN status_ssmp USING (cod_status_ssmp)
                                  JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                  JOIN sub_sistema USING (cod_sub_sistema)
                                  JOIN local ON (pe.cod_local = local.cod_local)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)) AS preventivas_abertas";
            $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_ra USING (cod_ssmp)
                                          JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)
                                        WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11 ) AS preventivas_encerradas";
            $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_ra USING (cod_ssmp)
                                      JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON (pe.cod_local = local.cod_local)
                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23) ) AS preventivas_canceladas";
            $arr['tituloFiltroGrupo'] = "GRUPO REDE AEREA";
            break;
        default:
            break;
    }

    //########################### Total Abertas
    //########################### Total Abertas
    //########################### Total Abertas

    $arr['totalAbertas'] = 0;

    $sql = ("SELECT 
            (SELECT count(*) FROM osp JOIN v_ssp USING(cod_ssp) WHERE tipo_orissp NOT IN (2,4) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$whereOs}) +
            (SELECT count(*) FROM v_ssp WHERE cod_status = 19 AND tipo_orissp NOT IN (2,4) AND data_programada >= '{$tempoInicial}' AND data_programada < '{$tempoFinal}' {$where}) AS preventivas_abertas");
    $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $resultado = $resultado[0];

    $arr['totalAbertas'] += $resultado['preventivas_abertas'];

    if ($selectPmpTotal) {
        $sql = $selectPmpTotal;
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalAbertas'] += $resultado['preventivas_abertas'];
    }
//        $arr['totalAbertas'] = $sql;

    //############################# Total Executadas
    //############################# Total Executadas
    //############################# Total Executadas

    $arr['totalEncerrada'] = 0;

    $sql = ("SELECT 
            (SELECT count(*) FROM osp JOIN v_ssp USING(cod_ssp) JOIN status_osp sosp USING(cod_ospstatus)  WHERE tipo_orissp NOT IN (2,4) AND osp.data_abertura >= '{$tempoInicial}' AND osp.data_abertura < '{$tempoFinal}' {$whereOs} AND sosp.cod_status=11) AS preventivas_encerradas");
    $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $resultado = $resultado[0];

    $arr['totalEncerrada'] += $resultado['preventivas_encerradas'];

    if ($selectPmpEncerradas) {
        $sql = $selectPmpEncerradas;
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalEncerrada'] += $resultado['preventivas_encerradas'];
    }

    // ###########################    Canceladas
    // ###########################    Canceladas
    // ###########################    Canceladas

    $arr['totalCancelada'] = 0;

    if ($selectPmpCancelada) {
        $sql = $selectPmpCancelada;
        $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalCancelada'] += $resultado['preventivas_canceladas'];
    }

    $sql = ("SELECT 
            (SELECT count(*) FROM osp JOIN status_osp sosp USING(cod_ospstatus) JOIN v_ssp USING(cod_ssp) WHERE tipo_orissp NOT IN (2,4) AND data_abertura >= '{$tempoInicial}' AND data_abertura < '{$tempoFinal}' {$whereOs} AND sosp.cod_status IN(23,24,25)) +
            (SELECT count(*) FROM v_ssp WHERE cod_status = 17 AND tipo_orissp NOT IN (2,4) AND data_programada >= '{$tempoInicial}' AND data_programada < '{$tempoFinal}' {$where} AND cod_ssp NOT IN (SELECT cod_ssp FROM osp) ) AS preventivas_canceladas");
    $resultado = $medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $resultado = $resultado[0];

    $arr['totalCancelada'] += $resultado['preventivas_canceladas'];

    $arr['totalAtendimento'] = $arr['totalAbertas'] - ($arr['totalCancelada'] + $arr['totalEncerrada']);

    return ($arr);
}

$grupo = $this->medoo->select("grupo", "*", ["ORDER" => "cod_grupo"]);

function preencherDadosPrev($linha, $grupo, $medoo)
{
    $_POST['linha'] = $linha;
    $count = 0;

    foreach ($grupo as $dados => $value) {
        if ($_POST['grupo'] != 22 || $_POST['grupo'] != 23) {
            $_POST['grupo'] = $value['cod_grupo'];

            $arr = getValueGrafPreventivo($medoo);

            echo("<td id='exec{$grupo[$count]["cod_grupo"]}{$linha}' style='background: #ECEFF1;' >{$arr['totalEncerrada']}</td>");
            echo("<td id='nexec{$grupo[$count]["cod_grupo"]}{$linha}' style='background: #ECEFF1;'>{$arr['totalCancelada']}</td>");
            $count = $count + 1;

        }

    }
}

function preencherDadosCorr($linha, $grupo, $medoo)
{
    $_POST['linha'] = $linha;
    $count = 0;

    foreach ($grupo as $dados => $value) {
        if ($_POST['grupo'] != 22 || $_POST['grupo'] != 23) {
            $_POST['grupo'] = $value['cod_grupo'];

            $arr = getValueGrafCorretivo($medoo);

            echo("<td id='cexec{$grupo[$count]["cod_grupo"]}{$linha}' style='background: #ECEFF1;'>{$arr['totalEncerrada']}</td>");
            echo("<td id='cnexec{$grupo[$count]["cod_grupo"]}{$linha}' style='background: #ECEFF1;'>{$arr['totalCancelada']}</td>");
            $count = $count + 1;
        }
    }
}

?>

<div>
    <div>
        <img src="<?php echo HOME_URI ?>/views/_images/metrofor.png" width="100px" height="50px" style="float: left">
    </div>
    <div style="text-align: center; width: 100%">
        <h1>Levantamento de Ordens de Servi�o</h1>
    </div>
    <div style="text-align: center">
        <h3 class="text-center"><?php echo MainController::$monthComplete[$_POST['mes']] . ' / ' . $_POST['ano'] ?></h3>
    </div>
</div>

<table style="text-align: center; width: 100%">

    <tr>
        <td colspan="9"
            style="padding: 7px; font-size: large; text-align: center; background: #424242; color: #ffffff;">Preventivas
        </td>
    </tr>
    <tr>
        <td rowspan="2"
            style="padding: 7px; font-size: large; text-align: center; background: #0b97c4; color: #ffffff;">
        </td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Rede A�rea</td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Edifica��es</td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Via Permanente
        </td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Subesta��o</td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Sul</td>
        <?php
        preencherDadosPrev(5, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Oeste</td>
        <?php
        preencherDadosPrev(1, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Cariri</td>
        <?php
        preencherDadosPrev(2, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Sumar� (Sobral Sul)</td>
        <?php
        preencherDadosPrev(3, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Grendene (Sobral
            Norte)
        </td>
        <?php
        preencherDadosPrev(4, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff">Total</td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevRaExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevRaNExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevEdExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevEdNExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevVpExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevVpNExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevSbExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="prevSbNExec"></td>
    </tr>
    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
    <tr>
        <td colspan="9"
            style="padding: 7px; font-size: large; text-align: center; background: #424242; color: #ffffff;">Corretivas
        </td>
    </tr>
    <tr>
        <td rowspan="2"
            style="padding: 7px; font-size: large; text-align: center; background: #0b97c4; color: #ffffff;"></td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Rede A�rea</td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Edifica��es</td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Via Permanente
        </td>
        <td colspan="2" style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Subesta��o</td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Executadas</td>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">N�o Executadas</td>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Sul</td>
        <?php
        preencherDadosCorr(5, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Oeste</td>
        <?php
        preencherDadosCorr(1, $grupo, $this->medoo);
        ?>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Cariri</td>
        <?php
        preencherDadosCorr(2, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Sumar� (Sobral Sul)</td>
        <?php
        preencherDadosCorr(3, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Linha Grendene (Sobral
            Norte)
        </td>
        <?php
        preencherDadosCorr(4, $grupo, $this->medoo);
        ?>
    </tr>
    <tr>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff">Total</td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrRaExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrRaNExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrEdExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrEdNExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrVpExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrVpNExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrSbExec"></td>
        <td style="text-align: center; padding: 7px; background: #4CAF50; color: #ffffff" id="corrSbNExec"></td>
    </tr>
</table>

<script src="<?php echo HOME_URI ?>views/_js/jquery.min.js"></script>
<script src="<?php echo HOME_URI ?>views/_js/scripts/relatorios/scriptLevantamentoOcorrenciasResult.js"></script>