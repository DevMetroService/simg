<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);
$whereLancadas = [];
$whereAtendidas = [];
$whereCanceladas = [];

//Monta o SQL para o tipo de formul�rio sem a OSMP.
if($_POST['form'] != "osmp") {
    switch ($_POST['form']) {
        case "saf":
            $sql = 'SELECT nome_grupo, COUNT(nome_grupo) AS quant FROM v_saf';
            $scriptNotAtendidas = "'Cancelada','Encerrada','Programada'";
            $scriptCanceladas = "'Cancelada'";
            break;
        case "ssm":
            $sql = 'SELECT nome_grupo, COUNT(nome_grupo) AS quant FROM v_ssm';
            $scriptNotAtendidas = "'Cancelada', 'Agendada', 'Encerrada','Programada'";
            $scriptCanceladas = "'Cancelada', 'Agendada', 'Programada'";
            break;
        case "osm":
            $sql = 'SELECT nome_grupo, COUNT(nome_grupo) AS quant
                        FROM osm_falha osm
                        JOIN status_osm sto USING (cod_ostatus)
                        JOIN status USING (cod_status)
                        JOIN osm_servico os ON os.cod_osm = osm.cod_osm
                        LEFT JOIN osm_encerramento oe ON oe.cod_osm = osm.cod_osm
                        LEFT JOIN osm_registro oreg ON oreg.cod_osm = osm.cod_osm
                        LEFT JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                        LEFT JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                        JOIN grupo ON osm.grupo_atuado = grupo.cod_grupo';
            $scriptNotAtendidas = "'N�o Executado','N�o Configura falha','Duplicado','Encerrada'";
            $scriptCanceladas = "'N�o Executado','N�o Configura falha','Duplicado'";
            break;
        case "ssp":
            $sql = 'SELECT nome_grupo, COUNT(nome_grupo) AS quant FROM v_ssp';
            $scriptNotAtendidas = "'Cancelada','Encerrada'";
            $scriptCanceladas = "'Cancelada'";
            break;
        case "osp":
            $sql = 'SELECT nome_grupo, COUNT(nome_grupo) AS quant
                            FROM osp
                            JOIN status_osp sto USING (cod_ospstatus)
                            JOIN status USING (cod_status)
                            JOIN osp_servico os ON os.cod_osp = osp.cod_osp
                            LEFT JOIN osp_encerramento oe ON oe.cod_osp = osp.cod_osp
                            LEFT JOIN osp_registro oreg ON oreg.cod_osp = osp.cod_osp
                            LEFT JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                            LEFT JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                            JOIN ssp USING (cod_ssp)
                            JOIN grupo ON osp.grupo_atuado = grupo.cod_grupo';
            $scriptNotAtendidas = "'N�o Executado','N�o Configura falha','Duplicado', 'Encerrada'";
            $scriptCanceladas = "'N�o Executado','N�o Configura falha','Duplicado'";
            break;
    }

//############         Periodo
    $dataPartirPeriodo = inverteData($_POST['dataPartirPeriodo']);
    $dataRetrocederPeriodo = inverteData($_POST['dataRetrocederPeriodo']);

    if ($dataPartirPeriodo != "" || $dataRetrocederPeriodo != "") {
        if ($_POST['form'] == "ssp") {
            $dat_ab = "data_programada";
        } else if ($_POST['form'] == "osm") {
            $dat_ab = "osm.data_abertura";
        } else if ($_POST['form'] == "osp") {
            $dat_ab = "osp.data_abertura";
        } else {
            $dat_ab = "data_abertura";
        }

        if ($dataPartirPeriodo == $dataRetrocederPeriodo) {
            $dataPartirPeriodo = $dataPartirPeriodo . " 00:00:00";
            $dataRetrocederPeriodo = $dataRetrocederPeriodo . " 23:59:59";

            $whereLancadas[] = "$dat_ab >= '{$dataPartirPeriodo}'";
            $whereLancadas[] = "$dat_ab <= '{$dataRetrocederPeriodo}'";

            $whereAtendidas[] = "data_status >= '{$dataPartirPeriodo}'";
            $whereAtendidas[] = "data_status <= '{$dataRetrocederPeriodo}'";

            $whereCanceladas[] = "data_status >= '{$dataPartirPeriodo}'";
            $whereCanceladas[] = "data_status <= '{$dataRetrocederPeriodo}'";

        } else {
            if ($dataPartirPeriodo) {
                $dataPartirPeriodo = $dataPartirPeriodo . " 00:00:00";

                $whereLancadas[] = "$dat_ab >= '{$dataPartirPeriodo}'";
                $whereAtendidas[] = "data_status >= '{$dataPartirPeriodo}'";
                $whereCanceladas[] = "data_status >= '{$dataPartirPeriodo}'";
            }

            if ($dataRetrocederPeriodo) {
                $dataRetrocederPeriodo = $dataRetrocederPeriodo . " 23:59:59";

                $whereLancadas[] = "$dat_ab < '{$dataRetrocederPeriodo}'";
                $whereAtendidas[] = "data_status < '{$dataRetrocederPeriodo}'";
                $whereCanceladas[] = "data_status < '{$dataRetrocederPeriodo}'";
            }
        }
    }

//#########################################         Lan�adas          #############################################
//#########################################         Lan�adas          #############################################
//#########################################         Lan�adas          #############################################

    $sqlLancadas = $sql;

    if (!empty($whereLancadas) > 0) {
        if ($where != '')
            $sqlLancadas = $sqlLancadas . ' WHERE ' . $where . ' AND ' . implode(' AND ', $whereLancadas);
        else
            $sqlLancadas = $sqlLancadas . ' WHERE ' . implode(' AND ', $whereLancadas);
    } else {
        if ($where != '')
            $sqlLancadas = $sqlLancadas . ' WHERE ' . $where;
    }
    $sqlLancadas = $sqlLancadas . " GROUP BY nome_grupo";

    $resultLancadas = $this->medoo->query($sqlLancadas)->fetchAll(PDO::FETCH_ASSOC);

    $lancadas = array();
    foreach ($resultLancadas as $dados) {
        $lancadas[$dados['nome_grupo']] = $dados['quant'];
    }

    $edL = (int)$lancadas['EDIFICA��ES'];
    $vpL = (int)$lancadas['VIA PERMANENTE'];
    $suL = (int)$lancadas['SUBESTA��O'];
    $raL = (int)$lancadas['REDE A�REA'];
    $enL = (int)$lancadas['ENERGIA'];
    $biL = (int)$lancadas['BILHETAGEM'];
    $teL = (int)$lancadas['TELECOM'];
    $vltL = (int)$lancadas['MATERIAL RODANTE - VLT'];
    $tueL = (int)$lancadas['MATERIAL RODANTE - TUE'];
    $locL = (int)$lancadas['MATERIAL RODANTE - LOCOMOTIVA'];

    $somaLancadas = $edL + $vpL + $suL + $raL + $vltL + $tueL + $locL + $enL;

//########################################        ATENDIDAS     ##############################################
//########################################        ATENDIDAS     ##############################################
//########################################        ATENDIDAS     ##############################################

    $sqlAtendidas = $sql . " WHERE nome_status = 'Encerrada'";

    if (!empty($whereAtendidas)) {
        if ($where != '')
            $sqlAtendidas = $sqlAtendidas . ' AND ' . $where . ' AND ' . implode(' AND ', $whereAtendidas);
        else
            $sqlAtendidas = $sqlAtendidas . ' AND ' . implode(' AND ', $whereAtendidas);
    } else {
        if ($where != '')
            $sqlAtendidas = $sqlAtendidas . ' AND ' . $where;
    }
    $sqlAtendidas = $sqlAtendidas . " GROUP BY nome_grupo";

    $resultAtendidas = $this->medoo->query($sqlAtendidas)->fetchAll(PDO::FETCH_ASSOC);

    $atendidas = array();
    foreach ($resultAtendidas as $dados) {
        $atendidas[$dados['nome_grupo']] = $dados['quant'];
    }

    $edA = (int)$atendidas['EDIFICA��ES'];
    $vpA = (int)$atendidas['VIA PERMANENTE'];
    $suA = (int)$atendidas['SUBESTA��O'];
    $raA = (int)$atendidas['REDE A�REA'];
    $enA = (int)$atendidas['ENERGIA'];
    $biA = (int)$atendidas['BILHETAGEM'];
    $teA = (int)$atendidas['TELECOM'];
    $vltA = (int)$atendidas['MATERIAL RODANTE - VLT'];
    $tueA = (int)$atendidas['MATERIAL RODANTE - TUE'];
    $locA = (int)$atendidas['MATERIAL RODANTE - LOCOMOTIVA'];

    $somaAtendidas = $edA + $vpA + $suA + $raA + $vltA + $tueA + $locA + $enA;

//########################################        EM ATENDIMENTO     ##############################################
//########################################        EM ATENDIMENTO     ##############################################
//########################################        EM ATENDIMENTO     ##############################################

    $sqlNotAtendidas = $sql . " WHERE nome_status NOT IN($scriptNotAtendidas)";

    if (!empty($whereAtendidas)) {
        if ($where != '')
            $sqlNotAtendidas = $sqlNotAtendidas . ' AND ' . $where . ' AND ' . implode(' AND ', $whereAtendidas);
        else
            $sqlNotAtendidas = $sqlNotAtendidas . ' AND ' . implode(' AND ', $whereAtendidas);
    } else {
        if ($where != '')
            $sqlNotAtendidas = $sqlNotAtendidas . ' AND ' . $where;
    }
    $sqlNotAtendidas = $sqlNotAtendidas . " GROUP BY nome_grupo";

    $resultNotAtendidas = $this->medoo->query($sqlNotAtendidas)->fetchAll(PDO::FETCH_ASSOC);

    $emAtendimento = array();
    foreach ($resultNotAtendidas as $dados) {
        $emAtendimento[$dados['nome_grupo']] = $dados['quant'];
    }

    $edE = (int)$emAtendimento['EDIFICA��ES'];
    $vpE = (int)$emAtendimento['VIA PERMANENTE'];
    $suE = (int)$emAtendimento['SUBESTA��O'];
    $raE = (int)$emAtendimento['REDE A�REA'];
    $enE = (int)$emAtendimento['ENERGIA'];
    $biE = (int)$emAtendimento['BILHETAGEM'];
    $teE = (int)$emAtendimento['TELECOM'];
    $vltE = (int)$emAtendimento['MATERIAL RODANTE - VLT'];
    $tueE = (int)$emAtendimento['MATERIAL RODANTE - TUE'];
    $locE = (int)$emAtendimento['MATERIAL RODANTE - LOCOMOTIVA'];

    $somaEmAtendimento = $edE + $vpE + $suE + $raE + $vltE + $tueE + $locE + $enE;

//########################################        CANCELADAS     ##############################################
//########################################        CANCELADAS     ##############################################
//########################################        CANCELADAS     ##############################################

$sqlCanceladas = $sql . " WHERE nome_status IN($scriptCanceladas)";

if (!empty($whereCanceladas)) {
    if ($where != '')
        $sqlCanceladas = $sqlCanceladas . ' AND ' . $where . ' AND ' . implode(' AND ', $whereCanceladas);
    else
        $sqlCanceladas = $sqlCanceladas . ' AND ' . implode(' AND ', $whereCanceladas);
} else {
    if ($where != '')
        $sqlCanceladas = $sqlCanceladas . ' AND ' . $where;
}
$sqlCanceladas = $sqlCanceladas . " GROUP BY nome_grupo";

$resultCanceladas = $this->medoo->query($sqlCanceladas)->fetchAll(PDO::FETCH_ASSOC);

$Canceladas = array();
foreach ($resultCanceladas as $dados) {
    $Canceladas[$dados['nome_grupo']] = $dados['quant'];
}

$edC = (int)$Canceladas['EDIFICA��ES'];
$vpC = (int)$Canceladas['VIA PERMANENTE'];
$suC = (int)$Canceladas['SUBESTA��O'];
$raC = (int)$Canceladas['REDE A�REA'];
$enC = (int)$Canceladas['ENERGIA'];
$biC = (int)$Canceladas['BILHETAGEM'];
$teC = (int)$Canceladas['TELECOM'];
$vltC = (int)$Canceladas['MATERIAL RODANTE - VLT'];
$tueC = (int)$Canceladas['MATERIAL RODANTE - TUE'];
$locC = (int)$Canceladas['MATERIAL RODANTE - LOCOMOTIVA'];

$somaCanceladas = $edC + $vpC + $suC + $raC + $vltC + $tueC + $locC + $enC;

    $label = array();
    $tableLabel = '';

    $lancadasValue = array();
    $tableLancadas = '';

    $atendidasValue = array();
    $tableAtendidas = '';

    $emAtendimentoValue = array();
    $tableEmAtendimento = '';

    $canceladasValue = array();
    $tableCanceladas = '';

    if ($edL > 0 || $edA > 0 || $edE > 0 || $edC > 0) {
        $label[] = 'EDIFICA��ES';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">EDIFICA��ES</th>';

        $lancadasValue[] = $edL;
        $tableLancadas .= '<td>' . $edL . '</td>';

        $atendidasValue[] = $edA;
        $tableAtendidas .= '<td>' . $edA . '</td>';

        $emAtendimentoValue[] = $edE;
        $tableEmAtendimento .= '<td>' . $edE . '</td>';

        $canceladasValue[] = $edC;
        $tableCanceladas .= '<td>' . $edC . '</td>';
    }
    if ($vpL > 0 || $vpA > 0 || $vpE > 0 || $vpC > 0) {
        $label[] = 'VIA PERMANENTE';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">VIA PERMANENTE</th>';

        $lancadasValue[] = $vpL;
        $tableLancadas .= '<td>' . $vpL . '</td>';

        $atendidasValue[] = $vpA;
        $tableAtendidas .= '<td>' . $vpA . '</td>';

        $emAtendimentoValue[] = $vpE;
        $tableEmAtendimento .= '<td>' . $vpE . '</td>';

        $canceladasValue[] = $vpC;
        $tableCanceladas .= '<td>' . $vpC . '</td>';
    }
    if ($suL > 0 || $suA > 0 || $suE > 0 || $suC > 0 ) {
        $label[] = 'SUBESTA��O';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">SUBESTA��O</th>';

        $lancadasValue[] = $suL;
        $tableLancadas .= '<td>' . $suL . '</td>';

        $atendidasValue[] = $suA;
        $tableAtendidas .= '<td>' . $suA . '</td>';

        $emAtendimentoValue[] = $suE;
        $tableEmAtendimento .= '<td>' . $suE . '</td>';

        $canceladasValue[] = $suC;
        $tableCanceladas .= '<td>' . $suC . '</td>';
    }
    if ($raL > 0 || $raA > 0 || $raE > 0 || $raC > 0) {
        $label[] = 'REDE A�REA';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">REDE A�REA</th>';

        $lancadasValue[] = $raL;
        $tableLancadas .= '<td>' . $raL . '</td>';

        $atendidasValue[] = $raA;
        $tableAtendidas .= '<td>' . $raA . '</td>';

        $emAtendimentoValue[] = $raE;
        $tableEmAtendimento .= '<td>' . $raE . '</td>';

        $canceladasValue[] = $raC;
        $tableCanceladas .= '<td>' . $raC . '</td>';
    }
    if ($biL > 0 || $biA > 0 || $biE > 0 || $biC > 0) {
        $label[] = 'BILHETAGEM';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">BILHETAGEM</th>';

        $lancadasValue[] = $biL;
        $tableLancadas .= '<td>' . $biL . '</td>';

        $atendidasValue[] = $biA;
        $tableAtendidas .= '<td>' . $biA . '</td>';

        $emAtendimentoValue[] = $biE;
        $tableEmAtendimento .= '<td>' . $biE . '</td>';

        $canceladasValue[] = $biC;
        $tableCanceladas .= '<td>' . $biC . '</td>';
    }
    if ($teL > 0 || $teA > 0 || $teE > 0 || $teC > 0) {
        $label[] = 'TELECOM';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">TELECOM</th>';

        $lancadasValue[] = $teL;
        $tableLancadas .= '<td>' . $teL . '</td>';

        $atendidasValue[] = $teA;
        $tableAtendidas .= '<td>' . $teA . '</td>';

        $emAtendimentoValue[] = $teE;
        $tableEmAtendimento .= '<td>' . $teE . '</td>';

        $canceladasValue[] = $teC;
        $tableCanceladas .= '<td>' . $teC . '</td>';
    }
    if ($vltL > 0 || $vltA > 0 || $vltE > 0 || $vltC > 0) {
        $label[] = 'MATERIAL RODANTE - VLT';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">MATERIAL RODANTE - VLT</th>';

        $lancadasValue[] = $vltL;
        $tableLancadas .= '<td>' . $vltL . '</td>';

        $atendidasValue[] = $vltA;
        $tableAtendidas .= '<td>' . $vltA . '</td>';

        $emAtendimentoValue[] = $vltE;
        $tableEmAtendimento .= '<td>' . $vltE . '</td>';

        $canceladasValue[] = $vltC;
        $tableCanceladas .= '<td>' . $vltC . '</td>';
    }
    if ($tueL > 0 || $tueA > 0 || $tueE > 0 || $tueC > 0) {
        $label[] = 'MATERIAL RODANTE - TUE';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">MATERIAL RODANTE - TUE</th>';

        $lancadasValue[] = $tueL;
        $tableLancadas .= '<td>' . $tueL . '</td>';

        $atendidasValue[] = $tueA;
        $tableAtendidas .= '<td>' . $tueA . '</td>';

        $emAtendimentoValue[] = $tueE;
        $tableEmAtendimento .= '<td>' . $tueE . '</td>';

        $canceladasValue[] = $tueC;
        $tableCanceladas .= '<td>' . $tueC . '</td>';
    }
    if ($locL > 0 || $locA > 0 || $locE > 0 || $locC > 0) {
        $label[] = 'MATERIAL RODANTE - LOCOMOTIVA';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">MATERIAL RODANTE - LOCOMOTIVA</th>';

        $lancadasValue[] = $locL;
        $tableLancadas .= '<td>' . $locL . '</td>';

        $atendidasValue[] = $locA;
        $tableAtendidas .= '<td>' . $locA . '</td>';

        $emAtendimentoValue[] = $locE;
        $tableEmAtendimento .= '<td>' . $locE . '</td>';

        $canceladasValue[] = $locC;
        $tableCanceladas .= '<td>' . $locC . '</td>';
    }
    if ($enL > 0 || $enA > 0 || $enE > 0 || $enC > 0) {
        $label[] = 'ENERGIA';
        $tableLabel .= '<th class="legendRel tituloTabelaRel">ENERGIA</th>';

        $lancadasValue[] = $enL;
        $tableLancadas .= '<td>' . $enL . '</td>';

        $atendidasValue[] = $enA;
        $tableAtendidas .= '<td>' . $enA . '</td>';

        $emAtendimentoValue[] = $enE;
        $tableEmAtendimento .= '<td>' . $enE . '</td>';

        $canceladasValue[] = $enC;
        $tableCanceladas .= '<td>' . $enC . '</td>';
    }
    if (count($label) > 1) {
        $label[] = "TOTAL";
        $tableLabel .= '<th class="legendRel tituloTabelaRel">TOTAL</th>';

        $lancadasValue[] = $somaLancadas;
        $tableLancadas .= '<td>' . $somaLancadas . '</td>';

        $atendidasValue[] = $somaAtendidas;
        $tableAtendidas .= '<td>' . $somaAtendidas . '</td>';

        $emAtendimentoValue[] = $somaEmAtendimento;
        $tableEmAtendimento .= '<td>' . $somaEmAtendimento . '</td>';

        $canceladasValue[] = $somaCanceladas;
        $tableCanceladas .= '<td>' . $somaCanceladas . '</td>';
    }

    $txtLabel = implode(',', $label);
    $txtLancadasValue = implode(',', $lancadasValue);
    $txtAtendidasValue = implode(',', $atendidasValue);
    $txtEmAtendimentoValue = implode(',', $emAtendimentoValue);
    $txtCanceladasValue = implode(',', $canceladasValue);
}else{
    if(!empty($_POST['grupoSistema'])){

        $joins = '';
        $label = [];

        switch ($_POST['grupoSistema']){
            case '20': //Rede A�rea
                $joins = "JOIN osmp_ra USING(cod_osmp) JOIN local USING(cod_local)";
                $label[] = 'REDE A�REA';
                break;

            case '21': //Edifica��es
                $joins = "JOIN osmp_ed USING(cod_osmp) JOIN local USING(cod_local)";
                $label[] = 'EDIFICA��ES';
                break;

            case '24': //Via Permanente
                $joins = "JOIN osmp_vp USING(cod_osmp) JOIN estacao ON(cod_estacao_inicial = cod_estacao)";
                $label[] = 'VIA PERMANENTE';
                break;

            case '25': //Subesta��o
                $joins = "JOIN osmp_su USING(cod_osmp) JOIN local USING(cod_local)";
                $label[] = 'SUBESTA��O';
                break;

            case '27': //TELECOM
                $joins = "JOIN osmp_te USING (cod_osmp) JOIN local USING(cod_local)";
                $label[] = 'TELECOM';
                break;

            case '28': // Bilhetagem
                $joins = "JOIN osmp_bi USING(cod_osmp) JOIN estacao USING(cod_estacao)";
                $label[] = 'BILHETAGEM';
                break;
        }

        $sql = "
            SELECT COUNT(*) FROM osmp

            {$joins}
            
            JOIN status_osmp USING(cod_status_osmp)
            JOIN ssmp USING(cod_ssmp)
            JOIN cronograma_pmp USING(cod_cronograma_pmp)
            JOIN pmp USING(cod_pmp)
            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
            JOIN sub_sistema USING(cod_sub_sistema)";

        //############         Periodo
        $dataPartirPeriodo = inverteData($_POST['dataPartirPeriodo']);
        $dataRetrocederPeriodo = inverteData($_POST['dataRetrocederPeriodo']);

        if ($dataPartirPeriodo != "" || $dataRetrocederPeriodo != "") {
            $dat_ab = "osmp.data_abertura";

            if ($dataPartirPeriodo == $dataRetrocederPeriodo) {
                $dataPartirPeriodo = $dataPartirPeriodo . " 00:00:00";
                $dataRetrocederPeriodo = $dataRetrocederPeriodo . " 23:59:59";

                $whereLancadas[] = "$dat_ab >= '{$dataPartirPeriodo}'";
                $whereLancadas[] = "$dat_ab <= '{$dataRetrocederPeriodo}'";

                $whereAtendidas[] = "data_status >= '{$dataPartirPeriodo}'";
                $whereAtendidas[] = "data_status <= '{$dataRetrocederPeriodo}'";

                $whereCanceladas[] = "data_status >= '{$dataPartirPeriodo}'";
                $whereCanceladas[] = "data_status <= '{$dataRetrocederPeriodo}'";

            } else {
                if ($dataPartirPeriodo) {
                    $dataPartirPeriodo = $dataPartirPeriodo . " 00:00:00";

                    $whereLancadas[] = "$dat_ab >= '{$dataPartirPeriodo}'";
                    $whereAtendidas[] = "data_status >= '{$dataPartirPeriodo}'";
                    $whereCanceladas[] = "data_status >= '{$dataPartirPeriodo}'";
                }

                if ($dataRetrocederPeriodo) {
                    $dataRetrocederPeriodo = $dataRetrocederPeriodo . " 23:59:59";

                    $whereLancadas[] = "$dat_ab < '{$dataRetrocederPeriodo}'";
                    $whereAtendidas[] = "data_status < '{$dataRetrocederPeriodo}'";
                    $whereCanceladas[] = "data_status < '{$dataRetrocederPeriodo}'";
                }
            }
        }

        //#########################################         Lan�adas          #############################################
        //#########################################         Lan�adas          #############################################
        //#########################################         Lan�adas          #############################################

        $sqlLancadas = $sql;

        if (!empty($whereLancadas) > 0) {
            if ($where != '')
                $sqlLancadas = $sqlLancadas . ' WHERE ' . $where . ' AND ' . implode(' AND ', $whereLancadas);
            else
                $sqlLancadas = $sqlLancadas . ' WHERE ' . implode(' AND ', $whereLancadas);
        } else {
            if ($where != '')
                $sqlLancadas = $sqlLancadas . ' WHERE ' . $where;
        }

        $resultLancadas = $this->medoo->query($sqlLancadas)->fetchAll(PDO::FETCH_ASSOC);
        $somaLancadas = $resultLancadas[0]['count'];

        //########################################        ATENDIDAS     ##############################################
        //########################################        ATENDIDAS     ##############################################
        //########################################        ATENDIDAS     ##############################################

        $sqlAtendidas = $sql . " WHERE cod_status = 11";

        if (!empty($whereAtendidas)) {
            if ($where != '')
                $sqlAtendidas = $sqlAtendidas . ' AND ' . $where . ' AND ' . implode(' AND ', $whereAtendidas);
            else
                $sqlAtendidas = $sqlAtendidas . ' AND ' . implode(' AND ', $whereAtendidas);
        } else {
            if ($where != '')
                $sqlAtendidas = $sqlAtendidas . ' AND ' . $where;
        }

        $resultAtendidas = $this->medoo->query($sqlAtendidas)->fetchAll(PDO::FETCH_ASSOC);
        $somaAtendidas = $resultAtendidas[0]['count'];

        //########################################        EM ATENDIMENTO     ##############################################
        //########################################        EM ATENDIMENTO     ##############################################
        //########################################        EM ATENDIMENTO     ##############################################

        $sqlNotAtendidas = $sql . " WHERE cod_status = 10";

        if (!empty($whereAtendidas)) {
            if ($where != '')
                $sqlNotAtendidas = $sqlNotAtendidas . ' AND ' . $where . ' AND ' . implode(' AND ', $whereAtendidas);
            else
                $sqlNotAtendidas = $sqlNotAtendidas . ' AND ' . implode(' AND ', $whereAtendidas);
        } else {
            if ($where != '')
                $sqlNotAtendidas = $sqlNotAtendidas . ' AND ' . $where;
        }

        $resultNotAtendidas = $this->medoo->query($sqlNotAtendidas)->fetchAll(PDO::FETCH_ASSOC);
        $somaEmAtendimento = $resultNotAtendidas[0]['count'];

        //########################################        CANCELADAS     ##############################################
        //########################################        CANCELADAS     ##############################################
        //########################################        CANCELADAS     ##############################################

        $sqlCanceladas = $sql . " WHERE cod_status IN(24,25,23,38)";

        if (!empty($whereCanceladas)) {
            if ($where != '')
                $sqlCanceladas = $sqlCanceladas . ' AND ' . $where . ' AND ' . implode(' AND ', $whereCanceladas);
            else
                $sqlCanceladas = $sqlCanceladas . ' AND ' . implode(' AND ', $whereCanceladas);
        } else {
            if ($where != '')
                $sqlCanceladas = $sqlCanceladas . ' AND ' . $where;
        }

        $resultCanceladas = $this->medoo->query($sqlCanceladas)->fetchAll(PDO::FETCH_ASSOC);
        $somaCanceladas = $resultCanceladas[0]['count'];


        //########################################        MONTAGEM     ##############################################
        //########################################        MONTAGEM     ##############################################
        //########################################        MONTAGEM     ##############################################


        $tableLabel = '';
        $lancadasValue = [];
        $tableLancadas = '';

        $atendidasValue = [];
        $tableAtendidas = '';

        $emAtendimentoValue = [];
        $tableEmAtendimento = '';

        $canceladasValue = [];
        $tableCanceladasValue = '';

        $tableLabel .= "<th class='legendRel tituloTabelaRel'>{$label[0]}</th>";

        $lancadasValue[] = $somaLancadas;
        $tableLancadas .= '<td>' . $somaLancadas . '</td>';

        $atendidasValue[] = $somaAtendidas;
        $tableAtendidas .= '<td>' . $somaAtendidas . '</td>';

        $emAtendimentoValue[] = $somaEmAtendimento;
        $tableEmAtendimento .= '<td>' . $somaEmAtendimento . '</td>';

        $canceladasValue[] = $somaCanceladas;
        $tableCanceladas = '<td>' . $somaCanceladas . '</td>';

        $txtLabel = implode(',', $label);
        $txtLancadasValue = implode(',', $lancadasValue);
        $txtAtendidasValue = implode(',', $atendidasValue);
        $txtEmAtendimentoValue = implode(',', $emAtendimentoValue);
        $txtCanceladasValue = implode(',', $canceladasValue);
    }
}

?>
<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> Imprimir
            Relat�rio
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>
    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="geralPdfRel">

    <?php
    echo("
    <input value='{$_POST['form']}' name='form' type='hidden'/>
    <input value='{$_POST['dataPartirPeriodo']}' name='dataDe' type='hidden'/>
    <input value='{$_POST['dataRetrocederPeriodo']}' name='dataAte' type='hidden'/>
    <input value='{$txtLabel}' name='label' type='hidden'/>
    <input value='{$txtLancadasValue}' name='lancadas' type='hidden'/>
    <input value='{$txtAtendidasValue}' name='atendidas' type='hidden'/>
    <input value='{$txtEmAtendimentoValue}' name='emAtendimento' type='hidden'/>
    <input value='{$txtCanceladasValue}' name='canceladas' type='hidden'/>
    <input value='{$_POST['grupoSistema']}' name='grupoSistema' type='hidden'/>");
    ?>

    <div class="chartRel">
        <div id="chart"></div>
    </div>

    <div>
        <?php if ($tableLabel != '') { ?>
            <table class="tableRel table table-bordered table-responsive">
                <tr>
                    <th></th>
                    <?php echo $tableLabel ?>
                </tr>
                <tr>
                    <th>
                        LAN�ADAS
                    </th>
                    <?php echo $tableLancadas ?>
                </tr>
                <tr>
                    <th>
                        ATENDIDAS
                    </th>
                    <?php echo $tableAtendidas ?>
                </tr>
                <tr>
                    <th>
                        EM ATENDIMENTO
                    </th>
                    <?php echo $tableEmAtendimento ?>
                </tr>
                <tr>
                    <th>
                        CANCELADAS
                    </th>
                    <?php echo $tableCanceladas ?>
                </tr>
            </table>
        <?php } ?>
    </div>
</div>