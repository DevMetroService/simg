<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);

//var_dump($_POST);

switch ($_POST['form']) {
    case "saf":
        $sql = 'SELECT nome_status, COUNT(cod_status) AS quant FROM v_saf';
        break;
    case "ssm":
        $sql = 'SELECT nome_status, COUNT(cod_status) AS quant FROM v_ssm';
        break;
    case "osm":
        $sql = 'SELECT nome_status, COUNT(cod_status) AS quant
                        FROM osm_falha osm
                        JOIN status_osm sto USING (cod_ostatus)
                        JOIN status USING (cod_status)
                        JOIN osm_servico os ON os.cod_osm = osm.cod_osm
                        LEFT JOIN osm_encerramento oe ON oe.cod_osm = osm.cod_osm
                        LEFT JOIN osm_registro oreg ON oreg.cod_osm = osm.cod_osm
                        LEFT JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                        LEFT JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                        JOIN ssm USING (cod_ssm)
                        LEFT JOIN material_rodante_osm USING (cod_veiculo)';
        break;
    case "ssp":
        $sql = 'SELECT nome_status, COUNT(cod_status) AS quant FROM v_ssp';
        break;
    case "osp":
        $sql = 'SELECT nome_status, COUNT(cod_status) AS quant
                            FROM osp
                            JOIN status_osp sto USING (cod_ospstatus)
                            JOIN status USING (cod_status)
                            JOIN osp_servico os ON os.cod_osp = osp.cod_osp
                            LEFT JOIN osp_encerramento oe ON oe.cod_osp = osp.cod_osp
                            LEFT JOIN osp_registro oreg ON oreg.cod_osp = osp.cod_osp
                            LEFT JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                            LEFT JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                            JOIN ssp USING (cod_ssp)
                            JOIN origem_ssp USING (cod_ssp)';
        break;
}

$sql = $sql . ' WHERE nome_status is not null';

if ($where != "") {
    $sql = $sql . ' AND ' . $where;
}
$sql = $sql . " GROUP BY nome_status ORDER BY nome_status";

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$labelTable = '';
$valueTable = '';
$quantTable = '';

$soma = 0;

$json = '{"pizza":[';
foreach ($result as $dados) {
    if ($dados == end($result)) {
        $json .= '{"x": "' . $dados['nome_status'] . '", "y": ' . $dados['quant'] . ',"color": "' . returnCor($dados['nome_status']) . '"}';
    } else {
        $json .= '{"x": "' . $dados['nome_status'] . '", "y": ' . $dados['quant'] . ',"color": "' . returnCor($dados['nome_status']) . '"},';
    }

    $soma += $dados['quant'];
}
$json .= "]}";

foreach ($result as $dados) {
    $labelTable .= '<th class="legendRel tituloTabelaRel">' . $dados['nome_status'] . '</th>';
    $porc = ($dados['quant']) ? number_format(($dados['quant'] * 100) / $soma, 2, ',', '.') : '0';
    $valueTable .= '<td class="legendRel tituloTabelaRel">' . $porc . '%</td>';
    $quantTable .= '<td class="legendRel tituloTabelaRel">' . $dados['quant'] . '</td>';

    //Relativo a Tabela
    $valueGraph .= "<input value='{$dados['quant']}' name='{$dados['nome_status']}' type='hidden'/>";

}
$arrSubtitulo = [];
$valueTable .= '<td class="legendRel tituloTabelaRel">100%</td>';
$quantTable .= '<td class="legendRel tituloTabelaRel">' . $soma . '</td>';

if ($_POST['linha']) {
    $sqlLinha = "SELECT nome_linha FROM linha WHERE cod_linha = {$_POST['linha']}";
    $linha = $this->medoo->query($sqlLinha)->fetchAll(PDO::FETCH_ASSOC);
    $linha = strtoupper($linha[0]['nome_linha']);
    $arrSubtitulo[] = $linha;
}

if ($_POST['grupoSistema']) {
    $sqlGrupo = "SELECT nome_grupo FROM grupo WHERE cod_grupo = {$_POST['grupoSistema']}";
    $grupo = $this->medoo->query($sqlGrupo)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $grupo[0]['nome_grupo'];
}

if ($_POST['sistema']) {
    $sqlSistema = "SELECT nome_sistema FROM sistema WHERE cod_sistema = {$_POST['sistema']}";
    $sistema = $this->medoo->query($sqlSistema)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $sistema[0]['nome_sistema'];
}

if ($_POST['subSistema']) {
    $sqlSubsistema = "SELECT nome_subsistema FROM subsistema WHERE cod_subsistema = {$_POST['subSistema']}";
    $subsistema = $this->medoo->query($sqlSubsistema)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $subsistema[0]['nome_subsistema'];
}

if ($_POST['dataPartirDatAber']) {
    $arrSubtitulo[] = "A PARTIR DE " . $_POST['dataPartirDatAber'];
}
if ($_POST['dataRetrocederDatAber']) {
    $arrSubtitulo[] = "ATE " . $_POST['dataRetrocederDatAber'];
}

?>

<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print">
            <i class="fa fa-print fa-fw"></i> Imprimir relatório
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="geralPdfRel">

    <input value='<?php echo $_POST['titulo'] ?>' name='Titulo' type='hidden'/>
    <input value='<?php echo implode(" | ", $arrSubtitulo) ?>' name='Subtitulo' type='hidden'/>
    <input value='<?php echo $_POST['form'] ?>' name='Form' type='hidden'/>
    <?php echo $valueGraph ?>

    <div class="chartRel">
        <div id="chart" class="centerChart"></div>
    </div>

    <table class="tableRel table table-bordered table-responsive">
        <tr>
            <th></th>
            <?php echo $labelTable; ?>
            <th class="legendRel tituloTabelaRel">
                TOTAL
            </th>
        </tr>
        <tr>
            <th>(%)</th>
            <?php echo $valueTable; ?>
        </tr>
        <tr>
            <th>(Quant)</th>
            <?php echo $quantTable; ?>
        </tr>
    </table>
</div>