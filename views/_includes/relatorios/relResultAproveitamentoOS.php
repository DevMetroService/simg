<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 27/09/2017
 * Time: 11:03
 */

//var_dump($_POST);
//var_dump($_SESSION);

$where = array();
if (!empty($_POST)) {
    $where[] = "WHERE cod_status =  {$_POST['status']}";
} else {
    $where[] = "WHERE cod_status =  10";
}

//if (!empty($_POST['tipoOs'])) {
//    $where[] = "data_abertura >= '{$_POST['aPartir']} 00:00:00'";
//} else {
//    $_POST['tipoOs'] = null;
//}

//if (!empty($_POST['status'])) {
//    $where[] = "cod_status = '{$_POST['status']}'";
//} else {
//    $_POST['status'] = null;
//}

if (!empty($_POST['grupo'])) {
    $where[] = "cod_grupo = " . $_POST['grupo'];
} else {
    $_POST['grupo'] = null;
}

if (!empty($_POST['sistema'])) {
    $where[] = "cod_sistema = " . $_POST['sistema'];
} else {
    $_POST['sistema'] = null;
}

if (!empty($_POST['subsistema'])) {
    $where[] = "cod_subsistema = " . $_POST['subsistema'];
} else {
    $_POST['subsistema'] = null;
}

$sqlWhere = '';
if (!empty(count($where))) {
    $sqlWhere .= implode(' AND ', $where);
}

$sql = "SELECT
  cod_osm as osm,
  nome_grupo as grupo,
  nome_sistema as sistema
FROM osm_falha
  JOIN status_osm USING (cod_osm)
  LEFT JOIN grupo ON osm_falha.grupo_atuado = grupo.cod_grupo
  LEFT JOIN sistema ON osm_falha.sistema_atuado = sistema.cod_sistema
  LEFT JOIN subsistema ON osm_falha.subsistema_atuado = subsistema.cod_subsistema {$sqlWhere} ORDER BY cod_osm";

$sql2 = "SELECT
  o.grupo_atuado, count(*)
FROM osm_falha o 
  JOIN status_osm s
  ON o.cod_ostatus = s.cod_ostatus 
  WHERE cod_status = {$_POST['status']} GROUP BY o.grupo_atuado";

//echo $sql;
//echo $sql2;

$returnSql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$returnSql2 = $this->medoo->query($sql2)->fetchAll(PDO::FETCH_ASSOC);

//var_dump($returnSql2);
//var_dump($returnSql2[0]);

foreach ($returnSql2 as $dados) {
    echo("<input type='hidden' name='{$dados['grupo_atuado']}' value='{$dados['count']}'>");
}

?>

<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print">
            <i class="fa fa-print fa-fw"></i> Imprimir relatório
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="geralPdfRel">

    <input value='<?php echo $_POST['tipoOs'] ?>' name='Titulo' type='hidden'/>
    <input value='<?php echo $_POST['status'] ?>' name='Form' type='hidden'/>

    <div class="chartRel">
        <div id="resultApc"></div>
    </div>

    <table id="tableOs" class="text-center" style="width: 100%;page-break-after:always;">
        <thead>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">OSM</th>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">Grupo</th>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">Sistema</th>
        </thead>
        <tbody>
        <?php
        foreach ($returnSql as $dados) {
            echo '<tr>';
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['osm']}</td>";
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['grupo']}</td>";
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['sistema']}</td>";
            echo '<tr>';
        }
        ?>
        </tbody>
    </table>
</div>