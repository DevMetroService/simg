<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 27/09/2017
 * Time: 11:03
 */
?>
<form action="<?php echo HOME_URI; ?>/dashboardGeral/gerarRelatorio/ResultAproveitamentoOS" target="_blank"
      method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Gr�fico / Relat�rio de Aproveitamento de Ordens de
                            Servi�os</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Tipo OS</label>
                                                    <select class="form-control" name="tipoOs">
                                                        <option selected value="osm">OSM (ORDEM DE SERVI�O DE
                                                            MANUTEN��O CORRETIVA)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Status</label>
                                                    <select class="form-control" name="status">
                                                        <?php

                                                        $selectStatus = $this->medoo->select("status", ['cod_status', 'nome_status'], ['tipo_status' => 'O', "ORDER" => "cod_status"]);

                                                        foreach ($selectStatus as $dados => $value) {
                                                            if ($_POST['status'] == $value['cod_status'])
                                                                echo("<option selected value='{$value['cod_status']}'>{$value["nome_status"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_status']}'>{$value["nome_status"]}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupo" class="form-control">
                                                        <option value="">GRUPO</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_grupo[!]" => [22, 23, 26]]]);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($_POST['grupo'] == $value['cod_grupo'])
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">SISTEMA</option>
                                                        <?php
                                                        if (!empty($_POST['grupo'])) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if ($_POST['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Subsistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">SUBSISTEMA</option>
                                                        <?php
                                                        if (!empty($_POST['sistema'])) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-1 col-md-5">
                                    <button type="submit" class="btn btn-primary btn-large btn-block filtrarRel"><i
                                                class="fa fa-gear"></i> Gerar Relat�rio
                                    </button>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                                class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>