
<div class="page-header">
    <h2>Relat�rio dos Dados de km Rodados Por Veiculo </h2>
</div>

<form id="formCsvKmRodadosPorVeiculo" action="<?php echo HOME_URI; ?>dashboardGeral/csvKmRodadosPorVeiculo" class="form-group" method="post" target="_blank">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Filtro</label></div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-4">
                            <label>Usuario</label>
                            <select class="form-control" name="codUsuario">
                                <option value="">Todos</option>
                                <?php
                                $usuarios = $this->medoo->select("usuario", ['cod_usuario', 'usuario'], ["OR" => ["nivel"=>['5.1', '7.2']]]);

                                foreach ($usuarios as $key => $value) {
                                    echo("<option value='{$value['cod_usuario']}'>{$value["usuario"]}</option>");
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Data de Abertura (a Partir)</label>
                            <input id="dataPartir" class="form-control data add-on validaData"
                                   type="text" name="dataPartir"
                                   value="" required/>
                        </div>
                        <div class="col-md-4">
                            <label>At�</label>
                            <input id="dataRetroceder" class="form-control data validaData"
                                   type="text" name="dataRetroceder"
                                   value="" required/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Grupo</label>
                            <select class="form-control" name="grupo" required>
                                <option value="">SELECIONE</option>
                                <option value="23">MATERIAL RODANTE - TUE</option>
                                <option value="22">MATERIAL RODANTE - VLT</option>
                            </select>
                        </div>
                        <div class="col-md-4" id="selectLinha">
                            <label>Linha</label>
                            <select class="form-control" name="linha">
                                <option value="">Todos</option>
                                <?php
                                $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                foreach ($selectLinha as $dados => $value) {
                                    if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaSaf'] == $value['cod_linha'])
                                        echo("<option value='{$value['cod_linha']}' selected>{$value["nome_linha"]}</option>");
                                    else
                                        echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                }
                                ?>
                            </select>
                        </div>


                        <?php
                        $selectFunc = $this->medoo->select("veiculo", ["cod_grupo", "cod_veiculo", "nome_veiculo"], ["cod_grupo" => [22,23] ] );

                        $txtOption = array();

                        foreach ($selectFunc as $dados => $value) {
                            $txtOption[$value['cod_grupo']] .= "<option value='{$value['cod_veiculo']}'>{$value["nome_veiculo"]}</option>";
                        }
                        ?>


                        <div class="col-md-3" id="multiSelectVeiculosTUE">
                            <label>Veiculos TUE</label>
                            <select
                                    id='selectVeiculos' name="veiculos[]"
                                    class="form-control multiselect"
                                    multiple="multiple">

                                <optgroup id="tue" label="TUE" >
                                    <?php echo $txtOption['23'] ?>
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-md-3" id="multiSelectVeiculosVLT">
                            <label>Veiculos VLT</label>
                            <select
                                    id='selectVeiculos' name="veiculos[]"
                                    class="form-control multiselect"
                                    multiple="multiple">

                                <optgroup id="vlt" label="VLT" >
                                    <?php echo $txtOption['22'] ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <br />

                    <?php
                    $this->inputButtonDownDocFile(HOME_URI . "/dashboardGeral/csvKmRodadosPorVeiculo");
                    ?>

                </div>
            </div>
        </div>
    </div>
</form>