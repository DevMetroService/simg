<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 29/01/2019
 * Time: 16:00
 */


$disponibilidade = $this->medoo->select("calc_disponibilidade", "*", ["cod_linha" => $parametros[1]]);

$linha = $this->medoo->select('linha', '*', ["cod_linha" => $parametros[1]]);
$linha = $linha[0];

?>

<div class="page-header">
    <h1>Disponibilidade de Circula��o Material Rodante </h1>
    <h2 id="linha" data-linha="<?php echo $linha['cod_linha'];?>"><?php echo $linha['nome_linha'];?></h2>
</div>


<form>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <label>Registro de Disponibilidade</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Composi��es Di�rias Programadas</label>
                            <input id="qtdMinVeiculo" class="form-control" type="text" name="qtdMinVeiculo">
                        </div>
                        <div class="col-md-3">
                            <label>Tempo Total de Opera��o Programada</label>
                            <input id="horaTotalOperacao" class="form-control hora" type="text" name="horaTotalOperacao">
                        </div>
                    </div>
                    <div id="info" class="row">
                        <div class="row">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px">
                        <div class="row">
                            <div class="col-md-offset-5 col-md-2">
                                <table style="text-align: center" class="table table-responsive table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center">Disponibilidade</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td id="resultDisp"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <button type="button" class="execRel btn btn-default btn-large"><i class="fa fa-gears fa-fw"></i>Executar</button>
            <button type="button" class="save btn btn-default btn-large" data-toggle="modal" data-target="#registroDisponibilidade"><i class="fa fa-save fa-fw"></i>Salvar</button>
        </div>
    </div>
</form>

<div class="modal fade" id="registroDisponibilidade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i> </span></button>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="modal-title">Registro de Disponibilidade Di�rio</h4>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4">
                        <label>Data de Registro</label>
                        <input id="dataRegistro" type="text" class="form-control data validaData" name="dataRegistro">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="salvar" type="button" class="btn btn-primary">Salvar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
