<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 05/04/2017
 * Time: 15:07
 */

$status = $_POST['status'];
$sql = "SELECT * FROM ssm JOIN status_ssm USING (cod_mstatus) JOIN status USING (cod_status) WHERE cod_status = {$status}";
$ssm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

//var_dump($ssm);
?>

<div class="container-fluid">
    <table class="table table-bordered table-striped text-center" style="margin-top: 20px; border: solid;">
        <thead>
        <tr>
            <td style="border: solid;"><b>N� SSM</b></td>
            <td style="border: solid;"><b>Status</b></td>
            <td style="border: solid;"><b>Data Abertura</b></td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($ssm as $dados) {

            echo("<tr>");
            echo("<td>{$dados['cod_ssm']}</td>");
            echo("<td>{$dados['nome_status']}</td>");
            echo("<td>{$dados['data_abertura']}</td>");
            echo("</tr>");
        }
        ?>
        </tbody>
    </table>
</div>