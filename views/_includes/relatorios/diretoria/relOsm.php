<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 05/04/2017
 * Time: 15:12
 */

$status = $_POST['status'];
$sql = "SELECT * FROM osm_falha JOIN status_osm USING (cod_ostatus) JOIN status USING (cod_status) WHERE cod_status = {$status}";
$osm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

//var_dump($osm);
?>

<div class="container-fluid">
    <table class="table table-bordered table-striped text-center" style="margin-top: 20px; border: solid;">
        <thead>
        <tr>
            <td style="border: solid;"><b>N� OSM</b></td>
            <td style="border: solid;"><b>Status</b></td>
            <td style="border: solid;"><b>Data Abertura</b></td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($osm as $dados) {

            echo("<tr>");
            echo("<td>{$dados['cod_osm']}</td>");
            echo("<td>{$dados['nome_status']}</td>");
            echo("<td>{$dados['data_abertura']}</td>");
            echo("</tr>");
        }
        ?>
        </tbody>
    </table>
</div>