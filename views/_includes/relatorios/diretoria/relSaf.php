<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 05/04/2017
 * Time: 09:51
 */

$status = $_POST['status'];
$sql = "SELECT * FROM saf JOIN status_saf USING (cod_ssaf) JOIN status USING (cod_status) WHERE cod_status = {$status}";
$saf = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

//var_dump($saf);
?>

<div class="container-fluid">
    <table class="table table-bordered table-striped text-center" style="margin-top: 20px; border: solid;">
        <thead>
        <tr>
            <td style="border: solid;"><b>N� SAF</b></td>
            <td style="border: solid;"><b>Status</b></td>
            <td style="border: solid;"><b>Data Abertura</b></td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($saf as $dados){

                echo("<tr>");
                echo("<td>{$dados['cod_saf']}</td>");
                echo("<td>{$dados['nome_status']}</td>");
                echo("<td>{$dados['data_abertura']}</td>");
                echo("</tr>");
        }
        ?>
        </tbody>
    </table>
</div>