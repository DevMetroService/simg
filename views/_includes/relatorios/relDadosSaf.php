<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 05/09/2017
 * Time: 19:00
 */
?>

<div class="page-header">
    <h2>Relat�rio de Formul�rio Saf</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/csvPesquisaSafCompleta" class="form-group" method="post" target="_blank">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Saf</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDadosGerais">
                                    <a href="#dadosGeraisPesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false" aria-controls="collapseDadosGerais">
                                        <button class="btn btn-default"><label>Dados Gerais</label></button>
                                    </a>
                                </div>

                                <div id="dadosGeraisPesquisa" role="tabpanel" aria-labelledby="headingDadosGerais"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['codigoSaf']) || !empty($dadosRefill['solicitanteUsuarioPesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>N� SAF</label>
                                                <input class="form-control number" type="text" name="codigoSaf"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['codigoSaf']; ?>"/>

                                            </div>
                                            <div class="col-md-4">
                                                <label>Respons�vel pelo Preenchimento</label>
                                                <select name="solicitanteUsuarioPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectUsuario = $this->medoo->select('usuario', ['cod_usuario', 'usuario'], ['ORDER' => 'usuario']);
                                                    foreach ($selectUsuario as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['solicitanteUsuarioPesquisa'] == $value['cod_usuario'])
                                                            echo("<option value='{$value['cod_usuario']}' selected >{$value['usuario']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_usuario']}'>{$value['usuario']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingStatusAtual">
                                    <a href="#statusAtualPesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false" aria-controls="collapseStatusAtual">
                                        <button class="btn btn-default"><label>Data</label></button>
                                    </a>
                                </div>

                                <div id="statusAtualPesquisa" role="tabpanel" aria-labelledby="headingStatusAtual"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['statusPesquisa']) || !empty($dadosRefill['dataPartir']) || !empty($dadosRefill['dataRetroceder'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Abertura (a Partir)</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataPartirAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataPartirAbertura'])) echo($dadosRefill['dataPartirAbertura']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataAteAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataAteAbertura'])) echo($dadosRefill['dataAteAbertura']); ?>"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Status Atual</label>
                                                <select name="statusPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $statusSaf = $this->medoo->select('status', ['cod_status', 'nome_status'], ['tipo_status' => 'F']);
                                                    foreach ($statusSaf as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['statusPesquisa'] == $value['cod_status'])
                                                            echo("<option value='{$value['cod_status']}' selected>{$value['nome_status']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_status']}'>{$value['nome_status']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Data do Status (a Partir)</label>
                                                <input id="dataPartir" class="form-control data validaData"
                                                       type="text" name="dataPartir"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataPartir']; ?>"/>
                                            </div>
                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input id="dataRetroceder" class="form-control data validaData"
                                                       type="text" name="dataRetroceder"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataRetroceder']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingLocal">
                                    <a href="#localPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseLocal">
                                        <button class="btn btn-default"><label>Local</label></button>
                                    </a>
                                </div>

                                <div id="localPesquisa" role="tabpanel" aria-labelledby="headingLocal"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['linhaPesquisaSaf']) || !empty($dadosRefill['trechoPesquisaSaf']) || !empty($dadosRefill['pnPesquisaSaf'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Linha</label>
                                                <select class="form-control" name="linhaPesquisaSaf">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                    foreach ($selectLinha as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaSaf'] == $value['cod_linha'])
                                                            echo("<option value='{$value['cod_linha']}' selected>{$value["nome_linha"]}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Trecho</label>
                                                <select class="form-control" name="trechoPesquisaSaf">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaSaf'] != "") {
                                                        $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$dadosRefill['linhaPesquisaSaf']]);
                                                    } else {
                                                        $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["ORDER" => "cod_linha"]);
                                                    }

                                                    foreach ($selectTrecho as $dados => $value) {
                                                        if ($dadosRefill['trechoPesquisaSaf'] == $value['cod_trecho'])
                                                            echo("<option value='{$value['cod_trecho']}' selected>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_trecho']}'>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ponto Not�vel</label>
                                                <select class="form-control" name="pnPesquisaSaf">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if ($dadosRefill['linhaPesquisaSaf'] != "" && $dadosRefill['trechoPesquisaSaf'] == "") {

                                                    } else {
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaSaf'] != "")
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["cod_trecho" => (int)$dadosRefill['trechoPesquisaSaf']]);
                                                        else
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["ORDER" => "cod_trecho"]);

                                                        foreach ($selectPn as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['pnPesquisaSaf'] == $value['cod_ponto_notavel'])
                                                                echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSolicitante">
                                    <a href="#solicitantePesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false" aria-controls="collapseSolicitante">
                                        <button class="btn btn-default"><label>Solicitante</label></button>
                                    </a>
                                </div>

                                <div id="solicitantePesquisa" role="tabpanel" aria-labelledby="headingSolicitante"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['solicitanteOrigemPesquisaSaf']) || !empty($dadosRefill['solicitanteMatriculaPesquisa']) || !empty($dadosRefill['solicitanteCpfPesquisa'])) ? 'in' : 'out' ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Origem</label>
                                                <select class="form-control" name="solicitanteOrigemPesquisaSaf">
                                                    <?php
                                                    $selectop = array(
                                                        "1" => "MetroService",
                                                        "2" => "MetroFor",
                                                        "3" => "Terceiros"
                                                    );

                                                    echo('<option value="">Todos</option>');

                                                    foreach ($selectop as $dados => $value) {
                                                        if (!empty($dadosRefill['solicitanteOrigemPesquisaSaf']) && $dadosRefill['solicitanteOrigemPesquisaSaf'] == $dados)
                                                            echo("<option value='{$dados}' selected >{$value}</option>");
                                                        else
                                                            echo("<option value='{$dados}' >{$value}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Matricula</label>
                                                <input class="form-control number" name="solicitanteMatriculaPesquisa"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['solicitanteMatriculaPesquisa']; ?>"
                                                       type="text"/>

                                            </div>
                                            <div class="col-md-3">
                                                <label>CPF</label>
                                                <input class="form-control cpf" name="solicitanteCpfPesquisa"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['solicitanteCpfPesquisa']; ?>"
                                                       type="text"/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFalha">
                                    <a href="#falhaPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseFalha">
                                        <button class="btn btn-default"><label>Falha</label></button>
                                    </a>
                                </div>

                                <div id="falhaPesquisa" role="tabpanel" aria-labelledby="headingFalha"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['grupoSistemaPesquisa']) || !empty($dadosRefill['sistemaPesquisa']) || !empty($dadosRefill['subSistemaPesquisa']) || !empty($dadosRefill['avariaPesquisa']) || !empty($dadosRefill['nivelPesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grupo de Sistema</label>
                                                <?php
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'],["ORDER" => "nome_grupo"]);
                                                MainForm::getSelectGrupo($_POST['grupo'], $grupo, 'grupoSistemaPesquisa', true);
                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sistema</label>
                                                <select name="sistemaPesquisa" class="form-control">
                                                    <option value="">Sistemas</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa'])
                                                        $sistema = $this->medoo->select("grupo_sistema", ["[><]sistema" => "cod_sistema"], ['cod_sistema', 'nome_sistema'], ["cod_grupo" => $dadosRefill['grupoSistemaPesquisa']]);
                                                    else
                                                        $sistema = $this->medoo->select("sistema", ['cod_sistema', 'nome_sistema']);

                                                    foreach ($sistema as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisa'] == $value['cod_sistema'])
                                                            echo("<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Sistema</label>
                                                <select name="subSistemaPesquisa" class="form-control">
                                                    <option value="">Sub-Sistemas</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa'] != "" && $dadosRefill['sistemaPesquisa'] == "") {

                                                    } else {
                                                        if ($dadosRefill['sistemaPesquisa'])
                                                            $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"], ['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisa']]);
                                                        else
                                                            $subsistema = $this->medoo->select("subsistema", ['cod_subsistema', 'nome_subsistema']);

                                                        foreach ($subsistema as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisa'] == $value['cod_subsistema'])
                                                                echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <label>Avaria</label>
                                                <input type="hidden" name="avariaPesquisa"/>
                                                <input name="avariaPesquisaInput" list="avariaPesquisaDataList"
                                                       class="form-control" placeholder="Clique na Avaria"
                                                       autocomplete="off"
                                                       value="<?php echo (!empty($dadosRefill['avariaPesquisa'])) ? $dadosRefill['avariaPesquisa'] : ''; ?>"/>

                                                <datalist id="avariaPesquisaDataList">
                                                    <option value=""></option>
                                                    <?php
                                                    $avaria = $this->medoo->select("avaria", ['cod_avaria', 'nome_avaria'], ['ORDER' => 'nome_avaria']);

                                                    foreach ($avaria as $dados => $value) {
                                                        echo('<option value="' . $value['cod_avaria'] . '">' . $value['nome_avaria'] . '</option>');
                                                    }
                                                    ?>
                                                </datalist>
                                            </div>
                                            <div class="col-md-2">
                                                <label>N�vel</label>
                                                <select name="nivelPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <option value="A" <?php if ($dadosRefill['nivelPesquisa'] == "A") echo("selected") ?>>
                                                        A
                                                    </option>
                                                    <option value="B" <?php if ($dadosRefill['nivelPesquisa'] == "B") echo("selected") ?>>
                                                        B
                                                    </option>
                                                    <option value="C" <?php if ($dadosRefill['nivelPesquisa'] == "C") echo("selected") ?>>
                                                        C
                                                    </option>
                                                </select>
                                            </div>

                                        </div>
                                        <?php
                                        if ($dadosRefill['grupoSistemaPesquisa'] == '27' || $dadosRefill['grupoSistemaPesquisa'] == '28') {
                                            $bilhetagem = true;
                                        }else {
                                            $bilhetagem = false;
                                        }
                                        ?>
                                        <div class="row" id="local_sublocal" style="display: <?php echo ($bilhetagem) ? "block" : "none"?>;">
                                            <div class="col-md-4">
                                                <label>Local</label>
                                                <select name="localGrupo" class="form-control" id="local_grupo">
                                                    <?php
                                                    $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                                    echo ("<option value = '' disable selected>Selecione o Local</option> ");
                                                    foreach ($local as $dados=>$value){
                                                        if ($dadosRefill['localGrupo'] == $value['cod_local_grupo']){
                                                            echo ("<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>");
                                                        }else {
                                                            echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Local</label>
                                                <select name="subLocalGrupo" id="sublocal_grupo" class="form-control">
                                                    <?php
                                                    if($dadosRefill['subLocalGrupo']){
                                                        $selectLocal = $this->medoo->select('sublocal_grupo', ['[><]local_sublocal_grupo' => 'cod_sublocal_grupo'], '*', ['cod_local_grupo' => $dadosRefill['localGrupo']]);
                                                        foreach ( $selectLocal as $dados=>$value){
                                                            if($dadosRefill['subLocalGrupo'] == $value['cod_sublocal_grupo']){
                                                                echo ("<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>");
                                                            }else {
                                                                echo("<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>");
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default mtRod" hidden>
                                <div class="panel-heading" role="tab" id="headingMaterialRodante">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#materialRodantePesquisa" aria-expanded="false"
                                       aria-controls="collapseMaterialRodante">
                                        <button class="btn btn-default"><label>Material Rodante</label></button>
                                    </a>
                                </div>

                                <div id="materialRodantePesquisa" role="tabpanel"
                                     aria-labelledby="headingMaterialRodante"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['veiculoPesquisa']) || !empty($dadosRefill['carroAvariadoPesquisa']) || !empty($dadosRefill['carroLiderPesquisa']) || !empty($dadosRefill['odometroPartirPesquisa']) || !empty($dadosRefill['odometroAtePesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Ve�culo</label>
                                                <select name="veiculoPesquisa" class="form-control">
                                                    <option value="">Ve�culo</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa']) {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                                    } else {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo']);
                                                    }

                                                    foreach ($veiculo as $dados => $value) {
                                                        if ($dadosRefill['veiculoPesquisa'] == $value['cod_veiculo'])
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Carro Avariado</label>
                                                <select name="carroAvariadoPesquisa" class="form-control">
                                                    <option value="">Carro Avariado</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa'] || $dadosRefill['veiculoPesquisa']) {
                                                        if ($dadosRefill['grupoSistemaPesquisa']) {
                                                            if ($dadosRefill['veiculoPesquisa']) {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['AND' => ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa'], 'cod_veiculo' => $dadosRefill['veiculoPesquisa']]]);
                                                            } else {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                                            }
                                                        } else {
                                                            $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_veiculo' => $dadosRefill['veiculoPesquisa']]);
                                                        }
                                                    } else {
                                                        $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro']);
                                                    }

                                                    foreach ($carro as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['carroAvariadoPesquisa'] == $value['cod_carro'])
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Prefixo</label>
                                                <select name="prefixoPesquisa" class="form-control">
                                                    <option value="">Prefixo</option>
                                                    <?php
                                                    $prefixo = $this->medoo->select("prefixo", ['cod_prefixo', 'nome_prefixo']);

                                                    foreach ($prefixo as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['prefixoPesquisa'] == $value['cod_prefixo'])
                                                            echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Od�metro a partir</label>
                                                <input name='odometroPartirPesquisa' class='form-control number'
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroPartirPesquisa']; ?>"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Od�metro at�</label>
                                                <input name="odometroAtePesquisa" class="form-control number"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroAtePesquisa']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php
                    $this->inputButtonDownDocFile(HOME_URI . "/dashboardGeral/csvPesquisaSafCompleta");
                    ?>

                </div>
            </div>
        </div>
    </div>
</form>