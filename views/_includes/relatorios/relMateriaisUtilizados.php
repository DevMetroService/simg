<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 03/06/2017
 * Time: 12:38
 */

require_once(ABSPATH . '/functions/functionsRelatorio.php');

$isFixo = false;
$where = returnWhere($_POST);
$hasEncDate = false;
if(!empty($_POST['dataPartirDatEnc']) || !empty($_POST['dataRetrocederDatEnc']))
{
    $hasEncDate = true;
}

if ($where) {
    $where = " WHERE " . $where;
}

$joinOSMP = array(
    '21' => 'JOIN osmp_ed ON osmp.cod_osmp = osmp_ed.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_ed USING (cod_ssmp)
                 JOIN pmp_edificacao USING (cod_pmp_edificacao)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '24' => 'JOIN osmp_vp ON osmp.cod_osmp = osmp_vp.cod_osmp
                 JOIN estacao ON osmp_vp.cod_estacao_inicial = estacao.cod_estacao
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_vp USING (cod_ssmp)
                 JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '20' => 'JOIN osmp_ra ON osmp.cod_osmp = osmp_ra.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_ra USING (cod_ssmp)
                 JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '25' => 'JOIN osmp_su ON osmp.cod_osmp = osmp_su.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_su USING (cod_ssmp)
                 JOIN pmp_subestacao USING (cod_pmp_subestacao)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '27' => 'JOIN osmp_te ON osmp.cod_osmp = osmp_te.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_te USING (cod_ssmp)
                 JOIN pmp_telecom USING (cod_pmp_telecom)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '28' => 'JOIN osmp_bi ON osmp.cod_osmp = osmp_bi.cod_osmp
                 JOIN estacao USING (cod_estacao)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_bi USING (cod_ssmp)
                 JOIN pmp_bilhetagem USING (cod_pmp_bilhetagem)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) '
);

switch ($_POST['form']) {
    case "osm":
        $joinStatus ='';

        if($hasEncDate)
        {
            $joinStatus = "JOIN status_osm osms USING(cod_ostatus)";
        }

        $joinMt = '';
        if (in_array($_POST['grupoSistema'], array('22', '23', '26'))){
            $joinMt = 'LEFT JOIN material_rodante_osm USING (cod_osm) ';
        }

        $sqlMat = "SELECT nome_material, SUM(cast(utilizado as float)), nome_uni_medida, material.descricao,
                    material.cod_uni_medida as cod_medida_pad, osmm.unidade as unidade_os, material.valor_unitario, tm.nome_tipo_material
                  FROM osm_material osmm
                  JOIN material USING (cod_material)
                  LEFT JOIN tipo_material tm USING(cod_tipo_material)
                  JOIN unidade_medida um ON(osmm.unidade = um.cod_uni_medida)
                  JOIN osm_falha osm USING (cod_osm) "
            . $joinStatus .$joinMt . $where .
            " GROUP BY nome_uni_medida, nome_material, material.cod_uni_medida, osmm.unidade, material.valor_unitario, material.descricao, tm.nome_tipo_material";
        break;
    case "osp":
        if($hasEncDate)
        {
            $joinStatus = "JOIN status_osp osps USING(cod_ospstatus)";
        }

        $joinMt = '';
        if (in_array($_POST['grupoSistema'], array('22', '23', '26'))){
            $joinMt = 'LEFT JOIN material_rodante_osp USING (cod_osp) ';
        }

        $sqlMat = "SELECT nome_material, SUM(cast(utilizado as float)), nome_uni_medida, material.descricao,
                      material.cod_uni_medida as cod_medida_pad, ospm.unidade as unidade_os, material.valor_unitario, tm.nome_tipo_material
                    FROM osp_material ospm
                      JOIN material USING (cod_material)
                      LEFT JOIN tipo_material tm USING(cod_tipo_material)
                      JOIN unidade_medida um ON(ospm.unidade = um.cod_uni_medida)
                      JOIN osp USING (cod_osp) "
            . $joinStatus .$joinMt . $where .
            " GROUP BY nome_uni_medida, nome_material, material.cod_uni_medida, ospm.unidade, material.valor_unitario, material.descricao, tm.nome_tipo_material";
        break;
    case "osmp":
        if($hasEncDate)
        {
            $joinStatus = "JOIN status_osmp osmps USING(cod_status_osmp)";
        }

        $sqlMat = "SELECT nome_material, SUM(cast(utilizado as float)), nome_uni_medida, material.descricao,
                      material.cod_uni_medida as cod_medida_pad, osmpm.unidade as unidade_os, material.valor_unitario, tm.nome_tipo_material
                    FROM osmp_material osmpm
                      JOIN material USING (cod_material)
                      LEFT JOIN tipo_material tm USING(cod_tipo_material)
                      JOIN unidade_medida um ON(osmpm.unidade = um.cod_uni_medida)
                      JOIN osmp USING (cod_osmp) "
            . $joinStatus . $joinOSMP[$_POST['grupoSistema']] . $where .
            " GROUP BY nome_uni_medida, nome_material, material.cod_uni_medida, osmpm.unidade, material.valor_unitario, material.descricao, tm.nome_tipo_material";
        break;
    case "todos":
        $isFixo = true;
        if ($where) {
            $_POST['form'] = 'osm';
            $whereOSM = returnWhere($_POST);
            $whereOSM = " WHERE " . $whereOSM;

            $_POST['form'] = 'osp';
            $whereOSP = returnWhere($_POST);
            $whereOSP = " WHERE " . $whereOSP;

            $_POST['form'] = 'osmp';
            $whereOSMP = returnWhere($_POST);
            $whereOSMP = " WHERE " . $whereOSMP;
        }

        $joinMt = '';
        $check = false;
        if (in_array($_POST['grupoSistema'], array('22', '23', '26'))){
            $check =true;
        }

        if($hasEncDate)
        {
            $joinStatusOsmp = "JOIN status_osmp osmps USING(cod_status_osmp)";
            $joinStatusOsp = "JOIN status_osp ostatus USING(cod_ospstatus)";
            $joinStatusOsm = "JOIN status_osm ostatus USING(cod_ostatus)";
        }

        if($check)
            $joinMt = 'LEFT JOIN material_rodante_osm USING (cod_osm) ';

        $sqlMatOSM = " SELECT nome_material, SUM(cast(utilizado as float)), nome_uni_medida, material.descricao,
                    material.cod_uni_medida as cod_medida_pad, osmm.unidade as unidade_os, material.valor_unitario, tm.nome_tipo_material
                  FROM osm_material osmm
                  JOIN material USING (cod_material)
                  LEFT JOIN tipo_material tm USING(cod_tipo_material)
                  JOIN unidade_medida um ON(osmm.unidade = um.cod_uni_medida)
                  JOIN osm_falha o USING (cod_osm) "
            . $joinStatusOsm . $joinMt . $where .
            " GROUP BY nome_uni_medida, nome_material, material.cod_uni_medida, osmm.unidade, material.valor_unitario, material.descricao, tm.nome_tipo_material";

        if($check)
            $joinMt = 'LEFT JOIN material_rodante_osp USING (cod_osp) ';

        $sqlMatOSP = $sqlMat = " SELECT nome_material, SUM(cast(utilizado as float)), nome_uni_medida, material.descricao,
                      material.cod_uni_medida as cod_medida_pad, ospm.unidade as unidade_os, material.valor_unitario, tm.nome_tipo_material
                    FROM osp_material ospm
                      JOIN material USING (cod_material)
                      LEFT JOIN tipo_material tm USING(cod_tipo_material)
                      JOIN unidade_medida um ON(ospm.unidade = um.cod_uni_medida)
                      JOIN osp o USING (cod_osp) "
            . $joinStatusOsp . $joinMt . $where .
            " GROUP BY nome_uni_medida, nome_material, material.cod_uni_medida, ospm.unidade, material.valor_unitario, material.descricao,tm.nome_tipo_material";

        if(!$check)
            $sqlMatOSMP ="UNION SELECT nome_material, SUM(cast(utilizado as float)), nome_uni_medida, material.descricao,
                          material.cod_uni_medida as cod_medida_pad, osmpm.unidade as unidade_os, material.valor_unitario, tm.nome_tipo_material
                        FROM osmp_material osmpm
                          JOIN material USING (cod_material)
                          LEFT JOIN tipo_material tm USING(cod_tipo_material)
                          JOIN unidade_medida um ON(osmpm.unidade = um.cod_uni_medida)
                          JOIN osmp USING (cod_osmp) " . $joinStatusOsmp . $joinOSMP[$_POST['grupoSistema']] . $whereOSMP . " GROUP BY nome_uni_medida, nome_material, material.cod_uni_medida, osmpm.unidade, material.valor_unitario, material.descricao, tm.nome_tipo_material";
        else
            $sqlMatOSMP = false;

        $sqlMat = "SELECT nome_material, SUM(sum), nome_uni_medida, valor_unitario, descricao, unidade_os, cod_medida_pad, nome_tipo_material
                    FROM ({$sqlMatOSM} UNION {$sqlMatOSP}  {$sqlMatOSMP}) AS coisa GROUP BY nome_uni_medida, nome_material, valor_unitario, descricao, unidade_os, cod_medida_pad, nome_tipo_material";
        break;
}

// echo $sqlMat . " ORDER BY nome_material";
$resultMat = $this->medoo->query($sqlMat . " ORDER BY nome_material")->fetchAll(PDO::FETCH_ASSOC);

$aux = $this->medoo->select("unidade_medida", "*");
foreach($aux as $dados=>$value)
{
  $uni_medida[$value['cod_uni_medida']]=$value['nome_uni_medida'];
}

$arrSubtitulo = [];

if ($_POST['linha']) {
    $sqlLinha = "SELECT nome_linha FROM linha WHERE cod_linha = {$_POST['linha']}";
    $linha = $this->medoo->query($sqlLinha)->fetchAll(PDO::FETCH_ASSOC);
    $linha = strtoupper($linha[0]['nome_linha']);
    $arrSubtitulo[] = $linha;
}

if ($_POST['grupoSistema']) {
    $sqlGrupo = "SELECT nome_grupo FROM grupo WHERE cod_grupo = {$_POST['grupoSistema']}";
    $grupo = $this->medoo->query($sqlGrupo)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $grupo[0]['nome_grupo'];
    if(in_array($_POST['grupoSistema'], $this->sistemas_fixos) )
        $isFixo = true;
}
if ($_POST['sistema']) {
    $sqlSistema = "SELECT nome_sistema FROM sistema WHERE cod_sistema = {$_POST['sistema']}";
    $sistema = $this->medoo->query($sqlSistema)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $sistema[0]['nome_sistema'];
}
if ($_POST['subSistema']) {
    $sqlSubsistema = "SELECT nome_subsistema FROM subsistema WHERE cod_subsistema = {$_POST['subSistema']}";
    $subsistema = $this->medoo->query($sqlSubsistema)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $subsistema[0]['nome_subsistema'];
}
if ($_POST['dataPartirDatAber']) {
    $arrSubtitulo[] = "ABERTOS A PARTIR DE " . $_POST['dataPartirDatAber'];
}
if ($_POST['dataRetrocederDatAber']) {
    $arrSubtitulo[] = "ABERTOS ATE " . $_POST['dataRetrocederDatAber'];
}
if ($_POST['dataPartirDatEnc']) {
    $arrSubtitulo[] = "ENCERRADOS A PARTIR DE " . $_POST['dataPartirDatEnc'];
}
if ($_POST['dataRetrocederDatEnc']) {
    $arrSubtitulo[] = "ENCERRADOS ATE " . $_POST['dataRetrocederDatEnc'];
}
if ($_POST['form']) {
    $arrSubtitulo[] = strtoupper($_POST['form']);
}

?>
<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> Imprimir
            relatório
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="text-center" style="font-size: 15pt; font-weight: bold; padding-bottom: 10px">Materiais Utilizados</div>
<div class="text-center" style="font-size: 10pt; padding-bottom: 20px">
    <?php echo implode(" | ", $arrSubtitulo); ?>
</div>

<div class="geralPdfRel">
    <div>
        <table class="table">
            <tr>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Material</td>
                <?php if($isFixo) echo '<td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Tipo</td> '?>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Quantidade</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">UN</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Valor Unitário</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Valor Total</td>
            </tr>
            <?php
            foreach ($resultMat as $value) {
                $valorUnitario="";
                $valorTotal="";
                if(!empty($value['valor_unitario']))
                  $valorUnitario = "R$ " . number_format($value['valor_unitario'], 2, ',', '.')."/".$uni_medida[$value['cod_medida_pad']];
                if($value['unidade_os'] == $value['cod_medida_pad'] && !empty($value['valor_unitario']))
                  $valorTotal = "R$ ". number_format(($value['valor_unitario'] * $value['sum']), 2, ',', '.');

                echo("<tr>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: left;'>{$value['nome_material']} - {$value['descricao']}</td>");
                if($isFixo) echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>{$value['nome_tipo_material']}</td>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>" . $value['sum'] . "</td>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>" . $value['nome_uni_medida'] . "</td>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'> {$valorUnitario} </td>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'> {$valorTotal} </td>");
                echo("</tr>");
            }
            ?>
        </table>
    </div>
</div>
