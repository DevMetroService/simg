<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 21/03/2017
 * Time: 16:30
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Relat�rio de Cronograma Simples</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <form method="post" id="parametros" <form action='<?php echo HOME_URI ?>/dashboardPmp/pdfEdCronoSimples' target="_blank">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div id="descricaoRel">
                                            Cronograma Simples com dados resumidos.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>M�s</label>
                                                    <select class="form-control" name="mes">
                                                        <?php
                                                        if ($_POST['mes'])
                                                            $mes = $_POST['mes'];
                                                        else
                                                            $mes = date('m');

                                                        foreach (MainController::$monthComplete as $key => $value) {
                                                            if ($key == $mes)
                                                                echo("<option value='{$key}' selected>{$value}</option>");
                                                            else
                                                                echo("<option value='{$key}'>{$value}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Ano</label>
                                                    <select class="form-control" name="ano">
                                                        <?php
                                                        $op1 = date('Y') - 5;
                                                        echo ($_POST['ano'] == $op1) ? "<option selected value='$op1'>$op1</option>" : "<option value='$op1'>$op1</option>";
                                                        $op2 = date('Y') - 4;
                                                        echo ($_POST['ano'] == $op2) ? "<option selected  value='$op2'>$op2</option>" : "<option value='$op2'>$op2</option>";
                                                        $op3 = date('Y') - 3;
                                                        echo ($_POST['ano'] == $op3) ? "<option selected  value='$op3'>$op3</option>" : "<option value='$op3'>$op3</option>";
                                                        $op4 = date('Y') - 2;
                                                        echo ($_POST['ano'] == $op4) ? "<option selected  value='$op4'>$op4</option>" : "<option value='$op4'>$op4</option>";
                                                        $op5 = date('Y') - 1;
                                                        echo ($_POST['ano'] == $op5) ? "<option selected  value='$op5'>$op5</option>" : "<option value='$op5'>$op5</option>";
                                                        $op6 = date('Y');
                                                        echo (empty($_POST['ano']) || $_POST['ano'] == $op6) ? "<option selected value='$op6'>$op6</option>" : "<option value='$op6'>$op6</option>";
                                                        $op7 = date('Y') + 1;
                                                        echo ($_POST['ano'] == $op7) ? "<option selected value='$op7'>$op7</option>" : "<option value='$op7'>$op7</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if ($_POST['linha'] == $value['cod_linha'])
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupo" class="form-control">
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_grupo[!]" => [22, 23, 26, 29]]]);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($_POST['grupoSistema'] == $value['cod_grupo'])
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if ($_POST['grupoSistema']) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);
                                                        }else {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 21]);
                                                        }

                                                        foreach ($sistema as $dados => $value) {
                                                            if ($_POST['sistema'] == $value['cod_sistema'])
                                                                echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if ($_POST['sistema']) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--                            <div class="row">-->
<!--                                <div class="col-md-12">-->
<!--                                    <div class="panel panel-default">-->
<!--                                        <div class="panel-body">-->
<!--                                            <div class="row">-->
<!--                                                <div class="col-md-12">-->
<!--                                                    <label>Servi�o</label>-->
<!--                                                    <select name='servico' class='form-control'>-->
<!--                                                        <option value=''>Todos</option>-->
<!--                                                        --><?php
//                                                        $servico = $this->medoo->select("servico_pmp", "*", ["cod_grupo" => $_POST['grupoSistema']]);
//                                                        foreach ($servico as $dados => $value) {
//                                                            echo("<option value='{$value['cod_servico_pmp']}'>{$value['nome_servico_pmp']}</option>");
//                                                        }
//                                                        ?>
<!--                                                    </select>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="row">
                                <div class="col-md-offset-1 col-md-5">
                                    <button type="submit" class="btn btn-primary btn-large btn-block filtrarRel"><i
                                            class="fa fa-refresh"></i> Gerar Cronograma
                                    </button>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                            class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>