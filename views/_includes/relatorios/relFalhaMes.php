<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 30/10/2017
 * Time: 09:00
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>Falha por M�s</h3>
            </div>

            <div class="panel-body">
                <form id="formRelatorio" method="post">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Filtros</label></div>

                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-2">
                                            <label>Ano</label>
                                            <select class="form-control" name="ano">
                                                <option value="2017">2017</option>
                                                <option value="2018" selected>2018</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>N�vel</label>
                                            <?php $this->form->getSelectNivel(); ?>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Linha</label>
                                            <?php
                                            $selectLinha = $this->medoo->select("linha", "*");

                                            $this->form->getSelectLinha(null, $selectLinha);
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Trecho</label>
                                            <select name="trecho" class="form-control"></select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Ponto Notavel</label>
                                            <select name="pontoNotavel" class="form-control"></select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Grupo de Sistema</label>
                                            <?php
                                            $selectGrupo = $this->medoo->select("grupo", "*", ["ativo" => "S"]);
                                            $this->form->getSelectGrupo(null, $selectGrupo);
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Sistema</label>
                                            <select name="sistema" class="form-control"></select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>SubSistema</label>
                                            <select name="subSistema" class="form-control"></select>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="btn-group">
                                        <button id="gerarGrafico" class="btn-group-justified btn btn-default btn-lg"
                                                type="button">Gerar Relat�rio
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

                <div id="grafico"></div>

            </div>
        </div>
    </div>
</div>
