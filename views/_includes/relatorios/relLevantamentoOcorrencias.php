<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 16/03/2017
 * Time: 09:34
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Levantamento de Ordens de Servi�o</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" id="parametros"
                              action="<?php echo HOME_URI ?>/dashboardGeral/levantamentoOcorrencias/LevantamentoOcorrenciasResult" target="_blank">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div id="descricaoRel">
                                            Quantitativo de preventivas <strong><em>Abertas, Concluidas e
                                                    Canceladas</em></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-offset-2 col-md-4">
                                                    <label>M�s</label>
                                                    <select class="form-control" name="mes">
                                                        <?php
                                                        if ($_POST['mes'])
                                                            $mes = $_POST['mes'];
                                                        else
                                                            $mes = date('m');

                                                        foreach (MainController::$monthComplete as $key => $value) {
                                                            if ($key == $mes)
                                                                echo("<option value='{$key}' selected>{$value}</option>");
                                                            else
                                                                echo("<option value='{$key}'>{$value}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Ano</label>
                                                    <select class="form-control" name="ano">
                                                        <?php
                                                        $op1 = date('Y') - 5;
                                                        echo ($_POST['ano'] == $op1) ? "<option selected value='$op1'>$op1</option>" : "<option value='$op1'>$op1</option>";
                                                        $op2 = date('Y') - 4;
                                                        echo ($_POST['ano'] == $op2) ? "<option selected  value='$op2'>$op2</option>" : "<option value='$op2'>$op2</option>";
                                                        $op3 = date('Y') - 3;
                                                        echo ($_POST['ano'] == $op3) ? "<option selected  value='$op3'>$op3</option>" : "<option value='$op3'>$op3</option>";
                                                        $op4 = date('Y') - 2;
                                                        echo ($_POST['ano'] == $op4) ? "<option selected  value='$op4'>$op4</option>" : "<option value='$op4'>$op4</option>";
                                                        $op5 = date('Y') - 1;
                                                        echo ($_POST['ano'] == $op5) ? "<option selected  value='$op5'>$op5</option>" : "<option value='$op5'>$op5</option>";
                                                        $op6 = date('Y');
                                                        echo (empty($_POST['ano']) || $_POST['ano'] == $op6) ? "<option selected value='$op6'>$op6</option>" : "<option value='$op6'>$op6</option>";
                                                        $op7 = date('Y') + 1;
                                                        echo ($_POST['ano'] == $op7) ? "<option selected value='$op7'>$op7</option>" : "<option value='$op7'>$op7</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn btn-primary btn-large btn-block filtrarRel"><i
                                                class="fa fa-print"></i> Imprimir
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
