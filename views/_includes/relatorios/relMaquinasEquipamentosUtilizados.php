<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 03/06/2017
 * Time: 12:38
 */

require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);

if ($where) {
    $where = " WHERE " . $where;
}

$joinOSMP = array(
    '21' => 'JOIN osmp_ed ON osmp.cod_osmp = osmp_ed.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_ed USING (cod_ssmp)
                 JOIN pmp_edificacao USING (cod_pmp_edificacao)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '24' => 'JOIN osmp_vp ON osmp.cod_osmp = osmp_vp.cod_osmp
                 JOIN estacao ON osmp_vp.cod_estacao_inicial = estacao.cod_estacao
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_vp USING (cod_ssmp)
                 JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '20' => 'JOIN osmp_ra ON osmp.cod_osmp = osmp_ra.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_ra USING (cod_ssmp)
                 JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ',

    '25' => 'JOIN osmp_su ON osmp.cod_osmp = osmp_su.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_su USING (cod_ssmp)
                 JOIN pmp_subestacao USING (cod_pmp_subestacao)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) '
);

switch ($_POST['form']) {
    case "osm":
        $joinMt = '';
        if (in_array($_POST['grupoSistema'], array('22', '23', '26'))){
            $joinMt = 'LEFT JOIN material_rodante_osm USING (cod_osm) ';
        }

        $sqlMaq = "SELECT nome_material, SUM(qtd)
                    FROM osm_maquina
                      JOIN material USING (cod_material)
                      JOIN osm_falha osm USING(cod_osm) "
            . $joinMt . $where .
            " GROUP BY nome_material";
        break;
    case "osp":
        $sqlMaq = "SELECT nome_material, SUM(qtd)
                    FROM osp_maquina
                      JOIN material USING (cod_material)
                      JOIN osp USING(cod_osp) "
            . $where .
            "GROUP BY nome_material";
        break;
    case "osmp":
        $sqlMaq = "SELECT nome_material, SUM(qtd)
                    FROM osmp_maquina
                      JOIN material USING (cod_material)
                      JOIN osmp USING(cod_osmp) "
            . $joinOSMP[$_POST['grupoSistema']] . $where .
            " GROUP BY nome_material";
        break;

    case "todos":
        if ($where) {
            $_POST['form'] = 'osm';
            $whereOSM = returnWhere($_POST);
            $whereOSM = " WHERE " . $whereOSM;

            $_POST['form'] = 'osp';
            $whereOSP = returnWhere($_POST);
            $whereOSP = " WHERE " . $whereOSP;

            $_POST['form'] = 'osmp';
            $whereOSMP = returnWhere($_POST);
            $whereOSMP = " WHERE " . $whereOSMP;
        }

        $joinMt = '';
        if (in_array($_POST['grupoSistema'], array('22', '23', '26'))){
            $joinMt = 'LEFT JOIN material_rodante_osm USING (cod_osm) ';
        }

        $sqlMaqOSM = "SELECT nome_material, SUM(qtd)
                        FROM osm_maquina
                          JOIN material USING (cod_material)
                          JOIN osm_falha osm USING(cod_osm) " . $joinMt . $whereOSM . " GROUP BY nome_material";

        $sqlMaqOSP = "SELECT nome_material, SUM(qtd)
                        FROM osp_maquina
                          JOIN material USING (cod_material)
                          JOIN osp USING(cod_osp) " . $whereOSP . " GROUP BY nome_material";

        $sqlMaqOSMP = "SELECT nome_material, SUM(qtd)
                        FROM osmp_maquina
                          JOIN material USING (cod_material)
                          JOIN osmp USING(cod_osmp) " . $joinOSMP[$_POST['grupoSistema']] . $whereOSMP . " GROUP BY nome_material";

        $sqlMaq = "SELECT nome_material, SUM(sum)
                    FROM ({$sqlMaqOSM} UNION {$sqlMaqOSP} UNION {$sqlMaqOSMP}) AS coisa GROUP BY nome_material";
        break;
}

$resultMaq = $this->medoo->query($sqlMaq . " ORDER BY nome_material")->fetchAll(PDO::FETCH_ASSOC);

$arrSubtitulo = [];

if ($_POST['linha']) {
    $sqlLinha = "SELECT nome_linha FROM linha WHERE cod_linha = {$_POST['linha']}";
    $linha = $this->medoo->query($sqlLinha)->fetchAll(PDO::FETCH_ASSOC);
    $linha = strtoupper($linha[0]['nome_linha']);
    $arrSubtitulo[] = $linha;
}
if ($_POST['grupoSistema']) {
    $sqlGrupo = "SELECT nome_grupo FROM grupo WHERE cod_grupo = {$_POST['grupoSistema']}";
    $grupo = $this->medoo->query($sqlGrupo)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $grupo[0]['nome_grupo'];
}
if ($_POST['sistema']) {
    $sqlSistema = "SELECT nome_sistema FROM sistema WHERE cod_sistema = {$_POST['sistema']}";
    $sistema = $this->medoo->query($sqlSistema)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $sistema[0]['nome_sistema'];
}
if ($_POST['subSistema']) {
    $sqlSubsistema = "SELECT nome_subsistema FROM subsistema WHERE cod_subsistema = {$_POST['subSistema']}";
    $subsistema = $this->medoo->query($sqlSubsistema)->fetchAll(PDO::FETCH_ASSOC);
    $arrSubtitulo[] = $subsistema[0]['nome_subsistema'];
}
if ($_POST['dataPartirDatAber']) {
    $arrSubtitulo[] = "A PARTIR DE " . $_POST['dataPartirDatAber'];
}
if ($_POST['dataRetrocederDatAber']) {
    $arrSubtitulo[] = "ATE " . $_POST['dataRetrocederDatAber'];
}
if ($_POST['form']) {
    $arrSubtitulo[] = strtoupper($_POST['form']);
}

?>
<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> Imprimir
            relatório
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="text-center" style="font-size: 15pt; font-weight: bold; padding-bottom: 10px">
    Maquinas e Equipamentos Utilizados
</div>
<div class="text-center" style="font-size: 10pt; padding-bottom: 20px">
    <?php echo implode(" | ", $arrSubtitulo); ?>
</div>

<div class="geralPdfRel">
    <div>
        <table class="table">
            <tr>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Maquina</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">Quantidade</td>
            </tr>
            <?php
            foreach ($resultMaq as $value) {
                echo("<tr>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: left;'>" . $value['nome_material'] . "</td>");
                echo("<td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>" . $value['sum'] . "</td>");
                echo("</tr>");
            }
            ?>
        </table>
    </div>
</div>