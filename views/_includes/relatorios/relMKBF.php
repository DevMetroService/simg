<?php
$where = array();

$whereOdometro = " AND mes = {$_POST['mes']} AND ano = {$_POST['ano']}";

$where[] = "WHERE v_saf.cod_status = 14 AND mes = {$_POST['mes']} AND ano = {$_POST['ano']} AND osm_registro.cod_ag_causador not in(16, 21, 24, 27, 28) AND v_saf.cod_grupo = 23";

if (!empty($_POST['nivel'])) {
    $where[] = "v_saf.nivel = '" . $_POST['nivel']."'";
}

if (!empty($_POST['veiculo'])) {
    $where[] = "v_saf.cod_veiculo = {$_POST['veiculo']}";
}

$sqlWhere = implode(' AND ', $where);

if( $_POST ) {
    $sql = "
SELECT v_saf.cod_veiculo, COUNT(v_saf.cod_veiculo) AS quant_saf, v_saf.nome_veiculo, v_saf.nome_grupo,
  (
    SELECT odometro FROM odometro_tue
      WHERE odometro_tue.cod_veiculo = v_saf.cod_veiculo {$whereOdometro}
      ORDER BY data_cadastro DESC LIMIT 1
  ) AS odometro_maior, 
  (
    SELECT odometro FROM odometro_tue
      WHERE odometro_tue.cod_veiculo = v_saf.cod_veiculo {$whereOdometro}
      ORDER BY data_cadastro LIMIT 1
  ) AS odometro_menor,
  (
    SELECT odometro FROM odometro_tue
      WHERE odometro_tue.cod_veiculo = v_saf.cod_veiculo
      ORDER BY data_cadastro DESC LIMIT 1
  ) AS odometro


FROM (
  SELECT date_part('month', data_abertura) as mes, date_part('year', data_abertura) as ano, * FROM v_saf 
) AS v_saf

LEFT JOIN (
  SELECT p.cod_saf, MIN(p.cod_ssm) As MenorSsm FROM saf AS c
    LEFT JOIN ssm AS p ON c.cod_saf = p.cod_saf GROUP BY p.cod_saf
) As saf_ssm ON v_saf.cod_saf = saf_ssm.cod_saf

LEFT OUTER JOIN ssm As ssm ON saf_ssm.MenorSsm = ssm.cod_ssm

LEFT JOIN (
  SELECT p.cod_ssm, MIN(p.cod_osm) As MenorOsm FROM ssm AS c
    LEFT JOIN osm_falha AS p ON c.cod_ssm = p.cod_ssm GROUP BY p.cod_ssm
) As ssm_osm ON ssm.cod_ssm = ssm_osm.cod_ssm

LEFT OUTER JOIN osm_falha As osm ON ssm_osm.MenorOsm = osm.cod_osm

LEFT OUTER JOIN osm_registro ON osm_registro.cod_osm = osm.cod_osm

{$sqlWhere}

GROUP by v_saf.cod_veiculo,v_saf.nome_veiculo, v_saf.nome_grupo ORDER BY v_saf.cod_veiculo";

    $returnSql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>
                    <i class="fa fa-pie-chart fa-fw"></i><strong>MKBF - M�dia de Quilometragem Entre Falhas</strong>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <form action="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/MKBF" method="post">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div id="descricaoRel">Quilometragem m�dia de falhas em per�odo determinado.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>M�s</label>
                                                    <select class="form-control" name="mes" required>
                                                        <?php
                                                        if ($_POST['mes'])
                                                            $mes = $_POST['mes'];
                                                        else
                                                            $mes = date('m');

                                                        foreach (MainController::$monthComplete as $key => $value) {
                                                            if ($key == $mes)
                                                                echo("<option value='{$key}' selected>{$value}</option>");
                                                            else
                                                                echo("<option value='{$key}'>{$value}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Ano</label>
                                                    <select class="form-control" name="ano" required>
                                                        <?php
                                                        $op1 = date('Y') - 5;
                                                        echo ($_POST['ano'] == $op1) ? "<option selected>$op1</option>" : "<option>$op1</option>";
                                                        $op2 = date('Y') - 4;
                                                        echo ($_POST['ano'] == $op2) ? "<option selected>$op2</option>" : "<option>$op2</option>";
                                                        $op3 = date('Y') - 3;
                                                        echo ($_POST['ano'] == $op3) ? "<option selected>$op3</option>" : "<option>$op3</option>";
                                                        $op4 = date('Y') - 2;
                                                        echo ($_POST['ano'] == $op4) ? "<option selected>$op4</option>" : "<option>$op4</option>";
                                                        $op5 = date('Y') - 1;
                                                        echo ($_POST['ano'] == $op5) ? "<option selected>$op5</option>" : "<option>$op5</option>";
                                                        $op6 = date('Y');
                                                        echo (empty($_POST['ano']) || $_POST['ano'] == $op6) ? "<option selected>$op6</option>" : "<option>$op6</option>";
                                                        $op7 = date('Y') + 1;
                                                        echo ($_POST['ano'] == $op7) ? "<option selected>$op7</option>" : "<option>$op7</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Ve�culo</label>
                                                    <?php
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'],
                                                            [
                                                                'cod_grupo' => 23,
                                                                'ORDER' => 'nome_veiculo'
                                                            ]);
                                                        MainForm::getSelectVeiculo($_POST['veiculo'], $veiculo);
                                                    ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>N�vel</label>
                                                    <select name="nivel" class="form-control" required>
                                                        <option value="C">C</option>
                                                        <option value="B">B</option>
                                                        <option value="A">A</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-4">
                                <button type="submit" class="atualizar btn btn-primary btn-large btn-block"><i
                                            class="fa fa-refresh"></i> Atualizar Tabela
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                            class="fa fa-reply-all"></i> Limpar Filtros
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <table id="tableMkbf" class="tableRel table table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <td><b>Ve�culo</b></td>
                        <td><b>Grupo Sistema</b></td>
                        <td><b>Od�metro Atual</b></td>
                        <td><b>MKBF</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($_POST) {
                        foreach ($returnSql as $dados => $value) {
                            $mkbf = $value['odometro_maior'] - $value['odometro_menor'];
                            if( $mkbf > 0 ){
                                $mkbf = round($mkbf / $value['quant_saf'], 2);
                                $mkbf = "{$mkbf} por falha";
                            }

                            echo '<tr>';
                            echo "<td>{$value['nome_veiculo']}</td>";
                            echo "<td>{$value['nome_grupo']}</td>";
                            echo "<td>{$value['odometro']}</td>";
                            echo "<td>{$mkbf}</td>";
                            echo '</tr>';
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>