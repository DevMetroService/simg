<?php

if($_POST['nivel'] == 'A') {
    $sql = "SELECT 
                osm.cod_osm, 
                osm.data_abertura,
                EXTRACT (MONTH FROM osm.data_abertura) as MES,
                EXTRACT (DAY FROM osm.data_abertura) as DIA
                FROM v_saf 
                JOIN (
                    SELECT p.cod_saf, MIN(p.cod_ssm) As MenorSsm
                    FROM saf AS c
                    JOIN ssm AS p ON c.cod_saf = p.cod_saf
                    GROUP BY p.cod_saf
                ) As saf_ssm ON v_saf.cod_saf = saf_ssm.cod_saf
            
                JOIN ssm As ssm ON saf_ssm.MenorSsm = ssm.cod_ssm
                JOIN (
                    SELECT p.cod_ssm, MIN(p.cod_osm) As MenorOsm
                    FROM ssm AS c
                    JOIN osm_falha AS p ON c.cod_ssm = p.cod_ssm
                    GROUP BY p.cod_ssm
                ) As ssm_osm ON ssm.cod_ssm = ssm_osm.cod_ssm
            
                JOIN osm_falha As osm ON ssm_osm.MenorOsm = osm.cod_osm 
                JOIN osm_registro ON osm_registro.cod_osm = osm.cod_osm
                JOIN agente_causador ON osm_registro.cod_ag_causador = agente_causador.cod_ag_causador
                JOIN causa ON osm_registro.cod_causa = causa.cod_causa 
                WHERE osm.nivel = 'A' AND osm.grupo_atuado IN(22) AND osm_registro.cod_ag_causador NOT IN (16, 21, 24, 27, 28) 
                GROUP BY osm.cod_osm ORDER BY cod_osm DESC LIMIT 2";
    $corretivas = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    if(!empty($corretivas)) {
        //Recebe a Soma de TODOS os Od�metros da data de ocorr�ncia Final
        $sql = "SELECT sum(otue.tracao_ma)
                    FROM
                    (SELECT DISTINCT ON(nome_veiculo) nome_veiculo, tracao_ma FROM odometro_vlt otue
                    JOIN veiculo v USING(cod_veiculo)
                    WHERE otue.data_cadastro <= '{$corretivas[0]['data_abertura']}' AND v.frota = '{$_POST['frota']}'  AND tracao_ma IS NOT NULL
                    ORDER BY nome_veiculo ASC, tracao_ma DESC ) as otue";

        $odometroF = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Recebe a Soma de TODOS os Od�metros da data de ocorr�ncia Final
        $sql = "SELECT sum(otue.tracao_ma)
                    FROM
                    (SELECT DISTINCT ON(nome_veiculo) nome_veiculo, tracao_ma FROM odometro_vlt otue
                    JOIN veiculo v USING(cod_veiculo)
                    WHERE otue.data_cadastro <= '{$corretivas[1]['data_abertura']}' AND v.frota = '{$_POST['frota']}'  AND tracao_ma IS NOT NULL
                    ORDER BY nome_veiculo ASC, tracao_ma DESC ) as otue";
        $odometroI = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Calcula o Delta antigo
        $deltaOdometroPassado = $odometroF[0]['sum'] - $odometroI[0]['sum'];

        $time = date('d-m-Y H:i:s', time());;

        $sql = "SELECT sum(otue.tracao_ma)
                    FROM
                    (SELECT DISTINCT ON(nome_veiculo) nome_veiculo, tracao_ma FROM odometro_vlt otue
                    JOIN veiculo v USING(cod_veiculo)
                    WHERE otue.data_cadastro <= '{$time}' AND v.frota = '{$_POST['frota']}'  AND tracao_ma IS NOT NULL
                    ORDER BY nome_veiculo ASC, tracao_ma DESC ) as otue";
        $odometroFinalAtual = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Calcula o novo Delta
        $deltaOdometroAtual = $odometroFinalAtual[0]['sum'] - $odometroF[0]['sum'];
    }else{
        $deltaOdometroPassado = "N/E";

        $time = date('d-m-Y H:i:s', time());;

        $sql = "SELECT sum(otue.tracao_ma)
                    FROM
                    (SELECT DISTINCT ON(nome_veiculo) nome_veiculo, tracao_ma FROM odometro_vlt otue
                    JOIN veiculo v USING(cod_veiculo)
                    WHERE otue.data_cadastro <= '{$time}' AND v.frota = '{$_POST['frota']}'  AND tracao_ma IS NOT NULL
                    ORDER BY nome_veiculo ASC, tracao_ma DESC ) as otue";
        $odometroFinalAtual = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Calcula o novo Delta
        $deltaOdometroAtual = $odometroFinalAtual[0]['sum'];
    }

}
else{
    if($_POST) {

        $mesI = $_POST['mes'];
        $yearI = $_POST['ano'];

        if ($mesI == '12') {
            $mesF = 1;
            $yearF = $yearI + 1;
        } else {
            $mesF = $mesI + 1;
            $yearF = $yearI;
        }


        $sql = "SELECT 
                    count(*)
                    FROM v_saf 
                    JOIN (
                        SELECT p.cod_saf, MIN(p.cod_ssm) As MenorSsm
                        FROM saf AS c
                        JOIN ssm AS p ON c.cod_saf = p.cod_saf
                        GROUP BY p.cod_saf
                    ) As saf_ssm ON v_saf.cod_saf = saf_ssm.cod_saf
                    
                    JOIN ssm As ssm ON saf_ssm.MenorSsm = ssm.cod_ssm
                    JOIN (
                        SELECT p.cod_ssm, MIN(p.cod_osm) As MenorOsm
                        FROM ssm AS c
                        JOIN osm_falha AS p ON c.cod_ssm = p.cod_ssm
                        GROUP BY p.cod_ssm
                    ) As ssm_osm ON ssm.cod_ssm = ssm_osm.cod_ssm
                    
                    JOIN osm_falha As osm ON ssm_osm.MenorOsm = osm.cod_osm 
                    JOIN osm_registro ON osm_registro.cod_osm = osm.cod_osm
                    JOIN agente_causador ON osm_registro.cod_ag_causador = agente_causador.cod_ag_causador
                    JOIN causa ON osm_registro.cod_causa = causa.cod_causa 
                    WHERE osm.nivel = '{$_POST['nivel']}' AND v_saf.data_abertura BETWEEN '{$yearI}-{$mesI}-01 00:00:00' AND '{$yearF}-{$mesF}-01 00:00:00' AND osm.grupo_atuado IN (22) AND osm_registro.cod_ag_causador NOT IN (16, 21, 24, 27, 28) ";
        $corretivas = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Recebe a Soma de TODOS os Od�metros da data de ocorr�ncia Final
        $sql = "SELECT sum(otue.tracao_ma)
                    FROM
                    (SELECT DISTINCT ON(nome_veiculo) nome_veiculo, tracao_ma FROM odometro_vlt
                    JOIN veiculo USING(cod_veiculo)
                    WHERE data_cadastro <= '{$yearI}-{$mesI}-01' AND frota = '{$_POST['frota']}' AND tracao_ma IS NOT NULL
                    ORDER BY nome_veiculo ASC, tracao_ma DESC ) as otue";
        $odometroI = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);


        //Recebe a Soma de TODOS os Od�metros da data de ocorr�ncia Final
        $sql = "SELECT sum(otue.tracao_ma)
                    FROM
                    (SELECT DISTINCT ON(nome_veiculo) nome_veiculo, tracao_ma FROM odometro_vlt
                    JOIN veiculo USING(cod_veiculo)
                    WHERE data_cadastro <= '{$yearF}-{$mesF}-01 00:00:00' AND frota = '{$_POST['frota']}' AND tracao_ma IS NOT NULL
                    ORDER BY nome_veiculo ASC, tracao_ma DESC ) as otue";
        $odometroF = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Calcula o Delta antigo
        $deltaOdometroAtual = $odometroF[0]['sum'] - $odometroI[0]['sum'];
        $MTBF = $deltaOdometroAtual / $corretivas[0]['count'];
    }
}
?>
<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>MTBF Frota</h1>
    <small>Relat�rio MTBF de frotas.</small>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/MTBFMR" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>Informa��es</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>M�s</label>
                            <select class="form-control" name="mes" required>
                                <?php
                                foreach(MainController::$monthComplete as $key=>$value){
                                    if($_POST['mes'] == $key){
                                        echo "<option value='{$key}' selected>{$value}</option>";
                                    }else{
                                        echo "<option value='{$key}'>{$value}</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Ano</label>
                            <select class="form-control" name="ano" required>
                                <?php
                                for($i= date('Y') ; $i >= 2016; $i--){

                                    echo "<option value='{$i}'";
                                    echo $_POST['ano'] == $i ? 'selected': '';
                                    echo ">{$i}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Frota</label>
                            <select id="frota" class="form-control" name="frota" required>

                                <option value="">Selecione a frota</option>
                                <?php
                                $frota = $this->medoo->query("SELECT distinct(frota) FROM veiculo WHERE cod_grupo = 22")->fetchAll();
                                foreach($frota as $key=>$value){
                                    echo "<option value='{$value['frota']}'";
                                    echo $_POST['frota'] == $value['frota'] ? ' selected ': ' ';
                                    echo ">{$value['frota']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>N�vel</label>
                            <select id="nivel" class="form-control" name="nivel" required>
                                <option value="" selected disabled>Selecione o nivel</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-2">
                            <button type="submit" class="atualizar btn btn-default btn-large btn-block"><i
                                        class="fa fa-refresh"></i> Executar
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</form>

<?php
if($_POST['nivel'] == 'A'){
    echo ("<div class='row'>
    <div class='col-md-offset-4 col-md-4'>
        <table class='table table-bordered table-striped table-responsive'>
            <thead>
            <tr>
                <th>MTBF-FROTA PASSADO</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                ". number_format($deltaOdometroPassado, '0', ',', '.') ." Horas rodadas por falha da frota {$_POST['frota']} em n�vel {$_POST['nivel']}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class='row'>
    <div class='col-md-offset-4 col-md-4'>
        <table class='table table-bordered table-striped table-responsive'>
            <thead>
            <tr>
                <th>MTBF-FROTA ATUAL</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    ". number_format($deltaOdometroAtual, '0',',','.')." Horas rodados por falha da frota {$_POST['frota']} em n�vel {$_POST['nivel']}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>");
    unset($_POST);
}else{
    if($_POST) {
        echo("
    <div class='row'>
        <div class='col-md-offset-4 col-md-4'>
            <table class='table table-bordered table-striped table-responsive'>
                <thead>
                <tr>
                    <th>FALHAS NO M�S</th>
                    <th>TOTAL HORAS RODADAS</th>
                    <th>MTBF-FROTA M�S DE " . MainController::$monthComplete[$_POST['mes']] . "</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{$corretivas[0]['count']}</td>
                    <td>". number_format($deltaOdometroAtual, '0',',','.') ."</td>
                    <td>
                        ". number_format($MTBF, '0',',','.') ." Horas rodados por falha da frota {$_POST['frota']} em n�vel {$_POST['nivel']}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    ");
    }
}
?>
