<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>DS - Disponibilidade do Sistema</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div id="descricaoRel">
                                            TEXTO EXPLICATIVO
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>M�s</label>
                                                    <select class="form-control" name="mes">
                                                        <?php
                                                        if(!empty($_POST['mes']))
                                                            $mes = $_POST['mes'];
                                                        else
                                                            $mes = date('m');

                                                        foreach(MainController::$monthComplete as $key=>$value){
                                                            if($key == $mes)
                                                                echo ("<option value='{$key}' selected>{$value}</option>");
                                                            else
                                                                echo ("<option value='{$key}'>{$value}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Ano</label>
                                                    <select class="form-control" name="ano">
                                                        <?php
                                                        $op1 = date('Y') - 5;
                                                        echo ($_POST['ano'] == $op1)? "<option selected>$op1</option>" : "<option>$op1</option>";
                                                        $op2 = date('Y') - 4;
                                                        echo ($_POST['ano'] == $op2)? "<option selected>$op2</option>" : "<option>$op2</option>";
                                                        $op3 = date('Y') - 3;
                                                        echo ($_POST['ano'] == $op3)? "<option selected>$op3</option>" : "<option>$op3</option>";
                                                        $op4 = date('Y') - 2;
                                                        echo ($_POST['ano'] == $op4)? "<option selected>$op4</option>" : "<option>$op4</option>";
                                                        $op5 = date('Y') - 1;
                                                        echo ($_POST['ano'] == $op5)? "<option selected>$op5</option>" : "<option>$op5</option>";
                                                        $op6 = date('Y');
                                                        echo (empty($_POST['ano']) || $_POST['ano'] == $op6)? "<option selected>$op6</option>" : "<option>$op6</option>";
                                                        $op7 = date('Y') +1;
                                                        echo ($_POST['ano'] == $op7)? "<option selected>$op7</option>" : "<option>$op7</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if($_POST['linha'] == $value['cod_linha'])
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupoSistema" class="form-control">
                                                        <option value="">Grupos de Sistemas</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if($_POST['grupoSistema'] == $value['cod_grupo'])
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if(!empty($_POST['grupoSistema'])){
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if($_POST['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if(!empty($_POST['sistema'])){
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-1 col-md-5">
                                    <button type="submit" class="btn btn-primary btn-large btn-block"><i class="fa fa-refresh"></i> Atualizar Gr�fico</button>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i class="fa fa-reply-all"></i> Limpar Filtros</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-5">
                        <div id="container"></div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="col-md-12">
                    <table class="tableRel table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>Grupo</th>
                                <th>Quinzena de Execu��o</th>
                                <th>Periodicidade</th>
                                <th>Status</th>
                                <th>A��es</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>