<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>TML - Tempo M�dio de Libera��o</strong></h3>
            </div>

            <div class="panel-body">
                <form id="formRelTML" method="post" action="">
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label>Filtro</label>
                            </div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Linha</label>
                                        <?php
                                        $sql = $this->medoo->select("linha", "*");
                                        $this->form->getSelectLinha(null, $sql, "linha");
                                        ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Trecho</label>
                                        <select name="trecho" class="form-control">
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Nivel</label>
                                        <?php
                                        $this->form->getSelectNivel();
                                        ?>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Grupo Sistema</label>
                                        <?php
                                        $sql = $this->medoo->select("grupo", "*",["ORDER" => "nome_grupo"]);
                                        $this->form->getSelectGrupo(null, $sql, "grupo");
                                        ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Sistema</label>
                                        <select name="sistema" class="form-control">
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Sub-Sistema</label>
                                        <select name="subsistema" class="form-control">
                                        </select>
                                    </div>

                                </div>

                                <div class="row" id="divMaterialRodante" style="display:none">
                                    <div class="col-md-2">
                                        <label>Ve�culo</label>
                                        <select name="veiculo" class="form-control">
                                        </select>
                                    </div>
                                </div>

                                <div class="row" id="sublocal" style="display:none">
                                    <div class="col-md-4">
                                        <label>Local</label>
                                        <select name="local" class="form-control">
                                            <?php
                                            $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                            echo ("<option value='' disabled selected>Selecione o Local</option>");
                                            foreach($local as $dados=>$value){
                                                echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Sub-Local</label>
                                        <select name="subLocal" class="form-control">
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Data Abertura (a Partir)</label>
                                        <div class="">
                                            <input id="dataPartir" class="form-control data add-on validaData"
                                                   type="text" name="dataPartir"
                                                   value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataPartir']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>At�</label>
                                        <div class="">
                                            <input id="dataRetroceder" class="form-control data validaData"
                                                   type="text" name="dataRetroceder"
                                                   value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataRetroceder']; ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- button area -->
                    <div class="row">
                        <div class="col-md-offset-2 col-md-4">
                            <button id="gerarGrafico" type="button" class="atualizar btn btn-primary btn-large btn-block"><i
                                    class="fa fa-refresh"></i> Atualizar Tabela
                            </button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                    class="fa fa-reply-all"></i> Limpar Filtros
                            </button>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table id="tableTML" class="tableRel table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <td colspan="4"><b>Total de Horas</b></td>
                                    <td colspan="4"><b>TML</b></td>
                                </tr>
                                <tr>
                                    <td colspan="4" id="total"></td>
                                    <td colspan="4" id="tml"></td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>