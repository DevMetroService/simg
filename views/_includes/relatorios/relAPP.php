<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = [];
$whereMr = [];
$whereMr[] = "tipo_orissp = 1";

if ($_POST['mes']) {
    $where[] = "date_part('month', osmp.data_abertura) = " . $_POST['mes'];
//    $whereMr[] = "DATE_PART('month', data_programada)= {$_POST['mes']}";
    $mes = $_POST['mes'];
} else {
    $mes = date('m');
    $where[] = "date_part('month', osmp.data_abertura) = " . $mes;
    $whereMr[] = "DATE_PART('month', data_programada)= {$mes}";
}

if ($_POST['ano']) {
    $where[] = "date_part('year', osmp.data_abertura) = " . $_POST['ano'];
//    $whereMr[] = "DATE_PART('year', data_programada)= {$_POST['ano']}";
    $ano = $_POST['ano'];
} else {
    $ano = date('Y');
    $where[] = "date_part('year', osmp.data_abertura) = " . $ano;
//    $whereMr[] = "DATE_PART('year', data_programada)= {$ano}";
}

if ($_POST['linha']) {
    $where[] = "l.cod_linha = " . $_POST['linha'];
//    $whereMr[] = "cod_linha = " . $_POST['linha'];
}

if ($_POST['sistema']) {
    $where[] = "cod_sistema = " . $_POST['sistema'];
//    $whereMr[] = "cod_sistema = " . $_POST['sistema'];
}

if ($_POST['subSistema']) {
    $where[] = "cod_subsistema = " . $_POST['subSistema'];
//    $whereMr[] = "cod_subsistema = " . $_POST['subSistema'];
}

$sqlWhere = '';
$sqlWhereMr = '';
if (count($where)) {
    $sqlWhere .= implode(' AND ', $where);
//    $sqlWhereMr .= implode(' AND ', $whereMr);
}

if (!empty($_POST['grupoSistema'])) {
    if($_POST['grupoSistema'] == 22 || $_POST['grupoSistema'] == 23){
        $sql = returnSqlApp($_POST['grupoSistema']) . " WHERE " . $sqlWhereMr;
    }else{
        $sql = returnSqlApp($_POST['grupoSistema']) . " WHERE " . $sqlWhere;
    }
} else {
    $sqlGrupo[] = "(". returnSqlApp(20) . " WHERE " . $sqlWhere .")";
    $sqlGrupo[] = "(".returnSqlApp(21) . " WHERE " . $sqlWhere.")";
//    $sqlGrupo[] = "(".returnSqlApp(22) . " WHERE " . $sqlWhereMr.")";
//    $sqlGrupo[] = "(".returnSqlApp(23) . " WHERE " . $sqlWhereMr.")";
    $sqlGrupo[] = "(".returnSqlApp(24) . " WHERE " . $sqlWhere.")";
    $sqlGrupo[] = "(".returnSqlApp(25) . " WHERE " . $sqlWhere.")";
    $sqlGrupo[] = "(".returnSqlApp(28) . " WHERE " . $sqlWhere.")";
    $sqlGrupo[] = "(".returnSqlApp(27) . " WHERE " . $sqlWhere.")";

    $sql = implode(" UNION ALL ", $sqlGrupo);
}

if($_POST){
    $returnSql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>APP - Aproveitamento de Programa��es
                        Preventivas de Sistemas Fixos</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel">
                                        <div id="descricaoRel">
                                            Compara��o da quantidade de registros do Cronograma que est�o <strong><em>Encerrados</em></strong>
                                            em rela��o ao TOTAL dos <strong><em>Previstos</em></strong>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>M�s</label>
                                                    <select class="form-control" name="mes">
                                                        <?php
                                                        if ($_POST['mes'])
                                                            $mes = $_POST['mes'];
                                                        else
                                                            $mes = date('m');

                                                        foreach (MainController::$monthComplete as $key => $value) {
                                                            if ($key == $mes)
                                                                echo("<option value='{$key}' selected>{$value}</option>");
                                                            else
                                                                echo("<option value='{$key}'>{$value}</option>");
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Ano</label>
                                                    <select class="form-control" name="ano">
                                                        <?php
                                                        $op1 = date('Y') - 5;
                                                        echo ($_POST['ano'] == $op1) ? "<option selected>$op1</option>" : "<option>$op1</option>";
                                                        $op2 = date('Y') - 4;
                                                        echo ($_POST['ano'] == $op2) ? "<option selected>$op2</option>" : "<option>$op2</option>";
                                                        $op3 = date('Y') - 3;
                                                        echo ($_POST['ano'] == $op3) ? "<option selected>$op3</option>" : "<option>$op3</option>";
                                                        $op4 = date('Y') - 2;
                                                        echo ($_POST['ano'] == $op4) ? "<option selected>$op4</option>" : "<option>$op4</option>";
                                                        $op5 = date('Y') - 1;
                                                        echo ($_POST['ano'] == $op5) ? "<option selected>$op5</option>" : "<option>$op5</option>";
                                                        $op6 = date('Y');
                                                        echo (empty($_POST['ano']) || $_POST['ano'] == $op6) ? "<option selected>$op6</option>" : "<option>$op6</option>";
                                                        $op7 = date('Y') + 1;
                                                        echo ($_POST['ano'] == $op7) ? "<option selected>$op7</option>" : "<option>$op7</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if ($_POST['linha'] == $value['cod_linha']) {
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupoSistema" class="form-control">
                                                        <option value="">Grupos de Sistemas</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_grupo[!]" => [26]]]);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($_POST['grupoSistema'] == $value['cod_grupo']) {
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if ($_POST['grupoSistema']) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if ($_POST['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if ($_POST['sistema']) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-1 col-md-5">
                                    <button type="submit" class="btn btn-primary btn-large btn-block"><i
                                                class="fa fa-refresh"></i> Atualizar Gr�fico
                                    </button>
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                                class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-5">
                        <div id="container"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-7 col-md-5">
                        <table class="table table-bordered text-center">
                            <tr>
                                <td>Previstos</td>
                                <td id="previstos"></td>
                            </tr>
                            <tr>
                                <td>Encerrados</td>
                                <td id="encerrados"></td>
                            </tr>
                            <tr>
                                <td>APP% > 95%</td>
                                <td id="porcentagem"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <?php
            if(is_array($returnSql) )
                $count = count($returnSql);

            $countEncerrada = 0;

            if($returnSql) {
                foreach ($returnSql as $dados => $value) {


                    $whereData = mesAno($mes, $ano);


                    $sqlStatus = "SELECT cod_status FROM status_osmp
                                JOIN status USING (cod_status)
                                WHERE 
                                    data_status < '{$whereData['dataAte']}' AND
                                    cod_osmp = {$value['cod_osmp']}
                                ORDER BY data_status DESC LIMIT 1";
                    $returnSqlStatus = $this->medoo->query($sqlStatus)->fetchAll(PDO::FETCH_ASSOC);
                    $returnSqlStatus = $returnSqlStatus[0];

                    if (!empty($returnSqlStatus['cod_status'])) {
                        $status = $returnSqlStatus['cod_status'];
                    } else {
                        $status = $value['cod_status'];
                    }

                    if ($status == 11)
                        $countEncerrada++;
                }
            }
            ?>
        </div>
    </div>
</div>


<input value="<?php echo $count ?>" name="previstas" type="hidden"/>
<input value="<?php echo $countEncerrada ?>" name="concluidas" type="hidden"/>

<?php
//$this->dashboard->modalExibirCronograma();

function returnSqlApp($grupo)
{
    switch ($grupo) {
        case 20:
            //Rede A�rea
            return "SELECT 
                        osmp.cod_osmp
                    FROM OSMP 
                    JOIN osmp_ra USING(cod_osmp)
                    JOIN local l USING(cod_local)
                    JOIN ssmp USING(Cod_ssmp)
                    JOIN cronograma_pmp USING(cod_cronograma_pmp)
                    
                    JOIN pmp USING(cod_pmp)
                    JOIN servico_pmp_periodicidade USING(Cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema )
                    JOIN sub_sistema USING(Cod_sub_sistema)
                    
                    JOIN status_osmp USING(cod_status_osmp)
                    JOIN status USING(cod_status)";
        case 21:
            //Edifica��es
            return "SELECT 
                        osmp.cod_osmp
                    FROM OSMP 
                    JOIN osmp_ed USING(cod_osmp)
                    JOIN local l USING(cod_local)
                    JOIN ssmp USING(Cod_ssmp)
                    JOIN cronograma_pmp USING(cod_cronograma_pmp)
                    
                    JOIN pmp USING(cod_pmp)
                    JOIN servico_pmp_periodicidade USING(Cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema )
                    JOIN sub_sistema USING(Cod_sub_sistema)
                    
                    JOIN status_osmp USING(cod_status_osmp)
                    JOIN status USING(cod_status)";
            break;
//        case 22:
//            //MR VLT
//            return "SELECT
//                cod_ssp AS cod_cronograma_grupo,
//                nome_linha AS nome_local,
//                cod_linha,
//                DATE_PART('month', data_programada) as mes,
//                DATE_PART('year', data_programada) as ano,
//                CASE
//                    WHEN DATE_PART('day', data_programada) > 15
//                        THEN DATE_PART('month', data_programada) * 2
//                        ELSE DATE_PART('month', data_programada)*2-1
//                END AS quinzena,
//                null AS homem_hora,
//                null AS nome_periodicidade,
//                nome_servico as nome_servico_pmp,
//                cod_sistema,
//                cod_subsistema,
//                nome_status,
//                'Material Rodante VLT' as nome_grupo, 'vlt' AS sigla_grupo
//            FROM v_ssp ";
//        case 23:
//            //MR TUE
//            return "SELECT
//                cod_ssp AS cod_cronograma_grupo,
//                nome_linha AS nome_local,
//                cod_linha,
//                DATE_PART('month', data_programada) as mes,
//                DATE_PART('year', data_programada) as ano,
//                CASE
//                    WHEN DATE_PART('day', data_programada) > 15
//                        THEN DATE_PART('month', data_programada) * 2
//                        ELSE DATE_PART('month', data_programada)*2-1
//                END AS quinzena,
//                null AS homem_hora,
//                null AS nome_periodicidade,
//                nome_servico as nome_servico_pmp,
//                cod_sistema,
//                cod_subsistema,
//                nome_status,
//                'Material Rodante TUE' as nome_grupo, 'tue' AS sigla_grupo
//            FROM v_ssp ";
        case 24:
            //Via Permanente
            return "SELECT 
                        osmp.cod_osmp
                    FROM OSMP 
                    JOIN osmp_vp ovp USING(cod_osmp)
                    JOIN estacao l on(l.cod_estacao = ovp.cod_estacao_inicial)
                    JOIN ssmp USING(Cod_ssmp)
                    JOIN cronograma_pmp USING(cod_cronograma_pmp)
                    
                    JOIN pmp USING(cod_pmp)
                    JOIN servico_pmp_periodicidade USING(Cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema )
                    JOIN sub_sistema USING(Cod_sub_sistema)
                    
                    JOIN status_osmp USING(cod_status_osmp)
                    JOIN status USING(cod_status)";
        case 25:
            //Subesta��o
            return "SELECT 
                        osmp.cod_osmp
                    FROM OSMP 
                    JOIN osmp_su USING(cod_osmp)
                    JOIN local l USING(cod_local)
                    JOIN ssmp USING(Cod_ssmp)
                    JOIN cronograma_pmp USING(cod_cronograma_pmp)
                    
                    JOIN pmp USING(cod_pmp)
                    JOIN servico_pmp_periodicidade USING(Cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema )
                    JOIN sub_sistema USING(Cod_sub_sistema)
                    
                    JOIN status_osmp USING(cod_status_osmp)
                    JOIN status USING(cod_status)";
        case 28:
            //Bilhetagem
            return "SELECT 
                        osmp.cod_osmp
                    FROM OSMP 
                    JOIN osmp_bi USING(cod_osmp)
                    JOIN estacao l USING(cod_estacao)
                    JOIN ssmp USING(Cod_ssmp)
                    JOIN cronograma_pmp USING(cod_cronograma_pmp)
                    
                    JOIN pmp USING(cod_pmp)
                    JOIN servico_pmp_periodicidade USING(Cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema )
                    JOIN sub_sistema USING(Cod_sub_sistema)
                    
                    JOIN status_osmp USING(cod_status_osmp)
                    JOIN status USING(cod_status)";
        case 27:
            //Telecom
            return "SELECT 
                        osmp.cod_osmp
                    FROM OSMP 
                    JOIN osmp_te USING(cod_osmp)
                    JOIN local l USING(cod_local)
                    JOIN ssmp USING(Cod_ssmp)
                    JOIN cronograma_pmp USING(cod_cronograma_pmp)
                    
                    JOIN pmp USING(cod_pmp)
                    JOIN servico_pmp_periodicidade USING(Cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema )
                    JOIN sub_sistema USING(Cod_sub_sistema)
                    
                    JOIN status_osmp USING(cod_status_osmp)
                    JOIN status USING(cod_status)";
    }
}
?>