<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 05/09/2017
 * Time: 19:40
 */
?>

<div class="page-header">
    <h2>Relat�rio de Formul�rio de Ordem de Servi�o de Manuten��o</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/csvPesquisaOsmCompleta" class="form-group" target="_blank" method="post" id="pesquisa">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Osm</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDadosGerais">
                                    <a href="#dadosGeraisPesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false" aria-controls="collapseDadosGerais">
                                        <button class="btn btn-default" type="button"><label>Dados Gerais</label>
                                        </button>
                                    </a>
                                </div>

                                <div id="dadosGeraisPesquisa" role="tabpanel" aria-labelledby="headingDadosGerais"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['numeroPesquisaOsm']) || !empty($dadosRefill['codigoSsm']) || !empty($dadosRefill['responsavelPreenchimentoPesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>N� OSM</label>
                                                <input class="form-control" name="numeroPesquisaOsm" type="text"
                                                       value="<?php if (!empty($dadosRefill['numeroPesquisaOsm'])) echo($dadosRefill['numeroPesquisaOsm']); ?>">
                                            </div>

                                            <div class="col-md-2">
                                                <label>N� SSM</label>
                                                <input class="form-control" name="codigoSsm" type="text"
                                                       value="<?php if (!empty($dadosRefill['codigoSsm'])) echo($dadosRefill['codigoSsm']); ?>">
                                            </div>

                                            <div class="col-md-4">
                                                <label>Respons�vel pelo Preenchimento</label>
                                                <select class="form-control" name="responsavelPreenchimentoPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectUsuario = $this->medoo->select('usuario', ['cod_usuario', 'usuario'], ["ORDER" => 'usuario']);
                                                    foreach ($selectUsuario as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['responsavelPreenchimentoPesquisaOsm'] == $value['cod_usuario'])
                                                            echo("<option value='" . $value['cod_usuario'] . "' selected >" . $value['usuario'] . "</option>");
                                                        else
                                                            echo("<option value='" . $value['cod_usuario'] . "'>" . $value['usuario'] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDatas">
                                    <a href="#datasPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseDatas">
                                        <button class="btn btn-default" type="button"><label>Datas e Status</label>
                                        </button>
                                    </a>
                                </div>

                                <div id="datasPesquisa" role="tabpanel" aria-labelledby="headingDatas"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['dataPartirOsmAbertura']) || !empty($dadosRefill['dataAteOsmAbertura']) || !empty($dadosRefill['dataPartirOsmEncerramento']) || !empty($dadosRefill['dataAteOsmEncerramento']) || !empty($dadosRefill['dataPartirOsm']) || !empty($dadosRefill['dataAteOsm']) || !empty($dadosRefill['situacaoPesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Abertura (a Partir)</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataPartirOsmAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataPartirOsmAbertura'])) echo($dadosRefill['dataPartirOsmAbertura']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataAteOsmAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataAteOsmAbertura'])) echo($dadosRefill['dataAteOsmAbertura']); ?>"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Encerramento (a Partir)</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataPartirOsmEncerramento"
                                                       value="<?php if (!empty($dadosRefill['dataPartirOsmEncerramento'])) echo($dadosRefill['dataPartirOsmEncerramento']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input name="dataAteOsmEncerramento" type="text"
                                                       class="form-control data validaData"
                                                       value="<?php if (!empty($dadosRefill['dataAteOsmEncerramento'])) echo($dadosRefill['dataAteOsmEncerramento']); ?>"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Status</label>
                                                <select class="form-control" name="situacaoPesquisaOsm">
                                                    <option value=''>Todos</option>
                                                    <?php
                                                    $statusSaf = $this->medoo->select('status', ['cod_status', 'nome_status'], ['tipo_status' => 'O']);
                                                    foreach ($statusSaf as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['situacaoPesquisaOsm'] == $value['cod_status'])
                                                            echo("<option value='{$value['cod_status']}' selected>{$value['nome_status']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_status']}'>{$value['nome_status']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Data do Status (a Partir)</label>
                                                <input type="text" name="dataPartirOsm"
                                                       class="form-control data validaData"
                                                       value="<?php if (!empty($dadosRefill['dataPartirOsm'])) echo($dadosRefill['dataPartirOsm']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input name="dataAteOsm" type="text"
                                                       class="form-control data validaData"
                                                       value="<?php if (!empty($dadosRefill['dataAteOsm'])) echo($dadosRefill['dataAteOsm']); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingLocal">
                                    <a href="#localPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseLocal">
                                        <button class="btn btn-default" type="button"><label>Local Atuado</label>
                                        </button>
                                    </a>
                                </div>

                                <div id="localPesquisa" role="tabpanel" aria-labelledby="headingLocal"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['linhaPesquisaOsm']) || !empty($dadosRefill['trechoPesquisaOsm']) || !empty($dadosRefill['pnPesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Linha Atuada</label>
                                                <select class="form-control" name="linhaPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectLinha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);
                                                    foreach ($selectLinha as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaOsm'] == $value['cod_linha'])
                                                            echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Trecho Atuado</label>
                                                <select class="form-control" name="trechoPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if (!empty($dadosRefill['linhaPesquisaOsm'])){
                                                        $selectTrecho = $this->medoo->select('trecho', ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ['cod_linha' => (int)$dadosRefill['linhaPesquisaOsm']]);
                                                    } else {
                                                        $selectTrecho = $this->medoo->select('trecho', ['cod_trecho', 'nome_trecho', 'descricao_trecho']);
                                                    }

                                                    foreach ($selectTrecho as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaOsm'] == $value['cod_trecho'])
                                                            echo('<option value="' . $value['cod_trecho'] . '" selected>' . $value['nome_trecho'] . ' - ' . $value['descricao_trecho'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_trecho'] . '">' . $value['nome_trecho'] . ' - ' . $value['descricao_trecho'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Ponto Not�vel Atuado</label>
                                                <select class="form-control" name="pnPesquisaOsm">
                                                    <option value="">Pontos Not�veis</option>
                                                    <?php
                                                    if ($dadosRefill['linhaPesquisaOsm'] != "" && $dadosRefill['trechoPesquisaOsm'] == "") {

                                                    } else {
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaOsm'] != "")
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["cod_trecho" => (int)$dadosRefill['trechoPesquisaOsm']]);
                                                        else
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["ORDER" => "cod_trecho"]);

                                                        foreach ($selectPn as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['pnPesquisaOsm'] == $value['cod_ponto_notavel'])
                                                                echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingServico">
                                    <a href="#servicoPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseServico">
                                        <button class="btn btn-default" type="button"><label>Servi�o</label></button>
                                    </a>
                                </div>

                                <div id="servicoPesquisa" role="tabpanel" aria-labelledby="headingServico"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['grupoPesquisaOsm']) || !empty($dadosRefill['sistemaPesquisaOsm']) || !empty($dadosRefill['subSistemaPesquisa']) || !empty($dadosRefill['servicoPesquisaOsm']) || !empty($dadosRefill['nivelPesquisaOsm'])) ? 'in' : 'out'; ?>">
                                    <div class="panel-body">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <label>Grupo Sistema</label>
                                                <?php
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);
                                                $this->form->getSelectGrupo($_POST['grupo'], $grupo, 'grupoPesquisaOsm', true);
                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sistema</label>
                                                <select class="form-control" name="sistemaPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoPesquisaOsm']))
                                                        $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema" => "cod_sistema"], ['cod_sistema', 'nome_sistema'], ["cod_grupo" => $dadosRefill['grupoPesquisaOsm']]);
                                                    else
                                                        $selectSistema = $this->medoo->select("sistema", ['cod_sistema', 'nome_sistema']);

                                                    foreach ($selectSistema as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisaOsm'] == $value['cod_sistema'])
                                                            echo('<option value="' . $value['cod_sistema'] . '" selected>' . $value['nome_sistema'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_sistema'] . '">' . $value['nome_sistema'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Sistema</label>
                                                <select class="form-control" name="subSistemaPesquisa">
                                                    <option value="">Sub-Sistemas</option>
                                                    <?php
                                                    if ($dadosRefill['grupoPesquisaOsm'] != "" && $dadosRefill['sistemaPesquisaOsm'] == "") {

                                                    } else {
                                                        if (!empty($dadosRefill['sistemaPesquisaOsm']))
                                                            $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"], ['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisaOsm']]);
                                                        else
                                                            $subsistema = $this->medoo->select("subsistema", ['cod_subsistema', 'nome_subsistema']);

                                                        foreach ($subsistema as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisa'] == $value['cod_subsistema'])
                                                                echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Servi�o</label>
                                                <select class="form-control" name="servicoPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectServico = $this->medoo->select('servico', ['nome_servico', 'cod_servico'], ["ORDER" => 'nome_servico']);
                                                    foreach ($selectServico as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['servicoPesquisaOsm'] == $value['cod_servico'])
                                                            echo('<option value="' . $value['cod_servico'] . '" selected>' . $value['nome_servico'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_servico'] . '">' . $value['nome_servico'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label>N�vel</label>
                                                <select name="nivelPesquisaOsm" class="form-control">
                                                    <option value="">Todos</option>
                                                    <option
                                                        value="A" <?php if ($dadosRefill['nivelPesquisaOsm'] == "A") echo("selected") ?>>
                                                        A
                                                    </option>
                                                    <option
                                                        value="B" <?php if ($dadosRefill['nivelPesquisaOsm'] == "B") echo("selected") ?>>
                                                        B
                                                    </option>
                                                    <option
                                                        value="C" <?php if ($dadosRefill['nivelPesquisaOsm'] == "C") echo("selected") ?>>
                                                        C
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                        if ($dadosRefill['grupoPesquisaOsm'] == '27' || $dadosRefill['grupoPesquisaOsm'] == '28') {
                                            $bilhetagem = true;
                                        }else {
                                            $bilhetagem = false;
                                        }
                                        ?>
                                        <div class="row" id="local_sublocal" style="display: <?php echo ($bilhetagem) ? "block" : "none"?>;">
                                            <div class="col-md-4">
                                                <label>Local</label>
                                                <select name="localGrupoOsm" class="form-control" id="local_grupo">
                                                    <?php
                                                    $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                                    echo ("<option value = '' disable selected>Selecione o Local</option> ");
                                                    foreach ($local as $dados=>$value){
                                                        if ($dadosRefill['localGrupoOsm'] == $value['cod_local_grupo']){
                                                            echo ("<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>");
                                                        }else {
                                                            echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Local</label>
                                                <select name="subLocalGrupoOsm" id="sublocal_grupo" class="form-control">
                                                    <?php
                                                    if($dadosRefill['subLocalGrupoOsm']){
                                                        $selectLocal = $this->medoo->select('sublocal_grupo', ['[><]local_sublocal_grupo' => 'cod_sublocal_grupo'], '*', ['cod_local_grupo' => $dadosRefill['localGrupo']]);
                                                        foreach ( $selectLocal as $dados=>$value){
                                                            if($dadosRefill['subLocalGrupoOsm'] == $value['cod_sublocal_grupo']){
                                                                echo ("<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>");
                                                            }else {
                                                                echo("<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>");
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">

                                <div class="panel-heading" role="tab" id="headingRegistroExecucao">
                                    <a href="#registroExecucaoPesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false"
                                       aria-controls="collapseRegistroExecucao">
                                        <button class="btn btn-default" type="button"><label>Registro de
                                                Execu��o</label></button>
                                    </a>
                                </div>

                                <div id="registroExecucaoPesquisa" role="tabpanel"
                                     aria-labelledby="headingRegistroExecucao"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['unidadePesquisaOsm']) || !empty($dadosRefill['equipePesquisaOsm']) || !empty($dadosRefill['causaPesquisaOsm']) || !empty($dadosRefill['agCausadorPesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Unidade</label>
                                                <select class="form-control" name="unidadePesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selecUnidade = $this->medoo->select('unidade', ['cod_unidade', 'nome_unidade']);
                                                    foreach ($selecUnidade as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['unidadePesquisaOsm'] == $value['cod_unidade']) {
                                                            echo('<option value="' . $value['cod_unidade'] . '" selected>' . $value['nome_unidade'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Equipe</label>
                                                <select class="form-control" name="equipePesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if (!empty($dadosRefill['unidadePesquisaOsm'])){
                                                        $selectEquipe = $this->medoo->select("un_equipe", ["[><]equipe" => "cod_equipe"], ['sigla', "nome_equipe"], ["cod_unidade" => $dadosRefill['unidadePesquisaOsm']]);
                                                    } else {
                                                        $selectEquipe = $this->medoo->select("equipe", ['sigla', "nome_equipe"]);
                                                    }

                                                    foreach ($selectEquipe as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['equipePesquisaOsm'] == $value['sigla']) {
                                                            echo('<option value="' . $value['sigla'] . '" selected>' . $value['nome_equipe'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-5">
                                                <label>Causa</label>
                                                <select class="form-control" name="causaPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selecCausa = $this->medoo->select('causa', ['cod_causa', 'nome_causa'], ["ORDER" => 'nome_causa']);
                                                    foreach ($selecCausa as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['causaPesquisaOsm'] == $value['cod_causa']) {
                                                            echo('<option value="' . $value['cod_causa'] . '" selected>' . $value['nome_causa'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_causa'] . '">' . $value['nome_causa'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Causador</label>
                                                <select class="form-control" name="agCausadorPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selecAgCausador = $this->medoo->select('agente_causador', ['cod_ag_causador', 'nome_agente'], ["ORDER" => 'nome_agente']);
                                                    foreach ($selecAgCausador as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['agCausadorPesquisaOsm'] == $value['cod_ag_causador']) {
                                                            echo('<option value="' . $value['cod_ag_causador'] . '" selected>' . $value['nome_agente'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_ag_causador'] . '">' . $value['nome_agente'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">

                                <div class="panel-heading" role="tab" id="headingMaquinasEquipamentos">
                                    <a href="#maquinasEquipamentosPesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false"
                                       aria-controls="collapseMaquinasEquipamentos">
                                        <button class="btn btn-default" type="button"><label>M�quinas Equipamentos e
                                                Materiais</label></button>
                                    </a>
                                </div>

                                <div id="maquinasEquipamentosPesquisa" role="tabpanel"
                                     aria-labelledby="headingMaquinasEquipamentos"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['maquinasEquipamentosPesquisaOsm']) || !empty($dadosRefill['materiaisPesquisaOsm']) || !empty($dadosRefill['estadoPesquisaOsm']) || !empty($dadosRefill['origemPesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>M�quinas e Equipamentos</label>
                                                <select class="form-control" name="maquinasEquipamentosPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectMaquinasEquipamentos = $this->medoo->select("v_material", ['nome_material', 'cod_material'], ["cod_categoria" => 10, "ORDER" => "nome_material"]);
                                                    foreach ($selectMaquinasEquipamentos as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['maquinasEquipamentosPesquisaOsm'] == $value['cod_material']) {
                                                            echo('<option value="' . $value['cod_material'] . '" selected>' . $value['nome_material'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_material'] . '">' . $value['nome_material'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Materiais</label>
                                                <select class="form-control" name="materiaisPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectMateriais = $this->medoo->select("v_material", ['nome_material', 'cod_material'], ["ORDER" => "nome_material", "cod_categoria" => "32"]);

                                                    foreach ($selectMateriais as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['materiaisPesquisaOsm'] == $value['cod_material']) {
                                                            echo('<option value="' . $value['cod_material'] . '" selected>' . $value['nome_material'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_material'] . '">' . $value['nome_material'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Estado</label>
                                                <select class="form-control" name="estadoPesquisaOsm">
                                                    <?php
                                                    switch ($dadosRefill['estadoPesquisaOsm']) {
                                                        case '':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n">Novo</option>');
                                                            echo('<option value="u">Usado</option>');
                                                            break;
                                                        case 'n':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n" selected>Novo</option>');
                                                            echo('<option value="u">Usado</option>');
                                                            break;
                                                        case 'u':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n">Novo</option>');
                                                            echo('<option value="u" selected>Usado</option>');
                                                            break;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Origem</label>
                                                <select class="form-control" name="origemPesquisaOsm">
                                                    <?php
                                                    switch ($dadosRefill['origemPesquisaOsm']) {
                                                        case '':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 's':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s" selected>MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 'f':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f" selected>MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 'o':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o" selected>Outros</option>');
                                                            break;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">

                                <div class="panel-heading" role="tab" id="headingFechamento">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#fechamentoPesquisa"
                                       aria-expanded="false" aria-controls="collapseFechamento">
                                        <button class="btn btn-default" type="button"><label>Fechamento</label></button>
                                    </a>
                                </div>

                                <div id="fechamentoPesquisa" role="tabpanel" aria-labelledby="headingFechamento"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['tipoFechamentoPesquisaOsm']) || !empty($dadosRefill['tipoPendenciaPesquisaOsm']) || !empty($dadosRefill['liberacaoPesquisaOsm']) || !empty($dadosRefill['transferenciaPesquisaOsm'])) ? 'in' : 'out'; ?>">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <label>Pend�ncia</label>
                                                <select class="form-control" name="tipoPendenciaPesquisaOsm">
                                                    <option value="">Todas</option>
                                                    <?php
                                                    $selectPendencia = $this->medoo->select('pendencia', ['cod_pendencia', 'nome_pendencia'], ["ORDER" => 'nome_pendencia']);
                                                    foreach ($selectPendencia as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['tipoPendenciaPesquisaOsm'] == $value['cod_pendencia']) {
                                                            echo('<option value="' . $value['cod_pendencia'] . '" selected>' . $value['nome_pendencia'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_pendencia'] . '">' . $value['nome_pendencia'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Tipo de Fechamento</label>
                                                <select class="form-control" name="tipoFechamentoPesquisaOsm">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectTipoFechamento = $this->medoo->select('tipo_fechamento', ['cod_tipo_fechamento', 'nome_fechamento']);
                                                    foreach ($selectTipoFechamento as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['tipoFechamentoPesquisaOsm'] == $value['cod_tipo_fechamento']) {
                                                            echo('<option value="' . $value['cod_tipo_fechamento'] . '" selected>' . $value['nome_fechamento'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_tipo_fechamento'] . '">' . $value['nome_fechamento'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Libera��o</label>
                                                <select class="form-control" name="liberacaoPesquisaOsm">
                                                    <option value="">Ambos</option>
                                                    <option
                                                        value="s" <?php if ($dadosRefill['liberacaoPesquisaOsm'] == "s") echo("selected") ?>>
                                                        Sim
                                                    </option>
                                                    <option
                                                        value="n" <?php if ($dadosRefill['liberacaoPesquisaOsm'] == "n") echo("selected") ?>>
                                                        N�o
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Transferencia</label>
                                                <select class="form-control" name="transferenciaPesquisaOsm">
                                                    <option value="">Ambos</option>
                                                    <option
                                                        value="s" <?php if ($dadosRefill['transferenciaPesquisaOsm'] == "s") echo("selected") ?>>
                                                        Sim
                                                    </option>
                                                    <option
                                                        value="n" <?php if ($dadosRefill['transferenciaPesquisaOsm'] == "n") echo("selected") ?>>
                                                        N�o
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default mtRod" hidden>
                                <div class="panel-heading" role="tab" id="headingMaterialRodante">
                                    <a href="#materialRodantePesquisaOsm" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false"
                                       aria-controls="collapseMaterialRodante">
                                        <button class="btn btn-default"><label>Material Rodante</label></button>
                                    </a>
                                </div>

                                <div id="materialRodantePesquisaOsm" role="tabpanel"
                                     aria-labelledby="headingMaterialRodante"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['veiculoPesquisaOsm']) || !empty($dadosRefill['carroAvariadoPesquisaOsm']) || !empty($dadosRefill['carroLiderPesquisaOsm']) || !empty($dadosRefill['odometroPartirPesquisaOsm']) || !empty($dadosRefill['odometroAtePesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Ve�culo</label>
                                                <select name="veiculoPesquisaOsm" class="form-control">
                                                    <option value="">Ve�culo</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoPesquisaOsm'])){
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo' => $dadosRefill['grupoPesquisaOsm']]);
                                                    } else {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo']);
                                                    }

                                                    foreach ($veiculo as $dados => $value) {
                                                        if ($dadosRefill['veiculoPesquisaOsm'] == $value['cod_veiculo'])
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Carro Avariado</label>
                                                <select name="carroAvariadoPesquisaOsm" class="form-control">
                                                    <option value="">Carro Avariado</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoPesquisaOsm']) || !empty($dadosRefill['veiculoPesquisaOsm'])){
                                                        if (!empty($dadosRefill['grupoPesquisaOsm'])){
                                                            if (!empty($dadosRefill['veiculoPesquisaOsm'])){
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['AND' => ['cod_grupo' => $dadosRefill['grupoPesquisaOsm'], 'cod_veiculo' => $dadosRefill['veiculoPesquisaOsm']]]);
                                                            } else {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_grupo' => $dadosRefill['grupoPesquisaOsm']]);
                                                            }
                                                        } else {
                                                            $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_veiculo' => $dadosRefill['veiculoPesquisaOsm']]);
                                                        }
                                                    } else {
                                                        $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro']);
                                                    }

                                                    foreach ($carro as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['carroAvariadoPesquisaOsm'] == $value['cod_carro'])
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Prefixo</label>
                                                <select name="prefixoPesquisaOsm" class="form-control">
                                                    <option value="">Prefixo</option>
                                                    <?php
                                                    $prefixo = $this->medoo->select("prefixo", ['cod_prefixo', 'nome_prefixo']);

                                                    foreach ($prefixo as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['prefixoPesquisaOsm'] == $value['cod_prefixo'])
                                                            echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Od�metro a partir</label>
                                                <input name='odometroPartirPesquisaOsm' class='form-control'
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroPartirPesquisaOsm']; ?>"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Od�metro at�</label>
                                                <input name="odometroAtePesquisaOsm" class="form-control"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroAtePesquisaOsm']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <?php
                        $this->inputButtonDownDocFile(HOME_URI . "/dashboardGeral/csvPesquisaOsmCompleta", HOME_URI . "/dashboardGeral/csvPesquisaOsmMaoObra", HOME_URI . "/dashboardGeral/csvPesquisaOsmMateriais");
                    ?>

                </div>
            </div>
        </div>
    </div>
</form>
