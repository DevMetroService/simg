<?php

//  ----------------- NEW IMPLEMENTATION -----------------

if ($_POST){

    $hasVlt = false;
    $hasTue = false;

    if ($_POST['veiculos']) {

        $veiculos_solicitados_tue = "";
        $veiculos_solicitados_vlt = "";

        $ctdTue = 0;
        $ctdVlt = 0;
        foreach ($_POST['veiculos'] as $index => $cod_veiculo) {

            $Vlt = $this->medoo->select("veiculo", "cod_grupo", ["cod_veiculo" => $cod_veiculo ])[0];

            if($Vlt == 22){
                $hasVlt = true;
                if($ctdVlt == 0) {
                    $veiculos_solicitados_vlt .= "o.cod_veiculo = $cod_veiculo";
                }
                else {
                    $veiculos_solicitados_vlt .= " OR o.cod_veiculo = $cod_veiculo";
                }
                $ctdVlt++;
            }else{
                $hasTue = true;
                if($ctdTue == 0) {
                    $veiculos_solicitados_tue .= "o.cod_veiculo = $cod_veiculo";
                }
                else {
                    $veiculos_solicitados_tue .= " OR o.cod_veiculo = $cod_veiculo";
                }
                $ctdTue++;
            }

        }

    }

    $mes = $_POST['mes'];
    $ano = $_POST['ano'];


    /*
     * Executa o Script para VLT (true) ou para TUE (false)
     * */
    $grafX = [];

    if ($hasVlt) {
        if(!empty($mes)){
            $sql = "SELECT
            cod_odometro_vlt,
            o.cod_veiculo,
            v.nome_veiculo,
            odometro_ma,
            cast(data_cadastro as date)
            FROM odometro_vlt o
            JOIN veiculo v on v.cod_veiculo = o.cod_veiculo
            WHERE  
            EXTRACT (year FROM data_cadastro) = '{$ano}'
            AND 
            EXTRACT (month FROM data_cadastro) = '{$mes}'
            AND 
            ( {$veiculos_solicitados_vlt} )
            ORDER BY v.nome_veiculo, data_cadastro";


            $dt = "$ano-$mes-01";
            $data_inicial = strtotime( date("Y-m-01", strtotime($dt)) );
            $data_final = strtotime( date("Y-m-t", strtotime($dt)) );

            $totalDias = (($data_final - $data_inicial) / 86400)+1; //retorna o numero de dias entre data inicial e data final
        }
        else{
            $sql = "SELECT
            cod_odometro_vlt,
            o.cod_veiculo,
            v.nome_veiculo,
            odometro_ma,
            cast(data_cadastro as date)
            FROM odometro_vlt o
            JOIN veiculo v on v.cod_veiculo = o.cod_veiculo
            WHERE  
            EXTRACT (year FROM data_cadastro) = '{$ano}'
            AND 
            ( {$veiculos_solicitados_vlt} )
            ORDER BY v.nome_veiculo, data_cadastro";

            if($ano != date('Y')){
                $data_inicial = strtotime( "{$ano}-01-01" );
                $data_final = strtotime( "{$ano}-12-31" );
                $totalDias = (($data_final - $data_inicial) / 86400)+1; //retorna o numero de dias entre data inicial e data final
            }else{
                $data_inicial = strtotime( "{$ano}-01-01" );
                $totalDias = date('z')+1;
            }
        }


        $registros = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);


        //Armazena uma lista com codigo veiculos
        $veiculos = [];
        $veiculos_nome = [];


        // subistitui a referencia da tabela de 'odometros_vlt as o' para 'veiculos as v'
        $veiculos_solicitados_vlt = str_replace("o.", "v.", $veiculos_solicitados_vlt);
        $veiculos = $this->medoo->query("SELECT cod_veiculo, nome_veiculo FROM veiculo as v WHERE ( {$veiculos_solicitados_vlt} )  ")->fetchAll(PDO::FETCH_ASSOC);


        //Percorre a lista de codigos de veiculos armazenadas
        foreach ($veiculos as $veiculo) {

            //Verificador se possui registro ou n�o;
            $hasRegistro = false;

            //Percorre os dias desde a data inicial, ate a data final
            $odometro_dia = [];

            for ($dia = 0; $dia < $totalDias; $dia++) {

                $data_atual = date('Y-m-d', strtotime("+$dia days", $data_inicial) ); // retorna uma String com data_inicial + x dias
                $odometro = 0;

                //Verifica cada registro por veiculo e dia.
                foreach ($registros as $registro) {
                    //Verifico se possuo registro deste ve�culo no per�odo.
                    if (  $registro['cod_veiculo'] ==  $veiculo['cod_veiculo'] ) {
                        //Verifica se a data de cadastro � a data atual do loop.
                        if($registro['data_cadastro'] == $data_atual )  {
                            $odometro = $registro['odometro_ma'];
                            $hasRegistro = true;
                            break;
                        }
                        else {
                            if( $data_atual >= $registro['data_cadastro']){
                                $odometro = $registro['odometro_ma'];
                                continue;
                            }
                            //Verifica se � o primeiro dia do loop.
                            if($dia == 0) {

                                $sql ="select * from odometro_vlt
                                    where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    and cod_odometro_vlt IN (
                                        select max(cod_odometro_vlt) from odometro_vlt
                                        where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    )";

                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                $odometro = $result[0]['odometro_ma'];

                                //Verifico se n�o teve resultado, preenchendo 0.
                                if (empty($odometro)) {
                                    $odometro = 0;
                                }
                                $hasRegistro = true;
                                break;
                            }
                            else
                            {
                                //Pega registro anterior.
                                $odometro = $odometro_dia[($dia-1)];
                                $hasRegistro = true;
                                break;
                            }

                        }
                    }
                }

                //Caso Ve�culo n�o possua registro, pega valor anterior ou zero.
                if(!$hasRegistro)
                {
                    $sql ="select * from odometro_vlt
                                    where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    and cod_odometro_vlt IN (
                                        select max(cod_odometro_vlt) from odometro_vlt
                                        where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    )";

                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                    $odometro = $result[0]['odometro_ma'];

                    //Verifico se n�o teve resultado, preenchendo 0.
                    if (empty($odometro)) {
                        $odometro = 0;
                    }

                    //Preenche valor encontra(ou n�o) por todos os dias e quebra loop de dia.
                    for ($dia = 0; $dia < $totalDias; $dia++)
                    {
                        array_push($odometro_dia, $odometro); //terminou o dia, salve o odometro
                    }

                    // busca e aramazena o nome do veiculo para depois montar o json

                    array_push($grafX, array("veiculo"=>$veiculo['cod_veiculo'], "nome_veiculo" => $veiculo['nome_veiculo'], "odometros"=>$odometro_dia));

                    break;
                }

                array_push($odometro_dia, (int) $odometro); //terminou o dia, salve o odometro
            }

            if(!$hasRegistro)
            {
                continue;
            }

            array_push($grafX, array("veiculo"=>$veiculo['cod_veiculo'], "nome_veiculo" => $veiculo['nome_veiculo'], "odometros"=>$odometro_dia));
        }

    }

    if($hasTue){
        if(!empty($mes)){
            $sql = "SELECT
            cod_odometro_tue,
            o.cod_veiculo,
            v.nome_veiculo,
            odometro,
            cast(data_cadastro as date)
            FROM odometro_tue o
            JOIN veiculo v on v.cod_veiculo = o.cod_veiculo
            WHERE
            EXTRACT (year FROM data_cadastro) = '{$ano}'
            AND
            EXTRACT (month FROM data_cadastro) = '{$mes}'
            AND
            ( {$veiculos_solicitados_tue} )
            ORDER BY v.nome_veiculo, data_cadastro";


            $dt = "$ano-$mes-01";
            $data_inicial = strtotime( date("Y-m-01", strtotime($dt)) );
            $data_final = strtotime( date("Y-m-t", strtotime($dt)) );
            $totalDias = (($data_final - $data_inicial) / 86400)+1; //retorna o numero de dias entre data inicial e data final
        }
        else{
            $sql = "SELECT
        cod_odometro_tue,
        o.cod_veiculo,
        v.nome_veiculo,
        odometro,
        cast(data_cadastro as date)
        FROM odometro_tue o
        JOIN veiculo v on v.cod_veiculo = o.cod_veiculo
        WHERE
        EXTRACT (year FROM data_cadastro) = '{$ano}'
        AND
        ( {$veiculos_solicitados_tue} )
        ORDER BY v.nome_veiculo, data_cadastro";

            if($ano != date('Y')){
                $data_inicial = strtotime( "{$ano}-01-01" );
                $data_final = strtotime( "{$ano}-12-31" );
                $totalDias = (($data_final - $data_inicial) / 86400)+1; //retorna o numero de dias entre data inicial e data final
            }else{
                $data_inicial = strtotime( "{$ano}-01-01" );
                $totalDias = date('z')+1;
            }
        }

        $registros = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        //Armazena uma lista com codigo veiculos
        $veiculos = [];
        $veiculos_nome = [];

        // subistitui a referencia da tabela de 'odometros_tue as o' para 'veiculos as v'
        $veiculos_solicitados_tue = str_replace("o.", "v.", $veiculos_solicitados_tue);
        $veiculos = $this->medoo->query("SELECT cod_veiculo, nome_veiculo FROM veiculo as v WHERE ( {$veiculos_solicitados_tue} )  ")->fetchAll(PDO::FETCH_ASSOC);

        //Percorre a lista de codigos de veiculos armazenadas
        foreach ($veiculos as $veiculo) {

            //Verificador se possui registro ou n�o;
            $hasRegistro = false;

            //Percorre os dias desde a data inicial, ate a data final
            $odometro_dia = [];

            for ($dia = 0; $dia < $totalDias; $dia++) {

                $data_atual = date('Y-m-d', strtotime("+$dia days", $data_inicial) ); // retorna uma String com data_inicial + x dias
                $odometro = 0;

                //Verifica cada registro por veiculo e dia.
                foreach ($registros as $registro) {
                    //Verifico se possuo registro deste ve�culo no per�odo.
                    if (  $registro['cod_veiculo'] ==  $veiculo['cod_veiculo'] ) {
                        //Verifica se a data de cadastro � a data atual do loop.
                        if($registro['data_cadastro'] == $data_atual )  {
                            $odometro = $registro['odometro'];
                            $hasRegistro = true;
                            break;
                        }
                        else {
                            if( $data_atual >= $registro['data_cadastro']){
                                $odometro = $registro['odometro'];
                                continue;
                            }
                            //Verifica se � o primeiro dia do loop.
                            if($dia == 0) {

                                $sql ="select * from odometro_tue
                                    where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    and cod_odometro_tue IN (
                                        select max(cod_odometro_tue) from odometro_tue
                                        where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    )";

                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                $odometro = $result[0]['odometro'];

                                //Verifico se n�o teve resultado, preenchendo 0.
                                if (empty($odometro)) {
                                    $odometro = 0;
                                }
                                $hasRegistro = true;
                                break;
                            }
                            else
                            {
                                //Pega registro anterior.
                                $odometro = $odometro_dia[($dia-1)];
                                $hasRegistro = true;
                                break;
                            }

                        }
                    }
                }

                //Caso Ve�culo n�o possua registro, pega valor anterior ou zero.
                if(!$hasRegistro)
                {
                    $sql ="select * from odometro_tue
                                    where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    and cod_odometro_tue IN (
                                        select max(cod_odometro_tue) from odometro_tue
                                        where cod_veiculo = {$veiculo['cod_veiculo']} and data_cadastro < '{$data_atual}'
                                    )";

                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                    $odometro = $result[0]['odometro'];

                    //Verifico se n�o teve resultado, preenchendo 0.
                    if (empty($odometro)) {
                        $odometro = 0;
                    }

                    //Preenche valor encontra(ou n�o) por todos os dias e quebra loop de dia.
                    for ($dia = 0; $dia < $totalDias; $dia++)
                    {
                        array_push($odometro_dia, $odometro); //terminou o dia, salve o odometro
                    }

                    // busca e aramazena o nome do veiculo para depois montar o json

                    array_push($grafX, array("veiculo"=>$veiculo['cod_veiculo'], "nome_veiculo" => $veiculo['nome_veiculo'], "odometros"=>$odometro_dia));

                    break;
                }

                array_push($odometro_dia, $odometro); //terminou o dia, salve o odometro
            }

            if(!$hasRegistro)
            {
                continue;
            }

            array_push($grafX, array("veiculo"=>$veiculo['cod_veiculo'], "nome_veiculo" => $veiculo['nome_veiculo'], "odometros"=>$odometro_dia));
        }

    }

}


$json = json_encode($grafX);

echo "<textarea id='dbug' style='display: none;'>$json</textarea>";
echo "<input id='totalDias' value='{$totalDias}' style='display: none;'/>";


?>

<div class="row" xmlns="http://www.w3.org/1999/html">
        <form action="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/KmRodadosPorVeiculo" method="post">
        <div class="col-md-12 hidden-print">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>
                        <i class="fa fa-pie-chart fa-fw"></i>
                        <strong>
                            Relat�rio de Km Rodados por Ve�culo
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <!-- Grafico -->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="container"></div>
                        </div>
                    </div>

                    <!-- Filtros -->
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>M�s</label>
                                                    <select id="mes" class="form-control" name="mes">
                                                        <option value="">TODOS</option>
                                                        <?php
                                                        foreach (MainController::$monthComplete as $key => $value){
                                                            if($_POST['mes'] == $key){
                                                                echo "<option value='{$key}' selected>{$value}</option>";
                                                            }else{
                                                                echo "<option value='{$key}'>{$value}</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php //echo "<input id='dataPartir' class='form-control data add-on validaData' type='text' name='dataPartir' value=' {$_POST['dataPartir']} ' required>"; ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Ano</label>
                                                    <select id="ano" name="ano" class="form-control">
                                                        <?php

                                                            $sql = "SELECT to_char(data_cadastro, 'YYYY') AS YEAR 
                                                                    from odometro_tue
                                                                    group by YEAR 
                                                                    order by YEAR DESC";

                                                            $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                            foreach ($result as $key => $value) {
                                                                if($_POST['ano'] == $value['year']){
                                                                    echo ("<option value='{$value['year']}' selected>{$value['year']}</option>");
                                                                }else{
                                                                    echo ("<option value='{$value['year']}'>{$value['year']}</option>");
                                                                }
                                                            }

                                                        ?>
                                                    </select>

                                                    <?php //echo "<input id='dataAte' class='form-control data add-on validaData' type='text' name='dataAte' value=' {$_POST['dataAte']} ' required>"; ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Veiculo</label>
                                                    <select
                                                            id='selectVeiculos' name="veiculos[]"
                                                            class="form-control multiselect"
                                                            multiple="multiple" required>
                                                        <?php
                                                        $selectFunc = $this->medoo->select("veiculo", ["cod_grupo", "cod_veiculo", "nome_veiculo"], ["cod_grupo" => [22,23] ] );

                                                        $txtOption = array();

                                                        foreach ($selectFunc as $dados => $value) {
                                                            $txtOption[$value['cod_grupo']] .= "<option value='{$value['cod_veiculo']}'>{$value["nome_veiculo"]}</option>";
                                                        }
                                                        ?>
                                                        <optgroup id="vlt" label="VLT" >
                                                            <?php echo $txtOption['22'] ?>
                                                        </optgroup>
                                                        <optgroup id="tue" label="TUE" >
                                                            <?php echo $txtOption['23'] ?>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-large btn-block">
                                        <i class="fa fa-refresh"></i> Atualizar Gr�fico
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block">
                                        <i class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-default btn-large btn-block btn_print_rel">
                                        <i class="fa fa-print"></i> Imprimir
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Tabela DataTables-->
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>Odometros por Ve�culo</h4>
                                </div>
                                <div id="listaDeDias" class="panel-body" style="overflow-y: auto; height: 430px;">

                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>

    </form>


</div>

<?php unset( $_POST ); ?>