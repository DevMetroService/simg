<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 02/10/2017
 * Time: 14:11
 */

$where = array();

$where[] = 'WHERE cod_status = 14 AND cod_causa = 538'; // Encerrada // Retrabalho

if ($_POST['aPartir']) {
    $where[] = "v_saf.data_abertura >= '{$_POST['aPartir']} 00:00:00'";
}

if ($_POST['ate']) {
    $where[] = "v_saf.data_abertura <= '{$_POST['ate']} 00:00:00'";
}

if ($_POST['grupo']) {
    $where[] = "v_saf.cod_grupo = " . $_POST['grupo'];
} else {
    $where[] = "v_saf.cod_grupo IN (22,23,26)";
}

if ($_POST['sistema']) {
    $where[] = "v_saf.cod_sistema = " . $_POST['sistema'];
}

if ($_POST['subSistema']) {
    $where[] = "v_saf.cod_subsistema = " . $_POST['subSistema'];
}

if ($_POST['veiculo']) {
    $where[] = "v.cod_veiculo = {$_POST['veiculo']}";
}

$sqlWhere = '';
if (count($where)) {
    $sqlWhere .= implode(' AND ', $where);
} else {
    $sqlWhere = "";
}

$sql = "SELECT
          cod_saf             AS saf,
          cod_osm             AS osm,
          v_saf.data_abertura AS abertura,
          nome_avaria         AS avaria,
          nome_atuacao        AS atuacao,
          v.nome_veiculo      AS veiculo,
          desc_atuacao        AS descricao
        FROM v_saf
          JOIN ssm USING (cod_saf)
          JOIN osm_falha USING (cod_ssm)
          JOIN osm_registro USING (cod_osm)
          JOIN atuacao USING (cod_atuacao)
          JOIN material_rodante_osm mro USING(cod_osm) 
          JOIN veiculo v ON(mro.cod_veiculo = v.cod_veiculo) {$sqlWhere}";

//echo $sql;

$returnSql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
?>

<form id="formPage" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Gr�fico / Relat�rio de �ndices de
                            Retrabalho</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <label>Sele��o de Filtros</label>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <?php
                                                    $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_grupo[!]" => [20, 21, 24, 25]]]);
                                                    MainForm::getSelectGrupo($_POST['grupo'], $grupo, 'grupo', false);
                                                    ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <?php
                                                    if ($_POST['grupo'])
                                                        $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupo']]);
                                                    MainForm::getSelectSistema($_POST['sistema'], $sistema, 'sistema', false);
                                                    ?>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Subsistema</label>
                                                    <?php
                                                    if ($_POST['sistema'])
                                                        $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);
                                                    MainForm::getSelectSubsistema($_POST['subSistema'], $subsistema, 'subSistema', false);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Ve�culo</label>
                                                    <?php
                                                    if ($_POST['grupo'])
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo' => $_POST['grupo']]);
                                                    MainForm::getSelectVeiculo($_POST['veiculo'], $veiculo);
                                                    ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>A partir</label>
                                                    <input class="form-control data"
                                                           value="<?php if (!empty($_POST['aPartir'])) echo $_POST['aPartir'] ?>"
                                                           name="aPartir">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>At�</label>
                                                    <input class="form-control data"
                                                           value="<?php if (!empty($_POST['ate'])) echo $_POST['ate'] ?>"
                                                           name="ate">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-offset-1 col-md-5">
                                        <button type="button" class="btn btn-primary btn-large btn-block filtrarRel"><i
                                                class="fa fa-gear"></i> Gerar Relat�rio
                                        </button>
                                    </div>
                                    <div class="col-md-5">
                                        <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                                class="fa fa-reply-all"></i> Limpar Filtros
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Tabela de Resultado</label></div>
                                <div class="panel-body">
                                    <table class="tableResultRel table table-bordered table-striped table-responsive">
                                        <thead>
                                        <tr>
                                            <th>SAF</th>
                                            <th>OSM</th>
                                            <th>Data Abertura</th>
                                            <th>Ve�culo</th>
                                            <th>Avaria</th>
                                            <th>Atua��o</th>
                                            <th>Descri��o</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach($returnSql as $dados=>$value){
                                            echo("<tr>");
                                            echo("<td>{$value['saf']}</td>");
                                            echo("<td>{$value['osm']}</td>");
                                            echo("<td>{$value['abertura']}</td>");
                                            echo("<td>{$value['veiculo']}</td>");
                                            echo("<td>{$value['avaria']}</td>");
                                            echo("<td>{$value['atuaca']}</td>");
                                            echo("<td>{$value['descricao']}</td>");
                                            echo("</tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
