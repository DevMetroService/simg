<?php
$saf = $_POST;
if(!empty($saf['cod'])){
    $sql = "SELECT cod_status, data_status, descricao FROM status_saf s 
                  WHERE cod_saf = {$saf['cod']} ORDER BY data_status DESC";

    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    $sqlData = "SELECT  data_abertura, usuario,
                        descricao_trecho, nome_grupo,
                        nome_linha, nivel, nome_avaria 
                        FROM v_saf WHERE cod_saf = {$saf['cod']}";
    
    $safResult = $this->medoo->query($sqlData)->fetchAll(PDO::FETCH_ASSOC);
    $safResult = $safResult[0];
    $dataAbertura = MainController::parse_timestamp_static($safResult['data_abertura']);
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Hist�rico da Solicita��o de Abertura de Falha</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4 hidden-print">
                        <form method="post">
                            <div class="row" style="margin-top: 5%">
                                <label>C�digo SAF</label>
                                <div class="input-group">
                                    <input required type="text" class="form-control number" name="cod" value="<?php echo $saf['cod'] ?>">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Pesquisar</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php
                    if(!empty($safResult)){
                        echo <<<HTML
                    <div class="col-md-offset-2 col-md-8 visible-print">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Saf de n�mero:</label>
                                <input value="{$saf['cod']}" disabled class="form-control"/>
                            </div>
                            <div class="col-xs-3">
                                <label>N�vel:</label>
                                <input value="{$safResult['nivel']}" disabled class="form-control"/>
                            </div>
                            <div class="col-xs-6">
                                <label>Criada por:</label>
                                <input value="{$safResult['usuario']}" disabled class="form-control"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Linha:</label>
                                <input value="{$safResult['nome_linha']}" disabled class="form-control"/>
                            </div>
                            <div class="col-xs-6">
                                <label>Trecho:</label>
                                <input value="{$safResult['descricao_trecho']}" disabled class="form-control"/>
                            </div>
                            <div class="col-xs-3">
                                <label>Grupo:</label>
                                <input value="{$safResult['nome_grupo']}" disabled class="form-control"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Avaria:</label>
                                <input value="{$safResult['nome_avaria']}" disabled class="form-control"/>
                            </div>
                        </div>
                    </div>
HTML;
                    }
                    ?>
                </div>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <?php
                        if(!empty($result)){
                            $tag = false;
                            $codigoSaf = "Saf: {$saf['cod']}";
                            foreach ($result as $dados){
                                $dados['data_status'] = MainController::parse_timestamp_static($dados['data_status']);

                                switch ($dados['cod_status']){
                                    case 4: // SAF Aberta
                                        if($dataAbertura == $dados['data_status']) {
                                            $status = "Aberta";
                                            $descricao = "Criada pelo us�ario, enviado ao CCM para verifica��o.";
                                        }else{
                                            $status = "Reenviada";
                                            $descricao = "Reenviada ao CCM para verifica��o.";
                                        }
                                        $data = $dados['data_status'];

                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSaf);
                                        break;
                                    case 3: // SAF Devolvida
                                        $status = "Devolvida";
                                        $descricao = "Devolvida pelo CCM com observa��o ao us�ario.<br />Obs: \"{$dados['descricao']}\"";
                                        $data = $dados['data_status'];

                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSaf);
                                        break;
                                    case 2: // SAF Cancelada
                                        $status = "Cancelada";
                                        $descricao = "Solicita��o Cancelada.<br />Justificativa: \"{$dados['descricao']}\"";
                                        $data = $dados['data_status'];

                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSaf);
                                        break;
                                    case 14: // SAF Encerrada
                                        $status = "Finalizada";
                                        $descricao = "Solicita��o conclu�da.";
                                        $data = $dados['data_status'];

                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSaf);
                                        break;
                                    case 27: // SAF Programada
                                        $sql = "SELECT cod_ssp, cod_status, data_status, data_programada, descricao FROM status_ssp
                                                                JOIN ssm_ssp_cancelada USING(cod_ssp)
                                                                JOIN ssp USING(cod_ssp)
                                                                JOIN ssm USING(cod_ssm)
                                                                WHERE cod_saf = {$saf['cod']} ORDER BY data_status DESC";

                                        $resultSsp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                        foreach ($resultSsp as $v) {
                                            $v['data_status'] = MainController::parse_timestamp_static($v['data_status']);

                                            $codigoSsp = "Ssp: {$v['cod_ssp']}";

                                            switch ($v['cod_status']) {
                                                case 19:  // SSP Programado
                                                    $status = "Em Atendimento";
                                                    $descricao = "Atendimento agendado, com data prevista para {$this->parse_timestamp($v['data_programada'])}";
                                                    $data = $v['data_status'];

                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                    break;
                                                case 22:  // SSP Pendente
                                                    $status = "Em Atendimento";
                                                    $descricao = "Aguardando Retorno";
                                                    $data = $v['data_status'];

                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                    break;
                                                case 21:  // SSP Autorizada
                                                    $status = "Em Atendimento";
                                                    $descricao = "A equipe est� atuando na solicita��o";
                                                    $data = $v['data_status'];

                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                    break;
                                                case 18:  // SSP Encerrada
                                                    $status = "Finalizada";
                                                    $descricao = "Solicita��o conclu�da.";
                                                    $data = $v['data_status'];

                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                    break;
                                                case 17:  // SSP Cancelada
                                                    $status = "Cancelada";
                                                    $descricao = "Solicita��o Cancelada.<br />Justificativa: \"{$v['descricao']}\"";
                                                    $data = $v['data_status'];

                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                    break;
                                            }
                                        }
                                        break;
                                    case 1: // SAF Autorizada
                                        if(!$safAutorizada) {
                                            $sql = "SELECT ss.cod_ssm, cod_status, data_status, descricao FROM status_ssm ss
                                                    JOIN ssm USING(cod_ssm)
                                                    WHERE cod_saf = {$saf['cod']} ORDER BY data_status DESC";

                                            $resultSsm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            $tagSsm = true;

                                            foreach ($resultSsm as $val) {
                                                $val['data_status'] = MainController::parse_timestamp_static($val['data_status']);

                                                $codigoSsm = "Ssm: {$val['cod_ssm']}";

                                                switch ($val['cod_status']) {
                                                    case 26: // SSM Programada
                                                    case 16: // SSM Encerrada
                                                    case 7:  // SSM Cancelada
                                                        // -- // -- Tratamento em SAF -- // -- //
                                                        // -- // -- Tratamento em SAF -- // -- //
                                                        break;
                                                    case 5: // SSM Autorizada
                                                        $status = "Em Atendimento";
                                                        $descricao = "A equipe est� atuando na solicita��o";
                                                        $data = $val['data_status'];

                                                        $sql = "SELECT cod_osm FROM osm_falha
                                                                  WHERE cod_ssm = {$val['cod_ssm']} AND data_abertura = '{$val['data_status']}' ";

                                                        $resultOsm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        $codigoOsm = "Osm: {$resultOsm[0]['cod_osm']}";

                                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoOsm);
                                                        $tagSsm = true;
                                                        break;
                                                    case 28: // SSM Agendada
                                                        $sql = "SELECT ss.cod_ssp, cod_status, data_status, data_programada FROM status_ssp ss
                                                                JOIN ssm_ssp USING(cod_ssp)
                                                                JOIN ssp USING(cod_ssp)
                                                                WHERE cod_ssm = {$val['cod_ssm']} ORDER BY data_status DESC";

                                                        $resultSsp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        foreach ($resultSsp as $v) {
                                                            $v['data_status'] = MainController::parse_timestamp_static($v['data_status']);

                                                            $codigoSsp = "Ssp: {$v['cod_ssp']}";

                                                            switch ($v['cod_status']) {
                                                                case 19:  // SSP Programado
                                                                    $status = "Em Atendimento";
                                                                    $descricao = "Atendimento agendado, com data prevista para {$this->parse_timestamp($v['data_programada'])}";
                                                                    $data = $val['data_status'];

                                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                                    break;
                                                                case 22:  // SSP Pendente
                                                                    $status = "Em Atendimento";
                                                                    $descricao = "Aguardando Retorno.";
                                                                    $data = $v['data_status'];

                                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsp);
                                                                    break;
                                                                case 21:  // SSP Autorizada
                                                                    $status = "Em Atendimento";
                                                                    $descricao = "A equipe est� atuando na solicita��o";
                                                                    $data = $v['data_status'];

                                                                    $sql = "SELECT cod_osp FROM osp
                                                                  WHERE cod_ssp = {$v['cod_ssp']} AND data_abertura = '{$v['data_status']}' ";

                                                                    $resultOsp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                                    $codigoOsp = "Osp: {$resultOsp[0]['cod_osp']}";

                                                                    $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoOsp);
                                                                    break;
                                                                case 18:  // SSP Encerrada
                                                                case 17:  // SSP Cancelada
                                                                    // -- // -- Tratamento em SAF -- // -- //
                                                                    // -- // -- Tratamento em SAF -- // -- //
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                    case 15: // SSM Pendente
                                                    case 8:  // SSM Transferida
                                                        $status = "Em Atendimento";
                                                        $descricao = "Aguardando Retorno.";
                                                        $data = $val['data_status'];

                                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsm);
                                                        $tagSsm = true;
                                                        break;
                                                    case 35:
                                                        $status = "Aguardando Aprova��o do Servi�o";
                                                        $descricao = "Aguardando valida��o do servi�o realizado.";
                                                        $data = $val['data_status'];

                                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsm);
                                                        $tagSsm = true;
                                                        break;
                                                    case 36:
                                                        $status = "Servi�o N�o Aprovado.";
                                                        $descricao = "Aguardando novo atendimento.";
                                                        $data = $val['data_status'];

                                                        $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsm);
                                                        $tagSsm = true;
                                                        break;
                                                    default: // SSM Devolvida
                                                             // SSM Aberta
                                                             // SSM Encaminhado
                                                        if ($tagSsm) {
                                                            $tagSsm = false;

                                                            $status = "Em An�lise";
                                                            $descricao = "A solicita��o est� sendo an�lisada pelo CCM.";
                                                            $data = $val['data_status'];

                                                            $this->imprimirAlteracaoStatus($status, $descricao, $data, $codigoSsm);
                                                        }
                                                        break;
                                                }
                                            }
                                            $safAutorizada = true;
                                        }
                                        break;
                                }
                            }

                            echo '<div class="row hidden-print" style="margin-top: 5%; margin-bottom: 5%">
                                        <div class="col-md-offset-4 col-md-4">
                                            <button class="btn btn-primary btn-large btn-block" onclick="window.print()"><i class="fa fa-print"></i> Imprimir</button>
                                        </div>
                                    </div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>