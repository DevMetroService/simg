<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);

$grupo = '';
if (!$_POST['grupoSistema']) {
    $grupo = "AND cod_grupo = 22 OR cod_grupo = 23 OR cod_grupo = 26 ";
}

$sql = "SELECT 
            cod_saf,
            nivel,
            nome_grupo,
            nome_avaria,
            nome_linha || ' - ' || descricao_trecho AS local,
            data_abertura,
            
            (
                CASE
                    WHEN 
                        data_abertura + ('60 days') :: INTERVAL < CURRENT_TIMESTAMP
                        THEN 
                            '1'
                    WHEN 
                        data_abertura + ('30 days') :: INTERVAL < CURRENT_TIMESTAMP 
                        THEN 
                            '2' 
                    WHEN 
                        data_abertura + ('7 days') :: INTERVAL < CURRENT_TIMESTAMP 
                        THEN 
                            '3'
                    WHEN 
                        data_abertura + ('5 days') :: INTERVAL < CURRENT_TIMESTAMP 
                        THEN 
                            '4'
                    WHEN 
                        data_abertura + ('2 days') :: INTERVAL < CURRENT_TIMESTAMP 
                        THEN 
                            '5'
                    ELSE 
                        '6'
                END
            ) AS categoria
            
            FROM v_saf
            
            WHERE nome_status <> 'Encerrada' AND nome_status <> 'Cancelada' AND nome_status <> 'Programada' {$grupo} ";

$sqlOrder = " ORDER BY data_abertura";

if ($where) {
    $sql = $sql . " AND " . $where . $sqlOrder;
} else {
    $sql = $sql . $sqlOrder;
}

//echo $sql;

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$cat0 = 0;
$cat2 = 0;
$cat5 = 0;
$cat7 = 0;
$cat30 = 0;
$cat60 = 0;
?>

    <div class="row">
        <div class="col-md-12 hidden-print">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>
                        <i class="fa fa-pie-chart fa-fw"></i>
                        <strong>
                            Falhas di�rias de Material Rodante.
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="container"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <form method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Linha</label>
                                                        <select class="form-control" name="linha">
                                                            <option value="">Todos</option>
                                                            <?php
                                                            $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                            foreach ($selectLinha as $dados => $value) {
                                                                if ($_POST['linha'] == $value['cod_linha']) {
                                                                    echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                                } else {
                                                                    echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Trecho</label>
                                                        <select class="form-control" name="trecho">
                                                            <option value="">Todos</option>
                                                            <?php
                                                            if ($_POST['linha']) {
                                                                $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$_POST['linha']]);

                                                                foreach ($selectTrecho as $dados => $value) {
                                                                    if ($_POST['trecho'] == $value['cod_trecho'])
                                                                        echo("<option value='{$value['cod_trecho']}' selected>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                                    else
                                                                        echo("<option value='{$value['cod_trecho']}'>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Ponto Not�vel</label>
                                                        <select class="form-control" name="pn">
                                                            <option value="">Todos</option>
                                                            <?php
                                                            if ($_POST['trecho']) {
                                                                $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["cod_trecho" => (int)$_POST['trecho']]);

                                                                foreach ($selectPn as $dados => $value) {
                                                                    if ($_POST['pn'] == $value['cod_ponto_notavel'])
                                                                        echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                                    else
                                                                        echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Grupo de Sistema</label>
                                                        <select name="grupoSistema" class="form-control">
                                                            <option value="">Grupos de Sistemas</option>
                                                            <?php
                                                            $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_grupo[=]" => [22, 23, 26]]]);

                                                            foreach ($grupoSistema as $dados => $value) {
                                                                if ($_POST['grupoSistema'] == $value['cod_grupo']) {
                                                                    echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                                } else {
                                                                    echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Sistema</label>
                                                        <select name="sistema" class="form-control">
                                                            <option value="">Sistemas</option>
                                                            <?php
                                                            if ($_POST['grupoSistema']) {
                                                                $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                                foreach ($sistema as $dados => $value) {
                                                                    if ($_POST['sistema'] == $value['cod_sistema'])
                                                                        echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                    else
                                                                        echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Sub-Sistema</label>
                                                        <select name="subSistema" class="form-control">
                                                            <option value="">Sub-Sistemas</option>
                                                            <?php
                                                            if ($_POST['sistema']) {
                                                                $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                                foreach ($subSistema as $dados => $value) {
                                                                    if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                        echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                    else
                                                                        echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary btn-large btn-block">
                                            <i class="fa fa-refresh"></i> Atualizar Gr�fico
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="limpaFiltro btn btn-success btn-large btn-block">
                                            <i class="fa fa-reply-all"></i> Limpar Filtros
                                        </button>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" onclick="print()"
                                                class="btn btn-default btn-large btn-block">
                                            <i class="fa fa-print"></i> Imprimir
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered text-center">
                                <tr>
                                    <td id="tabCat0"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #0b97c4; "></i> - de 0 a 2 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat2"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #4CAF50; "></i> - de 3 a 5 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat5"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #FFEB3B; "></i> - de 6 a 7 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat7"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #FF9800; "></i> - de 8 a 30 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat30"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #FF5722; "></i> - de 31 a 60 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat60"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #D50000; "></i> - Acima de 60 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold" id="total"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #B0BEC5; "></i> -
                                        <strong>Total</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel-body hidden-print">
                    <div class="col-md-12">
                        <table class="tableRel table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Cod SAF</th>
                                <th>Data de Abertura</th>
                                <th>Grupo</th>
                                <th>Local</th>
                                <th>Avaria</th>
                                <th>N�vel</th>
                                <th>Dias em Aberto</th>
                                <th>Categoria</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($result as $dados) {
                                switch ($dados['categoria']) {
                                    case '1':
                                        $categoria = "Acima de 60 dias";
                                        $cat60++;
                                        break;
                                    case '2':
                                        $categoria = "de 31 a 60 dias";
                                        $cat30++;
                                        break;
                                    case '3':
                                        $categoria = "de 7 a 30 dias";
                                        $cat7++;
                                        break;
                                    case '4':
                                        $categoria = "de 0 a 10 dias";
                                        $cat5++;
                                        break;
                                    case '5':
                                        $categoria = "de 11 a 30 dias";
                                        $cat2++;
                                        break;
                                    case '6':
                                        $categoria = "de 11 a 30 dias";
                                        $cat0++;
                                        break;
                                }

                                $dataAbertura = MainController::parse_timestamp_date($dados['data_abertura']);

                                $interval = date_diff(new DateTime($dataAbertura), new DateTime())->days;

                                echo("<tr>");
                                echo("<td>{$dados['cod_saf']}</td>");
                                echo("<td>{$dataAbertura}</td>");
                                echo("<td>{$dados['nome_grupo']}</td>");
                                echo("<td>{$dados['local']}</td>");
                                echo("<td>{$dados['nome_avaria']}</td>");
                                echo("<td>{$dados['nivel']}</td>");
                                echo("<td>{$interval}</td>");
//                            echo("<td>{$interval->format('%a')}</td>");
                                echo("<td>{$categoria}</td>");
                                echo("<td>");
                                echo("<button type='button' class='abrirModalSaf btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirSaf'><i class='fa fa-eye'></i></button>");
                                echo("</td>");
                                echo("</tr>");
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="tableRelHide visible-print" hidden>
            <div class="row">
                <div>
                    <img src="<?php echo HOME_URI ?>/views/_images/metrofor.png" width="100px" height="50px"
                         style="float: left">
                </div>
                <div style="text-align: center; width: 100%">
                    <h1>SAF's 60 Dias</h1>
                </div>
            </div>
            <div class="row" style="margin-top: 50px;">
                <div class="col-md-12">
                    <div id="containerPrint" style="width: 100%;"></div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table text-center" style="margin-top: 50px; font-family: Arial; text-align: center;">
                    <thead>
                    <tr style='border: solid;'>
                        <th style='border: solid;'>Cod SAF</th>
                        <th style='border: solid;'>Data de Abertura</th>
                        <th style='border: solid;'>Grupo</th>
                        <th style='border: solid;'>Local</th>
                        <th style='border: solid;'>Avaria</th>
                        <th style='border: solid;'>N�vel</th>
                        <th style='border: solid;'>Dias em Aberto</th>
                        <th style='border: solid;'>Categoria</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($result as $dados) {
                        switch ($dados['categoria']) {
                            case '1':
                                $categoria = "Acima de 60 dias";
                                $cat60++;
                                break;
                            case '2':
                                $categoria = "de 31 a 60 dias";
                                $cat30++;
                                break;
                            case '3':
                                $categoria = "de 7 a 30 dias";
                                $cat7++;
                                break;
                            case '4':
                                $categoria = "de 0 a 10 dias";
                                $cat5++;
                                break;
                            case '5':
                                $categoria = "de 11 a 30 dias";
                                $cat2++;
                                break;
                            case '6':
                                $categoria = "de 11 a 30 dias";
                                $cat0++;
                                break;
                        }

                        $dataAbertura = MainController::parse_timestamp_static($dados['data_abertura']);

                        $interval = date_diff(new DateTime($dataAbertura), new DateTime());

                        echo("<tr style='border: solid;'>");
                        echo("<td style='border: solid;'>{$dados['cod_saf']}</td>");
                        echo("<td style='border: solid;'>{$dataAbertura}</td>");
                        echo("<td style='border: solid;'>{$dados['nome_grupo']}</td>");
                        echo("<td style='border: solid;'>{$dados['local']}</td>");
                        echo("<td style='border: solid;'>{$dados['nome_avaria']}</td>");
                        echo("<td style='border: solid;'>{$dados['nivel']}</td>");
                        echo("<td style='border: solid;'>{$interval->format('%a')}</td>");
                        echo("<td style='border: solid;'>{$categoria}</td>");
                        echo("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php

echo "<input type='hidden' name='cat0' value='{$cat0}'/>";
echo "<input type='hidden' name='cat2' value='{$cat2}'/>";
echo "<input type='hidden' name='cat5' value='{$cat5}'/>";
echo "<input type='hidden' name='cat7' value='{$cat7}'/>";
echo "<input type='hidden' name='cat30' value='{$cat30}'/>";
echo "<input type='hidden' name='cat60' value='{$cat60}'/>";

$this->dashboard->modalExibirSaf();
?>