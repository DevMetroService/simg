<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

if($_POST) {
    $where = returnWhere($_POST);


    if(!empty($where))
        $where = " AND ". $where;

    $sql = "SELECT 
	count(*) as reprovacoes,
	(
		SELECT count(*) 
		FROM status_ssm s
		JOIN v_ssm vs USING(cod_ssm)
		WHERE s.cod_status = 16 {$where}
	) as validacoes

    FROM status_ssm s
    JOIN v_ssm vs USING(cod_ssm)
    WHERE s.cod_status = 36 {$where}";

    $quantidade = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC)[0];

    $sql = "SELECT 
    cod_ssm, cod_saf, nome_grupo, nome_linha, descricao_trecho, data_abertura, nome_avaria, nome_servico, count(*) as qtd
    FROM status_ssm s
    JOIN v_ssm USING(cod_ssm)
    WHERE s.cod_status = 36 {$where}
    GROUP BY cod_ssm, cod_saf, nome_grupo, nome_linha, descricao_trecho, data_abertura, nome_avaria, nome_servico";

    $listaReprovados = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    $sql = "SELECT 
    cod_ssm, cod_saf, nome_grupo, nome_linha, descricao_trecho, data_abertura, nome_avaria, nome_servico
    FROM status_ssm s
    JOIN v_ssm USING(cod_ssm)
    WHERE s.cod_status = 16 {$where}
    GROUP BY cod_ssm, cod_saf, nome_grupo, nome_linha, descricao_trecho, data_abertura, nome_avaria, nome_servico";

    $listaAprovados = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
}

?>

<div class="row" xmlns="http://www.w3.org/1999/html">
    <form id="form_rel" method="post">
        <div class="col-md-12 hidden-print">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>
                        <i class="fa fa-pie-chart fa-fw"></i>
                        <strong>
                            Relat�rio de Valida��o SSM
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="container"
                                 <?php
                                 echo (!empty($quantidade)) ? "data-validada='{$quantidade['validacoes']}' data-reprovada='{$quantidade['reprovacoes']}'" : ""
                                 ?>
                                 ></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if ($_POST['linha'] == $value['cod_linha']) {
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Trecho</label>
                                                    <select class="form-control" name="trecho">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        if ($_POST['linha']) {
                                                            $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$_POST['linha']]);

                                                            foreach ($selectTrecho as $dados => $value) {
                                                                if ($_POST['trecho'] == $value['cod_trecho'])
                                                                    echo("<option value='{$value['cod_trecho']}' selected>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_trecho']}'>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Data (a Partir)</label>
                                                    <input class="form-control data validaData" name="dataPartirDatAber"
                                                           <?php echo $_POST['dataPartirDatAber'] != '' ?
                                                               "value='{$_POST['dataPartirDatAber']}'" : "value=''"
                                                           ?>
                                                           type="text">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Data (At�)</label>
                                                    <input class="form-control data validaData" name="dataRetrocederDatAber"
                                                           <?php echo $_POST['dataRetrocederDatAber'] != '' ?
                                                               "value='{$_POST['dataRetrocederDatAber']}'" : "value=''"
                                                           ?>
                                                           type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupoSistema" class="form-control">
                                                        <option value="">Grupos de Sistemas</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'],["ORDER" => "nome_grupo"]);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($_POST['grupoSistema'] == $value['cod_grupo']) {
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if ($_POST['grupoSistema']) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if ($_POST['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if ($_POST['sistema']) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-large btn-block">
                                        <i class="fa fa-refresh"></i> Atualizar Gr�fico
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="reset" class="limpaFiltro btn btn-success btn-large btn-block">
                                        <i class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-default btn-large btn-block btn_print_rel">
                                        <i class="fa fa-print"></i> Imprimir
                                    </button>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <table class="table table-bordered text-center">
                                <tr>
                                    <td id="tabCat10"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x" style="color: #4CAF50; "></i> - Validadas</td>
                                </tr>
                                <tr>
                                    <td id="tabCat50"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x" style="color: #FF5722; "></i> - N�o Validadas</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold" id="total"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x" style="color: #B0BEC5; "></i> - <strong>Total</strong></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel-body hidden-print">
                    <div class="col-md-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#Appgeral" aria-controls="home" role="tab" data-toggle="tab">N�o nmhjValidadas</a></li>
                            <li role="presentation"><a href="#AppSul" aria-controls="profile" role="tab" data-toggle="tab">Validadas</a></li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade in active" id="Appgeral">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableRel table table-bordered table-responsive">
                                        <thead>
                                        <tr>
                                            <th>SAF</th>
                                            <th>SSM</th>
                                            <th>GRUPO DE SISTEMAS</th>
                                            <th>LOCAL</th>
                                            <th>DATA DE ABERTURA</th>
                                            <th>AVARIA</th>
                                            <th>SERVI�O</th>
                                            <th>QTD. N�O VALIDADAS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        if(!empty($listaReprovados)) {
                                            foreach ($listaReprovados as $dados=>$value) {
                                                $dataAbertura = $this->parse_timestamp($value['data_abertura']);

                                                echo("<tr>");
                                                echo("<td>{$value['cod_ssm']}</td>");
                                                echo("<td>{$value['cod_saf']}</td>");
                                                echo("<td>{$value['nome_grupo']}</td>");
                                                echo("<td>{$value['nome_linha']}</br><small>{$value['descricao_trecho']}</small></td>");
                                                echo("<td>{$dataAbertura}</td>");
                                                echo("<td>{$value['nome_avaria']}</td>");
                                                echo("<td>{$value['nome_servico']}</td>");
                                                echo("<td>{$value['qtd']}</td>");
                                                echo("</tr>");
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="AppSul">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableRel table table-bordered table-responsive">
                                        <thead>
                                        <tr>
                                            <th>SAF</th>
                                            <th>SSM</th>
                                            <th>GRUPO DE SISTEMAS</th>
                                            <th>LOCAL</th>
                                            <th>DATA DE ABERTURA</th>
                                            <th>AVARIA</th>
                                            <th>SERVI�O</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($listaAprovados)) {
                                            foreach ($listaAprovados as $dados=>$value) {

                                                $dataAbertura = $this->parse_timestamp($value['data_abertura']);

                                                echo("<tr>");
                                                echo("<td>{$value['cod_ssm']}</td>");
                                                echo("<td>{$value['cod_saf']}</td>");
                                                echo("<td>{$value['nome_grupo']}</td>");
                                                echo("<td>{$value['nome_linha']} </br><small>{$value['descricao_trecho']}</small></td>");
                                                echo("<td>{$dataAbertura}</td>");
                                                echo("<td>{$value['nome_avaria']}</td>");
                                                echo("<td>{$value['nome_servico']}</td>");
                                                echo("</tr>");
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php


        echo "<input type='hidden' name='cat0' value='{$cat0}'/>";
        echo "<input type='hidden' name='cat10' value='{$cat10}'/>";
        echo "<input type='hidden' name='cat30' value='{$cat30}'/>";
        echo "<input type='hidden' name='cat50' value='{$cat50}'/>";
        echo "<input type='hidden' name='cat60' value='{$cat60}'/>";

        echo "<input type='hidden' name='tituloGraf' value=''/>";

        $this->dashboard->modalExibirSaf();
        ?>
    </form>


</div>