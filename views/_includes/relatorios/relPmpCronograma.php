<?php
if(!empty($_POST['pmp'])){
    $dadosFormulario['pmp'] = $_POST['pmp'];
    $dadosFormulario['ano'] = $_POST['ano'];

    $sqlPmp = "SELECT cod_sistema, cod_subsistema, cod_servico_pmp, cod_local, cod_grupo, 
                    0 AS cod_estacao_inicial, 0 AS cod_estacao_final, 0 AS cod_amv, 0 AS km_inicial, 0 AS km_final, cod_linha
                      FROM pmp
                        JOIN pmp_edificacao USING (cod_pmp)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN local USING (cod_local)
                        where cod_pmp = {$_POST['pmp']}";
    $result = $this->medoo->query($sqlPmp)->fetchAll(PDO::FETCH_ASSOC);

    if(!$result[0]){
        $sqlPmp = "SELECT cod_sistema, cod_subsistema, cod_servico_pmp, cod_local, cod_grupo, 
                    0 AS cod_estacao_inicial, 0 AS cod_estacao_final, 0 AS cod_amv, 0 AS km_inicial, 0 AS km_final, cod_linha
                      FROM pmp
                        JOIN pmp_subestacao USING (cod_pmp)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN local USING (cod_local)
                        where cod_pmp = {$_POST['pmp']}";
        $result = $this->medoo->query($sqlPmp)->fetchAll(PDO::FETCH_ASSOC);

        if(!$result[0]){
            $sqlPmp = "SELECT cod_sistema, cod_subsistema, cod_servico_pmp, cod_estacao_inicial, cod_estacao_final, cod_amv, km_inicial, km_final, cod_grupo, '' AS cod_local, cod_linha
                      FROM pmp
                        JOIN pmp_via_permanente USING (cod_pmp)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN estacao ON cod_estacao_inicial = estacao.cod_estacao
                        where cod_pmp = {$_POST['pmp']}";
            $result = $this->medoo->query($sqlPmp)->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    $resultPmp = null;
    if(!empty($result)){
        $resultPmp = $result[0];
    }

    $dadosFormulario['grupoSistema'] = $resultPmp['cod_grupo'];
    $dadosFormulario['sistema'] = $resultPmp['cod_sistema'];
    $dadosFormulario['subSistema'] = $resultPmp['cod_subsistema'];
    $dadosFormulario['servicoPmp'] = $resultPmp['cod_servico_pmp'];
    $dadosFormulario['linha'] = $resultPmp['cod_linha'];
    $dadosFormulario['local'] = $resultPmp['cod_local'];
    $dadosFormulario['estacaoInicial'] = $resultPmp['cod_estacao_inicial'];
    $dadosFormulario['estacaoFinal'] = $resultPmp['cod_estacao_final'];
    $dadosFormulario['kmInicial'] = $resultPmp['km_inicial'];
    $dadosFormulario['kmFinal'] = $resultPmp['km_final'];
    $dadosFormulario['amv'] = $resultPmp['cod_amv'];
}else{
    $dadosFormulario = $_POST;
}

switch ($dadosFormulario['grupoSistema']){
    case 21:
        $siglaGrupo = "E";
        $where = "cod_sistema = {$dadosFormulario['sistema']} AND cod_subsistema = {$dadosFormulario['subSistema']} AND cod_servico_pmp = {$dadosFormulario['servicoPmp']} AND cod_local = {$dadosFormulario['local']} AND ano = {$dadosFormulario['ano']}";
        $tabela = "edificacao";
        $nomeGrupo = "Edifica��o";
        $siglaTabela = "ed";
        break;
    case 24:
        if(!$dadosFormulario['amv'])
            $dadosFormulario['amv'] = 'null';

        $siglaGrupo = "V";
        $where = "cod_sistema = {$dadosFormulario['sistema']} AND cod_subsistema = {$dadosFormulario['subSistema']} AND cod_servico_pmp = {$dadosFormulario['servicoPmp']}
                    AND cod_estacao_inicial = {$dadosFormulario['estacaoInicial']} AND cod_estacao_final = {$dadosFormulario['estacaoFinal']} 
                    AND km_inicial = {$dadosFormulario['kmInicial']} AND km_final = {$dadosFormulario['kmFinal']} AND cod_amv = {$dadosFormulario['amv']} AND ano = {$dadosFormulario['ano']}";
        $tabela = "via_permanente";
        $nomeGrupo = "Via Permanente";
        $siglaTabela = "vp";
        break;
    case 25:
        $siglaGrupo = "S";
        $where = "cod_sistema = {$dadosFormulario['sistema']} AND cod_subsistema = {$dadosFormulario['subSistema']} AND cod_servico_pmp = {$dadosFormulario['servicoPmp']} AND cod_local = {$dadosFormulario['local']} AND ano = {$dadosFormulario['ano']}";
        $tabela = "subestacao";
        $nomeGrupo = "Subesta��o";
        $siglaTabela = "su";
        break;
    case 20:
        $siglaGrupo = "R";
        $dadosFormulario['grupoSistema'] = null;
        break;
}
if(!empty($dadosFormulario['grupoSistema'])) {
    $sql = "SELECT cp.cod_cronograma_pmp AS cod_cron, cod_pmp, p.quinzena AS quinzena_inicial, c.quinzena, c.ano, nome_periodicidade, nome_status, '{$nomeGrupo}' AS tabela
                      FROM cronograma_pmp cp
                        JOIN pmp p USING (cod_pmp)
                        JOIN pmp_{$tabela} USING (cod_pmp)
                        JOIN cronograma c USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)
                        WHERE {$where}
                        ORDER BY c.quinzena";

    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
}
?>

<div class="row hidden-print">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>
                    <i class="fa fa-pie-chart fa-fw"></i>
                    <strong>
                        Pmp / Cronograma por servi�o
                    </strong>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" id="formRel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>C�digo do PMP</label>
                                                    <input name="pmp" class="form-control number" value="<?php echo $dadosFormulario['pmp'];?>" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Ano</label>
                                                    <select class="form-control" name="ano">
                                                        <?php
                                                        $op1 = date('Y') - 5;
                                                        echo ($dadosFormulario['ano'] == $op1) ? "<option selected>$op1</option>" : "<option>$op1</option>";
                                                        $op2 = date('Y') - 4;
                                                        echo ($dadosFormulario['ano'] == $op2) ? "<option selected>$op2</option>" : "<option>$op2</option>";
                                                        $op3 = date('Y') - 3;
                                                        echo ($dadosFormulario['ano'] == $op3) ? "<option selected>$op3</option>" : "<option>$op3</option>";
                                                        $op4 = date('Y') - 2;
                                                        echo ($dadosFormulario['ano'] == $op4) ? "<option selected>$op4</option>" : "<option>$op4</option>";
                                                        $op5 = date('Y') - 1;
                                                        echo ($dadosFormulario['ano'] == $op5) ? "<option selected>$op5</option>" : "<option>$op5</option>";
                                                        $op6 = date('Y');
                                                        echo (empty($dadosFormulario['ano']) || $dadosFormulario['ano'] == $op6) ? "<option selected>$op6</option>" : "<option>$op6</option>";
                                                        $op7 = date('Y') + 1;
                                                        echo ($dadosFormulario['ano'] == $op7) ? "<option selected>$op7</option>" : "<option>$op7</option>";
                                                        $op8 = date('Y') + 2;
                                                        echo ($dadosFormulario['ano'] == $op8) ? "<option selected>$op8</option>" : "<option>$op8</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupoSistema" class="form-control" required>
                                                        <option value="">Grupos de Sistemas</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_grupo[!]" => [22, 23, 26, 29]]]);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($dadosFormulario['grupoSistema'] == $value['cod_grupo']) {
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control" required>
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if ($dadosFormulario['grupoSistema']) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $dadosFormulario['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if ($dadosFormulario['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control" required>
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if ($dadosFormulario['sistema']) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $dadosFormulario['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($dadosFormulario['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Servi�o</label>
                                                    <select name="servicoPmp" class="form-control" required>
                                                        <option value="">Servi�o</option>
                                                        <?php
                                                        if ($dadosFormulario['subSistema']) {
                                                            $sql = "SELECT sp.nome_servico_pmp, sp.cod_servico_pmp
                                                                          FROM servico_pmp_sub_sistema sss 
                                                                          inner join servico_pmp sp on sss.cod_servico_pmp = sp.cod_servico_pmp
                                                                          inner join sub_sistema ss on sss.cod_sub_sistema = ss.cod_sub_sistema
                                                                          WHERE ss.cod_subsistema = {$dadosFormulario['subSistema']} AND ss.cod_sistema = {$dadosFormulario['sistema']}";
                                                            $servicoPmp = $this->medoo->query($sql);

                                                            foreach ($servicoPmp as $dados => $value) {
                                                                if ($dadosFormulario['servicoPmp'] == $value['cod_servico_pmp']) {
                                                                    echo("<option selected value='{$value['cod_servico_pmp']}'>{$value['nome_servico_pmp']}</option>");
                                                                } else {
                                                                    echo("<option value='{$value['cod_servico_pmp']}'>{$value['nome_servico_pmp']}</option>");
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha" required>
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if ($dadosFormulario['linha'] == $value['cod_linha']) {
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 local">
                                                    <label>Local</label>
                                                    <select class="form-control" name="local">
                                                        <option value="">Local</option>
                                                        <?php
                                                        if ($dadosFormulario['grupoSistema'] && $dadosFormulario['linha']) {
                                                            $selectTrecho = $this->medoo->select("local", ['cod_local', 'nome_local'], ["AND" => ["grupo" => $siglaGrupo, "cod_linha" => $dadosFormulario['linha']],"ORDER" => "nome_local"]);

                                                            foreach ($selectTrecho as $dados => $value) {
                                                                if ($dadosFormulario['local'] == $value['cod_local'])
                                                                    echo("<option value='{$value['cod_local']}' selected>{$value["nome_local"]}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_local']}'>{$value["nome_local"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 conjuntoVp">
                                                    <label>Esta��o Inicial</label>
                                                    <select class="form-control conjuntoVp" name="estacaoInicial">
                                                        <option value="">Esta��o Inicial</option>
                                                        <?php
                                                        if ($dadosFormulario['linha']) {
                                                            $selectEi = $this->medoo->select("estacao", ['cod_estacao', 'nome_estacao'],["cod_linha" => $dadosFormulario['linha'], "ORDER" => "nome_estacao"]);

                                                            foreach ($selectEi as $dados => $value) {
                                                                if ($dadosFormulario['estacaoInicial'] == $value['cod_estacao'])
                                                                    echo("<option value='{$value['cod_estacao']}' selected>{$value["nome_estacao"]}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_estacao']}'>{$value["nome_estacao"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 conjuntoVp">
                                                    <label>Esta��o Final</label>
                                                    <select class="form-control conjuntoVp" name="estacaoFinal">
                                                        <option value="">Esta��o Final</option>
                                                        <?php
                                                        if ($dadosFormulario['linha']) {
                                                            $selectEf = $this->medoo->select("estacao", ['cod_estacao', 'nome_estacao'],["cod_linha" => $dadosFormulario['linha'], "ORDER" => "nome_estacao"]);

                                                            foreach ($selectEf as $dados => $value) {
                                                                if ($dadosFormulario['estacaoFinal'] == $value['cod_estacao'])
                                                                    echo("<option value='{$value['cod_estacao']}' selected>{$value["nome_estacao"]}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_estacao']}'>{$value["nome_estacao"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row conjuntoVp">
                                                <div class="col-md-4">
                                                    <label>Km Inicial</label>
                                                    <input class="form-control conjuntoVp" name="kmInicial" value="<?php echo $dadosFormulario['kmInicial'];?>" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Km Final</label>
                                                    <input class="form-control conjuntoVp" name="kmFinal" value="<?php echo $dadosFormulario['kmFinal'];?>" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label>AMV</label>
                                                    <select class="form-control conjuntoVp" name="amv">
                                                        <option value=""></option>
                                                        <?php
                                                        if ($dadosFormulario['estacaoInicial']) {
                                                            $selectVia = $this->medoo->select("amv", ['cod_amv', 'nome_amv'],["cod_estacao" => $dadosFormulario['estacaoInicial'], "ORDER" => "nome_amv"]);

                                                            foreach ($selectVia as $dados => $value) {
                                                                if ($dadosFormulario['amv'] == $value['cod_amv'])
                                                                    echo("<option value='{$value['cod_amv']}' selected>{$value["nome_amv"]}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_amv']}'>{$value["nome_amv"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-3 col-md-3">
                                    <button type="submit" class="btn btn-primary btn-large btn-block"><i
                                            class="fa fa-refresh"></i> Atualizar Tabela
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                            class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="col-md-12">
                    <table class="tableRel table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>Cod Cronograma</th>
                            <th>Cod PMP</th>
                            <th>Quinzena Inicial</th>
                            <th>Quinzena Cronograma</th>
                            <th>Ano</th>
                            <th>Periodicidade</th>
                            <th>Status</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($result)) {
                            foreach ($result as $dados) {
                                echo("<tr>");
                                echo("<td>{$dados['cod_cron']}<input type='hidden' value='{$dados['tabela']}' ></td>");
                                echo("<td>{$dados['cod_pmp']}</td>");
                                echo("<td>{$dados['quinzena_inicial']}</td>");
                                echo("<td style='background-color: #cacaca'>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['ano']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo("<td>{$dados['nome_status']}</td>");
                                echo("<td>");
                                echo("<button type='button' class='abrirModalCronograma btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirCronograma'><i class='fa fa-eye'></i></button>");
                                echo("</td>");
                                echo("</tr>");
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->dashboard->modalExibirCronograma();
?>