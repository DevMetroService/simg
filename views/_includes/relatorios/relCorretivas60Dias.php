<?php
require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);

$sqlOrder = " GROUP BY  cod_saf, data_abertura, nome_grupo, nome_linha, descricao_trecho, nome_avaria, nivel ORDER BY data_abertura";

if ($where) {
    $where = " AND " . $where . $sqlOrder;
} else {
    $where = $sqlOrder;
}

$sql = "SELECT cod_saf, data_abertura, nome_grupo, nome_linha || ' - ' || descricao_trecho AS local, nome_avaria, nivel,
date_part('day', SUM(COALESCE(data_termino_status, CURRENT_TIMESTAMP) - ss.data_status) ) AS diasEmAberto,
(CASE
	WHEN date_part('day', SUM(COALESCE(data_termino_status, CURRENT_TIMESTAMP) - ss.data_status) ) < 10 THEN '1'
	WHEN date_part('day', SUM(COALESCE(data_termino_status, CURRENT_TIMESTAMP) - ss.data_status) ) < 20 THEN '2'
 	WHEN date_part('day', SUM(COALESCE(data_termino_status, CURRENT_TIMESTAMP) - ss.data_status) ) < 40 THEN '3'
	WHEN date_part('day', SUM(COALESCE(data_termino_status, CURRENT_TIMESTAMP) - ss.data_status) ) < 60 THEN '5'
	ELSE '4'
END) AS categoria

FROM status_saf ss
JOIN v_saf vsaf USING(cod_saf) 
WHERE vsaf.cod_status NOT IN (2,3,14,27, 37) {$where}";

// 2  - Cancelada
// 3  - Devolvida
// 14 - Encerrada
// 27 - Programada
// 37 - Aguardando Aprova��o

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$cat0 = 0;
$cat10 = 0;
$cat30 = 0;
$cat50 = 0;
$cat60 = 0;
?>

<div class="row" xmlns="http://www.w3.org/1999/html">
    <form id="form_rel" method="post">
        <div class="col-md-12 hidden-print">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3>
                        <i class="fa fa-pie-chart fa-fw"></i>
                        <strong>
                            SAF's acima de 60 dias
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="container"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Linha</label>
                                                    <select class="form-control" name="linha">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                        foreach ($selectLinha as $dados => $value) {
                                                            if ($_POST['linha'] == $value['cod_linha']) {
                                                                echo("<option selected value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Trecho</label>
                                                    <select class="form-control" name="trecho">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        if ($_POST['linha']) {
                                                            $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$_POST['linha']]);

                                                            foreach ($selectTrecho as $dados => $value) {
                                                                if ($_POST['trecho'] == $value['cod_trecho'])
                                                                    echo("<option value='{$value['cod_trecho']}' selected>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_trecho']}'>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Ponto Not�vel</label>
                                                    <select class="form-control" name="pn">
                                                        <option value="">Todos</option>
                                                        <?php
                                                        if ($_POST['trecho']) {
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["cod_trecho" => (int)$_POST['trecho']]);

                                                            foreach ($selectPn as $dados => $value) {
                                                                if ($_POST['pn'] == $value['cod_ponto_notavel'])
                                                                    echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Grupo de Sistema</label>
                                                    <select name="grupoSistema" class="form-control">
                                                        <option value="">Grupos de Sistemas</option>
                                                        <?php
                                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'],["ORDER" => "nome_grupo"]);

                                                        foreach ($grupoSistema as $dados => $value) {
                                                            if ($_POST['grupoSistema'] == $value['cod_grupo']) {
                                                                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            } else {
                                                                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sistema</label>
                                                    <select name="sistema" class="form-control">
                                                        <option value="">Sistemas</option>
                                                        <?php
                                                        if ($_POST['grupoSistema']) {
                                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupoSistema']]);

                                                            foreach ($sistema as $dados => $value) {
                                                                if ($_POST['sistema'] == $value['cod_sistema'])
                                                                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Sub-Sistema</label>
                                                    <select name="subSistema" class="form-control">
                                                        <option value="">Sub-Sistemas</option>
                                                        <?php
                                                        if ($_POST['sistema']) {
                                                            $subSistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $_POST['sistema']]);

                                                            foreach ($subSistema as $dados => $value) {
                                                                if ($_POST['subSistema'] == $value['cod_subsistema'])
                                                                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                                else
                                                                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary btn-large btn-block">
                                        <i class="fa fa-refresh"></i> Atualizar Gr�fico
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="limpaFiltro btn btn-success btn-large btn-block">
                                        <i class="fa fa-reply-all"></i> Limpar Filtros
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-default btn-large btn-block btn_print_rel">
                                        <i class="fa fa-print"></i> Imprimir
                                    </button>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered text-center">
                                <tr>
                                    <td id="tabCat0"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #0b97c4; "></i> - de 0 a 10 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat10"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #4CAF50; "></i> - de 11 a 20 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat30"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #FFEB3B; "></i> - de 21 a 40 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat50"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #FF5722; "></i> - de 41 a 60 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tabCat60"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #D50000; "></i> - Acima de 60 dias
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold" id="total"></td>
                                    <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                                    style="color: #B0BEC5; "></i> -
                                        <strong>Total</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel-body hidden-print">
                    <div class="col-md-12">
                        <table class="tableRel table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Cod SAF</th>
                                <th>Data de Abertura</th>
                                <th>Grupo</th>
                                <th>Local</th>
                                <th>Avaria</th>
                                <th>N�vel</th>
                                <th>Dias em Aberto</th>
                                <th>Categoria</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($result as $dados) {
                                switch ($dados['categoria']) {
                                    case '1':
                                        $categoria = "Acima de 60 dias";
                                        $cat60++;
                                        break;
                                    case '2':
                                        $categoria = "de 41 a 60 dias";
                                        $cat50++;
                                        break;
                                    case '3':
                                        $categoria = "de 21 a 40 dias";
                                        $cat30++;
                                        break;
                                    case '4':
                                        $categoria = "de 0 a 10 dias";
                                        $cat0++;
                                        break;
                                    case '5':
                                        $categoria = "de 11 a 20 dias";
                                        $cat10++;
                                        break;
                                    default:
                                        $categoria = "N�o Categorizado";
                                }

                                $dataAbertura = MainController::parse_timestamp_date($dados['data_abertura']);

                                echo("<tr>");
                                echo("<td>{$dados['cod_saf']}</td>");
                                echo("<td>{$dataAbertura}</td>");
                                echo("<td>{$dados['nome_grupo']}</td>");
                                echo("<td>{$dados['local']}</td>");
                                echo("<td>{$dados['nome_avaria']}</td>");
                                echo("<td>{$dados['nivel']}</td>");
                                echo("<td>{$dados['diasemaberto']}</td>");
                                echo("<td>{$categoria}</td>");
                                echo("<td>");
                                echo("<button type='button' class='abrirModalSaf btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirSaf'><i class='fa fa-eye'></i></button>");
                                echo("</td>");
                                echo("</tr>");
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php

        echo "<input type='hidden' name='cat0' value='{$cat0}'/>";
        echo "<input type='hidden' name='cat10' value='{$cat10}'/>";
        echo "<input type='hidden' name='cat30' value='{$cat30}'/>";
        echo "<input type='hidden' name='cat50' value='{$cat50}'/>";
        echo "<input type='hidden' name='cat60' value='{$cat60}'/>";

        echo "<input type='hidden' name='tituloGraf' value=''/>";

        $this->dashboard->modalExibirSaf();
        ?>
    </form>

</div>