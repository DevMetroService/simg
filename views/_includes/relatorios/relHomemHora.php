<?php

ini_set('memory_limit','1024M');

require_once(ABSPATH . '/functions/functionsRelatorio.php');

$join;

switch ($_POST['grupoSistema']) {
    case '21':
        $join = 'JOIN osmp_ed ON osmp.cod_osmp = osmp_ed.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_ed USING (cod_ssmp)
                 JOIN pmp_edificacao USING (cod_pmp_edificacao)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ';
        break;
    case '24':
        $join = 'JOIN osmp_vp ON osmp.cod_osmp = osmp_vp.cod_osmp
                 JOIN estacao ON osmp_vp.cod_estacao_inicial = estacao.cod_estacao
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_vp USING (cod_ssmp)
                 JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ';
        break;
    case '20':
        $join = 'JOIN osmp_ra ON osmp.cod_osmp = osmp_ra.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_ra USING (cod_ssmp)
                 JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ';
        break;
    case '25':
        $join = 'JOIN osmp_su ON osmp.cod_osmp = osmp_su.cod_osmp
                 JOIN local USING (cod_local)
                 JOIN linha USING (cod_linha)
                 JOIN ssmp_su USING (cod_ssmp)
                 JOIN pmp_subestacao USING (cod_pmp_subestacao)
                 JOIN pmp USING (cod_pmp)
                 JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                 JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                 JOIN servico_pmp USING (cod_servico_pmp)
                 JOIN sub_sistema USING (cod_sub_sistema) ';
        break;
    case '27':
        $join = 'JOIN osmp_te ON osmp.cod_osmp = osmp_te.cod_osmp
                    JOIN local USING (cod_local)
                    JOIN linha USING (cod_linha)
                    JOIN ssmp_te USING (cod_ssmp)
                    JOIN pmp_telecom USING (cod_pmp_telecom)
                    JOIN pmp USING (cod_pmp)
                    JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                    JOIN servico_pmp USING (cod_servico_pmp)
                    JOIN sub_sistema USING (cod_sub_sistema) ';
        break;
    case '28':
        $join = 'JOIN osmp_bi ON osmp.cod_osmp = osmp_bi.cod_osmp
                    JOIN estacao USING (cod_estacao)
                    JOIN linha USING (cod_linha)
                    JOIN ssmp_bi USING (cod_ssmp)
                    JOIN pmp_bilhetagem USING (cod_pmp_bilhetagem)
                    JOIN pmp USING (cod_pmp)
                    JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                    JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                    JOIN servico_pmp USING (cod_servico_pmp)
                    JOIN sub_sistema USING (cod_sub_sistema) ';
        break;
}

$where = returnWhere($_POST);

if ( $_POST['funcionario']) {
    $whereFuncionario = implode(', ', $_POST['funcionario']);
}

$subTitulo = '';
echo("<input type='hidden' id='tituloChart' value='{$_POST['form']}'>");
if ($_POST['form'] == 'osm')
    $subTitulo .= 'OSM';

if ($_POST['form'] == 'osp')
    $subTitulo .= 'OSP';

if ($_POST['form'] == 'osmp')
    $subTitulo .= 'OSMP';

if ($_POST['form'] == 'todos') {
    $subTitulo .= 'OSM / OSP / OSMP';
}

if ($whereFuncionario == '') {
    if ($where != "") {
        $where = ' WHERE ' . $where;
    }
} else {
    if ($where != "") {
        $where = ' WHERE f.cod_funcionario IN(' . $whereFuncionario . ') AND ' . $where;
    } else {
        $where = ' WHERE f.cod_funcionario IN(' . $whereFuncionario . ')';
    }
}


$txtNome = [];
$txtHh = [];
$txtHorario = [];

$sql = '';

switch ($_POST['form']) {
    case "osm":
        $sql = 'SELECT f.cod_funcionario, nome_funcionario, total_hrs, funcionario_empresa.matricula FROM osm_mao_de_obra
                    JOIN osm_falha osm USING(cod_osm)
                    JOIN funcionario f USING(cod_funcionario)
                    JOIN funcionario_empresa USING(cod_funcionario_empresa)
                    JOIN status_osm sto USING (cod_ostatus)
                    JOIN status USING (cod_status)
                    JOIN osm_servico os ON os.cod_osm = osm.cod_osm
                    JOIN osm_registro oreg ON oreg.cod_osm = osm.cod_osm
                    JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                    JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                    JOIN grupo ON osm.grupo_atuado = grupo.cod_grupo
                    LEFT JOIN material_rodante_osm USING (cod_veiculo)' . $where;
        break;
    case "osp":
        $sql = 'SELECT f.cod_funcionario, nome_funcionario, total_hrs, funcionario_empresa.matricula  
                FROM osp_mao_de_obra
                JOIN osp USING(cod_osp)
                JOIN funcionario f USING(cod_funcionario)
                JOIN funcionario_empresa USING(cod_funcionario_empresa)
                JOIN status_osp sto USING (cod_ospstatus)
                JOIN status USING (cod_status)
                JOIN osp_servico os ON os.cod_osp = osp.cod_osp
                JOIN osp_registro oreg ON oreg.cod_osp = osp.cod_osp
                JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                JOIN ssp USING (cod_ssp)
          
                JOIN grupo ON osp.grupo_atuado = grupo.cod_grupo' . $where;
        break;
    case "osmp":
        $sql = 'SELECT f.cod_funcionario, nome_funcionario, total_hrs, funcionario_empresa.matricula FROM osmp_mao_de_obra
                    JOIN osmp USING (cod_osmp)
                    JOIN funcionario f USING(cod_funcionario)
                    JOIN funcionario_empresa USING(cod_funcionario_empresa)
                    JOIN status_osmp sto USING (cod_status_osmp)
                    JOIN status USING (cod_status)
                    JOIN osmp_registro oreg ON osmp.cod_osmp = oreg.cod_osmp
                    JOIN un_equipe USING (cod_un_equipe)
                    JOIN equipe USING (cod_equipe)' . $join . $where;
        break;
    case "todos":

        if ($_POST['grupoSistema']) {

            $whereGrupo = "grupo.cod_grupo = {$_POST['grupoSistema']}";

            if ($_POST['sistema']) {
                $whereGrupo .= " AND sistema_atuado = {$_POST['sistema']}";
            }

            if ($_POST['subSistema']) {
                $whereGrupo .= " AND subsistema_atuado = {$_POST['subSistema']}";
            }

            if ($_POST['linha']) {
                $whereGrupo .= " AND cod_linha_atuado = {$_POST['linha']}";
            }

            if ($whereFuncionario == '') {
                if ($whereGrupo != "") {
                    $whereGrupo = ' WHERE ' . $whereGrupo;
                }
            } else {
                if ($whereGrupo != "") {
                    $whereGrupo = ' WHERE f.cod_funcionario IN(' . $whereFuncionario . ') AND ' . $whereGrupo;
                } else {
                    $whereGrupo = ' WHERE f.cod_funcionario IN(' . $whereFuncionario . ')';
                }
            }

            $sql = "SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osm_mao_de_obra
                  JOIN osm_falha o USING (cod_osm)
                  JOIN funcionario f USING(cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osm sto USING (cod_ostatus)
                  JOIN status USING (cod_status)
                  JOIN osm_servico os ON os.cod_osm = o.cod_osm
                  JOIN osm_registro oreg ON oreg.cod_osm = o.cod_osm
                  JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                  JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                  JOIN grupo ON o.grupo_atuado = grupo.cod_grupo " . $whereGrupo;

            $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osp_mao_de_obra
                  JOIN osp o USING (cod_osp)
                  JOIN funcionario f USING(cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osp sto USING (cod_ospstatus)
                  JOIN status USING (cod_status)
                  JOIN osp_servico os ON os.cod_osp = o.cod_osp
                  JOIN osp_registro oreg ON oreg.cod_osp = o.cod_osp
                  JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                  JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                  JOIN ssp USING (cod_ssp)
            
                  JOIN grupo ON o.grupo_atuado = grupo.cod_grupo " . $whereGrupo;

            switch ($_POST['grupoSistema']) {
                case '21':
                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario USING(cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_ed ON o.cod_osmp = osmp_ed.cod_osmp
                  JOIN local USING (cod_local)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_ed USING (cod_ssmp)
                  JOIN pmp_edificacao USING (cod_pmp_edificacao)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;
                    break;
                case '24':
                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_vp ON o.cod_osmp = osmp_vp.cod_osmp
                  JOIN estacao ON osmp_vp.cod_estacao_inicial = estacao.cod_estacao
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_vp USING (cod_ssmp)
                  JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;
                    break;
                case '20':
                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_ra ON o.cod_osmp = osmp_ra.cod_osmp
                  JOIN local USING (cod_local)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_ra USING (cod_ssmp)
                  JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;
                    break;
                case '25':
                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_su ON o.cod_osmp = osmp_su.cod_osmp
                  JOIN local USING (cod_local)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_su USING (cod_ssmp)
                  JOIN pmp_subestacao USING (cod_pmp_subestacao)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;
                    break;
                default:
                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_ed ON o.cod_osmp = osmp_ed.cod_osmp
                  JOIN local USING (cod_local)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_ed USING (cod_ssmp)
                  JOIN pmp_edificacao USING (cod_pmp_edificacao)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;

                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_vp ON o.cod_osmp = osmp_vp.cod_osmp
                  JOIN estacao ON osmp_vp.cod_estacao_inicial = estacao.cod_estacao
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_vp USING (cod_ssmp)
                  JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;

                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_ra ON o.cod_osmp = osmp_ra.cod_osmp
                  JOIN local USING (cod_local)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_ra USING (cod_ssmp)
                  JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;

                    $sql .= " UNION ALL
                
                SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osmp_mao_de_obra
                  JOIN osmp o USING (cod_osmp)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osmp sto USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN osmp_su ON o.cod_osmp = osmp_su.cod_osmp
                  JOIN local USING (cod_local)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_su USING (cod_ssmp)
                  JOIN pmp_subestacao USING (cod_pmp_subestacao)
                 JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema) " . $where;
                    break;
            }

        } else {

            $sql = "SELECT
                  f.cod_funcionario,
                  nome_funcionario,
                  total_hrs,
                  funcionario_empresa.matricula
                FROM osm_mao_de_obra
                  JOIN osm_falha o USING (cod_osm)
                  JOIN funcionario f USING (cod_funcionario)
                  JOIN funcionario_empresa USING(cod_funcionario_empresa)
                  JOIN status_osm sto USING (cod_ostatus)
                  JOIN status USING (cod_status)
                  JOIN osm_servico os ON os.cod_osm = o.cod_osm
                  JOIN osm_registro oreg ON oreg.cod_osm = o.cod_osm
                  JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                  JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                  JOIN grupo ON o.grupo_atuado = grupo.cod_grupo " . $where;

            $sql .= " UNION ALL
                
                SELECT f.cod_funcionario, nome_funcionario, total_hrs, funcionario_empresa.matricula
                FROM osp_mao_de_obra
                JOIN osp o USING (cod_osp)
                JOIN funcionario f USING (cod_funcionario)
                JOIN funcionario_empresa USING(cod_funcionario_empresa)
                JOIN status_osp sto USING (cod_ospstatus)
                JOIN status USING (cod_status)
                JOIN osp_servico os ON os.cod_osp = o.cod_osp
                JOIN osp_registro oreg ON oreg.cod_osp = o.cod_osp
                JOIN un_equipe ON un_equipe.cod_un_equipe = oreg.cod_un_equipe
                JOIN equipe ON un_equipe.cod_equipe = equipe.cod_equipe
                JOIN ssp USING (cod_ssp)
          
                JOIN grupo ON o.grupo_atuado = grupo.cod_grupo " . $where;

            $sql .= " UNION ALL
            
               SELECT f.cod_funcionario, nome_funcionario, total_hrs, funcionario_empresa.matricula
               FROM osmp_mao_de_obra
               JOIN osmp o USING (cod_osmp)
               JOIN funcionario f USING (cod_funcionario)
                JOIN funcionario_empresa USING(cod_funcionario_empresa)
               JOIN status_osmp sto USING (cod_status_osmp)
               JOIN status USING (cod_status)
               JOIN osmp_registro oreg ON o.cod_osmp = oreg.cod_osmp
               JOIN un_equipe USING (cod_un_equipe)
               JOIN equipe USING (cod_equipe) " . $where;

        }
        break;

}

$sql .= ' ORDER BY nome_funcionario';

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$whereFuncionario = explode(',', $whereFuncionario);

if ($result) {

    $funcionario = array();

    foreach ($result as $dados) {
        $funcionario[$dados['cod_funcionario']]['nome'] = $dados['nome_funcionario'];
        $funcionario[$dados['cod_funcionario']]['cod_funcionario'] = $dados['cod_funcionario'];
        $funcionario[$dados['cod_funcionario']]['matricula'] = $dados['matricula'];
        $funcionario[$dados['cod_funcionario']]['quant'] += 1;

        $hora = (int)substr($dados['total_hrs'], 0, 2);
        $mint = (int)substr($dados['total_hrs'], 3, 2);

        $hh = ($hora * 60) + $mint;

        $funcionario[$dados['cod_funcionario']]['hh'] += $hh;
        $funcionario[$dados['cod_funcionario']]['hhhh'] .= ' - ' . $dados['total_hrs'];
    }

    if ($whereFuncionario[0]) {
        foreach ($whereFuncionario as $dados) {
            $listMatriculas = array_column($funcionario, 'cod_funcionario');
            if (!in_array($dados, $listMatriculas)) {
                $sql = "SELECT fe.matricula, f.nome_funcionario FROM funcionario f JOIN funcionario_empresa fe USING(cod_funcionario_empresa) WHERE f.cod_funcionario = {$dados}";
                $fun = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                $fun = $fun[0];

                $funcionario[$dados]['nome'] = $fun['nome_funcionario'];
                $funcionario[$dados]['matricula'] = $fun['matricula'];
                $funcionario[$dados]['cod_funcionario'] = $dados;
                $funcionario[$dados]['quant'] = 0;
                $funcionario[$dados]['hh'] = 0;
                $funcionario[$dados]['hhhh'] = ' - 00:00';
            }
        }
    }

    $txtNome = [];
    $txtHh = [];
    $table;

    foreach ($funcionario as $dados => $value) {
        $hora = (int)($value['hh'] / 60);
        $mint = (int)($value['hh'] % 60);

        if ($hora < 10) {
            $hora = '0' . $hora;
        }
        if ($mint < 10) {
            $mint = '0' . $mint;
        }

        if ($value['hh'] != 0 || $value['quant'] != 0) {
            $media = $value['hh'] / $value['quant'];
        } else {
            $media = 0;
        }

        $horaM = (int)($media / 60);
        $mintM = (int)($media % 60);

        if ($horaM < 10) {
            $horaM = '0' . $horaM;
        }
        if ($mintM < 10) {
            $mintM = '0' . $mintM;
        }

        $table .= " <tr>
                    <td>{$value['matricula']}</td>
                    <td>{$value['nome']}</td>
                    <td>{$hora}:{$mint}:00</td>
                    <td>{$value['quant']}</td>
                    <td>{$horaM}:{$mintM}:00</td>
                    <td class='hidden-print'><button class='btn btn-default btnDadosFuncionario' data-cod='{$value['cod_funcionario']}' data-toggle='modal' data-target='.maoObra'><i class='fa fa-eye fa-fw'></i></button></td>
                </tr>";

        $txtNome[] = $value['nome'];
        $txtHh[] = $value['hh'];
        $txtHorario[] = "{$hora}:{$mint}:00";
    }
}

?>
<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> Imprimir
            relat�rio
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="geralPdfRel">
    <div class="chartRel">
        <?php
        echo "<input value='{$subTitulo}' name='subTitulo' type='hidden'/>";

        $txtNome = array_chunk($txtNome, 10);
        $txtHh = array_chunk($txtHh, 10);
        $txtHorario = array_chunk($txtHorario, 10);

        for ($i = 0; $i < count($txtNome); $i++) {
            $stringNome = implode(',', $txtNome[$i]);
            $stringHh = implode(',', $txtHh[$i]);
            $stringHorario = implode(',', $txtHorario[$i]);

            $stringNome = $this->retiraAcentos($stringNome);

            echo("<div class='chartHH' id='c{$i}'>
                    <input value='c{$i}' name='id' type='hidden'/>
                    <input value='{$stringNome}' name='nome' type='hidden'/>
                    <input value='{$stringHh}' name='hh' type='hidden'/>
                    <input value='{$stringHorario}' name='horariosString' type='hidden'/>
                </div>");
        }
        ?>
    </div>

    <div>
        <table class="tableRel table table-bordered table-responsive">
            <thead>
            <tr>
                <th>Matr�cula</th>
                <th>Nome</th>
                <th>HH (h:m:s)</th>
                <th>Quant. OS</th>
                <th>M�dia</th>
                <th class="hidden-print">A��o</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($table != '') {
                echo $table;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade maoObra" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><label>Dados Funcionario</label>
                    <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="fa fa-close"></i></button>
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>Matricula</label>
                        <input type="text" id="matricula" readonly class="form-control"
                               value="<?php echo $dadosFunc['matricula'] ?>">

                    </div>
                    <div class="col-md-5">
                        <label>Nome Completo</label>
                        <input type="text" id="nome" value="" readonly class="form-control">

                    </div>
                    <div class="col-md-5">
                        <label>Fun��o</label>
                        <input id="funcao" readonly class="form-control" value=""/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>CPF</label>
                        <input type="text" id="cpfCnpj" readonly class="form-control cpfpj" value="">
                    </div>
                    <div class="col-md-4">
                        <label>Centro de Resultado</label>
                        <input class="form-control" readonly id="centroResultado" value=""/>
                    </div>
                    <div class="col-md-5">
                        <label>Lota��o</label>
                        <input class="form-control" readonly id="unidade" value=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>