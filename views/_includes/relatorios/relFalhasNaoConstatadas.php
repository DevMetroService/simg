<?php
/**
 * Created by PhpStorm.
 * User: abner.andrade
 * Date: 19/10/2017
 * Time: 12:55
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">

            <div class="panel-heading">
                <h3>Relat�rio de Falhas N�o Constatadas</h3>
            </div>
            <div class="panel-body">
                <form id="formRelFalhaNaoConstatada" method="post" action="">
                    
                    <!-- campos do form area-->
                    <div class="row">
                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <label>Filtro</label>
                            </div>


                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Linha</label>
                                        <?php
                                        $sql = $this->medoo->select("linha", "*");
                                        $this->form->getSelectLinha(null, $sql, "linha");
                                        ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Trecho</label>
                                        <select name="trecho" class="form-control">
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Nivel</label>
                                        <?php
                                        $this->form->getSelectNivel();
                                        ?>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Grupo Sistema</label>
                                        <?php
                                        $sql = $this->medoo->select("grupo", "*",["ORDER" => "nome_grupo"]);
                                        $this->form->getSelectGrupo(null, $sql, "grupo");
                                        ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Sistema</label>
                                        <select name="sistema" class="form-control">
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Sub-Sistema</label>
                                        <select name="subsistema" class="form-control">
                                        </select>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-3">
                                        <label>Data Abertura (a Partir)</label>
                                        <div class="">
                                            <input id="dataPartir" class="form-control data add-on validaData"
                                                   type="text" name="dataPartir"
                                                   value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataPartir']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label>At�</label>
                                        <div class="">
                                            <input id="dataRetroceder" class="form-control data validaData"
                                                   type="text" name="dataRetroceder"
                                                   value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataRetroceder']; ?>"/>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <!-- button area -->
                    <div class="row">

                        <div class="btn-group">
                            <button id="gerarGrafico" class="btn-group-justified btn btn-default btn-lg" type="button">Gerar
                                Relat�rio
                            </button>
                        </div>

                    </div>
                    
                </form>

                <div class="row">
                    <div class="col-md-12">
                        <table id="tabelaResultado" class="table table-responsive table-bordered">
                            <thead>
                            <tr>
                                <th>SAF</th>
                                <th>Data de Abertura</th>
                                <th>Grupo de Sistema</th>
                                <th>Sistema</th>
                                <th>Avaria</th>
                                <th>Responsavel pela Cria��o</th>
                                <th>Solicitante</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        <!-- end�s panel-primary -->
        </div>
    </div>
</div>
