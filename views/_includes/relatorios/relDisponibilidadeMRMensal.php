<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 06/02/2019
 * Time: 14:19
 */

if( $_POST ) {
    $sql = "SELECT * FROM calc_disponibilidade WHERE cod_linha = {$_POST['linha']} AND mes = {$_POST['mes']} AND ano = {$_POST['ano']} ORDER BY dia";
    $disponibilidade = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
}


?>
<div class="page-header">
    <h1>Disponibilidade Material Rodante Mensal</h1>
</div>

<form method="post" class="form-group" action="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/DisponibilidadeMRMensal">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>Dados de Consulta</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Linha</label>
                            <select id="linha" name="linha" class="form-control" required>
                                <option value="">Selecione uma linha</option>
                                <?php
                                $linha = $this->medoo->select('linha', '*');

                                foreach($linha as $dados=>$value){
                                    if($_POST['linha'] == $value['cod_linha'])
                                        echo "<option value='{$value['cod_linha']}' selected>{$value['nome_linha']}</option>";
                                    else{
                                        echo "<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>M�s</label>
                            <select id="mes" name="mes" class="form-control" required>
                                <?php
                                foreach(MainController::$monthComplete as $key=>$value){
                                    if($_POST['mes'] == $key)
                                        echo "<option value='{$key}' selected>{$value}</option>";
                                    else
                                        echo "<option value='{$key}'>{$value}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Ano</label>
                            <select id="mes" name="ano" class="form-control" required>
                                <?php
                                $op = date('Y');
                                $op1 = $op - 5;
                                echo ($_POST['ano'] == $op1) ? "<option selected>$op1</option>" : "<option>$op1</option>";
                                $op2 = $op - 4;
                                echo ($_POST['ano'] == $op2) ? "<option selected>$op2</option>" : "<option>$op2</option>";
                                $op3 = $op - 3;
                                echo ($_POST['ano'] == $op3) ? "<option selected>$op3</option>" : "<option>$op3</option>";
                                $op4 = $op - 2;
                                echo ($_POST['ano'] == $op4) ? "<option selected>$op4</option>" : "<option>$op4</option>";
                                $op5 = $op - 1;
                                echo ($_POST['ano'] == $op5) ? "<option selected>$op5</option>" : "<option>$op5</option>";

                                echo (empty($_POST['ano']) || $_POST['ano'] == $op) ? "<option selected>$op</option>" : "<option>$op</option>";
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <button type="submit" class="btn btn-default btn-large"><i class="fa fa-gears fa-fw"></i>Gerar Relat�rio</button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md-12">
        <div id="grafico"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-4 col-md-4">
        <table id="mediaMensal" class="table table-bordered table-responsive" style="font-size: small; text-align: center">
            <thead>
            <tr>
                <th style="text-align: center">Disponibilidade Mensal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td id="resultMensal" style="font-size: large"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Disponibilidade Mensal</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tableRel" class="table table-bordered table-responsive" style="font-size: small">
                            <thead>
                            <tr>
                                <th>Dia</th>
                                <th>Dados de Disponibilidade</th>
                                <th>Disp. Di�ria</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($disponibilidade))
                            {
                                foreach($disponibilidade as $dados=>$value){
                                    echo ("<tr>");
                                    echo("<td>{$value['dia']}</td>");
                                    echo("<td>{$value['info']}</td>");
                                    echo("<td>{$value['resultado']}</td>");
                                    echo ("</tr>");
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

