<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 02/10/2017
 * Time: 14:11
 */

$where = array();

$where[] = 'WHERE cod_status = 14';

if ($_POST['aPartir']) {
    $where[] = "v_saf.data_abertura >= '{$_POST['aPartir']} 00:00:00'";
}

if ($_POST['ate']) {
    $where[] = "v_saf.data_abertura <= '{$_POST['ate']} 00:00:00'";
}

if ($_POST['grupo']) {
    $where[] = "cod_grupo = " . $_POST['grupo'];
} else {
    $where[] = "cod_grupo IN (22,23,26)";
}

if ($_POST['sistema']) {
    $where[] = "cod_sistema = " . $_POST['sistema'];
}

if ($_POST['subsistema']) {
    $where[] = "cod_subsistema = " . $_POST['subSistema'];
}

if ($_POST['veiculo']) {
    $where[] = "cod_veiculo = {$_POST['veiculo']}";
}

$sqlWhere = '';
if (count($where)) {
    $sqlWhere .= implode(' AND ', $where);
} else {
    $sqlWhere = "";
}

$sql = "SELECT
          cod_saf             AS saf,
          cod_osm             AS osm,
          v_saf.data_abertura AS abertura,
          nome_avaria         AS avaria,
          nome_atuacao        AS atuacao
          nome_veiculo        AS veiculo
        FROM v_saf
          JOIN ssm USING (cod_saf)
          JOIN osm_falha USING (cod_ssm)
          JOIN osm_registro USING (cod_osm)
          JOIN atuacao USING (cod_atuacao)
          LEFT JOIN material_rodante_osm USING(cod_osm)
          LEFT JOIN veiculo USING(cod_veiculo) {$sqlWhere} ORDER BY cod_saf";

$returnSql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print">
            <i class="fa fa-print fa-fw"></i> Imprimir relatório
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="geralPdfRel">

    <!--    <input value='--><?php //echo $_POST['tipoOs'] ?><!--' name='Titulo' type='hidden'/>-->
    <!--    <input value='--><?php //echo $_POST['status'] ?><!--' name='Form' type='hidden'/>-->

    <div class="chartRel">
        <div id="resultApc"></div>
    </div>

    <table id="tableOs" class="text-center" style="width: 100%;page-break-after:always;">
        <thead>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">SAF</th>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">Grupo</th>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">Sistema</th>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">Subsistema</th>
        <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff;">Avaria</th>
        </thead>
        <tbody>
        <?php
        foreach ($returnSql as $dados) {
            echo '<tr>';
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['saf']}</td>";
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['grupo']}</td>";
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['sistema']}</td>";
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['subsistema']}</td>";
            echo "<td style='padding: 7px; background: #E0E0E0; text-align: center; border: solid;'>{$dados['avaria']}</td>";
            echo '<tr>';
        }
        ?>
        </tbody>
    </table>
</div>
