<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 30/01/2017
 * Time: 17:58
 */
?>
<!-- MODAL PAR�METROS -->
<div class="modal fade modal-parametros" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title">Par�metros</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Per�odo</label></div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <label>M�s</label>
                                    <select class="form-control" name="mes">
                                        <option value="1">Janeiro</option>
                                        <option value="2">Fevereiro</option>
                                        <option value="3">Mar�o</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Maio</option>
                                        <option value="6">Junho</option>
                                        <option value="7">Julho</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Ano</label>
                                    <select class="form-control" name="ano">
                                        <?php
                                        echo "<option value='" . (date('Y') - 5) . "'>" . (date('Y') - 5) . "</option>";
                                        echo "<option value='" . (date('Y') - 4) . "'>" . (date('Y') - 4) . "</option>";
                                        echo "<option value='" . (date('Y') - 3) . "'>" . (date('Y') - 3) . "</option>";
                                        echo "<option value='" . (date('Y') - 2) . "'>" . (date('Y') - 2) . "</option>";
                                        echo "<option value='" . (date('Y') - 1) . "'>" . (date('Y') - 1) . "</option>";
                                        echo "<option value='" . date('Y') . "' selected>" . date('Y') . "</option>";
                                        echo "<option value='" . (date('Y') + 1) . "'>" . (date('Y') + 1) . "</option>";
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Local</label></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <label>Linha</label>
                                    <select class="form-control" name="linha">
                                        <option value="">Todos</option>
                                        <?php
                                        $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                        foreach ($selectLinha as $dados => $value) {
                                            echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>�rea</label></div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <label>Grupo de Sistema</label>
                                    <select name="grupoSistema" class="form-control">
                                        <option value="">Grupos de Sistemas</option>
                                        <?php
                                        $grupoSistema = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);

                                        foreach ($grupoSistema as $dados => $value) {
                                            echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Sistema</label>
                                    <select name="sistema" class="form-control">
                                        <option value="">Sistemas</option>
                                        <?php
                                        if (!empty($grupoSistema)) {
                                            $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => "{$grupoSistema['cod_grupo']}"]);

                                            foreach ($sistema as $dados => $value) {
                                                echo('<option value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Sub-Sistema</label>
                                    <select name="subSistema" class="form-control">
                                        <option value="">Sub-Sistemas</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button id="gerar" type="button" class="btn btn-primary">Gerar</button>
            </div>
        </div>
    </div>
</div>
<!-- FIM MODAL -->

<!-- MODAL DETALHES -->
<div class="modal fade modal-detalhes" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <h4 class="modal-title">Detalhes</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- FIM MODAL -->

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div id="resultGraph"></div>
                    </div>
                </div>
                <div class="row">
                    <table id="resultTable" class="table table-bordered text-center">
                        <thead>
                        <tr>
                            <td>N� OS</td>
                            <td>Tipo OS</td>
                            <td>Data Abertura</td>
                            <td>Status</td>
                            <td>A��o</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $sqlOsp = ("SELECT
                          o.cod_osp,
                          o.data_abertura,
                          s.nome_status
                        FROM osp o
                          JOIN status_osp so USING (cod_ospstatus)
                          JOIN status s USING (cod_status)
                        WHERE o.data_abertura < '01-01-2017 00:00:00'
                              AND o.data_abertura >= '01-12-2016 00:00:00'");
                        $resultadoOsp = $this->medoo->query($sqlOsp)->fetchAll(PDO::FETCH_ASSOC);

                        if ($resultadoOsp) {
                            foreach ($resultadoOsp as $value) {
                                echo('<tr>');
                                echo("<td>{$value['cod_osp']}</td>");
                                echo("<td>Preventiva</td>");
                                echo("<td>{$value['data_abertura']}</td>");
                                echo("<td>{$value['nome_status']}</td>");
                                echo("<td><button class='btn btn-default btn-circle' data-toggle='modal' data-target='.modal-detalhes'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }

                        $sqlOsm = ("SELECT
                          o.cod_osm,
                          o.data_abertura,
                          s.nome_status
                        FROM osm_falha o
                          JOIN status_osm so USING (cod_ostatus)
                          JOIN status s USING (cod_status)
                        WHERE o.data_abertura < '01-01-2017 00:00:00'
                              AND o.data_abertura >= '01-12-2016 00:00:00'");
                        $resultadoOsm = $this->medoo->query($sqlOsm)->fetchAll(PDO::FETCH_ASSOC);

                        if ($resultadoOsm) {
                            foreach ($resultadoOsm as $value) {
                                echo('<tr>');
                                echo("<td>{$value['cod_osm']}</td>");
                                echo("<td>Corretiva</td>");
                                echo("<td>{$value['data_abertura']}</td>");
                                echo("<td>{$value['nome_status']}</td>");
                                echo("<td><button class='btn btn-default btn-circle' data-toggle='modal' data-target='.modal-detalhes'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }

                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 40px">
    <div class="col-md-offset-8 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>A��es</label></div>
            <div class="panel-body">
                <div class="col-md-6">
                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                            data-target=".modal-parametros">
                        <i class="fa fa-gears"></i> Par�metros
                    </button>
                </div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-success btn-block">
                        <i class="fa fa-file-text"></i> Imprimir
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
