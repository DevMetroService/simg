<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 25/01/2018
 * Time: 11:37
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3><i class="fa fa-pie-chart fa-fw"></i><strong>Relat�rio de PN(Cancelas)</strong>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel">
                                    <div id="descricaoRel">Relat�rio espec�fico para Passagem de N�vel e Cancela.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>A partir</label>
                                                <input class="form-control data"
                                                       value="<?php if ($_POST['aPartir']) echo $_POST['aPartir'] ?>"
                                                       name="aPartir">
                                            </div>
                                            <div class="col-md-6">
                                                <label>At�</label>
                                                <input class="form-control data"
                                                       value="<?php if ($_POST['ate']) echo $_POST['ate'] ?>"
                                                       name="ate">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Linha</label>
                                                <?php
                                                $linha = $this->medoo->select("linha", "*", ["ODER" => "nome_linha"]);
                                                MainForm::getSelectGrupo($_POST['linha'], $linha);
                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Trecho</label>
                                                <?php
                                                if ($_POST['grupo'])
                                                    $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $_POST['grupo']]);
                                                MainForm::getSelectSistema($_POST['trecho'], $sistema);
                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>N�vel</label>
                                                <?php MainForm::getSelectNivel($_POST['nivel']); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-4">
                            <button type="submit" class="atualizar btn btn-primary btn-large btn-block"><i
                                    class="fa fa-refresh"></i> Atualizar Tabela
                            </button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="limpaFiltro btn btn-success btn-large btn-block"><i
                                    class="fa fa-reply-all"></i> Limpar Filtros
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="relGrafico"></div>

    </div>
</div>