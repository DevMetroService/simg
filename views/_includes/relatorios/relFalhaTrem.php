<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 18/10/2017
 * Time: 10:01
 */ ?>


<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary">

            <div class="panel-heading">
                <h3>Relat�rio de Falhas de Trem</h3>
            </div>


            <div class="panel-body">
                <form id="formRelFalhaTrem" method="post" action="">
                    <div class="row">

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <label>Filtro</label>
                            </div>

                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Linha</label>
                                        <?php
                                        $sql = $this->medoo->select("linha", "*");
                                        $this->form->getSelectLinha(null, $sql, "linha");
                                        ?>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Grupo</label>
                                        <?php
                                        $sqlGrupo = $this->medoo->select("grupo", "*", ["cod_tipo_grupo" => 3]);
                                        $this->form->getSelectGrupo(null, $sqlGrupo, "grupo", false);
                                        ?>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Sistema</label>
                                        <select name="sistema" class="form-control">
                                        </select>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Data de Abertura (a Partir)</label>
                                        <div class="input-group">
                                            <input id="dataPartir" class="form-control data add-on validaData"
                                                   type="text" name="dataPartir" value=""/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>At�</label>
                                        <div class="input-group">
                                            <input id="dataRetroceder" class="form-control data validaData"
                                                   type="text" name="dataRetroceder" value=""/>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>


                    <div class="row">

                        <div class="col_md_1">
                            <div class="btn-group">
                                <button id="gerarGrafico" class="btn-group-justified btn btn-default btn-lg"
                                        type="button">Gerar Relat�rio
                                </button>
                            </div>
                        </div>

                    </div>
                </form>


                <div id="grafico"></div>
            </div>
        </div>
    </div>
</div>
