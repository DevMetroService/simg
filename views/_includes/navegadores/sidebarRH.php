<!-- Sidebar com collapse -->
<div class="navbar-default sidebar navbar-fixed-top hidden-print" role="navigation">
    <div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">
        <ul id="side-menu" class="nav">

            <li class="divider" style="margin-top: 1em"></li>
            <li>
                <a href="<?php echo HOME_URI; ?>/dashboard/Rh"> <i class=" fa fa-dashboard fa-fw"></i> Dados Principais</a>
            </li>

            <li>
                <a href="#"> <i class="fa fa-edit fa-fw"></i> Cadastros <span class="fa arrow"></span></a>

                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo HOME_URI; ?>/funcionario/create">Cadastrar Funcionario</a></li>
                    <li><a href="<?php echo HOME_URI; ?>/dashboardRh/cargos">Inserir Cargos</a></li>
                </ul>
            </li>

            <li>
                <a href="#" data-toggle="modal" data-target="#modalRelatorioPesquisasRh"> OS. Consulta de M�o de Obra</a>
            </li>

            <li>
                <a href="#" data-toggle='modal' data-target='#modalPesquisasRh'>
                    <i class="fa fa-search fa-fw"></i> Pesquisar OS</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="modal fade" id="modalPesquisasRh" tabindex="-1" role="form" aria-labelledby="modalPesquisas" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-search fa-fw iconNavegador"></i> Pesquisas</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    <a class="list-group-item list-group-item-info">
                        <label>Corretivas</label>
                    </a>
                    <div class="row">
                        <div class="col-md-12" >
                            <a class="col-md-4 btn btn-default" href="<?php echo HOME_URI; ?>/dashboardGeral/pesquisaOsm">OSM</a>
                        </div>
                    </div>

                    <div style="margin:5%"></div>
                    <a class="list-group-item list-group-item-info">
                        <label>Preventivas</label>
                    </a>
                    <div class="row">
                        <div class="col-md-12" >
                            <a class="col-md-4 btn btn-default" href="<?php echo HOME_URI; ?>/dashboardGeral/pesquisaOsmp">OSMP</a>
                        </div>
                    </div>

                    <div style="margin:5%"></div>
                    <a class="list-group-item list-group-item-info">
                        <label>Programa��o</label>
                    </a>
                    <div class="row">
                        <div class="col-md-12" >
                            <a class="col-md-4 btn btn-default" href="<?php echo HOME_URI; ?>/dashboardGeral/pesquisaOsp">OSP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalRelatorioPesquisasRh" tabindex="-1" role="form" aria-labelledby="modalPesquisas" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-search fa-fw iconNavegador"></i> Relat�rio de Formul�rios</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    <a class="list-group-item list-group-item-info">
                        <label>Corretivas</label>
                    </a>
                    <div class="row">
                        <div class="col-md-12" >
                            <a class="col-md-4 btn btn-default" href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/DadosOsm">OSM</a>
                        </div>
                    </div>

                    <div style="margin:5%"></div>
                    <a class="list-group-item list-group-item-info">
                        <label>Preventivas</label>
                    </a>
                    <div class="row">
                        <div class="col-md-12" >
                            <a class="col-md-4 btn btn-default" href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/DadosOsmp">OSMP</a>
                        </div>
                    </div>

                    <div style="margin:5%"></div>
                    <a class="list-group-item list-group-item-info">
                        <label>Programa��o</label>
                    </a>
                    <div class="row">
                        <div class="col-md-12" >
                            <a class="col-md-4 btn btn-default" href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/DadosOsp">OSP</a>
                        </div>
                    </div>
                    <div style="margin:5%"></div>
                </div>
            </div>
        </div>
    </div>
</div>


</nav> 