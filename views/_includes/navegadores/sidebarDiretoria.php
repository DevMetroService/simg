<!-- Sidebar com collapse -->
<div class="navbar-default sidebar navbar-fixed-top" role="navigation">

    <div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">
        <ul id="side-menu" class="nav">
            <li class="divider">
            <li><a href="<?php echo HOME_URI ?>/dashboardDiretoria"> <i class=" fa fa-dashboard fa-fw"></i> Dados
                    Principais</a></li>
            <li><a href="#"> <i class="fa fa-edit fa-fw"></i> Solicitação de Abertura de Falha<span
                            class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo HOME_URI; ?>/dashboardGeral/saf">Criar</a></li>
                </ul>
            </li>
            <li>
                <a href='#'>
                    <i class='fa fa-list-ol fa-fw'></i> Consultas Gerais<span class='fa arrow'>
                </a>

                <ul class='nav nav-second-level collapse'>
                    <li><a href='<?php echo HOME_URI . "/dashboardGeral/consultaISMRTUE" ?>'>Consulta IS TUE</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li><a href='<?php echo HOME_URI . "/dashboardGeral/consultaISMRVLT" ?>'>Consulta IS VLT</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li><a href='<?php echo HOME_URI . "/dashboardGeral/consultaOcorrencia/2019/*/*/*/*/*";?>'>Consulta Ocorrência Por Frota/Veiculo</a></li>
                </ul>

                <ul class='nav nav-second-level collapse'>
                    <li><a href=' <?php echo HOME_URI . "/dashboardGeral/consultaOcorrenciaSistema/2019"; ?>'>Consulta  Por Frota/Sistema</a></li>
                </ul>
            </li>
            <li>
                <a href="#" data-toggle='modal' data-target='#modalPesquisas'>
                    <i class="fa fa-search fa-fw"></i> Pesquisar</span>
                </a>
            </li>
            <li class="divider" style="height: 20px"></li>

            <li>
                <a href="#">
                    <i class="fa fa-pie-chart fa-fw"></i> Relatórios<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo HOME_URI; ?>/dashboardGeral/relatorio"> Relatórios de Controle</a></li>
                    <li><a href="#" data-toggle='modal' data-target='#modalRelatoriosContratuais'> Indicadores de
                            Desempenho</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#modalRelatorioFalha"> Relatórios de Falha</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#modalRelatorioPesquisas"> Relatório de Formulários</a></li>
                    <li><a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/HistoricoStatus"> Histórico de
                            Formulários</a></li>
                    <li><a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/GeradorRelatorio"> Gerador de Relatório</a></li>
                    <li><a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/CronogramaSimples"> Cronograma Simples</a></li>
                </ul>


            </li>
        </ul>
    </div>
</div>


</nav>