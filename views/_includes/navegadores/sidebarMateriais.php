<!-- Sidebar com collapse -->
<div class="navbar-default sidebar navbar-fixed-top" role="navigation">

	<div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">

		<ul id="side-menu" class="nav">
			<li class="divider">
			
			<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais"> <i class=" fa fa-dashboard fa-fw"></i> Dados
					Principais
			    </a>
            </li>

			<li><a href="#"> <i class="fa fa-edit fa-fw"></i> Cadastrar<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="<?php echo HOME_URI; ?>/cadastroMateriais">Material</a></li>
					<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais/cadastroFornecedor">Fornecedor</a></li>
					<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais/cadastroCategoria">Categoria</a></li>
				</ul>
            </li>

			<li><a href="#"> <i class="fa fa-edit fa-fw"></i> Criar<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="<?php echo HOME_URI; ?>/cadastroMateriais/requisicaoMateriais">Requisi��o de Materiais</a></li>
					<li><a href="<?php echo HOME_URI; ?>/cadastroMateriais/pedidoCompras">Pedido de Compras</a></li>
					<li><a href="<?php echo HOME_URI; ?>/cadastroMateriais/cotacaoMateriais">Cota��o de Materiais</a></li>
				</ul>
            </li>

			<li><a href="#"> <i class="fa fa-search fa-fw"></i> Pesquisar<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais/pesquisaMaterial">Material</a></li>
					<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais/pesquisaFornecedor">Fornecedor</a></li>
					<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais/pesquisaPedidoCompra">Pedido de Compras</a></li>
					<li><a href="<?php echo HOME_URI; ?>/dashboardMateriais/pesquisaRequisicaoMateriais">Requisi��o de Materiais</a></li>
				</ul>
			<li><a href="<?php echo HOME_URI; ?>/dashboardGeral/controleEstoque" target="_blank"> <i class="fa fa-files-o fa-fw"></i> Controle de Estoque
			</a>
			
			<li>
				<a href="<?php echo HOME_URI; ?>/dashboardMateriais/entradaMaterial">
					<i class="fa fa-cubes fa-fw"></i> Entrada de Material
				</a>
			</li>	
		</ul>
	</div>
</div>

</nav>