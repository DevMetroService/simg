<!-- Sidebar com collapse -->
<div class="navbar-default sidebar navbar-fixed-top" role="navigation">

    <div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">

        <ul id="side-menu" class="nav">

            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input class="form-control" type="text" placeholder="Procurar..."> <span
                        class="input-group-btn">
						<button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
					</span>
                </div>
            </li>

            <li class="divider">

            <li><a href="<?php echo HOME_URI;?>"> <i class=" fa fa-dashboard fa-fw"></i> Dados Principais
                </a></li>
            <li><a href="#"> <i class="fa fa-edit fa-fw"></i> Usu�rios <span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="<?php echo HOME_URI;?>/dashboardTi/cadastrarUsuario">Cadastrar Usu�rio</a></li>
                </ul>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="<?php echo HOME_URI;?>/dashboardTi/editarUsuario">Editar Cadastro de Usuario</a></li>
                </ul>

            </li>
            <li><a  href="<?php echo HOME_URI;?>/dashboardRh/consulta"> <i class="fa fa-subway"></i> CCM </a></li>
            <li><a  href="<?php echo HOME_URI;?>/dashboardRh/relatorio"> <i class="fa fa-search fa-fw"></i> Almoxarifado </a></li>

        </ul>
    </div>
</div>

</nav> 