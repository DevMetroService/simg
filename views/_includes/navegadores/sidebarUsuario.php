<div class="navbar-default sidebar navbar-fixed-top" role="navigation">

	<div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">
		<ul id="side-menu" class="nav">
			<li><a href="<?php echo HOME_URI ?>/dashboardUsuario"> <i class=" fa fa-dashboard fa-fw"></i> Dados
					Principais
                </a>
            </li>

			<li>
                <a href="#"> <i class="fa fa-edit fa-fw">
                    </i> Solicitação de Abertura de Falha<span class="fa arrow"></span>
			    </a>
				<ul class="nav nav-second-level collapse">
					<li><a href="<?php echo HOME_URI;?>/saf/create">Criar</a></li>
				</ul>
            </li>

			<li>
				<a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/HistoricoSaf">
					<i class="fa fa-pie-chart fa-fw"></i> Histórico da Solicitação
				</a>
			</li>

			<li>
				<a href="#" data-toggle='modal' data-target='#modalPesquisas'>
					<i class="fa fa-search fa-fw"></i> Pesquisar</span>
				</a>
			</li>

		</ul>
	</div>
</div>

</nav>