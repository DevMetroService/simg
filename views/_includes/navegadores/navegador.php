<div class="navbar navbar-fixed-top hidden-print">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="navbar-header">
                        <!-- Bot�o de collapso -->
                        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                            <span class="icon-bar"></span> <span class="icon-bar"></span>
                        </button>
                        <?php $dirImg = ['gesiv','material', 'Rh', 'astig']; ?>
                        <a href="<?php echo (!in_array($_SESSION['direcionamento'], $dirImg)) ? HOME_URI . "/dashboard" . $_SESSION['direcionamento'] : "{$this->home_uri}/dashboard/" . $_SESSION['direcionamento'] ; ?>" class="navbar-brand">
                            <img src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo.png" alt="Cons�rcio Metro Service" style="width: 9em; height: 2em; margin: auto 15px;"/>
                        </a>
                        <label class="navbar-brand">
                            Sistema Integrado de Manuten��o e Gest�o
                        </label>

                    </div>


                    <!--  Lista de links direito do navbar -->
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a href="#" class="iconeAjuda" data-toggle="modal" data-target="#ajudaModal"><i class="fa fa-question-circle fa-fw iconNavegador"></i></a>
                        </li>

                        <!-- ################## Notifica��o RealTime ################## -->
                        <!-- ################## Notifica��o RealTime ################## -->
                        <!-- ################## Notifica��o RealTime ################## -->
                        <li class="dropdown iconeHistoricoRealTime">
                            <a href="#" class="iconeNotificacao dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-bell fa-fw iconNavegador"></i>
                                <button style="display: none; position: absolute; top: 45%; left: 50%; width: 25px; height: 25px; font-size: 10px; font-weight: bold;" class="btn btn-danger btn-circle iconNavegadorQuant"></button>
                            </a>
                            <ul class="dropdown-menu dropdown-user styleNotification listaNotificacao"></ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-cogs fa-fw iconNavegador"></i>
                                <i class="fa fa-caret-down"></i>
                            </a> <!-- Sub lista de itens do menu no navbar -->
                            <ul class="dropdown-menu dropdown-user" style="overflow: auto; max-height: 450px;">
                                <?php
                                    if(!empty($_SESSION['equipeUsuario'])){
                                        echo    '<li>
                                                    <a><i class="fa fa-users fa-fw"></i>Minhas Equipes';

                                        foreach ($_SESSION['equipeUsuario'] as $dados) {
                                            echo "<br /><span title='{$dados['nome_equipe']} / {$dados['nome_unidade']}' style='font-weight: bold'><i class='fa fa-dot-circle-o'></i> - {$dados['sigla']} / {$dados['sigla_unidade']}</span>";
                                        }

                                        echo    '</a></li>
                                                <!-- Classe de divis�o -->
                                                <li class="divider"></li>';
                                    }
                                ?>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#alteraSenhaModal"><i class="fa fa-user fa-fw"></i>Alterar Senha</a>
                                </li>

                                <!-- Classe de divis�o -->
                                <li class="divider"></li>

                                <li><a href="<?php echo HOME_URI; ?>/logout"><i class="fa fa-power-off fa-fw"></i>Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Fim dos links do navbar -->

                    <!-- Mensagem boas-vindas -->
                    <label class="navbar-right"> Seja bem-vindo, <?php echo " " . $this->dadosUsuario['usuario']; ?></label>

                    <div class="navbar-right dataRelogioNavBarDiv" data-placement="right" rel="tooltip-wrapper" data-title="Hor�rio do Servidor">
                        <i class="fa fa-clock-o  fa-fw iconNavegador"></i>
                        <span class="dataRelogioNavBarSpan"></span>
                    </div>

                    <div class="navbar-right dataRelogioNavBarDiv" data-placement="right" rel="tooltip-wrapper" data-title="Branch Atual">
                        <?php
                        if($_SERVER['HTTP_HOST'] == 'hgsimg.metrofor.ce.gov.br' || $_SERVER['HTTP_HOST'] == 'simg.test' )
                        {
                            $stringfromfile = file('.git/HEAD', FILE_USE_INCLUDE_PATH);

                            $firstLine = $stringfromfile[0]; //get the string from the array

                            $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string

                            $branchname = $explodedstring[2]; //get the one that is always the branch name

                            echo $branchname; //show it on the page
                        }
                        ?>
                    </div>

                </nav>
            </div>
            <?php
                //necess�rio para an�lise em Socket JS
                echo "<input type='hidden' value ='{$this->dadosUsuario['nivel']}' id='nivelUserSimg' />";

                //Necess�rio para direcionamento em SocketJS
                if(!empty($_SESSION['direcionamento'])){
                    echo "<input type='hidden' value ='{$_SESSION['direcionamento']}' id='direcionamentoLocal' />";
                }

                //Analisa e verifica se us�rio � de equipe de manuten��o.
                if(!empty($this->equipeUsuario)){
                    echo '<input type="hidden" value ="';

                    foreach ($this->equipeUsuario as $dados => $value) {
                        echo "/{$value}";
                    }

                    echo '" id="cod_Equipes_Usuario" />';
                }

                //Alterar nome medida provis�ria. fun��o ser� definitiva
                $medidaProvisoria = $this->sidebar->printSidebar($_SESSION['dadosUsuario'], $this->paginaDashboard);

                if(!$medidaProvisoria && !empty($_SESSION['navegador'])){
                    include($_SESSION['navegador']); // Deletar apos fun��o estar 100% em todos os niveis
                }
            ?>

        </div>
    </div>
</div>
