<?php
/**
 * Created by PhpStorm.
 * User: METROFOR
 * Date: 10/12/2019
 * Time: 09:40
 */
?>

<div class="page-header">
    <h1>Gerenciador de Cargos</h1>
</div>

<div class="row">
    <div class="col-md-12" id="messagebox">
        <div class="alert alert-<?php echo $parametros[0]?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">x</span>
            </button>
            <h4>
                <?php echo $message?>
            </h4>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><label>Cargos</label></div>

    <div class="panel-body">
        <form id="formCargo" method="POST" action="<?php echo HOME_URI . 'cadastro/cadastroCargo'; ?>">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label id="subtitulo">Dados do cargo a ser inserido</label></div>

                        <div class="panel-body">
                            <div id="divCodigo" class="row" style="display: none">
                                <div class="col-md-2">
                                    <label>C�digo do Cargo</label>
                                    <input name="cod_cargos" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nome do Cargo</label>
                                    <input name="descricao" required class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Botao Adicionar -->
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary btn-lg gerarRel">
                            <i class="fa fa-floppy-o"></i>
                            <label id="inserirAtualizar">Inserir</label>
                        </button>

                    </div>
                    <div class="btn-group">
                        <button type="submit" style="display: none" class="btn btn-default btn-lg resetarForm">
                            <i class="fa fa-undo fa-fw"></i>
                            <label>Resetar Formul�rio</label>
                        </button>
                    </div>
                </div>
            </div>


        </form>



        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Cargos Armazenados</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tabelaAvarias" class="table table-responsive table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>C�digo do Cargo</th>
                                        <th>Nome do Cargo</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php

                                    $selectCargos = $this->medoo->select('cargos_funcionarios', ['cod_cargos', 'descricao']);


                                    foreach ($selectCargos as $registros) {
                                        echo '<tr>';
                                        echo '<td>'.$registros['cod_cargos'].'</td>';
                                        echo '<td>'.$registros['descricao'].'</td>';
                                        echo('<td>');
                                        echo('<button class="btn btn-primary btn-circle btnEditarCargo" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo '</tr>';
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

