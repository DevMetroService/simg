<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 25/02/2019
 * Time: 09:45
 */
?>

<div class="page-header">
    <h1>Inserir Dados - Adicionar Local - Grupo de Sistema</h1>
</div>

<?php
$usuario = $_SESSION['dadosUsuario'];
$sql = "SELECT sigla FROM eq_us_unidade JOIN un_equipe USING (cod_un_equipe) JOIN equipe USING(cod_equipe) WHERE cod_usuario = {$usuario['cod_usuario']}";
$grupo = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$grupoPermitido = $grupo[0]['sigla'];

switch($grupoPermitido){
    case 'MRT':
    case 'MRV':
    case 'MRL':
        $grupoPermitido = [23, 22, 26];
        break;

    case 'OR':
    case 'EL':
    case 'EE':
        $grupoPermitido = 21;
        break;

    case 'TC':
        $grupoPermitido = 27;
        break;

    case 'BI':
        $grupoPermitido = 28;
        break;

    case 'VP':
        $grupoPermitido = 24;
        break;

    case 'EN':
    case 'RA':
        $grupo = [25, 20];
        break;

    DEFAULT:
        $grupo = false;
        break;

}
?>

<div class="panel panel-primary">
    <div class="panel-heading"><label>Formul�rio de Local</label></div>

    <div class="panel-body">
        <form method="POST" action="<?php echo HOME_URI . '/cadastroCampos/gravarLocalGrupo'; ?>">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Local Grupo</label></div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="col-md-5">
                                        <label>Nome Local</label>
                                        <input id="nome_local_grupo" name="nome_local_grupo" value="" required class="form-control">
                                        <input id="cod_local_grupo" type="hidden" name="cod_local_grupo">
                                    </div>

                                    <div class="col-md-3 col-xs-2">
                                        <label for="gSistema">Grupo Sistema</label>
                                        <?php
                                        if($grupo){
                                            $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["ativo" => 'S', "cod_grupo" => $grupoPermitido], "ORDER" => "nome_grupo"]);
                                        }else{
                                            $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S', "ORDER" => "nome_grupo"]);
                                        }
                                        $this->form->getSelectGrupo(null, $grupo, 'cod_grupo', true);
                                        ?>
                                    </div>

                                    <div class="col-md-2">
                                        <label for="nivel">Ativo?</label>
                                        <select name="ativo" class="form-control" required>
                                            <option value="s" selected="true" >Sim</option>
                                            <option value="n">N�o</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Botao Adicionar -->
            <div class="row">
                <div class="col-md-1">
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary btn-lg gerarRel">
                            <i class="fa fa-floppy-o"></i>
                            Salvar
                        </button>
                    </div>
                </div>
            </div>


        </form>



        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Local de Grupo - Armazenadas</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tabela" class="table table-responsive table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>Nome Local</th>
                                        <th>Grupo</th>
                                        <th>Ativo?</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php

                                    $grupo = array();
                                    $selectGrupo = $this->medoo->select('grupo', ['cod_grupo', 'nome_grupo']);
                                    foreach ($selectGrupo as $dados) {
                                        $grupo[$dados['cod_grupo']] = $dados['nome_grupo'];
                                    }

                                    $localGrupo = $this->medoo->select("local_grupo", '*');

                                    foreach ($localGrupo as $local) {
                                        echo '<tr>';
                                        echo '<td>'.$local['nome_local_grupo'].'</td>';
                                        echo '<td>'.$grupo[$local['cod_grupo']].'</td>';
                                        echo '<td>'.(($local['ativo'] == 's')? "Ativo":"Desativado").'</td>';
                                        echo('<td><button class="btn btn-default btn-circle btn_editar" type="button" title="Editar Dados" value="'.$local['cod_local_grupo'].'"><i class="fa fa-file-o fa-fw"></i></button></td>');
                                        echo '</tr>';
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
