<div class="page-header">
    <h1>Inserir Dados - Adicionar Avarias</h1>
</div>

<?php
$usuario = $_SESSION['dadosUsuario'];
$sql = "SELECT sigla FROM eq_us_unidade JOIN un_equipe USING (cod_un_equipe) JOIN equipe USING(cod_equipe) WHERE cod_usuario = {$usuario['cod_usuario']}";
$grupo = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$grupoPermitido = $grupo[0]['sigla'];

switch($grupoPermitido){
    case 'MRT':
    case 'MRV':
    case 'MRL':
        $grupoPermitido = [23, 22, 26];
        break;

    case 'OR':
    case 'EL':
    case 'EE':
        $grupoPermitido = 21;
        break;

    case 'TC':
        $grupoPermitido = 27;
        break;

    case 'BI':
        $grupoPermitido = 28;
        break;

    case 'VP':
        $grupoPermitido = 24;
        break;

    case 'EN':
    case 'RA':
        $grupo = [25, 20];
        break;

    DEFAULT:
        $grupo = false;
        break;

}
?>

<div class="panel panel-primary">
    <div class="panel-heading"><label>Adicionar Avarias</label></div>

    <div class="panel-body">
        <form method="POST" action="<?php echo HOME_URI . '/cadastroCampos/gravarAvaria'; ?>">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Avaria</label></div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="col-md-5">
                                        <label>Nome da Avaria</label>
                                        <input name="nome_avaria" value="<?php $avaria['nome_avaria']?>" required class="form-control">
                                        <input  type="hidden" name="cod_avaria">
                                    </div>

                                    <div class="col-md-3 col-xs-2">
                                        <label for="gSistema">Grupo Sistema</label>
                                        <?php
                                        if($grupo){
                                            $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["ativo" => 'S', "cod_grupo" => $grupoPermitido], "ORDER" => "nome_grupo"]);
                                        }else{
                                            $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S', "ORDER" => "nome_grupo"]);
                                        }
                                        $this->form->getSelectGrupo($saf['cod_grupo'], $grupo, 'cod_grupo', false);
                                        ?>
                                    </div>

                                    <div class="col-md-2">
                                        <label for="nivel">Sugest�o de N�vel</label>
                                        <select name="sugestao_nivel" class="form-control">
                                            <option selected="true"></option>
                                            <option data-toggle="tooltip"
                                                    title="Urgente: Interrompe a circula��o."
                                                    value="A">
                                                A
                                            </option>
                                            <option title="Emergencial: Atrapalha a circula��o."
                                                    value="B">
                                                B
                                            </option>
                                            <option title="Necess�rio: N�o afeta diretamente a circula��o."
                                                    value="C">
                                                C
                                            </option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <label for="nivel">Ativo?</label>
                                        <select name="ativo" class="form-control" required>
                                            <option value="s" selected="true" >Sim</option>
                                            <option value="n">N�o</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Botao Adicionar -->
            <div class="row">
                <div class="col-md-1">
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary btn-lg gerarRel">
                            <i class="fa fa-floppy-o"></i>
                            Salvar
                        </button>
                    </div>
                </div>
            </div>


        </form>



        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Avarias - Armazenadas</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <table id="tabelaAvarias" class="table table-responsive table-striped table-bordered no-footer">
                                <thead>
                                <tr>
                                    <th>Nome Avaria</th>
                                    <th>Nivel Sugerido</th>
                                    <th>Grupo</th>
                                    <th>Ativo?</th>
                                    <th>A��o</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php

                                $grupo = array();
                                $selectGrupo = $this->medoo->select('grupo', ['cod_grupo', 'nome_grupo']);
                                foreach ($selectGrupo as $dados) {
                                    $grupo[$dados['cod_grupo']] = $dados['nome_grupo'];
                                }

                                $avarias = $this->medoo->select("avaria", '*');
//                                $avarias = $this->medoo->select("avaria", '*', ['cod_grupo' =>$grupoPermitido]);

                                foreach ($avarias as $avaria) {
                                    echo '<tr>';
                                    echo '<td>'.$avaria['nome_avaria'].'</td>';
                                    echo '<td>'.$avaria['sugestao_nivel'].'</td>';
                                    echo '<td>'.$grupo[$avaria['cod_grupo']].'</td>';
                                    echo '<td>'.(($avaria['ativo'] == 's')? "Ativo":"Desativado").'</td>';
                                    echo('<td><button class="btn btn-default btn-circle btn_editar_avaria" type="button" title="Editar Dados" value="'.$avaria['cod_avaria'].'"><i class="fa fa-file-o fa-fw"></i></button></td>');
                                    echo '</tr>';
                                }

                                ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
