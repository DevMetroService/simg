<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 14/01/2019
 * Time: 11:51
 */
?>

<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>Cadastro PMP Material Rodante</h1>
    <small>Cadastro de PMP do Material Rodante VLT e TUE.</small>
</div>

<div class="row">
    <form method="post" id="formCadastroPMPMr" action="<?php echo HOME_URI . 'cadastroGeral/cadastrarPmpMaterialRodante'; ?>" class="form-group">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <label> Dados PMP</label></div>
                <div class="panel-body">
                    <div class="row">
                        <input id="cod_pmp_mr" name="cod_pmp_mr" class="form-control" type="hidden" readonly value="">
                        <div class="col-md-3">
                            <label>Grupo de Sistema</label>
                            <select id="grupo" name="grupo" class="form-control" required>
                                <option disabled selected="true" value="">SELECIONE</option>
                                <?php
                                $grupos = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ORDER" => "cod_grupo", 'cod_grupo' => [22,23]]);
                                foreach ($grupos as $grupo) {
                                    echo "<option value='{$grupo['cod_grupo']}'>{$grupo['nome_grupo']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Servi�o</label>
                            <select id="cod_servico_mr" name="cod_servico_mr" class="form-control" required>
                                <option disabled selected="true" value="">SELECIONE</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Linha</label>
                            <select id="cod_linha" name="cod_linha" class="form-control" required>
                                <option disabled selected="true" value="">SELECIONE</option>
                            <?php
                            $linhas = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);
                            foreach ($linhas as $linha) {
                                echo "<option value='{$linha['cod_linha']}'>{$linha['nome_linha']}</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label>Mes</label>
                            <select id="mes" name="mes" class="form-control" required>
                                <option disabled selected value="">SELECIONE</option>
                                <option value="1">Janeiro</option>
                                <option value="2">Fevereiro</option>
                                <option value="3">Mar�o</option>
                                <option value="4">Abril</option>
                                <option value="5">Maio</option>
                                <option value="6">Junho</option>
                                <option value="7">Julho</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setembro</option>
                                <option value="10">Outubro</option>
                                <option value="11">Novembro</option>
                                <option value="12">Dezembro</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Ano</label>
                            <select class="form-control" id="ano" name="ano" required>
                                <?php
                                $ano_atual = (int)date("Y") ;
                                echo "<option selected='true' value=$ano_atual>$ano_atual</option>";
                                $ano = $ano_atual + 1;
                                echo "<option value=$ano>$ano</option>";
                                $ano = $ano_atual - 1;
                                echo "<option value=$ano>$ano</option>";
                                ?>
                            </select>

                        </div>
                        <div class="col-md-2">
                            <label>Qtd. Manuten��es</label>
                            <input id="qtd_manutencao" name="qtd_manutencao" class="form-control number" type="text" value="" required>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="button" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="button" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

<!-- TABELAS DE PMP�s POR LINHA-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h4>PMP Material Rodante</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!-- Abas  TUE e VLT -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#AbaVLT" aria-controls="profile" role="tab" data-toggle="tab">VLT</a></li>
                            <li role="presentation"><a href="#AbaTUE" aria-controls="profile" role="tab" data-toggle="tab">TUE</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- VLT -->
                            <div role="tabpanel" class="tab-pane fade in active" id="AbaVLT">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#AppVLTSul" aria-controls="profile" role="tab" data-toggle="tab">L. Sul</a></li>
                                            <li role="presentation"><a href="#AppVLTOeste" aria-controls="profile" role="tab" data-toggle="tab">L. Oeste</a></li>
                                            <li role="presentation"><a href="#AppVLTCariri" aria-controls="profile" role="tab" data-toggle="tab">L. Cariri</a></li>
                                            <li role="presentation"><a href="#AppVLTSobral" aria-controls="profile" role="tab" data-toggle="tab">L. Sobral</a></li>
                                            <li role="presentation"><a href="#AppVLTParangabaMucuripe" aria-controls="profile" role="tab" data-toggle="tab">L. Parangaba - Mucuripe</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="AppVLTSul">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpVLT_Sul" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 22 AND pmp_mr.cod_linha = 5";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppVLTOeste">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpVLT_Oeste" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 22 AND pmp_mr.cod_linha = 1";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppVLTCariri">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpVLT_Cariri" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 22 AND pmp_mr.cod_linha = 2";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppVLTSobral">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpVLT_Sobral" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 22 AND pmp_mr.cod_linha = 7";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppVLTParangabaMucuripe">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpVLT_ParangMucuripe" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 22 AND pmp_mr.cod_linha = 6";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- TUE -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaTUE">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#AppTUESul" aria-controls="profile" role="tab" data-toggle="tab">L. Sul</a></li>
                                            <li role="presentation"><a href="#AppTUEOeste" aria-controls="profile" role="tab" data-toggle="tab">L. Oeste</a></li>
                                            <li role="presentation"><a href="#AppTUECariri" aria-controls="profile" role="tab" data-toggle="tab">L. Cariri</a></li>
                                            <li role="presentation"><a href="#AppTUESobral" aria-controls="profile" role="tab" data-toggle="tab">L. Sobral</a></li>
                                            <li role="presentation"><a href="#AppTUEParangabaMucuripe" aria-controls="profile" role="tab" data-toggle="tab">L. Parangaba - Mucuripe</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="AppTUESul">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpTUE_Sul" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 23 AND pmp_mr.cod_linha = 5";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppTUEOeste">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpTUE_Oeste" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 23 AND pmp_mr.cod_linha = 1";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppTUECariri">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpTUE_Cariri" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 23 AND pmp_mr.cod_linha = 2";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppTUESobral">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpTUE_Sobral" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 23 AND pmp_mr.cod_linha = 7";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AppTUEParangabaMucuripe">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="tablePmpTUE_ParangMucuripe" class="table table-striped table-responsive">

                                                        <thead>
                                                        <tr>
                                                            <th>M�s</th>
                                                            <th>Ano</th>
                                                            <th>Servico</th>
                                                            <th>Linha</th>
                                                            <th>Qtd. Manutencoes</th>
                                                            <th>A��o</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT * FROM pmp_mr
                                        INNER JOIN servico_material_rodante as servico_mr 
                                        ON pmp_mr.cod_servico_material_rodante = servico_mr.cod_servico_material_rodante
                                        INNER JOIN linha as linha
                                        ON pmp_mr.cod_linha = linha.cod_linha
                                        WHERE pmp_mr.cod_grupo = 23 AND pmp_mr.cod_linha = 6";

                                                        $dados_pmp_mr = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($dados_pmp_mr)) {

                                                            foreach ($dados_pmp_mr as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['mes']}'>{$month[ $value['mes'] ]}</td>";
                                                                echo "<td data-value='{$value['ano']}'>" . $value['ano'] . "</td>";
                                                                echo "<td data-value='{$value['cod_servico_material_rodante']}'>" . $value['nome_servico_material_rodante'] . "</td>";
                                                                echo "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
                                                                echo "<td>" . $value['qtd_manutencao'] . "</td>";
                                                                echo "<td><button type='button' data-value='{$value['cod_pmp_mr']}' data-grupo='{$value['cod_grupo']}' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                                echo "</tr>";

                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>