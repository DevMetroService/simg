<?php



if(!empty($returnPesquisa)){

    $cod_grupo = '';
    $rows = "";

    foreach ($returnPesquisa as $dados=>$value) {

        $cod_grupo = $value["cod_grupo"];

        $txt = $value["cod_odometro"] . ":::";
        $txt .= $this->parse_timestamp($value["data_cadastro"]) . ":::";
        $txt .= $value["cod_veiculo"] . ":::";
        $txt .= $value["nome_veiculo"] . ":::";

        //Preenche os dados especificos caso ser um VLT
        if ($cod_grupo == 22) {
            $txt .= $value["cod_linha"] . ":::";
            $txt .= $value["nome_linha"] . ":::";
            $txt .= $value["odometro_ma"] . ":::";
            $txt .= $value["tracao_ma"] . ":::";
            $txt .= $value["gerador_ma"] . ":::";
            $txt .= $value["odometro_mb"] . ":::";
            $txt .= $value["tracao_mb"] . ":::";
            $txt .= $value["gerador_mb"] . ":::";
        }

        //Preenche os dados especificos caso ser um TUE
        if ($cod_grupo == 23) {
            $txt .= $value["odometro"] . ":::";
        }

        $txt .= $value["cod_usuario"] . ":::";
        $txt .= $value["nome_usuario"] . ":::";
        $txt .= $value["ano"] . ":::";
        $txt .= $value["mes"];


        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $rows .= $txt ."\n";

    }
}

$cabecalho = "N. ODOMETRO;";
$cabecalho .= "Data de Cadastro;";
$cabecalho .= "N. Veículo;";
$cabecalho .= "Nome do Veículo;";

//Exibe a colunas especificas caso ser um VLT
if ($cod_grupo == 22) {
    $cabecalho .= "N. Linha;";
    $cabecalho .= "Nome da Linha;";
    $cabecalho .= "Odometro MA;";
    $cabecalho .= "Tração MA;";
    $cabecalho .= "Gerador MA;";
    $cabecalho .= "Odometro MB;";
    $cabecalho .= "Tração MB;";
    $cabecalho .= "Gerador MB;";
}

//Exibe a colunas especificas caso ser um TUE
if ($cod_grupo == 23) {
    $cabecalho .= "Odometro;";
}

$cabecalho .= "N. Usuario Responsável;";
$cabecalho .= "Usuario Responsável;";
$cabecalho .= "Ano;";
$cabecalho .= "Mês\n";

$cabecalho = utf8_decode($cabecalho);



//Monta o cabecalho mais as linhas de conteudo
$conteudo = $cabecalho . $rows;

$nome_grupo = $cod_grupo == 22 ? 'VLT' : 'TUE';

$nome_grupo = "-" . $nome_grupo;
$this->salvaTxt("KmRodadosPorVeiculo" . "$nome_grupo" . ".csv", $conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=KmRodadosPorVeiculo" . "$nome_grupo" . ".csv");
readfile("KmRodadosPorVeiculo" . "$nome_grupo" . ".csv");

unlink("KmRodadosPorVeiculo" . "$nome_grupo" . ".csv");