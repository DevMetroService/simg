<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Configura��o de Composi��o</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <form id="formulario" class="form-group" method="post"  action="<?php echo HOME_URI; ?>/cadastroGeral/ConfigComposicao">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Tipo de Composi��o</label>
                                    <select name="tipoComposicao" class="form-control" required>
                                        <option value=""></option>
                                        <option value="22">VLT</option>
                                        <option value="23">TUE</option>
                                        <option value="26">Locomotiva</option>
                                    </select>
                                </div>

                                <div class="col-lg-6" id="divComposicao">
                                    <label>Composi��o</label>
                                    <div class="input-group">
                                        <select name="composicao" class="form-control">
                                            <option value=""></option>
                                            <?php
                                            $selectComposicao = $this->medoo->select("composicao", "*");

                                            foreach ($selectComposicao as $dados => $value) {
                                                echo("<option value='{$value['cod_composicao']}'>{$value['numero_composicao']}</option>");
                                            }
                                            ?>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-secondary addComposicao" type="button" title="Criar Nova Composi��o"><i class="fa fa-plus fa-fw"></i></button>
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <hr style="border-top: 3px double #8c8b8b; margin-top: 2%; margin-bottom: 2%;"/>

                            <div class="row" style="margin-bottom: 2%;">
                                <div class="col-md-6">
                                    <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#novoVeiculoModal">
                                        <i class="fa fa-plus fa-fw"></i> Adicionar Ve�culo
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-success btn-block" data-toggle="modal" data-target="#novoCarroModal">
                                        <i class="fa fa-plus fa-fw"></i> Adicionar Carro
                                    </button>
                                </div>
                            </div>

                            <div class="panel-default panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Ve�culo</label>
                                            <select name="veiculo-1" class="form-control"></select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Carro</label>
                                            <select name="carro1[]" class="form-control" multiple="multiple"></select>
                                        </div>
                                    </div>
                                    <div class="divComp1"></div>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-default addVeiculoComposicao" value="1">
                                        <i class="fa fa-plus fa-fw"></i> Adicionar Ve�culo a Composi��o
                                    </button>
                                    <input type="hidden" value="1" name="quantVeiculoComposicao"/>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 2%;">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4">
                                        <div class="btn-group  btn-group-justified " role="group">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-default btn-lg" aria-label="right align" title="Salvar">
                                                    <i class="fa fa-floppy-o fa-2x"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Carro VLT</label></div>

            <div class="panel-body">
                <table id="indicadorCarroVLT" class="table table-striped table-bordered no-footer">
                    <thead>
                    <tr>
                        <th>Carro</th>
                        <th>Tipo</th>
                        <th>Ve�culo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selectCarroVLT = $this->medoo->select("carro", ["[>]veiculo" => "cod_veiculo","[>]tipo_carro" => "cod_tipo_carro"], "*", ["carro.cod_grupo" => (int) 22]);

                    if (!empty($selectCarroVLT)) {
                        foreach ($selectCarroVLT as $dados) {
                            echo('<tr>');
                            echo('<td>' . $dados['nome_carro'] . '</td>');
                            echo('<td>' . $dados['nome_tipo_carro'] . '</td>');
                            echo('<td>' . $dados['nome_veiculo'] . '</td>');
                            echo('</tr>');
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Carro TUE</label></div>

            <div class="panel-body">
                <table id="indicadorCarroTUE" class="table table-striped table-bordered no-footer">
                    <thead>
                    <tr>
                        <th>Carro</th>
                        <th>Tipo</th>
                        <th>Ve�culo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selectCarroTUE = $this->medoo->select("carro", ["[>]veiculo" => "cod_veiculo","[>]tipo_carro" => "cod_tipo_carro"], "*", ["carro.cod_grupo" => (int) 23]);

                    if (!empty($selectCarroTUE)) {
                        foreach ($selectCarroTUE as $dados) {
                            echo('<tr>');
                            echo('<td>' . $dados['nome_carro'] . '</td>');
                            echo('<td>' . $dados['nome_tipo_carro'] . '</td>');
                            echo('<td>' . $dados['nome_veiculo'] . '</td>');
                            echo('</tr>');
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Carro Locomotiva</label></div>

            <div class="panel-body">
                <table id="indicadorCarroLocomotiva" class="table table-striped table-bordered no-footer">
                    <thead>
                    <tr>
                        <th>Carro</th>
                        <th>Tipo</th>
                        <th>Ve�culo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selectCarroLocomotiva = $this->medoo->select("carro", ["[>]veiculo" => "cod_veiculo","[>]tipo_carro" => "cod_tipo_carro"], "*", ["carro.cod_grupo" => (int) 26]);

                    if (!empty($selectCarroLocomotiva)) {
                        foreach ($selectCarroLocomotiva as $dados) {
                            echo('<tr>');
                            echo('<td>' . $dados['nome_carro'] . '</td>');
                            echo('<td>PIDNER</td>');
                            echo('<td>' . $dados['nome_veiculo'] . '</td>');
                            echo('</tr>');
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- Ve�culo -->
<div class="modal fade" id="novoVeiculoModal" tabindex="-1" role="form" aria-labelledby="novoVeiculoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="novoVeiculoModalLabel">Criar Novo Ve�culo</h4>
            </div>

            <form method="post" action="<?php echo HOME_URI; ?>/cadastroGeral/cadastrarVeiculo">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nome Ve�culo</label>
                            <input name="nomeVeiculo" class="form-control" required/>
                        </div>
                        <div class="col-lg-6">
                            <label>Grupo</label>
                            <select name="tipoVeiculo" class="form-control" required>
                                <option value=""></option>
                                <option value="22">VLT</option>
                                <option value="23">TUE</option>
                                <option value="26">Locomotiva</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Ve�culo</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- Carro -->
<div class="modal fade" id="novoCarroModal" tabindex="-1" role="form" aria-labelledby="novoCarroModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="novoCarroModalLabel">Criar Novo Carro</h4>
            </div>

            <form method="post" action="<?php echo HOME_URI; ?>/cadastroGeral/cadastrarCarro">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nome Carro</label>
                            <input name="nomeCarro" class="form-control" required/>
                        </div>
                        <div class="col-lg-6">
                            <label>Tipo</label>
                            <select name="tipoCarro" class="form-control" required>
                                <option value=""></option>
                                <option value="1">Carro Motor</option>
                                <option value="2">Carro Sem Motor</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Grupo</label>
                            <select name="grupo" class="form-control" required>
                                <option value=""></option>
                                <option value="22">VLT</option>
                                <option value="23">TUE</option>
                                <option value="26">Locomotiva</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Carro</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>