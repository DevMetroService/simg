<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 10/02/2019
 * Time: 20:32
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 01/02/2019
 * Time: 10:41
 */
?>

<div class="page-header">
    <h1>Tabela de Registros de SAF's no Periodo Por Frota / Sistema</h1>
    <h4><em>Quantitativo de registro de solicitação feitas pelos reclamantes. Não nescessariamente falha.</em></h4>
</div>

<div class="row">
    <div class="col-md-2">
        <select id="ano" class="form-control">
            <?php
            for ($i = 0; $i < 4; $i++){
                $op = date('Y') - $i;
                echo $op == $ano ? ("<option value='{$op}' selected>{$op}</option>") : ("<option value='{$op}'>{$op}</option>") ;
            }
            ?>
        </select>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-default changeAno">Alterar</button>
    </div>
</div>

<!-- TUE Linha sul-->
<div class="row" style="margin-top: 25px">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <a href="" target=""></a>
                <label>Numero de Ocorrências por TUE L. Sul - <?php echo $ano; ?></label></br>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Sistema</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], "*", ["cod_grupo" => 23,"ORDER" => "nome_sistema"]);
                            $ctdCol=[];
                            $ctdline=0;

                            foreach($sistema as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_sistema']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_linha = 5 AND cod_sistema = {$value['cod_sistema']}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            <tfoot>
                                <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Oeste-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8">
                        <a href="" target=""></a>
                        <label>Numero de Ocorrências por VLT L. Oeste - <?php echo $ano; ?></label>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Sistema</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], "*", ["cod_grupo" => 22,"ORDER" => "nome_sistema"]);
                            $ctdCol=[];
                            $ctdline=0;

                            foreach($sistema as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_sistema']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_linha = 1 AND cod_sistema = {$value['cod_sistema']}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Parangaba-Mucuripe-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8">
                        <a href="" target=""></a>
                        <label>Numero de Ocorrências por VLT L. Parangaba Mucuripe - <?php echo $ano; ?></label>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Sistema</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], "*", ["cod_grupo" => 22,"ORDER" => "nome_sistema"]);
                            $ctdCol=[];
                            $ctdline=0;

                            foreach($sistema as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_sistema']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_linha = 6 AND cod_sistema = {$value['cod_sistema']}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Cariri-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8">
                        <a href="" target=""></a>
                        <label>Numero de Ocorrências por VLT L. Cariri - <?php echo $ano; ?></label>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Sistema</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], "*", ["cod_grupo" => 22,"ORDER" => "nome_sistema"]);
                            $ctdCol=[];
                            $ctdline=0;

                            foreach($sistema as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_sistema']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_linha = 2 AND cod_sistema = {$value['cod_sistema']}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Sobral-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <a href="" target=""></a>
                        <label>Numero de Ocorrências por VLT L. Sobral - <?php echo $ano; ?></label>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Sistema</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], "*", ["cod_grupo" => 22,"ORDER" => "nome_sistema"]);
                            $ctdCol=[];
                            $ctdline=0;

                            foreach($sistema as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_sistema']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_linha = 7 AND cod_sistema = {$value['cod_sistema']}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
