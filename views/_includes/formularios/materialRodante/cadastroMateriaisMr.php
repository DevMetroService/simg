<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 14/01/2019
 * Time: 11:51
 */
?>

<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>Cadastro de Materiais</h1>
    <small>Cadastro de Materiais Exclusivos para Material Rodante.</small>
</div>

<div class="row">


    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"> <label> Formulario de Cadastro</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <form id="formulario" class="form-group" action="<?php echo(HOME_URI); ?>/dashboardEngenhariaMr/adicionarMaterial" method="post">
                            <input type="hidden" name="cod_material" value="" />
                            <div class="row">
                                <div class="col-md-9">
                                    <label>Nome</label>
                                    <input type="text" class="form-control" name="nomeMaterial" placeholder="Digite somente o nome do material, sem marca ou medida: Ex: Cabo de rede" >
                                </div>
                                <div class=" col-md-3">
                                    <label>Grupo de Sistema</label>
                                    <select id="grupoSistema" name="grupoSistema" class="form-control" required>
                                        <option value="" readonly="" selected="">SELECIONE</option>
                                        <option value="23">MATERIAL RODANTE - TUE</option>
                                        <option value="22">MATERIAL RODANTE - VLT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Unidade de Medida</label>
                                    <?php
                                    $consultaUnidadeMedida = $this->medoo->select("unidade_medida", "*");

                                    echo('<select name="unidadeMedidaMaterial" class="form-control"
                                            required data-validation-required-message=" Campo precisa ser preenchido"
                                            data-bv-row=".col-lg-4"
                                            data-fv-notempty="true"
                                            data-fv-notempty-message="Selecione a unidade de medida.">');
                                    echo('<option selected="selected" value="" disabled="disabled">Unidade de Medida</option>');

                                    foreach ($consultaUnidadeMedida as $key => $value) {
                                        echo "<option value='{$value['cod_uni_medida']}' >{$value['sigla_uni_medida']}-{$value['nome_uni_medida']}</option>";
                                    }
                                    echo('</select>');

                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <label>Quantidade M�nima</label>
                                    <input type="text" name="qtdMinimaMaterial" class="form-control" title="Quantidade m�nima para controle de estoque">
                                </div>
                                <div class="col-md-3" id="selectRefreshModal_2">
                                    <div id="divRefreshedModal_2">
                                        <label>Categoria</label>

                                        <select class="form-control" name="categoriaMaterial" required>
                                            <option value="" readonly selected>Categoria</option>
                                            <option value="10">Equipamento</option>
                                            <option value="32">Material</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4" id="selectRefreshModal_1">
                                    <div id="divRefreshedModal_1">

                                        <label for="marcaMaterial">Marca</label>
                                        <div class="input-group">
                                          <?php
                                          $consultaMarcaMaterial = $this->medoo->select("marca_material", ["cod_marca", "nome_marca"], ["ORDER" => "nome_marca"]);

                                          echo('<select name="marcaMaterial" id="marcaMenu" class="form-control"
                                                  required data-validation-required-message=" Campo precisa ser preenchido"
                                                  data-bv-row=".col-lg-4"
                                                  data-fv-notempty="true"
                                                  data-fv-notempty-message="Selecione a unidade de medida.">');

                                          echo('<option selected="selected" value="" disabled="disabled">Marca</option>');
                                          foreach ($consultaMarcaMaterial as $value) {
                                              echo "<option value='{$value['cod_marca']}' >".strtoupper($value['nome_marca'])."</option>";
                                          }
                                          echo('</select>');
                                          ?>
                                          <span class="input-group-btn">
                                                   <button class="btn btn-default" type="button" data-toggle="modal" data-target="#marcaModal"><i class="fa fa-plus fa-fw"></i></button>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label>Descri��o</label>
                                    <input type="text" name="descricaoResumidaMaterial" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>C�digo de Barras (EAN)</label>
                                    <input type="text" name="codigoBarrasMaterial" class="form-control">
                                </div>
                            </div>
                            <div class="row">


                            </div>
                            <div class="row" style="margin-top: 15px">
                                <div class="col-md-4">
                                    <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                        <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                                    </button>
                                    <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>



<!-- TABELAS DE MATERIAIS PARA MR-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h4>MATERIAIS MR</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">


                <!-- Abas  TUE e VLT -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#AbaVLT" aria-controls="profile" role="tab" data-toggle="tab">VLT</a></li>
                            <li role="presentation"><a href="#AbaTUE" aria-controls="profile" role="tab" data-toggle="tab">TUE</a></li>
                        </ul>

                        <br />
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- VLT -->
                            <div role="tabpanel" class="tab-pane fade in active" id="AbaVLT">

                                <div class="row">
                                    <div class="col-md-12" style="height: 300px; overflow-y: scroll">
                                        <table id="tableMateriaisMr" class="table table-striped table-responsive tableMateriaisMr">

                                            <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Unid. Medida</th>
                                                <th>Qtd. Minima</th>
                                                <th>Categoria</th>
                                                <th>Marca</th>
                                                <th>Descri��o</th>
                                                <th>EAN</th>
                                                <th>Grupo</th>
                                                <th>A��o</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT *
                                                    FROM v_material
                                                    WHERE cod_grupo = 22 AND (cod_categoria = 10 OR cod_categoria = 32)
                                                    ORDER BY cod_material DESC
                                                    ";

                                            $lista_materiais = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($lista_materiais)) {

                                                foreach ($lista_materiais as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_material']}'>{$value['nome_material']}</td>";
                                                    echo "<td data-value='{$value['cod_uni_medida']}'>{$value['nome_uni_medida']}</td>"; //unid. medida
                                                    echo "<td data-value='{$value['quant_min']}'>{$value['quant_min']}</td>";
                                                    echo "<td data-value='{$value['cod_categoria']}'>{$value['nome_categoria']}</td>";
                                                    echo "<td data-value='{$value['cod_marca']}'>{$value['nome_marca']}</td>";
                                                    echo "<td data-value='{$value['descricao_material']}'>{$value['descricao_material']}</td>";
                                                    echo "<td data-value='{$value['ean']}'>{$value['ean']}</td>";
                                                    echo "<td data-value='{$value['cod_grupo']}'>{$value['nome_grupo']}</td>";
                                                    echo "<td><button type='button' data-value='{$value['cod_material']}' data-grupo='{$value['cod_grupo']}' title='Editar Material' class='btn btn-default btn-circle editMaterial'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                    echo "</tr>";

                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                            <!-- TUE -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaTUE">

                                <div class="row">
                                    <div class="col-md-12" style="height: 300px; overflow-y: scroll">
                                        <table id="tableMateriaisMr" class="table table-striped table-responsive tableMateriaisMr">

                                            <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Unid. Medida</th>
                                                <th>Qtd. Minima</th>
                                                <th>Categoria</th>
                                                <th>Marca</th>
                                                <th>Descriacao</th>
                                                <th>EAN</th>
                                                <th>Grupo</th>
                                                <th>A��o</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT *
                                                    FROM v_material
                                                    WHERE cod_grupo = 23 AND (cod_categoria = 10 OR cod_categoria = 32)
                                                    ORDER BY cod_material DESC
                                                    ";

                                            $lista_materiais = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($lista_materiais)) {

                                                foreach ($lista_materiais as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_material']}'>{$value['nome_material']}</td>";
                                                    echo "<td data-value='{$value['cod_uni_medida']}'>{$value['nome_uni_medida']}</td>"; //unid. medida
                                                    echo "<td data-value='{$value['quant_min']}'>{$value['quant_min']}</td>";
                                                    echo "<td data-value='{$value['cod_categoria']}'>{$value['nome_categoria']}</td>";
                                                    echo "<td data-value='{$value['cod_marca']}'>{$value['nome_marca']}</td>";
                                                    echo "<td data-value='{$value['descricao_material']}'>{$value['descricao_material']}</td>";
                                                    echo "<td data-value='{$value['ean']}'>{$value['ean']}</td>";
                                                    echo "<td data-value='{$value['cod_grupo']}'>{$value['nome_grupo']}</td>";
                                                    echo "<td><button type='button' data-value='{$value['cod_material']}' data-grupo='{$value['cod_grupo']}' title='Editar Material' class='btn btn-default btn-circle editMaterial'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                                    echo "</tr>";

                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="marcaModal" tabindex="-1" role="form" aria-labelledby="marcaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="marcaModalLabel">Cadastrar Marca</h4>
            </div>

            <form id="formularioModal_1" method="post">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome da marca:</label>
                            <input type="text" name="nomeMarca" class="form-control"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Selecione a unidade de medida.">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Marca</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Modals -->
    <!-- Marca  -->
<div class="modal fade" id="marcaModal" tabindex="-1" role="form" aria-labelledby="marcaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="marcaModalLabel">Cadastrar Marca</h4>
            </div>

            <form id="formularioModal_1" method="post">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome da marca:</label>
                            <input type="text" name="nomeMarca" class="form-control"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Selecione a unidade de medida.">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Marca</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>

        </div>
    </div>
</div>
