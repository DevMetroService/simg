<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>Registro de Od�metro - VLT</h1>
    <small>*S� � permitido n�meros inteiros para registro em banco de dados.</small>
</div>

<form method="post" action="<?php echo HOME_URI . '/cadastroGeral/registrarOdometroVlt'; ?>">
    <div class="row">
        <div class="col-md-12">
            <div class='panel-default panel'>
                <div class="panel-heading" role="tab" id="headingVeiculoFortaleza">
                    <div class="row">
                        <div class="col-xs-3">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorVeiculoFortaleza" aria-expanded="false" aria-controls="collapseVeiculoFortaleza">
                                <button class="btn btn-default"><label>VLT Fortaleza</label></button>
                            </a>
                        </div>
                    </div>
                </div>

                <div id="divIndicadorVeiculoFortaleza" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingVeiculoFortaleza">
                    <div class="panel-body"  id="VeiculoFortaleza">
                        <div class="row">
                            <?php
                            $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo"],
                                ["AND" => ["veiculo.cod_grupo" => (int) 22, "cod_linha" => [1, 6] ], "ORDER" => "nome_veiculo"]);

                            if (!empty($selectVeiculo)) {
                                foreach ($selectVeiculo as $dados) {

                                    $selectCarroMA = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                        ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "s", "cabine" => "s"]]);
                                    $selectCarroMA = $selectCarroMA[0];

                                    $selectCarroMB = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                        ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "n", "cabine" => "s"]]);
                                    $selectCarroMB = $selectCarroMB[0];

                                    $historicoOdometro = $this->medoo->query("SELECT * FROM odometro_vlt JOIN usuario USING(cod_usuario) WHERE cod_veiculo = {$dados['cod_veiculo']} ORDER BY data_cadastro DESC")->fetchAll();
                                    $historicoOdometro = $historicoOdometro[0];

                                    echo "<div class='col-md-4 div{$dados['cod_veiculo']}'>
                                                <div class='panel panel-default'>
                                                    <div class='panel-heading'>
                                                        <div class='huge-mr'>
                                                                {$dados['nome_veiculo']}
                                                                <a data-toggle='modal' data-info='{\"responsavel\":\"{$historicoOdometro['usuario']}\", \"data\": \"{$this->parse_timestamp($historicoOdometro['data_cadastro']) }\"}' data-target='#odometroInfo' class='btn-circle'>
                                                                    <i class='fa fa-user fa-fw' style='font-size:20px;'></i>
                                                                </a>
                                                                <a data-toggle='modal' data-info='{\"tipo\":\"22\", \"veiculo\":\"{$dados['cod_veiculo']}\", \"mes\":\"".date('m')."\", \"ano\":\"".date('Y')."\"}' data-target='#historicoInfo' class='btn-circle'>
                                                                    <i class='fa fa-clock-o fa-fw' style='font-size:20px;'></i>
                                                                </a>
                                                            </div>
                                                    </div>
                                                    <div class='panel-body'>
                                                        <div class='panel panel-default'>
                                                            <div class='panel-heading'><label>Cabine MA</label></div>
                                                            
                                                            <div class='panel-body'>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Od�metro
                                                                    </span>
                                                                    <input name='carroMA{$dados['cod_veiculo']}' value='{$selectCarroMA['cod_carro']}' type='hidden'>
                                                                    <input name='odometroMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['odometro']}' class='number form-control inputOdometro'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Tra��o
                                                                    </span>
                                                                    <input name='hori_tracaoMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['horimetro_tracao']}' class=' number form-control inputOdometro'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Gerador
                                                                    </span>
                                                                    <input name='hori_geradorMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['horimetro_gerador']}' class='number form-control inputOdometro'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='panel panel-default'>
                                                            <div class='panel-heading'><label>Cabine MB</label></div>
                                                            
                                                            <div class='panel-body'>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Od�metro
                                                                    </span>
                                                                    <input name='carroMB{$dados['cod_veiculo']}' value='{$selectCarroMB['cod_carro']}' type='hidden'>
                                                                    <input name='odometroMB{$dados['cod_veiculo']}' placeholder='{$selectCarroMB['odometro']}' class='number form-control inputOdometro'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Tra��o
                                                                    </span>
                                                                    <input name='hori_tracaoMB{$dados['cod_veiculo']}' placeholder='{$selectCarroMB['horimetro_tracao']}' class='number form-control inputOdometro'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Gerador
                                                                    </span>
                                                                    <input name='hori_geradorMB{$dados['cod_veiculo']}' placeholder='{$selectCarroMB['horimetro_gerador']}' class='number form-control inputOdometro'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class='panel-default panel'>
                <div class="panel-heading" role="tab" id="headingVeiculoCariri">
                    <div class="row">
                        <div class="col-xs-3">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorVeiculoCariri" aria-expanded="false" aria-controls="collapseVeiculoCariri">
                                <button class="btn btn-default"><label>VLT Cariri</label></button>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="divIndicadorVeiculoCariri" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingVeiculoCariri">
                    <div class="panel-body"  id="VeiculoCariri">
                        <div class="row">
                            <?php
                            $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo", "disponibilidade"],
                                ["AND" => ["veiculo.cod_grupo" => (int) 22, "cod_linha" => 2], "ORDER" => "nome_veiculo"]);

                            if (!empty($selectVeiculo)) {
                                foreach ($selectVeiculo as $dados) {

                                    $selectCarroMA = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                        ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "s", "cabine" => "s"]]);
                                    $selectCarroMA = $selectCarroMA[0];

                                    $selectCarroMB = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                        ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "n", "cabine" => "s"]]);
                                    $selectCarroMB = $selectCarroMB[0];

                                    $historicoOdometro = $this->medoo->query("SELECT * FROM odometro_vlt JOIN usuario USING(cod_usuario) WHERE cod_veiculo = {$dados['cod_veiculo']} ORDER BY data_cadastro DESC")->fetchAll();
                                    $historicoOdometro = $historicoOdometro[0];

                                    echo "<div class='col-md-4 div{$dados['cod_veiculo']}'>
                                            <div class='panel panel-default'>
                                                <div class='panel-heading'>
                                                    <div class='huge-mr'>
                                                        {$dados['nome_veiculo']}
                                                        <a data-toggle='modal' data-info='{\"responsavel\":\"{$historicoOdometro['usuario']}\", \"data\": \"{$this->parse_timestamp($historicoOdometro['data_cadastro']) }\"}' data-target='#odometroInfo' class='btn-circle'>
                                                            <i class='fa fa-user fa-fw' style='font-size:20px;'></i>
                                                        </a>
                                                        <a data-toggle='modal' data-info='{\"tipo\":\"22\", \"veiculo\":\"{$dados['cod_veiculo']}\", \"mes\":\"".date('m')."\", \"ano\":\"".date('Y')."\"}' data-target='#historicoInfo' class='btn-circle'>
                                                            <i class='fa fa-clock-o fa-fw' style='font-size:20px;'></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class='panel-body'>
                                                    <div class='input-group'>
                                                        <span class='input-group-addon'>
                                                            Od�metro Cabine MA
                                                        </span>
                                                        <input name='carroMA{$dados['cod_veiculo']}' value='{$selectCarroMA['cod_carro']}' type='hidden'>
                                                        <input name='odometroMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['odometro']}' class=' number form-control inputOdometro'>
                                                    </div>
                                                    <div class='input-group'>
                                                        <span class='input-group-addon'>
                                                            Horimetro Tra��o
                                                        </span>
                                                        <input name='hori_tracaoMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['horimetro_tracao']}' class='number form-control inputOdometro'>
                                                    </div>
                                                    <div class='input-group'>
                                                        <span class='input-group-addon'>
                                                            Horimetro Gerador
                                                        </span>
                                                        <input name='hori_geradorMB{$dados['cod_veiculo']}'  placeholder='{$selectCarroMB['horimetro_gerador']}' class='number form-control inputOdometro'>
                                                    </div>
                                                    <div class='input-group'>
                                                        <span class='input-group-addon'>
                                                            Od�metro Cabine MB
                                                        </span>
                                                        <input name='carroMB{$dados['cod_veiculo']}' value='{$selectCarroMB['cod_carro']}' type='hidden'>
                                                        <input name='odometroMB{$dados['cod_veiculo']}' placeholder='{$selectCarroMB['odometro']}' class='number form-control inputOdometro'>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class='panel-default panel'>
                <div class="panel-heading" role="tab" id="headingVeiculoSobral">
                    <div class="row">
                        <div class="col-xs-3">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorVeiculoSobral" aria-expanded="false" aria-controls="collapseVeiculoSobral">
                                <button class="btn btn-default"><label>VLT Sobral</label></button>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="divIndicadorVeiculoSobral" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingVeiculoSobral">
                    <div class="panel-body"  id="VeiculoSobral">
                        <div class="row">
                            <?php
                            $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo", "disponibilidade"],
                                ["AND" => ["veiculo.cod_grupo" => (int) 22, "cod_linha" => 7], "ORDER" => "nome_veiculo"]);

                            if (!empty($selectVeiculo)) {
                                foreach ($selectVeiculo as $dados) {

                                    $selectCarroMA = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                        ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "s", "cabine" => "s"]]);
                                    $selectCarroMA = $selectCarroMA[0];

                                    $selectCarroMB = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                        ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "n", "cabine" => "s"]]);
                                    $selectCarroMB = $selectCarroMB[0];

                                    $historicoOdometro = $this->medoo->query("SELECT * FROM odometro_vlt JOIN usuario USING(cod_usuario) WHERE cod_veiculo = {$dados['cod_veiculo']} ORDER BY data_cadastro DESC")->fetchAll();
                                    $historicoOdometro = $historicoOdometro[0];

                                    echo "<div class='col-md-4 div{$dados['cod_veiculo']}'>
                                                    <div class='panel panel-default'>
                                                        <div class='panel-heading'>
                                                            <div class='huge-mr'>
                                                                {$dados['nome_veiculo']}
                                                                <a data-toggle='modal' data-info='{\"responsavel\":\"{$historicoOdometro['usuario']}\", \"data\": \"{$this->parse_timestamp($historicoOdometro['data_cadastro']) }\"}' data-target='#odometroInfo' class='btn-circle'>
                                                                    <i class='fa fa-user fa-fw' style='font-size:20px;'></i>
                                                                </a>
                                                                <a data-toggle='modal' data-info='{\"tipo\":\"22\", \"veiculo\":\"{$dados['cod_veiculo']}\", \"mes\":\"".date('m')."\", \"ano\":\"".date('Y')."\"}' data-target='#historicoInfo' class='btn-circle'>
                                                                    <i class='fa fa-clock-o fa-fw' style='font-size:20px;'></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class='panel-body'>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Od�metro Cabine MA
                                                                </span>
                                                                <input name='carroMA{$dados['cod_veiculo']}' value='{$selectCarroMA['cod_carro']}' type='hidden'>
                                                                <input name='odometroMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['odometro']}' class='number form-control inputOdometro'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Horimetro Tra��o
                                                                </span>
                                                                <input name='hori_tracaoMA{$dados['cod_veiculo']}' placeholder='{$selectCarroMA['horimetro_tracao']}' class='number form-control inputOdometro'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Horimetro Gerador
                                                                </span>
                                                                <input name='hori_geradorMB{$dados['cod_veiculo']}' placeholder='{$selectCarroMB['horimetro_gerador']}' class='number form-control inputOdometro'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Od�metro Cabine MB
                                                                </span>
                                                                <input name='carroMB{$dados['cod_veiculo']}' value='{$selectCarroMB['cod_carro']}' type='hidden'>
                                                                <input name='odometroMB{$dados['cod_veiculo']}' placeholder='{$selectCarroMB['odometro']}' class='number form-control'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 5%;">
        <div class="col-md-offset-10 col-md-2">
            <button class="btn btn-default btn-lg btn-block" type="submit"><i class="fa fa-save fa-fw"></i> SALVAR</button>
        </div>
    </div>

    <div class="modal fade" id="odometroInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i> </span></button>
                    <h4 class="modal-title">Registro de Od�metro</h4>
                </div>
                <div class="modal-body">
                    <p>Realizado por:</p>
                    <label id="responsavel"></label>
                    <p>Em:</p>
                    <label id="dataRegistro"></label>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="historicoInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i> </span></button>
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="modal-title">Hist�rico de Od�metro</h4>
                        </div>
                        <div class="col-md-5">
                            <select id="mesHistorico" class="form-control">
                                <?php
                                foreach (MainController::$monthComplete as $key => $value) {
                                    if(date('m') == $key)
                                        echo("<option value='{$key}' selected>$value</option>");
                                    else
                                        echo("<option value='{$key}'>$value</option>");
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row table-responsive">
                        <div class="col-lg-12">
                            <table id="table" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Usu�rio</th>
                                    <th>Od�metro MA</th>
                                    <th>Od�metro MB</th>
                                    <th>Data de Altera��o</th>
                                    <th>Horimetro Tra��o MA</th>
                                    <th>Horimetro Gerador MA</th>
                                    <th>Horimetro Tra��o MB</th>
                                    <th>Horimetro Gerador MB</th>
                                </tr>
                                </thead>
                                <tbody id="bodyHistoricoInfo">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>