<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 21/01/2019
 * Time: 14:53
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 21/01/2019
 * Time: 12:06
 */
?>

<div class="page-header">
    <h1>Registro de Fichas MR</h1>
    <small>Realiza��o do registro de fichas para cada servi�o destinado a programa��o de Material Rodante.</small>
</div>

<form method="post" id="formRegFicha" class="form-group" action="<?php echo HOME_URI ?>/cadastroGeral/registrarFicha">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>Dados de Registro</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>C�digo</label>
                            <input id="cod_ficha" type="text" readonly class="form-control" name="codigo">
                        </div>
                        <div class="col-md-4">
                            <label>Grupo de Sistema</label>
                            <select id='grupo' class="form-control" name="grupo" required>
                                <option value="">Selecione Grupo</option>
                                <option value="23">Material Rodante TUE</option>
                                <option value="22">Material Rodante VLT</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Sistema</label>
                            <select id='sistema' class="form-control" name="sistema">
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>SubSistema</label>
                            <select id='subsistema' class="form-control" name="subsistema">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Servi�o</label>
                            <select id='servico'  class="form-control" name="servico" required>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Denomina��o</label>
                            <input id='denominacao' type="text" class="form-control" required name="denominacao">
                        </div>
                        <div class="col-md-3">
                            <label>Nome da Ficha</label>
                            <input id='ficha' type="text" class="form-control" required name="ficha">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Dados Registrados</label></div>
            <div class="panel-body">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-12">
                        <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>C�DIGO</th>
                                <th>DENOMICA��O</th>
                                <th>GRUPO DE SISTEMA</th>
<!--                                <th>SISTEMA</th>-->
<!--                                <th>SUBSISTEMA</th>-->
                                <th>SERVI�O</th>
                                <th>NOME DA FICHA</th>
                                <th>A��ES</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $fichas = $this->medoo->select('ficha_material_rodante', [
                                '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                '[><]grupo' => 'cod_grupo',
//                                '[><]sub_sistema' => 'cod_sub_sistema',
//                                '[><]sistema' => 'cod_sistema',
//                                '[><]subsistema' => 'cod_subsistema'
                            ],[
                                'ficha_material_rodante.cod_ficha_material_rodante', 'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'grupo.nome_grupo', 'grupo.cod_grupo', 'servico_material_rodante.nome_servico_material_rodante', 'servico_material_rodante.cod_servico_material_rodante'
                            ],['ORDER' => 'numero_ficha']);

                            foreach($fichas as $dados=>$value){
                                echo ("<tr>
                                    <td>{$value['cod_ficha_material_rodante']}</td>
                                    <td>{$value['denominacao']}</td>
                                    <td data-value='{$value['cod_grupo']}'>{$value['nome_grupo']}</td>");
//                                    <td>{$value['nome_sistema']}</td>
//                                    <td>{$value['nome_subsistema']}</td>
                                    echo("<td data-value='{$value['cod_servico_material_rodante']}'>{$value['nome_servico_material_rodante']}</td>
                                    <td>{$value['numero_ficha']}</td>
                                    <td>
                                        <button type='button' class='text-center btn btn-default btn-circle btnEditar'><i class='fa fa-file-o fa-fw'></i></button>
                                    </td>
                                </tr>");
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>