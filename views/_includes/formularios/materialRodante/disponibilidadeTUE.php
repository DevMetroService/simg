<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Disponibilidade TUE</div>

            <form method="post" action="<?php echo HOME_URI . 'cadastroGeral/disponibilidadeTue'; ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                        <?php
                        $selectVeiculo = $this->medoo->select("veiculo", ["[><]carro" => "cod_veiculo"],
                            ["cod_veiculo", "nome_veiculo", "odometro", "disponibilidade"],
                            ["AND" => ["veiculo.cod_grupo" => (int) 23, "carro.carro_lider" => "s"], "ORDER" => "nome_veiculo"]);

                        if (!empty($selectVeiculo)) {
                            foreach ($selectVeiculo as $dados) {
                                $checked = '';
                                switch ($dados['disponibilidade']){
                                    case 'n': //Imobilizado
                                        $panel = "danger";
                                        $option = "<option value='i'>Inoperante</option><option value='n' selected>Imobilizado</option>";
                                        break;
                                    case 's': //Dispon�vel
                                        $panel = "success";
                                        $option = "<option value='s' selected>Dispon�vel</option>";
                                        $checked = 'checked';
                                        break;
                                    case 'i': //Inoperante
                                        $panel = "default";
                                        $option = "<option value='i' selected>Inoperante</option><option value='n'>Imobilizado</option>";
                                        break;
                                    default: //Sem informa��o
                                        $panel = "warning";
                                        $option = "<option disabled selected>Sem informa��o</option><option value='i'>Inoperante</option><option value='n'>Imobilizado</option>";
                                }
                                echo "<div class='col-md-3 div{$dados['cod_veiculo']}'>
                                        <div class='panel panel-{$panel}'>
                                            <div class='panel-heading'>
                                                <div class='huge-mr'>{$dados['nome_veiculo']}</div>
                                            </div>
                                            <div class='panel-body'>
                                                <div class='input-group'>
                                                    <span class='input-group-addon'>
                                                        Od�metro
                                                    </span>
                                                    <input value='{$dados['odometro']}' placeholder='{$dados['odometro']}' name='odometro/{$dados['cod_veiculo']}' class='form-control'>
                                                </div>
                                            </div>
                                            
                                            <div class='panel-footer'>
                                                <label>Disponibilidade</label>
                                                <div class='input-group'>
                                                    <span class='input-group-addon'>
                                                        <input type='checkbox' name='check' value='{$dados['cod_veiculo']}' {$checked}>
                                                    </span>
                                                    <select identific='{$dados['cod_veiculo']}' class='form-control' style='font-weight: bold; background-color: #f5f5f5' name='disponibilidade/{$dados['cod_veiculo']}'>
                                                        {$option}
                                                    </select>
                                                </div>
                                                <div class='clearfix'></div>
                                            </div>
                                        </div>
                                    </div>";
                            }
                        }
                        ?>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-default btn-lg" type="submit">
                                <i class="fa fa-floppy-o fa-fw"></i>
                                Gravar Altera��es
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>