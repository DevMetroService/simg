<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 14/01/2019
 * Time: 11:51
 */
?>

<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>Registro de Frota</h1>
    <small>Registro de frota em ve�culos.</small>
</div>

<form method="post" id="formFrota" action="<?php echo HOME_URI . '/dashboardGeral/registroFrota'; ?>" class="form-group">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <label> Dados Ve�culo</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Ve�culo</label>
                            <input id="nomeVeiculo" name="nomeVeiculo" readonly class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label>Linha</label>
                            <input id="nomeLinha" name="nomeLinha" readonly class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label>Grupo</label>
                            <input id="nomeGrupo" name="nomeGrupo" readonly class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label>Frota</label>
                            <input type="text" id="frota" name="frota" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-3">
                            <button type="button" id='btnSave' class="btn btn-default btn-lg"><i class="fa fa-save fa-fw"></i> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>Tabela de ve�culos</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table" class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Ve�culo</th>
                                    <th>Linha</th>
                                    <th>Grupo</th>
                                    <th>Frota</th>
                                    <th>A��o</th>
                                </tr>
                                </thead>
                                <tbody id="bodyHistoricoInfo">
                                <?php
                                if($dadosVeiculos){
                                    foreach($dadosVeiculos as $dados=>$value){
                                        echo "<tr>";
                                        echo "<td>{$value['nome_veiculo']}</td>";
                                        echo "<td>{$value['nome_linha']}</td>";
                                        echo "<td>{$value['nome_grupo']}</td>";
                                        echo ($value['frota']) ? "<td>{$value['frota']}</td>" : "<td>{Sem Registro}</td>";
                                        echo "<td><button type='button' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button></td>";
                                        echo "</tr>";
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


