<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 13/11/2018
 * Time: 16:13
 */
?>
<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>Registro de Od�metro - TUE</h1>
    <small>*S� � permitido n�meros inteiros para registro em banco de dados.</small>
</div>

<form method="post" action="<?php echo HOME_URI . '/cadastroGeral/registrarOdometroTue'; ?>">

    <?php
    $veiculosTUE = $this->medoo->query("SELECT * FROM carro JOIN veiculo v USING(cod_veiculo) WHERE carro_lider = 's' AND v.cod_grupo = 23 ORDER BY nome_veiculo")->fetchAll();
    $ctd = 0;
    foreach($veiculosTUE as $dados){
        if($ctd == 0){
            echo "<div class='row'>";
        }

        $historicoOdometro = $this->medoo->query("SELECT * FROM odometro_tue JOIN usuario USING(cod_usuario) WHERE cod_veiculo = {$dados['cod_veiculo']} ORDER BY data_cadastro DESC")->fetchAll();
        $historicoOdometro = $historicoOdometro[0];

        echo "<div class='col-lg-2'>
                <label>{$dados['nome_veiculo']}
                    <a data-toggle='modal' data-info='{\"responsavel\":\"{$historicoOdometro['usuario']}\", \"data\": \"{$this->parse_timestamp($historicoOdometro['data_cadastro']) }\"}' data-target='#odometroInfo' class='btn-circle'>
                        <i class='fa fa-user fa-fw' style='font-size:20px;'></i>
                    </a>
                    <a data-toggle='modal' data-info='{\"tipo\":\"23\", \"veiculo\":\"{$dados['cod_veiculo']}\", \"mes\":\"".date('m')."\", \"ano\":\"".date('Y')."\"}' data-target='#historicoInfo' class='btn-circle'>
                        <i class='fa fa-clock-o fa-fw' style='font-size:20px;'></i>
                    </a>
                </label>
                <input type='text' class='form-control number inputOdometro' name='odometro{$dados['cod_veiculo']}' placeholder='{$dados['odometro']}'>
            </div>";


        if($ctd == 5){
            echo "</div>";
        }

        $ctd++;
        if($ctd == 6){
            $ctd = 0;
        }
    }
    ?>
    <div class="row" style="margin-top: 5%;">
        <div class="col-md-offset-10 col-md-2">
            <button class="btn btn-default btn-lg btn-block" type="submit"><i class="fa fa-save fa-fw"></i> SALVAR</button>
        </div>
    </div>
    <div class="modal fade" id="odometroInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i> </span></button>
                    <h4 class="modal-title">Registro de Od�metro</h4>
                </div>
                <div class="modal-body">
                    <p>Realizado por:</p>
                    <label id="responsavel"></label>
                    <p>Em:</p>
                    <label id="dataRegistro"></label>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="historicoInfo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i> </span></button>
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="modal-title">Hist�rico de Od�metro</h4>
                        </div>
                        <div class="col-md-4">
                            <select id="mesHistorico" class="form-control">
                                <?php
                                foreach (MainController::$monthComplete as $key => $value) {
                                    if(date('m') == $key)
                                        echo("<option value='{$key}' selected>$value</option>");
                                    else
                                        echo("<option value='{$key}'>$value</option>");
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <table id="table" class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Usu�rio</th>
                            <th>Od�metro</th>
                            <th>Data de Altera��o</th>
                        </tr>
                        </thead>
                        <tbody id="bodyHistoricoInfo">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>