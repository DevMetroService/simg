<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 21/01/2019
 * Time: 12:06
 */
?>

<div class="page-header">
    <h1>Registro de Procediementos MR</h1>
    <small>O material � exibido com o mesmo nome do procedimento indicado, com o indicador "-m" ao final do nome do arquivo.</small>
</div>

<form id="formRegProc" class="form-group" action="<?php echo HOME_URI ?>/cadastroGeral/registrarProcedimento" METHOD="post">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>Dados de Registro</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>C�digo</label>
                            <input id="cod_servico" type="text" readonly class="form-control" name="codigo">
                        </div>
                        <div class="col-md-3">
                            <label>Grupo de Sistema</label>
                            <select id="grupo" class="form-control" name="grupo" required>
                                <option value="">Selecione Grupo</option>
                                <option value="23">Material Rodante TUE</option>
                                <option value="22">Material Rodante VLT</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Servi�o</label>
                            <input id="servico" type="text" class="form-control" name="servico" required>
                        </div>
                        <div class="col-md-3">
                            <label>Procedimento</label>
                            <input id="procedimento" type="text" class="form-control" required name="procedimento">
                        </div>
                        <div class="col-md-1">
                            <label>Ativo</label>
                            <input id="ativo" type="checkbox" class="form-control" checked="checked" name="ativo">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Dados Registrados</label></div>
            <div class="panel-body">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-12">
                        <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>C�digo</th>
                                <th>GRUPO DE SISTEMA</th>
                                <th>SERVI�O</th>
                                <th>PROCEDIMENTO</th>
                                <th>ATIVO</th>
                                <th>A��ES</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $procedimentos = $this->medoo->select('servico_material_rodante',
                                [
                                        '[><]grupo' => "cod_grupo"
                                ], [
                                    'servico_material_rodante.cod_servico_material_rodante', 'grupo.nome_grupo', 'grupo.cod_grupo', 'servico_material_rodante.nome_servico_material_rodante',  'servico_material_rodante.procedimento', 'servico_material_rodante.ativo'
                                ]);

                            foreach($procedimentos as $dados=>$value){
                                echo("
                                    <tr>
                                        <td>{$value['cod_servico_material_rodante']}</td>
                                        <td data-value='{$value['cod_grupo']}'>{$value['nome_grupo']}</td>
                                        <td>{$value['nome_servico_material_rodante']}</td>
                                        <td>{$value['procedimento']}</td>
                                        <td data-value='{$value['ativo']}'>");
                                   echo $value['ativo'] == 's' ? "Ativo" : "Desativado";
                                   echo("</td>
                                        <td>
                                            <button type='button' class='text-center btn btn-default btn-circle btnEditar'><i class='fa fa-file-o fa-fw'></i></button>
                                        </td>
                                    </tr>
                                ");
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
