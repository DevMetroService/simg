<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 14/01/2019
 * Time: 11:51
 */
?>

<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>Registro de Disponibilidade</h1>
    <small>Registro da Disponibilidade de ve�culos.</small>
</div>

<form method="post" id="formFrota" action="<?php echo HOME_URI . '/dashboardGeral/registroDisponibilidade'; ?>" class="form-group">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <label> Dados Ve�culo</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Ve�culo</label>
                            <input id="nomeVeiculo" name="nomeVeiculo" readonly class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label>Linha</label>
                            <input id="nomeLinha" name="nomeLinha" readonly class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label>Grupo</label>
                            <input id="nomeGrupo" name="nomeGrupo" readonly class="form-control" type="text">
                        </div>
                        <div class="col-md-3">
                            <label>Disponibilidade</label>
                            <select id="disponibilidade" name="disponibilidade" class="form-control" required>
                                <option value="">Selecione uma op��o</option>
                                <option value="s">Dispon�vel</option>
                                <option value="n">Imobilizado</option>
                                <option value="i">Inoperante</option>
                            </select>
                        </div>
                    </div>
<!--                    <div class="row" id="observacao" style="display: none">-->
<!--                        <div class="col-md-12">-->
<!--                            <label>Observa��o</label>-->
<!--                            <textarea class="form-control" rows="3" spellcheck="true"></textarea>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-3">
                            <button type="button" id='btnSave' class="btn btn-default btn-lg"><i class="fa fa-save fa-fw"></i> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ve�culos</div>
                <div class="panel-body">
                    <div>
                        <!-- Nav tabs -->
                        <ul id='dispTab' class="nav nav-tabs" role="tablist">
                            <?php
                            $first = true;
                            foreach($frotas as $key => $value){
                                if($first) {
                                    echo("<li role='presentation' class='active'><a href='#{$value['frota']}' aria-controls='{$value['frota']}' role='tab' data-toggle='tab'>Frota {$value['case']}</a></li>");
                                    $first = false;
                                }else{
                                    echo("<li role='presentation'><a href='#{$value['frota']}' aria-controls='{$value['frota']}' role='tab' data-toggle='tab'>Frota {$value['case']}</a></li>");
                                }

                            }
                            ?>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                        <?php
                        $first = true;
                        foreach($frotas as $key => $value){
                        ?>

                            <div role="tabpanel" class="tab-pane fade <?php if($first){ echo "in active"; $first = false;}?> " id="<?php echo $value['frota'] ?>">
                                <div class="row" style="margin-top: 25px">
                                    <div class="col-md-12">
                                        <table id="table" class="table table-striped table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Ve�culo</th>
                                                <th>Linha</th>
                                                <th>Disponibilidade</th>
                                                <th>Observa��o</th>
                                                <th>A��o</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php
                                            $dadosVeiculos = $this->medoo->select('veiculo', ["[><]linha"=>"cod_linha"],
                                                '*', ['frota'=>$value['frota'], "ORDER" =>"nome_veiculo"]);

                                            if($dadosVeiculos){
                                                foreach($dadosVeiculos as $dados=>$value){
                                                    switch ($value['disponibilidade']){
                                                        case 's':
                                                            $disp = "Dispon�vel";
                                                            break;
                                                        case 'n':
                                                            $disp = "Imobilizado";
                                                            break;
                                                        case 'i':
                                                            $disp = "Inoperante";
                                                            break;
                                                        default:
                                                            $disp = "Sem Informa��o";
                                                            break;

                                                    }
                                                    echo "<tr>";
                                                    echo "<td>{$value['nome_veiculo']}</td>";
                                                    echo "<td>{$value['nome_linha']}</td>";
                                                    echo "<td>{$disp}</td>";
                                                    echo "<td></td>";
                                                    echo "<td>
                                                            <button type='button' title='Editar Frota' class='btn btn-default btn-circle editFrota'><i class='fa fa-file-o fa-fw'></i></button>
                                                            <button type='button' title='Ver Hist�rico' class='btn btn-default btn-circle historico'
                                                                data-toggle='modal' data-target='#historicoInfo'>
                                                                <i class='fa fa-eye fa-fw'></i>
                                                            </button>
                                                          </td>";
                                                    echo "</tr>";
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<div class="modal fade" id="historicoInfo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i> </span></button>
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="modal-title">Hist�rico de Disponiblidade  <strong>TUE01</strong></h4>
                    </div>
                    <div class="col-md-4">
                        <select id="mesHistorico" class="form-control">
                            <?php
                            foreach (MainController::$monthComplete as $key => $value) {
                                if(date('m') == $key)
                                    echo("<option value='{$key}' selected>$value</option>");
                                else
                                    echo("<option value='{$key}'>$value</option>");
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <table id="table" class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Usu�rio</th>
                        <th>Disponibilidade</th>
                        <th>Data de Altera��o</th>
                        <th>Observa��o</th>
                    </tr>
                    </thead>
                    <tbody id="bodyHistoricoInfo">
                    <tr>
                        <td>admin.equipeMr</td>
                        <td>INOPERANTE</td>
                        <td>dd/MM/aa hh:mm:ss</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>admin.equipeMr</td>
                        <td>DISPON�VEL</td>
                        <td>dd/MM/aa hh:mm:ss</td>
                        <td>ENTREGUE6 A MANUTEN��O COM O PARA-BRISA DANIFICA��O E INOPERANTE</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->