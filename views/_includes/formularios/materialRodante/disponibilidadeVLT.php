<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Disponibilidade VLT</div>
            <form method="post" action="<?php echo HOME_URI . 'cadastroGeral/disponibilidadeVlt'; ?>">
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-heading"><h3>Disponibilidade VLT Fortaleza</h3></div>

                        <div class="panel-body">
                            <div class="row">
                               <div class="col-md-12">
                                <?php
                                $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo", "disponibilidade"],
                                    ["AND" => ["veiculo.cod_grupo" => (int) 22, "cod_linha" => 1], "ORDER" => "nome_veiculo"]);

                                if (!empty($selectVeiculo)) {
                                    foreach ($selectVeiculo as $dados) {

                                        $selectCarroMA = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                            ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "s", "cabine" => "s"]]);
                                        $selectCarroMA = $selectCarroMA[0];

                                        $selectCarroMB = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                            ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "n", "cabine" => "s"]]);
                                        $selectCarroMB = $selectCarroMB[0];

                                        $checked = '';
                                        switch ($dados['disponibilidade']){
                                            case 'n': //Imobilizado
                                                $panel = "danger";
                                                $option = "<option value='i'>Inoperante</option><option value='n' selected>Imobilizado</option>";
                                                break;
                                            case 's': //Dispon�vel
                                                $panel = "success";
                                                $option = "<option value='s' selected>Dispon�vel</option>";
                                                $checked = 'checked';
                                                break;
                                            case 'i': //Inoperante
                                                $panel = "default";
                                                $option = "<option value='i' selected>Inoperante</option><option value='n'>Imobilizado</option>";
                                                break;
                                            default: //Sem informa��o
                                                $panel = "warning";
                                                $option = "<option disabled selected>Sem informa��o</option><option value='i'>Inoperante</option><option value='n'>Imobilizado</option>";
                                        }
                                        echo "<div class='col-md-4 div{$dados['cod_veiculo']}'>
                                                <div class='panel panel-{$panel}'>
                                                    <div class='panel-heading'>
                                                        <div class='huge-mr'>{$dados['nome_veiculo']}</div>
                                                    </div>
                                                    <div class='panel-body'>
                                                        <div class='panel panel-{$panel}'>
                                                            <div class='panel-heading'><label>Cabine MA</label></div>
                                                            
                                                            <div class='panel-body'>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Od�metro
                                                                    </span>
                                                                    <input name='carroMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['cod_carro']}' type='hidden'>
                                                                    <input name='odometroMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['odometro']}' placeholder='{$selectCarroMA['odometro']}' class='form-control'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Tra��o
                                                                    </span>
                                                                    <input name='hori_tracaoMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['horimetro_tracao']}' placeholder='{$selectCarroMA['horimetro_tracao']}' class='form-control'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Gerador
                                                                    </span>
                                                                    <input name='hori_geradorMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['horimetro_gerador']}' placeholder='{$selectCarroMA['horimetro_gerador']}' class='form-control'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='panel panel-{$panel}'>
                                                            <div class='panel-heading'><label>Cabine MB</label></div>
                                                            
                                                            <div class='panel-body'>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Od�metro
                                                                    </span>
                                                                    <input name='carroMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['cod_carro']}' type='hidden'>
                                                                    <input name='odometroMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['odometro']}' placeholder='{$selectCarroMB['odometro']}' class='form-control'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Tra��o
                                                                    </span>
                                                                    <input name='hori_tracaoMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['horimetro_tracao']}' placeholder='{$selectCarroMB['horimetro_tracao']}' class='form-control'>
                                                                </div>
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        Horimetro Gerador
                                                                    </span>
                                                                    <input name='hori_geradorMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['horimetro_gerador']}' placeholder='{$selectCarroMB['horimetro_gerador']}' class='form-control'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class='panel-footer'>
                                                        <div class='row'>
                                                            <div class='col-md-12'>
                                                                <label>Disponibilidade</label>    
                                                                <div class='input-group'>
                                                                    <span class='input-group-addon'>
                                                                        <input type='checkbox' name='check' value='{$dados['cod_veiculo']}' {$checked}>
                                                                    </span>
                                                                    <select identific='{$dados['cod_veiculo']}' class='form-control' name='disponibilidade/{$dados['cod_veiculo']}' style='font-weight: bold; background-color: #f5f5f5'>
                                                                        {$option}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>                                              
                                                        <div class='clearfix'></div>
                                                    </div>
                                                </div>
                                            </div>";
                                    }
                                }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading"><h3>Disponibilidade VLT Cariri</h3></div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo", "disponibilidade"],
                                        ["AND" => ["veiculo.cod_grupo" => (int) 22, "cod_linha" => 2], "ORDER" => "nome_veiculo"]);

                                    if (!empty($selectVeiculo)) {
                                        foreach ($selectVeiculo as $dados) {

                                            $selectCarroMA = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                                ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "s", "cabine" => "s"]]);
                                            $selectCarroMA = $selectCarroMA[0];

                                            $selectCarroMB = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                                ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "n", "cabine" => "s"]]);
                                            $selectCarroMB = $selectCarroMB[0];

                                            $checked = '';
                                            switch ($dados['disponibilidade']){
                                                case 'n': //Indispon�vel
                                                    $panel = "danger";
                                                    $option = "<option value='i'>Inoperante</option><option value='n' selected>Indispon�vel</option>";
                                                    break;
                                                case 's': //Dispon�vel
                                                    $panel = "success";
                                                    $option = "<option value='s' selected>Dispon�vel</option>";
                                                    $checked = 'checked';
                                                    break;
                                                case 'i': //Inoperante
                                                    $panel = "default";
                                                    $option = "<option value='i' selected>Inoperante</option><option value='n'>Indispon�vel</option>";
                                                    break;
                                                default: //Sem informa��o
                                                    $panel = "warning";
                                                    $option = "<option disabled selected>Sem informa��o</option><option value='i'>Inoperante</option><option value='n'>Indispon�vel</option>";
                                            }
                                            echo "<div class='col-md-4 div{$dados['cod_veiculo']}'>
                                                    <div class='panel panel-{$panel}'>
                                                        <div class='panel-heading'>
                                                            <div class='huge-mr'>{$dados['nome_veiculo']}</div>
                                                        </div>
                                                        <div class='panel-body'>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Od�metro Cabine MA
                                                                </span>
                                                                <input name='carroMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['cod_carro']}' type='hidden'>
                                                                <input name='odometroMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['odometro']}' placeholder='{$selectCarroMA['odometro']}' class='form-control'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Horimetro Tra��o
                                                                </span>
                                                                <input name='hori_tracaoMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['horimetro_tracao']}' placeholder='{$selectCarroMA['horimetro_tracao']}' class='form-control'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Horimetro Gerador
                                                                </span>
                                                                <input name='hori_geradorMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['horimetro_gerador']}' placeholder='{$selectCarroMB['horimetro_gerador']}' class='form-control'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Od�metro Cabine MB
                                                                </span>
                                                                <input name='carroMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['cod_carro']}' type='hidden'>
                                                                <input name='odometroMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['odometro']}' placeholder='{$selectCarroMB['odometro']}' class='form-control'>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class='panel-footer'>
                                                            <label>Disponibilidade</label>
                                                        <div class='input-group'>
                                                            <span class='input-group-addon'>
                                                                <input type='checkbox' name='check' value='{$dados['cod_veiculo']}' {$checked}>
                                                            </span>
                                                            <select identific='{$dados['cod_veiculo']}' class='form-control' name='disponibilidade/{$dados['cod_veiculo']}' style='font-weight: bold; background-color: #f5f5f5'>
                                                                {$option}
                                                            </select>
                                                        </div>
                                                            <div class='clearfix'></div>
                                                        </div>
                                                    </div>
                                                </div>";
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading"><h3>Disponibilidade VLT Sobral</h3></div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo", "disponibilidade"],
                                        ["AND" => ["veiculo.cod_grupo" => (int) 22, "cod_linha" => 7], "ORDER" => "nome_veiculo"]);

                                    if (!empty($selectVeiculo)) {
                                        foreach ($selectVeiculo as $dados) {

                                            $selectCarroMA = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                                ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "s", "cabine" => "s"]]);
                                            $selectCarroMA = $selectCarroMA[0];

                                            $selectCarroMB = $this->medoo->select("carro", ["cod_carro", "odometro", "horimetro_tracao", "horimetro_gerador"],
                                                ["AND" => ["cod_veiculo" => (int) $dados['cod_veiculo'], "carro_lider" => "n", "cabine" => "s"]]);
                                            $selectCarroMB = $selectCarroMB[0];

                                            $checked = '';
                                            switch ($dados['disponibilidade']){
                                                case 'n': //Indispon�vel
                                                    $panel = "danger";
                                                    $option = "<option value='i'>Inoperante</option><option value='n' selected>Indispon�vel</option>";
                                                    break;
                                                case 's': //Dispon�vel
                                                    $panel = "success";
                                                    $option = "<option value='s' selected>Dispon�vel</option>";
                                                    $checked = 'checked';
                                                    break;
                                                case 'i': //Inoperante
                                                    $panel = "default";
                                                    $option = "<option value='i' selected>Inoperante</option><option value='n'>Indispon�vel</option>";
                                                    break;
                                                default: //Sem informa��o
                                                    $panel = "warning";
                                                    $option = "<option disabled selected>Sem informa��o</option><option value='i'>Inoperante</option><option value='n'>Indispon�vel</option>";
                                            }
                                            echo "<div class='col-md-4 div{$dados['cod_veiculo']}'>
                                                    <div class='panel panel-{$panel}'>
                                                        <div class='panel-heading'>
                                                            <div class='huge-mr'>{$dados['nome_veiculo']}</div>
                                                        </div>
                                                        <div class='panel-body'>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Od�metro Cabine MA
                                                                </span>
                                                                <input name='carroMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['cod_carro']}' type='hidden'>
                                                                <input name='odometroMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['odometro']}' placeholder='{$selectCarroMA['odometro']}' class='form-control'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Horimetro Tra��o
                                                                </span>
                                                                <input name='hori_tracaoMA/{$dados['cod_veiculo']}' value='{$selectCarroMA['horimetro_tracao']}' placeholder='{$selectCarroMA['horimetro_tracao']}' class='form-control'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Horimetro Gerador
                                                                </span>
                                                                <input name='hori_geradorMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['horimetro_gerador']}' placeholder='{$selectCarroMB['horimetro_gerador']}' class='form-control'>
                                                            </div>
                                                            <div class='input-group'>
                                                                <span class='input-group-addon'>
                                                                    Od�metro Cabine MB
                                                                </span>
                                                                <input name='carroMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['cod_carro']}' type='hidden'>
                                                                <input name='odometroMB/{$dados['cod_veiculo']}' value='{$selectCarroMB['odometro']}' placeholder='{$selectCarroMB['odometro']}' class='form-control'>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class='panel-footer'>
                                                            <label>Disponibilidade</label>
                                                        <div class='input-group'>
                                                            <span class='input-group-addon'>
                                                                <input type='checkbox' name='check' value='{$dados['cod_veiculo']}' {$checked}>
                                                            </span>
                                                            <select identific='{$dados['cod_veiculo']}' class='form-control' name='disponibilidade/{$dados['cod_veiculo']}' style='font-weight: bold; background-color: #f5f5f5'>
                                                                {$option}
                                                            </select>
                                                        </div>
                                                            <div class='clearfix'></div>
                                                        </div>
                                                    </div>
                                                </div>";
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-default btn-lg">
                                <i class="fa fa-floppy-o fa-fw"></i>
                                Gravar Altera��es
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>