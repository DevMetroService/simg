<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 01/02/2019
 * Time: 10:41
 */
?>

<div class="page-header">
    <h1>Tabela de Registros de SAF's no Periodo Por Frota / Ve�culo</h1>
    <h4><em>Quantitativo de registro de solicita��o feitas pelos reclamantes. N�o nescessariamente falha.</em></h4>
</div>

<div class="row">
    <div class="col-md-2">
        <select id="ano" class="form-control">
            <?php
            for ($i = 0; $i < 4; $i++){
                $op = date('Y') - $i;
                echo $op == $ano ? ("<option value='{$op}' selected>{$op}</option>") : ("<option value='{$op}'>{$op}</option>") ;
            }
            ?>
        </select>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-default changeAno">Alterar</button>
    </div>
</div>

<!-- TUE Linha sul-->
<div class="row" style="margin-top: 25px;">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <a href="" target=""></a>
                        <label>Numero de Ocorr�ncias por TUE - <?php echo $ano; ?></label></br>
                        <?php
                            if($sistemaSul != "*") {
                                $sis = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => $sistemaSul]);
                                echo "<label>Sistema $sis[0]</label>";
                            }else{
                                echo "<label>Todos os Sistemas</label>";
                            }
                        ?>
                    </div>

                    <div class="col-md-4">
                        <select id="sistemaSul" class="form-control">
                            <option value="*">Todos</option>
                            <?php
                            $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema"=>"cod_sistema"],"*", ["cod_grupo"=>23, "ORDER" => "nome_sistema"]);
                            foreach ($selectSistema as $dados=>$value){
                                if($sistemaSul == $value['cod_sistema'] )
                                    echo "<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>";
                                else
                                    echo "<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default changeAno">Alterar</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Ve�culo</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $veiculo = $this->medoo->select("veiculo", "*", ["cod_grupo" => 23,"ORDER" => "nome_veiculo"]);
                            $ctdCol=[];
                            $ctdline=0;

                            if($sistemaSul != '*'){
                                $sistemaWhere = " AND cod_sistema = {$sistemaSul}";
                            }else{
                                $sistemaWhere = " ";
                            }

                            foreach($veiculo as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_veiculo']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_veiculo = {$value['cod_veiculo']} {$sistemaWhere}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }

                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Oeste-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <a href="" target=""></a>
                        <label>Numero de Ocorr�ncias por VLT L. Oeste - <?php echo $ano; ?></label></br>
                        <?php
                        if($sistemaOeste != "*") {
                            $sis = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => $sistemaOeste]);
                            echo "<label>Sistema $sis[0]</label>";
                        }else{
                            echo "<label>Todos os Sistemas</label>";
                        }
                        ?>
                    </div>

                    <div class="col-md-4">
                        <select id="sistemaOeste" class="form-control">
                            <option value="*">Todos</option>
                            <?php
                            $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema"=>"cod_sistema"],"*", ["cod_grupo"=>22, "ORDER" => "nome_sistema"]);
                            foreach ($selectSistema as $dados=>$value){
                                if($sistemaOeste == $value['cod_sistema'] )
                                    echo "<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>";
                                else
                                    echo "<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default changeAno">Alterar</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Ve�culo</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $veiculo = $this->medoo->select("veiculo", "*", ["AND" => ["cod_grupo" => 22, "cod_linha"=>1],"ORDER" => "nome_veiculo"]);
                            $ctdCol=[];
                            $ctdline=0;

                            if($sistemaOeste != '*'){
                                $sistemaWhere = " AND cod_sistema = {$sistemaOeste}";
                            }else{
                                $sistemaWhere = " ";
                            }

                            foreach($veiculo as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_veiculo']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_veiculo = {$value['cod_veiculo']} {$sistemaWhere}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Parangaba-Mucuripe-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <a href="" target=""></a>
                        <label>Numero de Ocorr�ncias por VLT L. Parangaba Mucuripe - <?php echo $ano; ?></label></br>
                        <?php
                        if($sistemaPM != "*") {
                            $sis = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => $sistemaPM]);
                            echo "<label>Sistema $sis[0]</label>";
                        }else{
                            echo "<label>Todos os Sistemas</label>";
                        }
                        ?>
                    </div>

                    <div class="col-md-4">
                        <select id="sistemaPM" class="form-control">
                            <option value="*">Todos</option>
                            <?php
                            $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema"=>"cod_sistema"],"*", ["cod_grupo"=>22, "ORDER" => "nome_sistema"]);
                            foreach ($selectSistema as $dados=>$value){
                                if($sistemaPM == $value['cod_sistema'] )
                                    echo "<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>";
                                else
                                    echo "<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default changeAno">Alterar</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Ve�culo</th>
                                    <?php
                                    foreach($this->month as $dados=>$value){
                                        echo "<th>{$value}</th>";
                                    }
                                    ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $veiculo = $this->medoo->select("veiculo", "*", ["AND" => ["cod_grupo" => 22, "cod_linha"=>6],"ORDER" => "nome_veiculo"]);
                            $ctdCol=[];
                            $ctdline=0;

                            if($sistemaPM != '*'){
                                $sistemaWhere = " AND cod_sistema = {$sistemaPM}";
                            }else{
                                $sistemaWhere = " ";
                            }

                            foreach($veiculo as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_veiculo']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_veiculo = {$value['cod_veiculo']} {$sistemaWhere}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Cariri-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <a href="" target=""></a>
                        <label>Numero de Ocorr�ncias por VLT L. Cariri - <?php echo $ano; ?></label></br>
                        <?php
                        if($sistemaCariri != "*") {
                            $sis = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => $sistemaCariri]);
                            echo "<label>Sistema $sis[0]</label>";
                        }else{
                            echo "<label>Todos os Sistemas</label>";
                        }
                        ?>
                    </div>

                    <div class="col-md-4">
                        <select id="sistemaCariri" class="form-control">
                            <option value="*">Todos</option>
                            <?php
                            $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema"=>"cod_sistema"],"*", ["cod_grupo"=>22, "ORDER" => "nome_sistema"]);
                            foreach ($selectSistema as $dados=>$value){
                                if($sistemaCariri == $value['cod_sistema'] )
                                    echo "<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>";
                                else
                                    echo "<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default changeAno">Alterar</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Ve�culo</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $veiculo = $this->medoo->select("veiculo", "*", ["AND" => ["cod_grupo" => 22, "cod_linha"=>2],"ORDER" => "nome_veiculo"]);
                            $ctdCol=[];
                            $ctdline=0;

                            if($sistemaCariri != '*'){
                                $sistemaWhere = " AND cod_sistema = {$sistemaCariri}";
                            }else{
                                $sistemaWhere = " ";
                            }

                            foreach($veiculo as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_veiculo']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_veiculo = {$value['cod_veiculo']} {$sistemaWhere}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--VLT Linha Sobral-->
<div class="row">
    <div class="col-md-12">
        <div class="panle panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <a href="" target=""></a>
                        <label>Numero de Ocorr�ncias por VLT L. Sobral - <?php echo $ano; ?></label></br>
                        <?php
                        if($sistemaSobral != "*") {
                            $sis = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => $sistemaSobral]);
                            echo "<label>Sistema $sis[0]</label>";
                        }else{
                            echo "<label>Todos os Sistemas</label>";
                        }
                        ?>
                    </div>

                    <div class="col-md-4">
                        <select id="sistemaSobral" class="form-control">
                            <option value="*">Todos</option>
                            <?php
                            $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema"=>"cod_sistema"],"*", ["cod_grupo"=>22, "ORDER" => "nome_sistema"]);
                            foreach ($selectSistema as $dados=>$value){
                                if($sistemaSobral == $value['cod_sistema'] )
                                    echo "<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>";
                                else
                                    echo "<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-default changeAno">Alterar</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="tblOcorrencias" class="table table-striped table-bordered no-footer tblOcorrencias">
                            <thead>
                            <tr role="row">
                                <th>Ve�culo</th>
                                <?php
                                foreach($this->month as $dados=>$value){
                                    echo "<th>{$value}</th>";
                                }
                                ?>
                                <th><strong>TOTAL</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $veiculo = $this->medoo->select("veiculo", "*", ["AND" => ["cod_grupo" => 22, "cod_linha"=>7],"ORDER" => "nome_veiculo"]);
                            $ctdCol=[];
                            $ctdline=0;

                            if($sistemaSobral != '*'){
                                $sistemaWhere = " AND cod_sistema = {$sistemaSobral}";
                            }else{
                                $sistemaWhere = " ";
                            }

                            foreach($veiculo as $dados=>$value){
                                echo ("<tr>");
                                echo ("<td>{$value['nome_veiculo']}</td>");

                                foreach ($this->month as $key=>$val){

                                    $sql = "SELECT COUNT(vsaf.cod_saf) AS valor 
                                            FROM
                                            (
                                                SELECT 
                                                    date_part('month', data_abertura) AS mes, 
                                                    date_part('year', data_abertura) AS ano, * 
                                                FROM v_saf
                                            ) AS vsaf
                                            WHERE mes = {$key} AND ano = {$ano} AND cod_veiculo = {$value['cod_veiculo']} {$sistemaWhere}";
//
                                    $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                    $valor = $resultado[0]['valor'];


                                    echo ("<td>{$valor}</td>");
                                    $ctdline += $valor;

                                    $ctdCol[$key] += $valor;
                                }
                                echo ("<td>{$ctdline}</td>");
                                $ctdline = 0;

                                echo ("</tr>");
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><strong>TOTAL</strong></th>
                                <?php
                                foreach($this->month as $key=>$value){
                                    echo("<th>{$ctdCol[$key]}</th>");
                                }
                                ?>
                                <th>-</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>