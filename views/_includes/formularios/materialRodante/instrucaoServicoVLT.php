<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 21/01/2019
 * Time: 11:31
 */
?>

<div class="page-header">
    <h1>Consulta de Instru��o de Servi�o VLT</h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Instru��o de Servi�o</label></div>
            <div class="panel-body">
                <div class="row">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Por Servi�o</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Por Denomina��o/Ficha</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>SERVI�O</th>
                                            <th>PROCEDIMENTO</th>
                                            <th>MATERIAL</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $procedimentos = $this->medoo->select('servico_material_rodante',
                                            [
                                                '[><]grupo' => "cod_grupo"
                                            ], [
                                                'grupo.nome_grupo', 'servico_material_rodante.nome_servico_material_rodante', 'servico_material_rodante.procedimento', 'servico_material_rodante.ativo'
                                            ],[
                                                'cod_grupo' => '22'
                                            ]);

                                        foreach($procedimentos as $dados=>$value){
                                            echo("<tr>
                                                    <td>{$value['nome_servico_material_rodante']}</td>
                                                    <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/VLT/" . strtoupper($value['procedimento']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                                    <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/VLT/" . strtoupper($value['procedimento'].'-m') ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                                </tr>
                                            ");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>Ficha</th>
                                            <th>Denomica��o</th>
                                            <th>Periodicidade</th>
                                            <th>PDF</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $fichas = $this->medoo->select('ficha_material_rodante', [
                                            '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                        ],[
                                            'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'servico_material_rodante.nome_servico_material_rodante'
                                        ],[
                                            'AND'=>[
                                                'servico_material_rodante.ativo' => 's',
                                                'cod_grupo' => '22'
                                            ], 'ORDER' => 'numero_ficha'
                                        ]);

                                        foreach($fichas as $dados=>$value){
                                            echo ("<tr>
                                                <td>{$value['numero_ficha']}</td>
                                                <td>{$value['denominacao']}</td>
                                                <td>{$value['nome_servico_material_rodante']}</td>
                                                <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/VLT/FICHA/" . strtoupper($value['numero_ficha']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                            </tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

