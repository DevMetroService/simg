<?php
/*
"Autorizada"
"Cancelada"
"Pendente"
"Encerrada"
"Programada"
"Agendada"
"N�o Validado"
"An�lise"
"Aguardando material"
"Pend�ncia (Compra Negada)"
*/
$statusBlock = [5, 15, 16, 7, 26, 28, 36, 39, 42, 43];
if (!empty($ssm) && (in_array($ssm['cod_status'], $statusBlock) ) )
    $_SESSION['bloqueio'] = true;
?>

<div class="page-header">
    <h1>SSM - Solicita��o de Servi�o da Manuten��o</h1>
</div>

<div class="row">
    <!--  Se for diferente de Encaminhada, Pendente ou Reprovada. Encaminhar SSM. Se n�o, gerar OSM.  -->
    <form id="formSsm" class="form-group" method="post"
          action="<?php echo ($ssm['cod_status'] != 12 && $ssm['cod_status'] != 15 && $ssm['cod_status'] != 36 && $ssm['cod_status'] != 42 && $ssm['cod_status'] != 43)? HOME_URI . '/cadastroGeral/encaminharSsm' : HOME_URI . '/OSM/gerarOsm' ?>">

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>SSM</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Origem</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Tipo de Interven��o</label>
                                            <select class="form-control" name="tipoIntervencao" required>
                                                <option disabled selected value="">Selecione o tipo de Interven��o
                                                </option>
                                                <?php
                                                $tipoIntervencao = $this->medoo->select("tipo_intervencao", ["cod_tipo_intervencao", "nome_tipo_intervencao"], ["ORDER" => "nome_tipo_intervencao"]);

                                                foreach ($tipoIntervencao as $dados => $value) {
                                                    if (!empty($ssm) && $value['nome_tipo_intervencao'] == $ssm['nome_tipo_intervencao'])
                                                        echo('<option value="' . $value['cod_tipo_intervencao'] . '" selected>' . $value['nome_tipo_intervencao'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['cod_tipo_intervencao'] . '">' . $value['nome_tipo_intervencao'] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>SSM</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <label>Data de Abertura</label>
                                            <input name="dataHoraSsm" type="datetime" class="form-control" disabled
                                                   value="<?php if (!empty($ssm['data_abertura'])) echo($this->parse_timestamp($ssm['data_abertura'])); ?>">

                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <label>N�</label>
                                            <input name="codigoSsm" readonly class="form-control"
                                                   value="<?php if (!empty($ssm['cod_ssm'])) echo $ssm['cod_ssm'] ?>"/>

                                        </div>
                                        <?php
                                        if (!empty($ssm['nome_status'])) {
                                            echo('<div class="col-md-4 col-xs-4">
                                                        <label>Status</label>
                                                        <input readonly name="statusAntigo" class="form-control" value="' . $ssm['nome_status'] . '"/>
                                                    </div>');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Preenchimento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <label>Data/Hora Encaminhamento</label>
                                            <input class="form-control" disabled
                                                   value="<?php if (!empty($dataEncaminhamento)) echo($this->parse_timestamp($dataEncaminhamento)); ?>"/>

                                        </div>
                                        <div class="col-md-5 col-xs-6">
                                            <label>Respons�vel</label>
                                            <input class="form-control" type="text" id="responsavelSsm" disabled
                                                   value="<?php echo (!empty($ssm['usuario'])) ? $ssm['usuario'] : ""; ?>"/>

                                            <input class="form-control" type="hidden" name="responsavelSsm"
                                                   value="<?php echo (!empty($ssm['usuario'])) ? $ssm['usuario'] : $this->dadosUsuario['usuario']; ?>"/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if (!empty($ssm) && ($historico != "" || $ssm['nome_status'] == 'Cancelada' || $ssm['nome_status'] == 'Programada')) {
                        echo '<div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><label>Hist�rico</label></div>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="descricaoStatusSsm">Descri��es dos Status</label>
                                                        <textarea id="descricaoStatusSsm" class="form-control" disabled spellcheck="true">';
                        echo ($ssm['nome_status'] == 'Cancelada' || $ssm['nome_status'] == 'Programada') ? $ssm['descricao_status'] . "\n" : "";
                        echo "{$historico}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o / Local</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Linha</label>
                                            <select name="linhaSsm" class="form-control" required>
                                                <option disabled selected>Selecione a linha</option>
                                                <?php
                                                $linha = $this->medoo->select('linha', ['cod_linha', 'nome_linha'], ['ORDER' => "cod_linha"]);

                                                $codLinhaSsm = (!empty($ssm['cod_linha'])) ? $ssm['cod_linha'] : $saf['cod_linha'];

                                                foreach ($linha as $dados => $value) {
                                                    if ($value['cod_linha'] == $codLinhaSsm)
                                                        echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Sigla / Trecho</label>
                                            <select name="trechoSsm" class="form-control" required>
                                                <?php
                                                if (!empty($ssm)) {
                                                    if (!empty($ssm['cod_linha']))
                                                        $trecho = $this->medoo->select("trecho", ["cod_trecho", "nome_trecho", "descricao_trecho"], ["cod_linha" => (int)$ssm['cod_linha']]);
                                                    else
                                                        $trecho = $this->medoo->select("trecho", ["cod_trecho", "nome_trecho", "descricao_trecho"], ["cod_linha" => (int)$saf['cod_linha']]);

                                                    $codTrechoSsm = (!empty($ssm['cod_trecho'])) ? $ssm['cod_trecho'] : $saf['cod_trecho'];

                                                    foreach ($trecho as $dados => $value) {
                                                        if ($value['cod_trecho'] == $codTrechoSsm)
                                                            echo('<option value="' . $value['cod_trecho'] . '" selected>' . $value['nome_trecho'] . " - " . $value['descricao_trecho'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_trecho'] . '">' . $value['nome_trecho'] . " - " . $value['descricao_trecho'] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Ponto Not�vel</label>
                                            <select name="pontoNotavelSsm" class="form-control">
                                                <option value="" selected disabled>Pontos Not�veis</option>
                                                <?php
                                                if (!empty($ssm)) {
                                                    if (!empty($ssm['cod_trecho']))
                                                        $pn = $this->medoo->select("ponto_notavel", ["nome_ponto", "cod_ponto_notavel"], ["cod_trecho" => (int)$ssm['cod_trecho']]);
                                                    else
                                                        $pn = $this->medoo->select("ponto_notavel", ["nome_ponto", "cod_ponto_notavel"], ["cod_trecho" => (int)$saf['cod_trecho']]);

                                                    $codPnSsm = (!empty($ssm['cod_ponto_notavel'])) ? $ssm['cod_ponto_notavel'] : $saf['cod_ponto_notavel'];

                                                    foreach ($pn as $dados => $value) {
                                                        if ($value['cod_ponto_notavel'] == $codPnSsm)
                                                            echo('<option value="' . $value['cod_ponto_notavel'] . '" selected>' . $value['nome_ponto'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_ponto_notavel'] . '">' . $value['nome_ponto'] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Complemento Local</label>
                                            <input name="complementoLocalSsm" type="text" class="form-control"
                                                   value="<?php echo (!empty($ssm['complemento_local'])) ? $ssm['complemento_local'] : $saf['complemento_local']; ?>"/>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <label>Via</label>
                                            <select name="viaSsm" id="viaSsm" class="form-control">
                                                <?php
                                                if (!empty($ssm)) {
                                                    if (!empty($ssm['cod_linha']))
                                                        $via = $this->medoo->select("via", ["nome_via", "cod_via"], ["cod_linha" => (int)$ssm['cod_linha'], "ORDER" => "cod_via"]);
                                                    else
                                                        $via = $this->medoo->select("via", ["nome_via", "cod_via"], ["cod_linha" => (int)$saf['cod_linha']]);

                                                    $codViaSsm = ($ssm['cod_via']) ? $ssm['cod_via'] : $saf['cod_via'];

                                                    echo('<option value="" selected>Selecione a via</option>');

                                                    foreach ($via as $dados => $value) {
                                                        if ($value['cod_via'] == $codViaSsm)
                                                            echo('<option value="' . $value['cod_via'] . '" selected>' . $value['nome_via'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_via'] . '">' . $value['nome_via'] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Posi��o</label>
                                            <input name="posicaoSsm" type="text" class="form-control"
                                                   value="<?php echo (!empty($ssm['posicao'])) ? $ssm['posicao'] : $saf['posicao']; ?>"/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Km Inicial</label>
                                            <input name="kmInicialSsm" type="text" class="form-control"
                                                   value="<?php echo (!empty($ssm['km_inicial'])) ? $ssm['km_inicial'] : $saf['km_inicial']; ?>"/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Km Final</label>
                                            <input name="kmFinalSsm" type="text" class="form-control"
                                                   value="<?php echo (!empty($ssm['km_final'])) ? $ssm['km_final'] : $saf['km_final']; ?>"/>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input disabled name="numeroGrupoSistemaSsm"
                                                   value="<?php echo (!empty($ssm['cod_grupo'])) ? $ssm['cod_grupo'] : $saf['cod_grupo'] ?>"
                                                   class="form-control">

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Grupo de Sistema</label>
                                            <?php
                                            if(!empty($ssm['cod_grupo']))
                                            {
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);
                                            }else
                                            {
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S']);
                                            }
                                            $codGrupoSsm = (!empty($ssm['cod_grupo'])) ? $ssm['cod_grupo'] : $saf['cod_grupo'];
                                            $this->form->getSelectGrupo($codGrupoSsm, $grupo, 'grupoSistemaSsm', false);
                                            ?>
                                        </div>
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input disabled name="numeroSistemaSsm"
                                                   value="<?php echo (!empty($ssm['cod_sistema'])) ? $ssm['cod_sistema'] : $saf['cod_sistema'] ?>"
                                                   class="form-control">

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sistema</label>
                                            <select id="sistema" name="sistemaSsm" class="form-control" required>
                                                <option disabled selected>Selecione o Sistema</option>
                                                <?php
                                                if (!empty($ssm['cod_grupo']))
                                                    $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => "{$ssm['cod_grupo']}"]);
                                                else
                                                    $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => "{$saf['cod_grupo']}"]);

                                                $codSistemaSsm = (!empty($ssm['cod_sistema'])) ? $ssm['cod_sistema'] : $saf['cod_sistema'];

                                                foreach ($sistema as $dados => $value) {
                                                    if ($codSistemaSsm == $value["cod_sistema"])
                                                        echo('<option value="' . $value["cod_sistema"] . '" selected>' . $value["nome_sistema"] . '</option>');
                                                    else
                                                        if($value['cod_sistema'] != 174)
                                                            echo('<option value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input class="form-control" name="codSubSistema"
                                                   value="<?php echo (!empty($ssm['cod_sistema'])) ? $ssm['cod_sistema'] : $saf['cod_subsistema'] ?>"
                                                   disabled>
                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sub-Sistema</label>
                                            <select class="form-control" name="subSistemaSsm" required>
                                                <?php
                                                echo('<option disabled selected> Sub-Sistema</option>');
                                                if (!empty($ssm['cod_sistema']))
                                                    $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $ssm['cod_sistema']]);
                                                else
                                                    $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => $saf['cod_sistema']]);

                                                $sub = (!empty($ssm['cod_subsistema'])) ? $ssm['cod_subsistema'] : $saf['cod_subsistema'];

                                                foreach ($subsistema as $dados => $value) {
                                                    if ($sub == $value["cod_subsistema"])
                                                        echo('<option value="' . $value["cod_subsistema"] . '" selected>' . $value["nome_subsistema"] . '</option>');
                                                    else
                                                        echo('<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php
                                    if (
                                        $ssm['cod_grupo'] == '22' || $ssm['cod_grupo'] == '23' || $ssm['cod_grupo'] == '26' ||
                                            (
                                                    empty($ssm['cod_grupo']) &&
                                                    ( $saf['cod_grupo'] == '22' || $saf['cod_grupo'] == '23' || $saf['cod_grupo'] == '26' )
                                            )
                                    ) {
                                        $materialRodante = true;
                                    }else{
                                        $materialRodante = false;
                                    }
                                    ?>
                                    <div class="row" id="divMaterialRodante"
                                         style="display:<?php echo ($materialRodante) ? "display" : "none" ?>">
                                        <div class="col-md-3">
                                            <label>C�digo Ve�culo</label>
                                            <select class="form-control"
                                                    name="veiculoMrSsm" <?php if ($materialRodante) echo "required"; ?>>
                                                <option value="" selected disabled>Ve�culo</option>
                                                <?php
                                                if (!empty($saf['cod_veiculo']) || !empty($ssm['cod_veiculo'])){
                                                    $selectVeiculo = $this->medoo->select("veiculo", "*", ['cod_grupo' => (int)$codGrupoSsm]);

                                                    $codVeiculoSsm = (!empty($ssm['cod_veiculo'])) ? $ssm['cod_veiculo'] : $saf['cod_veiculo'];

                                                    foreach ($selectVeiculo as $dados => $value) {
                                                        if ($codVeiculoSsm == $value['cod_veiculo']) {
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        } else {
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Carro Avariado</label>
                                            <select name="carroMrSsm"
                                                    class="form-control" <?php if ($materialRodante) echo "required"; ?>>
                                                <?php
                                                if (!empty($saf['cod_carro']) || !empty($ssm['cod_carro'])){
                                                    $selectCarro = $this->medoo->select("carro", "*", ['cod_veiculo' => (int)$codVeiculoSsm]);

                                                    foreach ($selectCarro as $dados => $value) {
                                                        if ($ssm['cod_carro'] == $value['cod_carro']) {
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                        } else if (empty($ssm['cod_carro']) && $saf['cod_carro'] == $value['cod_carro']) {
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                        } else {
                                                            echo("<option value='{$value['cod_carro']}'>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Od�metro</label>
                                            <input <?php
                                                if (!empty($ssm['odometro']))
                                                    echo "value='{$ssm['odometro']}'";
                                                else{
                                                    if($ssm['cod_carro']){
                                                        $odometro = $this->medoo->select("material_rodante_ssm", "*",
                                                            [
                                                                "cod_ssm" => $ssm['cod_ssm']
                                                            ]
                                                        );
                                                        echo " value='{$odometro[0]['odometro']}' ";
                                                    }else {
                                                        if (!empty($saf['odometro']))
                                                            echo " value='{$saf['odometro']}' ";
                                                        else {
                                                            if ($saf['cod_carro']) {
                                                                $odometro = $this->medoo->select("material_rodante_saf", "*",
                                                                    [
                                                                        "cod_saf" => $saf['cod_saf']
                                                                    ]
                                                                );
                                                                echo " value='{$odometro[0]['odometro']}' ";
                                                            }
                                                        }
                                                    }
                                                }


                                                if ($materialRodante) echo "required";

                                                ?>
                                               class="form-control number" type="text" name="odometroMrSsm">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Pref�xo</label>
                                            <select type="text" name="prefixoMrSsm" class="form-control">
                                                <option value="">Pref�xo</option>
                                                <?php
                                                $prefixo = $this->medoo->select("prefixo", "*");

                                                foreach ($prefixo as $dados => $value) {
                                                    if ($ssm['cod_prefixo'] == $value['cod_prefixo'])
                                                        echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                    else if (empty($ssm['cod_prefixo']) && $saf['cod_prefixo'] == $value['cod_prefixo'])
                                                        echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                    else
                                                        echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                    if ((empty($ssm['cod_grupo']) && ($saf['cod_grupo'] == '27' || $saf['cod_grupo'] == '28')) || ($ssm['cod_grupo'] == '27' || $ssm['cod_grupo'] == '28')) {
                                        $bilhetagem = true;
                                    }else{
                                        $bilhetagem = false;
                                    }
                                    ?>
                                    <div class="row" id="sublocal" style="display: <?php echo ($bilhetagem) ? "display" : "none" ?>;">
                                        <div class="col-md-4">
                                            <label>Local</label>
                                            <select name="localGrupo" class="form-control" id="local_grupo">
                                                <?php
                                                $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                                echo ("<option value='' disabled selected>Selecione o Local</option>");
                                                foreach($local as $dados=>$value){
                                                    if($ssm['cod_local_grupo'] == $value['cod_local_grupo'])
                                                        echo ("<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>");
                                                    else if (empty($ssm['cod_local_grupo']) && $saf['cod_local_grupo'] == $value['cod_local_grupo'])
                                                            echo("<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>");
                                                    else
                                                        echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Sub-Local</label>
                                            <select name="subLocalGrupo" id="sublocal_grupo" class="form-control">
                                                <?php
                                                if($ssm['cod_sublocal_grupo'])
                                                    $selectLocal = $this->medoo->select('sublocal_grupo', ['[><]local_sublocal_grupo' => 'cod_sublocal_grupo'], '*', ['cod_local_grupo' => $ssm['cod_local_grupo']]);
                                                else
                                                    $selectLocal = $this->medoo->select('sublocal_grupo', ['[><]local_sublocal_grupo' => 'cod_sublocal_grupo'], '*', ['cod_local_grupo' => $saf['cod_local_grupo']]);

                                                foreach ( $selectLocal as $dados => $value ){
                                                    if( (!empty($ssm['cod_sublocal_grupo'])) ){
                                                        if($ssm['cod_sublocal_grupo'] == $value['cod_sublocal_grupo'] ){
                                                            echo ("<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>");
                                                        }else {
                                                            echo("<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>");
                                                        }
                                                    }else{
                                                        if( $saf['cod_sublocal_grupo'] == $value['cod_sublocal_grupo'] ){
                                                            echo ("<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>");
                                                        }else {
                                                            echo("<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Avaria</label></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label for="codigoAvaria">C�digo</label>
                                            <input disabled class="form-control"
                                                   value="<?php if (!empty($saf['cod_avaria'])) echo $saf['cod_avaria']; ?>"/>
                                        </div>
                                        <div class="col-md-12 col-xs-10">
                                            <label for="avariaSaf">Descri��o </label>
                                            <input disabled class="form-control"
                                                   value="<?php if (!empty($saf['nome_avaria'])) echo $saf['nome_avaria']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Complemento</label>
                                            <textarea class="form-control"
                                                      readonly><?php if (!empty($saf['complemento_falha'])) echo $saf['complemento_falha']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Servi�o</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input name="servicoSsm"
                                                   value="<?php if (!empty($ssm['cod_servico']) && $ssm['cod_status'] != 12) echo $ssm['cod_servico'] ?>"
                                                   readonly required class="form-control">

                                        </div>
                                        <div class="col-md-12 col-xs-10">
                                            <label>Nome</label>
                                            <input name="servicoSsmInput" list="servicoSsmDataList"
                                                   placeholder="Selecione um Servi�o" autocomplete="off"
                                                   value="<?php if (!empty($ssm['nome_servico']) && $ssm['cod_status'] != 12) echo $ssm['nome_servico'] ?>"
                                                   required class="form-control">

                                            <datalist id="servicoSsmDataList">
                                                <option value=""></option>
                                                <?php
                                                $servicos = $this->medoo->select("servico", ["nome_servico", "cod_servico"], ["ORDER" => "nome_servico"]);

                                                foreach ($servicos as $dados => $value) {
                                                    echo('<option value="' . $value['cod_servico'] . '">' . $value['nome_servico'] . " - " . $value['cod_servico'] . '</option>');
                                                }
                                                ?>
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label>Complemento Servi�o</label>
                                            <textarea class="form-control"
                                                      name="complementoServicoSsm"><?php if (!empty($ssm['complemento_servico'])) echo $ssm['complemento_servico']; ?></textarea>

                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label for="nivel">N�vel</label>
                                            <input id='nivelOriginal' class="form-control" type="text" readonly name="nivelSsm" value="<?php echo $ssm['nivel']; ?>">
                                            <!-- <select id="nivel" name="nivelSsm" class="form-control">
                                                <option value="A" <?php if ($ssm['nivel'] == "A") echo("selected") ?>>
                                                    A
                                                </option>
                                                <option value="B" <?php if ($ssm['nivel'] == "B") echo("selected") ?>>
                                                    B
                                                </option>
                                                <option value="C" <?php if ($ssm['nivel'] == "C" || empty($ssm)) echo("selected") ?>>
                                                    C
                                                </option>
                                            </select> -->
                                        </div>
                                    </div>

                                    <div id="justificativaNivel" class="row" style="display: none">
                                        <div class="col-md-12" >
                                            <label>Justificativa de Altera��o de N�vel</label>
                                            <input id="justificativaAltNivel" class="form-control" name="justificativaAltNivel">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Encaminhamento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input name="numeroLocalSsm" disabled type="text"
                                                   value="<?php if (!empty($ssm['cod_unidade'])) echo $ssm['cod_unidade'] ?>"
                                                   class="form-control">

                                        </div>
                                        <div class="col-md-12 col-xs-10">
                                            <label>Local</label>
                                            <select name="localSsm" required class="form-control"
                                            <?php
                                            echo ($ssm['nome_status'] == "Encaminhado") ? ' disabled>' : '>';

                                            $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"]);

                                            echo('<option disabled selected>Selecione o Local</option>');

                                            foreach ($unidade as $dados => $value) {
                                                if (!empty($ssm) && $ssm['cod_unidade'] == $value['cod_unidade'])
                                                    echo('<option selected value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                            }
                                            ?>
                                            </select>
                                            <?php
                                            if ($ssm['nome_status'] == "Encaminhado")
                                                echo '<input type="hidden" name="localSsm" class="form-control" required value="' . $ssm["cod_unidade"] . '">';
                                            ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label>Sigla</label>
                                            <input name="siglaEquipeSsm" disabled type="text"
                                                   value="<?php if (!empty($ssm['sigla_equipe'])) echo $ssm['sigla_equipe'] ?>"
                                                   class="form-control">

                                        </div>
                                        <div class="col-md-10 col-xs-10">
                                            <label>Equipe</label>
                                            <select name="equipeSsm" required class="form-control"
                                            <?php
                                            echo ($ssm['nome_status'] == "Encaminhado") ? ' disabled>' : '>';

                                            if (!empty($ssm)) {
                                                $equipe = $this->medoo->select("un_equipe", ["[><]equipe" => "cod_equipe"], ["sigla", "nome_equipe"],
                                                    [
                                                        "ORDER" => "nome_equipe",
                                                        "cod_unidade" => $ssm['cod_unidade']
                                                    ]
                                                );

                                                echo('<option disabled selected value="">Selecione a Equipe</option>');

                                                foreach ($equipe as $dados => $value) {
                                                    if ($ssm['sigla_equipe'] == $value['sigla'])
                                                        echo('<option value="' . $value['sigla'] . '" selected>' . $value['nome_equipe'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                                }
                                            }
                                            ?>
                                            </select>
                                            <?php
                                            if ($ssm['nome_status'] == "Encaminhado") {
                                                echo '<input type="hidden" name="equipeSsm" class="form-control" required value="' . $ssm["sigla_equipe"] . '">';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <div class="col-md-offset-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">A��es</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group  btn-group-justified " role="group">
                                                <input type="hidden" id="validacao" value="">
                                                <?php echo $btnsAcao; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade modal-historico" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="fa fa-close"></i></button>
                <h4 class="modal-title">Hist�rico da SSM</h4>
            </div>
            <div class="modal-body">

                <?php
                if(!empty($ssm)) {
                    $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssm s
                JOIN usuario u on s.usuario = u.cod_usuario
                JOIN status USING(cod_status)
                WHERE cod_ssm = {$ssm['cod_ssm']} ORDER BY data_status";

                    if ($sql != "")
                        $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                }
                ?>

                <div class = "row">
                    <div class="col-md-12">
                        <table class="tableRel table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Responsavel</th>
                                <th>Data da altera��o</th>
                                <th>Complemento</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($result)){
                                foreach ($result as $dados){
                                    echo("<tr>");
                                    echo("<td>{$dados['nome_status']}</td>");
                                    echo("<td>{$dados['usuario']}</td>");
                                    echo("<td>{$this->parse_timestamp($dados['data_status'])}</td>");
                                    echo("<td>{$dados['descricao']}</td>");
                                    echo("</tr>");
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-solicitacaoNivel" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="fa fa-close"></i></button>
                <h4 class="modal-title">Solicita��o de Altera��o de N�vel da SSM <?= $ssm['cod_ssm'] ?></h4>
            </div>
            <div class="modal-body">

                <?php
                if(!empty($ssm)) {
                  $alterNivel = $this->carregaModelo('SSM/alterNivel-model');

                  $resultado = $alterNivel->getBySsm($ssm['cod_ssm']);
                }
                ?>

                <div class = "row">
                    <div class="col-md-12">
                        <table class="tableRel table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>Data da Solicita��o</th>
                                <th>N�vel Antigo</th>
                                <th>N�vel Solicitado</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($resultado)){
                                foreach ($resultado as $dados){
                                    echo("<tr>
                                    <td>{$dados['data_solicitacao']}</td>
                                    <td>{$dados['nivel_antigo']}</td>
                                    <td>{$dados['nivel_novo']}</td>
                                    <td>{$dados['nome_status']}</td>
                                    </tr>");
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-AlterarNivel" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form id="formAlterNivel" class="form-group" action="<?= "{$this->home_uri}/ssm/solicitarAlterNivel" ?>" method="post">
            <div class="modal-header">
                <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="fa fa-close"></i></button>
                <h4 class="modal-title">Solicita��o de Altera��o de N�vel</h4>
            </div>
            <div class="modal-body">

                <div class = "row">
                    <div class="col-md-3">
                      <label>SSM</label>
                      <input name="cod_ssm" class="form-control" type="text" readonly value="<?=$ssm['cod_ssm'] ?>"/>
                    </div>
                    <div class="col-md-3">
                      <label>Solicitante</label>
                      <input type="text"  class="form-control" disabled value="<?=$this->dadosUsuario['usuario'] ?>"/>
                      <input name="cod_usuario" type="hidden" value="<?=$this->dadosUsuario['cod_usuario'] ?>"/>
                    </div>

                    <div class="col-md-3">
                      <label>Nivel Atual</label>
                      <input id="nAtual" name="nivel_antigo" class="form-control" type="text" readonly value="<?=$ssm['nivel'] ?>"/>
                    </div>
                    <div class="col-md-3">
                      <label>Novo N�vel</label>
                      <select id="nNovo" name="nivel_novo" class="form-control free" type="text">
                        <option value="" selected disabled>Novo N�vel</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                      </select>
                    </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <label>Motivo</label>
                    <textarea id="motivoMudancaNivel" name="observacao" class="form-control free"></textarea>
                  </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" id="btnFormAlterNivel" class="btn btn-primary">Solicitar</button>
            </div>
          </form>
        </div>
    </div>
</div>
