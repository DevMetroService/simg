<?php

$conteudo = 'N. SSM;';
$conteudo .= 'N. SAF;';
$conteudo .= 'Responsável pelo Preenchimento;';
$conteudo .= 'Data Abertura;';
$conteudo .= 'Status;';
$conteudo .= 'Data Referente ao Status;';
$conteudo .= 'Descrição do Status;';
$conteudo .= 'Nível;';
$conteudo .= 'Linha;';
$conteudo .= 'Trecho;';
$conteudo .= 'Ponto Notável;';
$conteudo .= 'Complemento Local;';
$conteudo .= 'Via;';
$conteudo .= 'Km Inicial;';
$conteudo .= 'Km Final;';
$conteudo .= 'Posição;';
$conteudo .= 'Grupo de Sistema;';
$conteudo .= 'Sistema;';
$conteudo .= 'Sub-Sistema;';
$conteudo .= "Veículo;";
$conteudo .= "Carro;";
$conteudo .= "Odômetro;";
$conteudo .= 'Tipo de Intervenção;';
$conteudo .= 'Serviço;';
$conteudo .= 'Complemento do Serviço;';
$conteudo .= 'Local;';
$conteudo .= "Equipe;";
$conteudo .= 'Local_Grupo;';
$conteudo .= "Sublocal_Grupo;";
$conteudo .= "Agente Causador;";
$conteudo .= "Data Encerramento Última OSM;\n";

$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)){
    foreach ($returnPesquisa as $dados=>$value) {

        $dataEncerramento = $this->medoo->select("status_ssm", "data_status", [
            "AND" => [
                "cod_status" => 35,
                "cod_ssm" => $value['cod_ssm']
            ],
            
            "ORDER" => ["cod_mstatus" => "DESC"]
        ])[0];

        $agente = $this->medoo->select("osm_falha",
            [
                "[><]osm_registro" => "cod_osm",
                "[><]agente_causador" => "cod_ag_causador"
            ],[
                "nome_agente",
                "cod_ag_causador"
            ],
            [
                "cod_ssm" => $value['cod_ssm']
            ])[0];

        $txt = $value['cod_ssm'] . ":::";
        $txt .= $value['cod_saf'] . ":::";
        $txt .= $value['usuario'] . ":::";
        $txt .= $this->parse_timestamp($value['data_abertura']) . ":::";
        $txt .= $value['nome_status'] . ":::";
        $txt .= $this->parse_timestamp($value['data_status']) . ":::";
        $txt .= addslashes($value['descricao_status']) . ":::";
        $txt .= $value['nivel'] . ":::";
        $txt .= $value['nome_linha'] . ":::";
        $txt .= $value['nome_trecho'] . " - " . $value['descricao_trecho'] . ":::";
        $txt .= $value['nome_ponto'] . ":::";
        $txt .= $value['complemento'] . ":::";
        $txt .= $value['nome_via'] . ":::";
        $txt .= $value['km_inicial'] . ":::";
        $txt .= $value['km_final'] . ":::";
        $txt .= $value['posicao'] . ":::";
        $txt .= $value['nome_grupo'] . ":::";
        $txt .= $value['nome_sistema'] . ":::";
        $txt .= $value['nome_subsistema'] . ":::";
        $txt .= $value['nome_veiculo'] . ":::";
        $txt .= $value['nome_carro'] . ":::";
        $txt .= $value['odometro'] . ":::";
        $txt .= $value['nome_tipo_intervencao'] . ":::";
        $txt .= $value['nome_servico'] . ":::";
        $txt .= addslashes($value['complemento_servico']) . ":::";
        $txt .= $value['nome_unidade'] . ":::";
        $txt .= $value['nome_equipe'] . ":::";
        $txt .= $value['nome_local_grupo'] . ":::";
        $txt .= $value['nome_sublocal_grupo'] . ":::";
        $txt .= !empty($agente) ? $agente['nome_agente'] . ":::" : "" . ":::";
        $txt .= $dataEncerramento . ":::";

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("SsmCompleta.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=SsmCompleta.csv");
readfile(TEMP_PATH.'SsmCompleta.csv');

unlink(TEMP_PATH."SsmCompleta.csv");
