<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 04/09/2018
 * Time: 17:11
 */
?>
<div class="page-header">
    <h1>Valida��o de Servi�os</h1>
</div>

<div class="row">
    <form id="formSsm" class="form-group" method="post" action="<?php echo HOME_URI?>/dashboardEngenharia/ssmAnalisada">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label>Valida��o de Servi�o</label>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-2">
                        <label>C�digo SSM</label>
                        <input class="form-control" value="<?php echo $valores['cod_ssm']?>" readonly name="cod_ssm" />
                    </div>
                    <div class="col-xs-3">
                        <label>Data de Abertura da SSM</label>
                        <input class="form-control" value="<?php echo $valores['data_abertura']?>" readonly name="dataAberturaSaf" />
                    </div>
                    <div class="col-xs-2">
                        <label>C�digo SAF</label>
                        <input class="form-control" value="<?php echo $valores['cod_saf']?>" readonly name="cod_saf" />
                    </div>
                    <div class="col-xs-2">
                        <label>N�vel SAF</label>
                        <input class="form-control" value="<?php echo $valores['nivel_saf']?>" readonly name="cod_saf" />
                    </div>
                    <div class="col-xs-3">
                        <label>Equipe Respons�vel</label>
                        <input class="form-control" value="<?php echo $valores['nome_equipe']?>" readonly name="nome_equipe" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo</label>
                        <input class="form-control" value="<?php echo $valores['nome_grupo']?>" readonly name="grupoModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input class="form-control" value="<?php echo $valores['nome_sistema']?>" readonly name="sistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>SubSistema</label>
                        <input class="form-control" value="<?php echo $valores['nome_subsistema']?>" readonly name="subsistemaModal" />
                    </div>
                </div>
                <?php if(!empty($valores['nome_veiculo'])){?>
                <div class="row">
                    <div class="col-md-4">
                        <label>Nome do Ve�culo</label>
                        <input class="form-control" value="<?php echo $valores['nome_veiculo']?>" readonly name="subsistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Nome do Carro</label>
                        <input class="form-control" value="<?php echo $valores['nome_carro']?>" readonly name="subsistemaModal" />
                    </div>
                </div>
                <?php }?>
                <div class="row">
                    <div class="col-xs-4">
                        <label>Linha</label>
                        <input class="form-control" value="<?php echo $valores['nome_linha']?>" readonly name="linhaModal"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Trecho</label>
                        <input class="form-control" value="<?php echo $valores['nome_trecho'] . " - " . $valores['descricao_trecho']?>" readonly name="trechoModal"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Ponto Not�vel</label>
                        <input class="form-control" value="<?php echo $valores['nome_ponto']?>" readonly name="pontoNotavelModal"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Local</label>
                        <textarea class="form-control" readonly name="complementoLocalModal"><?php echo $valores['complemento_local']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Avaria</label>
                        <input class="form-control" value="<?php echo $valores['nome_avaria']?>" readonly name="avaria" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Avaria</label>
                        <textarea class="form-control" readonly name="complementoAvaria"><?php echo $valores['complemento_falha']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Servi�o</label>
                        <input class="form-control" value="<?php echo $valores['nome_servico']?>" readonly name="servico" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Servi�o</label>
                        <textarea class="form-control" readonly name="complementoServico"><?php echo $valores['complemento_servico']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Agente Causador</label>
                        <select class="form-control" name="agenteCausador">
                            <?php
                            $ag_causador = $this->medoo->select("agente_causador", '*', ["ativo"=>'s']);

                            foreach($ag_causador as $dados=>$value)
                            {
                                if($cod_ag_causador == $value['cod_ag_causador'])
                                    echo ("<option value='{$value['cod_ag_causador']}' selected>{$value['nome_agente']}</option>");
                                else
                                    echo ("<option value='{$value['cod_ag_causador']}'>{$value['nome_agente']}</option>");
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <?php
                foreach ($valores['registro_os'] as $dados=>$value){
                    echo("<div class='panel panel-default' style='margin-top: 15px'>
                            <div class='panel-heading'>OSM {$value['cod_osm']}</div>
                            <div class='panel-body'>
                    ");
                    echo "<div class='row' style='margin-top: 10px;'>
                            <div class='col-md-4'>
                                <label>Atua��o</label>
                                <input class='form-control' value='{$value['nome_atuacao']}' type='text' readonly>
                            </div>
                            <div class='col-md-8'>
                                <label>Descri��o da Atua��o</label>
                                <textarea class='form-control' type='text' readonly>{$value['desc_atuacao']}</textarea>
                            </div>
                        </div>";
                        $materiais = $this->medoo->select('osm_material', ['[><]v_material' => 'cod_material'] ,'*', ['cod_osm' => $value['cod_osm']]);
                        if(!empty($materiais)){
                ?>
                <div class='panel panel-default' style='margin-top: 15px'>
                    <div class='panel-heading'>Lista de Materiais</div>
                    <div class='panel-body'>
                        <?php
                        foreach ($materiais as $dados => $value) {
                            echo("
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <label>Material</label>
                                            <input class='form-control' value='{$value['nome_material']} - {$value['descricao_material']}' readonly/>
                                        </div>
                                        <div class='col-md-3'>
                                            <label>Quantidade</label>
                                            <input class='form-control' value='{$value['utilizado']} {$value['nome_uni_medida']}' readonly />
                                        </div>
                                        <div class='col-md-3'>
                                            <label>Origem</label>
                                            <input class='form-control' value='{$origem[$value['origem']]}' readonly />
                                        </div>
                                    </div>
                                    ");
                        }
                        }
                            }
                            ?>
                        </div>
                    </div>

                    </div>
                </div>
                <div class="row" style="margin-top:20px">
                    <div class="col-md-offset-10 col-md-2">
                        <div class="panel panel-primary">
                            <div class="panel-heading">A��es</div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="allowBtn" class="btn-group  btn-group-justified">
                                            <div class="btn-group">
                                                <button id="btnSubmit" type="button" class="btn btn-success btn-lg">
                                                    <i class="fa fa-check fa-2x"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </form>
</div>
