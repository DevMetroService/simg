<div class="page-header">
    <h2>Pesquisa de Solicita��o de Servi�o de Manuten��o</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/executarPesquisaSsm" class="form-group" method="post" id="pesquisa">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Ssm</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div id="headingDadosGerais" role="tab" class="panel-heading">
                                    <a href="#dadosGeraisPesquisa" aria-controls="collapseDadosGerais"
                                       aria-expanded="false" data-toggle="collapse" role="button" class="collapsed">
                                        <button class="btn btn-default"><label>Dados Gerais</label></button>
                                    </a>
                                </div>

                                <div aria-labelledby="headingDadosGerais" role="tabpanel" id="dadosGeraisPesquisa"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['numeroSsm']) || !empty($dadosRefill['codigoSaf']) || !empty($dadosRefill['responsavelPreenchimentoSsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>N� SSM</label>
                                                <input type="text" name="numeroSsm" class="form-control"
                                                       value="<?php if (!empty($dadosRefill['numeroSsm'])) echo($dadosRefill['numeroSsm']); ?>"/>
                                            </div>

                                            <div class="col-md-2">
                                                <label>N� SAF</label>
                                                <input type="text" name="codigoSaf" class="form-control"
                                                       value="<?php if (!empty($dadosRefill['codigoSaf'])) echo($dadosRefill['codigoSaf']); ?>"/>
                                            </div>

                                            <div class="col-md-4">
                                                <label> Respons�vel pelo Preenchimento</label>
                                                <select name="responsavelPreenchimentoSsm" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectUser = $this->medoo->select("usuario", ['cod_usuario', 'usuario'], ["nivel" => "2", 'ORDER' => 'usuario']);
                                                    foreach ($selectUser as $dados => $value) {
                                                        if ($dadosRefill['responsavelPreenchimentoSsm'] == $value['cod_usuario'])
                                                            echo("<option value='{$value['cod_usuario']}' selected>{$value['usuario']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_usuario']}'>{$value['usuario']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingStatus">
                                    <a href="#statusPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseStatus">
                                        <button class="btn btn-default"><label>Status</label></button>
                                    </a>
                                </div>

                                <div id="statusPesquisa" role="tabpanel" aria-labelledby="headingStatus"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['statusPesquisa']) || !empty($dadosRefill['dataPartir']) || !empty($dadosRefill['dataRetroceder']) || !empty($dadosRefill['dataPartirAbertura']) || !empty($dadosRefill['dataRetrocederAbertura'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Abertura (a Partir)</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataPartirAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataPartirAbertura'])) echo($dadosRefill['dataPartirAbertura']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataAteAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataAteAbertura'])) echo($dadosRefill['dataAteAbertura']); ?>"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Status Atual</label>
                                                <select name="statusPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $statusSaf = $this->medoo->select('status', ['cod_status', 'nome_status'], ['tipo_status' => 'M']);
                                                    foreach ($statusSaf as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['statusPesquisa'] == $value['cod_status'])
                                                            echo("<option value='{$value['cod_status']}' selected>{$value['nome_status']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_status']}'>{$value['nome_status']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Data do Status (a Partir)</label>
                                                <input id="dataPartir" class="form-control data validaData"
                                                       type="text" name="dataPartir"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataPartir']; ?>"/>
                                            </div>
                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input id="dataRetroceder" class="form-control data validaData"
                                                       type="text" name="dataRetroceder"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataRetroceder']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingLocal">
                                    <a href="#localPesquisa" class="collapsed" role="button" data-toggle="collapse"
                                       aria-expanded="false" aria-controls="collapseLocal">
                                        <button class="btn btn-default"><label>Local</label></button>
                                    </a>
                                </div>

                                <div id="localPesquisa" role="tabpanel" aria-labelledby="headingLocal"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['linhaPesquisaSsm']) || !empty($dadosRefill['trechoPesquisaSsm']) || !empty($dadosRefill['pnPesquisaSsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Linha</label>
                                                <select class="form-control" name="linhaPesquisaSsm">
                                                    <option value="">Linhas</option>
                                                    <?php
                                                    $selectLinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha'], ["ORDER" => "cod_linha"]);

                                                    foreach ($selectLinha as $dados => $value) {
                                                        echo("<option value='" . $value['cod_linha'] . "' ");
                                                        if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaSsm'] == $value['cod_linha']) {
                                                            echo("selected");
                                                        }
                                                        echo(">" . $value["nome_linha"] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Trecho</label>
                                                <select class="form-control" name="trechoPesquisaSsm">
                                                    <option value="">Trechos</option>
                                                    <?php
                                                    if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaSsm'] != "") {
                                                        $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$dadosRefill['linhaPesquisaSsm']]);
                                                    } else {
                                                        $selectTrecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["ORDER" => "cod_linha"]);
                                                    }

                                                    foreach ($selectTrecho as $dados => $value) {
                                                        echo("<option value='" . $value['cod_trecho'] . "' ");

                                                        if ($dadosRefill['trechoPesquisaSsm'] == $value['cod_trecho']) {
                                                            echo("selected");
                                                        }

                                                        echo(">{$value["nome_trecho"]} - {$value['descricao_trecho']} </option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ponto Not�vel</label>
                                                <select class="form-control" name="pnPesquisaSsm">
                                                    <option value="">Pontos Not�veis</option>
                                                    <?php
                                                    if ($dadosRefill['linhaPesquisaSsm'] != "" && $dadosRefill['trechoPesquisaSsm'] == "") {

                                                    } else {
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaSsm'] != "")
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["cod_trecho" => (int)$dadosRefill['trechoPesquisaSsm']]);
                                                        else
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["ORDER" => "cod_trecho"]);

                                                        foreach ($selectPn as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['pnPesquisaSsm'] == $value['cod_ponto_notavel'])
                                                                echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingServico" role="tab" class="panel-heading">
                                    <a href="#servicoPesquisa" aria-controls="collapseServico" aria-expanded="false"
                                       data-toggle="collapse" role="button" class="collapsed">
                                        <button class="btn btn-default"><label>Servi�o</label></button>
                                    </a>
                                </div>

                                <div id="servicoPesquisa" aria-labelledby="headingServico" role="tabpanel"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['grupoSistemaPesquisa']) || !empty($dadosRefill['sistemaPesquisa']) || !empty($dadosRefill['subSistemaPesquisa']) || !empty($dadosRefill['pesquisaIntervencaoSsm']) || !empty($dadosRefill['pesquisaServicoSsm']) || !empty($dadosRefill['nivelPesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grupo de Sistema</label>
                                                <?php
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);
                                                if(empty($dadosRefill['grupoSistemaPesquisa']))
                                                    $dadosRefill['grupoSistemaPesquisa'] = null;
                                                $this->form->getSelectGrupo($dadosRefill['grupoSistemaPesquisa'], $grupo, 'grupoSistemaPesquisa', true);
                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sistema</label>
                                                <select class="form-control" name="sistemaPesquisa">
                                                    <option value="">Sistemas</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoSistemaPesquisa']))
                                                        $sistema = $this->medoo->select("grupo_sistema", ["[><]sistema" => "cod_sistema"], ['cod_sistema', 'nome_sistema'], ["cod_grupo" => $dadosRefill['grupoSistemaPesquisa']]);
                                                    else
                                                        $sistema = $this->medoo->select("sistema", ['cod_sistema', 'nome_sistema']);

                                                    foreach ($sistema as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisa'] == $value['cod_sistema'])
                                                            echo("<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Sistema</label>
                                                <select class="form-control" name="subSistemaPesquisa">
                                                    <option value="">Sub-Sistemas</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa'] != "" && $dadosRefill['sistemaPesquisa'] == "") {

                                                    } else {
                                                        if (!empty($dadosRefill['sistemaPesquisa']))
                                                            $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"], ['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisa']]);
                                                        else
                                                            $subsistema = $this->medoo->select("subsistema", ['cod_subsistema', 'nome_subsistema']);

                                                        foreach ($subsistema as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisa'] == $value['cod_subsistema'])
                                                                echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Tipo de Interven��o</label>
                                                <select name="pesquisaIntervencaoSsm" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectIntevercao = $this->medoo->select("tipo_intervencao", ['cod_tipo_intervencao', "nome_tipo_intervencao"], ["ORDER" => "nome_tipo_intervencao"]);

                                                    foreach ($selectIntevercao as $dados => $value) {
                                                        echo("<option value='" . $value['cod_tipo_intervencao'] . "' ");
                                                        if (!empty($dadosRefill) && $dadosRefill['pesquisaIntervencaoSsm'] == $value['cod_tipo_intervencao']) {
                                                            echo("selected");
                                                        }
                                                        echo(">" . $value["nome_tipo_intervencao"] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Servi�o</label>
                                                <select name="pesquisaServicoSsm" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectServico = $this->medoo->select("servico", ['cod_servico', "nome_servico"], ["ORDER" => "nome_servico"]);

                                                    foreach ($selectServico as $dados => $value) {
                                                        echo("<option value='" . $value['cod_servico'] . "' ");
                                                        if (!empty($dadosRefill) && $dadosRefill['pesquisaServicoSsm'] == $value['cod_servico']) {
                                                            echo("selected");
                                                        }
                                                        echo(">" . $value["nome_servico"] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label>N�vel</label>
                                                <select class="form-control" name="nivelPesquisa">
                                                    <option value="">Todos</option>

                                                    <?php
                                                    $selectsp = array(
                                                        "A" => "A",
                                                        "B" => "B",
                                                        "C" => "C"
                                                    );

                                                    foreach ($selectsp as $dados => $value) {
                                                        echo("<option value='" . $dados . "' ");

                                                        if (!empty($dadosRefill) && $dadosRefill['nivelPesquisa'] == $dados) {
                                                            echo("selected");
                                                        }

                                                        echo(">" . $value . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                        if ($dadosRefill['grupoSistemaPesquisa'] == '27' || $dadosRefill['grupoSistemaPesquisa'] == '28') {
                                            $bilhetagem = true;
                                        }else{
                                            $bilhetagem = false;
                                        }
                                        ?>

                                        <div class="row" id="local_sublocal" style="display: <?php echo ($bilhetagem) ? "block" : "none" ?>;">
                                            <div class="col-md-4">
                                                <label>Local</label>
                                                <select name="localGrupo" class="form-control" id="local_grupo">
                                                    <?php
                                                    $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                                    echo ("<option value='' disabled selected>Selecione o Local</option>");
                                                    foreach($local as $dados=>$value){

                                                        if($dadosRefill['localGrupo'] == $value['cod_local_grupo']){
                                                            echo ("<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>");
                                                        }else {
                                                            echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Local</label>
                                                <select name="subLocalGrupo" id="sublocal_grupo" class="form-control">
                                                    <?php
                                                    if($dadosRefill['subLocalGrupo']){
                                                        $selectLocal = $this->medoo->select('sublocal_grupo', ['[><]local_sublocal_grupo' => 'cod_sublocal_grupo'], '*', ['cod_local_grupo' => $dadosRefill['localGrupo']]);
                                                        foreach ( $selectLocal as $dados=>$value){
                                                            if($dadosRefill['subLocalGrupo'] == $value['cod_sublocal_grupo']){
                                                                echo ("<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>");
                                                            }else {
                                                                echo("<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>");
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="headingEncaminhamento" role="tab" class="panel-heading">
                                    <a href="#encaminhamentoPesquisa" aria-controls="collapseEncaminhamento"
                                       aria-expanded="false" data-toggle="collapse" role="button" class="collapsed">
                                        <button class="btn btn-default"><label>Encaminhamento</label></button>
                                    </a>
                                </div>

                                <div id="encaminhamentoPesquisa" aria-labelledby="headingEncaminhamento" role="tabpanel"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['unidadeEncaminhadaPesquisa']) || !empty($dadosRefill['sistemaPesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Unidade Encaminhada</label>
                                                <select class="form-control" name="unidadeEncaminhadaPesquisa">
                                                    <option value="">Unidades</option>
                                                    <?php
                                                    $selectUnidade = $this->medoo->select("unidade", ['cod_unidade', "nome_unidade"]);

                                                    foreach ($selectUnidade as $dados => $value) {
                                                        echo("<option value='" . $value['cod_unidade'] . "' ");

                                                        if (!empty($dadosRefill) && $dadosRefill['unidadeEncaminhadaPesquisa'] == $value['cod_unidade']) {
                                                            echo("selected");
                                                        }

                                                        echo(">" . $value["nome_unidade"] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Equipe</label>
                                                <select class="form-control" name="equipePesquisa">
                                                    <option value="">Equipes</option>
                                                    <?php
                                                    if (!empty($dadosRefill['unidadeEncaminhadaPesquisa'])){
                                                        $selectEquipe = $this->medoo->select("un_equipe", ["[><]equipe" => "cod_equipe"], ['sigla', "nome_equipe"], ["cod_unidade" => $dadosRefill['unidadeEncaminhadaPesquisa']]);
                                                    } else {
                                                        $selectEquipe = $this->medoo->select("equipe", ['sigla', "nome_equipe"]);
                                                    }

                                                    foreach ($selectEquipe as $dados => $value) {
                                                        echo("<option value='" . $value['sigla'] . "' ");

                                                        if (!empty($dadosRefill) && $dadosRefill['equipePesquisa'] == $value['sigla']) {
                                                            echo("selected");
                                                        }

                                                        echo(">" . $value["nome_equipe"] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default mtRod" hidden>
                                <div class="panel-heading" role="tab" id="headingMaterialRodante">
                                    <a href="#materialRodantePesquisa" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false"
                                       aria-controls="collapseMaterialRodante">
                                        <button class="btn btn-default"><label>Material Rodante</label></button>
                                    </a>
                                </div>

                                <div id="materialRodantePesquisa" role="tabpanel"
                                     aria-labelledby="headingMaterialRodante"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['veiculoPesquisa']) || !empty($dadosRefill['carroAvariadoPesquisa']) || !empty($dadosRefill['carroLiderPesquisa']) || !empty($dadosRefill['odometroPartirPesquisa']) || !empty($dadosRefill['odometroAtePesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Ve�culo</label>
                                                <select name="veiculoPesquisa" class="form-control">
                                                    <option value="">Ve�culo</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoSistemaPesquisa'])){
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                                    } else {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo']);
                                                    }

                                                    foreach ($veiculo as $dados => $value) {
                                                        if ($dadosRefill['veiculoPesquisa'] == $value['cod_veiculo'])
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Carro Avariado</label>
                                                <select name="carroAvariadoPesquisa" class="form-control">
                                                    <option value="">Carro Avariado</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoSistemaPesquisa']) || !empty($dadosRefill['veiculoPesquisa'])){
                                                        if (!empty($dadosRefill['grupoSistemaPesquisa'])){
                                                            if (!empty($dadosRefill['veiculoPesquisa'])){
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['AND' => ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa'], 'cod_veiculo' => $dadosRefill['veiculoPesquisa']]]);
                                                            } else {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                                            }
                                                        } else {
                                                            $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_veiculo' => $dadosRefill['veiculoPesquisa']]);
                                                        }
                                                    } else {
                                                        $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro']);
                                                    }

                                                    foreach ($carro as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['carroAvariadoPesquisa'] == $value['cod_carro'])
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Prefixo</label>
                                                <select name="prefixoPesquisa" class="form-control">
                                                    <option value="">Prefixo</option>
                                                    <?php
                                                    $prefixo = $this->medoo->select("prefixo", ['cod_prefixo', 'nome_prefixo']);

                                                    foreach ($prefixo as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['prefixoPesquisa'] == $value['cod_prefixo'])
                                                            echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Od�metro a partir</label>
                                                <input name='odometroPartirPesquisa' class='form-control'
                                                       value="<?php if (!empty($dadosRefill['odometroPartirPesquisa'])) echo $dadosRefill['odometroPartirPesquisa']; ?>"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Od�metro at�</label>
                                                <input name="odometroAtePesquisa" class="form-control"
                                                       value="<?php if (!empty($dadosRefill['odometroAtePesquisa'])) echo $dadosRefill['odometroAtePesquisa']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
                            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro
                            </button>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 1em"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Resultado da Pesquisa
                                    <strong>Total
                                        Encontrado: <?php echo (!empty($returnPesquisa))? count($returnPesquisa) : 0; ?></strong>
                                </div>

                                <div class="panel-body">
                                    <div id="resultadoPesquisaSsm_wrapper"
                                         class="dataTables_wrapper form-inline no-footer">
                                        <table class="table table-striped table-bordered no-footer dataTable"
                                               id="resultadoPesquisaSsm" role="grid"
                                               aria-describedby="resultadoPesquisaSsm_info">
                                            <thead id="headIndicadorSsm">
                                            <tr role="row">
                                                <th>N� SSM</th>
                                                <th>N� SAF</th>
                                                <th>Data Abertura</th>
                                                <th>Sub Sistema</th>
                                                <th>Local</th>
                                                <th>Servi�o</th>
                                                <th>Status</th>
                                                <th>N�vel</th>
                                                <th>A��o</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if (!empty($returnPesquisa)) {
                                                foreach ($returnPesquisa as $dados => $value) {
                                                    if (!$value['nome_linha']) {
                                                        $pesquisaSaf = $this->medoo->select('v_saf', ['nome_linha', 'descricao_trecho'], ['cod_saf' => (int)$value['cod_saf']]);
                                                        $value['nome_linha'] = $pesquisaSaf[0]['nome_linha'];
                                                        $value['descricao_trecho'] = $pesquisaSaf[0]['descricao_trecho'];
                                                        $value['nome_veiculo'] = $pesquisaSaf[0]['nome_veiculo'];
                                                    }

                                                    if($value['nome_veiculo']) {
                                                        $nomeVeiculo = "<span class='primary-info'>{$value['nome_veiculo']}</span><br />";
                                                    }else{
                                                        $nomeVeiculo = "";
                                                    }

                                                    echo('<tr>');
                                                    echo('<td>' . $value['cod_ssm'] . '</td>');
                                                    echo('<td>' . $value['cod_saf'] . '</td>');
                                                    echo('<td>' . $this->parse_timestamp($value['data_abertura']) . '</td>');
                                                    echo('<td>' . $value['nome_subsistema'] . '</td>');
                                                    echo("<td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>");
                                                    echo("<td><span data-placement='right' rel='tooltip-wrapper' data-title='{$value['complemento_servico']}'>{$nomeVeiculo}{$value['nome_servico']}</span></td>");
                                                    echo('<td>' . $value['nome_status'] . '</td>');
                                                    echo('<td>' . $value['nivel'] . '</td>');
                                                    echo("<td><a href='{$this->home_uri}/ssm/edit/{$value['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>{$btnAcoes}</td>");
                                                    echo('</tr>');
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>