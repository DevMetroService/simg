<?php
$ssm = $this->medoo->select('v_ssm', '*', [
    "cod_ssm" => (int)$_SESSION['refillSsm']['codigoSsm']
]);
$ssm = $ssm[0];

$saf = $this->medoo->select("v_saf", "*", [
    "cod_saf" => (int)$ssm['cod_saf']
]);
$saf = $saf[0];

unset($_SESSION['refillSsm']['codigoSsm']);
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>SSM - Solicita��o de Servi�o de Manuten��o</h3>
    </div>

    <hr/>

    <div class="title-table">&raquo; Dados Gerais</div>
    <table>
        <tr>
            <th>SSM</th>
            <th>Respons�vel Preenchimento</th>
            <th>Status</th>
            <th>N�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo $ssm['cod_ssm']; ?></td>
            <td class="center"><?php echo $ssm['usuario'] ?></td>
            <td class="center"><?php echo $ssm['nome_status']; ?></td>
            <td class="center"><?php echo $ssm['nivel']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Data/Hora Abertura</th>
            <th colspan="2">Data/Hora Preenchimento</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $this->parse_timestamp($ssm['data_abertura']) ?></td>
            <td colspan="2" class="center"><?php echo $this->parse_timestamp($ssm['data_status']); ?></td>
        </tr>
        <?php if ($ssm['descricao_status']): ?>
            <tr>
                <th colspan="4">Descri��o Status</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $ssm['descricao_status']; ?></td>
            </tr>
        <?php endif ?>
    </table>

    <div class="title-table">&raquo; Identifica��o / Local</div>
    <?php if (!empty($ssm['nome_linha'])): ?>
        <table>
            <tr>
                <th>Linha</th>
                <th colspan="3">Trecho</th>
                <th colspan="2">Ponto Not�vel</th>
            </tr>
            <tr>
                <td class="center"><?php echo($ssm['nome_linha']); ?></td>
                <td colspan="3" class="center"><?php echo($ssm['nome_trecho'] . ' - ' . $ssm['descricao_trecho']); ?></td>
                <td colspan="2" class="center"><?php echo($ssm['nome_ponto']); ?></td>
            </tr>
            <tr>
                <th colspan="6">Complemento</th>
            </tr>
            <tr>
                <td colspan="6"><?php echo($ssm['complemento_local']) ?></td>
            </tr>
            <tr>
                <th colspan="2">Via</th>
                <th>KM Inicial</th>
                <th>KM Final</th>
                <th colspan="2">Posi��o</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo($ssm['nome_via']); ?></td>
                <td class="center"><?php echo($ssm['km_inicial']); ?></td>
                <td class="center"><?php echo($ssm['km_final']); ?></td>
                <td colspan="2" class="center"><?php echo($ssm['posicao']); ?></td>
            </tr>
            <tr>
                <th colspan="3">Grupo Sistema</th>
                <th colspan="3">Sistema</th>
            </tr>
            <tr>
                <td colspan="3" class="center"><?php echo $ssm['nome_grupo']; ?></td>
                <td colspan="3" class="center"><?php echo $ssm['nome_sistema']; ?></td>
            </tr>
            <tr>
                <th colspan="6">SubSistema</th>
            </tr>
            <tr>
                <td colspan="6" class="center"><?php echo $ssm['nome_subsistema']; ?></td>
            </tr>
            <tr>
                <th colspan="3">Local</th>
                <th colspan="3">Sublocal</th>
            </tr>
            <tr>
                <td colspan="3" class="center"><?php echo($ssm['nome_local_grupo']); ?></td>
                <td colspan="3" class="center"><?php echo($ssm['nome_sublocal_grupo']); ?></td>
            </tr>
            <tr>
        </table>
        <?php if (!empty($ssm['nome_veiculo'])){ ?>
            <div class="title-table">&raquo; Material Rodante</div>
            <table>
                <tr>
                    <th>Ve�culo</th>
                    <th>Carro Avariado</th>
                    <th>Od�metro</th>
                    <th>Pref�xo</th>
                </tr>
                <tr>
                    <td class="center"><?php echo($ssm['nome_veiculo']) ?></td>
                    <td class="center"><?php echo($ssm['nome_carro']) ?></td>
                    <td class="center"><?php echo($ssm['odometro']) ?></td>
                    <td class="center"><?php echo($ssm['nome_prefixo']) ?></td>
                </tr>
            </table>
        <?php } ?>
    <?php else: ?>
        <table>
            <tr>
                <th>Linha</th>
                <th colspan="3">Trecho</th>
                <th colspan="2">Ponto Not�vel</th>
            </tr>
            <tr>
                <td class="center"><?php echo($saf['nome_linha']); ?></td>
                <td colspan="3" class="center"><?php echo($saf['nome_trecho'] . ' - ' . $saf['descricao_trecho']); ?></td>
                <td colspan="2" class="center"><?php echo($saf['nome_ponto']); ?></td>
            </tr>
            <tr>
                <th colspan="6">Complemento</th>
            </tr>
            <tr>
                <td colspan="6"><?php echo($saf['complemento_local']) ?></td>
            </tr>
            <tr>
                <th colspan="2">Via</th>
                <th>KM Inicial</th>
                <th>KM Final</th>
                <th colspan="2">Posi��o</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo($saf['nome_via']); ?></td>
                <td class="center"><?php echo($saf['km_inicial']); ?></td>
                <td class="center"><?php echo($saf['km_final']); ?></td>
                <td colspan="2" class="center"><?php echo($saf['posicao']); ?></td>
            </tr>
            <tr>
                <th colspan="3">Grupo Sistema</th>
                <th colspan="3">Sistema</th>
            </tr>
            <tr>
                <td colspan="3" class="center"><?php echo $saf['nome_grupo']; ?></td>
                <td colspan="3" class="center"><?php echo $saf['nome_sistema']; ?></td>
            </tr>
            <tr>
                <th colspan="6">SubSistema</th>
            </tr>
            <tr>
                <td colspan="6" class="center"><?php echo $saf['nome_subsistema']; ?></td>
            </tr>
        </table>
        <?php if (!empty($saf['nome_veiculo'])){ ?>
            <div class="title-table">&raquo; Material Rodante</div>
            <table>
                <tr>
                    <th>Ve�culo</th>
                    <th>Carro Avariado</th>
                    <th>Od�metro</th>
                    <th>Pref�xo</th>
                </tr>
                <tr>
                    <td class="center"><?php echo($saf['nome_veiculo']) ?></td>
                    <td class="center"<?php echo($saf['nome_carro']) ?></td>
                    <td class="center"><?php echo($saf['odometro']) ?></td>
                    <td class="center"><?php echo($saf['nome_prefixo']) ?></td>
                </tr>
            </table>
        <?php } ?>
    <?php endif ?>

    <div class="title-table">&raquo; Servi�o</div>
    <table>
        <tr>
            <th>Tipo de Interven��o</th>
        </tr>
        <tr>
            <td><?php echo $ssm['nome_tipo_intervencao'] ?></td>
        </tr>
        <tr>
            <th>Servi�o</th>
        </tr>
        <tr>
            <td><?php echo $ssm['nome_servico'] ?></td>
        </tr>
        <tr>
            <th>Complemento</th>
        </tr>
        <tr>
            <td><?php echo $ssm['complemento_servico'] ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Encaminhamento</div>
    <table>
        <tr>
            <th>Local</th>
            <th>Equipe</th>
        </tr>
        <tr>
            <td class="center"><?php echo $ssm['nome_unidade'] ?></td>
            <td class="center"><?php echo $ssm['nome_equipe'] ?></td>
        </tr>
    </table>
</div>