<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 04/09/2018
 * Time: 17:11
 */
?>
<div class="page-header">
    <h1>Valida��o de Servi�os</h1>
</div>

<div class="row">
    <form id="formSsm" class="form-group" method="post" action="<?php echo HOME_URI?>/cadastroGeral/aprovarSsm">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <label>Valida��o de Servi�o</label>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-3">
                        <label>C�digo SSM</label>
                        <input class="form-control" value="<?php echo $valores['cod_ssm']?>" readonly name="cod_ssm" />
                    </div>
                    <div class="col-xs-3">
                        <label>Data de Abertura</label>
                        <input class="form-control" value="<?php echo $valores['data_abertura']?>" readonly name="dataAberturaSaf" />
                    </div>
                    <div class="col-xs-3">
                        <label>C�digo SAF</label>
                        <input class="form-control" value="<?php echo $valores['cod_saf']?>" readonly name="cod_saf" />
                    </div>
                    <div class="col-xs-3">
                        <label>Equipe Respons�vel</label>
                        <input class="form-control" value="<?php echo $valores['nome_equipe']?>" readonly name="nome_equipe" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo</label>
                        <input class="form-control" value="<?php echo $valores['nome_grupo']?>" readonly name="grupoModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input class="form-control" value="<?php echo $valores['nome_sistema']?>" readonly name="sistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>SubSistema</label>
                        <input class="form-control" value="<?php echo $valores['nome_subsistema']?>" readonly name="subsistemaModal" />
                    </div>
                </div>
                <?php if(!empty($valores['nome_veiculo'])){?>
                <div class="row">
                    <div class="col-md-4">
                        <label>Nome do Ve�culo</label>
                        <input class="form-control" value="<?php echo $valores['nome_veiculo']?>" readonly name="subsistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Nome do Carro</label>
                        <input class="form-control" value="<?php echo $valores['nome_carro']?>" readonly name="subsistemaModal" />
                    </div>
                </div>
                <?php }?>
                <div class="row">
                    <div class="col-xs-4">
                        <label>Linha</label>
                        <input class="form-control" value="<?php echo $valores['nome_linha']?>" readonly name="linhaModal"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Trecho</label>
                        <input class="form-control" value="<?php echo $valores['nome_trecho'] . " - " . $valores['descricao_trecho']?>" readonly name="trechoModal"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Ponto Not�vel</label>
                        <input class="form-control" value="<?php echo $valores['nome_ponto']?>" readonly name="pontoNotavelModal"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Local</label>
                        <textarea class="form-control" readonly name="complementoLocalModal"><?php echo $valores['complemento_local']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Avaria</label>
                        <input class="form-control" value="<?php echo $valores['nome_avaria']?>" readonly name="avaria" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Avaria</label>
                        <textarea class="form-control" readonly name="complementoAvaria"><?php echo $valores['complemento_falha']?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Servi�o</label>
                        <input class="form-control" value="<?php echo $valores['nome_servico']?>" readonly name="servico" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Servi�o</label>
                        <textarea class="form-control" readonly name="complementoServico"><?php echo $valores['complemento_servico']?></textarea>
                    </div>
                </div>

                <?php
                foreach ($valores['registro_os'] as $dados=>$value){
                    echo "<div class='row' style='margin-top: 10px;'>
                            <div class='col-md-2'>
                                <label>C�digo da OSM</label>
                                <input class='form-control' value='{$value['cod_osm']}' type='text' readonly>
                            </div>
                            <div class='col-md-3'>
                                <label>Atua��o</label>
                                <input class='form-control' value='{$value['nome_atuacao']}' type='text' readonly>
                            </div>
                            <div class='col-md-7'>
                                <label>Descri��o da Atua��o</label>
                                <textarea class='form-control' type='text' readonly>{$value['desc_atuacao']}</textarea>
                            </div>
                        </div>";
                }
                ?>
                <div class="row" style="margin-top:20px">
                    <div class="col-md-offset-10 col-md-2">
                        <div class="panel panel-primary">
                            <div class="panel-heading">A��es</div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="allowBtn" class="btn-group  btn-group-justified">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-success btn-lg" role="group" data-toggle="modal" data-target="#validarModal">
                                                    <i class="fa fa-check fa-2x"></i>
                                                </button>
                                            </div>
                                            <div class="btn-group">
                                                <button id="denyBtn" type="button" class="btn btn-danger btn-lg" role="group" data-toggle="modal" data-target="#invalidarModal">
                                                    <i class="fa fa-close fa-2x"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </form>
</div>

<div class="modal fade" id="invalidarModal" tabindex="-1" role="form" aria-labelledby="invalidarLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="motivoLabel">Motivo da n�o aprova��o.</h4>
            </div>

            <form id="formDesaprovar" action="<?php echo HOME_URI ?>/cadastroGeral/desaprovarSsm/<?php echo $valores['cod_ssm'] ?>" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Motivo</label>
                            <textarea id="motivoInvalidar" class="form-control" name="motivoInvalidar" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnDesaprovar" class="btn btn-default btn-lg" aria-label="right align" title="Devolver">
                        <i class="fa fa-mail-reply fa-2x"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="validarModal" tabindex="-1" role="form" aria-labelledby="validarLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="motivoLabel">Observa��es de aprova��o.</h4>
            </div>

            <form id="formAprovar" action="<?php echo HOME_URI?>/cadastroGeral/aprovarSsm/<?php echo $valores['cod_ssm'] ?>" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Observa��o</label>
                            <textarea id="observacaoValidar" class="form-control" name="observacaoValidar"></textarea>
                        </div>
                        <input type="hidden" class="form-control" value="<?php echo $valores['cod_ssm']?>" readonly name="cod_ssm" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnAprovar" class="btn btn-success btn-lg" aria-label="right align" title="Devolver">
                        <i class="fa fa-check fa-2x"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
