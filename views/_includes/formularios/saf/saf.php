<?php
if (!empty($saf) && ($saf['nome_status'] == "Autorizada" || $saf['nome_status'] == "Encerrada" || $saf['nome_status'] == "Cancelada" || $saf['nome_status'] == "Programada" || $saf['nome_status'] == "Pendente" || $saf['nome_status'] == "N�o Autorizado")) {
    if ($this->dadosUsuario['nivel'] != '7.1')
        $_SESSION['bloqueio'] = true;
}

?>

<div class="page-header" xmlns="http://www.w3.org/1999/html">
    <h1>SAF - Solicita��o de Abertura de Falha</h1>
</div>

<div class="row">
    <form id="formSaf" class="form-group"
          action="<?php echo (!empty($saf)) ? HOME_URI . '/cadastroGeral/alterarSaf' : HOME_URI . '/cadastroGeral/gerarSaf'; ?>"
          method="post">
        <input type="hidden" name="altSafGerSsm" value="yes" />

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>SAF</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Solicitante</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Origem</label>
                                            <select id="codTipoFuncionario" name="codTipoFuncionario" class="form-control" required>
                                              <option value="">Selecione a op��o</option>
                                                <?php
                                                $tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);

                                                foreach ($tipo_funcionario as $dados => $value) {
                                                    if (!empty($saf) && $saf['cod_tipo_funcionario'] == $value["cod_tipo_funcionario"]) {
                                                        echo("<option value='{$value["cod_tipo_funcionario"]}' selected >{$value["descricao"]}</option>");
                                                    } else {
                                                        echo("<option value='{$value["cod_tipo_funcionario"]}'>{$value["descricao"]}</option>");
                                                    }
                                                }
                                                ?>

                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-xs-8">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Preenchimento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="responsavelSaf">Respons�vel</label>
                                            <input name="responsavelSaf" class="form-control" type="text" readonly
                                                   value="<?php echo (!empty($saf)) ? $saf["usuario"] : $this->dadosUsuario['usuario']; ?>"/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>SAF</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8 col-xs-8">
                                            <label for="dataSAF">Data</label>
                                            <input class="form-control" id="dataSaf" disabled type="datetime"
                                                   value="<?php if (!empty($saf)) echo $this->parse_timestamp($saf["data_abertura"]); ?>"/>

                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <label for="codigoSAF">N�</label>
                                            <input type="text" name="codigoSaf" class="form-control" readonly
                                                   value="<?php if (!empty($saf)) echo $saf['cod_saf']; ?>"/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    if (!empty($saf) && ($historico != "" || $saf['nome_status'] == 'Cancelada')) {
                        $historicoStatus = ($saf['nome_status'] == 'Cancelada') ? $saf['descricao_status'] . "\n" . $historico : $historico;
                        echo "<div class='row'>
                                    <div class='col-md-12'>
                                        <div class='panel panel-default'>
                                            <div class='panel-heading'><label>Situa��o da Solicita��o</label></div>

                                            <div class='panel-body'>
                                                <div class='row'>
                                                    <div class='col-md-2'>
                                                        <label for='nomeStatusSaf'>Status</label>
                                                        <input id='nomeStatusSaf' type='text' class='form-control' disabled
                                                               value='{$saf['nome_status']}'/>
                                                    </div>
                                                    <div class='col-md-10'>
                                                        <label for='descricaoStatusSaf'>Descri��o</label>
                                                            <textarea id='descricaoStatusSaf' class='form-control' disabled
                                                                      spellcheck='true'>{$historicoStatus}</textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                    }
                    ?>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o do Solicitante</label></div>

                                <div class="panel-body">
                                    <div class="col-md-2 col-xs-2"
                                         id="matriculaOrigemSaf" <?php echo (empty($saf) || $saf['tipo_orisaf'] == '1') ? 'style="display:block"' : 'style="display:none"' ?>>
                                        <label>Matr�cula</label>
                                        <input type="text" name="matriculaSolicitante" class="form-control number"
                                               value="<?php if (!empty($saf['matricula'])) echo $saf['matricula']; ?>">

                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Nome Completo</label>
                                        <input type="text" name="nomeSolicitante" class="form-control" required
                                               list="nomeSolicitanteDataList"
                                               value="<?php if (!empty($saf['nome'])) echo $saf['nome']; ?>"
                                               autocomplete="off"/>

                                        <datalist id="nomeSolicitanteDataList">
                                            <?php
                                            $selectFuncionario = $this->medoo->select("funcionario", ["matricula", "nome_funcionario"], ["AND" => ["cod_tipo_funcionario" => 1, "ativo" =>"s"],"ORDER" => "nome_funcionario"] );

                                            foreach ($selectFuncionario as $dados => $value) {
                                                echo("<option  id='{$value['matricula']}' value='{$value['nome_funcionario']} - {$value['matricula']}'/>");
                                            }
                                            ?>
                                        </datalist>

                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <label>CPF</label>
                                        <input type="text" name="cpfSolicitante" class="form-control cpf" required
                                               value="<?php if (!empty($saf['cpf'])) echo $saf['cpf']; ?>"/>

                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <label>Contato</label>
                                        <input type="text" name="contatoSolicitante" class="form-control"
                                               required
                                               value="<?php if (!empty($saf['contato'])) echo $saf['contato']; ?>"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o / Local</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="linha">Linha</label>
                                            <select id="linha" name="linhaSaf" class="form-control" required>
                                                <option disabled selected></option>

                                                <?php
                                                $linha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);

                                                foreach ($linha as $dados => $value) {
                                                    if (!empty ($saf) && $value['cod_linha'] == $saf['cod_linha'])
                                                        echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                                }
                                                ?>

                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="trechoSaf">Sigla / Trecho </label>
                                            <select id="trechoSaf" class="form-control" name="trechoSaf" required>
                                                <?php
                                                if (!empty($saf)) {
                                                    $trecho = $this->medoo->select("trecho", ['cod_trecho', 'nome_trecho', 'descricao_trecho'], ["cod_linha" => (int)$saf['cod_linha']]);

                                                    foreach ($trecho as $dados => $value) {
                                                        if ($value['cod_trecho'] == $saf['cod_trecho'])
                                                            echo('<option value="' . $value['cod_trecho'] . '" selected>' . $value['nome_trecho'] . ' - ' . $value["descricao_trecho"] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_trecho'] . '">' . $value['nome_trecho'] . ' - ' . $value["descricao_trecho"] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="pontoNotavel">Ponto Not�vel</label>
                                            <select id="pontoNotavel" name="pontoNotavelSaf" class="form-control">
                                                <?php
                                                if (!empty($saf) && $saf['cod_trecho'] != "")
                                                    $pn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["cod_trecho" => (int)$saf['cod_trecho']]);
                                                else
                                                    $pn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel', 'nome_ponto'], ["ORDER" => "cod_trecho"]);

                                                echo('<option value="">Pontos Not�veis</option>');

                                                foreach ($pn as $dados => $value) {
                                                    if ($value['cod_ponto_notavel'] == $saf['cod_ponto_notavel'])
                                                        echo('<option value="' . $value['cod_ponto_notavel'] . '" selected>' . $value['nome_ponto'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['cod_ponto_notavel'] . '">' . $value['nome_ponto'] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="complementoLocal">Complemento Local</label>
                                            <input id="complementoLocal" type="text" class="form-control"
                                                   name="complementoLocal"
                                                   value="<?php if (!empty($saf)) echo $saf['complemento_local']; ?>"/>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <label for="via">Via </label>
                                            <select name="via" class="form-control">
                                                <?php
                                                if (!empty($saf)) {
                                                    $via = $this->medoo->select("via", ["nome_via", "cod_via"], ["cod_linha" => (int)$saf['cod_linha']]);

                                                    echo('<option value="">Selecione a Via</option>');

                                                    foreach ($via as $dados => $value) {
                                                        if ($value['cod_via'] == $saf['cod_via'])
                                                            echo('<option value="' . $value['cod_via'] . '" selected>' . $value['nome_via'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_via'] . '">' . $value['nome_via'] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="posicaoLocal">Posi��o</label>
                                            <input id="posicaoLocal" type="text" class="form-control"
                                                   name="posicaoLocal"
                                                   value="<?php if (!empty($saf)) echo $saf['posicao']; ?>"/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="kmInicial">Km Inicial </label>
                                            <input id="kmInicial" name="kmInicial" class="form-control"
                                                   value="<?php if (!empty($saf)) echo $saf['km_inicial']; ?>"/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="kmFinal">Km Final </label>
                                            <input id="kmFinal" name="kmFinal" class="form-control"
                                                   value="<?php if (!empty($saf)) echo $saf['km_final']; ?>"/>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label for="gSistemaNumber">N� </label>
                                            <input id="gSistemaNumber" disabled name="numeroGrupoSistema"
                                                   value="<?php if (!empty($saf)) echo $saf['cod_grupo']; ?>"
                                                   class="form-control"/>

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label for="gSistema">Grupo Sistema</label>
                                            <?php
                                            if(!empty($saf['cod_grupo']))
                                            {
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo']);
                                            }else
                                            {
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S', "ORDER" => "nome_grupo"]);
                                            }
                                            $this->form->getSelectGrupo($saf['cod_grupo'], $grupo, 'gSistema', false);
                                            ?>

                                        </div>
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label for="sistemaNumber">N� </label>
                                            <input id="sistemaNumber" disabled name="numeroSistema"
                                                   value="<?php if (!empty($saf)) echo $saf['cod_sistema']; ?>"
                                                   class="form-control"/>

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label for="sistema">Sistema </label>
                                            <select id="sistema" name="sistema" class="form-control"
                                                    required="required">
                                                <?php
                                                if (!empty($saf)) {
                                                    $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ["cod_grupo" => $saf['cod_grupo'], 'ORDER' => 'nome_sistema']);

                                                    echo('<option disabled selected>Selecione o Sistema</option>');

                                                    foreach ($sistema as $dados => $value) {
                                                        if ($saf['cod_sistema'] == $value["cod_sistema"])
                                                            echo('<option value="' . $value["cod_sistema"] . '" selected>' . $value["nome_sistema"] . '</option>');
                                                        else
                                                            if($value['cod_sistema'] != 174)
                                                                echo('<option value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>');
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input type="text" class="form-control" name="codSubSistema"
                                                   value="<?php if (!empty($saf)) echo $saf['cod_subsistema']; ?>"
                                                   disabled>
                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sub-Sistema</label>
                                            <select class="form-control" name="subSistemaSaf" required>
                                                <option disabled selected> Sub-Sistema</option>
                                                <?php
                                                if (!empty($saf) && $saf['cod_sistema']) {
                                                    $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['cod_sistema' => $saf['cod_sistema']]);

                                                    foreach ($subsistema as $dados => $value) {
                                                        if ($saf['cod_subsistema'] == $value["cod_subsistema"])
                                                            echo('<option value="' . $value["cod_subsistema"] . '" selected>' . $value["nome_subsistema"] . '</option>');
                                                        else
                                                            echo('<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php
                                    if ($saf['cod_grupo'] == '22' || $saf['cod_grupo'] == '23' || $saf['cod_grupo'] == '26') {
                                        $materialRodante = true;
                                    }else{
                                        $materialRodante = false;
                                    }
                                    ?>
                                    <div class="row" id="divMaterialRodante"
                                         style="display:<?php echo ($materialRodante) ? "display" : "none" ?>">
                                        <div class="col-md-2">
                                            <label>C�digo Ve�culo</label>
                                            <select class="form-control"
                                                    name="veiculoMrSaf" <?php if($materialRodante) echo "required"; ?>>
                                                <option value="" selected disabled>Ve�culo</option>
                                                <?php
                                                if ($saf['cod_veiculo'] && $saf['cod_grupo']) {
                                                    $selectVeiculo = $this->medoo->select("veiculo", "*", ['cod_grupo' => (int)$saf['cod_grupo']]);

                                                    foreach ($selectVeiculo as $dados => $value) {
                                                        if ($saf['cod_veiculo'] == $value['cod_veiculo']) {
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        } else {
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Carro Avariado</label>
                                            <select name="carroMrSaf"
                                                    class="form-control" <?php if($materialRodante) echo "required"; ?>>
                                                <?php
                                                if ($saf['cod_carro'] && $saf['cod_veiculo']) {
                                                    $selectCarro = $this->medoo->select("carro", "*", ['cod_veiculo' => (int)$saf['cod_veiculo']]);

                                                    foreach ($selectCarro as $dados => $value) {
                                                        if ($saf['cod_carro'] == $value['cod_carro']) {
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                        } else {
                                                            echo("<option value='{$value['cod_carro']}'>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Od�metro</label>
                                            <input
                                            <?php
                                                if(!empty($saf['odometro']))
                                                    echo " value='{$saf['odometro']}' ";
                                                else{
                                                    if($saf['cod_carro']){
                                                        $odometro = $this->medoo->select("material_rodante_saf", "*",
                                                            [
                                                                "cod_saf" => $saf['cod_saf']
                                                            ]
                                                        );
                                                        echo " value='{$odometro[0]['odometro']}' ";
                                                    }
                                                }
                                                if($materialRodante)
                                                    echo " required ";
                                            ?>
                                                   class="form-control number" type="text" name="odometroMrSaf">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Pref�xo</label>
                                            <select type="text" name="prefixoMrSaf" class="form-control">
                                                <option value="">Pref�xo</option>
                                                <?php
                                                if ($saf['cod_linha']) {
                                                    $prefixo = $this->medoo->select("prefixo", "*", ["cod_linha" => $saf['cod_linha']]);

                                                    foreach ($prefixo as $dados => $value) {
                                                        if ($saf['cod_prefixo'] == $value['cod_prefixo'])
                                                            echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");

                                                    }
                                                } else {
                                                    echo("<option value=''>Sem Prefixo</option>");
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php
                                    if ($saf['cod_grupo'] == '27' || $saf['cod_grupo'] == '28') {
                                        $bilhetagem = true;
                                    }else{
                                        $bilhetagem = false;
                                    }
                                    ?>
                                    <div class="row" id="sublocal" style="display: <?php echo ($bilhetagem) ? "display" : "none" ?>;">
                                        <div class="col-md-4">
                                            <label>Local</label>
                                            <select name="localGrupo" class="form-control" id="local_grupo">
                                                <?php
                                                $local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
                                                echo ("<option value='' disabled selected>Selecione o Local</option>");
                                                foreach($local as $dados=>$value){

                                                    if($saf['cod_local_grupo'] == $value['cod_local_grupo']){
                                                        echo ("<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>");
                                                    }else {
                                                        echo("<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>");
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Sub-Local</label>
                                            <select name="subLocalGrupo" id="sublocal_grupo" class="form-control">
                                            <?php
                                            if($saf['cod_sublocal_grupo']){
                                                $selectLocal = $this->medoo->select('sublocal_grupo', ['[><]local_sublocal_grupo' => 'cod_sublocal_grupo'], '*', ['cod_local_grupo' => $saf['cod_local_grupo']]);

                                                foreach ( $selectLocal as $dados=>$value){
                                                    if($saf['cod_sublocal_grupo'] == $value['cod_sublocal_grupo']){
                                                        echo ("<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>");
                                                    }else {
                                                        echo("<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>");
                                                    }
                                                }
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Avaria</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1 col-xs-3" style="display: none;">
                                            <label for="codigoAvaria">C�digo</label>
                                            <input name="avaria" readonly required
                                                   value="<?php if (!empty($saf)) echo $saf['cod_avaria']; ?>"
                                                   class="form-control"/>

                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <label for="avariaSaf">Op��o de Avaria</label>
                                            <input id="avariaSaf" name="avariaSaf" class="form-control"
                                                   placeholder="Selecione uma Avaria"
                                                   value="<?php if (!empty($saf)) echo $saf['nome_avaria']; ?>"
                                                   required list="avariaSafDataList" autocomplete="off">

                                            <datalist id="avariaSafDataList">
                                                <option value=""></option>
                                                <?php
                                                if ($saf['cod_grupo']) {
                                                    if($saf['cod_grupo'] == 22 || $saf['cod_grupo'] == 23 || $saf['cod_grupo'] == 26)
                                                        $avaria = $this->medoo->select('avaria', ['cod_avaria', 'nome_avaria'], ['cod_grupo' => $saf['cod_grupo']]);
                                                    else
                                                        $avaria = $this->medoo->query("SELECT * FROM avaria WHERE cod_grupo is null OR cod_grupo = {$saf['cod_grupo']}")->fetchAll();

                                                    foreach ($avaria as $dados) {
                                                        echo strtoupper("<option id='{$dados['cod_avaria']}' value='{$dados['nome_avaria']}'/>");
                                                    }
                                                }
                                                ?>
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="complementoAvaria">Descri��o</label>
                                                    <textarea id="complementoAvaria" name="complementoAvaria"
                                                              class="form-control" rows="3"
                                                              spellcheck="true"><?php if (!empty($saf)) echo $saf['complemento_falha']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="nivel">N�vel</label>
                                                    <a data-toggle="modal" data-target="#modalHelpNivel"
                                                       class="btn-circle hidden-print"><i
                                                                style="font-size: 20px"
                                                                class="fa fa-question-circle fa-fw"></i></a>
                                                    <select id="nivel" name="nivelSaf" class="form-control">
                                                        <option data-toggle="tooltip"
                                                                title="Urgente: Interrompe a circula��o."
                                                                value="A" <?php if ($saf['nivel'] == "A") echo("selected") ?>>
                                                            A
                                                        </option>
                                                        <option title="Emergencial: Atrapalha a circula��o."
                                                                value="B" <?php if ($saf['nivel'] == "B") echo("selected") ?>>
                                                            B
                                                        </option>
                                                        <option title="Necess�rio: N�o afeta diretamente a circula��o."
                                                                value="C" <?php echo ($saf['nivel'] == "C" || empty($saf)) ? "selected" : "" ?>>
                                                            C
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <div class="col-md-offset-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">A��es</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group  btn-group-justified " role="group">

                                                <?php echo $btnsAcao; ?>

                                            </div>
                                            <div class="modal fade modal-historico" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                                                            <i aria-hidden="true" class="fa fa-close"></i></button>
                                                            <h4 class="modal-title">Hist�rico da SAF</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <?php
                                                            if(!empty($saf)) {
                                                                $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_saf s
                                                            JOIN usuario u on s.usuario = u.cod_usuario
                                                            JOIN status USING(cod_status)
                                                            WHERE cod_saf = {$saf['cod_saf']} ORDER BY data_status";

                                                                if ($sql != "")
                                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                            }
                                                            ?>

                                                            <div class = "row">
                                                                <div class="col-md-12">
                                                                    <table class="tableRel table-bordered dataTable">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Status</th>
                                                                            <th>Responsavel</th>
                                                                            <th>Data da altera��o</th>
                                                                            <th>Complemento</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php
                                                                        if(!empty($result)){
                                                                            foreach ($result as $dados){
                                                                                echo("<tr>");
                                                                                echo("<td>{$dados['nome_status']}</td>");
                                                                                echo("<td>{$dados['usuario']}</td>");
                                                                                echo("<td>{$this->parse_timestamp($dados['data_status'])}</td>");
                                                                                echo("<td>{$dados['descricao']}</td>");
                                                                                echo("</tr>");
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
