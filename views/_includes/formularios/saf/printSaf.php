<?php
if (!empty ($_SESSION['refillSaf'])) {
    $saf = $this->medoo->select('v_saf', '*', [
        "cod_saf" => (int)$_SESSION['refillSaf']['codigoSaf']
    ]);
    $saf = $saf[0];
    unset($_SESSION['refillSaf']);
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>SAF - Solicita��o de Abertura de Falha</h3>
    </div>

    <hr/>

    <div class="title-table">&raquo; Dados Gerais</div>
    <table>
        <tr>
            <th>SAF</th>
            <th>Origem</th>
            <th>Respons�vel</th>
            <th>Status</th>
            <th>N�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo $saf['cod_saf']; ?></td>
            <td class="center">
            <?php
            $tipo_funcionario = $this->medoo->select('tipo_funcionario', '*');

            foreach($tipo_funcionario as $dados=>$value)
            {
              if($value['cod_tipo_funcionario'] == $saf['cod_tipo_funcionario'])
                echo $value['descricao'];
            }
            ?>
            </td>
            <td class="center"><?php echo($saf['usuario']) ?></td>
            <td class="center"><?php echo $saf['nome_status']; ?></td>
            <td class="center"><?php echo $saf['nivel']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Data/Hora Abertura</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo($this->parse_timestamp($saf['data_abertura'])) ?></td>
        </tr>
        <?php if ($saf['descricao_status'] != "") { ?>
            <tr>
                <th colspan="5">Descri��o do Status</th>
            </tr>
            <tr>
                <td colspan='5'><?php echo $saf['descricao_status'] ?></td>
            </tr>
        <?php } ?>
    </table>

    <div class="title-table">&raquo; Identifica��o do Solicitante</div>
    <table>
        <tr>
            <?php
                echo('<th>Matricula</th>');
            ?>
            <th>Nome Completo</th>
            <th>CPF</th>
            <th>Contato</th>
        </tr>
        <tr>
            <?php
                echo('<td class="center">' . $saf['matricula'] . '</td>');
            ?>
            <td><?php echo($saf['nome']); ?></td>
            <td class="center"><?php echo($saf['cpf']); ?></td>
            <td class="center"><?php echo($saf['contato']); ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o / Local</div>
    <table>
        <tr>
            <th>Linha</th>
            <th colspan="3">Trecho</th>
            <th colspan="2">Ponto Not�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo($saf['nome_linha']); ?></td>
            <td colspan="3" class="center"><?php echo($saf['nome_trecho'] . ' - ' . $saf['descricao_trecho']); ?></td>
            <td colspan="2" class="center"><?php echo($saf['nome_ponto']); ?></td>
        </tr>
        <tr>
            <th colspan="6">Complemento</th>
        </tr>
        <tr>
            <td colspan="6"><?php echo($saf['complemento_local']) ?></td>
        </tr>
        <tr>
            <th colspan="2">Via</th>
            <th>km Inicial</th>
            <th>km Final</th>
            <th colspan="2">Posi��o</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo($saf['nome_via']); ?></td>
            <td class="center"><?php echo($saf['km_inicial']); ?></td>
            <td class="center"><?php echo($saf['km_final']); ?></td>
            <td colspan="2" class="center"><?php echo($saf['posicao']); ?></td>
        </tr>
        <tr>
            <th colspan="3">Grupo Sistema</th>
            <th colspan="3">Sistema</th>
        </tr>
        <tr>
            <td colspan="3" class="center"><?php echo($saf['nome_grupo']); ?></td>
            <td colspan="3" class="center"><?php echo($saf['nome_sistema']); ?></td>
        </tr>
        <tr>
            <th colspan="6">SubSistema</th>
        </tr>
        <tr>
            <td colspan="6" class="center"><?php echo($saf['nome_subsistema']); ?></td>
        </tr>
        <tr>
            <th colspan="3">Local</th>
            <th colspan="3">Sublocal</th>
        </tr>
        <tr>
            <td colspan="3" class="center"><?php echo($saf['nome_local_grupo']); ?></td>
            <td colspan="3" class="center"><?php echo($saf['nome_sublocal_grupo']); ?></td>
        </tr>
        <tr>
    </table>

    <?php if (!empty($saf['nome_veiculo'])){ ?>
        <div class="title-table">&raquo; Material Rodante</div>
        <table>
            <tr>
                <th>Ve�culo</th>
                <th>Carro Avariado</th>
                <th>Od�metro</th>
                <th>Pref�xo</th>
            </tr>
            <tr>
                <td><?php echo($saf['nome_veiculo']) ?></td>
                <td><?php echo($saf['nome_carro']) ?></td>
                <td><?php echo($saf['odometro']) ?></td>
                <td><?php echo($saf['nome_prefixo']) ?></td>
            </tr>
        </table>
    <?php } ?>

    <div class="title-table">&raquo; Avaria</div>
    <table>
        <tr>
            <th>Avaria</th>
        </tr>
        <tr>
            <td><?php echo($saf['nome_avaria']) ?></td>
        </tr>
        <tr>
            <th>Complemento</th>
        </tr>
        <tr>
            <td><?php echo($saf['complemento_falha']) ?></td>
        </tr>
    </table>
</div>
