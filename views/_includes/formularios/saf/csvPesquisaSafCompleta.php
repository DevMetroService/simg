<?php

$conteudo = "N. SAF;";
$conteudo .= "Origem;";
$conteudo .= "Responsável;";
$conteudo .= "Data de Abertura;";
$conteudo .= "Status;";
$conteudo .= "Descrição Status;";
$conteudo .= "Nível;";
$conteudo .= "Matricula Solicitante;";
$conteudo .= "Nome Solicitante;";
$conteudo .= "CPF Solicitante;";
$conteudo .= "Contato Solicitante;";
$conteudo .= "Linha;";
$conteudo .= "Trecho;";
$conteudo .= "Ponto Notável;";
$conteudo .= "Complemento Local;";
$conteudo .= "Via;";
$conteudo .= "Km Inicial;";
$conteudo .= "Km Final;";
$conteudo .= "Posição;";
$conteudo .= "Grupo;";
$conteudo .= "Sistema;";
$conteudo .= "Sub-Sistema;";
$conteudo .= "Veículo;";
$conteudo .= "Carro;";
$conteudo .= "Odômetro;";
$conteudo .= "Avaria;";
$conteudo .= "Complemento Avaria;";
$conteudo .= "Local;";
$conteudo .= "Sub-Local;\n";

$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)){
    foreach ($returnPesquisa as $dados=>$value) {
        switch($value["tipo_orisaf"]){
            case 1:
                $origem ="MetroService";
                break;
            case 2:
                $origem ="MetroFor";
                break;
            case 3:
                $origem ="PJ - MetroService";
                break;
            default:
                $origem ="Terceiros";
        }

        $txt = $value["cod_saf"] . ":::";
        $txt .= utf8_decode($origem) . ":::";
        $txt .= $value["usuario"] . ":::";
        $txt .= $this->parse_timestamp($value["data_abertura"]) . ":::";
        $txt .= $value["nome_status"] . ":::";
        $txt .= $value["descricao_status"] . ":::";
        $txt .= $value["nivel"] . ":::";
        $txt .= $value["matricula"] . ":::";
        $txt .= $value["nome"] . ":::";
        $txt .= $value["cpf"] . ":::";
        $txt .= $value["contato"] . ":::";
        $txt .= $value["nome_linha"] . ":::";
        $txt .= $value["nome_trecho"] . " - " . $value["descricao_trecho"] . ":::";
        $txt .= $value["nome_ponto"] . ":::";
        $txt .= $value["complemento_local"] . ":::";
        $txt .= $value["nome_via"] . ":::";
        $txt .= $value["km_inicial"] . ":::";
        $txt .= $value["km_final"] . ":::";
        $txt .= $value["posicao"] . ":::";
        $txt .= $value["nome_grupo"] . ":::";
        $txt .= $value["nome_sistema"] . ":::";
        $txt .= $value["nome_subsistema"] . ":::";
        $txt .= $value["nome_veiculo"] . ":::";
        $txt .= $value["nome_carro"] . ":::";
        $txt .= $value["odometro"] . ":::";
        $txt .= $value["nome_avaria"] . ":::";
        $txt .= $value["complemento_falha"] . ":::";
        $txt .= $value["nome_local_grupo"] . ":::";
        $txt .= $value["nome_sublocal_grupo"] . ":::";

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("SafCompleta.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=SafCompleta.csv");
readfile(TEMP_PATH."SafCompleta.csv");

unlink(TEMP_PATH."SafCompleta.csv");
