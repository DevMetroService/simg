<div class="row">
    <div class="col-md-8">
        <label>Nome</label>
        <input value="<?php echo $dados['nome_servico_pmp'] ?>" disabled class="form-control"/>
    </div>
    <div class="col-md-2 col-xs-6">
        <label>Procedimento</label>
        <button class="exibirPdfLink button form-control" value="dashboardDiretoria/exibirPdfProcedimento:<?php echo $dados['nome_procedimento'] ?>"><?php echo $dados['nome_procedimento'] ?></button>
    </div>
    <div class="col-md-2 col-xs-6">
        <label>Periodicidade</label>
        <input value="<?php echo $dados['nome_periodicidade'] ?>" disabled class="form-control"/>
    </div>
</div>