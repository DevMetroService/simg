<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Identifica��o / Local</label></div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>Linha</label>
                        <input value="<?php echo $dados['nome_linha'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Local</label>
                        <input value="<?php echo $dados['nome_local'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Poste</label>
                        <input value="<?php echo $dados['nome_poste'] ?>" disabled class="form-control"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label>Via</label>
                        <input value="<?php echo $dados['nome_via'] ?>" disabled class="form-control"/>

                    </div>

                    <div class="col-md-2">
                        <label>Posi��o</label>
                        <input value="<?php echo $dados['posicao'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-8">
                        <label>Complemento</label>
                        <input name="complemento" value="<?php if (!empty($dados['complemento'])) echo $dados['complemento'] ?>" type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo de Sistema</label>
                        <input value="Rede A�rea" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input value="<?php echo($dados['nome_sistema']); ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Sub-Sistema</label>
                        <input value="<?php echo($dados['nome_subsistema']); ?>" disabled class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>