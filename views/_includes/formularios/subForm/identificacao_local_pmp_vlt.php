<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Identificação / Local</label></div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>Linha</label>
                        <input value="<?php echo $dados['nome_linha'] ?>" disabled class="form-control"/>
                    </div>
                    <div class="col-md-4">
                        <label>Veiculo</label>
                        <input value="<?php echo $dados['nome_veiculo'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-6">
                        <label>Complemento Local</label>
                        <input name="complemento" value="<?php if (!empty($dados['complemento'])) echo $dados['complemento'] ?>" type="text" class="form-control"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo de Sistema</label>
                        <input value="Subestação" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input value="<?php echo($dados['nome_sistema']); ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Sub-Sistema</label>
                        <input value="<?php echo($dados['nome_subsistema']); ?>" disabled class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>