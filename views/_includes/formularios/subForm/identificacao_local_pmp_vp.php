<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Identifica��o / Local</label></div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>Linha</label>
                        <input value="<?php echo $dados['nome_linha'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Esta��o Inicial</label>
                        <input value="<?php echo $dados['estacao_inicial'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Esta��o Final</label>
                        <input value="<?php echo $dados['estacao_final'] ?>" disabled class="form-control"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <label>Via</label>
                        <input value="<?php echo $dados['nome_via'] ?>" disabled class="form-control"/>
                    </div>
                    <div class="col-md-3">
                        <label>Km Inicial<?php echo (strtolower($tipoOS) == 'osmp/vp' || $tipoOS == 'osmp/Vp') ? ' Atuado' : '' ?></label>
                        <input <?php
                        if (strtolower($tipoOS) == 'osmp/vp' || $tipoOS == 'osmp/Vp') {
                            echo "value='{$dados['km_inicial']}' name='km_inicial' required";
                        } else {
                            echo "value='{$dados['km_inicial']}' disabled";
                        }
                        ?> class="form-control"/>
                    </div>
                    <div class="col-md-3">
                        <label>Km Final<?php echo (strtolower($tipoOS) == 'osmp/vp' || $tipoOS == 'osmp/Vp') ? ' Atuado' : '' ?></label>
                        <input <?php
                        if (strtolower($tipoOS) == 'osmp/vp' || $tipoOS == 'osmp/Vp') {
                            echo "value='{$dados['km_final']}' name='km_final' required";
                        } else {
                            echo "value='{$dados['km_final']}' disabled";
                        }
                        ?> class="form-control"/>
                    </div>

                    <div class="col-md-3">
                        <label>AMV</label>
                        <input value="<?php echo $dados['nome_amv'] ?>" disabled class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento</label>
                        <input name="complemento"
                               value="<?php if (!empty($dados['complemento'])) echo $dados['complemento'] ?>"
                               type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo de Sistema</label>
                        <input value="Via Permanente" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input value="<?php echo($dados['nome_sistema']); ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-4">
                        <label>Sub-Sistema</label>
                        <input value="<?php echo($dados['nome_subsistema']); ?>" disabled class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>