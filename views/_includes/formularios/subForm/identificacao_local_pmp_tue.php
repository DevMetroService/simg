<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Identifica��o / Servi�o</label></div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>Linha</label>
                        <input value="<?php echo $dados['nome_linha'] ?>" disabled class="form-control"/>
                    </div>
                    <div class="col-md-4">
                        <label>Veiculo</label>
                        <input value="<?php echo $dados['nome_veiculo'] ?>" disabled class="form-control"/>
                    </div>

                    <div class="col-md-6">
                        <label>Complemento</label>
                        <input name="complemento" value="<?php if (!empty($dados['complemento'])) echo $dados['complemento'] ?>" type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Servi�o</label>
                        <input value="<?php echo $dados['nome_servico_pmp'] ?>" disabled class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>