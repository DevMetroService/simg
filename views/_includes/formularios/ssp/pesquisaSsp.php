<div class="page-header">
    <h2>Pesquisa de Solicita��o de Servi�o Programado</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/executarPesquisaSsp" class="form-group" method="post" id="pesquisa">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Ssp</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDadosGerais">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#dadosGeraisPesquisa" aria-expanded="false" aria-controls="collapseDadosGerais">
                                        <button class="btn btn-default"><label>Dados Gerais</label></button>
                                    </a>
                                </div>

                                <div id="dadosGeraisPesquisa" class="panel-collapse
                                    <?php
                                    if (!empty($dadosRefill['numeroSsp']) || !empty($dadosRefill['solicitanteUsuario']) || !empty($dadosRefill['origemProgramacao']) || !empty($dadosRefill['numeroSA'])) {
                                        echo('in');
                                    } else {
                                        echo('out');
                                    }
                                    ?>
                                    collapse" role="tabpanel" aria-labelledby="headingDadosGerais">
                                        
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>N� SSP</label>
                                                <input class="form-control" name="numeroSsp"
                                                    <?php
                                                    if(!empty($dadosRefill['numeroSsp'])){
                                                        echo ('value="' . $dadosRefill['numeroSsp'] . '"');
                                                    }
                                                    ?>
                                                       type="text">
                                            </div>

                                            <div class="col-md-4">
                                                <label>Respons�vel pelo preenchimento</label>
                                                <select class="form-control" name="solicitanteUsuario">
                                                    <option value="">Selecione o usuario</option>
                                                    <?php
                                                    $selectUser = $this->medoo->select("usuario", ['cod_usuario', 'usuario'], ["nivel" => ['5', '2'],'ORDER' => 'usuario']);
                                                    foreach ($selectUser as $dados => $value){
                                                        if(!empty($dadosRefill['solicitanteUsuario']) && $dadosRefill['solicitanteUsuario'] == $value['cod_usuario'])
                                                            echo ("<option value='{$value['cod_usuario']}' selected>{$value['usuario']}</option>");
                                                        else
                                                            echo ("<option value='{$value['cod_usuario']}'>{$value['usuario']}</option>");
                                                    }

                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <label>Origem</label>
                                                <select name="origemProgramacao" class="form-control">
                                                    <option value="" selected>Ambos</option>
                                                    <?php
                                                    $selectop = array(
                                                        "1" => "Preventiva",
                                                        "2" => "Corretiva",
                                                        "3" => "Programa��o",
                                                        "4" => "Servi�o",
                                                        "5" => "Preditiva",
                                                        "6" => "Corretiva MR",
                                                        "7" => "Melhoria"// Material Rodante
                                                    );
                                                    foreach($selectop as $dados => $value){
                                                        echo("<option value='" . $dados . "' ");

                                                        if(!empty($dadosRefill['origemProgramacao']) && $dadosRefill['origemProgramacao'] == $dados) {
                                                            echo("selected");
                                                        }
                                                        echo(">" . $value . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <label>S.A.</label>
                                                <input class="form-control" name="numeroSA"
                                                    <?php
                                                    if(!empty($dadosRefill['numeroSA'])){
                                                        echo ('value="' . $dadosRefill['numeroSA'] . '"');
                                                    }
                                                    ?>
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingStatus">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#statusPesquisa" aria-expanded="false"
                                       aria-controls="collapseStatus">
                                        <button class="btn btn-default"><label>Status</label></button>
                                    </a>
                                </div>

                                <div id="statusPesquisa" class="panel-collapse
                                    <?php
                                        if (!empty($dadosRefill['dataPartir']) || !empty($dadosRefill['dataRetroceder']) || !empty($dadosRefill['statusSspPesquisa']) || !empty($dadosRefill['dataPartirAbertura']) || !empty($dadosRefill['dataRetrocederAbertura'])) {
                                            echo('in');
                                        } else {
                                            echo('out');
                                        }
                                    ?>
                                     collapse" role="tabpanel" aria-labelledby="headingStatus">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Abertura (a Partir)</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataPartirAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataPartirAbertura'])) echo($dadosRefill['dataPartirAbertura']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataAteAbertura"
                                                       value="<?php if (!empty($dadosRefill['dataAteAbertura'])) echo($dadosRefill['dataAteAbertura']); ?>"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Status Atual</label>
                                                <select name="statusPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $statusSaf = $this->medoo->select('status', ['cod_status', 'nome_status'], ['tipo_status' => 'P']);
                                                    foreach ($statusSaf as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['statusPesquisa'] == $value['cod_status'])
                                                            echo("<option value='{$value['cod_status']}' selected>{$value['nome_status']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_status']}'>{$value['nome_status']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Data do Status (a Partir)</label>
                                                <input id="dataPartir" class="form-control data validaData"
                                                       type="text" name="dataPartir"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataPartir']; ?>"/>
                                            </div>
                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input id="dataRetroceder" class="form-control data validaData"
                                                       type="text" name="dataRetroceder"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['dataRetroceder']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingLocal">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#localPesquisa" aria-expanded="false"
                                       aria-controls="collapseLocal">
                                        <button class="btn btn-default"><label>Local</label></button>
                                    </a>
                                </div>

                                <div id="localPesquisa" class="panel-collapse
                                <?php
                                if (!empty($dadosRefill['linhaPesquisaSsp']) || !empty($dadosRefill['trechoPesquisaSsp']) || !empty($dadosRefill['pnPesquisaSsp'])) {
                                    echo('in');
                                } else {
                                    echo('out');
                                }
                                ?>
                                     collapse" role="tabpanel" aria-labelledby="headingLocal">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Linha</label>
                                                <select class="form-control" name="linhaPesquisaSsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $linha= $this->medoo->select('linha', ['cod_linha', 'nome_linha'], ['ORDER' => "cod_linha"]);

                                                    foreach($linha as $dados => $value){
                                                        echo('<option value="'.$value['cod_linha']. '" ');
                                                        if(!empty ($dadosRefill['linhaPesquisaSsp']) && $value['cod_linha'] == $dadosRefill['linhaPesquisaSsp']){
                                                            echo(' selected');
                                                        }
                                                        echo(' >'. $value['nome_linha'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Trecho</label>
                                                <select class="form-control" name="trechoPesquisaSsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if(!empty($dadosRefill['linhaPesquisaSsp']) && $dadosRefill['linhaPesquisaSsp'] != ""){
                                                        $selectTrecho = $this->medoo->select("trecho", ['cod_trecho','nome_trecho','descricao_trecho'],["cod_linha" => (int)$dadosRefill['linhaPesquisaSsp']]);
                                                    }else{
                                                        $selectTrecho = $this->medoo->select("trecho", ['cod_trecho','nome_trecho','descricao_trecho'], ["ORDER" => "cod_linha"]);
                                                    }

                                                    foreach($selectTrecho as $dados => $value){
                                                        echo("<option value='" . $value['cod_trecho'] . "'");
                                                        if(!empty($dadosRefill['trechoPesquisaSsp']) && $dadosRefill['trechoPesquisaSsp'] == $value['cod_trecho']) {
                                                            echo(" selected");
                                                        }
                                                        echo(">" . $value["nome_trecho"] . " - " . $value['descricao_trecho'] . "</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Ponto Not�vel</label>
                                                <select class="form-control" name="pnPesquisaSsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if($dadosRefill['linhaPesquisaSsp'] != "" && $dadosRefill['trechoPesquisaSsp'] == ""){

                                                    }else{
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaSsp'] != "")
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel','nome_ponto'], ["cod_trecho" => (int)$dadosRefill['trechoPesquisaSsp']]);
                                                        else
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel','nome_ponto'], ["ORDER" => "cod_trecho"]);

                                                        foreach ($selectPn as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['pnPesquisaSsp'] == $value['cod_ponto_notavel'])
                                                                echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSolicitante">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#solicitantePesquisa" aria-expanded="false"
                                       aria-controls="collapseSolicitante">
                                        <button class="btn btn-default"><label>Solicitante</label></button>
                                    </a>
                                </div>

                                <div id="solicitantePesquisa" class="panel-collapse
                                <?php
                                if (!empty($dadosRefill['matriculaSolicitante']) || !empty($dadosRefill['cpfSolicitante'])) {
                                    echo('in');
                                } else {
                                    echo('out');
                                }
                                ?>
                                     collapse" role="tabpanel" aria-labelledby="headingSolicitante">

                                    <div class="panel-body">
                                        <div class="row">

                                            <div class="col-md-2">
                                                <label>Matricula</label>
                                                <input type="text"
                                                    <?php
                                                    if(!empty($dadosRefill['matriculaSolicitante'])){
                                                        echo(' value="'.$dadosRefill['matriculaSolicitante'].'" ');
                                                    }
                                                    ?>
                                                       name="matriculaSolicitante" class="form-control">
                                            </div>

                                            <div class="col-md-3">
                                                <label>CPF</label>
                                                <input type="text"
                                                    <?php
                                                    if(!empty($dadosRefill['cpfSolicitante'])){
                                                        echo(' value="'.$dadosRefill['cpfSolicitante'].'" ');
                                                    }
                                                    ?>
                                                       name="cpfSolicitante" class="form-control cpf">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingServico">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#servicoPesquisa" aria-expanded="false"
                                       aria-controls="collapseServico">
                                        <button class="btn btn-default"><label>Servi�o</label></button>
                                    </a>
                                </div>

                                <div id="servicoPesquisa" class="panel-collapse
                                <?php
                                    if (!empty($dadosRefill['grupoSistemaPesquisa']) || !empty($dadosRefill['sistemaPesquisa']) || !empty($dadosRefill['subSistemaPesquisa']) ||
                                        !empty($dadosRefill['tipoIntervencaoPesquisa']) || !empty($dadosRefill['servicoPesquisa'])) {
                                        echo(' in');
                                    } else {
                                        echo(' out');
                                    }
                                ?> collapse" role="tabpanel" aria-labelledby="headingServico">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grupo de Sistema</label>
                                                <?php
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ORDER" => "nome_grupo"]);
                                                $this->form->getSelectGrupo($dadosRefill['grupoSistemaPesquisa'], $grupo, 'grupoSistemaPesquisa', true);
                                                ?>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Sistema</label>
                                                <select name="sistemaPesquisa" class="form-control">
                                                    <option value="">Sistemas</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa'])
                                                        $sistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], ['cod_sistema','nome_sistema'], ["cod_grupo" => $dadosRefill['grupoSistemaPesquisa']]);
                                                    else
                                                        $sistema = $this->medoo->select("sistema", ['cod_sistema','nome_sistema']);

                                                    foreach ($sistema as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisa'] == $value['cod_sistema'])
                                                            echo("<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Sub-Sistema</label>
                                                <select class="form-control" name="subSistemaPesquisa">
                                                    <option value="">Sub-Sistemas</option>
                                                    <?php
                                                    if($dadosRefill['grupoSistemaPesquisa'] != "" && $dadosRefill['sistemaPesquisa'] == ""){

                                                    }else{
                                                        if($dadosRefill['sistemaPesquisa'])
                                                            $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"],['cod_subsistema','nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisa'] ]);
                                                        else
                                                            $subsistema = $this->medoo->select("subsistema", ['cod_subsistema','nome_subsistema']);

                                                        foreach ($subsistema as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisa'] == $value['cod_subsistema'])
                                                                echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Tipo de Interven��o</label>
                                                <select name="tipoIntervencaoPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $tipoIntervencao = $this->medoo->select("tipo_intervencao", ["cod_tipo_intervencao","nome_tipo_intervencao"] , ["ORDER" => "nome_tipo_intervencao"]);

                                                    foreach($tipoIntervencao as $dados=>$value){
                                                        echo('<option ');
                                                        if(!empty($dadosRefill['tipoIntervencaoPesquisa']) && $value['cod_tipo_intervencao'] == $dadosRefill['tipoIntervencaoPesquisa']){
                                                            echo(' selected');
                                                        }
                                                        echo(' value="'.$value['cod_tipo_intervencao']. '">'. $value['nome_tipo_intervencao'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <label>Servi�o</label>
                                                <select name="servicoPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $servicos = $this->medoo->select("servico", ["nome_servico", "cod_servico"],["ORDER" => "nome_servico"]);

                                                    foreach ($servicos as $dados => $value){
                                                        echo('<option value="'.$value['cod_servico'].'" ');
                                                        if(!empty($dadosRefill['servicoPesquisa']) && $dadosRefill['servicoPesquisa'] == $value['cod_servico']){
                                                            echo('selected');
                                                        }
                                                        echo('>'.$value['nome_servico'].'</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default mtRod" hidden>
                                <div class="panel-heading" role="tab" id="headingMaterialRodante">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#materialRodantePesquisa" aria-expanded="false"
                                       aria-controls="collapseMaterialRodante">
                                        <button class="btn btn-default"><label>Material Rodante</label></button>
                                    </a>
                                </div>

                                <div id="materialRodantePesquisa" role="tabpanel"
                                     aria-labelledby="headingMaterialRodante"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['veiculoPesquisa']) || !empty($dadosRefill['carroAvariadoPesquisa']) || !empty($dadosRefill['carroLiderPesquisa']) || !empty($dadosRefill['odometroPartirPesquisa']) || !empty($dadosRefill['odometroAtePesquisa'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Ve�culo</label>
                                                <select name="veiculoPesquisa" class="form-control">
                                                    <option value="">Ve�culo</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa']) {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                                    } else {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo[!]' => null]);
                                                    }

                                                    foreach ($veiculo as $dados => $value) {
                                                        if ($dadosRefill['veiculoPesquisa'] == $value['cod_veiculo'])
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Carro</label>
                                                <select name="carroAvariadoPesquisa" class="form-control">
                                                    <option value="">Carro</option>
                                                    <?php
                                                    if ($dadosRefill['grupoSistemaPesquisa'] || $dadosRefill['veiculoPesquisa']) {
                                                        if ($dadosRefill['grupoSistemaPesquisa']) {
                                                            if ($dadosRefill['veiculoPesquisa']) {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['AND' => ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa'], 'cod_veiculo' => $dadosRefill['veiculoPesquisa']]]);
                                                            } else {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                                            }
                                                        } else {
                                                            $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_veiculo' => $dadosRefill['veiculoPesquisa']]);
                                                        }
                                                    } else {
                                                        $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro']);
                                                    }

                                                    foreach ($carro as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['carroAvariadoPesquisa'] == $value['cod_carro'])
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Od�metro a partir</label>
                                                <input name='odometroPartirPesquisa' class='form-control number'
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroPartirPesquisa']; ?>"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Od�metro at�</label>
                                                <input name="odometroAtePesquisa" class="form-control number"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroAtePesquisa']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingEncaminhamento">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#encaminhamentoPesquisa" aria-expanded="false"
                                       aria-controls="collapseEncaminhamento">
                                        <button class="btn btn-default"><label>Encaminhamento</label></button>
                                    </a>
                                </div>

                                <div id="encaminhamentoPesquisa" class="panel-collapse
                                    <?php
                                    if (!empty($dadosRefill['localPesquisa']) || !empty($dadosRefill['equipePesquisa'])) {
                                        echo(' in');
                                    } else {
                                        echo(' out');
                                    }
                                    ?>
                                    collapse" role="tabpanel" aria-labelledby="headingEncaminhamento">
                                    
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Local</label>
                                                <select name="localPesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"]);
                                                    $unidade = array_combine(range(1,count($unidade)), $unidade);

                                                    foreach($unidade as $dados =>$value){
                                                        echo('<option value="'.$value['cod_unidade'].'" ');

                                                        if(!empty($dadosRefill['localPesquisa']) && $dadosRefill['localPesquisa'] == $value['cod_unidade']){
                                                            echo('selected');
                                                        }

                                                        echo('>'.$value['nome_unidade'].'</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Equipe</label>
                                                <select name="equipePesquisa" class="form-control">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if($dadosRefill['localPesquisa']){
                                                        $equipe = $this->medoo->select("un_equipe",["[><]equipe" => "cod_equipe"], ['sigla',"nome_equipe"], ['cod_unidade' => $dadosRefill['localPesquisa']]);
                                                    }else{
                                                        $equipe = $this->medoo->select("equipe", ['sigla',"nome_equipe"]);
                                                    }
                                                    
                                                    foreach($equipe as $dados => $value){
                                                        echo ('<option value="'.$value['sigla'].'" ');
                                                        if(!empty($dadosRefill['equipePesquisa']) && $dadosRefill['equipePesquisa'] == $value['sigla']){
                                                            echo ('selected');
                                                        }
                                                        echo ('>'.$value['nome_equipe'].'</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
                            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro</button>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 1em"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Resultado da Pesquisa
                                    <strong>Total Encontrado: <?php echo (!empty($returnPesquisa))? count($returnPesquisa): 0; ?></strong>
                                </div>

                                <div class="panel-body">
                                    <table id="resultadoPesquisaSsp" class="table table-striped table-bordered no-footer">
                                        <thead id="headIndicadorSsp">
                                            <tr role="row">
                                                <th>N� SSP</th>
                                                <th>Local</th>
                                                <th>Data Programada</th>
                                                <th>Tipo de Manuten��o</th>
                                                <th>Servi�o</th>
                                                <th>Status</th>
                                                <th>A��o</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if(!empty($returnPesquisa) ) {
                                                foreach ($returnPesquisa as $dados => $value) {
                                                    echo('<tr>');
                                                    echo('<td>' . $value['cod_ssp'].'</td>');
                                                    echo("<td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>");
                                                    echo('<td>' . $this->parse_timestamp($value['data_programada']) . '</td>');
                                                    echo('<td>' . $value['nome_tipo_intervencao'] . '</td>');
                                                    echo('<td>' . $value['nome_servico'] . '</td>');
                                                    echo('<td>' . $value['nome_status'] . '</td>');
                                                    echo("<td>{$btnAcoes}</td>");
                                                    echo('</tr>');
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>