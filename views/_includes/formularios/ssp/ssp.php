<?php
if (!empty($Ssp) && $Ssp['cod_status'] != 19 && $Ssp['cod_status'] != 30 && !in_array($this->dadosUsuario['cod_usuario'],GESIV))
{
    $_SESSION['bloqueio'] = true;
}else{
    $_SESSION['bloqueio'] = false;
}
if(!empty($Ssp['matricula']))
{
    $result = $this->medoo->query("
        SELECT cpf_cnpj, nome_funcionario, fe.cod_tipo_funcionario
        FROM funcionario 
        JOIN funcionario_empresa fe USING(cod_funcionario_empresa)
        WHERE fe.matricula = '{$Ssp['matricula']}'
    ")->fetchAll(PDO::FETCH_ASSOC)[0];

}
?>

<div class="page-header">
    <h1>SSP - Solicita��o de Servi�o Programado</h1>
</div>

<div class="row">
    <form
        <?php
        if(!empty($Ssp))
            echo(' action="'.HOME_URI.'/cadastroGeral/alterarSsp"');
        else
            echo(' action="'.HOME_URI.'/cadastroGeral/gerarSsp"');
        ?>
        class="form-group" method="post" id="formSsp" >
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>SSP</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Dados Gerais</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-4">
                                            <label>Origem</label>
                                            <select name="origemProgramacao" required
                                                    class="form-control" <?php if (!empty($Ssp['tipo_orissp'])) echo "disabled" ?> >
                                                <?php
                                                $equipe_mr = ['5.1', '5.2', '7.2'];
                                                if (in_array($_SESSION['dadosUsuario']['nivel'], $equipe_mr)) {
                                                    echo("<option value='1' ");
                                                    if (!empty($Ssp) && $Ssp['tipo_orissp'] == 1) {
                                                        echo("selected");
                                                    }
                                                    echo(">Preventiva</option>");

                                                    echo("<option value='5' ");
                                                    if (!empty($Ssp) && $Ssp['tipo_orissp'] == 5) {
                                                        echo("selected");
                                                    }
                                                    echo(">Preditiva</option>");

                                                    echo("<option value='6' ");
                                                    if (!empty($Ssp) && $Ssp['tipo_orissp'] == 6) {
                                                        echo("selected");
                                                    }
                                                    echo(">Corretiva MR</option>");

                                                    echo("<option value='7' ");
                                                    if (!empty($Ssp) && $Ssp['tipo_orissp'] == 7) {
                                                        echo("selected");
                                                    }
                                                    echo(">Melhoria MR</option>");
                                                }else{
                                                    if (!empty($Ssp)) {
                                                        $selectop = array(
                                                            "1" => "Preventiva",
                                                            "2" => "Corretiva",
                                                            "3" => "Programa��o",
                                                            "4" => "Servi�o",
                                                            "5" => "Preditiva",
                                                            "6" => "Corretiva MR" //Material Rodante
                                                        );
                                                    } else {
                                                        $selectop = array(
                                                            "3" => "Programa��o",
                                                            "4" => "Servi�o"
                                                        );
                                                    }

                                                    echo("<option disabled selected>Selecione a Origem</option>");
                                                    foreach ($selectop as $dados => $value) {
                                                        echo("<option value='" . $dados . "' ");

                                                        if (!empty($Ssp) && $Ssp['tipo_orissp'] == $dados) {
                                                            echo("selected");
                                                        }
                                                        echo(">" . $value . "</option>");
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <?php
                                            if(!empty($Ssp['tipo_orissp']))
                                                echo "<input type='hidden' value='{$Ssp['tipo_orissp']}' name='origemProgramacao' />"
                                            ?>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Tipo Funcion�rio</label>
                                            <select name="tipo_funcionario" class="form-control">
                                                <?php
                                                    $tipo_funcionario = $this->medoo->select("tipo_funcionario" ,["cod_tipo_funcionario" , "descricao"]);

                                                    foreach ($tipo_funcionario as $daoos => $values){
                                                        if(!empty($result) && $result['cod_tipo_funcionario']==$values["cod_tipo_funcionario"])
                                                        {
                                                            echo "<option value='{$values["cod_tipo_funcionario"]}' selected>{$values["descricao"]}</option>";
                                                        }else{
                                                            echo "<option value='{$values["cod_tipo_funcionario"]}'>{$values["descricao"]}</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-2 col-xs-4" id="origemServico" style="display: none">
                                            <label>N�mero da SSM</label>
                                            <input
                                                <?php
                                                if(!empty($Ssp) && !empty($Ssp['cod_ssm'])){
                                                    echo(" value='{$Ssp['cod_ssm']}' ");
                                                }
                                                if(!empty($Ssp['tipo_orissp'])) echo "readonly";
                                                ?>
                                                class="form-control" type="text" name="numeroSsmPendente">
                                        </div>
                                    </div>
                                    <div class="row" id="solicitanteProgramacaoFalha">
                                        <div class="col-md-2 col-xs-2" id="matriculaOrigemSsp"
                                             style="display: <?php echo (!in_array($_SESSION['dadosUsuario']['nivel'], $equipe_mr))? 'block':'none'; ?>">
                                            <label>Matr�cula</label>
                                            <input type="text" name="matriculaSolicitante"
                                                   value="<?php if (!empty($Ssp)) echo($Ssp['matricula']); ?>"
                                                   class="form-control">

                                        </div>
                                        <div class="col-md-7 col-xs-12">
                                            <label>Nome Completo</label>
                                            <input type="text" name="nomeSolicitante" autocomplete="off" list="nomeSolicitanteDataList"
                                                   value="<?php if(!empty($Ssp)) echo($Ssp['nome_origem']); ?>" class="form-control" required />

                                            <datalist id="nomeSolicitanteDataList">
                                                <?php
                                                $selectFuncionario = $this->medoo->select("funcionario", ["matricula", "nome_funcionario"], ["AND" => ["cod_tipo_funcionario" => 1, "ativo" =>"s"],"ORDER" => "nome_funcionario"]);

                                                foreach ($selectFuncionario as $dados => $value) {
                                                    echo("<option id='{$value['matricula']}' value='{$value['nome_funcionario']} - {$value['matricula']}'></option>");
                                                }
                                                ?>
                                            </datalist>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Contato</label>
                                            <input type="text" name="contatoSolicitante"
                                                   value="<?php if(!empty($Ssp)) echo($Ssp['contato_origem']); ?>" class="form-control telefone" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Tipo de Interven��o</label>
                                            <select class="form-control" name="tipoIntervencao" required>
                                                <option disabled selected>Selecione o tipo de Interven��o</option>
                                                <?php
                                                $tipoIntervencao = $this->medoo->select("tipo_intervencao", ["cod_tipo_intervencao","nome_tipo_intervencao"] , ["ORDER" => "nome_tipo_intervencao"]);

                                                foreach($tipoIntervencao as $dados=>$value){
                                                    echo('<option ');
                                                    if(!empty($Ssp) && $value['cod_tipo_intervencao'] == $Ssp['cod_tipo_intervencao']){
                                                        echo(' selected');
                                                    }
                                                    echo(' value="'.$value['cod_tipo_intervencao']. '">'. $value['nome_tipo_intervencao'] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>SSP</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <label>Data/Hora Programada</label>
                                            <input required name="dataHoraSsp"
                                                <?php
                                                if(!empty($Ssp)){
                                                    echo (' value="'.$this->parse_timestamp($Ssp['data_programada']).'" ');
                                                }
                                                ?>
                                                   type="datetime" class="form-control timestamp validaDataHora">
                                        </div>
                                        <div id="diasServico" class="col-md-2 col-xs-3">
                                            <label>Qtd. de Dias</label>
                                            <input name="diasServico" type="text"
                                                   value="<?php echo (!empty($Ssp))? $Ssp['dias_servico'] : '1';?>" class="form-control number">
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>N�</label>
                                            <input name="codigoSsp" readonly class="form-control" value="<?php if(!empty($Ssp)) echo $Ssp['cod_ssp']?>"/>

                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>S.A.</label>
                                            <input name="numeroSolicitacaoAcesso" type="text"
                                                <?php if(!empty($Ssp)) echo(' value="'.$Ssp['solicitacao_acesso'].'" '); ?> class="form-control">
                                        </div>
                                        <?php
                                        if(!empty($Ssp)) {
                                            echo   '<div class="col-md-3 col-xs-5" >
                                                        <label> Status</label>
                                                        <input type = "text" name="statusSsp" class="form-control" disabled value = "'.$Ssp['nome_status'].'" >
                                                    </div>';
                                        }?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Preenchimento</label> </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <label for="dataPreenchimentoSsp">Data/Hora de Abertura</label>
                                            <input class="form-control" name="dataPreenchimentoSsp" type="text"
                                                   value="<?php if(!empty($Ssp)) echo($this->parse_timestamp($Ssp['data_abertura_ssp'])); ?>" readonly />
                                        </div>
                                        <div class="col-md-5 col-xs-6">
                                            <label>Respons�vel</label>
                                            <input class="form-control"
                                                <?php
                                                echo (!empty($Ssp))?' value="'.$Ssp['usuario'].'" ':'';
                                                ?>
                                                   type="text" disabled/>
                                            <input
                                                <?php
                                                echo (!empty($Ssp))?' value="'.$Ssp['usuario'].'" ':' value="'. $this->dadosUsuario['usuario'].'" ';
                                                ?>
                                                   type="hidden" name="responsavelSsp"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php
                    if(!empty($Ssp) && $Ssp['nome_status'] == 'Cancelada') {
                        echo '<div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><label>Justificativa</label></div>

                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="descricaoStatusSsp">Descri��o do Status</label>
                                                            <textarea id="descricaoStatusSsp" class="form-control" disabled
                                                                      spellcheck="true">' . $Ssp['descricao'] . '</textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o / Local</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Linha</label>
                                            <select name="linhaSsp" required class="form-control">
                                                <option disabled selected>Selecione a linha</option>
                                                <?php
                                                $linha= $this->medoo->select('linha', ['cod_linha', 'nome_linha'], ['ORDER' => "cod_linha"]);

                                                foreach($linha as $dados => $value){
                                                    echo('<option value="'.$value['cod_linha']. '" ');
                                                    if(!empty ($Ssp) && $value['cod_linha'] == $Ssp['cod_linha']){
                                                        echo(' selected');
                                                    }
                                                    echo(' >'. $value['nome_linha'] . '</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Sigla / Trecho</label>
                                            <select name="trechoSsp" required class="form-control">
                                                <?php
                                                if(!empty($Ssp)){
                                                    $trecho = $this->medoo->select("trecho", ["cod_trecho", "nome_trecho", "descricao_trecho"], ["cod_linha" => (int) $Ssp['cod_linha']]);

                                                    foreach($trecho as $dados => $value){
                                                        echo('<option value="'.$value['cod_trecho']. '" ');

                                                        if(!empty($Ssp['cod_trecho']) && $value['cod_trecho'] == $Ssp['cod_trecho']){
                                                            echo('selected');
                                                        }
                                                        echo('>'. $value['nome_trecho']." - ".$value['descricao_trecho']. '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Ponto Not�vel</label>
                                            <select name="pontoNotavelSsp" class="form-control">
                                                <option value="">Ponto Not�vel</option>
                                                <?php
                                                if(!empty($Ssp)){
                                                    $pn = $this->medoo->select("ponto_notavel", ["nome_ponto", "cod_ponto_notavel"], [ "cod_trecho" => (int)$Ssp['cod_trecho'] ]);

                                                    foreach($pn as $dados => $value){
                                                        echo('<option value="'.$value['cod_ponto_notavel']. '"');

                                                        if(!empty($Ssp['cod_ponto_notavel']) && $value['cod_ponto_notavel'] == $Ssp['cod_ponto_notavel']){
                                                            echo(' selected');
                                                        }
                                                        echo('>'. $value['nome_ponto'] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Complemento Local</label>
                                            <input name="complementoLocalSsp" type="text"
                                                   value="<?php if(!empty($Ssp)) echo($Ssp['complemento_local']); ?>" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <label>Via</label>
                                            <select name="viaSsp" class="form-control">
                                                <option value="" selected>Selecione a via</option>
                                                <?php
                                                if(!empty($Ssp)){
                                                    $via = $this->medoo->select("via", ["nome_via","cod_via"], ["cod_linha" => (int)$Ssp['cod_linha']]);

                                                    foreach($via as $dados => $value){
                                                        if(!empty($Ssp['cod_via']) && $value['cod_via'] == $Ssp['cod_via']) {
                                                            echo('<option value="' . $value['cod_via'] . '" selected>' . $value['nome_via'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_via'] . '">' . $value['nome_via'] . '</option>');
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Posi��o</label>
                                            <input name="posicaoSsp" type="text"
                                                   value="<?php if(!empty ($Ssp)) echo($Ssp['posicao']);?>" class="form-control" />

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Km Inicial</label>
                                            <input name="kmInicialSsp" type="text"
                                                   value="<?php if(!empty($Ssp)) echo($Ssp['km_inicial']); ?>" class="form-control" />

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Km Final</label>
                                            <input name="kmFinalSsp" type="text"
                                                   value="<?php if(!empty($Ssp)) echo($Ssp['km_final']);?>" class="form-control" />

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input disabled name="numeroGrupoSistemaSsp"
                                                   value="<?php if(!empty ($Ssp)) echo($Ssp['cod_grupo']); ?>" type="text" class="form-control" />

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Grupo de Sistema</label>
                                            <?php
                                            if(!empty($ssp['cod_grupo']))
                                            {
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ORDER" => "nome_grupo"]);
                                            }else
                                            {
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S', "ORDER" => "nome_grupo"]);
                                            }
                                            $this->form->getSelectGrupo($Ssp['cod_grupo'], $grupo, 'grupoSistemaSsp', false);
                                            ?>
                                        </div>
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input name="numeroSistemaSsp" disabled
                                                   value="<?php if(!empty ($Ssp)) echo($Ssp['cod_sistema']); ?>" type="text" class="form-control" />

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sistema</label>
                                            <select id="sistema" name="sistemaSsp" class="form-control" required>
                                                <option disabled selected>Selecione o Sistema</option>
                                                <?php
                                                if(!empty($Ssp)) {
                                                    $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => "{$Ssp['cod_grupo']}"]);

                                                    foreach ($sistema as $dados => $value) {
                                                        echo('<option value="' . $value["cod_sistema"] . '" ');

                                                        if ($Ssp['nome_sistema'] == $value["nome_sistema"]) {
                                                            echo(' selected');
                                                        }
                                                        echo('>' . $value["nome_sistema"] . '</option>');
                                                    }
                                                }

                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input type="text" class="form-control" name="codSubSistema"
                                                   value="<?php if(!empty ($Ssp)) echo($Ssp['cod_subsistema']); ?>" disabled>

                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sub-Sistema</label>
                                            <select class="form-control" name="subSistemaSsp" required>
                                                <option disabled selected> Sub-sistema</option>
                                                <?php
                                                if (!empty($Ssp)) {
                                                    $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => "{$Ssp['cod_sistema']}"]);

                                                    foreach ($subsistema as $dados => $value) {
                                                        if ($Ssp['cod_subsistema'] == $value["cod_subsistema"]) {
                                                            echo('<option value="' . $value["cod_subsistema"] . '" selected>' . $value["nome_subsistema"] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>');
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php
                                    if ($Ssp['cod_grupo'] == '22' || $Ssp['cod_grupo'] == '23' || $Ssp['cod_grupo'] == '26') {
                                        $materialRodante = true;
                                    }else{
                                        $materialRodante = false;
                                    }
                                    ?>
                                    <div class="row" id="divMaterialRodante"
                                         style="display:<?php echo ($materialRodante) ? "display" : "none" ?>">
                                        <div class="col-md-3">
                                            <label>Ve�culo</label>
                                            <select class="form-control"
                                                    name="veiculoMrSsp" <?php if ($materialRodante) echo "required"; ?>>
                                                <option value="" selected disabled>Ve�culo</option>
                                                <?php
                                                if (!empty($Ssp['cod_veiculo'])){
                                                    $selectVeiculo = $this->medoo->select("veiculo", "*", ['cod_grupo' => (int)$Ssp['cod_grupo']]);


                                                    foreach ($selectVeiculo as $dados => $value) {
                                                        if ($Ssp['cod_veiculo'] == $value['cod_veiculo']) {
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        } else {
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Carro</label>
                                            <select class="form-control"
                                                    name="carroMrSsp">
                                                <option value="" selected disabled>Carro</option>
                                                <?php
                                                if (!empty($Ssp['cod_carro'])){
                                                    $selectCarro = $this->medoo->select("carro", "*", ['cod_grupo' => (int)$Ssp['cod_grupo']]);


                                                    foreach ($selectCarro as $dados => $value) {
                                                        if ($Ssp['cod_carro'] == $value['cod_carro']) {
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
                                                        } else {
                                                            echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Od�metro</label>
                                            <input <?php
                                                   if(!empty($Ssp['odometro']))  echo "value='{$Ssp['odometro']}'";
                                                   ?>class="form-control number" type="text" name="odometroMrSsp">
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Servi�o</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-1 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input name="servicoSsp" readonly class="form-control"
                                                   value="<?php if(!empty ($Ssp)) echo($Ssp['cod_servico']); ?>" type="text" />

                                        </div>
                                        <div class="col-md-12 col-xs-10">
                                            <label>Nome</label>
                                            <input name="servicoSspInput" list="servicoSspDataList" placeholder="Selecione um Servi�o" autocomplete="off"
                                                   value="<?php if(!empty($Ssp['nome_servico'])) echo $Ssp['nome_servico'] ?>" required class="form-control" />

                                            <datalist id="servicoSspDataList">
                                                <option value=""></option>
                                                <?php
                                                $servicos = $this->medoo->select("servico", ["nome_servico", "cod_servico"],["ORDER" => "nome_servico"]);

                                                foreach ($servicos as $dados => $value){
                                                    echo("<option value='{$value['cod_servico']}' >{$value['nome_servico']} - {$value['cod_servico']}</option>");
                                                }
                                                ?>
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Complemento Servi�o</label>
                                            <textarea class="form-control" name="complementoServicoSsp"><?php
                                                if(!empty ($Ssp))
                                                    echo ($Ssp['complemento']);
                                                ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Encaminhamento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2" style="display: none;">
                                            <label>N�</label>
                                            <input name="numeroLocalSsp" disabled
                                                   value="<?php if(!empty ($Ssp)) echo($Ssp['cod_unidade']); ?>" type="text" class="form-control">

                                        </div>
                                        <div class="col-md-12 col-xs-10">
                                            <label>Local</label>
                                            <select name="localSsp" class="form-control" required>
                                                <option disabled selected>Selecione o Local</option>
                                                <?php
                                                $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"]);

                                                foreach($unidade as $dados =>$value){
                                                    echo('<option value="'.$value['cod_unidade'].'" ');
                                                    if(!empty($Ssp) && $Ssp['cod_unidade'] == $value['cod_unidade']){
                                                        echo('selected');
                                                    }
                                                    echo('>'.$value['nome_unidade'].'</option>');
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label>Sigla</label>
                                            <input name="siglaEquipeSsp" disabled
                                                   value="<?php if(!empty($Ssp)) echo($Ssp['sigla_equipe']); ?>" type="text" class="form-control">

                                        </div>
                                        <div class="col-md-10 col-xs-10">
                                            <label>Equipe</label>
                                            <select name="equipeSsp" class="form-control" required>
                                                <?php
                                                if(!empty($Ssp)){
                                                    $equipe = $this->medoo->select("un_equipe",["[><]equipe"=> "cod_equipe"], ["sigla", "nome_equipe"],
                                                        [
                                                            "ORDER" => "nome_equipe",
                                                            "cod_unidade" => $Ssp['cod_unidade']
                                                        ]
                                                    );

                                                    echo('<option disabled selected>Selecione a Equipe</option>');

                                                    foreach ($equipe as $dados => $value) {
                                                        if ($Ssp['sigla_equipe'] == $value['sigla'])
                                                            echo('<option value="' . $value['sigla'] . '" selected>' . $value['nome_equipe'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <div class="col-md-offset-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">A��es</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group  btn-group-justified " role="group">

                                                <?php echo $btnsAcao; ?>

                                            </div>
                                            <div class="modal fade modal-historico" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                                                                <i aria-hidden="true" class="fa fa-close"></i></button>
                                                            <h4 class="modal-title">Hist�rico da SSP</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <?php
                                                            if(!empty($Ssp)) {
                                                                $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssp s 
                                                                  JOIN usuario u on s.usuario = u.cod_usuario
                                                                  JOIN status USING(cod_status)
                                                                  WHERE cod_ssp = {$Ssp['cod_ssp']} ORDER BY data_status";

                                                                if ($sql != "")
                                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                            }
                                                            ?>

                                                            <div class = "row">
                                                                <div class="col-md-12">
                                                                    <table class="tableRel table-bordered dataTable">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Status</th>
                                                                            <th>Responsavel</th>
                                                                            <th>Data da altera��o</th>
                                                                            <th>Complemento</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php
                                                                        if(!empty($result)){
                                                                            foreach ($result as $dados){
                                                                                echo("<tr>");
                                                                                echo("<td>{$dados['nome_status']}</td>");
                                                                                echo("<td>{$dados['usuario']}</td>");
                                                                                echo("<td>{$this->parse_timestamp($dados['data_status'])}</td>");
                                                                                echo("<td>{$dados['descricao']}</td>");
                                                                                echo("</tr>");
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
