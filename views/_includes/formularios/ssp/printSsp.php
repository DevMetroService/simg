<?php
if(!empty ($_SESSION['refillSsp'])){
    $Ssp = $this->medoo->select('v_ssp','*',[
        "cod_ssp"   => (int) $_SESSION['refillSsp']['codigoSsp']
    ]);

    $Ssp = $Ssp[0];

    unset($_SESSION['refillSsp']);
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>SSP - Solicita��o de Servi�o Preventivo</h3>
    </div>

    <hr/>

    <div class="title-table">&raquo; Dados Gerais</div>
    <table>
        <tr>
            <th>SSP</th>
            <th>Respons�vel Preenchimento</th>
            <th>Data/Hora Abertura</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssp['cod_ssp']; ?></td>
            <td class="center"><?php echo $Ssp['usuario']; ?></td>
            <td class="center"><?php echo $this->parse_timestamp($Ssp['data_abertura_ssp']); ?></td>
        </tr>
        <tr>
            <th>Data/Hora Programada</th>
            <th>Qtd. de dias</th>
            <th>S.A.</th>
        </tr>
        <tr>
            <td class="center"><?php echo $this->parse_timestamp($Ssp['data_programada']); ?></td>
            <td class="center"><?php echo $Ssp['dias_servico']; ?></td>
            <td class="center"><?php echo $Ssp['solicitacao_acesso']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Origem</div>
    <table>
        <tr>
            <th>Origem</th>
            <?php
            if($Ssp['tipo_orissp'] == 2){
                echo '<th>SSM</th>';
            }
            ?>
        </tr>
        <tr>
            <td class="center">
                <?php
                switch($Ssp['tipo_orissp']){
                    case "1":
                        echo 'Preventiva';
                        break;
                    case "2":
                        echo 'Corretiva';
                        break;
                    case "3":
                        echo 'Programa��o';
                        break;
                    case "4":
                        echo 'Servi�o';
                        break;
                    case "5":
                        echo 'Preditiva';
                        break;
                    case "6":
                        echo 'Corretiva MR';
                        break;
                    case "7":
                        echo 'Melhoria MR';
                }
                ?>
            </td>
            <?php
            if($Ssp['tipo_orissp'] == "2"){
                echo '<td class="center">'.$Ssp['cod_ssm'].'</td>';
            }
            ?>
        </tr>
        <tr>
            <th>Matr�cula</th>
            <th>Nome Completo</th>
            <th>Cpf</th>
            <th>Contato</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssp['matricula']; ?></td>
            <td><?php echo $Ssp['nome_origem']; ?></td>
            <td class="center"><?php echo $Ssp['cpf_origem']; ?></td>
            <td class="center"><?php echo $Ssp['contato_origem']; ?></td>
        </tr>
        <tr>
            <th colspan="4">Tipo Interven��o</th>
        </tr>
        <tr>
            <td colspan="4"><?php echo $Ssp['nome_tipo_intervencao']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o / Local</div>
    <table>
        <tr>
            <th>Linha</th>
            <th colspan="2">Sigla / Trecho</th>
            <th>Ponto Not�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssp['nome_linha']; ?></td>
            <td colspan="2" class="center"><?php echo $Ssp['nome_trecho'] . ' - ' . $Ssp['descricao_trecho']; ?></td>
            <td class="center"><?php echo $Ssp['nome_ponto']; ?></td>
        </tr>
        <tr>
            <th colspan="4">Complemento</th>
        </tr>
        <tr>
            <td colspan="4"><?php echo $Ssp['complemento_local']; ?></td>
        </tr>
        <tr>
            <th>Via</th>
            <th>Km Inicial</th>
            <th>Km Final</th>
            <th>Posi��o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssp['nome_via']; ?></td>
            <td class="center"><?php echo $Ssp['km_inicial']; ?></td>
            <td class="center"><?php echo $Ssp['km_final']; ?></td>
            <td class="center"><?php echo $Ssp['posicao']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Grupo Sistema</th>
            <th colspan="2">Sistema</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $Ssp['nome_grupo']; ?></td>
            <td colspan="2" class="center"><?php echo $Ssp['nome_sistema']; ?></td>
        </tr>
        <tr>
            <th colspan="4">SubSistema</th>
        </tr>
        <tr>
            <td colspan="4" class="center"><?php echo $Ssp['nome_subsistema']; ?></td>
        </tr>
    </table>

    <?php if (!empty($Ssp['nome_veiculo'])){ ?>
        <div class="title-table">&raquo; Material Rodante</div>
        <table>
            <tr>
                <th colspan="2">Ve�culo</th>
                <th>Carro Avariado</th>
                <th>Od�metro</th>
            </tr>
            <tr>
                <td colspan="2"><?php echo($Ssp['nome_veiculo']) ?></td>
                <td><?php echo($Ssp['nome_carro']) ?></td>
                <td><?php echo($Ssp['odometro']) ?></td>
            </tr>
        </table>
    <?php } ?>

    <div class="title-table">&raquo; Servi�o</div>
    <table>
        <tr>
            <th>Nome</th>
        </tr>
        <tr>
            <td><?php echo $Ssp['nome_servico']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Complemento</th>
        </tr>
        <tr>
            <td colspan="2"><?php echo $Ssp['complemento']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Encaminhamento</div>
    <table>
        <tr>
            <th>Local</th>
            <th>Equipe</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssp['nome_unidade']; ?></td>
            <td class="center"><?php echo $Ssp['nome_equipe']; ?></td>
        </tr>
    </table>
</div>