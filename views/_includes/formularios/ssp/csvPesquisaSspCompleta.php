<?php

$conteudo = 'N. SSP;';
$conteudo .= 'Origem;';
$conteudo .= 'N. SSM;';
$conteudo .= 'Data Programada;';
$conteudo .= 'Qtd de Dias;';
$conteudo .= 'SA;';
$conteudo .= 'Responsavel pelo Preenchimento;';
$conteudo .= 'Data Abertura;';
$conteudo .= 'Matricula;';
$conteudo .= 'Nome;';
$conteudo .= 'CPF;';
$conteudo .= 'Contato;';
$conteudo .= 'Intervenção;';
$conteudo .= 'Linha;';
$conteudo .= 'Trecho;';
$conteudo .= 'Ponto Notável;';
$conteudo .= 'Complemento Local;';
$conteudo .= 'Via;';
$conteudo .= 'Km Inicial;';
$conteudo .= 'Km Final;';
$conteudo .= 'Posição;';
$conteudo .= 'Grupo de Sistemas;';
$conteudo .= 'Sistema;';
$conteudo .= 'Sub-Sistema;';
$conteudo .= 'Veiculo;';
$conteudo .= 'Carro;';
$conteudo .= 'Odômetro;';
$conteudo .= 'Serviço;';
$conteudo .= 'Complemento do Serviço;';
$conteudo .= 'Unidade;';
$conteudo .= 'Equipe;';
$conteudo .= 'Status;';
$conteudo .= 'Data Referente ao Status;';
$conteudo .= "Descrição Status;\n";

$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)){
    foreach ($returnPesquisa as $dados=>$value) {
        switch($value['tipo_orissp']){
            case "1":
                $origem ='Preventiva';
                break;
            case "2":
                $origem='Corretiva';
                break;
            case "3":
                $origem='Programação';
                break;
            case "4":
                $origem='Serviço';
                break;
            case "5":
                $origem='Preditiva';
                break;
            case "6":
                $origem='Corretiva MR';
                break;
            case "7":
                $origem='Melhoria MR';
        }

        $txt = $value['cod_ssp'] . ":::";
        $txt .= utf8_decode($origem) . ":::";
        $txt .= $value['cod_ssm'] . ":::";
        $txt .= $this->parse_timestamp($value['data_programada']) . ":::";
        $txt .= $value['dias_servico'] . ":::";
        $txt .= $value['solicitacao_acesso'] . ":::";
        $txt .= $value['nome_usuario'] . ":::";
        $txt .= $this->parse_timestamp($value['data_abertura_ssp']) . ":::";
        $txt .= $value['matricula'] . ":::";
        $txt .= $value['nome_origem'] . ":::";
        $txt .= $value['cpf_origem'] . ":::";
        $txt .= $value['contato_origem'] . ":::";
        $txt .= $value['nome_tipo_intervencao'] . ":::";
        $txt .= $value['nome_linha'] . ":::";
        $txt .= $value['nome_trecho'] . " - " . $value['descricao_trecho'] . ":::";
        $txt .= $value['nome_ponto'] . ":::";
        $txt .= addslashes($value['complemento_local']) . ":::";
        $txt .= $value['nome_via'] . ":::";
        $txt .= $value['km_inicial'] . ":::";
        $txt .= $value['km_final'] . ":::";
        $txt .= $value['posicao'] . ":::";
        $txt .= $value['nome_grupo'] . ":::";
        $txt .= $value['nome_sistema'] . ":::";
        $txt .= $value['nome_subsistema'] . ":::";
        $txt .= $value['nome_veiculo'] . ":::";
        $txt .= $value['nome_carro'] . ":::";
        $txt .= $value['odometro'] . ":::";
        $txt .= $value['nome_servico'] . ":::";
        $txt .= addslashes($value['complemento']) . ":::";
        $txt .= $value['nome_unidade'] . ":::";
        $txt .= $value['nome_equipe'] . ":::";
        $txt .= $value['nome_status'] . ":::";
        $txt .= $this->parse_timestamp($value['data_status']) . ":::";
        $txt .= $value['descricao'] . ":::";

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("SspCompleta.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=SspCompleta.csv");
readfile(TEMP_PATH.'SspCompleta.csv');

unlink(TEMP_PATH."SspCompleta.csv");