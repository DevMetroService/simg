<div class="page-header">
    <h1>Gerador PMP - Subesta��o</h1>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><label>PMP - Subesta��o</label></div>

    <div class="panel-body">
        <div class="row">
            <?php echo moduloIdentificacaoGeral($refill, $sistemaSb, $subsistemaSb, $servicoPmpSb, $periodicidadeSb); ?>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Local (ais)</label></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Selecione</label>
                                <select name="local[]" class="form-control multiselect" multiple="multiple">
<!--                                    <optgroup label="Linha Oeste"></optgroup>-->
<!--                                    --><?php
//                                    $pmp_estacao = $this->medoo->select('local', '*', [ "AND"=>['cod_linha' => 1, "grupo" => 'S']]);
//
//                                    foreach ($pmp_estacao as $dados => $value) {
//                                        echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
//                                    }
//                                    ?>
                                    <optgroup label="Linha Cariri"></optgroup>
                                    <?php
                                    $pmp_estacao = $this->medoo->select('local', '*', [ "AND"=>['cod_linha' => 2, "grupo" => ['S', ""]]]);

                                    foreach ($pmp_estacao as $dados => $value) {
                                        echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                    }
                                    ?>
                                    <optgroup label="Linha Sul"></optgroup>
                                    <?php
                                    $pmp_estacao = $this->medoo->select('local', '*', [ "AND"=>['cod_linha' => 5, "grupo" => ['S', ""]]]);

                                    foreach ($pmp_estacao as $dados => $value) {
                                        echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                    }
                                    ?>
                                    <optgroup label="Linha Sobral"></optgroup>
                                    <?php
                                    $pmp_estacao = $this->medoo->select('local', '*', [ "AND"=>['cod_linha' => [7] , "grupo" => ['S', ""]]]);

                                    foreach ($pmp_estacao as $dados => $value) {
                                        echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                    }
                                    ?>
<!--                                    <optgroup label="Linha Grendene - Sobral Norte"></optgroup>-->
<!--                                    --><?php
//                                    $pmp_estacao = $this->medoo->select('local', '*', [ "AND"=>['cod_linha' => 4, "grupo" => ['S', ""]]]);
//
//                                    foreach ($pmp_estacao as $dados => $value) {
//                                        echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
//                                    }
//                                    ?>
                                </select>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
                <?php echo moduloRecursosHH($refill, 6); ?>
        </div>

        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <button type="button" aria-label="right align" name="btnPlus" class="btn btn-default btn-lg">
                        <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Linhas Geradas</label></div>

                    <div class="panel-body over-x">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="tabelaPmpSb" class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Sistema</th>
                                        <th>SubSistema</th>
                                        <th>Servi�o</th>
                                        <th>Periodicidade</th>
                                        <th>Procedimento</th>
                                        <th>Qzn In�cio</th>
                                        <th>Local</th>
                                        <th>Turno</th>
                                        <th>M�o de obra</th>
                                        <th>Horas �teis</th>
                                        <th>HH</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Pmp - Armazenados</label></div>

                    <div class="panel-body over-x">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="tabelaPmpSbExec" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Cod</th>
                                        <th>Sistema</th>
                                        <th>SubSistema</th>
                                        <th>Servi�o</th>
                                        <th>Periodicidade</th>
                                        <th>Procedimento</th>
                                        <th>Qzn In�cio</th>
                                        <th>Local</th>
                                        <th>Turno</th>
                                        <th>M�o de obra</th>
                                        <th>Horas �teis</th>
                                        <th>HH</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ctdDb = 1;
                                    if (!empty($pmpSubestacao)) {
                                        foreach ($pmpSubestacao as $dados) {
                                            if ($dados['turno'] == 'D') {
                                                $turno = 'Diurno';
                                            } else {
                                                $turno = 'Noturno';
                                            }

                                            echo '<tr>';
                                            echo("<td>{$dados['cod_pmp']}</td>");//<input type='hidden' name='codPmpSb{$ctdDb}' value='{$dados['cod_pmp']}'></td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_sistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_subsistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_servico_pmp']}</td>");
                                            echo("<td>{$periodicidade[$dados['cod_tipo_periodicidade']]}</td>");
                                            echo("<td>{$procedimento[$dados['cod_procedimento']]}</td>");
                                            echo("<td>{$dados['quinzena']}</td>");
                                            echo("<td>{$local[$dados['cod_local']]}</td>");
                                            echo("<td>{$turno}</td>");
                                            echo("<td>{$dados['mao_obra']}</td>");
                                            echo("<td>{$dados['horas_uteis']}</td>");
                                            echo("<td>{$dados['homem_hora']}</td>");
                                            echo("<td><input type='checkbox' value='{$dados['cod_pmp']}' name='excluirLinhaPmpBanco{$ctdDb}' id='excluirLinhaPmpBanco{$ctdDb}' class='form-control'><label for='excluirLinhaPmpBanco{$ctdDb}'>Excluir</label></td>");
                                            echo '</tr>';
                                            $ctdDb++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php if ($gerarPmpAnual){ ?>
    <div class="col-md-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Gerar PMP</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">
                            <div class="btn-group">
                                <button type="button" aria-label="right align"
                                        class="btn btn-success btn-lg gerarPmpAnual" title="Autorizar PMP Anual">
                                    <i class="fa fa-calendar-check-o fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-offset-8 col-md-2">
        <?php }else{ ?>
        <div class="col-md-offset-10 col-md-2">
            <?php } ?>
            <div class="panel panel-primary">
                <div class="panel-heading">A��es</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group  btn-group-justified " role="group">

                                <?php echo $btnsAcao; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo "<input value='{$ctdDb}' name='ctd' type='hidden'>"; ?>