<div class="panel-heading" style="text-align: center; font-size: large; font-weight: bold; padding-top: 25px">
    <img src="<?php echo HOME_URI?>/views/_images/metroservice_logo2.png" width="100px" height="50px" style="float: left">
    <label>PLANO DE MANUTEN��O PREVENTIVA</label>
    <br />
    <label>Subestacao</label>
</div>

<div class="panel-body">
    <div class="row">
        <table class="table table-bordered">
            <tr>
                <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">SERVI�OS DE MANUTEN��O PREVENTIVA</th>
                <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">PERIODICIDADE</th>
                <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">TURNO</th>
                <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">HH</th>
                <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">QZN</th>
                <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">PROCEDIMENTO</th>
            </tr>
<?php
$pmpSubestacao = $this->medoo->select("pmp_subestacao",
[
    '[><]pmp' => 'cod_pmp',
    '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
    '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
    '[><]servico_pmp' => 'cod_servico_pmp',
    '[><]tipo_periodicidade' => "cod_tipo_periodicidade",
    '[><]procedimento' => "cod_procedimento"
],[
    'nome_servico_pmp',
    'nome_periodicidade',
    'turno',
    'homem_hora',
    'quinzena',
    'nome_procedimento'
],['ORDER' => 'nome_servico_pmp', 'ativo[!]' => 'D']);

if(empty($pmpSubestacao)){
    echo('<tr><td colspan="7" style="font-size: 14px; text-align: center; padding: 10px;">N�o existe registros</td></tr>');
}else{
    foreach ($pmpSubestacao as $dados) {
        echo('<tr>');
        echo("<td style='font-size: 8px; padding: 5px;background-color: #76C04E'>{$dados['nome_servico_pmp']}</td>");
        echo("<td style='font-size: 8px; padding: 5px;'>{$dados['nome_periodicidade']}</td>");
        echo ($dados['turno']=='D')?"<td style='font-size: 8px; padding: 5px;'>Diurno</td>":"<td style='font-size: 8px; padding: 5px;'>Noturno</td>";
        echo("<td style='font-size: 8px; padding: 5px; text-align: center;'>{$dados['homem_hora']}</td>");
        echo("<td style='font-size: 8px; padding: 5px; text-align: center;'>{$dados['quinzena']}</td>");
        echo("<td style='font-size: 8px; padding: 5px;'>{$dados['nome_procedimento']}</td>");
        echo('<tr>');
    }
}
?>
        </table>
    </div>
</div>