<?php
$dados = $_SESSION['modal'];
unset($_SESSION['modal']);

$mes = $dados['mes'];
$linha = $dados['linha'];

//1;"Linha Oeste"
//2;"Linha Cariri"
//5;"Linha Sul"
//6;"Parangaba - Mucuripe"
//7;"Linha Sobral"

if($dados['ano'] == '2017' && $dados['mes'] == 1){ //Janeiro
    $arquivo = ABSPATH . '/includes/help/pmpJaneiroEd'.$linha.'.pdf';

    header("Content-Type: application/pdf");
    header("Content-Disposition: inline; filename=" . basename($arquivo));
    readfile($arquivo);
    exit;
}else{
    
    $sql = "SELECT * FROM (
               SELECT 
                 DISTINCT ON (cod_cronograma_pmp) cp.cod_cronograma_pmp, 
                 nome_linha, 
                 mes, 
                 cod_linha,
                 cod_servico_pmp, 
                 nome_servico_pmp,
                 nome_local, 
                 nome_periodicidade, 
                 c.quinzena, 
                 turno, 
                 nome_procedimento, 
                 nome_status, 
                 data_programada
               FROM cronograma c
                JOIN cronograma_pmp cp          USING (cod_cronograma)
                JOIN status_cronograma_pmp      USING (cod_status_cronograma_pmp)
                JOIN status                     USING (cod_status)    
                JOIN pmp                        USING (cod_pmp)
                JOIN pmp_edificacao             USING (cod_pmp)
                JOIN servico_pmp_periodicidade  USING (cod_servico_pmp_periodicidade)
                JOIN servico_pmp_sub_sistema    USING (cod_servico_pmp_sub_sistema)
                JOIN servico_pmp                USING (cod_servico_pmp)
                JOIN procedimento               USING (cod_procedimento)
                JOIN tipo_periodicidade         USING (cod_tipo_periodicidade)    
                JOIN local                      USING (cod_local)
                JOIN linha                      USING (cod_linha)    
                LEFT JOIN ssmp s                on s.cod_cronograma_pmp = cp.cod_cronograma_pmp
               where c.ano = ".$dados['ano']."
                ) alias";
    
    $posSql = " ORDER BY cod_linha, mes, cod_servico_pmp, quinzena, data_programada, nome_local";
    
    if ($dados['servico']) {
        $servico = " AND cod_servico_pmp = {$dados['servico']}";
    } else {
        $servico = "";
    }
    
    if ($mes == 0 && $linha == 0) {
        if ($servico) {
            $servico = " WHERE cod_servico_pmp = {$dados['servico']} ";
        }
        $selectServico = $this->medoo->query($sql . $servico . $posSql)->fetchAll(PDO::FETCH_ASSOC);
    } else if ($mes == 0) {
        $selectServico = $this->medoo->query($sql . " WHERE cod_linha = $linha" . $servico . $posSql)->fetchAll(PDO::FETCH_ASSOC);
    } else if ($linha == 0) {
        $selectServico = $this->medoo->query($sql . " WHERE mes = $mes" . $servico . $posSql)->fetchAll(PDO::FETCH_ASSOC);
    } else {
        $selectServico = $this->medoo->query($sql . " WHERE mes = $mes" . " AND cod_linha = $linha" . $servico . $posSql)->fetchAll(PDO::FETCH_ASSOC);
    }
    
    ?>
    
    <div class="panel-heading" style="text-align: center; font-size: large; font-weight: bold; padding-top: 25px">
        <img src="<?php echo HOME_URI ?>/views/_images/metrofor.png" width="100px" height="50px" style="float: left">
        <h3 style="text-align: center">CRONOGRAMA - EDIFICA��ES</h3>
    </div>
    
    <div>
        <?php
        $nomeMes = MainController::$monthComplete;
        $novaLinha;
        $novoMes;
        $codServico;
        $count = 0;
    
        foreach ($selectServico as $dados => $value) {
            if ($novaLinha != $value['nome_linha'] || $novoMes != $value['mes']) {
                if ($novaLinha) {
                    echo "</table>";
                    $codServico = "";
                }
    
                $novaLinha = $value['nome_linha'];
                $novoMes = $value['mes'];
    
                $titulo = $novaLinha . " - " . $nomeMes[$novoMes];
    
                echo '<h3 style="margin-top: 30px; margin-bottom: 1px; padding: 7px;text-align: center; background: #424242; color: #ffffff">' . $titulo . '</h3>
                        <table class="table table-bordered text-center" style="width: 100%;page-break-after:always;">
                            <tr>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Item</th>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Status</th>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Local</th>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Periodicidade</th>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Quinzena Prevista</th>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Turno</th>
                                <th style="text-align: center; padding: 7px; width: auto; background: #0b97c4; color: #ffffff">Tipo de Manuten��o</th>
                                
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">SSMP</th>
                                <th style="text-align: center; padding: 7px; width: auto; background: #0b97c4; color: #ffffff">Dura��o do Servi�o (Dias)</th>
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">In�cio</th>
                                <th style="text-align: center; padding: 7px; width: auto; background: #0b97c4; color: #ffffff">T�rmino Previsto</th>
                                
                                <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">OSMP</th>
                            </tr>';
            }
    
            $sql = "SELECT  cod_ssmp,  dias_servico,  data_programada, osmp.data_abertura, data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS data_prevista,  cod_osmp	
                        FROM cronograma_pmp
                        LEFT JOIN ssmp USING (cod_cronograma_pmp)
                        LEFT JOIN osmp USING (cod_ssmp)
                        
                        WHERE cod_cronograma_pmp = {$value['cod_cronograma_pmp']}
                        ORDER BY data_programada";
    
            $selectSsOs = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    
            $countSsOs = count($selectSsOs);
    
            if ($codServico != $value['cod_servico_pmp']) {
                echo("<tr><th colspan='12' style='background: #4CAF50; padding: 7px; color: #ffffff; text-align: center'>{$value['nome_servico_pmp']}</th></tr>");
                $codServico = $value['cod_servico_pmp'];
            }
            $count = $count + 1;
            $turno = $value['turno'] == 'D' ? "Diurno" : "Noturno";
    
            echo("<tr style='page-break-inside: avoid'>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$count}</td>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #ECEFF1; text-align: center'>{$value['nome_status']}</td>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0'>{$value['nome_local']}</td>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$value['nome_periodicidade']}</td>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$value['quinzena']}</td>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$turno}</td>");
            echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #ECEFF1; text-align: center'>{$value['nome_procedimento']}</td>");
    
    
            if ($countSsOs > 0) {
                $boolean = false;
                $cod_ssmp = 0;
    
                $cods = array_filter(array_column($selectSsOs, 'cod_ssmp'));
                $countOcorrencias = array_count_values($cods);
    
                foreach ($selectSsOs as $key => $val) {
                    if ($boolean) { // Ap�s o primeiro loop, criar nova linha
                        echo("<tr>");
                    }
                    $boolean = true;
    
                    if ($cod_ssmp != $val['cod_ssmp']) {
                        $countOs = $countOcorrencias[$val['cod_ssmp']];
    
                        echo("<td rowspan='{$countOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$val['cod_ssmp']}</td>");
                        echo("<td rowspan='{$countOs}' style='padding: 7px; background: #ECEFF1; text-align: center'>{$val['dias_servico']}</td>");
                        echo("<td rowspan='{$countOs}' style='padding: 7px; background: #ECEFF1; text-align: center'>" . $this->getOnlyData($val['data_programada']) . "</td>");
                        echo("<td rowspan='{$countOs}' style='padding: 7px; background: #ECEFF1; text-align: center'>" . $this->getOnlyData($val['data_abertura']) . "</td>");
    
                        $cod_ssmp = $val['cod_ssmp'];
                    }
    
                    echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'>{$val['cod_osmp']}</td>");
                    echo("</tr>");
                }
            } else {
                echo("<td style='padding: 7px; background: #E0E0E0; text-align: center'></td>");
                echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
                echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
                echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
                echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
                echo("</tr>");
            }
        }
        ?>
    </div>

<?php
}