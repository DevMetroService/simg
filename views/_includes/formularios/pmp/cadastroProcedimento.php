<div class="page-header">
    <h1>Cadastro de Procedimento em Servi�os <?php echo $nomeSistemaFormulario; ?></h1>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><label>Cadastro</label></div>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Filtro</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Sistema</label>
                                <select name="sistema" class="form-control" required>
                                    <option value="" disabled selected>Selecione o Sistema</option>
                                    <?php
                                    foreach ($sistemaProc as $dados => $value) {
                                        if(!empty($refill['sistema']) && $refill['sistema'] == $value["cod_sistema"])
                                            echo '<option value="' . $value["cod_sistema"] . '" selected>' . $value["nome_sistema"] . '</option>';
                                        else
                                            echo '<option value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>SubSistema</label>
                                <select name="subSistema" class="form-control" required>
                                    <option value="" disabled selected>Selecione o Subsistema</option>
                                    <?php
                                    if(!empty($refill['sistema'])) {
                                        foreach ($subSistemaProc as $dados => $value) {
                                            if (!empty($refill['subSistema']) && $refill['subSistema'] == $value["cod_subsistema"])
                                                echo '<option value="' . $value["cod_subsistema"] . '" selected>' . $value["nome_subsistema"] . '</option>';
                                            else
                                                echo '<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><label>Dados</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Servi�o</label>
                                <select name="servicoPmp" class="form-control" required>
                                    <option value="" disabled selected>Selecione o servi�o</option>
                                    <?php
                                    if (!empty($refill['subSistema'])) {
                                        foreach ($servicoPmpProc as $dados => $value) {
                                            echo("<option value='{$value['cod_servico_pmp']}'>{$value['nome_servico_pmp']}</option>");
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Periodicidade</label>
                                <select name="periodicidadePmp" class="form-control" required>
                                    <option value="" disabled selected>Selecione a periodicidade</option>
                                    <?php
                                    foreach ($periodicidade as $dados => $value) {
                                        echo("<option value='{$value['cod_tipo_periodicidade']}'>{$value['nome_periodicidade']}</option>");
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Procedimento</label>
                                <select name="procedimentoPmp" class="form-control" required>
                                    <option value="" disabled selected>Selecione o procedimento</option>
                                    <?php
                                    foreach ($procedimento as $dados => $value) {
                                        echo("<option value='{$value['cod_procedimento']}'>{$value['nome_procedimento']}</option>");
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <button type="button" aria-label="right align" name="btnPlus" class="btn btn-default btn-lg">
                        <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Lista de Procedimentos</label></div>

                    <div class="panel-body">
                        <table id="indicadorLista" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Sistema</th>
                                    <th>SubSistema</th>
                                    <th>Servi�o</th>
                                    <th>Periodicidade</th>
                                    <th>Procedimento</th>
                                    <th>Remover</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if(!empty($_SESSION['erroProcedimento'])){
            echo '<div class="row">
                        <div class="alert alert-danger col-md-12">
                            <h4>';

            foreach ($_SESSION['erroProcedimento'] as $mensagem){
                echo $mensagem . '<br/>';
            }

            echo '           </h4>
                        </div>
                  </div>';

            unset($_SESSION['erroProcedimento']);
        }
        ?>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Procedimento Existentes</label></div>

                    <div class="panel-body">
                        <table id="indicadorProcedimento" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Sistema</th>
                                    <th>SubSistema</th>
                                    <th>Servi�o</th>
                                    <th>Periodicidade</th>
                                    <th>Procedimento</th>
                                    <th>Remover</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $ctdDb = 1;
                            if (!empty($servicoPmpPeriodicidade)) {
                                foreach ($servicoPmpPeriodicidade as $dados => $value) {
                                    echo('<tr>');
                                    echo("<td>{$value['cod_servico_pmp_periodicidade']}<input type='hidden' name='codServPer{$ctdDb}' value='{$value['cod_servico_pmp_periodicidade']}'></td>");
                                    echo("<td>{$selectSistema[$value['cod_sistema']]}</td>");
                                    echo("<td>{$selectSubSistema[$value['cod_subsistema']]}</td>");
                                    echo("<td>{$value['nome_servico_pmp']}</td>");
                                    echo("<td>{$selectPeriodicidade[$value['cod_tipo_periodicidade']]}</td>");
                                    echo("<td>{$selectProcedimento[$value['cod_procedimento']]}</td>");
                                    echo("<td><input type='checkbox' name='removeRow{$ctdDb}'> Excluir</td>");
                                    echo('</tr>');
                                    $ctdDb++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-offset-10 col-md-2">
        <div class="panel panel-primary">
            <div class="panel-heading">A��es</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">
                            <div class="btn-group">
                                <button aria-label="right align" class="btn btn-default btn-lg salvarListaPmp" title="Salvar">
                                    <i class="fa fa-floppy-o fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo("<input value='{$ctdDb}' name='ctd' type='hidden'>");
?>