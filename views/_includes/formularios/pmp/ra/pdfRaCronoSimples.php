<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/03/2017
 * Time: 09:17
 */

$dados = $_SESSION['modal'];
unset($_SESSION['modal']);

$mes = $dados['mes'];
$ano = $dados['ano'];
$linha = $dados['linha'];
$sistema = $dados['sistema'];
$subsistema = $dados['subsistema'];

$where = [];

if(!empty($mes)){
    $where[] = "mes = {$mes}";
}
if(!empty($ano)){
    $where[] = "ano = {$ano}";
}
if(!empty($linha)){
    $where[] = "cod_linha = {$linha}";
}
if(!empty($sistema)){
    $where[] = "cod_sistema = {$sistema}";
}
if(!empty($subsistema)){
    $where[] = "cod_subsistema = {$subsistema}";
}

if (!empty($dados['servico'])) {
    $where[] = "cod_servico_pmp = {$dados['servico']}";
}

$sql = "SELECT * FROM (
            SELECT 
            DISTINCT ON (cod_cronograma_pmp) cp.cod_cronograma_pmp, 
            mes,
            ano,
            cod_sistema,
            cod_subsistema,
            l.cod_linha,
            l.nome_linha,
            cod_servico_pmp,
            nome_servico_pmp,
            nome_via,
            nome_local,
            nome_poste,
            data_programada,
            nome_periodicidade,
            c.quinzena,
            turno,
            nome_procedimento,
            nome_status
            FROM cronograma c
            JOIN cronograma_pmp cp USING (cod_cronograma)
            JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
            JOIN status USING (cod_status)
            JOIN pmp_rede_aerea p USING (cod_pmp)
            JOIN pmp USING (cod_pmp)
            JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
            JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
            JOIN servico_pmp USING (cod_servico_pmp)
            JOIN procedimento USING (cod_procedimento)
            JOIN sub_sistema USING(cod_sub_sistema)
            JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
            JOIN local USING(cod_local)
            JOIN linha l USING (cod_linha)
            LEFT JOIN poste USING(cod_poste)
            LEFT JOIN via USING(cod_via)
            LEFT JOIN ssmp s ON s.cod_cronograma_pmp = cp.cod_cronograma_pmp
            ) alias";

$posSql = " ORDER BY cod_linha, mes, cod_servico_pmp, quinzena, data_programada";

if(!empty($where)){
    $where = " WHERE ". implode(' AND ', $where);
}

$selectServico = $this->medoo->query($sql . $where . $posSql)->fetchAll(PDO::FETCH_ASSOC);

?>

<div class="panel-heading" style="text-align: center; font-size: large; font-weight: bold; padding-top: 25px">
    <img src="<?php echo HOME_URI ?>/views/_images/metrofor.png" width="100px" height="50px" style="float: left">
    <h2 style="text-align: center"><label>O.S. Preventiva</label></br>Controle de Execu��o - Rede Aerea</h2>
</div>

<div>
    <?php
    $nomeMes = MainController::$monthComplete;
    $novaLinha;
    $novoMes;
    $codServico;
    $count = 0;

    foreach ($selectServico as $dados => $value) {
        if ($novaLinha != $value['nome_linha'] || $novoMes != $value['mes']) {
            if ($novaLinha) {
                echo "</table>";
                $codServico = "";
            }

            $novaLinha = $value['nome_linha'];
            $novoMes = $value['mes'];

            $titulo = $novaLinha . " - " . $nomeMes[$novoMes];

            echo '<h3 style="margin-top: 30px; margin-bottom: 1px; padding: 7px;text-align: center; background: #424242; color: #ffffff">'.$titulo.'</h3>
                    <table class="table table-bordered text-center" style="width: 100%;page-break-after:always;">
                        <tr>
                            <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Local</th>
                            <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Poste</th>
                            <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Periodicidade</th>
                            <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">Per�odo de Execu��o</th>
                            
                            <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">In�cio</th>
                            <th style="text-align: center; padding: 7px; width: auto; background: #0b97c4; color: #ffffff">T�rmino</th>
                            
                            <th style="text-align: center; padding: 7px; background: #0b97c4; color: #ffffff">OSMP</th>
                        </tr>';
        }

        $sql = "SELECT  cod_ssmp,  dias_servico, complemento,  data_programada,  data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS data_prevista,  cod_osmp	
                    FROM cronograma_pmp
                    LEFT JOIN ssmp USING (cod_cronograma_pmp)
                    LEFT JOIN osmp USING (cod_ssmp)
                    
                    WHERE cod_cronograma_pmp = {$value['cod_cronograma_pmp']}
                    ORDER BY data_programada";

        $selectSsOs = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        $countSsOs = count($selectSsOs);

        if ($codServico != $value['cod_servico_pmp']) {
            echo("<tr><td colspan='7' style='background: #4CAF50; padding: 7px; color: #ffffff; text-align: center'>{$value['nome_servico_pmp']}</td></tr>");
            $codServico = $value['cod_servico_pmp'];
        }
        $count = $count + 1;

        echo("<tr style='page-break-inside: avoid'>");
        echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0'>{$value['nome_local']}</td>");
        echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0'>{$value['nome_poste']}</td>");
        echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$value['nome_periodicidade']}</td>");
        echo("<td rowspan='{$countSsOs}' style='padding: 7px; background: #E0E0E0; text-align: center'>{$this->getMesPorQzn($value['quinzena'],$value['ano'])}</td>");

        if($countSsOs > 0){
            $boolean = false;
            $cod_ssmp = 0;

            $cods = array_column($selectSsOs, 'cod_ssmp');
            if($cods[0] != null)
                $countOcorrencias = array_count_values($cods);
            else{
                $countOcorrencias[0]=null;
            }

            foreach ($selectSsOs as $key => $val) {
                if($boolean){ // Ap�s o primeiro loop, criar nova linha
                    echo("<tr>");
                }
                $boolean = true;

                if($cod_ssmp != $val['cod_ssmp']){
                    $countOs = $countOcorrencias[$val['cod_ssmp']];

                    if($value['quinzena'] < $this->getQznPerData($val['data_programada'])){
                        $style = "background-color: red";
                    }else{
                        $style="background-color: #ECEFF1";
                    }

                    echo("<td rowspan='{$countOs}' style='padding: 7px; text-align: center; {$style}'>" . $this->getOnlyData($val['data_programada']) . "</td>");
                    echo("<td rowspan='{$countOs}' style='padding: 7px; text-align: center; {$style}'>" . $this->getOnlyData($val['data_prevista']) . "</td>");

                    $cod_ssmp = $val['cod_ssmp'];
                }

                echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'>{$val['cod_osmp']}</td>");
                echo("</tr>");
            }
        }else{
            echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
            echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
            echo("<td style='padding: 7px; background: #ECEFF1; text-align: center'></td>");
            echo("</tr>");
        }
    }
    ?>
</div>