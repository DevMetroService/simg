<div class="page-header">
    <h1>Pesquisa PMP Vlt</h1>
</div>
<form action="<?php echo HOME_URI; ?>dashboardPmp/executarPesquisaPmpVlt" class="form-group" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <label>Pesquisa PMP Vlt</label>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo moduloIdentificacaoGeralPesquisa($refill, $selectSistema, $subsistema, $servicoPmp); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Local</label></div>
                                <div class="panel-body">
                                    <label>Local</label>
                                    <select name="local" class="form-control">
                                        <option selected value="">Todos</option>
                                        <optgroup label="Linha Oeste"></optgroup>
                                        <?php
                                        $pmp_estacao = $this->medoo->select('local', '*', ['cod_linha' => 1]);

                                        foreach ($pmp_estacao as $dados => $value) {
                                            if (!empty($refill['local']) && $refill['local'] == $value["cod_local"])
                                                echo('<option value="' . $value["cod_local"] . '" selected>' . $value['nome_local'] . '</option>');
                                            else
                                                echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                        }
                                        ?>
                                        <optgroup label="Linha Cariri"></optgroup>
                                        <?php
                                        $pmp_estacao = $this->medoo->select('local', '*', ['cod_linha' => 2]);

                                        foreach ($pmp_estacao as $dados => $value) {
                                            if (!empty($refill['local']) && $refill['local'] == $value["cod_local"])
                                                echo('<option value="' . $value["cod_local"] . '" selected>' . $value['nome_local'] . '</option>');
                                            else
                                                echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                        }
                                        ?>
                                        <optgroup label="Linha Sul"></optgroup>
                                        <?php
                                        $pmp_estacao = $this->medoo->select('local', '*', ['cod_linha' => 5]);

                                        foreach ($pmp_estacao as $dados => $value) {
                                            if (!empty($refill['local']) && $refill['local'] == $value["cod_local"])
                                                echo('<option value="' . $value["cod_local"] . '" selected>' . $value['nome_local'] . '</option>');
                                            else
                                                echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                        }
                                        ?>
                                        <optgroup label="Linha Sumar� - Sobral Sul"></optgroup>
                                        <?php
                                        $pmp_estacao = $this->medoo->select('local', '*', ['cod_linha' => 3]);

                                        foreach ($pmp_estacao as $dados => $value) {
                                            if (!empty($refill['local']) && $refill['local'] == $value["cod_local"])
                                                echo('<option value="' . $value["cod_local"] . '" selected>' . $value['nome_local'] . '</option>');
                                            else
                                                echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                        }
                                        ?>
                                        <optgroup label="Linha Grendene - Sobral Norte"></optgroup>
                                        <?php
                                        $pmp_estacao = $this->medoo->select('local', '*', ['cod_linha' => 4]);

                                        foreach ($pmp_estacao as $dados => $value) {
                                            if (!empty($refill['local']) && $refill['local'] == $value["cod_local"])
                                                echo('<option value="' . $value["cod_local"] . '" selected>' . $value['nome_local'] . '</option>');
                                            else
                                                echo('<option value="' . $value["cod_local"] . '">' . $value['nome_local'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php
                        echo moduloPeriodicidadePesquisa($refill, 4, $periodicidade);
                        ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
                            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro
                            </button>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 1em"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Resultado da Pesquisa
                                    <strong>Total Encontrado: <?php echo (!empty($_SESSION['resultadoPesquisaPmpVlt'])) ? count($_SESSION['resultadoPesquisaPmpVlt']) : 0; ?></strong>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tabelaPesquisaPmp" class="table table-bordered table-striped">
                                                <thead id="">
                                                <tr role="row">
                                                    <th>Cod</th>
                                                    <th>Veiculo</th>
                                                    <th>Sistema</th>
                                                    <th>SubSistema</th>
                                                    <th>Servi�o</th>
                                                    <th>Periodicidade</th>
                                                    <th>Status</th>
                                                    <th>A��o</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if (!empty($_SESSION['resultadoPesquisaPmpVlt'])) {
                                                    foreach ($_SESSION['resultadoPesquisaPmpVlt'] as $dados => $value) {
                                                        switch ($value['ativo']) {
                                                            case "A":
                                                                $status = "Ativo";
                                                                break;
                                                            case "E":
                                                                $status = "Aberto";
                                                                break;
                                                            case "D":
                                                                $status = "Depreciado";
                                                                break;
                                                        }

                                                        echo('<tr>');
                                                        echo("<td>{$value['cod_pmp']}<input type='hidden' value='Material Rodante Vlt' /></td>");
                                                        echo("<td>{$local[$value['cod_veiculo']]}</td>");
                                                        echo("<td>{$sistema[$value['cod_sistema']]}</td>");
                                                        echo("<td>{$subsistema[$value['cod_subsistema']]}</td>");
                                                        echo("<td>{$servico[$value['cod_servico_pmp']]}</td>");
                                                        echo("<td>{$periodicidade[$value['cod_tipo_periodicidade']]}</td>");
                                                        echo("<td>{$status}</td>");
                                                        echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button>");
                                                        if($value['ativo'] == "D")
                                                            echo("<button type='button' title='Ativar' class='btn btn-success btn-circle btnAtivarDepreciarPmp'><i class='fa fa-check'></i></button>");
                                                        else if($value['ativo'] == "E")
                                                            echo("<button type='button' title='Deletar' class='btn btn-danger btn-circle btnAtivarDepreciarPmp'><i class='fa fa-trash'></i></button>");
                                                        else
                                                            echo("<button type='button' title='Depreciar' class='btn btn-warning btn-circle btnAtivarDepreciarPmp'><i class='fa fa-times'></i></button>");
                                                        echo('</td>');
                                                        echo('</tr>');
                                                    }
                                                    unset($_SESSION['resultadoPesquisaPmpVlt']);
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>