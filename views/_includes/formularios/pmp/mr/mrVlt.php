<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 01/12/2017
 * Time: 11:51
 */
?>
<div class="page-header">
    <h1>Gerador PMP - Material Rodante VLT</h1>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><label>PMP - Material Rodante VLT</label></div>

    <div class="panel-body">
        <div class="row">
            <?php echo moduloIdentificacaoGeral($refill, $sistemaVlt, $subsistemaVlt, $servicoPmpVlt, $periodicidadeVlt); ?>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Ve�culo</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                echo('
                                            <label>Selecione</label>
                                            <select name="local[]" class="form-control multiselect" multiple="multiple">
                                            <optgroup label="Linha Oeste"></optgroup>');

                                foreach ($pmp_veiculo_oeste as $dados => $value) {
                                    echo('<option value="' . $value["cod_veiculo"] . '">' . $value['nome_veiculo'] . '</option>');
                                }
                                echo('<optgroup label="Linha Cariri"></optgroup>');
                                foreach ($pmp_veiculo_cariri as $dados => $value) {
                                    echo('<option value="' . $value["cod_veiculo"] . '">' . $value['nome_veiculo'] . '</option>');
                                }
                                echo('<optgroup label="Linha Sobral"></optgroup>');
                                foreach ($pmp_veiculo_sobral as $dados => $value) {
                                    echo('<option value="' . $value["cod_veiculo"] . '">' . $value['nome_veiculo'] . '</option>');
                                }
                                echo('<optgroup label="Linha Parangaba/Mucuripe"></optgroup>');
                                foreach ($pmp_veiculo_parangaba_mucuripe as $dados => $value) {
                                    echo('<option value="' . $value["cod_veiculo"] . '">' . $value['nome_veiculo'] . '</option>');
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <button type="button" aria-label="right align" name="btnPlus" class="btn btn-default btn-lg">
                        <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Linhas Geradas</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tabelaPmpMr" class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Veiculo</th>
                                        <th>Sistema</th>
                                        <th>SubSistema</th>
                                        <th>Servi�o</th>
                                        <th>Periodicidade</th>
                                        <th>Procedimento</th>
                                        <th>Qzn In�cio</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Pmp - Armazenados</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tabelaPmpMrExec" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Cod</th>
                                        <th>Veiculo</th>
                                        <th>Sistema</th>
                                        <th>SubSistema</th>
                                        <th>Servi�o</th>
                                        <th>Periodicidade</th>
                                        <th>Procedimento</th>
                                        <th>Qzn In�cio</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ctdDb = 1;
                                    if (!empty($pmpMaterialRodanteVlt)) {
                                        foreach ($pmpMaterialRodanteVlt as $dados) {

                                            echo '<tr>';
                                            echo("<td>{$dados['cod_pmp']}<input type='hidden' name='codPmpMr{$ctdDb}' value='{$dados['cod_pmp']}'></td>");
                                            echo("<td>{$veiculo[$dados['cod_veiculo']]}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_sistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_subsistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_servico_pmp']}</td>");
                                            echo("<td>{$periodicidade[$dados['cod_tipo_periodicidade']]}</td>");
                                            echo("<td>{$procedimento[$dados['cod_procedimento']]}</td>");
                                            echo("<td>{$dados['quinzena']}</td>");
                                            echo("<td><input type='checkbox' name='excluirLinhaPmpBanco{$ctdDb}' id='excluirLinhaPmpBanco{$ctdDb}' class='form-control'><label for='excluirLinhaPmpBanco{$ctdDb}'>Excluir</label></td>");
                                            echo '</tr>';
                                            $ctdDb++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php if (!empty($gerarPmpAnual)){ ?>
    <div class="col-md-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Gerar PMP</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">
                            <div class="btn-group">
                                <button type="button" aria-label="right align"
                                        class="btn btn-success btn-lg gerarPmpAnual" title="Autorizar PMP Anual">
                                    <i class="fa fa-calendar-check-o fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-offset-8 col-md-2">
        <?php }else{ ?>
        <div class="col-md-offset-10 col-md-2">
            <?php } ?>
            <div class="panel panel-primary">
                <div class="panel-heading">A��es</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group  btn-group-justified " role="group">

                                <?php echo $btnsAcao; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php echo "<input value='{$ctdDb}' name='ctd' type='hidden'>"; ?>
