<style>
    table, th, tr, td{
        border-collapse: collapse;
        border: 1px solid black;
    }

    thead {
        display: table-header-group
    }
    @media print{
        table, th, tr, td {
            page-break-after: ;
        }
    }
</style>

<div class="panel-heading" style="text-align: center; font-size: large; font-weight: bold; padding-top: 25px">
    <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo2.png" width="100px" height="50px"
         style="float: left">
    <h3>PLANO DE MANUTEN��O PREVENTIVA</h3>
</div>

<?php

$pmpTelecom = $this->medoo->select("pmp_telecom",
    [
        '[><]pmp' => 'cod_pmp',
        '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
        '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
        '[><]servico_pmp' => 'cod_servico_pmp',
        '[><]tipo_periodicidade' => "cod_tipo_periodicidade",
        '[><]procedimento' => "cod_procedimento",
        '[><]local' => "cod_local",
        '[><]linha' => "cod_linha"
    ], [
        'cod_local',
        'nome_local',
        'cod_servico_pmp',
        'nome_servico_pmp',
        'cod_sub_sistema',
        'cod_tipo_periodicidade',
        'nome_periodicidade',
        'nome_procedimento',
        'turno',
        'homem_hora',
        'nome_linha',
        'quinzena'
    ], [
        'ORDER' => [
            "cod_tipo_periodicidade" => "DESC",
            'nome_local',
            'nome_servico_pmp'
        ], 'ativo' => ['A', 'E']
    ]);

if(!empty($_SESSION['qzn'])) {
    $quinzena = $_SESSION['qzn'];
    $qznAno   = $_SESSION['qzn'];
}else{
    $quinzena = 1;
    $qznAno   = 24;
}

for ($quinzena; $quinzena <= $qznAno; $quinzena++) {

?>

<div class="panel-heading" style="text-align: center; font-size: large; font-weight: bold; padding-top: 25px">
    <label>Telecom - Quinzena <?php echo $quinzena ?></label>
</div>

<div class="panel-body">
    <div class="row">
        <table>
                <tr>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">LOCAL</th>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">LINHA</th>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">SERVI�OS DE MANUTEN��O PREVENTIVA</th>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">PERIODICIDADE</th>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">TURNO</th>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">HH</th>
                    <th style="font-size: 10px; text-align: center; padding: 10px; background-color: #b4c6e7">PROCEDIMENTO</th>
                </tr>
            <?php

            if (empty($pmpTelecom)) {
                echo('<tr><td colspan="7" style="font-size: 14px; text-align: center; padding: 5px;">N�o existe registros</td></tr>');
            } else {
                $dadosQzn = $this->filtrarQzn($pmpTelecom, $quinzena);

                $dadosArmazenados = array();

                if(empty($dadosQzn)){
                    echo('<tr><td colspan="7" style="font-size: 14px; text-align: center; padding: 5px;">N�o existe registros</td></tr>');
                }else{
                    foreach ($dadosQzn as $dado) {

                        if (!in_array(array($dado['cod_local'], $dado['cod_sub_sistema'], $dado['cod_servico_pmp']), $dadosArmazenados)) {

                            echo('<tr>');
                            echo("<td style='font-size: 8px; padding: 5px; background-color: #9d9d9d'>{$dado['nome_local']}</td>");
                            echo("<td style='font-size: 8px; padding: 5px; background-color: #9d9d9d;'>{$dado['nome_linha']}</td>");
                            echo("<td style='font-size: 8px; padding: 5px;background-color: #76C04E'>{$dado['nome_servico_pmp']}</td>");
                            echo("<td style='font-size: 8px; padding: 5px;'>{$dado['nome_periodicidade']}</td>");
                            echo ($dado['turno'] == 'D') ? "<td style='font-size: 8px; padding: 5px;'>Diurno</td>" : "<td style='font-size: 8px; padding: 5px;'>Noturno</td>";
                            echo("<td style='font-size: 8px; padding: 5px; text-align: center;'>{$dado['homem_hora']}</td>");
                            echo("<td style='font-size: 8px; padding: 5px;'>{$dado['nome_procedimento']}</td>");
                            echo('<tr>');

                            if ($dado['cod_tipo_periodicidade'] == 2) {
                                echo('<tr>');
                                echo("<td style='font-size: 8px; padding: 5px; background-color: #9d9d9d'>{$dado['nome_local']}</td>");
                                echo("<td style='font-size: 8px; padding: 5px; background-color: #9d9d9d;'>{$dado['nome_linha']}</td>");
                                echo("<td style='font-size: 8px; padding: 5px;background-color: #76C04E'>{$dado['nome_servico_pmp']}</td>");
                                echo("<td style='font-size: 8px; padding: 5px;'>{$dado['nome_periodicidade']}</td>");
                                echo ($dado['turno'] == 'D') ? "<td style='font-size: 8px; padding: 5px;'>Diurno</td>" : "<td style='font-size: 8px; padding: 5px;'>Noturno</td>";
                                echo("<td style='font-size: 8px; padding: 5px; text-align: center;'>{$dado['homem_hora']}</td>");
                                echo("<td style='font-size: 8px; padding: 5px;'>{$dado['nome_procedimento']}</td>");
                                echo('<tr>');
                            }

                            $dadosArmazenados[] = array($dado['cod_local'], $dado['cod_sub_sistema'], $dado['cod_servico_pmp']);
                        }
                    }
                }
            }
            ?>
        </table>
    </div>
</div>

<?php
}

function filtrarQzn($dados, $quinzena)
{
    foreach ($dados as $dado) {

        $qznInicial = $dado['quinzena'];
        $perio = $dado['cod_tipo_periodicidade'];

        $qzn = $qznInicial;

        while ($qzn <= $quinzena) {

            if ($quinzena == $qzn) {
                $dadosQzn[] = $dado;
            }

            switch ($perio) {
                case 9: // Anual // a cada 12 meses
                    $qzn += 24;
                    break;
                case 8: // Semestral // a cada 6 meses
                    $qzn += 12;
                    break;
                case 7: // Quadrimestral // a cada 4 meses
                    $qzn += 8;
                    break;
                case 6: // Trimestral // a cada 3 meses
                    $qzn += 6;
                    break;
                case 5: // Bimestral // a cada 2 meses
                    $qzn += 4;
                    break;
                case 4: // Mensal // a cada m�s
                    $qzn += 2;
                    break;
                case 3: // Quinzenal // uma vez na quinzena
                    $qzn += 1;
                    break;
                case 2: // Semanal // duas vezes na quinzena
                    $qzn += 1;
                    break;
                case 1: // Diario // uma vez na quinzena com quantidade de dias de servi�o em 15
                    $qzn += 1;
                    break;
            }
        }
    }

    return $dadosQzn;
}
unset($_SESSION['qzn']);
?>