<div class="page-header">
    <h1>Gerador PMP - Via Permanente</h1>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><label>PMP - Via Permanente</label></div>
    
    <div class="panel-body">
        <div class="row">
            <?php echo moduloIdentificacaoGeral($refill, $sistemaVp, $subsistemaVp, $servicoPmpVp, $periodicidadeVp); ?>
        </div>

        <div class="row">
            <?php echo moduloRecursosHH($refill, 12); ?>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Local(s)</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Linha</label>
                                <select name="linha" class="form-control">
                                    <option selected disabled value="">Selecione a Linha</option>
                                    <?php
                                    $selectlinha = $this->medoo->select('linha', '*');

                                    foreach ($selectlinha as $dados => $value) {
                                        if(!empty($refill['linha']) && $refill['linha'] == $value['cod_linha'])
                                            echo("<option selected value='{$value['cod_linha']}'>{$value['nome_linha']}</option>");
                                        else
                                            echo("<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>");
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Trecho Inicial</label>
                                <select name="trechoInicial" class="form-control">
                                    <option selected disabled value="">Selecione a trecho</option>
                                    <?php
                                    if (!empty($refill['linha'])) {
                                        $trecho = $this->medoo->select("estacao", ['cod_estacao', 'nome_estacao'], ["cod_linha" => (int)$refill['linha']]);

                                        foreach ($trecho as $dados => $value) {
                                            if (!empty($refill['trechoInicial']) && $refill['trechoInicial'] == $value['cod_estacao'])
                                                echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>Trecho Final</label>
                                <select name="trechoFinal" class="form-control">
                                    <option selected disabled value="">Selecione a trecho</option>
                                    <?php
                                    if (!empty($refill['linha'])) {
                                        $trecho = $this->medoo->select("estacao", ['cod_estacao', 'nome_estacao'], ["cod_linha" => (int)$refill['linha']]);

                                        foreach ($trecho as $dados => $value) {
                                            if (!empty($refill['trechoFinal']) && $refill['trechoFinal'] == $value['cod_estacao'])
                                                echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Via</label>
                                <select name="via" class="form-control">
                                    <option selected disabled value="">Selecione a Via</option>
                                    <?php
                                    if (!empty($refill['linha'])) {
                                        $viaSelect = $this->medoo->select("via", ['cod_via', 'nome_via'], ["cod_linha" => (int)$refill['linha']]);

                                        foreach ($viaSelect as $dados => $value) {
                                            if (!empty($refill['via']) && $refill['via'] == $value['cod_via'])
                                                echo('<option value="' . $value['cod_via'] . '" selected>' . $value['nome_via'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_via'] . '">' . $value['nome_via'] . '</option>');
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Km Inicial</label>
                                <input value="<?php if(!empty($refill['kmInicial'])) echo $refill['kmInicial'] ?>" name="kmInicial" class="form-control float">
                            </div>
                            <div class="col-md-3">
                                <label>Km Final</label>
                                <input value="<?php if(!empty($refill['kmFinal'])) echo $refill['kmFinal'] ?>" name="kmFinal" class="form-control float">
                            </div>
                            <div class="col-md-3">
                                <label>AMV</label>
                                <select name="amv" class="form-control">
                                    <option selected disabled value="">Selecione o AMV</option>
                                    <?php
                                    if (!empty($refill['amv'])) {
                                        $amvSelect = $this->medoo->select("amv", ['cod_amv', 'nome_amv'], ["cod_estacao" => (int)$refill['trechoInicial']]);

                                        foreach ($amvSelect as $dados => $value) {
                                            if (!empty($refill['amv']) && $refill['amv'] == $value['cod_amv'])
                                                echo('<option value="' . $value['cod_amv'] . '" selected>' . $value['nome_amv'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_amv'] . '">' . $value['nome_amv'] . '</option>');
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <button type="button" aria-label="right align" name="btnPlus" class="btn btn-default btn-lg">
                        <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Linhas Geradas</label></div>

                    <div class="panel-body over-x">
                        <div class="row table-responsive">
                            <div class="col-lg-12">
                                <table id="tabelaPmpVp" class="table table-striped table-bordered no-footer">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Linha</th>
                                            <th>Sistema</th>
                                            <th>SubSistema</th>
                                            <th>Servi�o</th>
                                            <th>Periodicidade</th>
                                            <th>Procedimento</th>
                                            <th>Qzn In�cio</th>
                                            <th>Turno</th>
                                            <th>M�o de obra</th>
                                            <th>Horas �teis</th>
                                            <th>HH</th>
                                            <th>Trecho Inicial</th>
                                            <th>Trecho Final</th>
                                            <th>AMV</th>
                                            <th>Via</th>
                                            <th>Km Inicial</th>
                                            <th>Km Final</th>
                                            <th>A��o</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Pmp - Armazenados</label></div>

                    <div class="panel-body over-x">
                        <div class="row table-responsive">
                            <div class="col-lg-12">
                                <table id="tabelaPmpVpExec" class="table table-striped table-bordered no-footer">
                                    <thead>
                                        <tr>
                                            <th>Cod</th>
                                            <th>Linha</th>
                                            <th>Sistema</th>
                                            <th>SubSistema</th>
                                            <th>Servi�o</th>
                                            <th>Periodicidade</th>
                                            <th>Procedimento</th>
                                            <th>Qzn In�cio</th>
                                            <th>Turno</th>
                                            <th>M�o de obra</th>
                                            <th>Horas �teis</th>
                                            <th>HH</th>
                                            <th>Trecho Inicial</th>
                                            <th>Trecho Final</th>
                                            <th>AMV</th>
                                            <th>Via</th>
                                            <th>Km Inicial</th>
                                            <th>Km Final</th>
                                            <th>A��o</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ctdDb = 1;
                                    if (!empty($pmpViaPermanente)) {
                                        foreach ($pmpViaPermanente as $dados) {
                                            if ($dados['turno'] == 'D') {
                                                $turno = 'Diurno';
                                            } else {
                                                $turno = 'Noturno';
                                            }

                                            echo '<tr>';
                                            echo("<td>{$dados['cod_pmp']}</td>");//<input type='hidden' name='codPmpVp{$ctdDb}' value='{$dados['cod_pmp']}'></td>");
                                            echo("<td>{$estacao[$dados['cod_estacao_inicial']]['nome_linha']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_sistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_subsistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_servico_pmp']}</td>");
                                            echo("<td>{$periodicidade[$dados['cod_tipo_periodicidade']]}</td>");
                                            echo("<td>{$procedimento[$dados['cod_procedimento']]}</td>");
                                            echo("<td>{$dados['quinzena']}</td>");
                                            echo("<td>{$turno}</td>");
                                            echo("<td>{$dados['mao_obra']}</td>");
                                            echo("<td>{$dados['horas_uteis']}</td>");
                                            echo("<td>{$dados['homem_hora']}</td>");
                                            echo("<td>{$estacao[$dados['cod_estacao_inicial']]['nome_estacao']}</td>");
                                            echo("<td>{$estacao[$dados['cod_estacao_final']]['nome_estacao']}</td>");
                                            echo("<td>{$amv[$dados['cod_amv']]}</td>");
                                            echo("<td>{$via[$dados['cod_via']]}</td>");
                                            echo("<td>{$dados['km_inicial']}</td>");
                                            echo("<td>{$dados['km_final']}</td>");
                                            echo("<td><input type='checkbox' value='{$dados['cod_pmp']}' name='excluirLinhaPmpBanco{$ctdDb}' id='excluirLinhaPmpBanco{$ctdDb}' class='form-control'><label for='excluirLinhaPmpBanco{$ctdDb}'>Excluir</label></td>");
                                            echo '</tr>';
                                            $ctdDb ++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php if(!empty($gerarPmpAnual)){ ?>
    <div class="col-md-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Gerar PMP</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">
                            <div class="btn-group">
                                <button type="button" aria-label="right align" class="btn btn-success btn-lg gerarPmpAnual" title="Autorizar PMP Anual">
                                    <i class="fa fa-calendar-check-o fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-offset-8 col-md-2">
        <?php }else{?>
        <div class="col-md-offset-10 col-md-2">
        <?php } ?>
        <div class="panel panel-primary">
            <div class="panel-heading">A��es</div>
            
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">

                            <?php echo $btnsAcao; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo "<input value='{$ctdDb}' name='ctd' type='hidden'>"; ?>