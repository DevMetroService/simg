<div class="page-header">
    <h1>Pesquisa PMP Via Permanente</h1>
</div>
<form action="<?php echo HOME_URI; ?>/dashboardPmp/executarPesquisaPmpVp" class="form-group" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <label>Pesquisa PMP Via Permanente</label>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo moduloIdentificacaoGeralPesquisa($refill, $selectSistema, $subsistemaVp, $servicoPmpVp); ?>
                    </div>
                    <div class="row">
                        <?php
                        echo moduloPeriodicidadePesquisa($refill, 6, $periodicidade);
                        echo moduloRecursosHHPesquisa($refill, 6);
                        ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
                            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro
                            </button>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 1em"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Resultado da Pesquisa
                                    <strong>Total Encontrado: <?php echo (!empty($_SESSION['resultadoPesquisaPmpVp'])) ? count($_SESSION['resultadoPesquisaPmpVp']) : 0; ?></strong>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tabelaPesquisaPmp" class="table table-bordered table-striped">
                                                <thead id="">
                                                <tr role="row">
                                                    <th>Cod</th>
                                                    <th>Sistema</th>
                                                    <th>SubSistema</th>
                                                    <th>Servi�o</th>
                                                    <th>Periodicidade</th>
    <!--                                                <th>Procedimento</th>-->
    <!--                                                <th>Qzn In�cio</th>-->
    <!--                                                <th>Turno</th>-->
    <!--                                                <th>M�o de obra</th>-->
    <!--                                                <th>Horas �teis</th>-->
    <!--                                                <th>HH</th>-->
                                                    <th>Status</th>
                                                    <th>A��o</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if (!empty($_SESSION['resultadoPesquisaPmpVp'])) {
                                                    foreach ($_SESSION['resultadoPesquisaPmpVp'] as $dados => $value) {

                                                        switch ($value['turno']) {
                                                            case "D":
                                                                $turno = "Diurno";
                                                                break;
                                                            case "N":
                                                                $turno = "Noturno";
                                                                break;
                                                        }

                                                        switch ($value['ativo']) {
                                                            case "A":
                                                                $status = "Ativo";
                                                                break;
                                                            case "E":
                                                                $status = "Aberto";
                                                                break;
                                                            case "D":
                                                                $status = "Depreciado";
                                                                break;
                                                        }

                                                        echo('<tr>');
                                                        echo("<td>{$value['cod_pmp']}<input type='hidden' value='Via Permanente' /></td>");
                                                        echo("<td>{$sistema[$value['cod_sistema']]}</td>");
                                                        echo("<td>{$subsistema[$value['cod_subsistema']]}</td>");
                                                        echo("<td>{$servico[$value['cod_servico_pmp']]}</td>");
                                                        echo("<td>{$periodicidade[$value['cod_tipo_periodicidade']]}</td>");
    //                                                    echo("<td>{$procedimento[$value['cod_procedimento']]}</td>");
    //                                                    echo("<td>{$value['quinzena']}</td>");
    //                                                    echo("<td>{$turno}</td>");
    //                                                    echo("<td>{$value['mao_obra']}</td>");
    //                                                    echo("<td>{$value['horas_uteis']}</td>");
    //                                                    echo("<td>{$value['homem_hora']}</td>");
                                                        echo("<td>{$status}</td>");
                                                        echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button>");
                                                        if($value['ativo'] == "D")
                                                            echo("<button type='button' title='Ativar' class='btn btn-success btn-circle btnAtivarDepreciarPmp'><i class='fa fa-check'></i></button>");
                                                        else if($value['ativo'] == "E")
                                                            echo("<button type='button' title='Deletar' class='btn btn-danger btn-circle btnAtivarDepreciarPmp'><i class='fa fa-trash'></i></button>");
                                                        else
                                                            echo("<button type='button' title='Depreciar' class='btn btn-warning btn-circle btnAtivarDepreciarPmp'><i class='fa fa-times'></i></button>");
                                                        echo('</td>');
                                                        echo('</tr>');
                                                    }
                                                    unset($_SESSION['resultadoPesquisaPmpVp']);
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>