<?php
function atraso($atraso){
    if(!empty($atraso))
        return $atraso . " minutos";
    else
        return '';
}

$conteudo = "N. Osp;";
$conteudo .= "N. Ssp;";
$conteudo .= "Tipo OSP;";
$conteudo .= "Responsável pelo Preenchimento;";
$conteudo .= "Data de Abertura Osp;";
$conteudo .= "Data de Abertura Ssp;";
$conteudo .= "Data Programada Ssp;";

$conteudo .= "Nome Status;";
$conteudo .= "Data Referente ao Status;";
$conteudo .= "Descrição Status;";
$conteudo .= "Data / Hora de Recebimento de Informações;";

$conteudo .= "Veiculo;";
$conteudo .= "Carro;";
$conteudo .= "Odometro;";

$conteudo .= "Linha Atuada;";
$conteudo .= "Trecho Atuado;";
$conteudo .= "Ponto Notável;";
$conteudo .= "Complemento Local;";

$conteudo .= "Via;";
$conteudo .= "Km Inicial;";
$conteudo .= "Km Final;";
$conteudo .= "Posição Atuada;";

$conteudo .= "Grupo;";
$conteudo .= "Sistema;";
$conteudo .= "Sub Sistema;";

$conteudo .= "Serviço;";
$conteudo .= "Complemento Serviço;";

$conteudo .= "Efetivo;";
$conteudo .= "Tempo;";
$conteudo .= "Unidade de tempo;";
$conteudo .= "Total Serviço;";
$conteudo .= "Unidade de Medida;";

$conteudo .= "Data Recebimento;";
$conteudo .= "Atraso Recebimento;";

$conteudo .= "Preparacao Inicio;";
$conteudo .= "Atraso Inicio Preparacao;";

$conteudo .= "Acesso Pronto Sair;";
$conteudo .= "Atraso Acesso Pronto Sair;";

$conteudo .= "Acesso Saída Autorizada;";

$conteudo .= "Acesso Saída;";
$conteudo .= "Atraso Acesso Saída;";

$conteudo .= "Acesso Chegada;";
$conteudo .= "Atraso Acesso Chegada;";

$conteudo .= "Execução Inicio;";
$conteudo .= "Atraso Execução Inicio;";

$conteudo .= "Execução Termino;";
$conteudo .= "Atraso Execução Termino;";

$conteudo .= "Regresso Pronto Sair;";
$conteudo .= "Atraso Regresso Pronto Sair;";

$conteudo .= "Regresso Saída Autorizada;";

$conteudo .= "Regresso Chegada;";
$conteudo .= "Atraso Regresso Chegada;";

$conteudo .= "Regresso Desmobilização;";
$conteudo .= "Descricao Tempo;";

$conteudo .= "Local;";
$conteudo .= "Equipe;";

$conteudo .= "Causa Avaria;";
$conteudo .= "Obs. Causa;";

$conteudo .= "Atuacao Execução;";
$conteudo .= "Obs. Atuação;";

$conteudo .= "Agente Causador;";

$conteudo .= "Clima;";
$conteudo .= "Temperatura;";

$conteudo .= "Transporte;";

$conteudo .= "Cautela;";
$conteudo .= "km Inicial Cautela;";
$conteudo .= "km Fim Cautela;";
$conteudo .= "Restringir Velocidade;";
$conteudo .= "Dados Complementares;";

$conteudo .= "Descrição tipo fechamento;";

$conteudo .= "Liberação;";

$conteudo .= "Tipo pendencia;";
$conteudo .= "Pendencia;";

$conteudo .= "Motivo da não Execução;";

$conteudo .= "Responsavel pelas Informações;\n";

$conteudo = utf8_decode($conteudo);


if(!empty($returnPesquisa)) {

    //// Funções basicas executadas apenas uma vez no loop ////

    $responsavel = array();
    $selectResponsavel = $this->medoo->select("usuario", ["cod_usuario", "usuario"]);
    foreach ($selectResponsavel as $dados) {
        $responsavel[$dados['cod_usuario']] = $dados['usuario'];
    }

    $funcionario = array();
    $selectFuncionario = $this->medoo->select("funcionario",
    ["[><]funcionario_empresa" => "cod_funcionario_empresa"], 
    ["funcionario_empresa.matricula", "funcionario.cod_funcionario", "nome_funcionario"]);
    foreach ($selectFuncionario as $dados) {
        $funcionario[$dados['cod_funcionario']] = $dados['nome_funcionario'] . " - " . $dados['matricula'];
    }

    $linha = array();
    $selectLinha = $this->medoo->select("linha", ["cod_linha", "nome_linha"]);
    foreach ($selectLinha as $dados) {
        $linha[$dados['cod_linha']] = $dados['nome_linha'];
    }

    $trecho = array();
    $selectTrecho = $this->medoo->select("trecho", ["cod_trecho", "nome_trecho","descricao_trecho"]);
    foreach ($selectTrecho as $dados) {
        $trecho[$dados['cod_trecho']] = $dados;
    }

    $pn = array();
    $selectPn = $this->medoo->select("ponto_notavel", ["cod_ponto_notavel", "nome_ponto"]);
    foreach ($selectPn as $dados) {
        $pn[$dados['cod_ponto_notavel']] = $dados["nome_ponto"];
    }

    $grupo = array();
    $selectGrupo = $this->medoo->select('grupo', ['cod_grupo', 'nome_grupo']);
    foreach ($selectGrupo as $dados) {
        $grupo[$dados['cod_grupo']] = $dados['nome_grupo'];
    }

    $sistema = array();
    $selectSistema = $this->medoo->select('sistema', ["cod_sistema", 'nome_sistema']);
    foreach ($selectSistema as $dados) {
        $sistema[$dados['cod_sistema']] = $dados['nome_sistema'];
    }

    $subsistema = array();
    $selectSubSistema = $this->medoo->select('subsistema', ['cod_subsistema', 'nome_subsistema']);
    foreach ($selectSubSistema as $dados) {
        $subsistema[$dados['cod_subsistema']] = $dados['nome_subsistema'];
    }

    $via = array();
    $selectVia = $this->medoo->select("via", ["cod_via", "nome_via"]);
    foreach ($selectVia as $dados) {
        $via[$dados['cod_via']] = $dados['nome_via'];
    }

    $unidadeTempo = array();
    $selectUnidadeTempo = $this->medoo->select('unidade_tempo', ['cod_uni_tempo', 'nome_un_tempo']);
    foreach ($selectUnidadeTempo as $dados) {
        $unidadeTempo[$dados['cod_uni_tempo']] = $dados['nome_un_tempo'];
    }

    $unidadeMedida = array();
    $selectUnidadeMedida = $this->medoo->select('unidade_medida', ['cod_uni_medida', 'nome_uni_medida']);
    foreach ($selectUnidadeMedida as $dados) {
        $unidadeMedida[$dados['cod_uni_medida']] = $dados['nome_uni_medida'];
    }

    $veiculo = array();
    $selectVeiculo = $this->medoo->select("automovel", ['cod_automovel', 'nome_automovel', 'numero_automovel']);
    foreach ($selectVeiculo as $dados) {
        $veiculo[$dados['cod_automovel']] = $dados['nome_automovel'] . ' - ' . $dados['numero_automovel'];
    }

    $pendencia = array();
    $selectPendencia = $this->medoo->select("pendencia", ['cod_pendencia', "nome_pendencia", "cod_tipo_pendencia"]);
    foreach ($selectPendencia as $dados) {
        $pendencia[$dados['cod_pendencia']] = $dados;
    }

    $causa = array();
    $selectCausa = $this->medoo->select("causa", ['cod_causa', "nome_causa"]);
    foreach ($selectCausa as $dados) {
        $causa[$dados['cod_causa']] = $dados['nome_causa'];
    }

    $atuacao = array();
    $selectAtuacao = $this->medoo->select("atuacao", ['cod_atuacao', "nome_atuacao"]);
    foreach ($selectAtuacao as $dados) {
        $atuacao[$dados['cod_atuacao']] = $dados['nome_atuacao'];
    }

    $agCausador = array();
    $selectAgCausador = $this->medoo->select("agente_causador", ['cod_ag_causador', "nome_agente"]);
    foreach ($selectAgCausador as $dados) {
        $agCausador[$dados['cod_ag_causador']] = $dados['nome_agente'];
    }

    $tipoPendencia = array();
    $selectTipoPendencia = $this->medoo->select("tipo_pendencia", ['cod_tipo_pendencia', "nome_tipo_pendencia"]);
    foreach ($selectTipoPendencia as $dados) {
        $tipoPendencia[$dados['cod_tipo_pendencia']] = $dados['nome_tipo_pendencia'];
    }

    $tipoFechamento = array();
    $selectTipoFechamento = $this->medoo->select("tipo_fechamento", ['cod_tipo_fechamento', "nome_fechamento"]);
    foreach ($selectTipoFechamento as $dados) {
        $tipoFechamento[$dados['cod_tipo_fechamento']] = $dados['nome_fechamento'];
    }

    $servico = array();
    $selectServico = $this->medoo->select("servico", ['cod_servico', "nome_servico"]);
    foreach ($selectServico as $dados) {
        $servico[$dados['cod_servico']] = $dados['nome_servico'];
    }

    $unEquipe = array();
    $selectUnEquipe = $this->medoo->select("un_equipe",
        [
            "[><]equipe" => "cod_equipe",
            "[><]unidade" => "cod_unidade"
        ], "*");
    foreach ($selectUnEquipe as $dados) {
        $unEquipe[$dados['cod_un_equipe']] = $dados;
    }

    $codigosOsp = array();

    foreach ($returnPesquisa as $dados => $value){
        $codigosOsp[] = $value['cod_osp'];
    }

    $sql = 'SELECT *, osp.cod_osp, osp.data_abertura AS data_abertura_osp, 
                ssp.data_abertura AS data_abertura_ssp, 
                ssp.data_programada AS data_programada_ssp, 
                ot.descricao AS descricao_tempo, 
                oe.descricao AS descricao_encerramento, 
                os.complemento AS complemento_servico, 
                osp.complemento AS complemento_local, 
                sto.descricao AS descricao_status,
                v.nome_veiculo,
                c.nome_carro,
                mr_o.odometro,
                oe.cod_funcionario
                
	           		FROM osp
            		JOIN status_osp sto USING (cod_ospstatus)
            		JOIN status USING (cod_status)
                    LEFT JOIN osp_encerramento oe ON oe.cod_osp = osp.cod_osp
                    LEFT JOIN osp_registro oreg ON oreg.cod_osp = osp.cod_osp
                    LEFT JOIN osp_servico os ON os.cod_osp = osp.cod_osp
            		LEFT JOIN osp_tempo ot ON ot.cod_osp = osp.cod_osp
            		
            		LEFT JOIN material_rodante_osp mr_o ON mr_o.cod_osp = osp.cod_osp
            		LEFT JOIN carro c ON c.cod_carro = mr_o.cod_carro 
            		LEFT JOIN veiculo v ON v.cod_veiculo = mr_o.cod_veiculo
            		
            		JOIN ssp USING (cod_ssp)';

    if (count($codigosOsp)) {
        $sql = $sql . ' WHERE osp.cod_osp IN(' . implode(' , ', $codigosOsp) . ')';
    }

    $osp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    foreach ($osp as $dados => $value) {
        if(!empty($value['clima'])) {
            switch ($value['clima']){
                case "b":
                    $clima = "Bom";
                    break;
                case "c":
                    $clima = "Chuvoso";
                    break;
                case "n":
                    $clima = "Nublado";
                    break;
                case "e":
                    $clima = "Neblina";
                    break;
            }
        }else
            $clima = '';

        if(!empty($value['tipo_orissp'])) {
            switch ($value['tipo_orissp']){
                case "1":
                    $tipoOs = 'Preventiva';
                    break;
                case "2":
                    $tipoOs = 'Corretiva';
                    break;
                case "3":
                    $tipoOs = 'Programação';
                    break;
                case "4":
                    $tipoOs = 'Serviço';
                    break;
            }
        }else
            $tipoOs = '';

        $txt = $value['cod_osp'] . ":::";
        $txt .= $value['cod_ssp'] . ":::";
        $txt .= utf8_decode($tipoOs) . ":::";
        $txt .= $responsavel[$value['usuario_responsavel']] . ":::";
        $txt .= $this->parse_timestamp($value['data_abertura_osp']) . ":::";
        $txt .= $this->parse_timestamp($value['data_abertura_ssp']) . ":::";
        $txt .= $this->parse_timestamp($value['data_programada_ssp']) . ":::";

        $txt .= $value['nome_status'] . ":::";
        $txt .= $this->parse_timestamp($value['data_status']) . ":::";
        $txt .= $value['descricao_status'] . ":::";
        $txt .= $this->parse_timestamp($value['data_encerramento']) . ":::";

        $txt .= $value['nome_veiculo'] . ":::";
        $txt .= $value['nome_carro'] . ":::";
        $txt .= $value['odometro'] . ":::";

        $txt .= $linha[$value['cod_linha_atuado']] . ":::";
        $txt .= $trecho[$value['trecho_atuado']]['nome_trecho'] . " - " . $trecho[$value['trecho_atuado']]['descricao_trecho'] . ":::";
        $txt .= $pn[$value['cod_ponto_notavel_atuado']] . ":::";
        $txt .= addslashes($value['complemento_local']) . ":::";

        $txt .= $via[$value['cod_via']] . ":::";
        $txt .= $value['km_inicial'] . ":::";
        $txt .= $value['km_final'] . ":::";
        $txt .= $value['posicao_atuado'] . ":::";

        $txt .= $grupo[$value['grupo_atuado']] . ":::";
        $txt .= $sistema[$value['sistema_atuado']] . ":::";
        $txt .= $subsistema[$value['subsistema_atuado']] . ":::";

        $txt .= $servico[$value['cod_servico']] . ":::";
        $txt .= addslashes($value['complemento_servico']) . ":::";

        $txt .= $value['qtd_pessoas'] . ":::";
        $txt .= $value['tempo'] . ":::";
        $txt .= $unidadeTempo[$value['unidade_tempo']] . ":::";
        $txt .= $value['total_servico'] . ":::";
        $txt .= $unidadeMedida[$value['unidade_medida']] . ":::";

        $txt .= $this->parse_timestamp($value['data_recebimento']) . ":::";
        $txt .= atraso($value['atraso_recebimento']) . ":::";

        $txt .= $this->parse_timestamp($value['data_preparacao']) . ":::";
        $txt .= atraso($value['atraso_preparacao']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_pronto']) . ":::";
        $txt .= atraso($value['atraso_ace_pronto']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_saida_autorizada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_saida']) . ":::";
        $txt .= atraso($value['atraso_ace_saida']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_chegada']) . ":::";
        $txt .= atraso($value['atraso_ace_chegada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_exec_inicio']) . ":::";
        $txt .= atraso($value['atraso_exec_inicio']) . ":::";

        $txt .= $this->parse_timestamp($value['data_exec_termino']) . ":::";
        $txt .= atraso($value['atraso_exec_termino']) . ":::";

        $txt .= $this->parse_timestamp($value['data_reg_pronto']) . ":::";
        $txt .= atraso($value['atraso_reg_pronto']) . ":::";

        $txt .= $this->parse_timestamp($value['data_reg_saida_autorizada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_reg_chegada']) . ":::";
        $txt .= atraso($value['atraso_reg_chegada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_desmobilizacao']) . ":::";
        $txt .= addslashes($value['descricao_tempo']) . ":::";

        $txt .= $unEquipe[$value['cod_un_equipe']]['nome_unidade'] . ":::";
        $txt .= $unEquipe[$value['cod_un_equipe']]['nome_equipe'] . ":::";

        $txt .= $causa[$value['cod_causa']] . ":::";
        $txt .= addslashes($value['desc_causa']) . ":::";

        $txt .= $atuacao[$value['cod_atuacao']] . ":::";
        $txt .= addslashes($value['desc_atuacao']) . ":::";

        $txt .= $agCausador[$value['cod_ag_causador']] . ":::";

        $txt .= $clima . ":::";
        $txt .= $value['temperatura'] . ":::";

        $txt .= $veiculo[$value['cod_automovel']] . ":::";

        $txt .= $value['cautela'] . ":::";
        $txt .= $value['km_inicial_cautela'] . ":::";
        $txt .= $value['km_final_cautela'] . ":::";
        $txt .= $value['restricao_veloc'] . ":::";
        $txt .= addslashes($value['dados_complementar']) . ":::";

        $txt .= $tipoFechamento[$value['cod_tipo_fechamento']] . ":::";

        $txt .= $value['liberacao'] . ":::";

        $txt .= $tipoPendencia[$pendencia[$value["cod_pendencia"]]['cod_tipo_pendencia']] . ":::";
        $txt .= $pendencia[$value["cod_pendencia"]]['nome_pendencia'] . ":::";

        $txt .= addslashes($value['descricao_encerramento']) . ":::";

        $txt .= $funcionario[$value['cod_funcionario']] . ":::";

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("OspCompleta.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OspCompleta.csv");
readfile(TEMP_PATH.'OspCompleta.csv');

unlink(TEMP_PATH."OspCompleta.csv");