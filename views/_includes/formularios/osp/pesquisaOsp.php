<div class="page-header">
    <h2>Pesquisa de Ordem de Servi�o Preventivo</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/executarPesquisaOsp" class="form-group" method="post" id="pesquisa">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Osp</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDadosGerais">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#dadosGeraisPesquisa" aria-expanded="false" aria-controls="collapseDadosGerais">
                                        <button class="btn btn-default" type="button"><label>Dados Gerais</label></button>
                                    </a>
                                </div>

                                <div id="dadosGeraisPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['numeroPesquisaOsp']) || !empty($dadosRefill['codigoSsp']) || !empty($dadosRefill['responsavelPreenchimentoPesquisaOsp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingDadosGerais">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>N� OSP</label>
                                                <input class="form-control" name="numeroPesquisaOsp" value="<?php if (!empty($dadosRefill)) echo($dadosRefill['numeroPesquisaOsp']);?>" type="text">
                                            </div>

                                            <div class="col-md-2">
                                                <label>N� SSP</label>
                                                <input class="form-control" name="codigoSsp" value="<?php if (!empty($dadosRefill)) echo($dadosRefill['codigoSsp']);?>" type="text">
                                            </div>

                                            <div class="col-md-4">
                                                <label>Respons�vel pelo Preenchimento</label>
                                                <select class="form-control" name="responsavelPreenchimentoPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectUsuario = $this->medoo->select('usuario', ['cod_usuario', 'usuario'], [
                                                        "OR" => [
                                                            "nivel" => ["2", "5", "5.1"]
                                                        ],
                                                        'ORDER' => 'usuario'
                                                    ]);
                                                    foreach ($selectUsuario as $dados => $value){
                                                        if(!empty($dadosRefill) && $dadosRefill['responsavelPreenchimentoPesquisaOsp'] == $value['cod_usuario'] )
                                                            echo ("<option value='".$value['cod_usuario']."' selected >".$value['usuario']."</option>");
                                                        else
                                                            echo ("<option value='".$value['cod_usuario']."'>".$value['usuario']."</option>");
                                                    }

                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDatas">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#datasPesquisa" aria-expanded="false"
                                       aria-controls="collapseDatas">
                                        <button class="btn btn-default" type="button"><label>Datas e Status</label>
                                        </button>
                                    </a>
                                </div>

                                <div id="datasPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['dataPartirOspAbertura']) || !empty($dadosRefill['dataAteOspAbertura']) ||
                                        !empty($dadosRefill['dataPartirOspEncerramento']) || !empty($dadosRefill['dataAteOspEncerramento']) ||
                                        !empty($dadosRefill['dataPartirOsp']) || !empty($dadosRefill['dataAteOsp']) || !empty($dadosRefill['situacaoPesquisaOsp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingDatas">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Abertura (a Partir)</label>
                                                <input class="form-control data validaData" name="dataPartirOspAbertura"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataPartirOspAbertura']); ?>"
                                                       type="text">
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input class="form-control data validaData" name="dataAteOspAbertura"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataAteOspAbertura']); ?>"
                                                       type="text">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Encerramento (a Partir)</label>
                                                <input class="form-control data validaData" name="dataPartirOspEncerramento"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataPartirOspEncerramento']); ?>"
                                                       type="text">
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input class="form-control data validaData" name="dataAteOspEncerramento"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataAteOspEncerramento']); ?>"
                                                       type="text">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Status</label>
                                                <select class="form-control" name="situacaoPesquisaOsp">
                                                    <option value=''>Todos</option>
                                                    <option value='10' <?php if($dadosRefill['situacaoPesquisaOsp'] == "10") echo("selected") ?>>Execu��o</option>
                                                    <option value='11' <?php if($dadosRefill['situacaoPesquisaOsp'] == "11") echo("selected") ?>>Encerrada</option>
                                                    <option value='23' <?php if($dadosRefill['situacaoPesquisaOsp'] == "23") echo("selected") ?>>N�o Executado</option>
                                                    <option value='24' <?php if($dadosRefill['situacaoPesquisaOsp'] == "24") echo("selected") ?>>Duplicado</option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Data do Status (a Partir)</label>
                                                <input class="form-control data validaData" name="dataPartirOsp"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataPartirOsp']); ?>"
                                                       type="text">
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input class="form-control data validaData" name="dataAteOsp"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataAteOsp']); ?>"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingLocal">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#localPesquisa" aria-expanded="false" aria-controls="collapseLocal">
                                        <button class="btn btn-default" type="button"><label>Local Atuado</label></button>
                                    </a>
                                </div>

                                <div id="localPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['linhaPesquisaOsp']) || !empty($dadosRefill['trechoPesquisaOsp']) || !empty($dadosRefill['pnPesquisaOsp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingServico">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Linha Atuada</label>
                                                <select class="form-control" name="linhaPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectLinha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);
                                                    foreach($selectLinha as $dados => $value){
                                                        if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaOsp'] == $value['cod_linha'] )
                                                            echo('<option value="'.$value['cod_linha'].'" selected>'.$value['nome_linha'].'</option>');
                                                        else
                                                            echo('<option value="'.$value['cod_linha'].'">'.$value['nome_linha'].'</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Trecho Atuado</label>
                                                <select class="form-control" name="trechoPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if(!empty($dadosRefill['linhaPesquisaOsp'])){
                                                        $selectTrecho = $this->medoo->select('trecho', ['cod_trecho', 'nome_trecho', 'descricao_trecho'],['cod_linha' => (int) $dadosRefill['linhaPesquisaOsp']]);
                                                    }else{
                                                        $selectTrecho = $this->medoo->select('trecho', ['cod_trecho', 'nome_trecho', 'descricao_trecho']);
                                                    }

                                                    foreach($selectTrecho as $dados => $value){
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaOsp'] == $value['cod_trecho'] )
                                                            echo('<option value="'.$value['cod_trecho'].'" selected>'.$value['nome_trecho'].' - '.$value['descricao_trecho'].'</option>');
                                                        else
                                                            echo('<option value="'.$value['cod_trecho'].'">'.$value['nome_trecho'].' - '.$value['descricao_trecho'].'</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Ponto Not�vel Atuado</label>
                                                <select class="form-control" name="pnPesquisaOsp">
                                                    <option value="">Pontos Not�veis</option>
                                                    <?php
                                                    if($dadosRefill['linhaPesquisaOsp'] != "" && $dadosRefill['trechoPesquisaOsp'] == ""){

                                                    }else{
                                                        if (!empty($dadosRefill) && $dadosRefill['trechoPesquisaOsp'] != "")
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel','nome_ponto'], ["cod_trecho" => (int)$dadosRefill['trechoPesquisaOsp']]);
                                                        else
                                                            $selectPn = $this->medoo->select("ponto_notavel", ['cod_ponto_notavel','nome_ponto'], ["ORDER" => "cod_trecho"]);

                                                        foreach ($selectPn as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['pnPesquisaOsp'] == $value['cod_ponto_notavel'])
                                                                echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingServico">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#servicoPesquisa" aria-expanded="false" aria-controls="collapseServico">
                                        <button class="btn btn-default" type="button"><label>Servi�o</label></button>
                                    </a>
                                </div>

                                <div id="servicoPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['grupoPesquisaOsp']) || !empty($dadosRefill['sistemaPesquisaOsp']) || !empty($dadosRefill['subSistemaPesquisaOsp']) ||
                                        !empty($dadosRefill['servicoPesquisaOsp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingServico">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grupo Sistema</label>
                                                <?php
                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ORDER" => "nome_grupo"]);
                                                $this->form->getSelectGrupo($dadosRefill['grupoPesquisaOsp'], $grupo, 'grupoPesquisaOsp', true);
                                                ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sistema</label>
                                                <select class="form-control" name="sistemaPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if ($dadosRefill['grupoPesquisaOsp'])
                                                        $selectSistema = $this->medoo->select("grupo_sistema",["[><]sistema" => "cod_sistema"], ['cod_sistema','nome_sistema'], ["cod_grupo" => $dadosRefill['grupoPesquisaOsp']]);
                                                    else
                                                        $selectSistema = $this->medoo->select("sistema", ['cod_sistema','nome_sistema']);

                                                    foreach ($selectSistema as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisaOsp'] == $value['cod_sistema'])
                                                            echo('<option value="' . $value['cod_sistema'] . '" selected>' . $value['nome_sistema'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_sistema'] . '">' . $value['nome_sistema'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sub-Sistema</label>
                                                <select class="form-control" name="subSistemaPesquisa">
                                                    <option value="">Sub-Sistemas</option>
                                                    <?php
                                                    if($dadosRefill['grupoPesquisaOsm'] != "" && $dadosRefill['sistemaPesquisaOsm'] == ""){

                                                    }else{
                                                        if(!empty($dadosRefill['sistemaPesquisaOsm']))
                                                            $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"],['cod_subsistema','nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisaOsm'] ]);
                                                        else
                                                            $subsistema = $this->medoo->select("subsistema", ['cod_subsistema','nome_subsistema']);

                                                        foreach ($subsistema as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisa'] == $value['cod_subsistema'])
                                                                echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Servi�o</label>
                                                <select class="form-control" name="servicoPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectServico = $this->medoo->select('servico', ['nome_servico', 'cod_servico'] ,['ORDER'=>'nome_servico']);
                                                    foreach($selectServico as $dados => $value){
                                                        if (!empty($dadosRefill) && $dadosRefill['servicoPesquisaOsp'] == $value['cod_servico'] )
                                                            echo('<option value="'.$value['cod_servico'].'" selected>'.$value['nome_servico'].'</option>');
                                                        else
                                                            echo('<option value="'.$value['cod_servico'].'">'.$value['nome_servico'].'</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">

                                <div class="panel-heading" role="tab" id="headingMaquinasEquipamentos">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#maquinasEquipamentosPesquisa" aria-expanded="false"
                                       aria-controls="collapseMaquinasEquipamentos">
                                        <button class="btn btn-default" type="button"><label>M�quinas Equipamentos e
                                                Materiais</label></button>
                                    </a>
                                </div>

                                <div id="maquinasEquipamentosPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['maquinasEquipamentosPesquisaOsp']) || !empty($dadosRefill['materiaisPesquisaOsp']) ||
                                        !empty($dadosRefill['estadoPesquisaOsp']) || !empty($dadosRefill['origemPesquisaOsp'])
                                    ) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingMaquinasEquipamentos">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>M�quinas e Equipamentos</label>
                                                <input id="maquinasEquipamentosPesquisaOsp" type="hidden" name="maquinasEquipamentosPesquisaOsp">
                                                <input id="maquinasPesquisa" class="form-control"
                                                       placeholder="Selecione uma M�quina ou Equipamento"
                                                       value="" required list="maquinaDataList" autocomplete="off">

                                                <datalist id="maquinaDataList">
                                                    <?php
                                                    $selectMaquinas = $this->medoo->select("v_material", ['nome_material', 'descricao_material', 'cod_material'], [
                                                        "cod_categoria" => 10, "ORDER" => "nome_material"]);

                                                    foreach ($selectMaquinas as $dados) {
                                                        echo strtoupper("<option id='{$dados['cod_material']}' value='{$dados['nome_material']} - {$dados['descricao_material']}'/>");
                                                    }
                                                    ?>
                                                </datalist>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Materiais</label>
                                                <input id="materiaisPesquisaOsp" type="hidden" name="materiaisPesquisaOsp">
                                                <input id="materiaisPesquisa" name="materiaisPesquisa" class="form-control"
                                                       placeholder="Selecione um Material"
                                                       value="" required list="materialDataList" autocomplete="off">

                                                <datalist id="materialDataList">
                                                    <option value=""></option>
                                                    <?php
                                                    $selectMateriais = $this->medoo->select("v_material", ['nome_material', 'descricao_material', 'cod_material'], ["ORDER" => "nome_material", "cod_categoria" => "32"]);

                                                    foreach ($selectMateriais as $dados) {
                                                        echo strtoupper("<option id='{$dados['cod_material']}' value='{$dados['nome_material']} - {$dados['descricao_material']}'/>");
                                                    }
                                                    ?>
                                                </datalist>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Estado</label>
                                                <select class="form-control" name="estadoPesquisaOsp">
                                                    <?php
                                                    switch ($dadosRefill['estadoPesquisaOsp']) {
                                                        case '':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n">Novo</option>');
                                                            echo('<option value="u">Usado</option>');
                                                            break;
                                                        case 'n':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n" selected>Novo</option>');
                                                            echo('<option value="u">Usado</option>');
                                                            break;
                                                        case 'u':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n">Novo</option>');
                                                            echo('<option value="u" selected>Usado</option>');
                                                            break;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Origem</label>
                                                <select class="form-control" name="origemPesquisaOsp">
                                                    <?php
                                                    switch ($dadosRefill['origemPesquisaOsp']) {
                                                        case '':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 's':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s" selected>MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 'f':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f" selected>MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 'o':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o" selected>Outros</option>');
                                                            break;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingRegistroExecucao">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#registroExecucaoPesquisa" aria-expanded="false" aria-controls="collapseRegistroExecucao">
                                        <button class="btn btn-default" type="button"><label>Registro de Execu��o</label></button>
                                    </a>
                                </div>

                                <div id="registroExecucaoPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['unidadePesquisaOsp']) || !empty($dadosRefill['equipePesquisaOsp']) ||
                                        !empty($dadosRefill['causaPesquisaOsp']) || !empty($dadosRefill['agCausadorPesquisaOsp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingRegistroExecucao">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Unidade</label>
                                                <select class="form-control" name="unidadePesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selecUnidade = $this->medoo->select('unidade', ['cod_unidade', 'nome_unidade']);
                                                    foreach($selecUnidade as $dados => $value){
                                                        if( !empty($dadosRefill) &&  $dadosRefill['unidadePesquisaOsp'] == $value['cod_unidade']){
                                                            echo('<option value="'.$value['cod_unidade'].'" selected>'.$value['nome_unidade'].'</option>');
                                                        }else{
                                                            echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Equipe</label>
                                                <select class="form-control" name="equipePesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if(!empty($dadosRefill['unidadePesquisaOsp'])){
                                                        $selectEquipe = $this->medoo->select("un_equipe",["[><]equipe" => "cod_equipe"], ['sigla',"nome_equipe"], ["cod_unidade" => $dadosRefill['unidadePesquisaOsp']]);
                                                    }else{
                                                        $selectEquipe = $this->medoo->select("equipe", ['sigla',"nome_equipe"]);
                                                    }

                                                    foreach($selectEquipe as $dados => $value){
                                                        if( !empty($dadosRefill) &&  $dadosRefill['equipePesquisaOsp'] == $value['sigla']){
                                                            echo('<option value="'.$value['sigla'].'" selected>'.$value['nome_equipe'].'</option>');
                                                        }else{
                                                            echo('<option value="'.$value['sigla'].'">'.$value['nome_equipe'].'</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Causa</label>
                                                <select class="form-control" name="causaPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selecCausa = $this->medoo->select('causa', ['cod_causa', 'nome_causa'], ["ORDER" => 'nome_causa']);
                                                    foreach($selecCausa as $dados => $value){
                                                        if( !empty($dadosRefill) &&  $dadosRefill['causaPesquisaOsp'] == $value['cod_causa']){
                                                            echo('<option value="'.$value['cod_causa'].'" selected>'.$value['nome_causa'].'</option>');
                                                        }else{
                                                            echo('<option value="'.$value['cod_causa'].'">'.$value['nome_causa'].'</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label>Causador</label>
                                                <select class="form-control" name="agCausadorPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selecAgCausador = $this->medoo->select('agente_causador', ['cod_ag_causador', 'nome_agente'], ["ORDER" => 'nome_agente']);
                                                    foreach($selecAgCausador as $dados => $value){
                                                        if( !empty($dadosRefill) &&  $dadosRefill['agCausadorPesquisaOsp'] == $value['cod_ag_causador']){
                                                            echo('<option value="'.$value['cod_ag_causador'].'" selected>'.$value['nome_agente'].'</option>');
                                                        }else{
                                                            echo('<option value="'.$value['cod_ag_causador'].'">'.$value['nome_agente'].'</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFechamento">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#fechamentoPesquisa" aria-expanded="false" aria-controls="collapseFechamento">
                                        <button class="btn btn-default" type="button"><label>Fechamento</label></button>
                                    </a>
                                </div>

                                <div id="fechamentoPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['tipoFechamentoPesquisaOsp']) || !empty($dadosRefill['tipoPendenciaPesquisaOsp']) ||
                                        !empty($dadosRefill['liberacaoPesquisaOsp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingFechamento">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <label>Pend�ncia</label>
                                                <select class="form-control" name="tipoPendenciaPesquisaOsp">
                                                    <option value="">Todas</option>
                                                    <?php
                                                    $selectPendencia = $this->medoo->select('pendencia', ['cod_pendencia', 'nome_pendencia'], ['ORDER' => 'nome_pendencia']);
                                                    foreach($selectPendencia as $dados => $value){
                                                        if( !empty($dadosRefill) &&  $dadosRefill['tipoPendenciaPesquisaOsp'] == $value['cod_pendencia']){
                                                            echo('<option value="'.$value['cod_pendencia'].'" selected>'.$value['nome_pendencia'].'</option>');
                                                        }else{
                                                            echo('<option value="'.$value['cod_pendencia'].'">'.$value['nome_pendencia'].'</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Tipo de Fechamento</label>
                                                <select class="form-control" name="tipoFechamentoPesquisaOsp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectTipoFechamento = $this->medoo->select('tipo_fechamento', ['cod_tipo_fechamento', 'nome_fechamento']);
                                                    foreach($selectTipoFechamento as $dados => $value){
                                                        if( !empty($dadosRefill) &&  $dadosRefill['tipoFechamentoPesquisaOsp'] == $value['cod_tipo_fechamento']){
                                                            echo('<option value="'.$value['cod_tipo_fechamento'].'" selected>'.$value['nome_fechamento'].'</option>');
                                                        }else{
                                                            echo('<option value="'.$value['cod_tipo_fechamento'].'">'.$value['nome_fechamento'].'</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Libera��o</label>
                                                <select class="form-control" name="liberacaoPesquisaOsp">
                                                    <option value="">Ambos</option>
                                                    <option value="s" <?php if($dadosRefill['liberacaoPesquisaOsp'] == "s") echo("selected") ?>>Sim</option>
                                                    <option value="n" <?php if($dadosRefill['liberacaoPesquisaOsp'] == "n") echo("selected") ?>>N�o</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default mtRod" <?php if (!empty($dadosRefill['grupoPesquisaOsp'])){} else{ ?> hidden <?php } ?> >
                                <div class="panel-heading" role="tab" id="headingMaterialRodante">
                                    <a href="#materialRodantePesquisaOsp" class="collapsed" role="button"
                                       data-toggle="collapse" aria-expanded="false"
                                       aria-controls="collapseMaterialRodante">
                                        <button class="btn btn-default"><label>Material Rodante</label></button>
                                    </a>
                                </div>

                                <div id="materialRodantePesquisaOsp" role="tabpanel"
                                     aria-labelledby="headingMaterialRodante"
                                     class="panel-collapse collapse <?php echo (!empty($dadosRefill['veiculoPesquisaOsp']) || !empty($dadosRefill['carroAvariadoPesquisaOsp']) || !empty($dadosRefill['carroLiderPesquisaOsm']) || !empty($dadosRefill['odometroPartirPesquisaOsm']) || !empty($dadosRefill['odometroAtePesquisaOsm'])) ? 'in' : 'out'; ?>">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Ve�culo</label>
                                                <select name="veiculoPesquisaOsp" class="form-control">
                                                    <option value="">Ve�culo</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoPesquisaOsp'])){
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo'], ['cod_grupo' => $dadosRefill['grupoPesquisaOsp']]);
                                                    } else {
                                                        $veiculo = $this->medoo->select("veiculo", ['cod_veiculo', 'nome_veiculo']);
                                                    }

                                                    foreach ($veiculo as $dados => $value) {
                                                        if ($dadosRefill['veiculoPesquisaOsp'] == $value['cod_veiculo'])
                                                            echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Carro Avariado</label>
                                                <select name="carroAvariadoPesquisaOsp" class="form-control">
                                                    <option value="">Carro Avariado</option>
                                                    <?php
                                                    if (!empty($dadosRefill['grupoPesquisaOsp']) || !empty($dadosRefill['veiculoPesquisaOsp'])){
                                                        if (!empty($dadosRefill['grupoPesquisaOsp'])){
                                                            if (!empty($dadosRefill['veiculoPesquisaOsp'])){
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['AND' => ['cod_grupo' => $dadosRefill['grupoPesquisaOsp'], 'cod_veiculo' => $dadosRefill['veiculoPesquisaOsp']]]);
                                                            } else {
                                                                $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_grupo' => $dadosRefill['grupoPesquisaOsp']]);
                                                            }
                                                        } else {
                                                            $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro'], ['cod_veiculo' => $dadosRefill['veiculoPesquisaOsp']]);
                                                        }
                                                    } else {
                                                        $carro = $this->medoo->select("carro", ['cod_carro', 'nome_carro']);
                                                    }

                                                    foreach ($carro as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['carroAvariadoPesquisaOsp'] == $value['cod_carro'])
                                                            echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Prefixo</label>
                                                <select name="prefixoPesquisaOsp" class="form-control">
                                                    <option value="">Prefixo</option>
                                                    <?php
                                                    $prefixo = $this->medoo->select("prefixo", ['cod_prefixo', 'nome_prefixo']);

                                                    foreach ($prefixo as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['prefixoPesquisaOsp'] == $value['cod_prefixo'])
                                                            echo("<option value='{$value['cod_prefixo']}' selected>{$value['nome_prefixo']}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_prefixo']}'>{$value['nome_prefixo']}</option>");
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Od�metro a partir</label>
                                                <input name='odometroPartirPesquisaOsp' class='form-control'
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroPartirPesquisaOsp']; ?>"/>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Od�metro at�</label>
                                                <input name="odometroAtePesquisaOsp" class="form-control"
                                                       value="<?php if (!empty($dadosRefill)) echo $dadosRefill['odometroAtePesquisaOsp']; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
                            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro</button>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 1em"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Resultado da Pesquisa
                                    <strong>Total Encontrado: <?php echo (!empty($returnPesquisa))? count($returnPesquisa): 0; ?></strong>
                                </div>

                                <div class="panel-body">
                                    <table id="resultadoPesquisaOsp" class="table table-striped table-bordered no-footer">
                                        <thead id="headIndicadorOsp">
                                        <tr role="row">
                                            <th>N� Osp</th>
                                            <th>N� Ssp</th>
                                            <th>Local</th>
                                            <th>Data de Abertura</th>
                                            <th>Servi�o</th>
                                            <th>Status</th>
                                            <th>A��o</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (!empty($returnPesquisa)) {
                                            foreach ($returnPesquisa as $dados => $value) {
                                                echo('<tr>');
                                                echo('<td>' . $value['cod_osp'] . '</td>');
                                                echo("<td><span class='primary-info' id='codSsp'>{$value['cod_ssp']}</span><br /><span class='sub-info'>{$value['nome_fechamento']}</span></td>");
                                                echo("<td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>");
                                                echo('<td>' . $this->parse_timestamp($value['data_abertura_osp']) . '</td>');
                                                echo('<td><span class="primary-info" rel="tooltip-wrapper" data-placement="right" data-title="'.$value['complemento_servico'].'">'.$value['nome_servico'] . '</span></td>');
                                                echo('<td>' . $value['nome_status'] . '</td>');
                                                echo("<td>{$btnAcoes}</td>");
                                                echo('</tr>');
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>