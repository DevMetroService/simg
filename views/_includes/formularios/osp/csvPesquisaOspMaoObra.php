<?php

$conteudo = "N. OSP;";
$conteudo .= "Nome Funcionario;";
$conteudo .= "Data Inicio;";
$conteudo .= "Data Final;";
$conteudo .= "Total Horas\n";

$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)) {
    $responsavel = array();
    $selectResponsavel = $this->medoo->select("funcionario", ["matricula", "nome_funcionario"]);
    foreach ($selectResponsavel as $dados){
        $responsavel[$dados['matricula']] = $dados['nome_funcionario'];
    }

    foreach ($returnPesquisa as $d => $v) {
        $selectMaoObra = $this->medoo->select('osp_mao_de_obra','*',["cod_osp" => (int)$v['cod_osp']]);

        foreach ($selectMaoObra as $dados => $value) {
            $txt = $value['cod_osp'] . ":::";
            $txt .= $responsavel[$value['matricula']] . ":::";
            $txt .= $value['data_inicio'] . ":::";
            $txt .= $value['data_termino'] . ":::";
            $txt .= $value['total_hrs'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("OspMaoObra.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OspMaoObra.csv");
readfile(TEMP_PATH.'OspMaoObra.csv');

unlink(TEMP_PATH."OspMaoObra.csv");