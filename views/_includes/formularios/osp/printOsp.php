<?php
if (!empty($_SESSION['refillOs'])) {
    $osp = $this->medoo->query("
SELECT mro.odometro as odometro_registro, * FROM osp
LEFT JOIN grupo ON (grupo.cod_grupo = osp.grupo_atuado)
LEFT JOIN sistema ON(osp.sistema_atuado = sistema.cod_sistema)
LEFT JOIN linha ON(osp.cod_linha_atuado = linha.cod_linha)
LEFT JOIN trecho ON(osp.trecho_atuado = trecho.cod_trecho)
LEFT JOIN ponto_notavel ON(osp.cod_ponto_notavel_atuado = ponto_notavel.cod_ponto_notavel)
LEFT JOIN via USING(cod_via)
JOIN osp_servico USING(cod_osp)
JOIN servico USING(cod_servico)
LEFT JOIN material_rodante_osp mro USING(cod_osp)
LEFT JOIN veiculo USING(cod_veiculo)
LEFT JOIN carro USING(cod_carro)
LEFT JOIN usuario ON (osp.usuario_responsavel = usuario.cod_usuario)
WHERE cod_osp = {$_SESSION['refillOs']['codigoOs']}")->fetchAll(PDO::FETCH_ASSOC);
    $osp = $osp[0];

    $status = $this->medoo->select("status_osp", ["[><]status" => "cod_status"], "*", ["cod_ospstatus" => (int)$osp['cod_ospstatus']]);
    $status = $status[0];;

    $tempo = $this->medoo->select('osp_tempo','*',["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $tempo = $tempo[0];

    $maoObra = $this->medoo->select("osp_mao_de_obra", ["[><]funcionario" => "cod_funcionario"], "*",["cod_osp" => (int) $_SESSION['refillOs']['codigoOs']]);

    $registro = $this->medoo->select("osp_registro", [
        "[>]un_equipe" => "cod_un_equipe",
        "[>]unidade" => "cod_unidade",
        "[>]equipe" => "cod_equipe",
        "[>]causa" => "cod_causa",
        "[>]atuacao" => "cod_atuacao",
        "[>]agente_causador" => "cod_ag_causador"
    ],"*",["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $registro = $registro[0];

    $maquinas = $this->medoo->select("osp_maquina", ["[><]material" => "cod_material"],
        [
            "cod_material",
            "nome_material",
            "qtd",
            "material.descricao(descricao_material)",
            "total_hrs",
            "num_serie",
            "osp_maquina.descricao"
        ], [ "cod_osp" => $_SESSION['refillOs']['codigoOs']]);

    $materiais = $this->medoo->query("SELECT
                                        *
                                    FROM osp_material ospm
                                    JOIN material USING (cod_material)
                                    JOIN unidade_medida um ON(ospm.unidade = um.cod_uni_medida)
                                    JOIN osp USING (cod_osp)
                                    WHERE cod_osp = {$_SESSION['refillOs']['codigoOs']}
                                    ORDER BY nome_material")->fetchAll(PDO::FETCH_ASSOC);

    $encerramento = $this->medoo->select("osp_encerramento",
        [
            "[>]funcionario" => "cod_funcionario",
            "[>]tipo_fechamento" => "cod_tipo_fechamento",
            "[>]pendencia" => "cod_pendencia",
            "[>]tipo_pendencia" => "cod_tipo_pendencia"
        ], "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $encerramento = $encerramento[0];

    $ssp = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$osp['cod_ssp']]);
    $ssp = $ssp[0];
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>OSP - Ordem de Servi�o Programado</h3>
    </div>
    <hr/>
    <div class="title-module">Dados Gerais</div>
    <table>
        <tr>
            <th>OSP</th>
            <th>SSP</th>
            <th>Data/Hora Abertura</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osp['cod_osp'];?></td>
            <td class="center"><?php echo $osp['cod_ssp'];?></td>
            <td class="center"><?php echo MainController::parse_timestamp_static($osp['data_abertura']);?></td>
        </tr>
        <tr>
            <th colspan="2">Equipe</th>
            <th>Grupo/Sistema</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $ssp['sigla_equipe'] . " / " . $ssp['nome_equipe'];?></td>
            <td class="center"><?php
                echo($ssp['grupo_sistema'] . $ssp['sigla_sistema'] . " / " . $ssp['nome_grupo'] . " - " . $ssp['nome_sistema']);?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Origem</div>
    <table>
        <tr>
            <th>Matr�cula</th>
            <th>Nome Completo</th>
            <th>Contato</th>
        </tr>
        <tr>
            <td><?php if (!empty($osp)) {
                    echo $ssp['matricula'];
                } ?></td>
            <td><?php if (!empty($osp)) {
                    echo $ssp['nome_origem'];
                } ?></td>
            <td><?php if (!empty($osp)) {
                    echo $ssp['contato_origem'];
                } ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o</div>
    <table>
        <tr>
            <th colspan="2">Grupo de Sistema Atuado</th>
            <th colspan="2">Sistema Atuado</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $osp['nome_grupo'];?></td>
            <td colspan="2" class="center"><?php echo $osp['nome_sistema'];?></td>
        </tr>

        <tr>
            <th colspan="4">SubSistema Atuado</th>
        </tr>
        <tr>
            <td colspan="4" class="center"><?php echo $osp['nome_subsistema'];?></td>
        </tr>
        <tr>
            <th>Linha</th>
            <th colspan="2">Trecho</th>
            <th>Ponto Not�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osp['nome_linha'];?></td>
            <td colspan="2" class="center">
                <?php if (!empty($osp)) {
                    echo $osp['nome_trecho'] . ' - ' . $osp['descricao_trecho'];
                } ?>
            </td>
            <td class="center">
                <?php if (!empty($osp)) {
                    echo $osp['nome_ponto'];
                } ?>
            </td>
        </tr>
        <tr>
            <th colspan="4">Complemento</th>
        </tr>
        <tr>
            <td colspan="4"><?php echo $osp['complemento_local'];?></td>
        </tr>
        <tr>
            <th>Via</th>
            <th>Km Inicial</th>
            <th>Km Final</th>
            <th>Posi��o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osp['nome_via'];?></td>
            <td class="center"><?php echo $osp['km_inicial'] ?></td>
            <td class="center"><?php echo $osp['km_final']; ?></td>
            <td class="center"><?php echo $osp['posicao_atuado']; ?></td>
        </tr>
    </table>

    <?php if (!empty($osp['nome_veiculo'])) { ?>
        <div class="title-table">&raquo; Material Rodante</div>
        <table>
            <tr>
                <th>Ve�culo</th>
                <th>Carro</th>
                <th>Odometro</th>
            </tr>
            <tr>
                <td>
                    <?php echo $osp['nome_veiculo']; ?>
                </td>
                <td>
                    <?php echo $osp['nome_carro']; ?>
                </td>
                <td>
                    <?php echo $osp['odometro_registro']; ?>
                </td>
            </tr>
        </table>
    <?php } ?>

    <div class="title-table">&raquo; Servi�o a Ser Executado</div>
    <table>
        <tr>
            <th colspan="3">Descri��o do Servi�o</th>
        </tr>
        <tr>
            <td colspan="3"><?php echo $osp['nome_servico']; ?></td>
        </tr>
        <tr>
            <th colspan="3">Complemento</th>
        </tr>
        <tr>
            <td colspan="3"><?php echo $osp['complemento']; ?></td>
        </tr>
        <tr>
            <th>Efetivo</th>
            <th>Tempo</th>
            <th>Total Servi�o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osp['qtd_pessoas']; ?></td>
            <td class="center"><?php echo $osp['tempo']; ?></td>
            <td class="center"><?php echo $osp['total_servico']; ?></td>
        </tr>
    </table>

    <?php if(!empty($status['descricao'])) { ?>
        <div class="title-table">&raquo; Situa��o da Solicita��o</div>
        <table>
            <tr>
                <th>Status</th>
                <th>Descri��o</th>
            </tr>
            <tr>
                <td><?php echo $status['nome_status'];?></td>
                <td><?php echo $status['descricao'];?></td>
            </tr>
        </table>
    <?php } ?>

    <table class="footer">
        <tr>
            <th>Respons�vel</th>
            <th>Situa��o</th>
            <th>Condi��o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osp['usuario']; ?></td>
            <td class="center"><?php echo $status['nome_status']; ?></td>
            <td class="center"><?php echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado'; ?></td>
        </tr>
    </table>

    <?php
    if (!empty($tempo)) {
        ?>
        <div class="title-module">Tempos Totais</div>

        <div class="title-table">&raquo; Prepara�ao e Acesso</div>
        <table>
            <tr>
                <th>In�cio</th>
                <th>Pronto para Sair</th>
                <th>Sa�da Autorizada</th>
                <th>Sa�da</th>
                <th>Chegada</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_preparacao']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_pronto']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_saida_autorizada']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_saida']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_chegada']);}
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Execu��o</div>
        <table>
            <tr>
                <th>In�cio</th>
                <th>T�rmino</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_exec_inicio']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_exec_termino']);}
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Regresso</div>
        <table>
            <tr>
                <th>Pronto para Sair</th>
                <th>Sa�da Autorizada</th>
                <th>Chegada</th>
                <th>Desmobiliza��o</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_reg_pronto']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_reg_saida_autorizada']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_reg_chegada']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_desmobilizacao']);}
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Observa��o</div>
        <table>
            <tr>
                <td><?php echo $tempo['descricao']; ?></td>
            </tr>
        </table>
        <?php
    }

    if (!empty($maoObra)) {
        echo '<div class="title-module">M�o de Obra</div>
                    <table>
                    <tr>
                        <th>Matr�cula</th>
                        <th>Nome</th>
                        <th>In�cio</th>
                        <th>T�rmino</th>
                    </tr>';

        foreach ($maoObra as $dados => $value){
            ?>
            <tr>
                <td class="center">
                    <?php
                    echo $value['matricula'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $value['nome_funcionario'];
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $value['data_inicio'];
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $value['data_termino'];
                    ?>
                </td>
            </tr>
            <?php
        }
        echo '</table>';
    }

    if (!empty($registro)) {
        ?>
        <div class="title-module">Registro de Execu��o</div>

        <table>
            <tr>
                <th colspan="2">Local</th>
                <th colspan="2">Equipe</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo $registro['nome_unidade']; ?></td>
                <td colspan="2" class="center"><?php echo $registro['sigla'] . ' - ' . $registro['nome_equipe']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Causa</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['nome_causa']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o da Causa</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['desc_causa']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Atua��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['nome_atuacao'];?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o da Atua��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['desc_atuacao']; ?></td>
            </tr>
            <tr>
                <th colspan="2">Ag.Causador</th>
                <th>Clima</th>
                <th>Temperatura</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo $registro['nome_agente']; ?></td>
                <td class="center"><?php
                    switch ($registro['clima']){
                        case "b":
                            echo "Bom";
                            break;
                        case "c":
                            echo "Chuvoso";
                            break;
                        case "n":
                            echo "Nublado";
                            break;
                        case "e":
                            echo "Neblina";
                            break;
                    }
                    ?></td>
                <td class="center"><?php echo $registro['temperatura']; ?></td>
            </tr>
            <tr>
                <th>Cautela</th>
                <th>Km Inicial</th>
                <th>Km Final</th>
                <th>Restringir Velocidade</th>
            </tr>
            <tr>
                <td class="center"><?php echo $registro['cautela']; ?></td>
                <td class="center"><?php echo $registro['km_inicial_cautela']; ?></td>
                <td class="center"><?php echo $registro['km_final_cautela'];?></td>
                <td class="center"><?php echo $registro['restricao_veloc'];?></td>
            </tr>
            <tr>
                <th>Hor�metro Tra��o MA</th>
                <th>Hor�metro Gerador MA</th>
                <th>Hor�metro Tra��o MB</th>
                <th>Hor�metro Gerador MB</th>
            </tr>
            <tr>
                <td class="center"><?php echo $osp['horimetro_tracao_ma']; ?></td>
                <td class="center"><?php echo $osp['horimetro_gerador_ma']; ?></td>
                <td class="center"><?php echo $osp['horimetro_tracao_mb']; ?></td>
                <td class="center"><?php echo $osp['horimetro_gerador_mb']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['dados_complementar']; ?></td>
            </tr>
        </table>
        <?php
    }

    if (!empty($maquinas)) {
        echo '<div class="title-module">M�quinas e Equipamentos Utilizados</div>';

        foreach ($maquinas as $dados => $value) {
            ?>
            <table>
                <tr>
                    <th>C�digo</th>
                    <th colspan="2">Nome</th>
                    <th>Quantidade</th>
                </tr>
                <tr>
                    <td class="center"><?php echo $value['cod_material'];?></td>
                    <td colspan="2"><?php echo $value['nome_material'];?></td>
                    <td class="center"><?php echo $value['qtd'];?></td>
                </tr>
                <tr>
                    <th colspan="4">Complemento</th>
                </tr>
                <tr>
                    <td colspan="4"><?php echo $value['descricao_material'];?></td>
                </tr>
                <tr>
                    <th colspan="2">Total de Hora</th>
                    <th colspan="2">N�mero de S�rie</th>
                </tr>
                <tr>
                    <td colspan="2" class="center"><?php echo $value['total_hrs']; ?></td>
                    <td colspan="2" class="center"><?php echo $value['num_serie'];?></td>
                </tr>
                <tr>
                    <th colspan="4">Descri��o</th>
                </tr>
                <tr class="tr-destaque">
                    <td colspan="4"><?php echo $value['descricao'];?></td>
                </tr>
            </table>
            <?php
        }
    }

    if (!empty($materiais)) {
        echo ' <div class="title-module">Materiais Utilizados</div><table>';

        foreach ($materiais as $dados => $value) {
            ?>
            <tr>
                <th>C�digo</th>
                <th>Nome</th>
                <th>Quantidade</th>
            </tr>
            <tr>
                <td class="center"><?php echo "{$value['cod_material']}"; ?></td>
                <td><?php echo "{$value['nome_material']} - {$value['descricao']}";?></td>
                <td class="center"><?php echo $value['utilizado'];?></td>
            </tr>
            <tr>
                <th>Estado</th>
                <th>Origem</th>
                <th>Unidade de Medida</th>
            </tr>
            <tr class="tr-destaque">
                <td class="center">
                    <?php
                    switch($value['estado']){
                        case "n":
                            echo 'Novo';
                            break;
                        case "u":
                            echo 'Usado';
                            break;
                        case "r":
                            echo 'Reparado';
                            break;
                        default:
                            echo 'ERROR, contatar TI.';
                            break;
                    }
                    ?></td>
                <td class="center"><?php
                    switch ($value['origem']){
                        case 's':
                            echo "MetroService";
                            break;
                        case 'f':
                            echo "MetroFor";
                            break;
                        default:
                            echo "Outros";
                            break;
                    }
                    ?></td>
                <td class="center"><?php
                    echo $value['sigla_uni_medida'] . ' - ' . $value['nome_uni_medida'];
                    ?></td>
            </tr>
            <?php
        }
        echo '</table>';
    }

    if (!empty($encerramento)) {
        ?>
        <div class="title-module">Encerramento</div>

        <table>
            <tr>
                <th colspan="2">Data / Hora de Recebimento de Informa��es</th>
                <th>Matricula</th>
                <th>Respons�vel pelas Informa��es</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <?php
                    echo MainController::parse_timestamp_static($encerramento['data_encerramento']);
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $encerramento['matricula'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $encerramento['nome_funcionario'];
                    ?>
                </td>
            </tr>
            <tr>
                <th>Libera��o</th>
                <th colspan="3">Tipo de Encerramento</th>
            </tr>
            <tr>
                <td class="center">
                    <?php
                    echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado';
                    ?>
                </td>
                <td colspan="3" class="center">
                    <?php
                    echo $encerramento['nome_fechamento'];
                    ?>
                </td>
            </tr>
            <?php
            if($encerramento['cod_tipo_fechamento'] == 3) {
                ?>
                <tr>
                    <th colspan="4">Motivo da N�o Execu��o</th>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php
                        echo $encerramento['descricao'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            if(!empty($encerramento['cod_pendencia'])) {
                ?>
                <tr>
                    <th colspan="3">Tipo de Pend�ncia</th>
                    <th>Pend�ncia</th>
                </tr>
                <tr>
                    <td colspan="3">
                        <?php
                        echo $encerramento['nome_tipo_pendencia'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $encerramento['nome_pendencia'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    <?php
    }
    ?>
</div>
