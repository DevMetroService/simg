<?php

$conteudo = "N. OSP;";
$conteudo .= "Data Abertura Osp;";
$conteudo .= "Linha;";
$conteudo .= "Material;";
$conteudo .= "Detalhes Material;";
$conteudo .= "Unidade;";
$conteudo .= "Estado;";
$conteudo .= "Utilizado;";
$conteudo .= "Origem;";
$conteudo .= "Detalhes Origem;";
$conteudo .= "Veiculo";
$conteudo .= "Tipo de Material\n";


$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)) {
    $material = array();
    $selectMaterial = $this->medoo->select("v_material","*");
    foreach ($selectMaterial as $dados){
        $material[$dados['cod_material']] = $dados;
    }

    $unidade = array();
    $selectUnidade = $this->medoo->select("unidade_medida", ["cod_uni_medida", "nome_uni_medida", "sigla_uni_medida"]);
    foreach ($selectUnidade as $dados){
        $unidade[$dados['cod_uni_medida']] = "({$dados['sigla_uni_medida']})  {$dados['nome_uni_medida']}";
    }

    $estado = array('n' => 'Novo', 'u' => 'Usado', "r" => "Reparado");
    $origem = array('s' => 'MetroService', 'f' => 'MetroFor', "t" => "TSM", 'o' => 'Outros');

    $veiculo = array();
    $selectVeiculo = $this->medoo->select("veiculo","*");
    foreach ($selectVeiculo as $dados){
        $veiculo[$dados['cod_veiculo']] = "{$dados['nome_veiculo']}";
    }

    foreach ($returnPesquisa as $d => $v) {

      if(in_array($v['grupo_atuado'], [22,23]) ){
        $hasVeiculo = true;
      }
        $selectOspMaterial = $this->medoo->select('osp_material','*',["cod_osp" => (int)$v['cod_osp']]);
        $select_veiculo = $this->medoo->select('material_rodante_osp','cod_veiculo',["cod_osp" => (int)$v['cod_osp']]);

        foreach ($selectOspMaterial as $dados => $value) {
            $mat = $material[$value['cod_material']];

            $txt = $value['cod_osp'] . ":::";
            $txt .= $v['data_abertura_osp'] . ":::";
            $txt .= $v['nome_linha'] . ":::";
            if(!empty($mat['descricao_material']))
              $txt .= $mat['nome_material'] .' - ' . $mat['descricao_material'] . ":::";
            else {
                $txt .= $mat['nome_material'] . ":::";
            }
            $txt .= $value['descricao'] . ":::";
            $txt .= $unidade[$value['unidade']] . ":::";
            $txt .= $estado[$value['estado']] . ":::";
            $txt .= $value['utilizado'] . ":::";
            $txt .= $origem[$value['origem']] . ":::";
            $txt .= $value['descricao_origem'] . ":::";
            if($hasVeiculo)
              $txt .= $veiculo[$select_veiculo[0]] . ":::";
            else {
              $txt .= "N/E:::";
            }
            
            $txt .= $mat['nome_tipo_material'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("OspMateriais.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OspMateriais.csv");
readfile(TEMP_PATH.'OspMateriais.csv');

unlink(TEMP_PATH."OspMateriais.csv");
