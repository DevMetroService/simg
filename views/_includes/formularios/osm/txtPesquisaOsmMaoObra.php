<?php

$conteudo = "N. OSM;";
$conteudo .= "Nome Funcionario;";
$conteudo .= "Data Inicio;";
$conteudo .= "Data Final;";
$conteudo .= "Total Horas\n";

$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)) {
    $responsavel = array();
    $selectResponsavel = $this->medoo->select("funcionario", ["cod_funcionario", "nome_funcionario"]);
    foreach ($selectResponsavel as $dados){
        $responsavel[$dados['cod_funcionario']] = $dados['nome_funcionario'];
    }

    foreach ($returnPesquisa as $d => $v) {
        $selectMaoObra = $this->medoo->select('osm_mao_de_obra','*',["cod_osm" => (int)$v['cod_osm']]);

        foreach ($selectMaoObra as $dados => $value) {
            $txt = $value['cod_osm'] . ":::";
            $txt .= $responsavel[$value['cod_funcionario']] . ":::";
            $txt .= $value['data_inicio'] . ":::";
            $txt .= $value['data_termino'] . ":::";
            $txt .= $value['total_hrs'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("OsmMaoObra.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OsmMaoObra.csv");
readfile(TEMP_PATH.'OsmMaoObra.csv');

unlink(TEMP_PATH."OsmMaoObra.csv");
