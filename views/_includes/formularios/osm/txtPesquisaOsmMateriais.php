<?php

$conteudo = "N. OSM;";
$conteudo .= "Data de Abertura da OSM;";
$conteudo .= "Linha;";
$conteudo .= "Material;";
$conteudo .= "Detalhes Material;";
$conteudo .= "Unidade;";
$conteudo .= "Estado;";
$conteudo .= "Utilizado;";
$conteudo .= "Origem;";
$conteudo .= "Detalhes Origem;";
$conteudo .= "Veiculo;";
$conteudo .= "Tipo de Material;\n";

$conteudo = utf8_decode($conteudo);

if(!empty($returnPesquisa)) {
    $material = [];

    $materialSelectBD = $this->medoo->select("v_material","*");
    foreach ($materialSelectBD as $dados){
        $material[$dados['cod_material']] = $dados;
    }

    $unidade = array();
    $selectUnidade = $this->medoo->select("unidade_medida", ["cod_uni_medida", "nome_uni_medida", "sigla_uni_medida"]);
    foreach ($selectUnidade as $dados){
        $unidade[$dados['cod_uni_medida']] = "({$dados['sigla_uni_medida']})  {$dados['nome_uni_medida']}";
    }

    $veiculo = array();
    $selectVeiculo = $this->medoo->select("veiculo","*");
    foreach ($selectVeiculo as $dados) {
        $veiculo[$dados['cod_veiculo']] = "{$dados['nome_veiculo']}";
    }

    $estado = array('n' => 'Novo', 'u' => 'Usado', "r" => "Reparado");
    $origem = array('s' => 'MetroService', 'f' => 'MetroFor', "t" => "TSM", 'o' => 'Outros');

    foreach ($returnPesquisa as $d => $v) {

        if(in_array($v['grupo_atuado'], [22,23]) ){
            $hasVeiculo = true;
        }

        $selectOsmMaterial = $this->medoo->select('osm_material','*',["cod_osm" => (int)$v['cod_osm']]);
        $select_veiculo = $this->medoo->select('material_rodante_osm','cod_veiculo',["cod_osm" => (int)$v['cod_osm']]);
        foreach ($selectOsmMaterial as $dados => $value) {
            $txt = $value['cod_osm'] . ":::";
            $txt .= $v['data_abertura_osm'] . ":::";
            $txt .= $v['nome_linha'] . ":::";
            $txt .= $material[$value['cod_material']]['nome_material'] . ":::";
            $txt .= $material[$value['cod_material']]['descricao_material'] . ":::";
            $txt .= $unidade[$value['unidade']] . ":::";
            $txt .= $estado[$value['estado']] . ":::";
            $txt .= $value['utilizado'] . ":::";
            $txt .= $origem[$value['origem']] . ":::";
            $txt .= $value['descricao_origem'] . ":::";

            if($hasVeiculo)
                $txt .= $veiculo[$select_veiculo[0]] . ":::";
            else {
                $txt .= "N/E:::";
            }

            $txt .= $material[$value['cod_material']]['nome_tipo_material'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("OsmMateriais.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OsmMateriais.csv");
readfile(TEMP_PATH.'OsmMateriais.csv');

unlink(TEMP_PATH."OsmMateriais.csv");
