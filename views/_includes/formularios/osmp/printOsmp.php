<?php
if (!empty($_SESSION['refillOs'])) {
    switch ($_SESSION['refillOs']['form']) {
        case "ed":
            $sql = Sql::osmpEd();
            $nomeGrupo = "Edifica��o";
            break;
        case "vp":
            $sql = Sql::osmpVp();
            $nomeGrupo = "Via Permanente";
            break;
        case "su":
            $sql = Sql::osmpSu();
            $nomeGrupo = "Subesta��o";
            break;
        case "ra":
            $sql = Sql::osmpRa();
            $nomeGrupo = "Rede A�rea";
            break;
        case "tl":
            $sql = Sql::osmpTl();
            $nomeGrupo = "Telecom";
            break;
        case "bl":
            $sql = Sql::osmpBl();
            $nomeGrupo = "Bilhetagem";
            break;
    }
    $sql .= " WHERE osmp.cod_osmp = {$_SESSION['refillOs']['codigoOs']}";

    $osmp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $osmp = $osmp[0];

    $status = $this->medoo->select("status_osmp", ["[><]status" => "cod_status"], "*", ["cod_status_osmp" => $osmp['cod_status_osmp']]);
    $status = $status[0];

    $tempo = $this->medoo->select('osmp_tempo','*',["cod_osmp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $tempo = $tempo[0];

    $maoObra = $this->medoo->select("osmp_mao_de_obra", ["[><]funcionario" => "matricula"], "*",["cod_osmp" => (int) $_SESSION['refillOs']['codigoOs']]);

    $registro = $this->medoo->select("osmp_registro", [
        "[>]un_equipe" => "cod_un_equipe",
        "[>]unidade" => "cod_unidade",
        "[>]equipe" => "cod_equipe",
        "[>]atuacao" => "cod_atuacao"
    ],"*",["cod_osmp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $registro = $registro[0];

    $maquinas = $this->medoo->select("osmp_maquina", ["[><]material" => "cod_material"],
        [
            "cod_material",
            "nome_material",
            "qtd",
            "material.descricao(descricao_material)",
            "total_hrs",
            "num_serie",
            "osmp_maquina.descricao"
        ], [ "cod_osmp" => $_SESSION['refillOs']['codigoOs']]);

    $materiais = $this->medoo->query("SELECT 
                                        *
                                    FROM osmp_material osmpm
                                    JOIN material USING (cod_material)
                                    JOIN unidade_medida um ON(osmpm.unidade = um.cod_uni_medida)
                                    JOIN osmp USING (cod_osmp)
                                    WHERE cod_osmp = {$_SESSION['refillOs']['codigoOs']}
                                    ORDER BY nome_material")->fetchAll(PDO::FETCH_ASSOC);

    $encerramento = $this->medoo->select("osmp_encerramento",
        [
            "[>]funcionario" => "cod_funcionario",
            "[>]tipo_fechamento" => "cod_tipo_fechamento",
            "[>]pendencia" => "cod_pendencia",
            "[>]tipo_pendencia" => "cod_tipo_pendencia"
        ], "*", ["cod_osmp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $encerramento = $encerramento[0];
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>OSMP - Ordem de Servi�o de Manuten��o Preventiva</h3>
    </div>
    <hr/>

    <div class="title-module">Dados Gerais</div>
    <table>
        <tr>
            <th>OSMP</th>
            <th>SSMP</th>
            <th>Data/Hora Abertura</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osmp['cod_osmp'];?></td>
            <td class="center"><?php echo $osmp['cod_ssmp'];?></td>
            <td class="center"><?php echo MainController::parse_timestamp_static($osmp['data_abertura']);?></td>
        </tr>
        <tr>
            <th colspan=2">Equipe</th>
            <th>Unidade</th>
        </tr>
        <tr>
            <td colspan=2" class="center"><?php echo $osmp['nome_equipe'];?></td>
            <td class="center"><?php echo $osmp['nome_unidade'];?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o / Local</div>
    <table>
        <tr>
            <th colspan="2">Grupo de Sistema Atuado</th>
            <th colspan="2">Sistema Atuado</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $nomeGrupo;?></td>
            <td colspan="2" class="center"><?php echo $osmp['nome_sistema'];?></td>
        </tr>
        <tr>
            <th colspan="4">SubSistema Atuado</th>
        </tr>
        <tr>
            <td colspan="4" class="center"><?php echo $osmp['nome_subsistema'];?></td>
        </tr>
        <tr>
            <?php
            if (!empty($osmp['estacao_inicial'])) {
                echo "<th>Linha</th>";
                echo "<th>Esta��o Inicial</th>";
                echo "<th>Esta��o Final</th>";
                echo "<th>Via</th>";
            }else if ($nomeGrupo == "Rede A�rea") {
                echo "<th>Linha</th>";
                echo "<th>Poste</th>";
                echo "<th>Via</th>";
                echo "<th>Posi��o</th>";
            }else{
                echo "<th colspan='4'>Linha</th>";
            }
            ?>
        </tr>
        <tr>
            <?php
            if ($osmp['estacao_inicial']) {
                echo "<td class='center'>{$osmp['nome_linha']}</td>";
                echo "<td class='center'>{$osmp['estacao_inicial']}</td>";
                echo "<td class='center'>{$osmp['estacao_final']}</td>";
                echo "<td class='center'>{$osmp['nome_via']}</td>";
            }else if ($nomeGrupo == "Rede A�rea") {
                echo "<td class='center'>{$osmp['nome_linha']}</td>";
                echo "<td class='center'>{$osmp['nome_poste']}</td>";
                echo "<td class='center'>{$osmp['nome_via']}</td>";
                echo "<td class='center'>{$osmp['posicao']}</td>";
            }else{
                echo "<td colspan='4' class='center'>{$osmp['nome_linha']}</td>";
            }
            ?>
        </tr>
        <?php
        if (!empty($osmp['km_inicial'])){
            echo "
            <tr>
                <th>Km Inicial Atuado</th>
                <th>Km Final Atuado</th>
                <th colspan='2'>AMV</th>
            </tr>
            <tr>
                <td class='center'>{$osmp['km_inicial']}</td>
                <td class='center'>{$osmp['km_final']}</td>
                <td colspan='2' class='center'>{$osmp['nome_amv']}</td>
            </tr>
            ";
        }
        if (!empty($osmp['nome_local'])){
            echo "
            <tr>
                <th colspan='4'>Local</th>
            </tr>
            <tr>
                <td colspan='4' class='center'>{$osmp['nome_local']}</td>
            </tr>
            ";
        }else if(!empty($osmp['nome_estacao']))
        {
            echo "
            <tr>
                <th colspan='4'>Local</th>
            </tr>
            <tr>
                <td colspan='4' class='center'>{$osmp['nome_estacao']}</td>
            </tr>
            ";
        }
        ?>
        <tr>
            <th colspan="4">Complemento</th>
        </tr>
        <tr>
            <td colspan="4"><?php if (!empty($osmp)) {
                    echo $osmp['complemento'];
                } ?></td>
        </tr>
    </table>
    
    <div class="title-table">&raquo; Servi�o a Ser Executado</div>
    <table>
        <tr>
            <th colspan="3">Nome Servi�o</th>
        </tr>
        <tr>
            <td colspan="3"><?php echo $osmp['nome_servico_pmp'];?></td>
        </tr>
        <tr>
            <th colspan="2">Procedimento</th>
            <th>Periodicidade</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $osmp['nome_procedimento'];?></td>
            <td class="center"><?php echo $osmp['nome_periodicidade'];?></td>
        </tr>
        <tr>
            <th>Efetivo</th>
            <th>Tempo</th>
            <th>Total Servi�o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osmp['mao_obra'];?></td>
            <td class="center"><?php echo $osmp['horas_uteis'];?></td>
            <td class="center"><?php echo $osmp['homem_hora'];?></td>
        </tr>
    </table>

    <?php if(!empty($status['descricao'])) { ?>
        <div class="title-table">&raquo; Situa��o da Solicita��o</div>
        <table>
            <tr>
                <th>Status</th>
                <th>Descri��o</th>
            </tr>
            <tr>
                <td><?php echo $status['nome_status'];?></td>
                <td><?php echo $status['descricao'];?></td>
            </tr>
        </table>
    <?php } ?>
    
    <table class="footer">
        <tr>
            <th>Respons�vel</th>
            <th>Situa��o</th>
            <th>Condi��o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osmp['usuario']; ?></td>
            <td class="center"><?php echo $osmp['nome_status']; ?></td>
            <td class="center"><?php echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado'; ?></td>
        </tr>
    </table>

    <?php
    if (!empty($tempo)) {
        ?>
        <div class="title-module">Tempos Totais</div>

        <div class="title-table">&raquo; Prepara�ao e Acesso</div>
        <table>
            <tr>
                <th>In�cio</th>
                <th>Pronto para Sair</th>
                <th>Sa�da Autorizada</th>
                <th>Sa�da</th>
                <th>Chegada</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_preparacao']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_pronto']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_saida_autorizada']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_saida']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_ace_chegada']);}
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Execu��o</div>
        <table>
            <tr>
                <th>In�cio</th>
                <th>T�rmino</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_exec_inicio']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_exec_termino']);}
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Regresso</div>
        <table>
            <tr>
                <th>Pronto para Sair</th>
                <th>Sa�da Autorizada</th>
                <th>Chegada</th>
                <th>Desmobiliza��o</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_reg_pronto']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_reg_saida_autorizada']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_reg_chegada']);}
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {echo MainController::parse_timestamp_static($tempo['data_desmobilizacao']);}
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Observa��o</div>
        <table>
            <tr>
                <td><?php echo $tempo['descricao']; ?></td>
            </tr>
        </table>
        <?php
    }

    if (!empty($maoObra)) {
        echo '<div class="title-module">M�o de Obra</div>
                    <table>
                    <tr>
                        <th>Matr�cula</th>
                        <th>Nome</th>
                        <th>In�cio</th>
                        <th>T�rmino</th>
                    </tr>';

        foreach ($maoObra as $dados => $value){
            ?>
            <tr>
                <td class="center">
                    <?php
                    echo $value['matricula'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $value['nome_funcionario'];
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $value['data_inicio'];
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $value['data_termino'];
                    ?>
                </td>
            </tr>
            <?php
        }
        echo '</table>';
    }

    if (!empty($registro)) {
        ?>
        <div class="title-module">Registro de Execu��o</div>

        <table>
            <tr>
                <th colspan="2">Local</th>
                <th colspan="2">Equipe</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo $registro['nome_unidade']; ?></td>
                <td colspan="2" class="center"><?php echo $registro['sigla'] . ' - ' . $registro['nome_equipe']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Atua��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['nome_atuacao'];?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o da Atua��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['desc_atuacao']; ?></td>
            </tr>
            <tr>
                <th>Clima</th>
                <th>Temperatura</th>
            </tr>
            <tr>
                <td class="center"><?php
                    switch ($registro['clima']){
                        case "b":
                            echo "Bom";
                            break;
                        case "c":
                            echo "Chuvoso";
                            break;
                        case "n":
                            echo "Nublado";
                            break;
                        case "e":
                            echo "Neblina";
                            break;
                    }
                    ?></td>
                <td class="center"><?php echo $registro['temperatura']; ?></td>
            </tr>
            <?php
            if($registro['cautela'] != 'n') {
                ?>
                <tr>
                    <th>Cautela</th>
                    <th>Km Inicial</th>
                    <th>Km Final</th>
                    <th>Restringir Velocidade</th>
                </tr>
                <tr>
                    <td class="center"><?php echo $registro['cautela']; ?></td>
                    <td class="center"><?php echo $registro['km_inicial_cautela']; ?></td>
                    <td class="center"><?php echo $registro['km_final_cautela']; ?></td>
                    <td class="center"><?php echo $registro['restricao_veloc']; ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <th colspan="4">Observa��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['dados_complementar']; ?></td>
            </tr>
        </table>
        <?php
    }

    if (!empty($maquinas)) {
        echo '<div class="title-module">M�quinas e Equipamentos Utilizados</div>';

        foreach ($maquinas as $dados => $value) {
            ?>
            <table>
                <tr>
                    <th>C�digo</th>
                    <th colspan="2">Nome</th>
                    <th>Quantidade</th>
                </tr>
                <tr>
                    <td class="center"><?php echo $value['cod_material'];?></td>
                    <td colspan="2"><?php echo $value['nome_material'];?></td>
                    <td class="center"><?php echo $value['qtd'];?></td>
                </tr>
                <tr>
                    <th colspan="4">Complemento</th>
                </tr>
                <tr>
                    <td colspan="4"><?php echo $value['descricao_material'];?></td>
                </tr>
                <tr>
                    <th colspan="2">Total de Hora</th>
                    <th colspan="2">N�mero de S�rie</th>
                </tr>
                <tr>
                    <td colspan="2" class="center"><?php echo $value['total_hrs']; ?></td>
                    <td colspan="2" class="center"><?php echo $value['num_serie'];?></td>
                </tr>
                <tr>
                    <th colspan="4">Descri��o</th>
                </tr>
                <tr class="tr-destaque">
                    <td colspan="4"><?php echo $value['descricao'];?></td>
                </tr>
            </table>
            <?php
        }
    }

    if (!empty($materiais)) {
        echo ' <div class="title-module">Materiais Utilizados</div><table>';

        foreach ($materiais as $dados => $value) {
            ?>
            <tr>
                <th>C�digo</th>
                <th>Nome</th>
                <th>Quantidade</th>
            </tr>
            <tr>
                <td class="center"><?php echo "{$value['cod_material']}"; ?></td>
                <td><?php echo "{$value['nome_material']} - {$value['descricao']}";?></td>
                <td class="center"><?php echo $value['utilizado'];?></td>
            </tr>
            <tr>
                <th>Estado</th>
                <th>Origem</th>
                <th>Unidade de Medida</th>
            </tr>
            <tr class="tr-destaque">
                <td class="center"><?php
                    if($value['estado'] = "n") echo 'Novo';
                    else echo $value['estado'];
                    ?></td>
                <td class="center"><?php
                    switch ($value['origem']){
                        case 's':
                            echo "MetroService";
                            break;
                        case 'f':
                            echo "MetroFor";
                            break;
                        default:
                            echo "Outros";
                            break;
                    }
                    ?></td>
                <td class="center"><?php
                    echo $value['sigla_uni_medida'] . ' - ' . $value['nome_uni_medida'];
                    ?></td>
            </tr>
            <?php
        }
        echo '</table>';
    }

    if (!empty($encerramento)) {
        ?>
        <div class="title-module">Encerramento</div>
        <table>
            <tr>
                <th colspan="2">Data / Hora de Recebimento de Informa��es</th>
                <th>Matricula</th>
                <th>Respons�vel pelas Informa��es</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <?php
                    echo MainController::parse_timestamp_static($encerramento['data_encerramento']);
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $encerramento['matricula'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $encerramento['nome_funcionario'];
                    ?>
                </td>
            </tr>
            <tr>
                <th>Libera��o</th>
                <th colspan="3">Tipo de Encerramento</th>
            </tr>
            <tr>
                <td class="center">
                    <?php
                    echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado';
                    ?>
                </td>
                <td colspan="3" class="center">
                    <?php
                    echo $encerramento['nome_fechamento'];
                    ?>
                </td>
            </tr>
            <?php
            if($encerramento['cod_tipo_fechamento'] == 3) {
                ?>
                <tr>
                    <th colspan="4">Motivo da N�o Execu��o</th>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php
                        echo $encerramento['descricao'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            if(!empty($encerramento['cod_pendencia'])) {
                ?>
                <tr>
                    <th colspan="3">Tipo de Pend�ncia</th>
                    <th>Pend�ncia</th>
                </tr>
                <tr>
                    <td colspan="3">
                        <?php
                        echo $encerramento['nome_tipo_pendencia'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $encerramento['nome_pendencia'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    ?>
</div>