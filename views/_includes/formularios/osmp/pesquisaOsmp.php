<div class="page-header">
    <h2>Pesquisa de OSMP</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/executarPesquisaOsmp" class="form-group" method="post"
      id="pesquisa">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Osmp</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDadosGerais">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#dadosGeraisPesquisa" aria-expanded="false"
                                       aria-controls="collapseDadosGerais">
                                        <button class="btn btn-default" type="button">
                                            <label>Dados Gerais</label>
                                        </button>
                                    </a>
                                </div>

                                <div id="dadosGeraisPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['numeroPesquisaOsmp']) || !empty($dadosRefill['codigoSsmp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingDadosGerais">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>N� OSMP</label>
                                                <input class="form-control" name="numeroPesquisaOsmp"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['numeroPesquisaOsmp']); ?>"
                                                       type="text">
                                            </div>

                                            <div class="col-md-2">
                                                <label>N� SSMP</label>
                                                <input class="form-control" name="codigoSsmp"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['codigoSsmp']); ?>"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDatas">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#datasPesquisa" aria-expanded="false"
                                       aria-controls="collapseDatas">
                                        <button class="btn btn-default" type="button"><label>Datas e Status</label>
                                        </button>
                                    </a>
                                </div>

                                <div id="datasPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['dataPartirOsmpAbertura']) || !empty($dadosRefill['dataAteOsmpAbertura']) ||
                                        !empty($dadosRefill['dataPartirOsmpEncerramento']) || !empty($dadosRefill['dataAteOsmpEncerramento']) ||
                                        !empty($dadosRefill['dataPartirOsmp']) || !empty($dadosRefill['dataAteOsmp']) || !empty($dadosRefill['situacaoPesquisaOsmp'])
                                    ) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingDatas">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Abertura (a Partir)</label>
                                                <input class="form-control data validaData"
                                                       name="dataPartirOsmpAbertura"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataPartirOsmpAbertura']); ?>"
                                                       type="text">
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input class="form-control data validaData" name="dataAteOsmpAbertura"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataAteOsmpAbertura']); ?>"
                                                       type="text">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Data de Encerramento (a Partir)</label>
                                                <input type="text" class="form-control data validaData"
                                                       name="dataPartirOsmpEncerramento"
                                                       value="<?php if (!empty($dadosRefill['dataPartirOsmpEncerramento'])) echo($dadosRefill['dataPartirOsmpEncerramento']); ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input name="dataAteOsmpEncerramento" type="text"
                                                       class="form-control data validaData"
                                                       value="<?php if (!empty($dadosRefill['dataAteOsmpEncerramento'])) echo($dadosRefill['dataAteOsmpEncerramento']); ?>"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Status</label>
                                                <select class="form-control" name="situacaoPesquisaOsmp">
                                                    <option value=''>Todos</option>
                                                    <option value='10' <?php if ($dadosRefill['situacaoPesquisaOsmp'] == "10") echo("selected") ?>>
                                                        Execu��o
                                                    </option>
                                                    <option value='11' <?php if ($dadosRefill['situacaoPesquisaOsmp'] == "11") echo("selected") ?>>
                                                        Encerrada
                                                    </option>
                                                    <option value='23' <?php if ($dadosRefill['situacaoPesquisaOsmp'] == "23") echo("selected") ?>>
                                                        N�o Executado
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Data do Status (a Partir)</label>
                                                <input class="form-control data validaData" name="dataPartirOsmp"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataPartirOsmp']); ?>"
                                                       type="text">
                                            </div>

                                            <div class="col-md-3">
                                                <label>At�</label>
                                                <input class="form-control data validaData" name="dataAteOsmp"
                                                       value="<?php if (!empty($dadosRefill)) echo($dadosRefill['dataAteOsmp']); ?>"
                                                       type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingServico">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#servicoPesquisa"
                                       aria-expanded="false" aria-controls="collapseServico">
                                        <button class="btn btn-default" type="button"><label>Servi�o</label></button>
                                    </a>
                                </div>

                                <div id="servicoPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['grupoPesquisaOsmp']) || !empty($dadosRefill['sistemaPesquisaOsmp']) || !empty($dadosRefill['subSistemaPesquisaOsmp']) ||
                                        !empty($dadosRefill['servicoPesquisaOsmp'])
                                    ) {
                                        echo('class="panel"');
                                    } else {
                                        echo('class="panel"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingServico">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grupo Sistema <span style="color: red">(*)</span></label>
                                                <select class="form-control" name="grupoPesquisaOsmp">
                                                    <?php
                                                    if (empty($dadosRefill['grupoSistemaPesquisa'])){
                                                        $dadosRefill['grupoSistemaPesquisa'] = 21;
                                                    }
                                                    ?>
                                                    <option <?php if ($dadosRefill['grupoPesquisaOsmp'] == 21) echo 'selected' ?>
                                                            value="21">Edifica��es
                                                    </option>
                                                    <option <?php if ($dadosRefill['grupoPesquisaOsmp'] == 24) echo 'selected' ?>
                                                            value="24">Via Permanente
                                                    </option>
                                                    <option <?php if ($dadosRefill['grupoPesquisaOsmp'] == 25) echo 'selected' ?>
                                                            value="25">Subesta��o
                                                    </option>
                                                    <option <?php if ($dadosRefill['grupoPesquisaOsmp'] == 20) echo 'selected' ?>
                                                            value="20">Rede A�rea
                                                    </option>
                                                    <option <?php if ($dadosRefill['grupoPesquisaOsmp'] == 27) echo 'selected' ?>
                                                            value="27">Telecom
                                                    </option>
                                                    <option <?php if ($dadosRefill['grupoPesquisaOsmp'] == 28) echo 'selected' ?>
                                                            value="28">Bilhetagem
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sistema</label>
                                                <select class="form-control" name="sistemaPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectSistema = $this->medoo->select("grupo_sistema", ["[><]sistema" => "cod_sistema"], ['cod_sistema', 'nome_sistema'], ["cod_grupo" => $dadosRefill['grupoPesquisaOsmp']]);

                                                    foreach ($selectSistema as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisaOsmp'] == $value['cod_sistema'])
                                                            echo('<option value="' . $value['cod_sistema'] . '" selected>' . $value['nome_sistema'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_sistema'] . '">' . $value['nome_sistema'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Subsistema</label>
                                                <select class="form-control" name="subSistemaPesquisaOsmp">
                                                    <option value="">Subsistemas</option>
                                                    <?php
                                                    if ($dadosRefill['sistemaPesquisaOsmp']) {

                                                        $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"], ['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisaOsmp']]);

                                                        foreach ($subsistema as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisaOsmp'] == $value['cod_subsistema'])
                                                                echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                            else
                                                                echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Servi�o</label>
                                                <select class="form-control" name="servicoPesquisaOsmp">
                                                    <option value="">TODOS</option>
                                                    <?php
                                                    $servico = $this->medoo->select('servico_pmp', ['nome_servico_pmp', 'cod_servico_pmp'], ['cod_grupo' => $dadosRefill['grupoPesquisaOsmp']]);

                                                    foreach ($servico as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['servicoPesquisaCronograma'] == $value['cod_servico_pmp'])
                                                            echo('<option value="' . $value['cod_servico_pmp'] . '" selected>' . $value['nome_servico_pmp'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_servico_pmp'] . '">' . $value['nome_servico_pmp'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingLocal">
                                    <a class="collapsed" role="button" data-toggle="collapse" href="#localPesquisa"
                                       aria-expanded="false" aria-controls="collapseLocal">
                                        <button class="btn btn-default" type="button"><label>Local</label></button>
                                    </a>
                                </div>

                                <div id="localPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['linhaPesquisaOsmp']) || !empty($dadosRefill['localPesquisaOsmp'])) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingLocal">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Linha</label>
                                                <select class="form-control" name="linhaPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectlinha = $this->medoo->select("linha", ['cod_linha', 'nome_linha']);

                                                    foreach ($selectlinha as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaOsmp'] == $value['cod_linha'])
                                                            echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div id="local" class="col-md-4">
                                                <label>Local</label>
                                                <select class="form-control" name="localPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    if ($dadosRefill['linhaPesquisaOsmp']) {
                                                        $selectLocal = $this->medoo->select("local", ["[><]linha" => "cod_linha"], ['cod_local', 'nome_local'], ["cod_linha" => $dadosRefill['linhaPesquisaOsmp']]);
                                                        foreach ($selectLocal as $dados => $value) {
                                                            if (!empty($dadosRefill) && $dadosRefill['localPesquisaOsmp'] == $value['cod_local'])
                                                                echo('<option value="' . $value['cod_local'] . '" selected>' . $value['nome_local'] . '</option>');
                                                            else
                                                                echo('<option value="' . $value['cod_local'] . '">' . $value['nome_local'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div id="estacaoInicial" class="col-md-4">
                                                <label>Esta��o Inicial</label>
                                                <select class="form-control" name="estacaoInicialPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectEstacao = $this->medoo->select('estacao', ['nome_estacao', 'cod_estacao']);
                                                    foreach ($selectEstacao as $dados => $value) {
                                                        if ($dadosRefill['estacaoInicialPesquisaOsmp'] == $value['cod_estacao'])
                                                            echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div id="estacaoFinal" class="col-md-4">
                                                <label>Esta��o Final</label>
                                                <select class="form-control" name="estacaoFinalPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    foreach ($selectEstacao as $dados => $value) {
                                                        if ($dadosRefill['estacaoFinalPesquisaOsmp'] == $value['cod_estacao'])
                                                            echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                                        else
                                                            echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">

                                <div class="panel-heading" role="tab" id="headingMaquinasEquipamentos">
                                    <a class="collapsed" role="button" data-toggle="collapse"
                                       href="#maquinasEquipamentosPesquisa" aria-expanded="false"
                                       aria-controls="collapseMaquinasEquipamentos">
                                        <button class="btn btn-default" type="button"><label>M�quinas Equipamentos e
                                                Materiais</label></button>
                                    </a>
                                </div>

                                <div id="maquinasEquipamentosPesquisa"
                                    <?php
                                    if (!empty($dadosRefill['maquinasEquipamentosPesquisaOsmp']) || !empty($dadosRefill['materiaisPesquisaOsmp']) ||
                                        !empty($dadosRefill['estadoPesquisaOsmp']) || !empty($dadosRefill['origemPesquisaOsmp'])
                                    ) {
                                        echo('class="panel-collapse collapse in"');
                                    } else {
                                        echo('class="panel-collapse collapse out"');
                                    }
                                    ?>
                                     role="tabpanel" aria-labelledby="headingMaquinasEquipamentos">

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>M�quinas e Equipamentos</label>
                                                <select class="form-control" name="maquinasEquipamentosPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectMaquinasEquipamentos = $this->medoo->select("v_material", ['nome_material', 'cod_material'], [
                                                        "cod_categoria" => 10, "ORDER" => "nome_material"]);
                                                    foreach ($selectMaquinasEquipamentos as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['maquinasEquipamentosPesquisaOsmp'] == $value['cod_material']) {
                                                            echo('<option value="' . $value['cod_material'] . '" selected>' . $value['nome_material'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_material'] . '">' . $value['nome_material'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Materiais</label>
                                                <select class="form-control" name="materiaisPesquisaOsmp">
                                                    <option value="">Todos</option>
                                                    <?php
                                                    $selectMateriais = $this->medoo->select("v_material", ['nome_material', 'cod_material'], ["ORDER" => "nome_material", "cod_categoria" => "32"]);

                                                    foreach ($selectMateriais as $dados => $value) {
                                                        if (!empty($dadosRefill) && $dadosRefill['materiaisPesquisaOsmp'] == $value['cod_material']) {
                                                            echo('<option value="' . $value['cod_material'] . '" selected>' . $value['nome_material'] . '</option>');
                                                        } else {
                                                            echo('<option value="' . $value['cod_material'] . '">' . $value['nome_material'] . '</option>');
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Estado</label>
                                                <select class="form-control" name="estadoPesquisaOsmp">
                                                    <?php
                                                    switch ($dadosRefill['estadoPesquisaOsmp']) {
                                                        case '':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n">Novo</option>');
                                                            echo('<option value="u">Usado</option>');
                                                            break;
                                                        case 'n':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n" selected>Novo</option>');
                                                            echo('<option value="u">Usado</option>');
                                                            break;
                                                        case 'u':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="n">Novo</option>');
                                                            echo('<option value="u" selected>Usado</option>');
                                                            break;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Origem</label>
                                                <select class="form-control" name="origemPesquisaOsmp">
                                                    <?php
                                                    switch ($dadosRefill['origemPesquisaOsmp']) {
                                                        case '':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 's':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s" selected>MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 'f':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f" selected>MetroFor</option>');
                                                            echo('<option value="o">Outros</option>');
                                                            break;
                                                        case 'o':
                                                            echo('<option value="">Todos</option>');
                                                            echo('<option value="s">MetroService</option>');
                                                            echo('<option value="f">MetroFor</option>');
                                                            echo('<option value="o" selected>Outros</option>');
                                                            break;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
                            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro
                            </button>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 1em"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Resultado da Pesquisa
                                    <strong>Total
                                        Encontrado: <?php echo (!empty($_SESSION['resultadoPesquisaOsmp'])) ? count($_SESSION['resultadoPesquisaOsmp']) : 0; ?></strong>
                                </div>

                                <div class="panel-body">
                                    <table id="resultadoPesquisaOsmp"
                                           class="table table-striped table-bordered no-footer">
                                        <thead id="headIndicadorOsmp">
                                        <tr role="row">
                                            <th>N� Osmp</th>
                                            <th>N� Ssmp</th>
                                            <th>Local</th>
                                            <th>Data de Abertura</th>
                                            <th>Servi�o</th>
                                            <th>Status</th>
                                            <th>A��o</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (!empty($_SESSION['resultadoPesquisaOsmp'])) {
                                            foreach ($_SESSION['resultadoPesquisaOsmp'] as $dados => $value) {
                                                echo('<tr>');
                                                echo('<td>' . $value['cod_osmp'] . '</td>');
                                                echo("<td><span class='primary-info'>{$value['cod_ssmp']}</span><br /><span class='sub-info'>{$value['nome_fechamento']}</span></td>");
                                                echo("<td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['local']}</span></td>");
                                                echo('<td>' . $this->parse_timestamp($value['data_abertura']) . '</td>');
                                                echo('<td>' . $value['nome_servico_pmp'] . '</span></td>');
                                                echo('<td>' . $value['nome_status'] . '</td>');
                                                echo("<td>
                                                        <button value='{$value['form']}' class='btn btn-primary btn-circle btnEditarOsmp' type='button' title='Ver Dados/Editar'>
                                                            <i class='fa fa-file-o fa-fw'></i>
                                                        </button>
                                                   <button class='btn btn-default btn-circle btnImprimirOsmp' value='{$value['form']}' type='button' title='Imprimir'><i class='fa fa-print fa-fw'></i></button>
                                                    </td>");
                                                echo('</tr>');
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>