<?php
function atraso($atraso){
    if(!empty($atraso))
        return $atraso . " minutos";
    else
        return '';
}

$conteudo = "N. Osmp;";
$conteudo .= "N. Ssmp;";
$conteudo .= "Responsável pelo Preenchimento;";
$conteudo .= "Data de Abertura Osmp;";
$conteudo .= "Data de Abertura Ssmp;";
$conteudo .= "Data Programada - Ssmp;";

$conteudo .= "Nome Status;";
$conteudo .= "Data Referente ao Status;";
$conteudo .= "Descrição Status;";
$conteudo .= "Data / Hora de Recebimento de Informações;";

$conteudo .= "Linha;";
$conteudo .= "Local;";
$conteudo .= "Estação Inicial;";
$conteudo .= "Estação Final;";
$conteudo .= "Via;";
$conteudo .= "Poste;";
$conteudo .= "Poste Final;";
$conteudo .= "Posição;";
$conteudo .= "Km Inicial;";
$conteudo .= "Km Final;";
$conteudo .= "AMV;";
$conteudo .= "Complemento Local;";

$conteudo .= "Grupo;";
$conteudo .= "Sistema;";
$conteudo .= "Sub Sistema;";

$conteudo .= "Serviço;";
$conteudo .= "Procedimento;";
$conteudo .= "Periodicidade;";

$conteudo .= "Efetivo;";
$conteudo .= "Tempo;";
$conteudo .= "Unidade de tempo;";
$conteudo .= "Total Serviço;";
$conteudo .= "Unidade de Medida;";

$conteudo .= "Data Recebimento;";
$conteudo .= "Atraso Recebimento;";

$conteudo .= "Preparacao Inicio;";
$conteudo .= "Atraso Inicio Preparacao;";

$conteudo .= "Acesso Pronto Sair;";
$conteudo .= "Atraso Acesso Pronto Sair;";

$conteudo .= "Acesso Saída Autorizada;";

$conteudo .= "Acesso Saída;";
$conteudo .= "Atraso Acesso Saída;";

$conteudo .= "Acesso Chegada;";
$conteudo .= "Atraso Acesso Chegada;";

$conteudo .= "Execução Inicio;";
$conteudo .= "Atraso Execução Inicio;";

$conteudo .= "Execução Termino;";
$conteudo .= "Atraso Execução Termino;";

$conteudo .= "Regresso Pronto Sair;";
$conteudo .= "Atraso Regresso Pronto Sair;";

$conteudo .= "Regresso Saída Autorizada;";

$conteudo .= "Regresso Chegada;";
$conteudo .= "Atraso Regresso Chegada;";

$conteudo .= "Regresso Desmobilização;";
$conteudo .= "Descricao Tempo;";

$conteudo .= "Unidade;";
$conteudo .= "Equipe;";

$conteudo .= "Causa Avaria;";
$conteudo .= "Obs. Causa;";

$conteudo .= "Atuacao Execução;";
$conteudo .= "Obs. Atuação;";

$conteudo .= "Agente Causador;";

$conteudo .= "Clima;";
$conteudo .= "Temperatura;";

$conteudo .= "Transporte;";

$conteudo .= "Cautela;";
$conteudo .= "km Inicial Cautela;";
$conteudo .= "km Fim Cautela;";
$conteudo .= "Restringir Velocidade;";
$conteudo .= "Dados Complementares;";

$conteudo .= "Descrição tipo fechamento;";

$conteudo .= "Liberação;";

$conteudo .= "Tipo pendencia;";
$conteudo .= "Pendencia;";

$conteudo .= "Motivo da não Execução;";

$conteudo .= "Responsavel pelas Informações;\n";

$conteudo = utf8_decode($conteudo);


if(!empty($_SESSION['resultadoPesquisaOsmp'])) {

    //// Funções basicas executadas apenas uma vez no loop ////

    $responsavel = array();
    $selectResponsavel = $this->medoo->select("usuario", ["cod_usuario", "usuario"]);
    foreach ($selectResponsavel as $dados) {
        $responsavel[$dados['cod_usuario']] = $dados['usuario'];
    }

    $status = array();
    $selectStatus = $this->medoo->select("status", ["cod_status", "nome_status"]);
    foreach ($selectStatus as $dados) {
        $status[$dados['cod_status']] = $dados['nome_status'];
    }

    $linha = array();
    $selectLinha = $this->medoo->select("linha", ["cod_linha", "nome_linha"]);
    foreach ($selectLinha as $dados) {
        $linha[$dados['cod_linha']] = $dados['nome_linha'];
    }

    $via = array();
    $selectVia = $this->medoo->select("via", ["cod_via", "nome_via"]);
    foreach ($selectVia as $dados) {
        $via[$dados['cod_via']] = $dados['nome_via'];
    }

    $amv = array();
    $selectAmv = $this->medoo->select("amv", ["cod_amv", "nome_amv"]);
    foreach ($selectAmv as $dados) {
        $amv[$dados['cod_amv']] = $dados['nome_amv'];
    }

    $sistema = array();
    $selectSistema = $this->medoo->select("sistema", ["cod_sistema", "nome_sistema"]);
    foreach ($selectSistema as $dados) {
        $sistema[$dados['cod_sistema']] = $dados['nome_sistema'];
    }

    $subsistema = array();
    $selectSubsistema = $this->medoo->select("subsistema", ["cod_subsistema", "nome_subsistema"]);
    foreach ($selectSubsistema as $dados) {
        $subsistema[$dados['cod_subsistema']] = $dados['nome_subsistema'];
    }

    $servico = array();
    $selectServico = $this->medoo->select("servico_pmp", ["cod_servico_pmp", "nome_servico_pmp"]);
    foreach ($selectServico as $dados) {
        $servico[$dados['cod_servico_pmp']] = $dados['nome_servico_pmp'];
    }

    $procedimento = array();
    $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"]);
    foreach ($selectProcedimento as $dados) {
        $procedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];
    }

    $periodicidade = array();
    $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
    foreach ($selectPeriodicidade as $dados) {
        $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];
    }

    $unEquipe = array();
    $selectUnEquipe = $this->medoo->select("un_equipe",
        [
            "[><]equipe" => "cod_equipe",
            "[><]unidade" => "cod_unidade"
        ], "*");
    foreach ($selectUnEquipe as $dados) {
        $unEquipe[$dados['cod_un_equipe']] = $dados;
    }

    $causa = array();
    $selectCausa = $this->medoo->select("causa", ['cod_causa', "nome_causa"]);
    foreach ($selectCausa as $dados) {
        $causa[$dados['cod_causa']] = $dados['nome_causa'];
    }

    $atuacao = array();
    $selectAtuacao = $this->medoo->select("atuacao", ['cod_atuacao', "nome_atuacao"]);
    foreach ($selectAtuacao as $dados) {
        $atuacao[$dados['cod_atuacao']] = $dados['nome_atuacao'];
    }

    $agCausador = array();
    $selectAgCausador = $this->medoo->select("agente_causador", ['cod_ag_causador', "nome_agente"]);
    foreach ($selectAgCausador as $dados) {
        $agCausador[$dados['cod_ag_causador']] = $dados['nome_agente'];
    }

    $veiculo = array();
    $selectVeiculo = $this->medoo->select("automovel", ['cod_automovel', 'nome_automovel', 'numero_automovel']);
    foreach ($selectVeiculo as $dados) {
        $veiculo[$dados['cod_automovel']] = $dados['nome_automovel'] . ' - ' . $dados['numero_automovel'];
    }

    $tipoFechamento = array();
    $selectTipoFechamento = $this->medoo->select("tipo_fechamento", ['cod_tipo_fechamento', "nome_fechamento"]);
    foreach ($selectTipoFechamento as $dados) {
        $tipoFechamento[$dados['cod_tipo_fechamento']] = $dados['nome_fechamento'];
    }

    $tipoPendencia = array();
    $selectTipoPendencia = $this->medoo->select("tipo_pendencia", ['cod_tipo_pendencia', "nome_tipo_pendencia"]);
    foreach ($selectTipoPendencia as $dados) {
        $tipoPendencia[$dados['cod_tipo_pendencia']] = $dados['nome_tipo_pendencia'];
    }

    $pendencia = array();
    $selectPendencia = $this->medoo->select("pendencia", ['cod_pendencia', "nome_pendencia", "cod_tipo_pendencia"]);
    foreach ($selectPendencia as $dados) {
        $pendencia[$dados['cod_pendencia']] = $dados;
    }

    $funcionario = array();
    $selectFuncionario = $this->medoo->select("funcionario",
    ["[><]funcionario_empresa" => "cod_funcionario_empresa"], 
    ["funcionario_empresa.matricula", "funcionario.cod_funcionario", "nome_funcionario"]);
    foreach ($selectFuncionario as $dados) {
        $funcionario[$dados['cod_funcionario']] = $dados['nome_funcionario'] . " - " . $dados['matricula'];
    }
    ///////////////////////////

    $codigosOsmp = array();

    foreach ($_SESSION['resultadoPesquisaOsmp'] as $dados => $value){
        $codigosOsmp[] = $value['cod_osmp'];
    }

    switch ($_SESSION['resultadoPesquisaOsmp'][0]['form']){
        case 'ed':
            $colunas = "l.cod_linha AS linha,
                        l.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        ss.complemento AS complemento_local,
                        'Edificações' AS grupo,
                        cod_sistema AS sistema,
                        cod_subsistema AS sub_sistema,
                        cod_servico_pmp AS servico,
                        cod_procedimento AS procedimento,
                        cod_tipo_periodicidade AS periodicidade,
                        mao_obra AS efetivo,
                        horas_uteis AS tempo,
                        'Horas' AS unidade_tempo,
                        homem_hora AS total_servico,
                        'Horas' AS unidade_medida,";

            $joins = "JOIN ssmp_ed s USING(cod_ssmp)
                      JOIN local l USING(cod_local)
                      JOIN pmp_edificacao USING(cod_pmp_edificacao)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'vp':
            $colunas = "ei.cod_linha AS linha,
                        '' AS local,
                        ei.nome_estacao AS estacao_inicial,
                        ef.nome_estacao AS estacao_final,
                        s.cod_via AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        s.posicao AS posicao,
                        s.km_inicial AS km_inicial,
                        s.km_final AS km_final,
                        cod_amv AS amv,
                        ss.complemento AS complemento_local,
                        'Via Permanente' AS grupo,
                        cod_sistema AS sistema,
                        cod_subsistema AS sub_sistema,
                        cod_servico_pmp AS servico,
                        cod_procedimento AS procedimento,
                        cod_tipo_periodicidade AS periodicidade,
                        mao_obra AS efetivo,
                        horas_uteis AS tempo,
                        'Horas' AS unidade_tempo,
                        homem_hora AS total_servico,
                        'Horas' AS unidade_medida,";

            $joins = "JOIN ssmp_vp s USING(cod_ssmp)
                      JOIN estacao ei ON s.cod_estacao_inicial = ei.cod_estacao
                      JOIN estacao ef ON s.cod_estacao_final = ef.cod_estacao
                      JOIN pmp_via_permanente USING(cod_pmp_via_permanente)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'su':
            $colunas = "l.cod_linha AS linha,
                        l.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        ss.complemento AS complemento_local,
                        'Subestação' AS grupo,
                        cod_sistema AS sistema,
                        cod_subsistema AS sub_sistema,
                        cod_servico_pmp AS servico,
                        cod_procedimento AS procedimento,
                        cod_tipo_periodicidade AS periodicidade,
                        mao_obra AS efetivo,
                        horas_uteis AS tempo,
                        'Horas' AS unidade_tempo,
                        homem_hora AS total_servico,
                        'Horas' AS unidade_medida,";

            $joins = "JOIN ssmp_su s USING(cod_ssmp)
                      JOIN local l USING(cod_local)
                      JOIN pmp_subestacao USING(cod_pmp_subestacao)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'ra':
            $colunas = "linha.cod_linha AS linha,
                        local.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        via.cod_via AS via,
                        nome_poste AS poste_inicial,
                        '' AS poste_final,
                        s.posicao AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        ss.complemento AS complemento_local,
                        'Rede Aerea' AS grupo,
                        cod_sistema AS sistema,
                        cod_subsistema AS sub_sistema,
                        cod_servico_pmp AS servico,
                        cod_procedimento AS procedimento,
                        cod_tipo_periodicidade AS periodicidade,
                        mao_obra AS efetivo,
                        horas_uteis AS tempo,
                        'Horas' AS unidade_tempo,
                        homem_hora AS total_servico,
                        'Horas' AS unidade_medida,";

            $joins = "JOIN ssmp_ra s USING(cod_ssmp)
                      JOIN local USING(cod_local)
                      JOIN linha USING (cod_linha)
                      LEFT JOIN poste USING(cod_poste)
                      LEFT JOIN via USING(cod_via)
                      JOIN pmp_rede_aerea USING(cod_pmp_rede_aerea)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'tl':
            $colunas = "l.cod_linha AS linha,
                        l.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        ss.complemento AS complemento_local,
                        'Telecom' AS grupo,
                        cod_sistema AS sistema,
                        cod_subsistema AS sub_sistema,
                        cod_servico_pmp AS servico,
                        cod_procedimento AS procedimento,
                        cod_tipo_periodicidade AS periodicidade,
                        mao_obra AS efetivo,
                        horas_uteis AS tempo,
                        'Horas' AS unidade_tempo,
                        homem_hora AS total_servico,
                        'Horas' AS unidade_medida,";

            $joins = "JOIN ssmp_te s USING(cod_ssmp)
                      JOIN local l USING(cod_local)
                      JOIN pmp_telecom USING(cod_pmp_telecom)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'bl':
            $colunas = "l.cod_linha AS linha,
                        '' AS local,
                        l.nome_estacao AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        ss.complemento AS complemento_local,
                        'Bilhetagem' AS grupo,
                        cod_sistema AS sistema,
                        cod_subsistema AS sub_sistema,
                        cod_servico_pmp AS servico,
                        cod_procedimento AS procedimento,
                        cod_tipo_periodicidade AS periodicidade,
                        mao_obra AS efetivo,
                        horas_uteis AS tempo,
                        'Horas' AS unidade_tempo,
                        homem_hora AS total_servico,
                        'Horas' AS unidade_medida,";

            $joins = "JOIN ssmp_bi s USING(cod_ssmp)
                      JOIN estacao l USING(cod_estacao)
                      JOIN pmp_bilhetagem USING(cod_pmp_bilhetagem)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
    }

    $colunas = utf8_decode($colunas);

    $sql = "SELECT
              o.cod_osmp, o.cod_ssmp, o.usuario_responsavel,
              o.data_abertura AS data_abertura_osmp,
              ss.data_abertura AS data_abertura_ssmp,
              ss.data_programada,
              sto.cod_status, sto.data_status,
              sto.descricao AS descricao_status,
              oe.cod_funcionario,
              {$colunas}
              * 
                FROM osmp o
                {$joins}
                LEFT JOIN osmp_tempo USING(cod_osmp)
                LEFT JOIN osmp_registro USING(cod_osmp)
                LEFT JOIN osmp_encerramento oe USING(cod_osmp)
                JOIN status_osmp sto USING(cod_status_osmp)
                JOIN ssmp ss USING(cod_ssmp)";

    if (count($codigosOsmp)) {
        $sql = $sql . ' WHERE o.cod_osmp IN(' . implode(' , ', $codigosOsmp) . ')';
    }

    $osmps = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    foreach ($osmps as $dados => $value) {
        if(!empty($value['clima'])) {
            switch ($value['clima']){
                case "b":
                    $clima = "Bom";
                    break;
                case "c":
                    $clima = "Chuvoso";
                    break;
                case "n":
                    $clima = "Nublado";
                    break;
                case "e":
                    $clima = "Neblina";
                    break;
            }
        }else
            $clima = '';

        $txt = $value['cod_osmp'] . ":::";
        $txt .= $value['cod_ssmp'] . ":::";
        $txt .= $responsavel[$value['usuario_responsavel']] . ":::";
        $txt .= $this->parse_timestamp($value['data_abertura_osmp']) . ":::";
        $txt .= $this->parse_timestamp($value['data_abertura_ssmp']) . ":::";
        $txt .= $this->parse_timestamp($value['data_programada']) . ":::";

        $txt .= $status[$value['cod_status']] . ":::";
        $txt .= $this->parse_timestamp($value['data_status']) . ":::";
        $txt .= $value['descricao_status'] . ":::";
        $txt .= $this->parse_timestamp($value['data_encerramento']) . ":::";



        $txt .= $linha[$value['linha']] . ":::";
        $txt .= $value['local'] . ":::";
        $txt .= $value['estacao_inicial'] . ":::";
        $txt .= $value['estacao_final'] . ":::";
        $txt .= $via[$value['via']] . ":::";
        $txt .= $value['poste_inicial'] . ":::";
        $txt .= $value['poste_final'] . ":::";
        $txt .= $value['posicao'] . ":::";
        $txt .= $value['km_inicial'] . ":::";
        $txt .= $value['km_final'] . ":::";
        $txt .= $amv[$value['amv']] . ":::";
        $txt .= $value['complemento_local'] . ":::";

        $txt .= $value['grupo'] . ":::";
        $txt .= $sistema[$value['sistema']] . ":::";
        $txt .= $subsistema[$value['sub_sistema']] . ":::";

        $txt .= $servico[$value['servico']] . ":::";
        $txt .= $procedimento[$value['procedimento']] . ":::";
        $txt .= $periodicidade[$value['periodicidade']] . ":::";

        $txt .= $value['efetivo'] . ":::";
        $txt .= $value['tempo'] . ":::";
        $txt .= $value['unidade_tempo'] . ":::";
        $txt .= $value['total_servico'] . ":::";
        $txt .= $value['unidade_medida'] . ":::";



        $txt .= $this->parse_timestamp($value['data_recebimento']) . ":::";
        $txt .= atraso($value['atraso_recebimento']) . ":::";

        $txt .= $this->parse_timestamp($value['data_preparacao']) . ":::";
        $txt .= atraso($value['atraso_preparacao']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_pronto']) . ":::";
        $txt .= atraso($value['atraso_ace_pronto']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_saida_autorizada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_saida']) . ":::";
        $txt .= atraso($value['atraso_ace_saida']) . ":::";

        $txt .= $this->parse_timestamp($value['data_ace_chegada']) . ":::";
        $txt .= atraso($value['atraso_ace_chegada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_exec_inicio']) . ":::";
        $txt .= atraso($value['atraso_exec_inicio']) . ":::";

        $txt .= $this->parse_timestamp($value['data_exec_termino']) . ":::";
        $txt .= atraso($value['atraso_exec_termino']) . ":::";

        $txt .= $this->parse_timestamp($value['data_reg_pronto']) . ":::";
        $txt .= atraso($value['atraso_reg_pronto']) . ":::";

        $txt .= $this->parse_timestamp($value['data_reg_saida_autorizada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_reg_chegada']) . ":::";
        $txt .= atraso($value['atraso_reg_chegada']) . ":::";

        $txt .= $this->parse_timestamp($value['data_desmobilizacao']) . ":::";
        $txt .= $value['descricao_tempo'] . ":::";

        $txt .= $unEquipe[$value['cod_un_equipe']]['nome_unidade'] . ":::";
        $txt .= $unEquipe[$value['cod_un_equipe']]['nome_equipe'] . ":::";

        $txt .= $causa[$value['cod_causa']] . ":::";
        $txt .= $value['desc_causa'] . ":::";

        $txt .= $atuacao[$value['cod_atuacao']] . ":::";
        $txt .= $value['desc_atuacao'] . ":::";

        $txt .= $agCausador[$value['cod_ag_causador']] . ":::";

        $txt .= $clima . ":::";
        $txt .= $value['temperatura'] . ":::";

        $txt .= $veiculo[$value['cod_automovel']] . ":::";

        $txt .= $value['cautela'] . ":::";
        $txt .= $value['km_inicial_cautela'] . ":::";
        $txt .= $value['km_final_cautela'] . ":::";
        $txt .= $value['restricao_veloc'] . ":::";
        $txt .= $value['dados_complementar'] . ":::";

        $txt .= $tipoFechamento[$value['cod_tipo_fechamento']] . ":::";

        $txt .= $value['liberacao'] . ":::";

        $txt .= $tipoPendencia[$pendencia[$value["cod_pendencia"]]['cod_tipo_pendencia']] . ":::";
        $txt .= $pendencia[$value["cod_pendencia"]]['nome_pendencia'] . ":::";

        $txt .= $value['descricao_encerramento'] . ":::";

        $txt .= $funcionario[$value['cod_funcionario']] . ":::";

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("OsmpCompleta.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OsmpCompleta.csv");
readfile(TEMP_PATH.'OsmpCompleta.csv');

unlink(TEMP_PATH."OsmpCompleta.csv");

unset($_SESSION['resultadoPesquisaOsmp']);