<?php

$conteudo = "N. Osmp;";
$conteudo .= "Data Abertura OSMP;";
$conteudo .= "Linha;";
$conteudo .= "Material;";
$conteudo .= "Detalhes Material;";
$conteudo .= "Unidade;";
$conteudo .= "Estado;";
$conteudo .= "Utilizado;";
$conteudo .= "Origem;";
$conteudo .= "Detalhes Origem;";
$conteudo .= "Tipo de Material\n";

$conteudo = utf8_decode($conteudo);

if(!empty($_SESSION['resultadoPesquisaOsmp'])) {
    $material = array();
    $selectMaterial = $this->medoo->select("v_material", ["cod_material", "nome_material", "descricao_material", "nome_tipo_material"]);
    foreach ($selectMaterial as $dados){
        $material[$dados['cod_material']] = $dados;
    }

    $unidade = array();
    $selectUnidade = $this->medoo->select("unidade_medida", ["cod_uni_medida", "nome_uni_medida", "sigla_uni_medida"]);
    foreach ($selectUnidade as $dados){
        $unidade[$dados['cod_uni_medida']] = "({$dados['sigla_uni_medida']})  {$dados['nome_uni_medida']}";
    }

    $estado = array('n' => 'Novo', 'u' => 'Usado');
    $origem = array('s' => 'MetroService', 'f' => 'MetroFor', 'o' => 'Outros');

    foreach ($_SESSION['resultadoPesquisaOsmp'] as $d => $v) {
        $selectOsmpMaterial = $this->medoo->select('osmp_material','*',["cod_osmp" => (int)$v['cod_osmp']]);

        foreach ($selectOsmpMaterial as $dados => $value) {
            $txt = $value['cod_osmp'] . ":::";
            $txt .= $v['data_abertura'] . ":::";
            $txt .= $v['nome_linha'] . ":::";
            $txt .= $material[$value['cod_material']]['nome_material'] . ":::";
            $txt .= $material[$value['cod_material']]['descricao_material'] . ":::";
            $txt .= $unidade[$value['unidade']] . ":::";
            $txt .= $estado[$value['estado']] . ":::";
            $txt .= $value['utilizado'] . ":::";
            $txt .= $origem[$value['origem']] . ":::";
            $txt .= $value['descricao_origem'] . ":::";

            $txt .= $material[$value['cod_material']]['nome_tipo_material'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("OsmpMateriais.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OsmpMateriais.csv");
readfile(TEMP_PATH.'OsmpMateriais.csv');

unlink(TEMP_PATH."OsmpMateriais.csv");

unset($_SESSION['resultadoPesquisaOsmp']);