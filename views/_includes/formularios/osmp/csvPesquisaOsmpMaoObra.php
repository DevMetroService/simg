<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 18/08/2016
 * Time: 11:14
 */

$conteudo = "N. Osmp;";
$conteudo .= "Nome Funcionario;";
$conteudo .= "Data Inicio;";
$conteudo .= "Data Final;";
$conteudo .= "Total Horas\n";

$conteudo = utf8_decode($conteudo);

if(!empty($_SESSION['resultadoPesquisaOsmp'])) {
    $responsavel = array();
    $selectResponsavel = $this->medoo->select("funcionario", ["matricula", "cod_funcionario", "nome_funcionario"]);
    foreach ($selectResponsavel as $dados){
        $responsavel[$dados['cod_funcionario']] = $dados['nome_funcionario'];
    }

    foreach ($_SESSION['resultadoPesquisaOsmp'] as $d => $v) {
        $selectMaoObra = $this->medoo->select('osmp_mao_de_obra','*',["cod_osmp" => (int)$v['cod_osmp']]);

        foreach ($selectMaoObra as $dados => $value) {
            $txt = $value['cod_osmp'] . ":::";
            $txt .= $responsavel[$value['cod_funcionario']] . ":::";
            $txt .= $value['data_inicio'] . ":::";
            $txt .= $value['data_termino'] . ":::";
            $txt .= $value['total_hrs'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("OsmpMaoObra.csv",$conteudo);



header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=OsmpMaoObra.csv");
readfile(TEMP_PATH.'OsmpMaoObra.csv');

unlink(TEMP_PATH."OsmpMaoObra.csv");

unset($_SESSION['resultadoPesquisaOsmp']);
