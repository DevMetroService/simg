<div class="page-header">
    <h1><?php echo $tituloOs ?></h1>
</div>

<div class="row">
    <form class="form-group" method="post" id="osDadosGerais" action="<?php echo HOME_URI . "/" . $actionForm . "/salvarDadosGerais"; ?>">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Dados Gerais</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Equipe</label>
                                                <input class="form-control" value="<?php echo $dados['nome_equipe'] ?>" disabled/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Unidade</label>
                                                <input disabled class="form-control" value="<?php echo $dados['cod_unidade'] . ' - ' . $dados['nome_unidade'] ?>"/>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Data / Hora Abertura</label>
                                                <input disabled class="form-control" value="<?php echo $this->parse_timestamp($dados['data_abertura']) ?>">
                                            </div>

                                            <div class="col-md-2">
                                                <label>C�digo OS</label>
                                                <input id="dta" readonly name="codigoOs" class="form-control" value="<?php echo($_SESSION['refillOs']['codigoOs']); ?>">

                                                <?php
                                                if ($tipoOS != 'osm' || $tipoOS != 'osp') {
                                                    echo('<input name="form" type="hidden" required class="free" value="'.$_SESSION['refillOs']['form'].'" />');
                                                }
                                                ?>
                                            </div>

                                            <div class="col-md-2">
                                                <label>C�digo SSMP</label>
                                                <input readonly class="form-control" value="<?php echo($dados['cod_ssmp']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        require_once(ABSPATH . "/views/_includes/formularios/subForm/identificacao_local_pmp_" . $_SESSION['refillOs']['form'] . ".php");
                        if ($tipoOS != 'osmp/Tue') {
                            ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><label>Servi�o a ser executado</label></div>

                                        <div class="panel-body">

                                            <?php
                                            require_once(ABSPATH . "/views/_includes/formularios/subForm/servico_preventivo.php");
                                            ?>

                                            <?php
                                            if ($tipoOS != 'osmp/Vlt') {
                                                echo('
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Efetivo</label>
                                                        <input class="form-control" disabled value="'. $dados['mao_obra'] .'"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Tempo</label>
                                                        <input class="form-control number" disabled value="'. $dados['horas_uteis'] .'"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Unidade</label>
                                                        <input disabled class="form-control" value="Horas"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Total Servi�o</label>
                                                        <input class="form-control number" disabled value="'. $dados['homem_hora'] .'"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Unidade</label>
                                                        <input disabled class="form-control" value="Horas"/>
                                                    </div>
                                                </div>
                                            ');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <label>Preenchimento</label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Respons�vel</label>
                                                                <input disabled class="form-control" value="<?php if(!empty($dados['usuario'])) echo $dados['usuario'] ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <label>Situa��o da OS</label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Situa��o</label>
                                                                <input disabled class="form-control" value="<?php echo $dados['nome_status'] ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <label>Condi��o da Programa��o</label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Condi��o</label>
                                                                <input name="descricao" class="form-control" disabled value="<?php echo ($dados['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado' ?>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                        if (!empty($selectDescStatus['nome_status']) && ($selectDescStatus['nome_status'] == 'N�o Executado' || $selectDescStatus['nome_status'] == 'Duplicado' || $selectDescStatus['nome_status'] == 'N�o Configura falha')){
                                            echo "<div class='row'>
                                                <div class='col-lg-12'>
                                                    <div class='panel panel-default'>
                                                        <div class='panel-heading'><label>Situa��o da Solicita��o</label></div>
            
                                                        <div class='panel-body'>
                                                            <div class='row'>
                                                                <div class='col-md-2'>
                                                                    <label for='nomeStatusSaf'>Status</label>
                                                                    <input id='nomeStatusSaf' type='text' class='form-control' disabled
                                                                           value='{$selectDescStatus['nome_status']}'/>
                                                                </div>";
                                            if($selectDescStatus['cod_status'] == 24){
                                                echo"<div class='col-md-8'>";
                                                $divDuplicada = ($tipoOS == 'osm') ? "<label>C�digo duplicado:</label> <input type='button' class='btnOsmDuplicado form-control' value='{$selectDescStatus['cod_osm_duplicado']}'>" : "<label>C�digo duplicado:</label> <input type='button' class='btnOspDuplicado form-control' value='{$selectDescStatus['cod_osp_duplicado']}'>";
                                            }else{
                                                echo"<div class='col-md-10'>";
                                            }
                                            echo"<label for='descricaoStatusSaf'>Descri��o</label>
                                                                    <textarea id='descricaoStatusSaf' class='form-control' disabled
                                                                                  spellcheck='true'>{$selectDescStatus['descricao']}";
                                            echo "</textarea>
                                                                </div>";
                                            if(!empty($divDuplicada)){
                                                echo"<div class=col-md-2>{$divDuplicada}</div>";
                                            }
                                            echo"</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>"; }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                            require_once(ABSPATH . "/views/_includes/formularios/os/btnNavegacao.php");
                            if(!empty($modal)) echo $modal;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>