<?php

$conteudo = 'N. Ssmp;';
$conteudo .= 'Data Abertura;';
$conteudo .= 'Data Programada;';
$conteudo .= 'Responsavel pelo Preenchimento;';

$conteudo .= 'Status;';
$conteudo .= 'Data Referente ao Status;';
$conteudo .= "Descrição Status;";

$conteudo .= 'Origem;';
$conteudo .= 'Tipo de Intervenção;';
$conteudo .= 'Qtd de Dias;';
$conteudo .= 'SA;';

$conteudo .= "Linha;";
$conteudo .= "Local;";
$conteudo .= "Estação Inicial;";
$conteudo .= "Estação Final;";
$conteudo .= "Via;";
$conteudo .= "Poste Inicial;";
$conteudo .= "Poste Final;";
$conteudo .= "Posição;";
$conteudo .= "Km Inicial;";
$conteudo .= "Km Final;";
$conteudo .= "AMV;";
$conteudo .= "Complemento Local;";

$conteudo .= 'Grupo de Sistemas;';
$conteudo .= 'Sistema;';
$conteudo .= 'Sub-Sistema;';

$conteudo .= 'Serviço;';
$conteudo .= 'Procedimento;';
$conteudo .= 'Periodicidade;';

$conteudo .= 'Unidade;';
$conteudo .= "Equipe;\n";

$conteudo = utf8_decode($conteudo);

if(!empty($_SESSION['resultadoPesquisaSsmp'])){

    //// Funções basicas executadas apenas uma vez no loop ////

    $responsavel = array();
    $selectResponsavel = $this->medoo->select("usuario", ["cod_usuario", "usuario"]);
    foreach ($selectResponsavel as $dados) {
        $responsavel[$dados['cod_usuario']] = $dados['usuario'];
    }

    $status = array();
    $selectStatus = $this->medoo->select("status", ["cod_status", "nome_status"]);
    foreach ($selectStatus as $dados) {
        $status[$dados['cod_status']] = $dados['nome_status'];
    }

    $linha = array();
    $selectLinha = $this->medoo->select("linha", ["cod_linha", "nome_linha"]);
    foreach ($selectLinha as $dados) {
        $linha[$dados['cod_linha']] = $dados['nome_linha'];
    }

    $via = array();
    $selectVia = $this->medoo->select("via", ["cod_via", "nome_via"]);
    foreach ($selectVia as $dados) {
        $via[$dados['cod_via']] = $dados['nome_via'];
    }

    $amv = array();
    $selectAmv = $this->medoo->select("amv", ["cod_amv", "nome_amv"]);
    foreach ($selectAmv as $dados) {
        $amv[$dados['cod_amv']] = $dados['nome_amv'];
    }

    $sistema = array();
    $selectSistema = $this->medoo->select("sistema", ["cod_sistema", "nome_sistema"]);
    foreach ($selectSistema as $dados) {
        $sistema[$dados['cod_sistema']] = $dados['nome_sistema'];
    }

    $subsistema = array();
    $selectSubsistema = $this->medoo->select("subsistema", ["cod_subsistema", "nome_subsistema"]);
    foreach ($selectSubsistema as $dados) {
        $subsistema[$dados['cod_subsistema']] = $dados['nome_subsistema'];
    }

    $servico = array();
    $selectServico = $this->medoo->select("servico_pmp", ["cod_servico_pmp", "nome_servico_pmp"]);
    foreach ($selectServico as $dados) {
        $servico[$dados['cod_servico_pmp']] = $dados['nome_servico_pmp'];
    }

    $procedimento = array();
    $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"]);
    foreach ($selectProcedimento as $dados) {
        $procedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];
    }

    $periodicidade = array();
    $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
    foreach ($selectPeriodicidade as $dados) {
        $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];
    }

    $unEquipe = array();
    $selectUnEquipe = $this->medoo->select("un_equipe",
        [
            "[><]equipe" => "cod_equipe",
            "[><]unidade" => "cod_unidade"
        ], "*");
    foreach ($selectUnEquipe as $dados) {
        $unEquipe[$dados['cod_un_equipe']] = $dados;
    }

    ///////////////////////////

    $codigosSsmp = array();

    foreach ($_SESSION['resultadoPesquisaSsmp'] as $dados => $value){
        $codigosSsmp[] = $value['cod_ssmp'];
    }

    switch ($_SESSION['resultadoPesquisaSsmp'][0]['form']){
        case 'ed':
            $colunas = "l.cod_linha AS linha,
                        l.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        'Edificações' AS grupo";

            $joins = "ssmp_ed s
                      JOIN ssmp ss USING(cod_ssmp)
                      JOIN status_ssmp st USING(cod_status_ssmp)
                      JOIN local l USING(cod_local)
                      JOIN pmp_edificacao USING(cod_pmp_edificacao)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'vp':
            $colunas = "ei.cod_linha AS linha,
                        '' AS local,
                        ei.nome_estacao AS estacao_inicial,
                        ef.nome_estacao AS estacao_final,
                        s.cod_via AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        s.posicao AS posicao,
                        s.km_inicial AS km_inicial,
                        s.km_final AS km_final,
                        cod_amv AS amv,
                        'Via Permanente' AS grupo";

            $joins = "ssmp_vp s
                      JOIN ssmp ss USING(cod_ssmp)
                      JOIN status_ssmp st USING(cod_status_ssmp)
                      JOIN estacao ei ON s.cod_estacao_inicial = ei.cod_estacao
                      JOIN estacao ef ON s.cod_estacao_final = ef.cod_estacao
                      JOIN pmp_via_permanente USING(cod_pmp_via_permanente)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'su':
            $colunas = "l.cod_linha AS linha,
                        l.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        'Subestação' AS grupo";

            $joins = "ssmp_su s
                      JOIN ssmp ss USING(cod_ssmp)
                      JOIN status_ssmp st USING(cod_status_ssmp)
                      JOIN local l USING(cod_local)
                      JOIN pmp_subestacao USING(cod_pmp_subestacao)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'ra':
            $colunas = "local.cod_linha AS linha,
                        local.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        s.cod_via AS via,
                        s.cod_poste AS poste_inicial,
                        '' AS poste_final,
                        s.posicao AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        'Rede Aerea' AS grupo";

            $joins = "ssmp_ra s 
                      JOIN ssmp ss USING(cod_ssmp)
                      JOIN status_ssmp st USING(cod_status_ssmp)
                      JOIN local USING(cod_local)
                      JOIN pmp_rede_aerea USING(cod_pmp_rede_aerea)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'tl':
            $colunas = "l.cod_linha AS linha,
                        l.nome_local AS local,
                        '' AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        'Telecom' AS grupo";

            $joins = "ssmp_te s
                      JOIN ssmp ss USING(cod_ssmp)
                      JOIN status_ssmp st USING(cod_status_ssmp)
                      JOIN local l USING(cod_local)
                      JOIN pmp_telecom USING(cod_pmp_telecom)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
        case 'bl':
            $colunas = "l.cod_linha AS linha,
                        '' AS local,
                        l.nome_estacao AS estacao_inicial,
                        '' AS estacao_final,
                        '' AS via,
                        '' AS poste_inicial,
                        '' AS poste_final,
                        '' AS posicao,
                        '' AS km_inicial,
                        '' AS km_final,
                        '' AS amv,
                        'Bilhetagem' AS grupo";

            $joins = "ssmp_bi s
                      JOIN ssmp ss USING(cod_ssmp)
                      JOIN status_ssmp st USING(cod_status_ssmp)
                      JOIN estacao l USING(cod_estacao)
                      JOIN pmp_bilhetagem USING(cod_pmp_bilhetagem)
                      JOIN pmp USING(cod_pmp)
                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                      JOIN sub_sistema USING(cod_sub_sistema)";
            break;
    }

    if(!empty($joins)) {
        $sql = "SELECT
                ss.cod_ssmp,
                ss.data_abertura,
                ss.data_programada,
                ss.cod_usuario AS usuario_responsavel,
                st.cod_status,
                st.data_status,
                st.descricao,
                ss.dias_servico,
                ss.solicitacao_acesso,
                ss.complemento AS complemento_local,
                cod_sistema AS sistema,
                cod_subsistema AS sub_sistema,
                cod_servico_pmp AS servico,
                cod_procedimento AS procedimento,
                cod_tipo_periodicidade AS periodicidade,
                ss.cod_un_equipe,
                {$colunas} FROM {$joins}";

        $sql = utf8_decode($sql);

        if (count($codigosSsmp)) {
            $sql = $sql . ' WHERE ss.cod_ssmp IN(' . implode(' , ', $codigosSsmp) . ')';
        }

        $ssmps = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        foreach ($ssmps as $dados => $value) {
            $txt = $value['cod_ssmp'] . ":::";
            $txt .= $this->parse_timestamp($value['data_abertura']) . ":::";
            $txt .= $this->parse_timestamp($value['data_programada']) . ":::";
            $txt .= $responsavel[$value['usuario_responsavel']] . ":::";

            $txt .= $status[$value['cod_status']] . ":::";
            $txt .= $this->parse_timestamp($value['data_status']) . ":::";
            $txt .= $value['descricao'] . ":::";

            $txt .= utf8_decode("Preventiva") . ":::";
            $txt .= utf8_decode("Manut. Peventiva") . ":::";
            $txt .= $value['dias_servico'] . ":::";
            $txt .= $value['solicitacao_acesso'] . ":::";

            $txt .= $linha[$value['linha']] . ":::";
            $txt .= $value['local'] . ":::";
            $txt .= $value['estacao_inicial'] . ":::";
            $txt .= $value['estacao_final'] . ":::";
            $txt .= $via[$value['via']] . ":::";
            $txt .= $value['poste_inicial'] . ":::";
            $txt .= $value['poste_final'] . ":::";
            $txt .= $value['posicao'] . ":::";
            $txt .= $value['km_inicial'] . ":::";
            $txt .= $value['km_final'] . ":::";
            $txt .= $amv[$value['amv']] . ":::";
            $txt .= $value['complemento_local'] . ":::";

            $txt .= $value['grupo'] . ":::";
            $txt .= $sistema[$value['sistema']] . ":::";
            $txt .= $subsistema[$value['sub_sistema']] . ":::";

            $txt .= $servico[$value['servico']] . ":::";
            $txt .= $procedimento[$value['procedimento']] . ":::";
            $txt .= $periodicidade[$value['periodicidade']] . ":::";

            $txt .= $unEquipe[$value['cod_un_equipe']]['nome_unidade'] . ":::";
            $txt .= $unEquipe[$value['cod_un_equipe']]['nome_equipe'] . ":::";

            //Excluir quebras de linha
            $txt = str_replace("\r\n", " ", trim($txt));
            $txt = str_replace(";", " ", trim($txt));
            $txt = str_replace("<BR />", " ", trim($txt));

            $txt = str_replace(":::", ";", trim($txt));

            $conteudo .= $txt . "\n";
        }
    }
}

$this->salvaTxt("SsmpCompleta.csv",$conteudo);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=SsmpCompleta.csv");
readfile(TEMP_PATH.'SsmpCompleta.csv');

unlink(TEMP_PATH."SsmpCompleta.csv");

unset($_SESSION['resultadoPesquisaSsmp']);