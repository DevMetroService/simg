<div class="page-header">
    <h1>SSMP - <?php echo $nomeTipo ?></h1>
</div>

<div class="row">
    <form action="<?php echo HOME_URI; ?>/cadastroGeral/programarSsmp" class="form-group" method="post" id="formSsmp">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>Ssmp - <?php echo $nomeTipo ?></label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Dados Gerais</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-3">
                                            <label>Origem</label>
                                            <input value="Preventiva" disabled class="form-control"/>
                                            <input name="form" type="hidden" required class="free" value="<?php echo $tipo; ?>" />
                                        </div>
                                        <div class="col-md-10 col-xs-9">
                                            <label>Tipo de Interven��o</label>
                                            <input value="Manut. Preventiva" disabled class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Ssmp</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-8">
                                            <label>Data/Hora Programada</label>
                                            <a data-toggle="modal" data-target="#modalHelpProgramada" class="btn-circle hidden-print"><i
                                                    style="font-size: 20px" class="fa fa-question-circle fa-fw"></i></a>
                                            <input name="dataHoraProgramada" required value="<?php if (!empty($dados['data_programada'])) echo $this->parse_timestamp($dados['data_programada']) ?>" type="text" class="form-control timestamp validaDataHora"/>
                                        </div>
                                        <div id="diasServico" class="col-md-2 col-xs-4">
                                            <label>Qtd. de Dias</label>
                                            <?php if($dados['cod_tipo_periodicidade'] == 1){?>
                                                <input name="diasServico" required readonly value="15" class="form-control number"/>
                                            <?php }else{ ?>
                                                <input name="diasServico" required value="<?php echo (!empty($dados['dias_servico'])) ? $dados['dias_servico'] : 1 ?>" class="form-control number"/>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>N�</label>
                                            <input name="codigoSsmp" value="<?php echo $dados['cod_ssmp'] ?>" type="text" readonly class="form-control"/>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>S.A.</label>
                                            <input name="numeroSolicitacaoAcesso" value="<?php if (!empty($dados['solicitacao_acesso'])) echo $dados['solicitacao_acesso'] ?>" type="text" class="form-control"/>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label>Status</label>
                                            <input name="status" value="<?php echo $dados['nome_status'] ?>" class="form-control" disabled/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Preenchimento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <label>Data/Hora Abertura</label>
                                            <input value="<?php echo($this->parse_timestamp($dados['data_abertura'])); ?>" class="form-control" disabled/>
                                        </div>
                                        <div class="col-md-5 col-xs-6">
                                            <label>Respons�vel</label>
                                            <input value="<?php echo $dados['usuario']; ?>" class="form-control" disabled/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    require_once(ABSPATH . "/views/_includes/formularios/subForm/identificacao_local_pmp_" . $tipo . ".php");

                    if($tipo != 'tue') {
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><label>Servi�o</label></div>

                                    <div class="panel-body">
                                        <?php
                                        require_once(ABSPATH . "/views/_includes/formularios/subForm/servico_preventivo.php");
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Encaminhamento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label>Unidade</label>

                                            <?php
                                            switch ($dados['cod_linha']){
                                                case 2:
                                                    $nomeUnidade = "Centro de Manunten��o do Metr� do Cariri";
                                                    $codUnidade = 5;
                                                    break;
                                                case 7:
                                                    $nomeUnidade = "Centro de Manunten��o do Metr� de Sobral";
                                                    $codUnidade = 4;
                                                    break;
                                                default:
                                                    $nomeUnidade = "Matriz Couto Fernandes - Fortaleza";
                                                    $codUnidade = 1;
                                            }
                                            ?>

                                            <input value="<?php echo $nomeUnidade; ?>" class="form-control" disabled/>
                                            <input type="hidden" name="unidade" value="<?php echo $codUnidade; ?>" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <label>Equipe</label>
                                            <input value="<?php echo $nomeTipo ?>" disabled class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <div class="col-md-offset-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">A��es</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group  btn-group-justified " role="group">

                                                <?php echo $btnsAcao; ?>

                                            </div>
                                            <div class="modal fade modal-historico" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                                                                <i aria-hidden="true" class="fa fa-close"></i></button>
                                                            <h4 class="modal-title">Hist�rico da SSMP</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <?php
                                                            if(!empty($dados)) {
                                                                $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_ssmp s 
                                                              JOIN usuario u on s.usuario = u.cod_usuario
                                                              JOIN status USING(cod_status)
                                                              WHERE cod_ssmp = {$dados['cod_ssmp']} ORDER BY data_status";

                                                                if ($sql != "")
                                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                            }
                                                            ?>

                                                            <div class = "row">
                                                                <div class="col-md-12">
                                                                    <table class="tableRel table-bordered dataTable">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Status</th>
                                                                            <th>Responsavel</th>
                                                                            <th>Data da altera��o</th>
                                                                            <th>Complemento</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php
                                                                        if(!empty($result)){
                                                                            foreach ($result as $dados){
                                                                                echo("<tr>");
                                                                                echo("<td>{$dados['nome_status']}</td>");
                                                                                echo("<td>{$dados['usuario']}</td>");
                                                                                echo("<td>{$this->parse_timestamp($dados['data_status'])}</td>");
                                                                                echo("<td>{$dados['descricao']}</td>");
                                                                                echo("</tr>");
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>