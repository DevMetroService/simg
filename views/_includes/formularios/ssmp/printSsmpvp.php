<?php
if(!empty ($_SESSION['refillSsmp'])){
    $sql = "SELECT ssmp_vp.cod_ssmp, data_abertura, solicitacao_acesso, dias_servico, 
                data_programada, usuario.usuario, nome_status, un_equipe.cod_unidade, unidade.nome_unidade,
                
                ei.nome_estacao AS estacao_inicial, ef.nome_estacao AS estacao_final,
                nome_via,ssmp_vp.posicao,ssmp_vp.km_inicial,ssmp_vp.km_final, l.nome_linha, l.cod_linha,
                ssmp_vp.km_inicial,ssmp_vp.km_final, nome_amv,
                
                nome_servico_pmp, nome_sistema, nome_subsistema, nome_procedimento, 
                ssmp.complemento, nome_periodicidade, cod_tipo_periodicidade
            from ssmp_vp
            JOIN ssmp USING(cod_ssmp)
            
            JOIN usuario USING(cod_usuario)
            JOIN status_ssmp USING(cod_status_ssmp)
            JOIN status USING (cod_status)
            LEFT JOIN un_equipe USING(cod_un_equipe)
            LEFT JOIN unidade USING(cod_unidade)
            
            JOIN estacao ei ON ssmp_vp.cod_estacao_inicial = ei.cod_estacao
            JOIN linha l USING(cod_linha)
            JOIN estacao ef ON ssmp_vp.cod_estacao_final = ef.cod_estacao
            LEFT JOIN via USING(cod_via)
            
            JOIN pmp_via_permanente USING(cod_pmp_via_permanente)
            JOIN pmp USING(cod_pmp)
            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
            JOIN servico_pmp USING(cod_servico_pmp)
            JOIN sub_sistema USING(cod_sub_sistema)
            JOIN sistema USING(cod_sistema)
            JOIN subsistema USING(cod_subsistema)
            JOIN procedimento USING(cod_procedimento)
            JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
            LEFT JOIN amv USING(cod_amv)
            WHERE ssmp_vp.cod_ssmp = {$_SESSION['refillSsmp']['codigoSsmp']}";

    $Ssmp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $Ssmp = $Ssmp[0];
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>SSMP - Solicita��o de Servi�o de Manuten��o Preventiva</h3>
    </div>
    <hr/>

    <div class="title-table">&raquo; Dados Gerais</div>
    <table>
        <tr>
            <th>N�</th>
            <th>Responsavel Preenchimento</th>
            <th>Data/Hora Abertura</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['cod_ssmp']; ?></td>
            <td class="center"><?php echo $Ssmp['usuario']; ?></td>
            <td class="center"><?php echo $this->parse_timestamp($Ssmp['data_abertura']); ?></td>
        </tr>
        <tr>
            <th>Data/Hora Programada</th>
            <th>Qtd. de dias</th>
            <th>S.A.</th>
        </tr>
        <tr>
            <td class="center"><?php echo $this->parse_timestamp($Ssmp['data_programada']); ?></td>
            <td class="center"><?php echo $Ssmp['dias_servico']; ?></td>
            <td class="center"><?php echo $Ssmp['solicitacao_acesso']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Origem</div>
    <table>
        <tr>
            <th>Origem</th>
            <th>Status</th>
        </tr>
        <tr>
            <td class="center">Preventiva</td>
            <td class="center"><?php echo $Ssmp['nome_status']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Tipo Interven��o</th>
        </tr>
        <tr>
            <td colspan="2" class="center">Manut. Preventiva</td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o / Local</div>
    <table>
        <tr>
            <th>Linha</th>
            <th>Esta��o Inicial</th>
            <th>Esta��o Final</th>
            <th>Via</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['nome_linha']; ?></td>
            <td class="center"><?php echo $Ssmp['estacao_inicial']; ?></td>
            <td class="center"><?php echo $Ssmp['estacao_final']; ?></td>
            <td class="center"><?php echo $Ssmp['nome_via']; ?></td>
        </tr>
        <tr>
            <th>Km Inicial</th>
            <th>Km Final</th>
            <th colspan="2">AMV</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['km_inicial']; ?></td>
            <td class="center"><?php echo $Ssmp['km']; ?></td>
            <td colspan="2" class="center"><?php echo $Ssmp['nome_amv']; ?></td>
        </tr>
        <tr>
            <th colspan="4">Complemento</th>
        </tr>
        <tr>
            <td colspan="4"><?php echo $Ssmp['complemento']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Grupo</th>
            <th colspan="2">Sistema</th>
        </tr>
        <tr>
            <td colspan="2" class="center">Via Permanente</td>
            <td colspan="2" class="center"><?php echo $Ssmp['nome_sistema']; ?></td>
        </tr>
        <tr>
            <th colspan="4">SubSistema</th>
        </tr>
        <tr>
            <td colspan="4" class="center"><?php echo $Ssmp['nome_subsistema']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Servi�o</div>
    <table>
        <tr>
            <th colspan="2">Nome</th>
        </tr>
        <tr>
            <td colspan="2"><?php echo $Ssmp['nome_servico_pmp']; ?></td>
        </tr>
        <tr>
            <th>Procedimento</th>
            <th>Periodicidade</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['nome_procedimento']; ?></td>
            <td class="center"><?php echo $Ssmp['nome_periodicidade']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Encaminhamento</div>
    <table>
        <tr>
            <th>Unidade</th>
            <th>Equipe</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['nome_unidade']; ?></td>
            <td class="center">Via Permanente</td>
        </tr>
    </table>
</div>