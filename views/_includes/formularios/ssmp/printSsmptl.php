<?php
if(!empty ($_SESSION['refillSsmp'])){
    $sql = "SELECT ssmp_te.cod_ssmp, data_abertura, solicitacao_acesso,
                dias_servico, data_programada, usuario.usuario, nome_status,
                un_equipe.cod_unidade, nome_local, l.nome_linha, l.cod_linha, unidade.nome_unidade,
                nome_servico_pmp, nome_sistema, nome_subsistema, nome_procedimento, ssmp.complemento, nome_periodicidade, cod_tipo_periodicidade
            from ssmp_te
            JOIN ssmp USING(cod_ssmp)
            
            JOIN usuario USING(cod_usuario)
            JOIN status_ssmp USING(cod_status_ssmp)
            JOIN status USING (cod_status)
            LEFT JOIN un_equipe USING(cod_un_equipe)
            LEFT JOIN unidade USING(cod_unidade)
            
            JOIN local USING(cod_local)
            JOIN linha l USING(cod_linha)
            
            JOIN pmp_telecom USING(cod_pmp_telecom)
            JOIN pmp USING(cod_pmp)
            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
            JOIN servico_pmp USING(cod_servico_pmp)
            JOIN sub_sistema USING(cod_sub_sistema)
            JOIN sistema USING(cod_sistema)
            JOIN subsistema USING(cod_subsistema)
            JOIN procedimento USING(cod_procedimento)
            JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
            WHERE ssmp_te.cod_ssmp = {$_SESSION['refillSsmp']['codigoSsmp']}";

    $Ssmp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $Ssmp = $Ssmp[0];
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>
        <h3>SSMP - Solicita��o de Servi�o de Manuten��o Preventiva</h3>
    </div>
    <hr/>

    <div class="title-table">&raquo; Dados Gerais</div>
    <table>
        <tr>
            <th>N�</th>
            <th>Responsavel Preenchimento</th>
            <th>Data/Hora Abertura</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['cod_ssmp']; ?></td>
            <td class="center"><?php echo $Ssmp['usuario']; ?></td>
            <td class="center"><?php echo $this->parse_timestamp($Ssmp['data_abertura']); ?></td>
        </tr>
        <tr>
            <th>Data/Hora Programada</th>
            <th>Qtd. de dias</th>
            <th>S.A.</th>
        </tr>
        <tr>
            <td class="center"><?php echo $this->parse_timestamp($Ssmp['data_programada']); ?></td>
            <td class="center"><?php echo $Ssmp['dias_servico']; ?></td>
            <td class="center"><?php echo $Ssmp['solicitacao_acesso']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Origem</div>
    <table>
        <tr>
            <th>Origem</th>
            <th>Status</th>
        </tr>
        <tr>
            <td class="center">Preventiva</td>
            <td class="center"><?php echo $Ssmp['nome_status']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Tipo Interven��o</th>
        </tr>
        <tr>
            <td colspan="2" class="center">Manut. Preventiva</td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o / Local</div>
    <table>
        <tr>
            <th>Linha</th>
            <th>Local</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['nome_linha']; ?></td>
            <td class="center"><?php echo $Ssmp['nome_local']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Complemento</th>
        </tr>
        <tr>
            <td colspan="2"><?php echo $Ssmp['complemento']; ?></td>
        </tr>
        <tr>
            <th>Grupo</th>
            <th>Sistema</th>
        </tr>
        <tr>
            <td class="center">Telecom</td>
            <td class="center"><?php echo $Ssmp['nome_sistema']; ?></td>
        </tr>
        <tr>
            <th colspan="2">SubSistema</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $Ssmp['nome_subsistema']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Servi�o</div>
    <table>
        <tr>
            <th colspan="2">Nome</th>
        </tr>
        <tr>
            <td colspan="2"><?php echo $Ssmp['nome_servico_pmp']; ?></td>
        </tr>
        <tr>
            <th>Procedimento</th>
            <th>Periodicidade</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['nome_procedimento']; ?></td>
            <td class="center"><?php echo $Ssmp['nome_periodicidade']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Encaminhamento</div>
    <table>
        <tr>
            <th>Unidade</th>
            <th>Equipe</th>
        </tr>
        <tr>
            <td class="center"><?php echo $Ssmp['nome_unidade']; ?></td>
            <td class="center">Telecom</td>
        </tr>
    </table>
</div>