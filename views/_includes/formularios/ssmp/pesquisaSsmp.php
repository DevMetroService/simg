<div class="page-header">
    <h2>Pesquisa SSMP</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/executarPesquisaSsmp" class="form-group" method="post" id="pesquisa">
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Ssmp</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Dados Gerais</label></div>
                            
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>N� SSMP</label>
                                        <input class="form-control number" name="numeroSsmp" value="<?php echo($dadosRefill['numeroSsmp']); ?>"/>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Grupo de Sistema <span style="color: red">(*)</span></label>
                                        <?php
                                        if (empty($dadosRefill['grupoSistemaPesquisa'])){
                                            $dadosRefill['grupoSistemaPesquisa'] = 21;
                                        }

                                        $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_tipo_grupo" => 5, "cod_grupo[!]" => [29,30,31]], "ORDER" => "nome_grupo"]);
                                        $this->form->getSelectGrupo($dadosRefill['grupoSistemaPesquisa'], $grupo, 'grupoSistemaPesquisa', true);
                                        ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Sistema</label>
                                        <select class="form-control" name="sistemaPesquisa">
                                            <option value="">Sistemas</option>
                                            <?php
                                            $sistema = $this->medoo->select("grupo_sistema", ["[><]sistema" => "cod_sistema"], ['cod_sistema', 'nome_sistema'], ["cod_grupo" => $dadosRefill['grupoSistemaPesquisa']]);

                                            foreach ($sistema as $dados => $value) {
                                                if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisa'] == $value['cod_sistema'])
                                                    echo("<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>");
                                                else
                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Sub-Sistema</label>
                                        <select class="form-control" name="subSistemaPesquisa">
                                            <option value="">Sub-Sistemas</option>
                                            <?php
                                            if ($dadosRefill['sistemaPesquisa']) {
                                                $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"], ['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisa']]);

                                                foreach ($subsistema as $dados => $value) {
                                                    if (!empty($dadosRefill) && $dadosRefill['subSistemaPesquisa'] == $value['cod_subsistema'])
                                                        echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                    else
                                                        echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Servi�o</label>
                                        <select class="form-control" name="servico">
                                            <option value="">Todos</option>
                                            <?php
                                            if(!empty($dadosRefill)){
                                                $selectServico = $this->medoo->select('servico_pmp', ['nome_servico_pmp', 'cod_servico_pmp'], ['ORDER' => 'nome_servico_pmp', 'cod_grupo' => $dadosRefill['grupoSistemaPesquisa']]);
                                            }else{
                                                $selectServico = $this->medoo->select('servico_pmp', ['nome_servico_pmp', 'cod_servico_pmp'], ['ORDER' => 'nome_servico_pmp']);
                                            }

                                            foreach ($selectServico as $dados => $value) {
                                                if ($dadosRefill['servico'] == $value['cod_servico_pmp'])
                                                    echo('<option value="' . $value['cod_servico_pmp'] . '" selected>' . $value['nome_servico_pmp'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_servico_pmp'] . '">' . $value['nome_servico_pmp'] . '</option>');
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Periodicidade</label>
                                        <select class="form-control" name="periodicidade">
                                            <option value="">Todos</option>
                                            <?php
                                            $selectPeriodicidade = $this->medoo->select('tipo_periodicidade', ['nome_periodicidade', 'cod_tipo_periodicidade']);
                                            foreach ($selectPeriodicidade as $dados => $value) {
                                                if (!empty($dadosRefill) && $dadosRefill['periodicidade'] == $value['cod_tipo_periodicidade'])
                                                    echo('<option value="' . $value['cod_tipo_periodicidade'] . '" selected>' . $value['nome_periodicidade'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_tipo_periodicidade'] . '">' . $value['nome_periodicidade'] . '</option>');
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Status / Per�odo</label></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Status</label>
                                        <select class="form-control" name="statusSsmpPesquisa">
                                            <option value="">Selecione o Status</option>
                                            <?php
                                            $selectStatus = $this->medoo->select("status", ['cod_status', 'nome_status'], ["tipo_status" => "P"]);

                                            foreach ($selectStatus as $dados => $value) {
                                                echo("<option value='{$value['cod_status']}' ");
                                                if ($value['cod_status'] == $dadosRefill['statusSsmpPesquisa']) {
                                                    echo(' selected');
                                                }
                                                echo(' >' . $value['nome_status'] . '</option>');
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Quinzena</label>
                                        <input class="form-control"
                                            <?php
                                                if (!empty($dadosRefill['quinzenaPesquisaSsmp'])) {
                                                    echo('value="' . $dadosRefill['quinzenaPesquisaSsmp'] . '"');
                                                }
                                            ?>
                                               type="text" name="quinzenaPesquisaSsmp">
                                    </div>

                                    <div class="col-md-3">
                                        <label>Ano</label>
                                        <input class="form-control"
                                            <?php
                                                if (!empty($dadosRefill['anoPesquisaSsmp'])) {
                                                    echo('value="' . $dadosRefill['anoPesquisaSsmp'] . '"');
                                                }
                                            ?>
                                               type="text" name="anoPesquisaSsmp">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Data Programada de</label>
                                        <input class="form-control data" name="dataPartir"
                                            <?php
                                            if (!empty($dadosRefill['dataPartir'])) {
                                                echo('value="' . $dadosRefill['dataPartir'] . '"');
                                            }
                                            ?>
                                               type="text">
                                    </div>
                                    <div class="col-md-3">
                                        <label>At�</label>
                                        <input class="form-control data" name="dataRetroceder"
                                            <?php
                                            if (!empty($dadosRefill['dataRetroceder'])) {
                                                echo('value="' . $dadosRefill['dataRetroceder'] . '"');
                                            }
                                            ?>
                                               type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Local</label></div>

                            <div class="panel-body">
                                <div id="linha" class="col-md-4">
                                    <label>Linha</label>
                                    <select class="form-control" name="linha">
                                        <option value="">Todos</option>
                                        <?php
                                        $selectLinha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);
                                        foreach ($selectLinha as $dados => $value) {
                                            if ($dadosRefill['linha'] == $value['cod_linha'])
                                                echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div id="local" class="col-md-4">
                                    <label>Local</label>
                                    <select class="form-control" name="localPesquisa">
                                        <option value="">Todos</option>
                                        <?php
                                        $selectLinha = $this->medoo->select('local', ['nome_local', 'cod_local']);
                                        foreach ($selectLinha as $dados => $value) {
                                            if (!empty($dadosRefill) && $dadosRefill['localPesquisa'] == $value['cod_local'])
                                                echo('<option value="' . $value['cod_local'] . '" selected>' . $value['nome_local'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_local'] . '">' . $value['nome_local'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div id="estacaoInicial" class="col-md-4">
                                    <label>Esta��o Inicial</label>
                                    <select class="form-control" name="estacaoInicialPesquisa">
                                        <option value="">Todos</option>
                                        <?php
                                        $selectEstacao = $this->medoo->select('estacao', ['nome_estacao', 'cod_estacao']);
                                        foreach ($selectEstacao as $dados => $value) {
                                            if ($dadosRefill['estacaoInicialPesquisa'] == $value['cod_estacao'])
                                                echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div id="estacaoFinal" class="col-md-4">
                                    <label>Esta��o Final</label>
                                    <select class="form-control" name="estacaoFinalPesquisa">
                                        <option value="">Todos</option>
                                        <?php
                                        foreach ($selectEstacao as $dados => $value) {
                                            if ($dadosRefill['estacaoFinalPesquisa'] == $value['cod_estacao'])
                                                echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro
            </button>
        </div>
    </div>

    <div class="row" style="padding-top: 1em"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Resultado da Pesquisa
                    <strong>Total
                        Encontrado: <?php echo (!empty($_SESSION['resultadoPesquisaSsmp'])) ? count($_SESSION['resultadoPesquisaSsmp']) : 0; ?></strong>
                </div>

                <div class="panel-body">
                    <table id="resultadoPesquisaSsmp" class="table table-striped table-bordered no-footer">
                        <thead id="headIndicadorSsmp">
                        <tr role="row">
                            <th>N� SSMP</th>
                            <th>N� Cronograma</th>
                            <th>Local</th>
                            <th>Data Programada</th>
                            <th>Quinzena Prevista</th>
                            <th>Servi�o</th>
                            <th>Status</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (!empty($_SESSION['resultadoPesquisaSsmp'])) {
                            foreach ($_SESSION['resultadoPesquisaSsmp'] as $dados => $value) {
                                echo('<tr>');
                                echo('<td>' . $value['cod_ssmp'] . '</td>');
                                echo('<td>' . $value['cod_cronograma_pmp'] . '</td>');
                                echo("<td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['nome_local']}</span></td>");
                                echo("<td>{$this->parse_timestamp($value['data_programada'])}</td>");
                                echo('<td>' . $value['quinzena'] . '</td>');
                                echo('<td>' . $value['nome_servico_pmp'] . '</td>');
                                echo('<td>' . $value['nome_status'] . '</td>');
                                echo('<td>
                                        <button value="' . $value['form'] . '" class="btn btn-primary btn-circle btnEditarSsmp" type="button" title="Ver Dados/Editar">
                                            <i class="fa fa-file-o fa-fw"></i>
                                        </button>
                                        <button value="' . $value['form'] . '" class="btn btn-default btn-circle btnImprimirSsmp" type="button" title="Imprimir">
                                            <i class="fa fa-print fa-fw"></i>
                                        </button>
                                    </td>');
                                echo('</tr>');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>