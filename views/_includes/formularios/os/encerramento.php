<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 15/03/2016
 * Time: 11:36
 */
?>

<div class="page-header">
    <h1><?php echo $tituloOs;?></h1>
</div>

<div class="row">
    <form action="<?php echo HOME_URI . "/" . $actionForm . "/salvarEncerramento"; ?>" class="form-group" method="post" id="formEncerramentoOs">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Encerramento</label></div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <?php
                            if($tipoOS == "osp")
                                echo "<input type='hidden' value='{$qtdDias}' id='diasRestantes'>";

                                echo "<input id='codGrupoSistema' type='hidden' value='{$os['cod_grupo']}' id='diasRestantes'>"
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="equipe">Equipe / Grupo / Sistema Afetado (Falha) </label>
                                        <input type="text" disabled id="equipe" class="form-control"
                                               value="<?php
                                               if ($tipoOS != 'osm' && $tipoOS != 'osp') {
                                                   echo $unidadeEquipe['sigla']." / ".$nomeGrupo." / ".$ss['nome_sistema'];
                                               }else if(!empty($os))
                                                   echo $unidadeEquipe['sigla']." / ".$os['cod_grupo'].' -- '.$os['nome_grupo']." / ".$ss['sigla_sistema']." -- ".$ss['nome_sistema']
                                               ?>"/>

                                    </div>

                                    <?php if($tipoOS == "osm"){ ?>
                                    <div class="col-md-1 col-xs-2">
                                        <label for="nivelSs">N�vel</label>
                                        <input type="text" id="nivelSs" disabled class="form-control"
                                               value="<?php if(!empty($os)) echo $ss['nivel']?>" />

                                    </div>
                                    <?php } ?>

                                    <div class="col-md-3 col-xs-4">
                                        <label for="dataHora">Data / Hora Abertura</label>
                                        <input type="text" disabled id="dataHora" class="form-control"
                                               value="<?php echo $this->parse_timestamp($os['data_abertura']); ?>" />

                                        <!-- Data Desmobiliza��o -->
                                        <input type="hidden" id="desmobilizacao" value="<?php echo $this->parse_timestamp($tempoTotal[0]['data_desmobilizacao'])?>"/>
                                    </div>

                                    <div class="col-md-2 col-xs-2">
                                        <label>C�digo OS</label>
                                        <input name="codigoOs" class="form-control" readonly value="<?php echo $_SESSION['refillOs']['codigoOs']?>"/>
                                        <?php
                                        if ($tipoOS != 'osm' && $tipoOS != 'osp') {
                                            echo('<input name="form" type="hidden" required class="free" value="'.$_SESSION['refillOs']['form'].'" />');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Fechamento</label></div>

                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-md-4 col-xs-4">
                                        <label>Descri��o tipo do fechamento</label>
                                        <select name="descricaoTipoFechamento" <?php if($tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> required class="form-control">
                                            <option value="" disabled selected>Selecione o Tipo de Fechamento</option>
                                            <?php
                                            $selectTipoFechamento = $this->medoo->select("tipo_fechamento",['cod_tipo_fechamento','nome_fechamento'], ['ativo' => 's']);

                                            $hasManobra = $this->hasManobraEntrada($os['cod_ssm']);

                                            foreach ($selectTipoFechamento as $key => $value){
                                                if (($hasManobra && $value['cod_tipo_fechamento'] == 1))
                                                    continue;

                                                if(!empty($encerramento) && $encerramento['cod_tipo_fechamento'] == $value['cod_tipo_fechamento']){
                                                    echo('<option value="'.$value['cod_tipo_fechamento'].'" selected>'.$value['nome_fechamento'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_tipo_fechamento'].'">'.$value['nome_fechamento'].'</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if($tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$encerramento['cod_tipo_fechamento']}' name='descricaoTipoFechamento' />";
                                        ?>
                                    </div>

                                    <div class="col-md-4 col-xs-4">
                                        <label>Unidade</label>
                                        <input type="text" disabled class="form-control"
                                               value="<?php echo($unidadeEquipe['nome_unidade']);?>" />

                                    </div>

                                    <div class="col-md-3 col-xs-4">
                                        <label>Data / Hora de Recebimento de Informa��es</label>
                                        <input name="dataHoraFechamento" required type="text" class="form-control timeStamp"
                                               value="<?php if(!empty($encerramento)){ echo($this->parse_timestamp($encerramento['data_encerramento']) );} ?>" />

                                    </div>

                                </div>

                                <div class="row" id="comPendencia" style="display:none">
                                    <div class="col-md-1 col-xs-2" style="display: none;">
                                        <label>N�</label>
                                        <input name="numeroTipoPendenciaFechamento" disabled type="text" class="form-control"
                                               value="<?php if(!empty($encerramento['cod_tipo_pendencia'])) echo $encerramento['cod_tipo_pendencia']?>"/>
                                    </div>
                                    <div class="col-md-6 col-xs-10">
                                        <label>Tipo Pend�ncia</label>
                                        <select name="tipoPendenciaFechamento" class="form-control">
                                            <option selected disabled value=''>Selecione o tipo de pend�ncia</option>
                                            <?php
                                            $selectTipoPendencia = $this->medoo->select("tipo_pendencia", ['cod_tipo_pendencia','nome_tipo_pendencia']);

                                            foreach($selectTipoPendencia as $key => $value){
                                                if(!empty($encerramento) && $encerramento['cod_tipo_pendencia'] == $value['cod_tipo_pendencia'] ){
                                                    echo('<option value="'.$value['cod_tipo_pendencia'].'" selected>'.$value['nome_tipo_pendencia'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_tipo_pendencia'].'">'.$value['nome_tipo_pendencia'].'</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1 col-xs-2" style="display: none;">
                                        <label>N�</label>
                                        <input name="numeroPendenciaFechamento" disabled type="text" class="form-control"
                                               value="<?php if(!empty($encerramento['cod_pendencia'])) echo $encerramento['cod_pendencia'] ?>"/>
                                    </div>
                                    <div class="col-md-6 col-xs-10">
                                        <label>Pend�ncia</label>
                                        <select name="pendenciaFechamento" class="form-control">
                                            <?php
                                            if(!empty($encerramento)){
                                                $selectPendencia = $this->medoo->select("pendencia", ['cod_pendencia','nome_pendencia'], ["cod_tipo_pendencia" => $encerramento['cod_tipo_pendencia']]);

                                                echo("<option disabled selected>Selecione a pend�ncia</option>");

                                                foreach($selectPendencia as $key => $value){
                                                    if($encerramento['cod_pendencia'] == $value['cod_pendencia'] ){
                                                        echo('<option value="'.$value['cod_pendencia'].'" selected>'.$value['nome_pendencia'].'</option>');
                                                    }else{
                                                        echo('<option value="'.$value['cod_pendencia'].'">'.$value['nome_pendencia'].'</option>');
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row" id="motivoNaoExecucao" style="display: none">
                                    <div class="col-md-12">
                                        <label>Motivo da N�o Execu��o</label>
                                        <input type="text" name="motivoNaoExecucao" class="form-control"
                                               value="<?php if(!empty($encerramento)) echo $encerramento['descricao'] ?>" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-xs-2">
                                        <label>  <?php if($tipoOS != "osm" && $tipoOS != "osp"){ echo("Atividade Conclu�da");}else{echo("Libera��o");}?></label>
                                        <select name="liberacaoTrafego" <?php if($tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                            <option value="s" <?php if(empty($encerramento) || $encerramento['liberacao']=="s") echo 'selected' ?>>Sim</option>
                                            <option value="n" <?php if($encerramento['liberacao']=="n") echo 'selected' ?>>N�o</option>
                                        </select>
                                        <?php
                                        if($tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$encerramento['liberacao']}' name='liberacaoTrafego' />";
                                        ?>
                                    </div>

                                    <?php if ($os['grupo_atuado'] != '22' && $os['grupo_atuado'] != '23' && $os['grupo_atuado'] != '26') { ?>
                                        <?php if($tipoOS == "osm"){ ?>
                                        <div class="col-md-2 col-xs-2">
                                            <label>Transfer�ncia</label>
                                            <input type="hidden" id="selectTransferencia" name="selectTranferencia" value="<?php echo $encerramento['transferida'] == "s" ? 's' : 'n' ?>" />
                                            <select name="transferenciaAtividadeOs" <?php if($tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                                <option value="s" <?php if(!empty($encerramento) && $encerramento['transferida']=="s") echo 'selected' ?>>Sim</option>
                                                <option value="n" <?php if(empty($encerramento) || $encerramento['transferida']=="n") echo 'selected' ?>>N�o</option>
                                            </select>
                                        </div>
                                            <?php
                                            if($tipoOS == "osm" && $encerramento['transferida'] == "s")
                                                echo "<input type='hidden' value='{$encerramento['transferida']}' name='transferenciaAtividadeOs' />";
                                            ?>
                                        <?php } ?>
                                    <?php }
                                    ?>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-2 ">
                                        <label>Tipo Funcionario</label>
                                        <select name="codTipoFuncionario" class="form-control" required>
                                          <option disabled selected>Selecione o Tipo de Funcionario </option>
                                            <?php
                                            $tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);

                                            foreach($tipo_funcionario as $key =>$values){
                                                if($encerramento['cod_tipo_funcionario'] == $values["cod_tipo_funcionario"])
                                                    echo"<option value='{$values["cod_tipo_funcionario"]}' selected>{$values["descricao"]}</option>";
                                                else
                                                    echo"<option value='{$values["cod_tipo_funcionario"]}'>{$values["descricao"]}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        <label>Matr�cula</label>
                                        <input name="matriculaResponsavelInformacoesOs" required="required" type="text" readonly class="form-control number"
                                               value="<?php if(!empty($encerramento)){echo($encerramento['matricula']); } ?>">
                                        <input type="hidden" name="codFuncionario">
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <label>Respons�vel pelas informa��es</label>
                                        <input type="text" name="responsavelInformacaoOs" class="form-control" required list="nomeSolicitanteDataList"
                                               value="<?php if (!empty($encerramento['nome_funcionario'])) echo $encerramento['nome_funcionario']; ?>" autocomplete="off"/>

                                        <datalist id="nomeSolicitanteDataList">
                                        </datalist>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if($tipoOS == "osm"){ ?>
                <div class="row" id="dadosTransferencia" style="display: <?php echo $encerramento['transferida'] == 's' ? 'block' : 'none' ?>">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Continuidade</label></div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-1 col-xs-2">
                                        <label>N�</label>
                                        <input name="numeroUnidadeContinuidade" type="text" disabled class="form-control"
                                               value="<?php if(!empty($encerramento['cod_un_equipe'])) echo $unEquipe['cod_unidade']?>" />
                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Local</label>
                                        <select name="unidadeContinuidade" <?php if($encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                            <option disabled selected value="">Selecione o Local</option>
                                            <?php
                                            $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"], ["ORDER" => "cod_unidade"]);

                                            foreach($unidade as $key =>$value){
                                                if(!empty($encerramento['cod_un_equipe']) && $unEquipe['cod_unidade'] == $value['cod_unidade'] ){
                                                    echo('<option value="'.$value['cod_unidade'].'" selected>'.$value['nome_unidade'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if($tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$unEquipe['cod_unidade']}' name='unidadeContinuidade' />";
                                        ?>
                                    </div>
                                    <div class="col-md-1 col-xs-2">
                                        <label>N�</label>
                                        <input name="numeroEquipeContinuidade" disabled type="text" class="form-control" value="<?php if(!empty($unEquipe['sigla'])) echo $unEquipe['sigla'] ?>"/>
                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Equipe</label>
                                        <select name="equipeContinuidade" <?php if($tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                            <?php
                                            if(!empty($encerramento['cod_un_equipe'])){
                                                $equipe = $this->medoo->select("un_equipe",["[><]equipe"=> "cod_equipe"], ["sigla", "nome_equipe"],
                                                    [
                                                        "ORDER" => "nome_equipe",
                                                        "cod_unidade" => $unEquipe['cod_unidade']
                                                    ]
                                                );

                                                echo('<option disabled selected value="">Selecione a Equipe</option>');

                                                foreach ($equipe as $key => $value) {
                                                    if ($unEquipe['sigla'] == $value['sigla'])
                                                        echo('<option value="' . $value['sigla'] . '" selected>' . $value['nome_equipe'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if($tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$unEquipe['sigla_equipe']}' name='equipeContinuidade' />";
                                        ?>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Recomenda��es</label>
                                        <textarea name="recomendacoesContinuidade" <?php if($tipoOS == "osm" && $encerramento['transferida'] == "s") echo "readonly"; ?> class="form-control"><?php if(!empty($encerramento)) echo($encerramento['descricao']);?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="row hidden-print">
                    <?php
                        require_once(ABSPATH . "/views/_includes/formularios/os/btnNavegacao.php");
                    ?>
                </div>
            </div>
        </div>
    </form>
</div>
