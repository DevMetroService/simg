<?php if( !($_SESSION['bloqueio'] && $pag != 'dadosGerais') ){ ?>
    <div class="col-lg-<?php echo ($pag == 'dadosGerais' && $selectDescStatus['nome_status'] != "Encerrada" && ($tipoOS == 'osm' || $tipoOS == 'osp')) ? "3" : "3" ?> col-md-5">
        <div class="panel panel-primary">
            <div class="panel-heading">A��es</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified" role="group">
                            <?php
                            if ($acao && !$_SESSION['bloqueio']) {
                                ?>
                                <div class="btn-group">
                                    <?php
                                    if ($pag == 'encerramento' && empty($encerramento)) { ?>
                                        <button type="button" name="btnEncerrarOs" class="btn btn-default"
                                                aria-label="Left Align" title="Finalizar">
                                            <i class="fa fa-check-square-o fa-2x"></i>
                                        </button>
                                        <?php
                                    } else { ?>
                                        <button id='btnSalvarDadosGerais' type="button" name="btnSalvarOs" class="btn btn-default"
                                                aria-label="Left Align" title="Salvar">
                                            <i class="fa fa-floppy-o fa-2x"></i>
                                        </button>
                                        <?php
                                    } ?>
                                </div>
                                <?php
                                if ($pag == 'dadosGerais' && $selectDescStatus['nome_status'] != "Encerrada") {?>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning" aria-label="Left Align"
                                                title="Cancelar OS" data-toggle="modal" data-action="cancelar"
                                                data-target="#motivoModal">
                                            <i class="fa fa-copy fa-2x"></i>
                                        </button>
                                    </div>
                                    <?php
                                }
                            }
                            if ($pag == 'dadosGerais') { ?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btnImprimirFormulario"
                                            data-form="<?php echo $tipoOS?>" title="Imprimir" aria-label="right align">
                                        <i class="fa fa-print fa-2x"></i>
                                    </button>
                                </div>

                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary  btnHistorico"
                                            data-toggle="modal" data-target=".modal-historico"
                                            title="Ver Hist�rico do Formul�rio">
                                        <i class="fa fas fa-list-ul fa-2x"></i>
                                    </button>

                                    <div class="modal fade modal-historico" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                                                        <i aria-hidden="true" class="fa fa-close"></i></button>
                                                    <h4 class="modal-title">Hist�rico do Fomul�rio</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php


                                                    if(!empty($os)) {
                                                        if ($tipoOS == "osm") {
                                                            $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_osm s 
                                                    JOIN usuario u on s.usuario = u.cod_usuario
                                                    JOIN status USING(cod_status)
                                                    WHERE cod_osm = {$os['cod_osm']} ORDER BY data_status";

                                                            if ($sql != "")
                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                        }else {
                                                            $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_osp s 
                                                    JOIN usuario u on s.usuario = u.cod_usuario
                                                    JOIN status USING(cod_status)
                                                    WHERE cod_osp = {$os['cod_osp']} ORDER BY data_status";

                                                            if ($sql != "")
                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                        }

                                                    }
                                                    if ($boolean){
                                                        $sql = "SELECT nome_status, data_status, u.usuario, descricao FROM status_osmp s 
                                                JOIN usuario u on s.usuario = u.cod_usuario
                                                JOIN status USING(cod_status)
                                                WHERE cod_osmp = {$dados['cod_osmp']} ORDER BY data_status";

                                                        if ($sql != "")
                                                            $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                                    }
                                                    ?>

                                                    <div class = "row">
                                                        <div class="col-md-12">
                                                            <table class="tableRel table-bordered dataTable">
                                                                <thead>
                                                                <tr>
                                                                    <th>Status</th>
                                                                    <th>Responsavel</th>
                                                                    <th>Data da altera��o</th>
                                                                    <th>Complemento</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                if(!empty($result)){
                                                                    foreach ($result as $dadosR){
                                                                        echo("<tr>");
                                                                        echo("<td>{$dadosR['nome_status']}</td>");
                                                                        echo("<td>{$dadosR['usuario']}</td>");
                                                                        echo("<td>{$this->parse_timestamp($dadosR['data_status'])}</td>");
                                                                        echo("<td>{$dadosR['descricao']}</td>");
                                                                        echo("</tr>");
                                                                    }
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="col-lg-8 pull-right" id="menuComplementoFixed" style="position: fixed; right: 5%; top: 80%; width: 50%">
    <div class="panel panel-primary panelModulosOs">
        <div class="panel-heading">Complementos</div>
        <div class="panel-body">
            <div class="btn-group btn-group-justified" role="group">
                <div class="btn-group">
                    <a <?php if ($pag != 'dadosGerais') echo 'href="' .HOME_URI . '/dashboardGeral/' . $tipoOS . '"'; ?>>
                        <?php
                        if ($tipoOS == 'osmp/vp' || $tipoOS == 'osmp/Vp') {
                            echo "<button type='button' class='btn ";
                            echo (!empty($dados['km_inicial']) && !empty($dados['km_final'])) ? "btn-success' " : "btn-danger' ";
                            echo "aria-label='Left Align' title='Dados Gerais' ";
                            echo ($pag == 'dadosGerais') ? "disabled>" : ">";
                            echo "<i class='fa fa-home fa-2x'></i></button>";
                        } else {
                            echo "<button type='button' class='btn btn-default' aria-label='Left Align' title='Dados Gerais' ";
                            echo ($pag == 'dadosGerais') ? "disabled>" : ">";
                            echo "<i class='fa fa-home fa-2x'></i></button>";
                        }
                        ?>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'manobrasEletricas') echo 'href="' . HOME_URI . '/'.$actionForm . '/manobrasEletricas"'; ?> >
                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas"
                            <?php if ($pag == 'manobrasEletricas') echo "disabled" ?>>
                            <i class="fa fa-bolt fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'tempoTotal') echo 'href="' . HOME_URI . '/'.$actionForm . '/tempoTotal"'; ?> >
                        <button type="button"
                                class="btn <?php echo (!empty($tempoTotal)) ? "btn-success" : "btn-danger"; ?>"
                                aria-label="Left Align" title="Tempos Totais"
                            <?php if ($pag == 'tempoTotal') echo "disabled" ?>>
                            <i class="fa fa-clock-o fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'maoObra') echo 'href="' . HOME_URI . '/'.$actionForm . '/maoObra"'; ?> >
                        <button type="button"
                                class="btn <?php echo (!empty($maoObra)) ? "btn-success" : "btn-danger"; ?>"
                                aria-label="Left Align" title="M�o de Obra"
                            <?php if ($pag == 'maoObra') echo "disabled" ?>>
                            <i class="fa fa-users fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'registroExecucao') echo 'href="' . HOME_URI . '/'.$actionForm . '/registroExecucao"'; ?> >
                        <button type="button"
                                class="btn <?php echo (!empty($regExecucao)) ? "btn-success" : "btn-danger"; ?>"
                                aria-label="Left Align" title="Registro de Execu��o"
                            <?php if ($pag == 'registroExecucao') echo "disabled" ?>>
                            <i class="fa fa-crosshairs fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'maquinaUtilizada') echo 'href="' . HOME_URI . '/'.$actionForm . '/maquinaUtilizada"'; ?> >
                        <button type="button" class="btn <?php
                        if ($_SESSION['dadosCheck']['statusOs'] == 10) {
                            echo (!empty($_SESSION['dadosCheck']['maquina']) || ($_SESSION['dadosCheck']['maquinaCheck'] && ($_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) ? "btn-success" : "btn-danger";
                        } else {
                            echo "btn-default";
                        } ?>" aria-label="Left Align"
                                title="M�quinas e Equipamentos"
                            <?php if ($pag == 'maquinaUtilizada') echo "disabled" ?>>
                            <!--                            <i class="fa fa-truck fa-2x"></i>-->
                            <i class="fa icon-driller fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'materialUtilizado') echo 'href="' . HOME_URI . '/'.$actionForm . '/materialUtilizado"'; ?> >
                        <button type="button" class="btn <?php
                        if ($_SESSION['dadosCheck']['statusOs'] == 10) {
                            echo (!empty($_SESSION['dadosCheck']['material']) || ($_SESSION['dadosCheck']['materialCheck'] && ($_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) ? "btn-success" : "btn-danger";
                        } else {
                            echo "btn-default";
                        } ?>" aria-label="Left Align" title="Materiais Utilizados"
                            <?php if ($pag == 'materialUtilizado') echo "disabled" ?>>
                            <!--                            <i class="fa fa-wrench fa-2x"></i>-->
                            <i class="fa icon-barrow-with-construction-materials fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <?php
                    if ($_SESSION['dadosCheck']['statusOs'] == 10) {
                        if ($tipoOS == 'osmp/vp' || $tipoOS == 'osmp/Vp') {
                            if ($dados['km_inicial'] && $dados['km_final']) {
                                if (!empty($maoObra) && !empty($tempoTotal) && !empty($regExecucao) && (!empty($_SESSION['dadosCheck']['material']) || ($_SESSION['dadosCheck']['materialCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs'])) && (!empty($_SESSION['dadosCheck']['maquina']) || ($_SESSION['dadosCheck']['maquinaCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) {
                                    echo ($pag != 'encerramento') ? '<a href="' . HOME_URI . '/'.$actionForm . '/encerramento">' : '<a>';
                                    echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento" ');
                                    echo ($pag == 'encerramento') ? "disabled >" : ">";
                                    echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                    echo('</a>');
                                } else {
                                    echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                                    echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                                    echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                    echo('</div>');
                                }
                            } else {
                                echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                                echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                                echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                echo('</div>');
                            }
                        } else {
                            if (!empty($maoObra) && !empty($tempoTotal) && !empty($regExecucao) && (!empty($_SESSION['dadosCheck']['material']) || ($_SESSION['dadosCheck']['materialCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs'])) && (!empty($_SESSION['dadosCheck']['maquina']) || ($_SESSION['dadosCheck']['maquinaCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) {
                                echo ($pag != 'encerramento') ? '<a href="' . HOME_URI . '/'.$actionForm . '/encerramento">' : '<a>';
                                echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento" ');
                                echo ($pag == 'encerramento') ? "disabled >" : ">";
                                echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                echo('</a>');
                            } else {
                                echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                                echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                                echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                echo('</div>');
                            }
                        }
                    } else {
                        if (!empty($maoObra) && !empty($tempoTotal) && !empty($regExecucao)) {
                            echo ($pag != 'encerramento') ? '<a href="' . HOME_URI . '/'.$actionForm . '/encerramento">' : '<a>';
                            echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento" ');
                            echo ($pag == 'encerramento') ? "disabled >" : ">";
                            echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                            echo('</a>');
                        } else {
                            echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                            echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                            echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                            echo('</div>');
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-8 pull-right" id="menuComplementoStatic" style="display: block">
    <div class="panel panel-primary panelModulosOs">
        <div class="panel-heading">Complementos</div>
        <div class="panel-body">
            <div class="btn-group btn-group-justified" role="group">
                <div class="btn-group">
                    <a <?php if ($pag != 'dadosGerais') echo 'href="' .HOME_URI . '/dashboardGeral/' . $tipoOS . '"'; ?>>
                        <?php
                        if ($tipoOS == 'osmp/vp' || $tipoOS == 'osmp/Vp') {
                            echo "<button type='button' class='btn ";
                            echo (!empty($dados['km_inicial']) && !empty($dados['km_final'])) ? "btn-success' " : "btn-danger' ";
                            echo "aria-label='Left Align' title='Dados Gerais' ";
                            echo ($pag == 'dadosGerais') ? "disabled>" : ">";
                            echo "<i class='fa fa-home fa-2x'></i></button>";
                        } else {
                            echo "<button type='button' class='btn btn-default' aria-label='Left Align' title='Dados Gerais' ";
                            echo ($pag == 'dadosGerais') ? "disabled>" : ">";
                            echo "<i class='fa fa-home fa-2x'></i></button>";
                        }
                        ?>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'manobrasEletricas') echo 'href="' . HOME_URI . '/'.$actionForm . '/manobrasEletricas"'; ?> >
                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas"
                            <?php if ($pag == 'manobrasEletricas') echo "disabled" ?>>
                            <i class="fa fa-bolt fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'tempoTotal') echo 'href="' . HOME_URI . '/'.$actionForm . '/tempoTotal"'; ?> >
                        <button type="button"
                                class="btn <?php echo (!empty($tempoTotal)) ? "btn-success" : "btn-danger"; ?>"
                                aria-label="Left Align" title="Tempos Totais"
                            <?php if ($pag == 'tempoTotal') echo "disabled" ?>>
                            <i class="fa fa-clock-o fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'maoObra') echo 'href="' . HOME_URI . '/'.$actionForm . '/maoObra"'; ?> >
                        <button type="button"
                                class="btn <?php echo (!empty($maoObra)) ? "btn-success" : "btn-danger"; ?>"
                                aria-label="Left Align" title="M�o de Obra"
                            <?php if ($pag == 'maoObra') echo "disabled" ?>>
                            <i class="fa fa-users fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'registroExecucao') echo 'href="' . HOME_URI . '/'.$actionForm . '/registroExecucao"'; ?> >
                        <button type="button"
                                class="btn <?php echo (!empty($regExecucao)) ? "btn-success" : "btn-danger"; ?>"
                                aria-label="Left Align" title="Registro de Execu��o"
                            <?php if ($pag == 'registroExecucao') echo "disabled" ?>>
                            <i class="fa fa-crosshairs fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'maquinaUtilizada') echo 'href="' . HOME_URI . '/'.$actionForm . '/maquinaUtilizada"'; ?> >
                        <button type="button" class="btn <?php
                        if ($_SESSION['dadosCheck']['statusOs'] == 10) {
                            echo (!empty($_SESSION['dadosCheck']['maquina']) || ($_SESSION['dadosCheck']['maquinaCheck'] && ($_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) ? "btn-success" : "btn-danger";
                        } else {
                            echo "btn-default";
                        } ?>" aria-label="Left Align"
                                title="M�quinas e Equipamentos"
                            <?php if ($pag == 'maquinaUtilizada') echo "disabled" ?>>
                            <!--                            <i class="fa fa-truck fa-2x"></i>-->
                            <i class="fa icon-driller fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <a <?php if ($pag != 'materialUtilizado') echo 'href="' . HOME_URI . '/'.$actionForm . '/materialUtilizado"'; ?> >
                        <button type="button" class="btn <?php
                        if ($_SESSION['dadosCheck']['statusOs'] == 10) {
                            echo (!empty($_SESSION['dadosCheck']['material']) || ($_SESSION['dadosCheck']['materialCheck'] && ($_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) ? "btn-success" : "btn-danger";
                        } else {
                            echo "btn-default";
                        } ?>" aria-label="Left Align" title="Materiais Utilizados"
                            <?php if ($pag == 'materialUtilizado') echo "disabled" ?>>
                            <!--                            <i class="fa fa-wrench fa-2x"></i>-->
                            <i class="fa icon-barrow-with-construction-materials fa-2x"></i>
                        </button>
                    </a>
                </div>
                <div class="btn-group">
                    <?php
                    if ($_SESSION['dadosCheck']['statusOs'] == 10) {
                        if ($tipoOS == 'osmp/vp' || $tipoOS == 'osmp/Vp') {
                            if ($dados['km_inicial'] && $dados['km_final']) {
                                if (!empty($maoObra) && !empty($tempoTotal) && !empty($regExecucao) && (!empty($_SESSION['dadosCheck']['material']) || ($_SESSION['dadosCheck']['materialCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs'])) && (!empty($_SESSION['dadosCheck']['maquina']) || ($_SESSION['dadosCheck']['maquinaCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) {
                                    echo ($pag != 'encerramento') ? '<a href="' . HOME_URI . '/'.$actionForm . '/encerramento">' : '<a>';
                                    echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento" ');
                                    echo ($pag == 'encerramento') ? "disabled >" : ">";
                                    echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                    echo('</a>');
                                } else {
                                    echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                                    echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                                    echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                    echo('</div>');
                                }
                            } else {
                                echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                                echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                                echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                echo('</div>');
                            }
                        } else {
                            if (!empty($maoObra) && !empty($tempoTotal) && !empty($regExecucao) && (!empty($_SESSION['dadosCheck']['material']) || ($_SESSION['dadosCheck']['materialCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs'])) && (!empty($_SESSION['dadosCheck']['maquina']) || ($_SESSION['dadosCheck']['maquinaCheck'] && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs']))) {
                                echo ($pag != 'encerramento') ? '<a href="' . HOME_URI . '/'.$actionForm . '/encerramento">' : '<a>';
                                echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento" ');
                                echo ($pag == 'encerramento') ? "disabled >" : ">";
                                echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                echo('</a>');
                            } else {
                                echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                                echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                                echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                                echo('</div>');
                            }
                        }
                    } else {
                        if (!empty($maoObra) && !empty($tempoTotal) && !empty($regExecucao)) {
                            echo ($pag != 'encerramento') ? '<a href="' . HOME_URI . '/'.$actionForm . '/encerramento">' : '<a>';
                            echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento" ');
                            echo ($pag == 'encerramento') ? "disabled >" : ">";
                            echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                            echo('</a>');
                        } else {
                            echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                            echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                            echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                            echo('</div>');
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

