<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 17/03/2016
 * Time: 09:19
 */
?>

<div class="page-header">
    <h1><?php echo $tituloOs; ?></h1>
</div>

<div class="row">
    <form class="form-group" method="post" action="<?php echo HOME_URI . "/". $actionForm; ?>/salvarMaterialUtilizado" id="osMaterialUtilizado">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>Materiais Utilizados</label></div>

                <div class="panel-body">
                    <div class="panel panel-default hidden-print">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="nomeMaterial">Nome</label>

                                    <input type="hidden" id="codigoMaterial" name="codigoMaterial" readonly/>
                                    <input id="nomeMaterial" name="nomeMaterial" class="form-control" list="nomeMaterialDataList" autocomplete="off"/>

                                    <datalist id="nomeMaterialDataList">
                                        <?php
                                        if($actionForm == "moduloOsm"){
                                            $mr = $this->medoo->select("v_osm", "grupo_atuado", ['cod_osm' => $_SESSION['refillOs']['codigoOs']]);
                                            $mr = $mr[0];
                                        }

                                        if($actionForm == "moduloOsp"){
                                            $mr = $this->medoo->select("v_osp", "grupo_atuado", ['cod_osp' => $_SESSION['refillOs']['codigoOs']]);
                                            $mr = $mr[0];
                                        }
                                        $check;
                                        switch($mr){
                                            case 22:
                                                $check = true;
                                                $selectEquipamento = $this->medoo->select("v_material", ['nome_material','cod_material', 'descricao_material'],
                                                    ["ORDER" => "nome_material", "AND" => ["cod_categoria" => 32, "cod_grupo" => 22 , "ativo" => 's']]);
                                                break;
                                            case 23:
                                                $check = true;
                                                $selectEquipamento = $this->medoo->select("v_material", ['nome_material','cod_material', 'descricao_material'],
                                                    ["ORDER" => "nome_material", "AND" => ["cod_categoria" => 32, "cod_grupo" => 23 , "ativo" => 's']]);
                                                break;
                                            default:
                                                $check = false;
                                                $selectEquipamento = $this->medoo->select("v_material", ['nome_material','cod_material', 'descricao_material'],
                                                    ["ORDER" => "nome_material", "AND" => ["cod_categoria" => 32, "ativo" => 's', "OR" => ["cod_grupo[!]" => [22,23], "cod_grupo" => null]]]);
                                                break;
                                        }

                                        foreach($selectEquipamento as $key => $value){
                                            echo("<option id='{$value['cod_material']}' value='{$value['nome_material']} . {$value['descricao_material']}'></option>");
                                        }
                                        ?>
                                    </datalist>
                                </div>
                                <div class="col-md-2">
                                    <label>Sem informa��o</label>
                                    <input type="checkbox" class="form-control" name="checkMaquina" <?php if(!empty($_SESSION['dadosCheck']['materialCheck']) && $_SESSION['dadosCheck']['codigoOs'] == $_SESSION['refillOs']['codigoOs'] ) echo "checked" ?> >
                                </div>
                                <div class="col-md-2">
                                    <label>C�digo OS</label>
                                    <input readonly name="codigoOs" class="form-control" value="<?php echo($_SESSION['refillOs']['codigoOs']); ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <label>Qtd. Utilizada</label>
                                    <input class="form-control float" type="text" name="qtdUtilizada">
                                </div>

                                <div class="col-md-2">
                                    <label>Un.</label>
                                        <?php
                                        echo('<select id="unidadeMaterial" name="unidadeMaterial" class="form-control">');
                                        if($check)
                                        {

                                            $unidade = $this->medoo->select('unidade_medida', '*');

                                            echo('<option selected disabled value="">Selecione a Unidade</option>');

                                            foreach($unidade as $key => $value){
                                                echo("<option value='{$value['cod_uni_medida']}'>{$value['sigla_uni_medida']} - {$value['nome_uni_medida']}</option>");
                                            }
                                        }else{
                                            echo("<option id='optionUnidadeMedidaFixa' value=''></option>");
                                        }
                                        echo('</select>');
                                        ?>


                                </div>

                                <div class="col-md-2">
                                    <label>Estado</label>
                                    <select class="form-control" name="estadoMaterial">
                                        <option disabled selected value="">Selecione</option>
                                        <option value="n">Novo</option>
                                        <option value="u">Usado</option>
                                        <?php
                                        if($check)
                                            echo ("<option value='r'>Reparado</option>")
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Origem</label>
                                    <select name="origemMaterial" class="form-control">
                                        <option disabled selected value="">Selecione</option>
                                        <option value="s">MetroService</option>
                                        <option value="f">MetroFor</option>
                                        <option value="t">TSM</option>
                                        <option value="o">Outros</option>
                                    </select>
                                </div>
                                <div class="col-md-3" id="outraOrigem" style="display: none;" >
                                    <label>Outro</label>
                                    <input type="text" name="outroOrigem" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2" style="margin-left: 1em; margin-bottom: 1em;">
                                <div class="btn-group-lg">
                                    <button type="button" id="addMaterialOs" class="btn btn-lg btn-default" >
                                        <i class="fa fa-plus fa-fw"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Materiais Utilizados</label></div>

                        <div class="panel-body">
                            <div id="materialUtilizadoAdicionado">
                                <?php
                                $materialOs = $this->medoo->query($sql);
                                $contador = 0;

                                $estadoMat = array("n" => "Novo", "u" => "Usado", "r" => "Reparado");
                                $origemMat = array("s" => "MetroService", "f" => "MetroFor", "t" => "TSM", "o" => "Outros");

                                foreach ($materialOs as $dados => $value) {
                                    $contador += 1;
                                    echo('<div class="row">');

                                    echo('<div class="col-md-2"> <label>Cod.</label> <input type="text" class="form-control" disabled value="'.$value['cod_material'].'"></div>');
                                    echo('<input type="hidden" name="codigoMaterialDelete'.$contador.'" value="'.$value['cod_material'].'" >');

                                    echo('<div class="col-md-3"><label>Nome</label> <input type="text" class="form-control" disabled value="'.$value['nome_material'].'"></div>');

                                    echo('<div class="col-md-1"><label>Un.</label> <input type="text" class="form-control" disabled value="'.$value['sigla_uni_medida'].'"></div>');

                                    echo('<div class="col-md-1"><label>Qtd</label> <input type="text" readonly class="form-control" value="'.$value['utilizado'].'"></div>');

                                    echo('<div class="col-md-2"><label>Estado</label> <input type="text" disabled class="form-control" value="'.$estadoMat[$value['estado']].'"></div>');

                                    echo('<div class="col-md-2"><label>Origem</label> <input type="text" disabled class="form-control" value="'.$origemMat[$value['origem']].' '.$value['descricao_origem'].'"></div>');

                                    echo('<div class="col-md-1"><label>Excluir</label> <input type="checkbox" class="checkbox" name="retirarMaterial'.$contador.'"></div>');
                                    echo('</div>');
                                }

                                echo('<input type="hidden" value="'.$contador.'" name="contadorMaterial">');
                                ?>

                                <div class="row" style="border-bottom: 1px solid #000000; padding-top: 1em"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <?php
                        require_once(ABSPATH . "/views/_includes/formularios/os/btnNavegacao.php");
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>