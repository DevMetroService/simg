<div class="page-header">
    <h1><?php echo $tituloOs; ?></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Tempos Totais</label></div>

            <div class="panel-body">
                <form class="form-group" method="post" id="formTemposTotais"
                      action="<?php echo HOME_URI . "/" . $actionForm; ?>/salvarTemposTotais">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading"><label>Recebimento pelo T�cnico</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-4">
                                            <label>Data/Hora</label>
                                            <?php
                                            if (!empty($selectTempo)) {
                                                echo('<input readonly name="dataRecebimento" value="' . $this->parse_timestamp($selectTempo['data_recebimento']) . '" class="form-control timeStamp">');
                                            } else {
                                                echo('<input readonly name="dataRecebimento" value="' . $this->parse_timestamp($selectAbertura) . '" class="form-control timeStamp">');
                                            }
                                            ?>

                                        </div>
                                        <div class="col-md-2 col-xs-4">
                                            <label>Atraso</label>
                                            <select name="atrasoRecebimento" class="form-control">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_recebimento']);
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-offset-5 col-md-2 col-xs-4">
                                            <label>C�digo OS</label>
                                            <input readonly name="codigoOs" class="form-control"
                                                   value="<?php echo($_SESSION['refillOs']['codigoOs']); ?>">
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading"><label>Prepara��o e Acesso</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-4">
                                            <label>In�cio</label>
                                            <input class="form-control timeStamp tempoTotais tt1"
                                                   data-target="1" required name="preparacaoInicio"
                                                   value="<?php if (!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_preparacao'])); ?>">
                                        </div>
                                        <div class="col-md-2 col-xs-4">
                                            <label>Pronto para Sair</label>
                                            <input class="form-control timeStamp tempoTotais tt2"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_ace_pronto'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="2" required name="acessoProntoSair"/>
                                        </div>
                                        <div class="col-md-2 col-xs-4">
                                            <label>Sa�da Autorizada</label>
                                            <input class="form-control timeStamp tempoTotais tt3"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_ace_saida_autorizada'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="3" required name="acessoSaidaAutorizada"/>
                                        </div>
                                        <div class="col-md-2 col-xs-4">
                                            <label>Sa�da</label>
                                            <input class="form-control timeStamp tempoTotais tt4"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_ace_saida'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="4" required name="acessoSaida"/>
                                        </div>
                                        <div class="col-md-2 col-xs-4">
                                            <label>Chegada</label>
                                            <input class="form-control timeStamp tempoTotais tt5"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_ace_chegada'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="5" name="acessoChegada"/>
                                        </div>
                                    </div>

                                    <div class="row hidden-print">
                                        <div class="col-md-2 ">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoIniPreparacao">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_preparacao']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoAcessoProntoSair">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_ace_pronto']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoAcessoSaida">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_ace_saida']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoAcessoChegada">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_ace_chegada']);
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Execu��o</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-6">
                                            <label>In�cio</label>
                                            <input class="form-control timeStamp tempoTotais tt6"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_exec_inicio'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="6" required
                                                   name="execucaoInicio"/>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoExecInicio">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_exec_inicio']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-2 col-xs-6">
                                            <label>T�rmino</label>
                                            <input class="form-control timeStamp tempoTotais tt7"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_exec_termino'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="7" required
                                                   name="execucaoTermino"/>
                                        </div>
                                        <div class="col-md-2 col-xs-6">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoExecTermino">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_exec_termino']);
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Regresso</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2 col-xs-3">
                                            <label>Pronto para Sair</label>
                                            <input class="form-control timeStamp tempoTotais tt8"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_reg_pronto'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="8" required
                                                   name="regressoProntoSair"/>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>Sa�da Autorizada</label>
                                            <input class="form-control timeStamp tempoTotais tt9"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_reg_saida_autorizada'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="9" required
                                                   name="regressoSaidaAutorizada"/>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>Chegada</label>
                                            <input class="form-control timeStamp tempoTotais tt10"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_reg_chegada'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="10" required
                                                   name="regressoChegada"/>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <label>Desmobiliza��o</label>
                                            <input class="form-control timeStamp tempoTotais tt11"
                                                <?php
                                                if (!empty($selectTempo)) {
                                                    echo("value='{$this->parse_timestamp($selectTempo['data_desmobilizacao'])}'");
                                                } else {
                                                    echo 'readonly';
                                                }
                                                ?>
                                                   data-target="11" required
                                                   name="regressoDesmobilizacao"/>
                                        </div>
                                    </div>

                                    <div class="row hidden-print">
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoRegressoProntoSair">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_reg_pronto']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoRegressoChegada">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_reg_chegada']);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Observa��o</label></div>

                                <div class="panel-body">
                                    <label>Observa��o</label>
                                    <textarea rows="3" cols="110" class="form-control"
                                              name="descricaoTempo"><?php echo($selectTempo['descricao']); ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <?php
                        require_once(ABSPATH . "/views/_includes/formularios/os/btnNavegacao.php");
                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
