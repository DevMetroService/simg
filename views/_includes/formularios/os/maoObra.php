<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 16/03/2016
 * Time: 12:31
 */
?>

<div class="page-header">
    <h1><?php echo $tituloOs; ?></h1>
</div>

<div class="row">
    <form method="POST" action="<?php echo HOME_URI . "/" . $actionForm; ?>/salvarMaoObra" class="form-group"
          id="formMaoObra">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>M�o de Obra</label></div>

                <div class="panel-body">

                    <div class="panel panel-default hidden-print">
                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-2">
                                    <label>Tipo Funcionario</label>
                                    <select name="codTipoFuncionario" class="form-control">
                                        <option value=""> Selecione</option>
                                        <?php
                                        $tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);

                                        foreach( $tipo_funcionario as $key =>$values){
                                            echo"<option value='{$values["cod_tipo_funcionario"]}'>{$values["descricao"]}</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>Matr�cula</label>
                                    <input type="text" class="form-control number" name="matriculaMaoObraOs" disabled>
                                    <input type="hidden" name="codFuncionarioMaoObraOs">
                                </div>

                                <div class="col-md-6">
                                    <label>Nome</label>
                                    <input type="text" class="form-control" name="nomeMaoObraOs" list="nomeDataList"
                                           autocomplete="off"/>

                                    <datalist id="nomeDataList">
                                    </datalist>
                                </div>
                                <div class="col-md-offset--1 col-md-2">
                                    <label>C�digo OS</label>
                                    <input readonly name="codigoOs" class="form-control"
                                           value="<?php echo($_SESSION['refillOs']['codigoOs']); ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <label>In�cio</label>
                                    <input type="text" <?php echo $disableMaoObra; ?>
                                           class="form-control hora validaHora" name="inicioServicoMaoObraOs">
                                </div>
                                <div class="col-md-2">
                                    <label>T�rmino</label>
                                    <input type="text" <?php echo $disableMaoObra; ?>
                                           class="form-control hora validaHora" name="terminoServicoMaoObraOs">
                                </div>
                                <div class="col-md-2">
                                    <label>Total de Horas</label>
                                    <input type="text" class="form-control hora" disabled name="totalHoraMaoObraOs">
                                </div>

                                <div class="col-md-offset-2 col-md-2">
                                    <label>In�cio Execu��o</label>
                                    <input value="<?php echo $this->parse_timestamp($tempoTotal[0]['data_exec_inicio'])?>" class="form-control" readonly>
                                </div>
                                <div class="col-md-2">
                                    <label>T�rmino Execu��o</label>
                                    <input value="<?php echo $this->parse_timestamp($tempoTotal[0]['data_exec_termino'])?>" class="form-control" readonly>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-2" style="margin-left: 1em; margin-bottom: 1em;">
                                <div class="btn-group-lg">
                                    <button type="button" <?php if (!$disableMaoObra) {
                                        echo('id="addMaoObraOs"');
                                    } else {
                                        echo('rel="tempoTotal" title="Necess�rio preencher Tempos Totais"');
                                    }; ?> class="btn btn-lg btn-default">
                                        <i class="fa fa-plus fa-fw"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><label>M�o de Obra Utilizada</label></div>
                        <div class="panel-body">
                            <div id="maoObraOs">

                                <?php
                                $contador = 0;
                                foreach ($maoObraFuncionario as $key => $value) {
                                    if(empty($value['cod_osmao']))
                                    {
                                      if(empty($value['cod_ospmao']))
                                      {
                                        $value['cod_osmao'] = $value['cod_osmp_mao_de_obra'];
                                      }else{
                                        $value['cod_osmao'] = $value['cod_ospmao'];
                                      }
                                    }
                                    $contador += 1;
                                    echo('<div class="row">');
                                    echo('<div class="col-md-2 col-xs-2"> <label>Matr�cula</label> <input type="text" class="form-control" disabled value="' . $value['matricula'] . '"></div>');
                                    echo('<input type="hidden" class="form-control" value="' . $value['cod_funcionario'] . '" name="matriculaFuncionarioMaoObraOs' . $contador . '">');
                                    echo('<input type="hidden" class="form-control" value="' . $value['cod_osmao'] . '" name="codMaoObraOs' . $contador . '">');

                                    echo('<div class="col-md-4 col-xs-10"><label>Nome</label> <input type="text" class="form-control" disabled value="' . $value['nome_funcionario'] . '"></div>');

                                    echo('<div class="col-md-2 col-xs-4"><label>In�cio</label> <input type="text" class="form-control" disabled value="' . $value['data_inicio'] . '"></div>');
                                    echo('<div class="col-md-2 col-xs-4"><label>T�rmino</label> <input type="text" class="form-control" disabled value="' . $value['data_termino'] . '"></div>');
                                    echo('<div class="col-md-1 hidden-print"><label>Excluir</label> <input type="checkbox" class="checkbox" name="retirarFuncionario' . $contador . '"></div>');
                                    echo('</div>');
                                }

                                echo('<input type="hidden" value="' . $contador . '" name="contadorMaoObra">');

                                ?>

                                <div class="row" style="border-bottom: 1px solid #000000; padding-top: 1em"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row hidden-print">
            <?php
            require_once(ABSPATH . "/views/_includes/formularios/os/btnNavegacao.php");
            ?>
        </div>
    </form>
</div>
