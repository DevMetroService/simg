<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/04/2016
 * Time: 11:07
 */
$nivelAcesso = array(
    "1" => "RH",
    "2" => "ccm",
    "2.1" => "Supervis�o",
    "2.3" => "Usu�rio Simples",
    "3" => "Ti",
    "4" => "Materiais",
    "5" => "Equipe de Manuten��o",
    "6" => "Diretoria"
);

?>
<div class="page-header">
    <h2>Cadastro de Usu�rio</h2>
</div>

<form method="post" action="<?php echo HOME_URI?>cadastroGeral/cadastrarUsuario" class="form-group">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Cadastro de Usu�rio</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-3">
                            <label>Nome do usu�rio(Login)</label>
                            <input value="<?php echo $dadosUser['usuario'] ?>" type="text" class="form-control"
                                   name="nomeUsuario" placeholder="Ex.: nome.usuario"/>
                        </div>
                        <div class="col-md-3">
                            <label>N�vel de acesso</label>
                            <select name="nivelAcesso" required class="form-control">
                                <option value="" selected disabled>Selecione o n�vel</option>
                                <?php
                                foreach ($nivelAcesso as $dados => $value) {
                                    if ($dadosUser['nivel'] == $dados)
                                        echo("<option value='{$dados}' selected>{$value}</option>");
                                    else
                                        echo("<option value='{$dados}'>{$value}</option>");
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>C�digo Funcionario</label>
                            <input class="form-control" value="<?php echo $dadosUser['matricula'] ?>"
                                   name="codFuncionarioUsuario" type="text"
                                   placeholder="Informe, se houver, a matr�cula do usu�rio.">
                        </div>
                    </div>
                    <div class="row" id="equipeUsuario">
                        <div class="col-md-6">
                            <label>Equipe</label>
                            <br/><select name="equipeUsuario[]" class="form-control multiSelect"
                                         multiple="multiple">
                                <optgroup label="Selecionar Todos">
                                    <?php
                                    $equipe = $this->medoo->select('equipe', "*");

                                    foreach ($equipe as $dados => $value) {
                                        echo("<option value='{$value['cod_equipe']}'>{$value['nome_equipe']}</option>");
                                    }
                                    ?>
                                </optgroup>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-offset-8 col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">A��es</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group  btn-group-justified " role="group">

                                <?php echo $btnsAcao; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>