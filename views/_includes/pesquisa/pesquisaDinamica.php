<div class="page-header">
    <h1>Pesquisa Din�mica</h1>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Filtro (s)</label></div>
            <div class="panel-body">
                <div class="col-md-3">
                    <label>Sistema</label>
                    <select name="sistema" class="form-control">
                        <option value="" selected disabled="">Selecione o Sistema</option>
                        <option value="sistemaEd">Edifica��es</option>
                        <option value="sistemaRa">Rede A�rea</option>
                        <option value="sistemaSb">Subesta��o</option>
                        <option value="sistemaVp">Via Permanente</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <label>Pesquisar</label>
                    <button id="btnPesquisaPrimeiroNivel"
                            class="btn btn-large btn-default btn-block form-control text-center">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
        <div id="divSegundoNivel" class="panel panel-default" style="display: none">
            <div class="panel-heading">
                <div class="text-right">
                    <span class="btn btn-circle" id="divClose"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-2">
                    <label>Periodicidade</label>
                    <select class="form-control">
                        <option selected disabled>Todos</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Linha</label>
                    <select class="form-control">
                        <option selected disabled>Todos</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Local</label>
                    <select class="form-control">
                        <option selected disabled>Todos</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Servi�o</label>
                    <select class="form-control">
                        <option selected disabled>Todos</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <label>Pesquisar</label>
                    <button id="btnPesquisaSegundoNivel"
                            class="btn btn-large btn-default btn-block form-control text-center">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>