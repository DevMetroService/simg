<?php
//var_dump($_SESSION);
//var_dump($_SESSION['dadosPesquisaCronograma']);
?>

<div class="page-header">
    <h2>Pesquisa Cronograma</h2>
</div>

<form action="<?php echo HOME_URI; ?>/dashboardGeral/executarPesquisaCronograma" class="form-group" method="post" id="pesquisa">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Pesquisa Cronograma</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Dados Gerais</label></div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>N� Cronograma</label>
                                        <input class="form-control number" name="numeroPesquisaCronograma"
                                               value="<?php echo($dadosRefill['numeroPesquisaCronograma']); ?>"/>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Grupo Sistema <span style="color: red">(*)</span></label>
                                        <?php
                                        if (empty($dadosRefill['grupoPesquisaCronograma'])){
                                            $dadosRefill['grupoPesquisaCronograma'] = 21; // Edifica��o
                                        }

                                        $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["AND" => ["cod_tipo_grupo" => 5, "cod_grupo[!]" => [29,30,31]], "ORDER" => "nome_grupo"]);
                                        $this->form->getSelectGrupo($dadosRefill['grupoPesquisaCronograma'], $grupo, 'grupoPesquisaCronograma', true);
                                        ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Sistema</label>
                                        <select class="form-control" name="sistemaPesquisaCronograma">
                                            <option value="">Sistemas</option>
                                            <?php
                                            $sistema = $this->medoo->select("grupo_sistema", ["[><]sistema" => "cod_sistema"], ['cod_sistema', 'nome_sistema'], ["cod_grupo" => $dadosRefill['grupoPesquisaCronograma']]);

                                            foreach ($sistema as $dados => $value) {
                                                if (!empty($dadosRefill) && $dadosRefill['sistemaPesquisaCronograma'] == $value['cod_sistema'])
                                                    echo("<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>");
                                                else
                                                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Sub-Sistema</label>
                                        <select class="form-control" name="subsistemaPesquisaCronograma">
                                            <option value="">Sub-Sistemas</option>
                                            <?php
                                            if ($dadosRefill['sistemaPesquisaCronograma']) {
                                                $subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema" => "cod_subsistema"], ['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $dadosRefill['sistemaPesquisaCronograma']]);

                                                foreach ($subsistema as $dados => $value) {
                                                    if (!empty($dadosRefill) && $dadosRefill['subsistemaPesquisaCronograma'] == $value['cod_subsistema'])
                                                        echo("<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>");
                                                    else
                                                        echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Servi�o</label>
                                        <select class="form-control" name="servicoPesquisaCronograma">
                                            <option value="">TODOS</option>
                                            <?php
                                            $selectServico = $this->medoo->select('servico_pmp', ['nome_servico_pmp', 'cod_servico_pmp'], ['cod_grupo' => $dadosRefill['grupoPesquisaCronograma']]);
                                            foreach ($selectServico as $dados => $value) {
                                                if (!empty($dadosRefill) && $dadosRefill['servicoPesquisaCronograma'] == $value['cod_servico_pmp'])
                                                    echo('<option value="' . $value['cod_servico_pmp'] . '" selected>' . $value['nome_servico_pmp'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_servico_pmp'] . '">' . $value['nome_servico_pmp'] . '</option>');
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Periodicidade</label>
                                        <select class="form-control" name="periodicidadePesquisaCronograma">
                                            <option value="">Todos</option>
                                            <?php
                                            $selectPeriodicidade = $this->medoo->select('tipo_periodicidade', ['nome_periodicidade', 'cod_tipo_periodicidade']);
                                            foreach ($selectPeriodicidade as $dados => $value) {
                                                if (!empty($dadosRefill) && $dadosRefill['periodicidadePesquisaCronograma'] == $value['cod_tipo_periodicidade'])
                                                    echo('<option value="' . $value['cod_tipo_periodicidade'] . '" selected>' . $value['nome_periodicidade'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_tipo_periodicidade'] . '">' . $value['nome_periodicidade'] . '</option>');
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Status / Per�odo</label></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Status</label>
                                        <select class="form-control" name="statusPesquisaCronograma">
                                            <option value="">Selecione o Status</option>
                                            <?php
                                            $selectStatus = $this->medoo->select("status", ['cod_status', 'nome_status'], ["tipo_status" => "P"]);

                                            foreach ($selectStatus as $dados => $value) {
                                                echo("<option value='{$value['cod_status']}' ");
                                                if ($value['cod_status'] == $dadosRefill['statusPesquisaCronograma']) {
                                                    echo(' selected');
                                                }
                                                echo(' >' . $value['nome_status'] . '</option>');
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Quinzena</label>
                                        <input class="form-control number" name="quinzenaPesquisaCronograma"
                                               value="<?php if (!empty($dadosRefill)) echo($dadosRefill['quinzenaPesquisaCronograma']); ?>" type="text">
                                    </div>

                                    <div class="col-md-3">
                                        <label>M�s</label>
                                        <select class="form-control" name="mesPesquisaCronograma" value="">
                                            <option value="" selected>Todos</option>
                                            <?php
                                            foreach (MainController::$monthComplete as $key => $value) {
                                                echo (date('n') == $key) ? "<option value='$key'>$value</option>" : "<option value='$key'>$value</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Ano</label>
                                        <input class="form-control number" name="anoPesquisaCronograma"
                                               value="<?php if (!empty($dadosRefill)) echo($dadosRefill['anoPesquisaCronograma']); ?>" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Local</label></div>

                            <div class="panel-body">
                                <div id="linha" class="col-md-4">
                                    <label>Linha</label>
                                    <select class="form-control" name="linhaPesquisaCronograma">
                                        <option value="">Todos</option>
                                        <?php
                                        $selectLinha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);
                                        foreach ($selectLinha as $dados => $value) {
                                            if (!empty($dadosRefill) && $dadosRefill['linhaPesquisaCronograma'] == $value['cod_linha'])
                                                echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div id="local" class="col-md-4">
                                    <label>Local</label>
                                    <select class="form-control" name="localPesquisaCronograma">
                                        <option value="">Todos</option>
                                        <?php
                                        if(!empty($dadosRefill['linhaPesquisaCronograma'])) {
                                            $selectLinha = $this->medoo->select('local', ['nome_local', 'cod_local'], ['cod_linha' => $dadosRefill['linhaPesquisaCronograma']]);
                                            foreach ($selectLinha as $dados => $value) {
                                                if (!empty($dadosRefill) && $dadosRefill['localPesquisaCronograma'] == $value['cod_local'])
                                                    echo('<option value="' . $value['cod_local'] . '" selected>' . $value['nome_local'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_local'] . '">' . $value['nome_local'] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div id="estacaoInicial" class="col-md-4">
                                    <label>Esta��o Inicial</label>
                                    <select class="form-control" name="estacaoInicialPesquisaCronograma">
                                        <option value="">Todos</option>
                                        <?php
                                        if ($dadosRefill['linhaPesquisaCronograma']) {
                                            $selectEstacao = $this->medoo->select('estacao', ['nome_estacao', 'cod_estacao'], ['cod_linha' => $dadosRefill['linhaPesquisaCronograma']]);
                                            foreach ($selectEstacao as $dados => $value) {
                                                if ($dadosRefill['estacaoInicialPesquisaCronograma'] == $value['cod_estacao'])
                                                    echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div id="estacaoFinal" class="col-md-4">
                                    <label>Esta��o Final</label>
                                    <select class="form-control" name="estacaoFinalPesquisaCronograma">
                                        <option value="">Todos</option>
                                        <?php
                                        if ($dadosRefill['linhaPesquisaCronograma']) {
                                            foreach ($selectEstacao as $dados => $value) {
                                                if ($dadosRefill['estacaoFinalPesquisaCronograma'] == $value['cod_estacao'])
                                                    echo('<option value="' . $value['cod_estacao'] . '" selected>' . $value['nome_estacao'] . '</option>');
                                                else
                                                    echo('<option value="' . $value['cod_estacao'] . '">' . $value['nome_estacao'] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
            <button type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro</button>
        </div>
    </div>

    <div class="row" style="padding-top: 1em"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Resultado da Pesquisa
                    <strong>Total
                        Encontrado: <?php echo (!empty($_SESSION['resultadoPesquisaCronograma'])) ? count($_SESSION['resultadoPesquisaCronograma']) : 0; ?></strong>
                </div>

                <div class="panel-body">
                    <table id="resultadoPesquisaCronograma" class="table table-striped table-bordered no-footer">
                        <thead id="headIndicadorCronograma">
                        <tr role="row">
                            <th>N� Cronograma</th>
                            <th>Local</th>
                            <th>Servi�o</th>
                            <th>Quinzena</th>
                            <th>Status</th>
                            <th>A��es</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (!empty($_SESSION['resultadoPesquisaCronograma'])) {
                            foreach ($_SESSION['resultadoPesquisaCronograma'] as $dados => $value) {
                                echo('<tr>');
                                echo('<td>' . $value['cod_cronograma'] . "<input type='hidden' value='{$value['nome_grupo']}' ></td>");
                                echo("<td><span class='primary-info'>{$value['linha']}</span><br /><span class='sub-info'>{$value['local']}</span></td>");
                                echo('<td>' . $value['servico'] . '</td>');
                                echo('<td>' . $value['quinzena'] . '</td>');
                                echo('<td>' . $value['status'] . '</td>');
                                echo("<td>");
                                echo("<button type='button' class='abrirModalCronograma btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirCronograma'><i class='fa fa-eye'></i></button>");
                                echo("{$btnAcoes}");
                                if($value['status'] == "Aberta"){
                                    echo(" <button type='button' class='btn btn-circle btn-success btnGerarSsmp' title='Gerar Ssmp'><i class='fa fa-check fa-fw'></i></button>");
                                }
                                echo("</td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>