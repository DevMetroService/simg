</div>
</div>
</div>
</body>


<footer>

<div id='boxSetaVolteAoTopo' class='hidden-print'>
    <span id='setaVolteAoTopo' title='Volte ao Topo' class='fa fa-chevron-circle-up'></span>
</div>

<div id='footer' class='hidden-print'>
    <p style='padding:20px; text-align: center'>
        &copy; Copyright 2014 <a href='#' data-toggle='modal' data-target='#metroServiceModal'>Metro Service</a>.Todos
        os direitos reservados.
    </p>
</div>

<?php require ABSPATH . '/includes/help/help.php'; ?>
<!-- Config Global -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/config.js' charset='UTF-8'></script>

<!-- DateTimePicker -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/jquery.datetimepicker.full.js'></script>

<!-- Chamada dos scripts de a��o do sidebar -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/metisMenu.min.js'></script>

<!-- Chamada do script principal -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/script.js' charset='UTF-8'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/functionsSocket.js' charset='UTF-8'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/scripts/scriptAPI.js'></script>
<?php if ($_SESSION['bloqueio']) echo "<script type='text/javascript' src='{$this->home_uri}/views/_js/bloqueio.js'></script>" ?>


<!-- Chamada dos plugins de formata��o de campo -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/jquery.inputmask.js'></script>
<script type='text/javascript'
        src='<?php echo HOME_URI ?>/views/_js/plugins/jquery.inputmask.numeric.extensions.js'></script>
<script type='text/javascript'
        src='<?php echo HOME_URI ?>/views/_js/plugins/jquery.inputmask.date.extensions.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/bootstrap-multiselect.js'></script>

<!-- Chamada dos plugins de gerenciamento de tabelas -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/dataTables.bootstrap.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/fixHeader.dataTable.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/moment.js'></script>

<!-- Chamada dos plugins validadores de campos -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/formValidation.min.js'></script>
<script type='text/javascript'
        src='<?php echo HOME_URI ?>/views/_js/plugins/frameworkValidation/bootstrap.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/linguageValidation/pt_BR.js'></script>

<!-- Chamada dos plugins de FullCalendar -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/moment.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/fullcalendar.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/pt-br.js' charset='UTF-8'></script>

<!-- Chamada de plugins de gr�ficos -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/d3.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/Chart.bundle.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/newChart.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/highcharts.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/highcharts-export.js'></script>

<!-- Plugin Alert e Notifica��o RealTime -->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/jquery.msgbox.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/bootstrap-notify.min.js'></script>

<!-- DataTables -->
<!--<script type='text/javascript' src='--><?php //echo HOME_URI ?><!--/views/_js/plugins/DataTables/dataTables.min.js'></script>-->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/dataTables.min.js'></script>
<script type='text/javascript'
        src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/dataTables.buttons.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/buttons.flash.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/jszip.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/pdfmake.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/vfs_fonts.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/buttons.html5.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/buttons.print.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/buttons.colVis.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/plugins/datatable/datetime-moment.js'></script>

<!---->
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/scripts/<?= $this->script; ?>'></script>
<script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/scripts/scriptValidacao.js'></script>

</footer>
</html>
