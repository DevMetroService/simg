<html lang="br">

<head>

    <?php header('Content-type: text/html; charset=iso-8859-1'); ?>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content="Consorcio Metro Service"/>
    <meta http-equiv="Set-Cookie" content="expires=Thursday, 01-Jan-2015 00:00:00 GMT">
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-store"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>


    <!--<script src="http://10.0.1.141:4555/socket.io/socket.io.js"></script>-->

    <title><?php echo $this->titulo ?></title>
    <link rel="icon" type="image/jpg" href="<?php echo HOME_URI ?>/views/_images/mini_metroservice_logo.png"/>

    <!-- Bloco de CSS -->
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/multselect.css">
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/sb-admin-2.css">

    <!-- Jquery UI-->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/jquery-ui.css">

    <!-- Chamada do css para auxilio dos campos de valida��o -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/formValidation.min.css">

    <!-- Chamada do css para auxilio do FullCalendar -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/fullcalendar.css">
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/fullcalendar.print.css" media='print'>

    <!-- Chamada do css do font-Awesome -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/font-awesome/css/font-awesome.min.css">
    <!-- Chamada do css do icomoon -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/icomoon/style.css">
    <!-- Chamada do css morris para graficos -->
<!--    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">-->

    <!-- Chamada do css para graficos -->
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/relatorios.css">
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/nv.d3.css">
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/style.css">

    <!-- Plugin DateTimePicker Jquery -->
<!--    <link type="text/css" rel="stylesheet"-->
<!--          href="--><?php //echo HOME_URI ?><!--/views/_css/bootstrap/plugins/bootstrap-datetimepicker.min.css">-->

    <!-- Plugin Select Jquery -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/jquery.editable-select.min.css">

    <!-- Plugin Select Jquery -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/bootstrap/plugins/animate.css">

    <?php # include_once("strings/strings_pt_BR.php");?>

    <!--Datatables-->
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/datatable/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/datatable/buttons.dataTables.min.css">

<!--    <link rel="stylesheet" href="--><?php //echo HOME_URI ?><!--/views/_js/plugins/DataTables/datatables.min.css">-->

    <!--DateimePicker-->
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/jquery.datetimepicker.min.css">

    <!--Custom-->
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/custom.css">

    <!-- Bloco de BootStrap / Jquery  -->
      <script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/jquery.min.js'></script>
      <script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/jquery-ui.js'></script>
      <script type='text/javascript' src='<?php echo HOME_URI ?>/views/_js/bootstrap.min.js'></script>

</head>

<body data-spy="scroll" data-target=".scrollAuto">
<!--<div class="wrapper">-->
