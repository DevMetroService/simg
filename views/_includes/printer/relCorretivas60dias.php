<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 10/10/2017
 * Time: 10:13
 */

require_once(ABSPATH . '/functions/functionsRelatorio.php');

$where = returnWhere($_POST);

$sql = "SELECT cod_saf, data_abertura, nome_grupo, nome_status, nome_linha || ' - ' || descricao_trecho AS local, nome_avaria, nivel,
date_part('day', CURRENT_TIMESTAMP - data_abertura - COALESCE(diferenca,('0 days'))) AS diasEmAberto,
(CASE
	WHEN data_abertura + COALESCE(diferenca,('0 days')) + ('60 days') :: INTERVAL < CURRENT_TIMESTAMP THEN '1'
	WHEN data_abertura + COALESCE(diferenca,('0 days')) + ('40 days') :: INTERVAL < CURRENT_TIMESTAMP THEN '2' 
	WHEN data_abertura + COALESCE(diferenca,('0 days')) + ('20 days') :: INTERVAL < CURRENT_TIMESTAMP THEN '3'
	WHEN data_abertura + COALESCE(diferenca,('0 days')) + ('10 days') :: INTERVAL < CURRENT_TIMESTAMP THEN '5'
	ELSE '4'
END) AS categoria

FROM v_saf
LEFT JOIN (
	SELECT SUM(diferenca) AS diferenca, cod_saf FROM (
		SELECT cod_saf,
			(CASE
				WHEN (SELECT data_status FROM status_saf s2 WHERE s2.cod_ssaf > s.cod_ssaf AND s2.cod_saf = s.cod_saf ORDER BY s2.cod_ssaf LIMIT 1) IS NULL
					THEN CURRENT_TIMESTAMP - data_status
				ELSE
					(SELECT data_status FROM status_saf s2 WHERE s2.cod_ssaf > s.cod_ssaf AND s2.cod_saf = s.cod_saf ORDER BY s2.cod_ssaf LIMIT 1)-data_status
			END) AS diferenca
		FROM status_saf s WHERE cod_status = 37
	) tt   GROUP BY cod_saf
) AS t USING(cod_saf)
WHERE cod_status NOT IN(14, 2, 27, 37) ";

$sqlOrder = " ORDER BY data_abertura";

if ($where) {
    $sql = $sql . " AND " . $where . $sqlOrder;
} else {
    $sql = $sql . $sqlOrder;
}

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

?>
    <div class="pagina" style="width: inherit">
        <div id="containerPrintRel" style="width: 100%;"></div>

        <div style="float: left; width: 45%; margin-bottom: 50px;">
        <table style="margin-top: 50px; font-size:15px !important; font-family: Arial; text-align: center;">
            <tr>
                <td id="tabCat0"></td>
                <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                style="color: #0b97c4; "></i> - de 0 a 10 dias
                </td>
            </tr>
            <tr>
                <td id="tabCat10"></td>
                <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                style="color: #4CAF50; "></i> - de 11 a 20 dias
                </td>
            </tr>
            <tr>
                <td id="tabCat30"></td>
                <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                style="color: #FFEB3B; "></i> - de 21 a 40 dias
                </td>
            </tr>
            <tr>
                <td id="tabCat50"></td>
                <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                style="color: #FF5722; "></i> - de 41 a 60 dias
                </td>
            </tr>
            <tr>
                <td id="tabCat60"></td>
                <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                style="color: #D50000; "></i> - Acima de 60 dias
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" id="total"></td>
                <td style="text-align: left"><i class="fa fa-area-chart fa-2x"
                                                style="color: #B0BEC5; "></i> -
                    <strong>Total</strong>
                </td>
            </tr>
        </table>
        </div>
        <div style="float: right; width: auto">
        <table style="margin-top: 50px; font-size:2em !important; font-family: Arial; text-align: center;">
            <thead>
            <tr>
                <th colspan="3">Nivel</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>C</td>
                <td style="background: yellow">B</td>
                <td style="background: red">A</td>
            </tr>
            </tbody>
        </table>
        </div>

        <table class="tableResultado">
            <thead>
            <tr style='border: solid;'>
                <th>SAF</th>
                <th>SSM</th>
                <th>OSM</th>
                <th>Status</th>
                <th>Data de Abertura</th>
                <th>Grupo</th>
                <th>Local</th>
                <th>Avaria</th>
                <th>N�vel</th>
                <th>Dias em Aberto</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($result as $dados) {
                switch ($dados['nivel']){
                    case 'A':
                        $color= 'red';
                        break;
                    case 'B':
                        $color= 'yellow';
                        break;
                    default:
                        $color= 'white';
                }

                $dataAbertura = MainController::parse_timestamp_static($dados['data_abertura']);

//                $interval = date_diff(new DateTime($dataAbertura), new DateTime());

                $hasSsm = $this->medoo->select("ssm", "cod_ssm", ["cod_saf"=> $dados['cod_saf'] ]);
                if($hasSsm){
                    $hasOsm = $this->medoo->select("osm_falha", "cod_osm", ["cod_ssm" => $hasSsm[0]["cod_ssm"] ]);
                    $hasSsm = "X";
                    if($hasOsm)
                        $hasOsm = "X";
                    else
                        $hasOsm = "-";
                }else{
                    $hasSsm = "-";
                    $hasOsm = "-";
                }


                echo("<tr style='border: solid;'>");
                echo("<td style='border: solid;width:8%'>{$dados['cod_saf']}</td>");
                echo("<td style='border: solid;width:8%'>{$hasSsm}</td>");
                echo("<td style='border: solid;width:8%'>{$hasOsm}</td>");
                echo("<td style='border: solid;width:8%'>{$dados['nome_status']}</td>");
                echo("<td style='border: solid;width:13%'>{$dataAbertura}</td>");
                echo("<td style='border: solid;width:15%'>{$dados['nome_grupo']}</td>");
                echo("<td style='border: solid;'>{$dados['local']}</td>");
                echo("<td style='border: solid;'>{$dados['nome_avaria']}</td>");
                echo("<td style='border: solid;width:7%; background: {$color}'>{$dados['nivel']}</td>");
                echo("<td style='border: solid;width:7%'>{$dados['diasemaberto']}</td>");
                echo("</tr>");
            }
            ?>
            </tbody>
        </table>
    </div>
<?php

echo "<input type='hidden' name='cat0' value='{$_POST['cat0']}'/>";
echo "<input type='hidden' name='cat10' value='{$_POST['cat10']}'/>";
echo "<input type='hidden' name='cat30' value='{$_POST['cat30']}'/>";
echo "<input type='hidden' name='cat50' value='{$_POST['cat50']}'/>";
echo "<input type='hidden' name='cat60' value='{$_POST['cat60']}'/>";

echo "<input type='hidden' name='tituloGraf' value='{$_POST['tituloGraf']}'/>";
?>