<?php

$conteudo = "Matricula;";
$conteudo .= "Nome Funcionario;";
$conteudo .= "Cargo;";
$conteudo .= "Centro de Resultado;";
$conteudo .= "Cnpj/Cpf;";
$conteudo .= "Email Corporativo;";
$conteudo .= "Email Pessoal;";
$conteudo .= "Fone Corporativo;";
$conteudo .= "Fone Pessoal;";
$conteudo .= "Loca��o\n";

$func = $this->medoo->select("funcionario_crj",
    [
        "[><]cargos_funcionarios(cf)" => ["cod_cargo"=>"cod_cargos"],
        "[><]centro_resultado(cr)" => "cod_centro_resultado",
        "[><]unidade" => ["unidade" => "cod_unidade"]
    ], [
        "matricula",
        "nome_funcionario_crj",
        "cf.descricao(cargo)",
        "cr.descricao(centro_resultado)",
        "cnpj_cpf",
        "celular_corporativo",
        "email_corporativo",
        "email_pessoal",
        "celular_pessoal",
        "nome_unidade"
    ]);

if(!empty($func)){
    foreach ($func as $dados => $value) {
        $txt = $value["matricula"] . ":::";
        $txt .= $value["nome_funcionario_crj"] . ":::";
        $txt .= $value["cargo"] . ":::";
        $txt .= $value["centro_resultado"] . ":::";
        $txt .= $value["cnpj_cpf"] . ":::";
        $txt .= $value["email_corporativo"] . ":::";
        $txt .= $value["email_pessoal"] . ":::";
        $txt .= $value["celular_corporativo"] . ":::";
        $txt .= $value["celular_pessoal"] . ":::";
        $txt .= $value["nome_unidade"];

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("FuncionariosCRJ.csv",$conteudo);



header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=FuncionariosCRJ.csv");
header("Pragma: no-cache");
header("Expires: 0");
readfile('FuncionariosCRJ.csv');

unlink("FuncionariosCRJ.csv");