<?php

$conteudo = "Matricula;";
$conteudo .= "Nome Funcionario;";
$conteudo .= "Data de Nascimento;";
$conteudo .= "Escolaridade;";
$conteudo .= "Curso T�cnico;";
$conteudo .= "Cargo;";
$conteudo .= "Centro de Resultado;";
$conteudo .= "Cpf;";
$conteudo .= "Email Corporativo;";
$conteudo .= "Email Pessoal;";
$conteudo .= "Fone Corporativo;";
$conteudo .= "Fone Pessoal;";
$conteudo .= "Loca��o\n";

$func = $this->medoo->select("v_funcionario",
    [
        "*"
    ], ["matricula[<]" => 6999]);

if(!empty($func)){
    foreach ($func as $dados => $value) {
        $txt = $value["matricula"] . ":::";
        $txt .= $value["nome_funcionario"] . ":::";
        $txt .= $value["cargo"] . ":::";
        $txt .= $value["centro_resultado"] . ":::";
        $txt .= $value["cpf"] . ":::";
        $txt .= $value["email"] . ":::";
        $txt .= $value["celular"] . ":::";
        $txt .= $value["email_pessoal"] . ":::";
        $txt .= $value["fone"] . ":::";
        $txt .= $value["nome_unidade"];

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("FuncionariosMetroService.csv",$conteudo);



header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=FuncionariosMetroService.csv");
header("Pragma: no-cache");
header("Expires: 0");
readfile('FuncionariosMetroService.csv');

unlink("FuncionariosMetroService.csv");