<?php

$conteudo = "C�digo;";
$conteudo .= "Centro de Resultado\n";

$centroCusto = $this->medoo->select("centro_resultado", "*", ["ORDER" => "descricao"]);

if(!empty($centroCusto)){
    foreach ($centroCusto as $dados => $value) {
        $txt = $value["num_centro_resultado"] . ":::";
        $txt .= $value["descricao"];

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("CentroResultado.csv",$conteudo);



header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=CentroResultado.csv");
header("Pragma: no-cache");
header("Expires: 0");
readfile('CentroResultado.csv');

unlink("CentroResultado.csv");