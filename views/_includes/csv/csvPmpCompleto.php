<?php

$conteudo = "cod_pmp;";
$conteudo .= "mao_obra;";
$conteudo .= "horas_uteis;";
$conteudo .= "homem_hora;";
$conteudo .= "turno;";
$conteudo .= "quinzena;";
$conteudo .= "ativo;";

$conteudo .= "nome_grupo;";

$conteudo .= "nome_procedimento;";

$conteudo .= "nome_periodicidade;";

$conteudo .= "nome_servico_pmp;";

$conteudo .= "nome_sistema;";

$conteudo .= "nome_subsistema;";

$conteudo .= "local_ed;";

$conteudo .= "via_vp;";
$conteudo .= "estacao_inicial_vp;";
$conteudo .= "estacao_final_vp;";
$conteudo .= "posicao_vp;";
$conteudo .= "unidade_tag;";
$conteudo .= "km_inicial_vp;";
$conteudo .= "km_final_vp;";
$conteudo .= "amv_vp;";

$conteudo .= "local_su;";

$conteudo .= "via_ra;";
$conteudo .= "posicao_ra;";
$conteudo .= "local_ra;";
$conteudo .= "poste_ra;";

$conteudo .= "local_tl;";

$conteudo .= "estacao_bl\n";

$sql = "select 

pmp.cod_pmp,
pmp.mao_obra,
pmp.horas_uteis,
pmp.homem_hora,
pmp.turno,
pmp.quinzena,
pmp.ativo,

grupo.nome_grupo,

procedimento.nome_procedimento,

tipo_periodicidade.nome_periodicidade,

servico_pmp.nome_servico_pmp,

sistema.nome_sistema,

subsistema.nome_subsistema,

ed_local.nome_local AS local_ed,

vp_via.nome_via AS via_vp,
vp_estacao_inicial.nome_estacao AS estacao_inicial_vp,
vp_estacao_final.nome_estacao AS estacao_final_vp,
vp.posicao AS posicao_vp,
vp.unidade_tag,
vp.km_inicial AS km_inicial_vp,
vp.km_final AS km_final_vp,
vp_amv.nome_amv AS amv_vp,

su_local.nome_local AS local_su,

ra_via.nome_via AS via_ra,
ra.posicao AS posicao_ra,
ra_local.nome_local AS local_ra,
ra_poste.nome_poste AS poste_ra,

tl_local.nome_local AS local_tl,

bl_estacao.nome_estacao AS estacao_bl

from pmp

join grupo using(cod_grupo)
join servico_pmp_periodicidade using(cod_servico_pmp_periodicidade)
join procedimento using (cod_procedimento)
join tipo_periodicidade using(cod_tipo_periodicidade)
join servico_pmp_sub_sistema using(cod_servico_pmp_sub_sistema)
join servico_pmp using(cod_servico_pmp)
join sub_sistema using(cod_sub_sistema)
join sistema using(cod_sistema)
join subsistema using(cod_subsistema)


left join pmp_edificacao ed using(cod_pmp)
left join pmp_via_permanente vp using(cod_pmp)
left join pmp_subestacao su using(cod_pmp)
left join pmp_rede_aerea ra using(cod_pmp)
left join pmp_telecom tl using(cod_pmp)
left join pmp_bilhetagem bl using(cod_pmp)

left join local ed_local on ed.cod_local = ed_local.cod_local

left join via vp_via on vp.cod_via = vp_via.cod_via
left join estacao vp_estacao_inicial on vp.cod_estacao_inicial = vp_estacao_inicial.cod_estacao
left join estacao vp_estacao_final on vp.cod_estacao_final = vp_estacao_final.cod_estacao
left join amv vp_amv on vp.cod_amv = vp_amv.cod_amv

left join local su_local on su.cod_local = su_local.cod_local

left join via ra_via on ra.cod_via = ra_via.cod_via
left join local ra_local on ra.cod_local = ra_local.cod_local
left join poste ra_poste on ra.cod_poste = ra_poste.cod_poste

left join local tl_local on tl.cod_local = tl_local.cod_local

left join estacao bl_estacao on bl.cod_estacao = bl_estacao.cod_estacao";

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

if(!empty($result)){
    foreach ($result as $dados => $value) {
        $txt = $value["cod_pmp"] . ":::";
        $txt .= $value["mao_obra"] . ":::";
        $txt .= $value["horas_uteis"] . ":::";
        $txt .= $value["homem_hora"] . ":::";
        $txt .= $value["turno"] . ":::";
        $txt .= $value["quinzena"] . ":::";
        $txt .= $value["ativo"] . ":::";

        $txt .= $value["nome_grupo"] . ":::";

        $txt .= $value["nome_procedimento"] . ":::";

        $txt .= $value["nome_periodicidade"] . ":::";

        $txt .= $value["nome_servico_pmp"] . ":::";

        $txt .= $value["nome_sistema"] . ":::";

        $txt .= $value["nome_subsistema"] . ":::";

        $txt .= $value["local_ed"] . ":::";

        $txt .= $value["via_vp"] . ":::";
        $txt .= $value["estacao_inicial_vp"] . ":::";
        $txt .= $value["estacao_final_vp"] . ":::";
        $txt .= $value["posicao_vp"] . ":::";
        $txt .= $value["unidade_tag"] . ":::";
        $txt .= $value["km_inicial_vp"] . ":::";
        $txt .= $value["km_final_vp"] . ":::";
        $txt .= $value["amv_vp"] . ":::";

        $txt .= $value["local_su"] . ":::";

        $txt .= $value["via_ra"] . ":::";
        $txt .= $value["posicao_ra"] . ":::";
        $txt .= $value["local_ra"] . ":::";
        $txt .= $value["poste_ra"] . ":::";

        $txt .= $value["local_tl"] . ":::";

        $txt .= $value["estacao_bl"];

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("pmpCompleto.csv",$conteudo);



header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=pmpCompleto.csv");
header("Pragma: no-cache");
header("Expires: 0");
readfile('pmpCompleto.csv');

unlink("pmpCompleto.csv");