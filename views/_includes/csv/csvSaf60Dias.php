<?php

$conteudo = "Cod SAF;";
$conteudo .= "Data de Abertura;";
$conteudo .= "Grupo Sistema;";
$conteudo .= "Local;";
$conteudo .= "Avaria;";
$conteudo .= "N�vel;";
$conteudo .= "Dias em Aberto;";
$conteudo .= "Categoria\n";

$sql = "SELECT 
            cod_saf,
            nivel,
            nome_grupo,
            nome_avaria,
            nome_linha || ' - ' || descricao_trecho AS local,
            data_abertura,
            
            (
                CASE
                    WHEN 
                        data_abertura + ('60 days') :: INTERVAL < CURRENT_TIMESTAMP
                        THEN 
                            '1'
                    WHEN 
                        data_abertura + ('50 days') :: INTERVAL < CURRENT_TIMESTAMP
                        THEN 
                            '2' 
                    WHEN 
                        data_abertura + ('30 days') :: INTERVAL < CURRENT_TIMESTAMP
                        THEN 
                            '3'
                    ELSE 
                        '4'
                END
            ) AS categoria
            
            FROM v_saf
            
            WHERE nome_status <> 'Encerrada' AND nome_status <> 'Cancelada' AND nome_status <> 'Programada' ORDER BY data_abertura";

$result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

if(!empty($result)){
    foreach ($result as $dados => $value) {
        switch ($value['categoria']){
            case '1':
                $categoria = "Acima de 60 dias";
                break;
            case '2':
                $categoria = "de 51 a 60 dias";
                break;
            case '3':
                $categoria = "de 31 a 50 dias";
                break;
            case '4':
                $categoria = "de 0 a 30 dias";
                break;
        }

        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

        $interval = date_diff(new DateTime($dataAbertura),new DateTime());

        $txt = $value["cod_saf"] . ":::";
        $txt .= $dataAbertura . ":::";
        $txt .= $value["nome_grupo"] . ":::";
        $txt .= $value["local"] . ":::";
        $txt .= $value["nome_avaria"] . ":::";
        $txt .= $value["nivel"] . ":::";
        $txt .= $interval->format('%a') . ":::";
        $txt .= $categoria;

        //Excluir quebras de linha
        $txt = str_replace("\r\n"," ",trim($txt));
        $txt = str_replace(";"," ",trim($txt));
        $txt = str_replace("<BR />"," ",trim($txt));

        $txt = str_replace(":::",";",trim($txt));

        $conteudo .= $txt ."\n";
    }
}

$this->salvaTxt("Safs60dias.csv",$conteudo);



header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=Safs60dias.csv");
header("Pragma: no-cache");
header("Expires: 0");
readfile('Safs60dias.csv');

unlink("Safs60dias.csv");