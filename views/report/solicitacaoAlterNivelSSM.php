<?php

require_once(ABSPATH . "/views/layout/topLayout.php");

$status = $this->medoo->select("status", "*", ["tipo_status"=>'S']);
$status_filter_option = $this->getOptionSelectByVariable($status, 'nome_status', 'cod_status');




echo <<<HTML


<div class="row" style="margin-top: 15px">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        HIST�RICO DE SOLICITA��ES DE ALTERA��O DE N�VEL DA SSM
      </div>
      <div class="panel-body">

        <!-- FILTROS DE CONSULTA -->
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <labe>FILTRO DE PESQUISA</labe>
              </div>
              <div class="panel-body">
                <form action="{$this->home_uri}/report/solicitacaoAlterNivelSSM" method="GET" >
                  <div class="row">
                    <div class="col-md-2">
                      <label>C�digo da SSM</label>
                      <input type="text" name="cod_ssm" class="form-control number" value="" />
                    </div>
                    <div class="col-md-1">
                      <label>N�vel De</label>
                      <select name="nivel_antigo" class="form-control">
                          <option value="">TODOS</option>
                          <option value="A">A</option>
                          <option value="B">B</option>
                          <option value="C">C</option>
                      </select>
                    </div>
                    <div class="col-md-1">
                      <label>N�vel Para</label>
                      <select name="nivel_novo" class="form-control">
                          <option value="">TODOS</option>
                          <option value="A">A</option>
                          <option value="B">B</option>
                          <option value="C">C</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <label>Status Atual</label>
                      <select type="text" name="cod_status" class="form-control">
                        <option value="">TODOS</option>
                        {$status_filter_option}
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3" style="margin-top: 15px;">
                      <button class="btn btn-default btn-lg form" type="submit">FILTRAR</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                LISTA DE SSM'S COM SOLICITA��O DE ALTERA��O DE N�VEL
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <table id="tableSSM" class="table table-bordered table-responsive table-stripped">
                        <thead>
                        <tr>
                            <th>SSM</th>
                            <th>DATA SOLICITA��O</th>
                            <th>DE</th>
                            <th>PARA</th>
                            <th>SITU��O</th>
                            <th>A��ES</th>
                        </tr>
                        </thead>
                        <tbody>
                          {$rows}
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


HTML;

require_once(ABSPATH . "/views/layout/bottonLayout.php");


?>
