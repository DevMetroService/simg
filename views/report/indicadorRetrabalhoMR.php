<?php

require_once(ABSPATH . "/views/layout/topLayout.php");

$optionGrupo = $this->medoo->select("grupo", "*", ["AND"=>["ativo"=>'S', "cod_tipo_grupo"=>3] ]);
$optionGrupo = $this->getOptionSelectByVariable($optionGrupo, 'nome_grupo', 'cod_grupo', $_GET['cod_grupo']);

$optionLinha = $this->medoo->select("linha", "*");
$optionLinha = $this->getOptionSelectByVariable($optionLinha, 'nome_linha', 'cod_linha', $_GET['cod_linha']);

if(!empty($_GET['cod_grupo']) && !empty($_GET['cod_linha']) )
{
  $optionVeiculo = $this->medoo->select("veiculo", "*", 
  [
    "AND" => [
      "cod_grupo" => $_GET['cod_grupo'], 
      "cod_linha" => $_GET['cod_linha']
    ]
  ]);
  $optionVeiculo = $this->getOptionSelectByVariable($optionVeiculo, 'nome_veiculo', 'cod_veiculo', $_GET['cod_veiculo']);

}else{

  if(!empty($_GET['cod_grupo']))
  {
    $optionVeiculo = $this->medoo->select("veiculo", "*", 
    [
      "AND" => [
        "cod_grupo" => $_GET['cod_grupo']
      ]
    ]);
    $optionVeiculo = $this->getOptionSelectByVariable($optionVeiculo, 'nome_veiculo', 'cod_veiculo', $_GET['cod_veiculo']);
  }else{
    $optionVeiculo = $this->medoo->select("veiculo", "*", 
    [
      "AND" => [
        "cod_linha" => $_GET['cod_linha']
      ]
    ]);
    $optionVeiculo = $this->getOptionSelectByVariable($optionVeiculo, 'nome_veiculo', 'cod_veiculo', $_GET['cod_veiculo']);
  }
}

if(!empty($_GET['cod_veiculo']))
{
  $optionCarro = $this->medoo->select("carro", "*", ["cod_veiculo" => $_GET['cod_veiculo']]);
  $optionCarro = $this->getOptionSelectByVariable($optionCarro, 'sigla_carro', 'cod_carro', $_GET['cod_carro'], 'nome_carro');
}

if(!empty($result))
{
  foreach($result as $dados=>$value)
  {
    $contentPopover = "";
    $sql = "SELECT
               vsaf.cod_saf, vsaf.nome_status, vsaf.data_abertura
             FROM
               v_saf vsaf
             WHERE
               vsaf.cod_carro = {$value['cod_carro']}
               AND vsaf.cod_avaria = {$value['cod_avaria']}
               {$whereData}
             ORDER BY vsaf.cod_saf";

    $saf = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $ctdSaf = count($saf);

    foreach($saf as $valor)
    {
        $dataAbertura = $this->parse_timestamp($valor['data_abertura']);
        $contentPopover .= "<tr>
        <td>
          <a class='btnPrintSaf'>{$valor['cod_saf']}</a>
        </td>
        <td>
          {$valor['nome_status']}
        </td>
        <td>
          {$dataAbertura}
        </td>
        </tr>";
    }

    $table = "
    <table class='table-bordered table-stripped'>
      <thead>
        <tr>
          <th>
            SAF
          </th>
          <th>
          STATUS
          </th>
          <th>
          DATA ABERTURA
          </th>
        </tr>
      </thead>
      <tbody>
        {$contentPopover}
      </tbody>
    </table>";

     $rows .= <<<HTML
     <tr>
     <td>
     {$value["nome_avaria"]}
     </td>
     <td>
     {$value["nome_veiculo"]}
     </td>
     <td>
     {$value["nome_carro"]}
     </td>
     <td class="text-center">
         <button tabindex="0" class="btn btn-block btn-default clickPop" role="button"
         data-toggle="popover"
         data-placement="top"
         data-trigger="focus"
         title="SAFs"
         data-content="{$table}">
             {$ctdSaf}
         </button>
     </td>
     </tr>
HTML;
  }
  $buttonPrint = '<div class="col-md-2 col-md-offset-7" style="margin-top: 15px;">
  <button id="btnPrint" class="btn btn-primary btn-lg btn-block" type="button"><i class="fa fa-print fa-fw"></i>IMPRIMIR</button>
</div>';
}
$helpInfo = "
<p>Este relat�rio � um indicador que auxilia a identifica��o de SAF's abertas com as seguintes informa��es id�nticas:</p>

<ul class='list-group list-group-flush'>
  <li class='list-group-item'>Grupo de Sistema</li>
  <li class='list-group-item'>Linha</li>
  <li class='list-group-item'>Ve�culo</li>
  <li class='list-group-item'>Carro</li>
  <li class='list-group-item'>Avaria</li>
</ul>
  
  <p>Caso n�o seja utilizado o filtro de data, o relat�rio retornar� as SAF's que nos �ltimos 30 dias entraram no crit�rio citado.</p>
";
echo <<<HTML

<div class="row" style="margin-top: 15px">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        INDICADOR DE SAF's COM RETRABALHO
        <button data-toggle='popover' title='AJUDA' data-placement="right" class="btn btn-circle btn-default" data-trigger="focus"
                  data-content="{$helpInfo}">
                  <i id="questionMaterial" class="fa fa-question fa-lg"></i>
                </button>
      </div>
      <div class="panel-body">

        <!-- FILTROS DE CONSULTA -->
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                FILTRO DE PESQUISA
              </div>
              <div class="panel-body">
                <form id="reworkForm" action="{$this->home_uri}/report/indicadorRetrabalhoMR" method="GET" >
                  <div class="row">
                    <div class="col-md-2">
                      <label>Grupo de Sistemas</label>
                      <select id="grupo" name="cod_grupo" class="form-control">
                        <option value="">TODOS</option>
                        {$optionGrupo}
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Linha</label>
                      <select id="linha" name="cod_linha" class="form-control">
                        <option value="">TODOS</option>
                        {$optionLinha}
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Veiculo</label>
                      <select id="veiculo" name="cod_veiculo" class="form-control">
                        <option value="">TODOS</option>
                        {$optionVeiculo}
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Carro</label>
                      <select id="carro" name="cod_carro" class="form-control">
                        <option value="">TODOS</option>
                        {$optionCarro}
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <label>Data a Partir</label>
                      <input class="form-control data" type="text" name="dataPartir" value="{$dataPartir}"/>
                    </div>
                    <div class="col-md-2">
                      <label>Data At�</label>
                      <input class="form-control data" type="text" name="dataAte" value="{$dataAte}"/>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3" style="margin-top: 15px;">
                      <button class="btn btn-default btn-lg form" type="submit">FILTRAR</button>
                    </div>
                    {$buttonPrint}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                LISTA DE SAF's {$title}
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <table id="tableSSM" class="table table-bordered table-responsive table-stripped">
                        <thead>
                        <tr>
                            <th>AVARIA</th>
                            <th>VE�CULO</th>
                            <th>CARRO</th>
                            <th>QTD SAF'S</th>
                        </tr>
                        </thead>
                        <tbody>
                          {$rows}
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

HTML;



require_once(ABSPATH . "/views/layout/bottonLayout.php");

?>
