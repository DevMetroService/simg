<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Cadastro de Fornecedor</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">

						<form class="form-group" method="post" action="<?= $this->home_uri ?>/fornecedor/store" role="form">
							<div class="row">
								<div class="col-md-12">
									<label>Nome Fantasia</label>
									<input type="text" class="form-control"
                                           value=""
                                           name="nome_fantasia"
                                    >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="razaoSocial">Raz�o Social</label>
									<input type="text" class="form-control" value="" name="razao_social">
								</div>
							</div>

							<div class="row">
	              <div class="col-md-3">
	                  <label for="cnpjFornecedor">CNPJ</label>
	                  <input type="text" class="form-control cnpj"
	                         value=""
	                         name="cpfcnpj"
	                  >
	              </div>
	              <div class="col-md-3">
	                  <label>Optante pelo Simples:</label>
	                  <select class="form-control" name="optante_simples" required>
	                      <option value="" selected="selected" disabled="disabled">Selecione uma op��o</option>
	                      <option value="n">N�o</option>
	                      <option value="s">Sim</option>
	                  </select>
	              </div>
	              <div class="col-md-3">
	                  <label>Inscri��o Municipal</label>
	                  <input type="text" class="form-control" name="insc_municipal"
	                         value="">
	              </div>
	              <div class="col-md-3">
	                  <label>Inscri��o Estadual</label>
	                  <input type="text" class="form-control" name="insc_estadual"
	                         value="">
	              </div>
							</div>

                          <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                            </div>

                            <div class="row" style="margin-top: 2em">
                                <div class="col-md-2">
                                    <label for="cepFornecedorMaterial">CEP</label>
                                    <input class="form-control cep" name="cep" type="text"
                                           value="">
                                </div>

                                <div class="col-md-10">
                                    <label for="logradouroEnderecoFornecedor">Endere�o</label>
                                    <input type="text" id="logradouro" name="logradouro"
                                           value="" class="form-control" >
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <label for="numeroEnderecoFornecedor">N�mero</label>
                                    <input type="text" name="numero_endereco" id="numero" class="form-control"
                                           value="">
                                </div>
                                <div class="col-md-4">
                                    <label for="complementoEnderecoFornecedor">Complemento</label>
                                    <input type="text" name="complemento" class="form-control"
                                           value="">
                                </div>

                                <div class="col-md-2">
                                    <label for="bairroEnderecoFornecedor">Bairro</label>
                                    <input type="text" id="bairro" name="bairro" class="form-control"
                                           value="">
                                </div>
                                <div class="col-md-2">
                                    <label for="municipioEnderecoFornecedor">Munic�pio</label>
                                    <input type="text" id="cidade" name="municipio"
                                           class="form-control"
                                           value=""
                                    >
                                </div>
                                <div class="col-md-2">
                                    <label for="ufEnderecoFornecedor">UF</label>
                                    <input type="text" class="form-control" name="uf" id="uf"
                                           value="">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                            </div>

                            <div class="row" style="margin-top: 2em">
                                <div class="col-md-3">
                                    <label for="nomeContatoFornecedor">Nome do Contato</label>
                                    <input class="form-control" name="nome_contato" type="text"
                                           value="">
                                </div>
                                <div class="col-md-3">
                                    <label for="telefoneContatoFornecedor">Telefone</label>
                                    <input class="form-control telefone" name="fone_fornecedor" type="text"
                                           value="">
                                </div>
                                <div class="col-md-3">
                                    <label for="emailContatoFornecedor">E-mail</label>
                                    <input class="form-control" name="email_fornecedor" type="text"
                                        value="">
                                </div>
                                <div class="col-md-3">
                                    <label for="siteFornecedor">Site</label>
                                    <input class="form-control" name="site" type="text"
                                        value="">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                            </div>

                            <div class="row" style="margin-top: 2em">
                                <div class="col-md-3">
                                    <label for="agenciaBancoFornecedor">Ag�ncia</label>
                                    <input type="text" class="form-control" name="agencia"
                                           value="">
                                </div>
                                <div class="col-md-3">
                                    <label for="ccBancoFornecedor">Conta Corrente</label>
                                    <input type="text" class="form-control" name="conta_corrente"
                                           value="">
                                </div>
                                <div class="col-md-3">
                                    <label for="nomeBancoFornecedor">Banco</label>
                                    <input type="text" class="form-control" name="banco" value="">
                                </div>
                                <div class="col-md-3">
                                    <label for="numeroBancoFornecedor">N�mero do Banco</label>
                                    <input type="text" class="form-control" name="num_transferencia" value="">
                                </div>
                            </div>

                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                            </div>

                            <div class="row" style="margin-top: 2em">
                                <div class="col-md-4">
                                    <label>Avali��o</label>
                                    <input type="text" name="avaliacao" class="form-control"
                                           value=""
                                    >
                                </div>
                                <div class="col-md-4">
                                    <label>Ramo de Atividade</label>
                                    </br><select name="equipeUsuario[]" class="form-control" id="multiSelectCategoria"
                                            multiple="multiple">
                                        <optgroup label="Selecionar Todos">
                                            <?php
                                            $selectCategoria = $this->medoo->select("categoria_material", "*");

                                            foreach($selectCategoria as $dados=>$value){
                                                echo ("<option value='{$value['cod_categoria']}'>{$value['nome_categoria']}</option>");
                                            }
                                            ?>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Ramos</label>
                                    <div id="ramoAtividade"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 2em">
								<div class="col-md-12">
									<label>Informa��es Adicionais</label>
									<textarea rows="2" class="form-control" name="descricaoFornecedor"></textarea>
								</div>
							</div>

							<div class="row" style="margin-top: 2em">
								<div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-lg">Cadastrar Fornecedor</button>
								</div>
								<div class="col-md-4">
									<button type="reset" class="btn btn-default btn-lg">Zerar Campos</button>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>

		</div>
	</div>
</div>
