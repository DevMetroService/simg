<?php

$tableLinha;
foreach ($linha as $dados => $value) {
    $tableLinha .= "<tr>";
    $tableLinha .= "<td>{$value['cod_linha']}</td>";
    $tableLinha .= "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
    $tableLinha .= "<td><button type='button' title='Editar Linha' data-codigo='{$value['cod_linha']}' class='btn btn-default btn-circle editLinha'><i class='fa fa-file-o fa-fw'></i></button>";
    $tableLinha .= "</tr>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de Linha</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Dados de Linhas</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/linha/store" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Nome da Linha</label>                            
                            <input id="linha" type="text" class="form-control" name="nome_linha" required />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de Linhas</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="amvTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Linha</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tableLinha}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
