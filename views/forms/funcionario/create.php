<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 27/08/2020
 * Time: 16:04
 */
?>

<div class="page-header">
    <h2>Cadastro de Funcionario</h2>
</div>

<div class="row">
    <form id="formFuncionario" method="post" action="<?php echo HOME_URI ?>/funcionario/store" class="form-group">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Cadastro de Funcionario</label></div>

                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-5 col-lg-2" >
                                <label>Empresa*</label>
                                <select name="cod_tipo_funcionario" class="form-control" >
                                    <?php
                                    foreach ($dadosFormulario['tipo_funcionario'] as $dados => $value) {
                                        echo("<option value='{$value["cod_tipo_funcionario"]}'>{$value["descricao"]}</option>");
                                    }

                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Matricula*</label>
                                <input name='matricula' required class='form-control number'/>
                            </div>

                            <div class="col-md-6">
                                <label>Nome Completo*</label>
                                <input type="text" name="nome_funcionario" required class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label>Data de Nascimento*</label>
                                <input type="text" class="form-control data" name="data_nasc" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2 col-lg-3">
                                <label>Fun��o*</label>
                                <select name="cod_cargos" required class="form-control">
                                    <option disabled selected>Selecione a Fun��o</option>
                                    <?php
                                    foreach ($dadosFormulario['cargos'] as $dados => $value)
                                    {
                                        echo('<option value="' . $value['cod_cargos'] . '">' . $value['descricao'] . '</option>');
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-3">

                                <label>Escolaridade*</label>
                                <select class="form-control" name="cod_tipo_escolaridade" required>
                                    <?php
                                    foreach ($dadosFormulario['escolaridade'] as $dados => $value)
                                    {
                                        echo ("<option value='{$value["cod_tipo_escolaridade"]}'>{$value['descricao']}</option>");
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label>Em andamento</label>
                                <input type="checkbox" class="form-control" name="ef_andamento">
                            </div>
                            <div class="col-md-2">
                                <label>Curso t�cnico</label>
                                <input type="checkbox" class="form-control" name="ef_curso_tecnico">
                            </div>
                            <div class="col-md-3 hidden" id="divNomeCursoTecnico">
                                <label>Nome do Curso T�cnico</label>
                                <input type="text" class="form-control" name="ef_nome_curso_tecnico">
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-3">
                                <label>CPF*</label>
                                <input id="cpf_cnpj" type="text" name="cpf_cnpj" required class="form-control cpfpj">
                            </div>

                            <div class="col-md-4 centro_lotacao"  >
                                <label>Centro de Resultado*</label>
                                <select class="form-control " required name="cod_centro_resultado">
                                    <option disabled selected>Selecione a Ger�ncia</option>
                                    <?php
                                    foreach ($dadosFormulario['centro_custo'] as $dados => $value)
                                    {
                                        echo('<option value="' . $value['cod_centro_resultado'] . '">' . $value['num_centro_resultado'] . " - " . $value['descricao'] . '</option>');
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-5 centro_lotacao"  >
                                <label>Lota��o*</label>
                                <select class="form-control" required name="cod_unidade">
                                    <option disabled selected> Selecione a Lota��o</option>
                                    <?php
                                    foreach ($dadosFormulario['unidade'] as $dados => $value)
                                    {
                                        echo('<option value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <label>E-mail Corporativo ou Principal</label>
                                <input type="email" name="cf_email" class="form-control">
                            </div>

                            <div class="col-md-3">
                                <label>E-mail Pessoal</label>
                                <input type="email" name="email_pessoal" class="form-control">
                            </div>

                            <div class="col-md-3">
                                <label>Telefone Corporativo ou Principal*</label>
                                <input type="text" name="celular" required class="form-control telefone">
                            </div>

                            <div class="col-md-3">
                                <label>Telefone Pessoal</label>
                                <input type="text" name="cf_fone_res" class="form-control telefone">
                            </div>
                        </div>

                        <div class="row hidden-print" style="padding-top: 2em">
                            <div class="col-md-3">
                                <button class="btn btn-success btn-lg btn-block btnSalvarContinuar" type="submit">
                                    <i class="fa fa-edit fa-fw"></i> Cadastrar
                                </button>
                            </div>

                            <div class="col-md-3">
                                <button class="btn btn-primary btn-lg btn-block" onclick="window.print()"><i class="fa fa-print"></i> Imprimir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
