<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 27/08/2020
 * Time: 17:08
 */
?>

<div class="page-header">
    <h2>Editar Funcionario</h2>
</div>

<div class="row">
    <form id="formFuncionario" method="post" action="<?= "{$this->home_uri}/funcionario/update/{$dadosFuncionario['cod_funcionario']}"?>" class="form-group">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><label>Cadastro de Funcionario</label></div>

                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-5 col-lg-2" >
                                    <label>Tipo Funcion�rio</label>
                                    <select name="cod_tipo_funcionario" class="form-control" >
                                        <?php
                                        foreach ($dadosFormularios['tipo_funcionario'] as $dados => $value) {
                                            if ($dadosFuncEmpresa['cod_tipo_funcionario'] == $value["cod_tipo_funcionario"]) {
                                                echo("<option value='{$value["cod_tipo_funcionario"]}' selected >{$value["descricao"]}</option>");
                                            } else {
                                                echo("<option value='{$value["cod_tipo_funcionario"]}'>{$value["descricao"]}</option>");
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-1">
                                    <label>Matricula</label>
                                    <?php
                                        echo "<input name='matricula' required value='{$dadosFuncionario['matriculaF']}' class='form-control number'/>";
                                    ?>

                                </div>

                                <div class="col-md-5">
                                    <label>Nome Completo</label>
                                    <input type="text" name="nome_funcionario" value="<?php echo $dadosFuncionario['nome_funcionario']?>" required class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Data de Nascimento</label>
                                    <?php
                                    $dataNasci = $this->parse_timestamp($dadosFuncionario['data_nasc'], 'd-m-Y');
                                    echo "<input type='text' name='data_nasc' class='form-control data' placeholder='{$dataNasci}' value='{$dataNasci}' name='dataNascimento' required>"
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <label>Ativo</label>
                                    <input type="checkbox" class="form-control" <?php if($dadosFuncionario['ativo'] == 's'){ echo("checked"); $displayCt = "block"; }else{$displayCt = "none";}?> name="ativo">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2 col-lg-3">
                                    <label>Fun��o</label>
                                    <select name="cod_cargos" required class="form-control">
                                        <option disabled selected>Selecione a Fun��o</option>
                                        <?php
                                        foreach ($dadosFormularios['cargos'] as $dados => $value) {
                                            if($dadosEmpresa['cod_cargos'] == $value['cod_cargos'])
                                                echo('<option value="' . $value['cod_cargos'] . '" selected>' . $value['descricao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_cargos'] . '">' . $value['descricao'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label>Escolaridade</label>
                                    <select class="form-control" name="cod_tipo_escolaridade" required>
                                        <?php
                                        foreach ($dadosFormularios['escolaridade'] as $dados => $value){
                                            if($dadosEscolaridade['cod_tipo_escolaridade'] == $value['cod_tipo_escolaridade'])
                                                echo ("<option value='{$value["cod_tipo_escolaridade"]}' selected>{$value['descricao']}</option>");
                                            else{
                                                echo ("<option value='{$value["cod_tipo_escolaridade"]}'>{$value['descricao']}</option>");
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>Em andamento</label>
                                    <input type="checkbox" class="form-control" <?php if($dadosEscolaridade['ef_andamento'] == 's') echo("checked"); ?> name="ef_andamento">
                                </div>
                                <div class="col-md-2">
                                    <label>Curso t�cnico</label>
                                    <input type="checkbox" class="form-control" <?php if($dadosEscolaridade['ef_curso_tecnico'] == 's'){ echo("checked"); $displayCt = "block"; }else{$displayCt = "none";}?> name="ef_curso_tecnico">
                                </div>
                                <div class="col-md-3" id="divNomeCursoTecnico" style = "display: <?php echo $displayCt; ?>;">
                                    <label>Nome do Curso T�cnico</label>
                                    <input type="text" class="form-control" value="<?php echo $dadosEscolaridade['ef_nome_curso_tecnico']?>" name="ef_nome_curso_tecnico">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>CPF</label>
                                    <input type="text" name="cpd_cnpj" required class="form-control cpfpj" value="<?php echo $dadosFuncionario['cpf_cnpj']?>">
                                </div>

                                <div class="col-md-4 centro_lotacao"  >
                                    <label>Centro de Resultado</label>
                                    <select class="form-control " required name="cod_centro_resultado">
                                        <option disabled selected>Selecione a Ger�ncia</option>
                                        <?php
                                        foreach ($dadosFormularios['centro_custo'] as $dados => $value) {
                                            if($dadosEmpresa['cod_centro_resultado'] == $value['cod_centro_resultado'])
                                                echo('<option value="' . $value['cod_centro_resultado'] . '" selected>' . $value['num_centro_resultado'] . " - " . $value['descricao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_centro_resultado'] . '">' . $value['num_centro_resultado'] . " - " . $value['descricao'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-5 centro_lotacao"  >
                                    <label>Lota��o</label>
                                    <select class="form-control" required name="cod_unidade">
                                        <option disabled selected> Selecione a Lota��o</option>
                                        <?php
                                        foreach ($dadosFormularios['unidade'] as $dados => $value) {
                                            if($dadosEmpresa['cod_unidade'] == $value['cod_unidade'])
                                                echo('<option value="' . $value['cod_unidade'] . '" selected>' . $value['nome_unidade'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>E-mail Corporativo ou Principal</label>
                                    <input type="email" name="cf_email" class="form-control" value="<?php echo $dadosContato['cf_email']?>">
                                </div>

                                <div class="col-md-3">
                                    <label>E-mail Pessoal</label>
                                    <input type="email" name="email_pessoal" class="form-control" value="<?php echo $dadosContato['email_pessoal']?>">
                                </div>

                                <div class="col-md-3">
                                    <label>Telefone Corporativo ou Principal</label>
                                    <input type="text" name="celular" required class="form-control telefone" value="<?php echo $dadosContato['celular']?>">
                                </div>

                                <div class="col-md-3">
                                    <label>Telefone Pessoal</label>
                                    <input type="text" name="cf_fone_res" class="form-control telefone" value="<?php echo $dadosContato['cf_fone_res']?>">
                                </div>
                            </div>

                            <div class="row hidden-print" style="padding-top: 2em">
                                <div class="col-md-3">
                                    <button class="btn btn-success btn-lg btn-block btnSalvarContinuar" type="submit">
                                        <i class="fa fa-edit fa-fw"></i>
                                            Atualizar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
