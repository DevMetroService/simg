<?php

switch($saf['nivel'])
{
    case 'A':
        $selectedA= "selected";
        break;
    case 'B':
        $selectedB= "selected";
        break;
    case 'C':
        $selectedC= "selected";
        break;
}

$tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);
$optionTipoFuncionario;
foreach ($tipo_funcionario as $dados => $value) {
    if($value['cod_tipo_funcionario'] == $saf['cod_tipo_funcionario'])
        $optionTipoFuncionario .= "<option value='{$value["cod_tipo_funcionario"]}' selected>{$value["descricao"]}</option>";
    else
        $optionTipoFuncionario .= "<option value='{$value["cod_tipo_funcionario"]}'>{$value["descricao"]}</option>";
}

$linha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);
$optionLinha;
foreach ($linha as $dados => $value) {
    if($value['cod_linha'] == $saf['cod_linha'])
        $optionLinha .= "<option value='{$value['cod_linha']}' selected>{$value['nome_linha']}</option>";
    else
        $optionLinha .= "<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>";
}

$trecho = $this->medoo->select('trecho', "*", ["cod_linha" => $saf['cod_linha']]);
$optionTrecho;
foreach ($trecho as $dados => $value) {
    if($value['cod_trecho'] == $saf['cod_trecho'])
        $optionTrecho .= "<option value='{$value['cod_trecho']}' selected>{$value['nome_trecho']} - {$value['descricao_trecho']}</option>";
    else
        $optionTrecho .= "<option value='{$value['cod_trecho']}'>{$value['nome_trecho']} - {$value['descricao_trecho']}</option>";
}

$pontoNotavel = $this->medoo->select('ponto_notavel', "*", ["cod_trecho" => $saf['cod_trecho']]);
$optionPontoNotavel;
foreach ($pontoNotavel as $dados => $value) {
    if($value['cod_ponto_notavel'] == $saf['cod_ponto_notavel'])
        $optionPontoNotavel .= "<option value='{$value['cod_ponto_notavel']}' selected>{$value['nome_ponto']}</option>";
    else
        $optionPontoNotavel .= "<option value='{$value['cod_ponto_notavel']}'>{$value['nome_ponto']}</option>";
}

$via = $this->medoo->select("via", "*", ["cod_linha" => $saf['cod_linha']]);
$optionVia;
foreach ($via as $dados => $value) {
    if($value['cod_via'] == $saf['cod_via'])
        $optionVia .= "<option value='{$value['cod_via']}' selected>{$value['nome_via']}</option>";
    else
        $optionVia .= "<option value='{$value['cod_via']}'>{$value['nome_via']}</option>";
}

$grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S', "ORDER" => "nome_grupo"]);
$optionGrupo;
foreach($grupo as $dados=>$value){
    if($value['cod_grupo'] == $saf['cod_grupo'])
        $optionGrupo .= "<option value='{$value['cod_grupo']}' selected>{$value['nome_grupo']}</option>";
    else
        $optionGrupo .= "<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>";
}

$sistema = $this->medoo->select("grupo_sistema", ["[><]sistema"=>"cod_sistema"],['cod_sistema', 'nome_sistema'], ["cod_grupo" => $saf['cod_grupo']]);
$optionSistema;
foreach($sistema as $dados=>$value){
    if($value['cod_sistema'] == $saf['cod_sistema'])
        $optionSistema .= "<option value='{$value['cod_sistema']}' selected>{$value['nome_sistema']}</option>";
    else
        $optionSistema .= "<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>";
}

$subsistema = $this->medoo->select("sub_sistema", ["[><]subsistema"=>"cod_subsistema"],['cod_subsistema', 'nome_subsistema'], ["cod_sistema" => $saf['cod_sistema']]);
$optionSubsistema;
foreach($subsistema as $dados=>$value){
    if($value['cod_sistema'] == $saf['cod_sistema'])
        $optionSubsistema .= "<option value='{$value['cod_subsistema']}' selected>{$value['nome_subsistema']}</option>";
    else
        $optionSubsistema .= "<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>";
}

$local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
$optionLocal;
foreach($local as $dados=>$value){
    if($value['cod_local_grupo'] == $saf['cod_local_grupo'])
        $optionLocal .= "<option value='{$value['cod_local_grupo']}' selected>{$value['nome_local_grupo']}</option>";
    else
        $optionLocal .= "<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>";
}

$sublocal  = $this->medoo->select('sublocal_grupo', '*', ['ORDER' => 'nome_sublocal_grupo']);
$optionSubLocal;
foreach($sublocal as $dados=>$value){
    if($value['cod_sublocal_grupo'] == $saf['cod_sublocal_grupo'])
        $optionSubLocal .= "<option value='{$value['cod_sublocal_grupo']}' selected>{$value['nome_sublocal_grupo']}</option>";
    else
        $optionSubLocal .= "<option value='{$value['cod_sublocal_grupo']}'>{$value['nome_sublocal_grupo']}</option>";
}

$avariaDataList= $this->medoo->select('avaria', '*', ['cod_grupo' => $saf['cod_grupo']]);
$optionDataListAvaria;
foreach($avariaDataList as $dados=>$value){
    $optionDataListAvaria .= "<option id='{$value['cod_avaria']}' value='{$value['nome_avaria']}'></option>";
}
$avaria = $this->medoo->select('avaria', '*', ['cod_avaria' => $saf['cod_avaria']])[0];

if(!$btnBlock)
    $btnOpen = '<div class="btn-group">
    <button type="submit" class="btn btn-success btn-lg" aria-label="right align" title="Aprovar">
        <i class="fa fa-arrow-right fa-2x"></i>
    </button>
    </div>
    <div class="btn-group">
        <button type="button" class="btn btn-danger btn-lg" aria-label="right align" title="Cancelar"
            data-toggle="modal" data-target=".modalCancelSaf">
            <i class="fa fa-times fa-2x"></i>
        </button>
    </div>';


echo <<<HTML

<div class="page-header">
    <h1>SAF - Solicita��o de Abertura de Falha</h1>
</div>

<div class="row">
    <form id="formSaf" class="form-group" action="{$this->home_uri}/saf/update/{$saf['cod_saf']}" method="post">
        <input type="hidden" name="altSafGerSsm" value="yes" />

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>SAF</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Solicitante</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Origem</label>
                                            <select id="codTipoFuncionario" name="cod_tipo_funcionario" class="form-control" required>
                                              <option value="">Selecione a op��o</option>
                                                {$optionTipoFuncionario}
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-xs-8">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Preenchimento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="responsavelSaf">Respons�vel</label>
                                            <input name="responsavelSaf" class="form-control" type="text" readonly value="{$saf['usuario']}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>SAF</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8 col-xs-8">
                                            <label for="dataSAF">Data</label>
                                            <input class="form-control" id="dataSaf" disabled type="datetime"
                                                   value="{$saf['data_abertura']}"/>

                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <label for="codigoSAF">N�</label>
                                            <input type="text" name="cod_saf" class="form-control" readonly value="{$saf['cod_saf']}"/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='panel panel-default'>
                                <div class='panel-heading'><label>Situa��o da Solicita��o</label></div>

                                <div class='panel-body'>
                                    <div class='row'>
                                        <div class='col-md-2'>
                                            <label for='nomeStatusSaf'>Status</label>
                                            <input id='nomeStatusSaf' type='text' class='form-control' disabled
                                                    value='Devolvida'/>
                                        </div>
                                        <div class='col-md-10'>
                                            <label for='descricaoStatusSaf'>Descri��o</label>
                                                <textarea id='descricaoStatusSaf' class='form-control' disabled
                                                            spellcheck='true'>{$saf['descricao_status']}</textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o do Solicitante</label></div>

                                <div class="panel-body">
                                    <div class="col-md-2 col-xs-2" id="matriculaOrigemSaf">
                                        <label>Matr�cula</label>
                                        <input id="matriculaSolicitante" type="text" name="matricula" class="form-control number" value="{$saf['matricula']}">
                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Nome Completo</label>
                                        <input id="nomeSolicitante" type="text" name="nome" class="form-control" required
                                               list="nomeSolicitanteDataList" value="{$saf['nome']}" autocomplete="off"/>

                                        <datalist id="nomeSolicitanteDataList">
                                        </datalist>

                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <label>CPF</label>
                                        <input type="text" id="cpfSolicitante" name="cpf" class="form-control cpf" required value="{$saf['cpf']}"/>

                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <label>Contato</label>
                                        <input type="text" id="contatoSolicitante" name="contato" class="form-control" required value="{$saf['contato']}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o / Local</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="linha">Linha</label>
                                            <select id="linha" name="cod_linha" class="form-control" required>
                                                <option disabled selected></option>
                                                {$optionLinha}
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="trechoSaf">Sigla / Trecho </label>
                                            <select id="trechoSaf" class="form-control" name="cod_trecho" required>
                                                {$optionTrecho}
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="pontoNotavel">Ponto Not�vel</label>
                                            <select id="pontoNotavel" name="cod_ponto_notavel" class="form-control">
                                                <option value="" disabled selected>Ponto Not�vel</option>
                                                {$optionPontoNotavel}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="complementoLocal">Complemento Local</label>
                                            <input id="complementoLocal" type="text" class="form-control" name="complemento_local" value="{$saf['complemento_local']}"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <label for="via">Via </label>
                                            <select name="via" class="form-control">
                                                <option value="" disabled selected>Via</option>
                                                {$optionVia}
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="posicaoLocal">Posi��o</label>
                                            <input id="posicaoLocal" type="text" class="form-control" name="posicao" value="{$saf['posicao']}"/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="kmInicial">Km Inicial </label>
                                            <input id="kmInicial" name="km_inicial" class="form-control" value="{$saf['km_inicial']}"/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="kmFinal">Km Final </label>
                                            <input id="kmFinal" name="km_final" class="form-control" value="{$saf['km_final']}"/>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-10">
                                            <label for="cod_grupo">Grupo Sistema</label>
                                            <select id="gSistema" name="cod_grupo" class="form-control">
                                                <option value="" disabled selected>Selecione o Grupo</option>
                                                {$optionGrupo}
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label for="sistema">Sistema </label>
                                            <select id="sistema" name="cod_sistema" class="form-control" required="required">
                                            {$optionSistema}
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sub-Sistema</label>
                                            <select id="subSistemaSaf" class="form-control" name="cod_subsistema" required>
                                            {$optionSubsistema}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row" id="divMaterialRodante" style="display: {$showMR}">
                                        <div class="col-md-2">
                                            <label>C�digo Ve�culo</label>
                                            <select id="veiculoMrSaf" class="form-control" name="cod_veiculo">
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Carro Avariado</label>
                                            <select id="carroMrSaf" name="cod_carro" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Od�metro</label>
                                            <input id="odometroMrSaf" class="form-control number" type="text" name="odometro">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Pref�xo</label>
                                            <select id="prefixoMrSaf" type="text" name="cod_prefixo" class="form-control">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row" id="sublocal" style="display: {$showBlTl}">
                                        <div class="col-md-4">
                                            <label>Local</label>
                                            <select id="localGrupo" class="form-control" name="cod_local_grupo">
                                                <option value='' disabled selected>Selecione o Local</option>
                                                {$optionLocal}
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Sub-Local</label>
                                            <select name="cod_sublocal_grupo" id="subLocalGrupo" class="form-control">
                                            {$optionSubLocal}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Avaria</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div style="display: none;">
                                            <input id="avaria" name="cod_avaria" readonly required value="{$avaria['cod_avaria']}" class="form-control"/>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <label for="avariaSaf">Op��o de Avaria</label>
                                            <input id="avariaSaf" class="form-control"
                                                   placeholder="Selecione uma Avaria"
                                                   value="{$avaria['nome_avaria']}" required list="avariaSafDataList" autocomplete="off">

                                            <datalist id="avariaSafDataList">
                                                {$optionDataListAvaria}
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="complementoAvaria">Descri��o</label>
                                                    <textarea id="complementoAvaria" name="complemento_falha"
                                                              class="form-control" rows="3" spellcheck="true">{$saf['complemento_falha']}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="nivel">N�vel</label>
                                                    <a data-toggle="modal" data-target="#modalHelpNivel"
                                                       class="btn-circle hidden-print"><i
                                                                style="font-size: 20px"
                                                                class="fa fa-question-circle fa-fw"></i></a>
                                                    <select id="nivel" name="nivel" class="form-control">
                                                    <option title="Necess�rio: N�o afeta diretamente a circula��o."
                                                                value="C" {$selectedC}>
                                                            C
                                                        </option>
                                                        <option title="Emergencial: Atrapalha a circula��o."
                                                                value="B" {$selectedB}>
                                                            B
                                                        </option>
                                                        <option data-toggle="tooltip"
                                                                title="Urgente: Interrompe a circula��o."
                                                                value="A" {$selectedA}>
                                                            A
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <div class="col-md-offset-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">A��es</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group  btn-group-justified " role="group">
                                                {$btnOpen}
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-lg btnImprimirFormulario" title="Imprimir" aria-label="right align">
                                                        <i class="fa fa-print fa-2x"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary btn-lg btnHistorico"
                                                        data-toggle="modal" data-target=".modal-historico" title="Ver Hist�rico do Formul�rio">
                                                        <i class="fa fas fa-list-ul fa-2x"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
HTML;


require_once(ABSPATH . "/views/widgets/modalHistorico.php");
require_once(ABSPATH . "/views/widgets/modalCancelSaf.php");
?>