<?php

echo <<<HTML

<div class="page-header">
    <h1>SAF - Solicita��o de Abertura de Falha</h1>
</div>

<div class="row">
    <form id="formCancel" class="form-group" action="{$this->home_uri}/saf/cancel" method="post">

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>Cancel SAF</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4 hidden-print">
                            <div class="row" style="margin-top: 5%">
                                <label>C�digo SAF</label>
                                <div class="input-group">
                                    <input id="codSaf" required type="text" class="form-control number" name="cod_saf">
                                    <span class="input-group-btn">
                                        <button id="cancelBtn" type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Cancelar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
HTML;

?>