<?php

$tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);
$optionTipoFuncionario;
foreach ($tipo_funcionario as $dados => $value) {
    $optionTipoFuncionario .= "<option value='{$value["cod_tipo_funcionario"]}'>{$value["descricao"]}</option>";
}

$linha = $this->medoo->select('linha', ['nome_linha', 'cod_linha']);
$optionLinha;
foreach ($linha as $dados => $value) {
    $optionLinha .= "<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>";
}

$local  = $this->medoo->select('local_grupo', '*', ['ORDER' => 'nome_local_grupo']);
$optionLocal;
foreach($local as $dados=>$value){
    $optionLocal .= "<option value='{$value['cod_local_grupo']}'>{$value['nome_local_grupo']}</option>";
}

$grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ativo" => 'S', "ORDER" => "nome_grupo"]);
$optionGrupo;
foreach($grupo as $dados=>$value){
    $optionGrupo .= "<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>";
}

echo <<<HTML

<div class="page-header">
    <h1>SAF - Solicita��o de Abertura de Falha</h1>
</div>

<div class="row">
    <form id="formSaf" class="form-group" action="{$this->home_uri}/saf/store" method="post">
        <input type="hidden" name="altSafGerSsm" value="yes" />

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>SAF</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Solicitante</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Origem</label>
                                            <select id="codTipoFuncionario" name="cod_tipo_funcionario" class="form-control" required>
                                              <option value="">Selecione a op��o</option>
                                                {$optionTipoFuncionario}
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-xs-8">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Preenchimento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="responsavelSaf">Respons�vel</label>
                                            <input name="responsavelSaf" class="form-control" type="text" readonly value="{$this->dadosUsuario['usuario']}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>SAF</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-8 col-xs-8">
                                            <label for="dataSAF">Data</label>
                                            <input class="form-control" id="dataSaf" disabled type="datetime"
                                                   value=""/>

                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <label for="codigoSAF">N�</label>
                                            <input type="text" name="codigoSaf" class="form-control" readonly value=""/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o do Solicitante</label></div>

                                <div class="panel-body">
                                    <div class="col-md-2 col-xs-2" id="matriculaOrigemSaf">
                                        <label>Matr�cula</label>
                                        <input id="matriculaSolicitante" type="text" name="matricula" class="form-control number" value="">
                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Nome Completo</label>
                                        <input id="nomeSolicitante" type="text" name="nome" class="form-control" required
                                               list="nomeSolicitanteDataList" value="" autocomplete="off"/>

                                        <datalist id="nomeSolicitanteDataList">
                                        </datalist>

                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <label>CPF</label>
                                        <input type="text" id="cpfSolicitante" name="cpf" class="form-control cpf" required value=""/>

                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <label>Contato</label>
                                        <input type="text" id="contatoSolicitante" name="contato" class="form-control" required value=""/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o / Local</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="linha">Linha</label>
                                            <select id="linha" name="cod_linha" class="form-control" required>
                                                <option disabled selected></option>
                                                {$optionLinha}
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="trechoSaf">Sigla / Trecho </label>
                                            <select id="trechoSaf" class="form-control" name="cod_trecho" required>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="pontoNotavel">Ponto Not�vel</label>
                                            <select id="pontoNotavel" name="cod_ponto_notavel" class="form-control">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="complementoLocal">Complemento Local</label>
                                            <input id="complementoLocal" type="text" class="form-control" name="complemento_local" value=""/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-xs-6">
                                            <label for="via">Via </label>
                                            <select name="via" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="posicaoLocal">Posi��o</label>
                                            <input id="posicaoLocal" type="text" class="form-control" name="posicao" value=""/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="kmInicial">Km Inicial </label>
                                            <input id="kmInicial" name="km_inicial" class="form-control" value=""/>

                                        </div>
                                        <div class="col-md-3 col-xs-6">
                                            <label for="kmFinal">Km Final </label>
                                            <input id="kmFinal" name="km_final" class="form-control" value=""/>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-10">
                                            <label for="cod_grupo">Grupo Sistema</label>
                                            <select id="gSistema" name="cod_grupo" class="form-control">
                                                <option value="" disabled selected>Selecione o Grupo</option>
                                                {$optionGrupo}
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label for="sistema">Sistema </label>
                                            <select id="sistema" name="cod_sistema" class="form-control" required="required">
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-xs-10">
                                            <label>Sub-Sistema</label>
                                            <select id="subSistemaSaf" class="form-control" name="cod_subsistema" required>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row" id="divMaterialRodante" style="display: none">
                                        <div class="col-md-2">
                                            <label>C�digo Ve�culo</label>
                                            <select id="veiculoMrSaf" class="form-control" name="cod_veiculo">
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Carro Avariado</label>
                                            <select id="carroMrSaf" name="cod_carro" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Od�metro</label>
                                            <input id="odometroMrSaf" class="form-control number" type="text" name="odometro">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Pref�xo</label>
                                            <select id="prefixoMrSaf" type="text" name="cod_prefixo" class="form-control">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row" id="sublocal" style="display: none">
                                        <div class="col-md-4">
                                            <label>Local</label>
                                            <select id="localGrupo" class="form-control" name="cod_local_grupo">
                                                <option value='' disabled selected>Selecione o Local</option>
                                                {$optionLocal}
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Sub-Local</label>
                                            <select name="cod_sublocal_grupo" id="subLocalGrupo" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Avaria</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div style="display: none;">
                                            <input id="avaria" name="cod_avaria" readonly required value="" class="form-control"/>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <label for="avariaSaf">Op��o de Avaria</label>
                                            <input id="avariaSaf" class="form-control"
                                                   placeholder="Selecione uma Avaria"
                                                   value="" required list="avariaSafDataList" autocomplete="off">

                                            <datalist id="avariaSafDataList">
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="complementoAvaria">Descri��o</label>
                                                    <textarea id="complementoAvaria" name="complemento_falha"
                                                              class="form-control" rows="3" spellcheck="true"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="nivel">N�vel</label>
                                                    <a data-toggle="modal" data-target="#modalHelpNivel"
                                                       class="btn-circle hidden-print"><i
                                                                style="font-size: 20px"
                                                                class="fa fa-question-circle fa-fw"></i></a>
                                                    <select id="nivel" name="nivel" class="form-control">
                                                    <option title="Necess�rio: N�o afeta diretamente a circula��o."
                                                                value="C">
                                                            C
                                                        </option>
                                                        <option title="Emergencial: Atrapalha a circula��o."
                                                                value="B">
                                                            B
                                                        </option>
                                                        <option data-toggle="tooltip"
                                                                title="Urgente: Interrompe a circula��o."
                                                                value="A">
                                                            A
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <div class="col-md-offset-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">A��es</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group  btn-group-justified " role="group">
                                                <div class="btn-group">
                                                    <button type="submit" aria-label="right align" class="btn btn-default btn-lg" title="Salvar">
                                                        <i class="fa fa-floppy-o fa-2x"></i>
                                                    </button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" disabled class="btn btn-default btn-lg btnImprimirFormulario" title="Imprimir" aria-label="right align">
                                                        <i class="fa fa-print fa-2x"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
HTML;

?>