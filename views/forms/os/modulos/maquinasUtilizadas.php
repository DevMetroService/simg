<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 17/03/2016
 * Time: 09:11
 */
?>

<div class="page-header">
    <h1><?php echo $this->tituloOs; ?></h1>
</div>

<div class="row">
    <form class="form-group" method="post" action="<?= "{$this->home_uri}/{$this->actionForm}/salvarMaquinaUtilizada/{$this->codOs}"?>" id="osMaquinaUtilizada">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>M�quinas e Equipamentos Utilizados</label></div>

                <div class="panel-body">

                    <div class="panel panel-default hidden-print">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="nomeMaquinaOs">Nome</label>

                                    <input type="hidden" id="nomeMaquina" name="nomeMaquina" readonly/>
                                    <input id="nomeMaquinaOs" name="nomeMaquinaOs" class="form-control" list="nomeMaquinaDataList" autocomplete="off">

                                    <datalist id="nomeMaquinaDataList">
                                        <?php
                                        if($this->actionForm == "OSM"){
                                            $mr = $this->medoo->select("v_osm", "grupo_atuado", ['cod_osm' => $this->codOs]);
                                            $mr = $mr[0];
                                        }

                                        if($this->actionForm == "moduloOsp"){
                                            $mr = $this->medoo->select("v_osp", "grupo_atuado", ['cod_osp' => $_SESSION['refillOs']['codigoOs']]);
                                            $mr = $mr[0];
                                        }

                                        switch($mr){
                                            case 22:
                                                $selectEquipamento = $this->medoo->select("v_material", ['nome_material', 'cod_material'],
                                                    ["AND" => ["cod_categoria" => 10, "cod_grupo" => 22], "ORDER" => "nome_material"]);
                                                break;
                                            case 23:
                                                $selectEquipamento = $this->medoo->select("v_material", ['nome_material', 'cod_material'],
                                                    ["AND" => ["cod_categoria" => 10, "cod_grupo" => 23], "ORDER" => "nome_material"]);
                                                break;
                                            default:
                                                $selectEquipamento = $this->medoo->select("v_material", ['nome_material','cod_material', 'descricao_material'],
                                                ["ORDER" => "nome_material", "AND" => ["cod_categoria" => 10, "ativo" => 's', "OR" => ["cod_grupo[!]" => [22,23], "cod_grupo" => null]]]);
                                                break;
                                        }

                                        foreach($selectEquipamento as $key => $value){
                                            echo('<option id="'.$value['cod_material'].'" value = "'.$value['nome_material'].' - '. $value['descricao_material'] .'"></option>');
                                        }
                                        ?>
                                    </datalist>
                                </div>
                                <div class="col-md-2">
                                    <label>Sem informa��o</label>
                                    <input type="checkbox" class="form-control" name="checkMaquina" <?php if(!empty($_SESSION['dadosCheck']['maquinaCheck']) && $_SESSION['dadosCheck']['codigoOs'] == $this->codOs ) echo "checked" ?> >
                                </div>
                                <div class="col-md-2">
                                    <label>C�digo OS</label>
                                    <input readonly name="codigoOs" class="form-control" value="<?= $this->codOs?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Descri��o</label>
                                    <input class="form-control" type="text" name="descricaoMaquina">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>Quantidade</label>
                                    <input class="form-control number" type="text" name="qtdMaquina">
                                </div>
                                <div class="col-md-3">
                                    <label>Total de Horas</label>
                                    <input class="form-control hora validaHora" type="text" name="totalHorasMaquina">
                                </div>
                                <div class="col-md-6">
                                    <label>N� S�rie</label>
                                    <input class="form-control" type="text" name="numeroSerieMaquina">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2" style="margin-left: 1em; margin-bottom: 1em;">
                                <div class="btn-group-lg">
                                    <button type="button" id="addMaquinaOs" class="btn btn-lg btn-default">
                                        <i class="fa fa-plus fa-fw"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading"><label>M�quinas e Equipamentos Cadastrados</label></div>

                        <div class="panel-body">
                            <div id="maquinaUtilizada">
                                <?php
                                $contador = 0;

                                foreach($maquinaUtilizada as $dados=>$value){
                                    $contador = $contador +1;
                                    echo('<div class="row">');
                                    echo('<div class="col-md-1"><label>C�digo</label><input type="text" disabled value="'.$value['cod_material'].'" class="form-control"></div>');
                                    echo('<input type="hidden" name="codigoMaquina'.$contador.'" value="'.$value['cod_material'].'">');
                                    $selectMaquina = $this->medoo->select("v_material", "*", ["cod_material" => (int) $value['cod_material'] ]);
                                    $selectMaquina = $selectMaquina[0];
                                    echo('<div class="col-md-4"><label>Nome</label><input type="text" disabled class="form-control" value="'.$selectMaquina['nome_material'].' '.$selectMaquina['descricao_material'].'"></div>');
                                    echo('<div class="col-md-3"><label>N� S�rie</label><input type="text" disabled class="form-control" value="'.$value['num_serie'].'"></div>');
                                    echo('<input type="hidden" name="numeroSerieMaquina'.$contador.'" value="'.$value['num_serie'].'">');
                                    echo('<div class="col-md-1"><label>Qtd.</label><input type="text" name="qtdMaquina'.$contador.'" class="form-control" value="'.$value['qtd'].'"></div>');
                                    echo('<div class="col-md-2"><label>Total de Horas</label><input type="text" name="totalHorasMaquina'.$contador.'" class="form-control hora" value="'.$value['total_hrs'].'"></div>');
                                    echo('<div class="col-md-1"><label>Excluir</label> <input type="checkbox" class="checkbox" name="retirarMaquina'.$contador.'"></div>');
                                    echo("</div>");
                                    echo('<div class="row">');
                                    echo('<div class="col-md-12"><label>Descri��o</label><input class="form-control" type="text" name="descricaoMaquina'.$contador.'" value="'.$value['descricao'].'"></div>');
                                    echo("</div>");

                                }

                                echo('<input type="hidden" value="'.$contador.'" name="contadorMaquina">');

                                ?>

                                <div class="row" style="border-bottom: 1px solid #000000; padding-top: 1em"></div>
                            </div>

                        </div>
                    </div>

                    <div class="row hidden-print">
                        <?php
                        require_once(ABSPATH . "/views/forms/os/modulos/btnNavegacao.php");
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
