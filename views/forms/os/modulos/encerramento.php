<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 15/03/2016
 * Time: 11:36
 */
?>

<div class="page-header">
    <h1><?php echo $tituloOs;?></h1>
</div>

<div class="row">
    <form action="<?= "{$this->home_uri}/{$this->actionForm}/salvarEncerramento/{$this->codOs}"; ?>" class="form-group" method="post" id="formEncerramentoOs">
        <div class="panel panel-primary">
            <div class="panel-heading"><label>Encerramento</label></div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <?php
                            if($this->tipoOS == "osp")
                                echo "<input type='hidden' value='{$qtdDias}' id='diasRestantes'>";

                                echo "<input id='codGrupoSistema' type='hidden' value='{$os['cod_grupo']}' id='diasRestantes'>"
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="equipe">Equipe / Grupo / Sistema Afetado (Falha) </label>
                                        <input type="text" disabled id="equipe" class="form-control"
                                               value="<?php
                                               if ($this->tipoOS != 'osm' && $this->tipoOS != 'osp') {
                                                   echo $unidadeEquipe['sigla']." / ".$nomeGrupo." / ".$ss['nome_sistema'];
                                               }else if(!empty($os))
                                                   echo $unidadeEquipe['sigla']." / ".$os['cod_grupo'].' -- '.$os['nome_grupo']." / ".$ss['sigla_sistema']." -- ".$ss['nome_sistema']
                                               ?>"/>

                                    </div>

                                    <?php if($this->tipoOS == "osm"){ ?>
                                    <div class="col-md-1 col-xs-2">
                                        <label for="nivelSs">N�vel</label>
                                        <input type="text" id="nivelSs" disabled class="form-control"
                                               value="<?php if(!empty($os)) echo $ss['nivel']?>" />

                                    </div>
                                    <?php } ?>

                                    <div class="col-md-3 col-xs-4">
                                        <label for="dataHora">Data / Hora Abertura</label>
                                        <input type="text" disabled id="dataHora" class="form-control"
                                               value="<?php echo $this->parse_timestamp($os['data_abertura']); ?>" />

                                        <!-- Data Desmobiliza��o -->
                                        <input type="hidden" id="desmobilizacao" value="<?php echo $this->parse_timestamp($tempoTotal[0]['data_desmobilizacao'])?>"/>
                                    </div>

                                    <div class="col-md-2 col-xs-2">
                                        <label>C�digo OS</label>
                                        <input name="codigoOs" class="form-control" readonly value="<?= $this->codOs ?>"/>
                                        <?php
                                        if ($this->tipoOS != 'osm' && $this->tipoOS != 'osp') {
                                            echo('<input name="form" type="hidden" required class="free" value="'.$_SESSION['refillOs']['form'].'" />');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Fechamento</label></div>

                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-md-4 col-xs-4">
                                        <label>Tipo do Fechamento</label>
                                        <select name="descricaoTipoFechamento" <?php if($this->tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> required class="form-control">
                                            <option value="" disabled selected>Selecione o Tipo de Fechamento</option>
                                            <?php
                                            if(!empty($encerramento))
                                            {
                                                $selectTipoFechamento = $this->medoo->select("tipo_fechamento",['cod_tipo_fechamento','nome_fechamento']);
                                            }else{
                                                $selectTipoFechamento = $this->medoo->select("tipo_fechamento",['cod_tipo_fechamento','nome_fechamento'], ['ativo'=>'s']);
                                            }

                                            $hasManobra = $this->hasManobraEntrada($os['cod_ssm']);

                                            foreach ($selectTipoFechamento as $key => $value){
                                                if (($hasManobra && $value['cod_tipo_fechamento'] == 1))
                                                    continue;

                                                if(!empty($encerramento) && $encerramento['cod_tipo_fechamento'] == $value['cod_tipo_fechamento']){
                                                    echo('<option value="'.$value['cod_tipo_fechamento'].'" selected>'.$value['nome_fechamento'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_tipo_fechamento'].'">'.$value['nome_fechamento'].'</option>');
                                                }
                                            }

                                            if(in_array($os['cod_grupo'], $this->sistemas_fixos))
                                                echo('<option value="4">COM PEND�NCIA POR MATERIAL N�O CONTRATUAL</option>');
                                            ?>
                                        </select>
                                        <?php
                                        if($this->tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$encerramento['cod_tipo_fechamento']}' name='descricaoTipoFechamento' />";
                                        ?>
                                    </div>

                                    <div class="col-md-4 col-xs-5">
                                        <label>Unidade</label>
                                        <input type="text" disabled class="form-control"
                                               value="<?php echo($unidadeEquipe['nome_unidade']);?>" />

                                    </div>

                                    <div class="col-md-4 col-xs-4">
                                        <label>Data / Hora de Recebimento de Informa��es</label>
                                        <input name="dataHoraFechamento" required type="text" class="form-control timeStamp"
                                               value="<?php if(!empty($encerramento)){ echo($this->parse_timestamp($encerramento['data_encerramento']) );} ?>" />

                                    </div>

                                </div>

                                <div class="row" id="comPendencia" style="display:none">
                                    <div class="col-md-1 col-xs-2" style="display: none;">
                                        <label>N�</label>
                                        <input name="numeroTipoPendenciaFechamento" disabled type="text" class="form-control"
                                               value="<?php if(!empty($encerramento['cod_tipo_pendencia'])) echo $encerramento['cod_tipo_pendencia']?>"/>
                                    </div>
                                    <div class="col-md-4 col-xs-6">
                                        <label>Tipo Pend�ncia</label>
                                        <select id="tipoPendencia" name="tipoPendenciaFechamento" class="form-control">
                                            <option selected disabled value=''>Selecione o tipo de pend�ncia</option>
                                            <?php
                                            $selectTipoPendencia = $this->medoo->select("tipo_pendencia", ['cod_tipo_pendencia','nome_tipo_pendencia']);

                                            foreach($selectTipoPendencia as $key => $value){
                                                if(!empty($encerramento) && $encerramento['cod_tipo_pendencia'] == $value['cod_tipo_pendencia'] ){
                                                    echo('<option value="'.$value['cod_tipo_pendencia'].'" selected>'.$value['nome_tipo_pendencia'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_tipo_pendencia'].'">'.$value['nome_tipo_pendencia'].'</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input id="materialTipoPendencia" type="text" value='MATERIAL' class="form-control" disabled style="display:none"/>
                                    </div>
                                    <div class="col-md-1 col-xs-2" style="display: none;">
                                        <label>N�</label>
                                        <input name="numeroPendenciaFechamento" disabled type="text" class="form-control"
                                               value="<?php if(!empty($encerramento['cod_pendencia'])) echo $encerramento['cod_pendencia'] ?>"/>
                                    </div>
                                    <div class="col-md-4 col-xs-6">
                                        <label>Pend�ncia</label>
                                        <select id="pendencia" name="pendenciaFechamento" class="form-control">
                                            <?php
                                            if(!empty($encerramento)){
                                                $selectPendencia = $this->medoo->select("pendencia", ['cod_pendencia','nome_pendencia'], ["cod_tipo_pendencia" => $encerramento['cod_tipo_pendencia']]);

                                                echo("<option disabled selected>Selecione a pend�ncia</option>");

                                                foreach($selectPendencia as $key => $value){
                                                    if($encerramento['cod_pendencia'] == $value['cod_pendencia'] ){
                                                        echo('<option value="'.$value['cod_pendencia'].'" selected>'.$value['nome_pendencia'].'</option>');
                                                    }else{
                                                        echo('<option value="'.$value['cod_pendencia'].'">'.$value['nome_pendencia'].'</option>');
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input id="materialPendencia" type="text" value='Pend�ncia de Falta de Material CONTRATUAL' class="form-control" disabled style="display:none"/>
                                    </div>
                                    <div id='listaMaterial' class="col-md-4">
                                      <label>Material N�o Contratual</label>
                                      <button id="btnListaMaterial" type="button" class="btn <?= !empty($listaMaterialSolicitado) ? "btn-success" : "btn-danger"?> btn-group btn-block" data-toggle="modal" data-target="#materialAguardandoLiberacao">Lista de Materiais</button>
                                    </div>
                                </div>

                                <div class="row" id="motivoNaoExecucao" style="display: none">
                                    <div class="col-md-12">
                                        <label>Motivo da N�o Execu��o</label>
                                        <input type="text" name="motivoNaoExecucao" class="form-control"
                                               value="<?php if(!empty($encerramento)) echo $encerramento['descricao'] ?>" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2 col-xs-2">
                                        <label>  <?php if($this->tipoOS != "osm" && $this->tipoOS != "osp"){ echo("Atividade Conclu�da");}else{echo("Libera��o");}?></label>
                                        <select name="liberacaoTrafego" <?php if($this->tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                            <option value="s" <?php if(empty($encerramento) || $encerramento['liberacao']=="s") echo 'selected' ?>>Sim</option>
                                            <option value="n" <?php if($encerramento['liberacao']=="n") echo 'selected' ?>>N�o</option>
                                        </select>
                                        <?php
                                        if($this->tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$encerramento['liberacao']}' name='liberacaoTrafego' />";
                                        ?>
                                    </div>

                                    <?php if (in_array($os['grupo_atuado'], $this->sistemas_fixos) && $this->tipoOS == "osm") { ?>
                                        <div class="col-md-2 col-xs-2">
                                            <label>Transfer�ncia</label>
                                            <input type="hidden" id="selectTransferencia" name="selectTranferencia" value="<?php echo $encerramento['transferida'] == "s" ? 's' : 'n' ?>" />
                                            <select name="transferenciaAtividadeOs" <?php if($this->tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                                <option value="s" <?php if(!empty($encerramento) && $encerramento['transferida']=="s") echo 'selected' ?>>Sim</option>
                                                <option value="n" <?php if(empty($encerramento) || $encerramento['transferida']=="n") echo 'selected' ?>>N�o</option>
                                            </select>
                                        </div>
                                            <?php
                                            if($this->tipoOS == "osm" && $encerramento['transferida'] == "s")
                                                echo "<input type='hidden' value='{$encerramento['transferida']}' name='transferenciaAtividadeOs' />";
                                            ?>
                                    <?php }
                                    ?>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4 col-md-2 ">
                                        <label>Tipo Funcionario</label>
                                        <select name="codTipoFuncionario" class="form-control">
                                            <option value="" disabled selected>
                                              Selecione o Tipo de Funcionario
                                            </option>
                                            <?php
                                            $tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);

                                            foreach($tipo_funcionario as $dados =>$values){
                                                if($empresaFuncionario['cod_tipo_funcionario'] == $values["cod_tipo_funcionario"])
                                                    echo"<option value='{$values["cod_tipo_funcionario"]}' selected>{$values["descricao"]}</option>";
                                                else
                                                    echo"<option value='{$values["cod_tipo_funcionario"]}'>{$values["descricao"]}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-xs-2" style="display: none">
                                        <label>Matr�cula</label>
                                        <input name="matriculaResponsavelInformacoesOs" type="text" readonly class="form-control number"
                                               value="<?php if(!empty($empresaFuncionario)){echo($empresaFuncionario['matricula']); } ?>">
                                        <input type="hidden" name="codFuncionario">
                                    </div>
                                    <div class="col-md-8 col-xs-8">
                                        <label>Respons�vel pelas informa��es</label>
                                        <input type="text" name="responsavelInformacaoOs" class="form-control" required list="nomeSolicitanteDataList"
                                               value="<?php if (!empty($encerramento['nome_funcionario'])) echo $encerramento['nome_funcionario']; ?>" autocomplete="off"/>
                                        <datalist id="nomeSolicitanteDataList">
                                        </datalist>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if($this->tipoOS == "osm"){ ?>
                <div class="row" id="dadosTransferencia" style="display: <?php echo $encerramento['transferida'] == 's' ? 'block' : 'none' ?>">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Continuidade</label></div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-1 col-xs-2">
                                        <label>N�</label>
                                        <input name="numeroUnidadeContinuidade" type="text" disabled class="form-control"
                                               value="<?php if(!empty($encerramento['cod_un_equipe'])) echo $unEquipe['cod_unidade']?>" />
                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Local</label>
                                        <select name="unidadeContinuidade" <?php if($encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                            <option disabled selected value="">Selecione o Local</option>
                                            <?php
                                            $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"], ["ORDER" => "cod_unidade"]);

                                            foreach($unidade as $key =>$value){
                                                if(!empty($encerramento['cod_un_equipe']) && $unEquipe['cod_unidade'] == $value['cod_unidade'] ){
                                                    echo('<option value="'.$value['cod_unidade'].'" selected>'.$value['nome_unidade'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if($this->tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$unEquipe['cod_unidade']}' name='unidadeContinuidade' />";
                                        ?>
                                    </div>
                                    <div class="col-md-1 col-xs-2">
                                        <label>N�</label>
                                        <input name="numeroEquipeContinuidade" disabled type="text" class="form-control" value="<?php if(!empty($unEquipe['sigla'])) echo $unEquipe['sigla'] ?>"/>
                                    </div>
                                    <div class="col-md-5 col-xs-10">
                                        <label>Equipe</label>
                                        <select name="equipeContinuidade" <?php if($this->tipoOS == "osm" && $encerramento['transferida'] == "s") echo "disabled"; ?> class="form-control">
                                            <?php
                                            if(!empty($encerramento['cod_un_equipe'])){
                                                $equipe = $this->medoo->select("un_equipe",["[><]equipe"=> "cod_equipe"], ["sigla", "nome_equipe"],
                                                    [
                                                        "ORDER" => "nome_equipe",
                                                        "cod_unidade" => $unEquipe['cod_unidade']
                                                    ]
                                                );

                                                echo('<option disabled selected value="">Selecione a Equipe</option>');

                                                foreach ($equipe as $key => $value) {
                                                    if ($unEquipe['sigla'] == $value['sigla'])
                                                        echo('<option value="' . $value['sigla'] . '" selected>' . $value['nome_equipe'] . '</option>');
                                                    else
                                                        echo('<option value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                                }
                                            }
                                            ?>
                                        </select>
                                        <?php
                                        if($this->tipoOS == "osm" && $encerramento['transferida'] == "s")
                                            echo "<input type='hidden' value='{$unEquipe['sigla_equipe']}' name='equipeContinuidade' />";
                                        ?>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Recomenda��es</label>
                                        <textarea name="recomendacoesContinuidade" <?php if($this->tipoOS == "osm" && $encerramento['transferida'] == "s") echo "readonly"; ?> class="form-control"><?php if(!empty($encerramento)) echo($encerramento['descricao']);?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="row hidden-print">
                    <?php
                        require_once(ABSPATH . "/views/forms/os/modulos/btnNavegacao.php");
                    ?>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="materialAguardandoLiberacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Aguardando Aprova��o</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="panel panel-default">
                  <div class="panel-heading">Adicionar Material</div>
                  <div  class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <label>Material   
                                <button data-toggle='popover' data-placement='top' title='Materiais' data-placement="right"
                                    data-content="Caso n�o esteja encontrando seu material abaixo, entre em contato com o setor respons�vel para acrescimo dele na lista de materiais 'Complementares'. Ap�s isto ele ir� aparecer no campo abaixo.">
                                    <i id="questionMaterial" class="fa fa-question-circle"></i>
                                </button>
                            </label>
                            
                                <select id="materialNContratual" class="form-control" name="materialContratual">
                            <?php
                            foreach($materialNContratual as $key => $value){
                                echo("<option value='{$value['cod_material']}'>{$value['nome_material']} - {$value['descricao_material']}</option>");
                            }
                            ?>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <label>Quantidade</label>
                          <input id="qtdMaterialNContratual" class="form-control float" type="text"/>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2" style="margin-top: 25px">
                        <button id="btnAddMat" class="btn btn-default btn-lg" type="button"><i class="fa fa-plus fa-fw"></i></button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <table id="tableMaterial" class="table table-bordered table-responsive table-stripped tableMaterial">
                      <thead>
                      <tr>
                          <th>Registro</th>
                          <th>Material</th>
                          <th>Quantidade</th>
                          <!-- <th>Unidade de Medida</th> -->
                          <th>A��es</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                        if(!empty($listaMaterialSolicitado))
                          foreach($listaMaterialSolicitado as $dados => $value)
                          {
                            echo("
                            <tr>
                              <td>
                                {$value['cod_osm_material_solicitado']}
                              </td>
                              <td>
                                {$value['nome_material']} - {$value['descricao']}
                              </td>
                              <td>
                                {$value['quantidade']}
                              </td>
                              <td>
                                <button class='btn btn-circle btn-danger btnExcludeLine' title='Excluir Material'><i class='fa fa-close fa-fw'></i></button>
                              </td>
                            </tr>
                            ");
                          }
                        ?>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
