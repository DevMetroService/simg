<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 17/03/2016
 * Time: 10:17
 */
?>

<div class="page-header">
    <h1><?php echo $this->tituloOs;?></h1>
</div>

<div class="row">
    <form class="form-group" method="post" id="osDadosGerais"
          action="<?= "{$this->home_uri}/{$this->actionForm}/salvarDadosGerais/{$this->codOs}"; ?>">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Dados Gerais</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-1 col-xs-2">
                                                <label for="equipe">Equipe</label>
                                                <input name="equipeOs" readonly class="form-control"
                                                       value="<?php if (!empty($this->os)) echo $ss['sigla_equipe'] ?>"/>

                                            </div>

                                            <div class="col-md-4 col-xs-10">
                                                <label for="equipe">Grupo / Sistema Afetado (Falha) </label>
                                                <input type="text" disabled id="equipe" class="form-control"
                                                       value="<?php if (!empty($this->os)) echo($ss['grupo_sistema'] . $ss['sigla_sistema'] . " / " . $ss['nome_grupo'] . " - " . $ss['nome_sistema']); ?>"/>
                                            </div>

                                            <?php
                                            if ($this->tipoOS == "osm") {
                                                $nivelOs;
                                                if (!empty($this->os['nivel']))
                                                    $nivelOs = $this->os['nivel'];
                                                else
                                                    $nivelOs = $ss['nivel'];
                                                ?>
                                                <div class="col-md-1 col-xs-2">
                                                    <label for="nivelSs">N�vel</label>
                                                    <select id="nivelSs" name="nivelAtuado" class="form-control">
                                                        <option value="A" <?php if ($nivelOs == "A") echo("selected") ?>>
                                                            A
                                                        </option>
                                                        <option value="B" <?php if ($nivelOs == "B") echo("selected") ?>>
                                                            B
                                                        </option>
                                                        <option value="C" <?php if ($nivelOs == "C") echo("selected") ?>>
                                                            C
                                                        </option>
                                                    </select>
                                                </div>
                                            <?php } ?>

                                            <div class="col-md-3 col-xs-6">
                                                <label for="dataHora">Data / Hora Abertura</label>
                                                <input type="text" disabled id="dataHora"
                                                       value="<?php echo $this->parse_timestamp($this->os['data_abertura']); ?>"
                                                       class="form-control">

                                            </div>

                                            <div class="col-md-2 col-xs-3">
                                                <label for="numeroOs">C�digo OS</label>
                                                <input readonly name="codigoOs" class="form-control"
                                                       value="<?php if (!empty($this->os)) echo $this->codOs ?>">

                                            </div>
                                            <?php
                                            if ($this->tipoOS == "osp") {
                                                echo("<div class='col-md-2 col-xs-3'>
                                                            <label>N� SSP </label>
                                                            <input type='text' id='numSsp' disabled value='{$ss['cod_ssp']}' class='form-control'/>
                                                        </div>");
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Origem</label></div>

                                                    <div class="panel-body">
                                                        <?php if ($this->tipoOS == "osm") { ?>
                                                            <div class="row">
                                                                <div class="col-md-2  col-xs-4">
                                                                    <label for="tipo">Tipo</label>
                                                                    <input type="text" id="tipo" disabled
                                                                           value="Corretiva" class="form-control"/>
                                                                </div>
                                                                <div class="col-md-2 col-xs-4">
                                                                    <label for="numeroFalha">N� Falha</label>
                                                                    <input type="text" id="numeroFalha" disabled
                                                                           value="<?php echo($saf['cod_saf']); ?>"
                                                                           class="form-control"/>
                                                                </div>
                                                                <div class="col-md-1 col-xs-4">
                                                                    <label for="nivelSAF">N�vel</label>
                                                                    <input type="text" id="nivelSAF" disabled
                                                                           value="<?php echo($saf['nivel']); ?>"
                                                                           class="form-control"/>
                                                                </div>
                                                                <div class="col-md-3 col-xs-8">
                                                                    <label for="horaSAF">Data / Hora</label>
                                                                    <input type="text" id="horaSAF" disabled
                                                                           value="<?php echo($this->parse_timestamp($saf['data_abertura'])); ?>"
                                                                           class="form-control"/>
                                                                </div>
                                                                <div class="col-md-4 col-xs-4">
                                                                    <label for="areaOrigemSaf">�rea</label>
                                                                    <input type="text" disabled id="areaOrigemSaf"
                                                                           class="form-control" value="<?php
                                                                    switch ($saf['tipo_orisaf']) {
                                                                        case 1:
                                                                            echo("MetroService");
                                                                            break;
                                                                        case 2:
                                                                            echo("MetroFor");
                                                                            break;
                                                                        case 3:
                                                                            echo("PJ - MetroService");
                                                                            break;
                                                                        default:
                                                                            echo("Terceiros");
                                                                    }
                                                                    ?>"/>
                                                                </div>
                                                            </div>
                                                        <?php } ?>

                                                        <div class="row">
                                                            <?php
                                                            if (!empty($ss['matricula']) || $saf['tipo_orisaf'] == 1) {
                                                                echo('<div class="col-md-2">
                                                                            <label for="matricula">Matr�cula </label>
                                                                            <input type="text" id="matricula" disabled value="');

                                                                echo (!empty($ss['matricula'])) ? $ss['matricula'] : $saf['matricula'];

                                                                echo('" class="form-control"/></div>');
                                                            }
                                                            ?>
                                                            <div class="col-md-5">
                                                                <label for="nomeCompletoFuncionario">Nome
                                                                    Completo </label>
                                                                <input id="nomeCompletoFuncionario" disabled
                                                                       value="<?php echo(!empty($ss['nome_origem']) ? $ss['nome_origem'] : $saf['nome']); ?>"
                                                                       class="form-control"/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="contato">Contato</label>
                                                                <input id="contato" disabled
                                                                       value="<?php echo(!empty($ss['contato_origem']) ? $ss['contato_origem'] : $saf['contato']); ?>"
                                                                       class="form-control"/>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Identifica��o/Local</label></div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-1 col-xs-2" style="display: none;">
                                                                <label for="codGrupo">Cod.</label>
                                                                <input id="codGrupo" disabled name="codGrupoOsAtuado"
                                                                       value="<?php if (!empty($this->os['grupo_atuado'])) echo $this->os['grupo_atuado'] ?>"
                                                                       class="form-control"/>
                                                            </div>
                                                            <div class="col-md-4 col-xs-10">
                                                                <label for="grupoOsAtuado">Grupo de Sistema Atuado </label>
                                                                <?php
                                                                $grupo = $this->medoo->select("grupo", ['cod_grupo', 'nome_grupo'], ["ORDER" => "nome_grupo"]);
                                                                MainForm::getSelectGrupo($this->os['grupo_atuado'], $grupo, 'grupoOsAtuado', false);
                                                                ?>
                                                            </div>
                                                            <div class="col-md-1 col-xs-2" style="display: none;">
                                                                <label for="grupo">Cod.</label>
                                                                <input id="grupo" disabled name="codSistemaOsAtuado"
                                                                       value="<?php if (!empty($this->os)) echo $this->os['sistema_atuado'] ?>"
                                                                       class="form-control"/>
                                                            </div>
                                                            <div class="col-md-4 col-xs-10">
                                                                <label for="sistema">Sistema Atuado </label>
                                                                <select id="sistema" required name="sistemaOsAtuado"
                                                                        class="form-control">
                                                                    <option value="" disabled selected>Selecione o
                                                                        Sistema
                                                                    </option>
                                                                    <?php
                                                                    if (!empty($this->os)) {
                                                                        $sistema = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => "{$this->os['grupo_atuado']}"]);

                                                                        foreach ($sistema as $dados => $value) {
                                                                            if ($this->os['sistema_atuado'] == $value["cod_sistema"]) {
                                                                                echo('<option value="' . $value["cod_sistema"] . '" selected>' . $value["nome_sistema"] . '</option>');
                                                                            } else {
                                                                                if($value['cod_sistema'] != 174)
                                                                                    echo('<option value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1 col-xs-2" style="display: none;">
                                                                <label>N�</label>
                                                                <input name="codSubSistema" type="text"
                                                                       class="form-control"
                                                                       value="<?php echo $this->os['subsistema_atuado']; ?>"
                                                                       disabled>
                                                            </div>
                                                            <div class="col-md-4 col-xs-10">
                                                                <label>Sub-Sistema</label>
                                                                <select class="form-control" name="subSistemaOsAtuado"
                                                                        required>
                                                                    <option disabled selected> Subsistema</option>
                                                                    <?php
                                                                    if (!empty($this->os) && $this->os['sistema_atuado']) {
                                                                        $subsistema = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['ORDER' => 'nome_subsistema', 'cod_sistema' => "{$this->os['sistema_atuado']}"]);

                                                                        foreach ($subsistema as $dados => $value) {
                                                                            if ($this->os['subsistema_atuado'] == $value["cod_subsistema"]) {
                                                                                echo('<option value="' . $value["cod_subsistema"] . '" selected>' . $value["nome_subsistema"] . '</option>');
                                                                            } else {
                                                                                echo('<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-4 col-xs-3">
                                                                <label for="linha">Linha</label>
                                                                <select required id="linha" name="linhaOsAtuado"
                                                                        class="form-control">
                                                                    <?php
                                                                    $linha = $this->medoo->select('linha', ['nome_linha', 'cod_linha'], ["ORDER" => "cod_linha"]);

                                                                    foreach ($linha as $dados => $value) {
                                                                        if (!empty ($this->os) && $this->os['cod_linha_atuado'] == $value['cod_linha']) {
                                                                            echo('<option value="' . $value['cod_linha'] . '" selected>' . $value['nome_linha'] . '</option>');
                                                                        } else {
                                                                            echo('<option value="' . $value['cod_linha'] . '">' . $value['nome_linha'] . '</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3 col-xs-9">
                                                                <label for="trecho">Sigla/Trecho</label>
                                                                <select required id="trecho" name="trechoOsAtuado"
                                                                        class="form-control">
                                                                    <?php
                                                                    if (!empty($this->os)) {
                                                                        $trecho = $this->medoo->select("trecho", ["nome_trecho", "cod_trecho", "descricao_trecho"], ["cod_linha" => (int)$this->os['cod_linha_atuado']]);

                                                                        foreach ($trecho as $dados => $value) {
                                                                            if ($this->os['trecho_atuado'] == $value['cod_trecho']) {
                                                                                echo('<option value="' . $value['cod_trecho'] . '" selected>' . $value['nome_trecho'] . " - " . $value['descricao_trecho'] . '</option>');
                                                                            } else {
                                                                                echo('<option value="' . $value['cod_trecho'] . '">' . $value['nome_trecho'] . " - " . $value['descricao_trecho'] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="pontoNotavel">Ponto Not�vel</label>
                                                                <select id="pontoNotavel" name="pontoNotavelOsAtuado"
                                                                        class="form-control">
                                                                    <option value="">Ponto Not�vel</option>
                                                                    <?php
                                                                    if (!empty($this->os)) {
                                                                        $pn = $this->medoo->select("ponto_notavel", ["nome_ponto", "cod_ponto_notavel"], ["cod_trecho" => (int)$this->os['trecho_atuado']]);

                                                                        foreach ($pn as $dados => $value) {
                                                                            if ($value['cod_ponto_notavel'] == $this->os['cod_ponto_notavel_atuado']) {
                                                                                echo('<option value="' . $value['cod_ponto_notavel'] . '" selected>' . $value['nome_ponto'] . '</option>');
                                                                            } else {
                                                                                echo('<option value="' . $value['cod_ponto_notavel'] . '">' . $value['nome_ponto'] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="complemento">Complemento Local</label>
                                                                <input id="complementoLocal" type="text"
                                                                       class="form-control"
                                                                       name="complementoLocalOsAtuado"
                                                                       value="<?php if (!empty($this->os)) echo $this->os[($this->tipoOS == "osm") ? 'complemento_local' : 'complent'] ?>">

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3 col-xs-3">
                                                                <label for="viaAtuada">Via</label>
                                                                <select id="viaAtuada" class="form-control"
                                                                        name="viaOsAtuada">
                                                                    <?php
                                                                    if (!empty($this->os)) {
                                                                        $via = $this->medoo->select("via", ["nome_via", "cod_via"], ["cod_linha" => (int)$this->os['cod_linha_atuado']]);

                                                                        foreach ($via as $dados => $value) {
                                                                            if ($this->os['cod_via'] == $value['cod_via']) {
                                                                                echo('<option value="' . $value['cod_via'] . '" selected>' . $value['nome_via'] . '</option>');
                                                                            } else {
                                                                                echo('<option value="' . $value['cod_via'] . '">' . $value['nome_via'] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3 col-xs-3">
                                                                <label for="kmInicial">Km. Inicial</label>
                                                                <input type="text" name="kmInicialOs" id="kmInicial"
                                                                       class="form-control"
                                                                       value="<?php if (!empty($this->os['km_inicial'])) echo $this->os['km_inicial'] ?>">
                                                            </div>

                                                            <div class="col-md-3 col-xs-3">
                                                                <label for="kmFinal">Km. Final</label>
                                                                <input type="text" name="kmFinalOs" id="kmFinal"
                                                                       class="form-control"
                                                                       value="<?php if (!empty($this->os['km_final'])) echo $this->os['km_final'] ?>"/>

                                                            </div>

                                                            <div class="col-md-3 col-xs-3">
                                                                <label for="posicao">Posi��o</label>
                                                                <input type="text" name="posicaoAtuado" id="posicao"
                                                                       class="form-control"
                                                                       value="<?php if (!empty($this->os['posicao_atuado'])) echo $this->os['posicao_atuado'] ?>"/>

                                                            </div>
                                                        </div>

                                                        <?php if ($this->os['grupo_atuado'] == '22' || $this->os['grupo_atuado'] == '23' || $this->os['grupo_atuado'] == '26') {
                                                            $materialRodante = true;
                                                        } ?>
                                                        <div class="row" id="divMaterialRodante"
                                                             style="display:<?php echo ($materialRodante) ? "display" : "none" ?>">
                                                            <div class="col-md-2 col-xs-3">
                                                                <label>C�digo Ve�culo</label>
                                                                <select class="form-control"
                                                                        name="veiculoMrOsm" <?php if ($materialRodante) echo "required"; ?>>
                                                                    <option value="" selected disabled>Ve�culo</option>
                                                                    <?php
                                                                    if ($ss['cod_veiculo'] || $this->os['cod_veiculo']) {
                                                                        $selectVeiculo = $this->medoo->select("veiculo", "*", ['cod_grupo' => (int)$this->os['grupo_atuado']]);

                                                                        $codVeiculo = (!empty($this->os['cod_veiculo'])) ? $this->os['cod_veiculo'] : $ss['cod_veiculo'];

                                                                        foreach ($selectVeiculo as $dados => $value) {
                                                                            if ($codVeiculo == $value['cod_veiculo']) {
                                                                                echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
                                                                            } else {
                                                                                echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Carro Avariado</label>
                                                                <select name="carroMrOs"
                                                                        class="form-control">
                                                                    <?php
                                                                        $selectCarro = $this->medoo->select("carro", "*", ['cod_veiculo' => (int)$this->os['cod_veiculo']]);

                                                                        foreach ($selectCarro as $dados => $value) {
                                                                            if ($this->os['cod_carro'] == $value['cod_carro']) {
                                                                                echo("<option value='{$value['cod_carro']}' selected>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                                            } else {
                                                                                echo("<option value='{$value['cod_carro']}'>{$value['sigla_carro']} - {$value['nome_carro']}</option>");
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3 col-xs-3">
                                                                <label>Od�metro</label>
                                                                <input <?php
                                                                echo "value='{$this->os['odometro']}'";

                                                                if ($materialRodante) echo "required";
                                                                ?>
                                                                        class="form-control number" type="text" name="odometroMrOsm">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Servi�o a ser Executado</label>
                                                    </div>

                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-2 col-xs-2" style="display: none;">
                                                                <label>C�digo</label>
                                                                <input name="servicoExecutadoOs" readonly required
                                                                       value="<?php if (!empty($this->os['cod_servico'])) echo $this->os['cod_servico'] ?>"
                                                                       class="form-control"/>

                                                            </div>
                                                            <div class="col-md-12">
                                                                <label>Descri��o servi�o</label>
                                                                <input name="servicoInput" list="servicoDataList"
                                                                       placeholder="Selecione um Servi�o"
                                                                       autocomplete="off"
                                                                       value="<?php if (!empty($this->os['nome_servico'])) echo $this->os['nome_servico'] ?>"
                                                                       required class="form-control"/>

                                                                <datalist id="servicoDataList">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    $servicos = $this->medoo->select("servico", ["nome_servico", "cod_servico"], ["ORDER" => "nome_servico"]);

                                                                    foreach ($servicos as $dados => $value) {
                                                                        echo('<option value="' . $value['cod_servico'] . '">' . $value['nome_servico'] . " - " . $value['cod_servico'] . '</option>');
                                                                    }
                                                                    ?>
                                                                </datalist>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2 col-xs-4">
                                                                <label for="efetivoServico">Efetivo</label>
                                                                <input type="text" class="form-control number"
                                                                       id="efetivoServico" name="efetivoServico"
                                                                       value="<?php if (!empty($this->os)) echo $this->os['qtd_pessoas'] ?>"/>

                                                            </div>

                                                            <div class="col-md-3 col-xs-4">
                                                                <label for="tempoServico">Tempo</label>
                                                                <input type="text" class="form-control number"
                                                                       id="tempoServico" name="tempoServico"
                                                                       value="<?php if (!empty($this->os['tempo'])) echo $this->os['tempo'] ?>"/>

                                                            </div>

                                                            <div class="col-md-2 col-xs-4">
                                                                <label for="unidadeTempoServico">Unidade</label>
                                                                <select id="unidadeTempoServico"
                                                                        name="unidadeTempoServico" class="form-control">
                                                                    <?php
                                                                    $unidadeTempo = $this->medoo->select('unidade_tempo', ['cod_uni_tempo', 'nome_un_tempo']);

                                                                    foreach ($unidadeTempo as $dados => $value) {
                                                                        if (!empty($this->os) && $this->os['unidade_tempo'] == $value['cod_uni_tempo']) {
                                                                            echo('<option value="' . $value['cod_uni_tempo'] . '" selected>' . $value['nome_un_tempo'] . '</option>');
                                                                        } else {
                                                                            echo('<option value="' . $value['cod_uni_tempo'] . '">' . $value['nome_un_tempo'] . '</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3 col-xs-4">
                                                                <label for="totalServico">Total Servi�o</label>
                                                                <input type="text" class="form-control number"
                                                                       id="totalServico" name="totalServico"
                                                                       value="<?php if (!empty($this->os['total_servico'])) echo $this->os['total_servico'] ?>"/>

                                                            </div>

                                                            <div class="col-md-2 col-xs-4">
                                                                <label for="unidadeTotalServico">Unidade</label>
                                                                <select id="unidadeTotalServico"
                                                                        name="unidadeTotalServico" class="form-control">
                                                                    <?php
                                                                    $unidadeTempo = $this->medoo->select('unidade_medida', ['cod_uni_medida', 'nome_uni_medida']);

                                                                    foreach ($unidadeTempo as $dados => $value) {
                                                                        if (!empty($this->os) && $this->os['unidade_medida'] == $value['cod_uni_medida']) {
                                                                            echo('<option value="' . $value['cod_uni_medida'] . '" selected>' . $value['nome_uni_medida'] . '</option>');
                                                                        } else {
                                                                            echo('<option value="' . $value['cod_uni_medida'] . '">' . $value['nome_uni_medida'] . '</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="complementoServico">Complemento
                                                                    Servi�o</label>
                                                                <textarea class="form-control" rows="3" spellcheck='true'
                                                                          id="complementoServico" name="complementoServico"><?php if (!empty($this->os['complemento'])) echo $this->os['complemento'] ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4 col-xs-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Preenchimento</label></div>


                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="responsavelSAF">Respons�vel</label>
                                                                <input class="form-control" type="text"
                                                                       id="responsavelSAF" disabled
                                                                       value="<?php echo (empty($responsavel)) ? "" : $responsavel; ?>"/>
                                                                <input class="form-control" type="hidden"
                                                                       name="responsavelPreenchimento"
                                                                       value="<?php echo (empty($responsavel)) ? $this->dadosUsuario['usuario'] : $responsavel; ?>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-xs-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Situa��o da OS</label></div>


                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="situacao">Situa��o</label>
                                                                <input type="text" disabled class="form-control"
                                                                       value="<?php echo($this->statusOs['nome_status']); ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-xs-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Condi��o da Falha</label></div>


                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="condicao">Condi��o</label>
                                                                <input class="form-control" type="text" id="situacao"
                                                                       disabled
                                                                       value="<?php echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado' ?>"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if (!empty($this->statusOs['nome_status']) && ($this->statusOs['nome_status'] == 'N�o Executado' || $this->statusOs['nome_status'] == 'Duplicado' || $this->statusOs['nome_status'] == 'N�o Configura falha')) {
                                            echo "<div class='row'>
                                                <div class='col-lg-12'>
                                                    <div class='panel panel-default'>
                                                        <div class='panel-heading'><label>Situa��o da Solicita��o</label></div>

                                                        <div class='panel-body'>
                                                            <div class='row'>
                                                                <div class='col-md-2'>
                                                                    <label for='nomeStatusSaf'>Status</label>
                                                                    <input id='nomeStatusSaf' type='text' class='form-control' disabled
                                                                           value='{$this->statusOs['nome_status']}'/>
                                                                </div>";
                                            if ($this->statusOs['cod_status'] == 24) {
                                                echo "<div class='col-md-8'>";
                                                $divDuplicada = ($this->tipoOS == 'osm') ? "<label>C�digo duplicado:</label> <input type='button' class='btnOsmDuplicado form-control' value='{$this->statusOs['cod_osm_duplicado']}'>" : "<label>C�digo duplicado:</label> <input type='button' class='btnOspDuplicado form-control' value='{$this->statusOs['cod_osp_duplicado']}'>";
                                            } else {
                                                echo "<div class='col-md-10'>";
                                            }
                                            echo "<label for='descricaoStatusSaf'>Descri��o</label>
                                                                    <textarea id='descricaoStatusSaf' class='form-control' disabled
                                                                                  spellcheck='true'>{$this->statusOs['descricao']}";
                                            echo "</textarea>
                                                                </div>";
                                            if (!empty($divDuplicada)) {
                                                echo "<div class=col-md-2>{$divDuplicada}</div>";
                                            }
                                            echo "</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row hidden-print">
                            <?php
                            require_once(ABSPATH . "/views/forms/os/modulos/btnNavegacao.php");
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
