<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 17/03/2016
 * Time: 09:29
 */
?>

<div class="page-header">
    <h1><?php echo $this->tituloOs; ?></h1>
</div>
<div class="row">
    <form method="post" class="form-group" action="<?= "{$this->home_uri}/{$this->actionForm}/salvarRegistroExecucao/{$this->codOs}"?>" id="osRegistroExecucao">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><label>Registro de Execu��o</label></div>

                <div class="panel-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-1 col-xs-2" style="display: none;">
                                    <label>N�</label>
                                    <input name="codUnidadeExecucao" disabled type="text"
                                           value="<?php echo (!empty($registro)) ? $unEquipe['cod_unidade'] : $ss['cod_unidade']; ?>"
                                           class="form-control">
                                </div>
                                <div class="col-md-5 col-xs-10">
                                    <label>Local</label>
                                    <select name="unidadeExecucao" class="form-control" required>
                                        <option value="" disabled selected>Selecione o Local</option>
                                        <?php
                                        $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"], ["ORDER" => "cod_unidade"]);

                                        if (!empty($registro)) {
                                            $codUnidade = $unEquipe['cod_unidade'];
                                        } else {
                                            $codUnidade = $ss['cod_unidade'];
                                        }

                                        foreach ($unidade as $key => $value) {
                                            if ($codUnidade == $value['cod_unidade']) {
                                                echo('<option value="' . $value['cod_unidade'] . '" selected>' . $value['nome_unidade'] . '</option>');
                                            } else {
                                                echo('<option value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-1 col-xs-2" style="display: none;">
                                    <label>N�</label>
                                    <input name="codEquipeExecucao" disabled type="text"
                                           value="<?php echo (!empty($registro)) ? $unEquipe['sigla'] : $ss['sigla_equipe']; ?>"
                                           class="form-control">
                                </div>

                                <div class="col-md-5 col-xs-10">
                                    <label>Equipe</label>
                                    <select name="equipeExecucao" class="form-control" required>
                                        <option value="" disabled selected>Selecione a Equipe</option>
                                        <?php
                                        $equipe = $this->medoo->select("un_equipe", ["[><]equipe" => "cod_equipe"], ["sigla", "nome_equipe", "cod_equipe"],
                                            [
                                                "ORDER" => "nome_equipe",
                                                "cod_unidade" => $codUnidade
                                            ]
                                        );

                                        if (!empty($registro)) {
                                            $codEquipe = $unEquipe['cod_equipe'];
                                        } else {
                                            $codEquipe = $ss['cod_equipe'];
                                        }

                                        foreach ($equipe as $key => $value) {
                                            if ($codEquipe == $value['cod_equipe']) {
                                                echo('<option selected value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                            } else {
                                                echo('<option value="' . $value['sigla'] . '">' . $value['nome_equipe'] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-2 col-xs-4">
                                    <label>C�digo OS</label>
                                    <input readonly name="codigoOs" class="form-control"
                                           value="<?= $this->codOs ?>">
                                </div>
                            </div>

                            <div class="row" style="padding-top: 2em;">
                                <div class="col-md-1 col-xs-2" style="display: none;">
                                    <label>Cod.</label>
                                    <input name="causaAvaria" readonly required class="form-control" type="text"
                                           value="<?php
                                           if ($this->tipoOS != 'osm')
                                               echo 136;
                                           else
                                               if (!empty($registro))
                                                   echo $registro['cod_causa'];
                                               else{
                                                   if( $firstRE ){
                                                       echo $firstRE['cod_causa'];
                                                   }
                                               }
                                           ?>">
                                </div>

                                <div class="col-md-6 col-xs-10">
                                    <label>Descri��o da Causa</label>
                                    <?php
                                    if ($this->tipoOS != 'osm') {
                                        echo '<input disabled class="form-control" value="INEXISTENTE">';
                                    } else {
                                        ?>
                                        <input name="causaAvariaInput" list="causaAvariaDataList" autocomplete="off"
                                               required class="form-control">

                                        <datalist id="causaAvariaDataList">
                                            <option value=""></option>
                                            <?php
                                            $selectCausa = $this->medoo->select("causa", ["cod_causa", "nome_causa"], ["ORDER" => "nome_causa"]);

                                            foreach ($selectCausa as $key => $value) {
                                                echo '<option value="' . $value['cod_causa'] . '">' . $value['nome_causa'] . '</option>';
                                            }
                                            ?>
                                        </datalist>
                                    <?php } ?>
                                </div>
                                <div class="col-md-1 col-xs-2" style="display: none;">
                                    <label>Cod.</label>
                                    <input name="atuacaoExecucao" readonly class="form-control" type="text"
                                           value="<?php if (!empty($registro)) echo $registro['cod_atuacao'] ?>">
                                </div>
                                <div class="col-md-6 col-xs-10">
                                    <label>Descri��o da Atua��o</label>
                                    <input name="atuacaoExecucaoInput" list="atuacaoExecucaoDataList" autocomplete="off"
                                           required class="form-control">

                                    <datalist id="atuacaoExecucaoDataList">
                                        <option value=""></option>
                                        <?php
                                        $selectAtuacao = $this->medoo->select("atuacao", ['cod_atuacao', 'nome_atuacao'], ["ORDER" => "cod_atuacao"]);

                                        foreach ($selectAtuacao as $key => $value) {
                                            echo('<option value="' . $value['cod_atuacao'] . '">' . $value['nome_atuacao'] . '</option>');
                                        }
                                        ?>
                                    </datalist>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label>Observa��o da Causa</label>
                                    <textarea <?php if ($this->tipoOS != 'osm') echo "disabled" ?>
                                            name="obsCausa"
                                            class="form-control"><?php if (!empty($registro['desc_causa'])) {
                                            echo($registro['desc_causa']);
                                        } ?></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label>Observa��o da Atua��o</label>
                                    <textarea name="obsAtuacao"
                                              class="form-control"><?php if (!empty($registro['desc_atuacao'])) {
                                            echo($registro['desc_atuacao']);
                                        } ?></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-1 col-xs-2" style="display: none;">
                                    <label>Cod.</label>
                                    <input name="codAgCausador" disabled class="form-control" type="text"
                                           value="<?php
                                           if ($this->tipoOS != 'osm')
                                               echo 22;
                                           else  if (!empty($registro))
                                               echo $registro['cod_ag_causador'];
                                           else{
                                               if( $firstRE ){
                                                   echo $firstRE['cod_ag_causador'];
                                               }
                                           }
                                           ?>">
                                </div>
                                <div class="col-md-6 col-xs-10">
                                    <label>Ag. Causador</label>
                                    <?php if ($this->tipoOS != 'osm') {
                                        echo '<input disabled class="form-control" value="INEXISTENTE">';
                                    } else
                                    {
                                        $agCausador = $this->medoo->select("agente_causador", ['cod_ag_causador', 'nome_agente'], [
                                            "ativo" => "s",
                                            "ORDER" => "nome_agente"
                                        ]);

                                        if($blocked){
                                            echo("<select class='form-control' disabled>");
                                            foreach ($agCausador as $key => $value) {
                                                if($firstRE['cod_ag_causador'] == $value['cod_ag_causador']){
                                                    echo("<option disabled selected value='{$value['cod_ag_causador']}'>{$value['nome_agente']}</option>");
                                                }
                                            }
                                            echo("</select>");
                                            echo "<input type='hidden' readonly class='form-control' name='agCausador' value='{$firstRE['cod_ag_causador']}'>";
                                        }
                                        else
                                        {
                                        ?>
                                        <select class="form-control" name="agCausador" required>
                                            <option disabled selected value="">Selecione o agente causador</option>
                                            <?php
                                            foreach ($agCausador as $key => $value) {
                                                if (!empty($registro)){
                                                    if($registro['cod_ag_causador'] == $value['cod_ag_causador']){
                                                        echo('<option value="' . $value['cod_ag_causador'] . '" selected>' . $value['nome_agente'] . '</option>');
                                                    }else{
                                                        echo('<option value="' . $value['cod_ag_causador'] . '">' . $value['nome_agente'] . '</option>');
                                                    }
                                                } else {
                                                    if($firstRE){
                                                        if($firstRE['cod_ag_causador'] == $value['cod_ag_causador']){
                                                            echo('<option value="' . $value['cod_ag_causador'] . '" selected>' . $value['nome_agente'] . '</option>');
                                                        }
                                                    }else{
                                                        echo('<option value="' . $value['cod_ag_causador'] . '">' . $value['nome_agente'] . '</option>');
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    <?php
                                        }
                                    }?>
                                </div>
                                <div class="col-md-3 col-xs-4">
                                    <label>Clima</label>
                                    <select name="climaExecucao" class="form-control">
                                        <option value="b" <?php if ($registro['clima'] == "b") echo 'selected' ?>>Bom
                                        </option>
                                        <option value="c" <?php if ($registro['clima'] == "c") echo 'selected' ?>>
                                            Chuvoso
                                        </option>
                                        <option value="n" <?php if ($registro['clima'] == "n") echo 'selected' ?>>
                                            Nublado
                                        </option>
                                        <option value="e" <?php if ($registro['clima'] == "e") echo 'selected' ?>>
                                            Neblina
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-xs-4">
                                    <label>Temperatura</label>
                                    <input type="text" name="temperaturaExecucao" maxlength="10"
                                           value="<?php if (!empty($registro)) {
                                               echo($registro['temperatura']);
                                           } ?>" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div style="display: none">
                                    <div class="col-md-2 col-xs-4">
                                        <label>N� Locomotiva</label>
                                    </div>
                                    <div class="col-md-2 col-xs-4">
                                        <label>Prefixo</label>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-4">
                                    <label>Transporte de Via Utilizado</label>
                                    <select name="transporteExecucao" class="form-control">
                                        <option value="">Selecione Transporte de Via</option>
                                        <?php
                                        $selectVeiculo = $this->medoo->select("automovel", ['cod_automovel', 'nome_automovel', 'numero_automovel'], ["ORDER" => "cod_automovel"]);

                                        foreach ($selectVeiculo as $key => $value) {
                                            if (!empty($registro) && $registro['cod_automovel'] == $value['cod_automovel']) {
                                                echo('<option value="' . $value['cod_automovel'] . '" selected>' . $value['nome_automovel'] . ' - ' . $value['numero_automovel'] . '</option>');
                                            } else {
                                                echo('<option value="' . $value['cod_automovel'] . '">' . $value['nome_automovel'] . ' - ' . $value['numero_automovel'] . '</option>');
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-1 hidden-print">
                                    <label>Cautela</label>
                                    <input type="checkbox" class="checkbox"
                                        <?php if (!empty($registro) && $registro['cautela'] == "s") echo 'checked' ?>
                                           name="cautelaExecucao">
                                </div>
                                <div id="cautelaRegistro" <?php if ($registro['cautela'] == "n" || empty($registro['cautela'])) echo 'style="display: none"'; ?>>
                                    <div class="col-md-1 col-xs-2">
                                        <label>Km Inicial</label>
                                        <input type="text" name="kmIniCautela" class="form-control" maxlength="20"
                                               value="<?php if (!empty($registro['km_inicial_cautela'])) echo $registro['km_inicial_cautela'] ?>">
                                    </div>
                                    <div class="col-md-1 col-xs-2">
                                        <label>Km Final</label>
                                        <input type="text" name="kmFimCautela" class="form-control" maxlength="20"
                                               value="<?php if (!empty($registro['km_final_cautela'])) echo $registro['km_final_cautela'] ?>">
                                    </div>
                                    <div class="col-md-2 col-xs-3">
                                        <label>Restringir Velocidade</label>
                                        <input type="text" name="restringirVelocidadeExecucao"
                                               class="form-control number" maxlength="20"
                                               value="<?php echo ($registro['restricao_velocidade']) ? $registro['restricao_velocidade'] : $registro['restricao_veloc'] ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Dados Complementares</label>
                                    <textarea name="dadosComplementares"
                                              class="form-control"><?php if (!empty($registro)) {
                                            echo($registro['dados_complementar']);
                                        } ?></textarea>
                                </div>
                            </div>

                            <?php
                                if($this->os['grupo_atuado'] == 22){
                                    echo ("<div class='row'>
                                        <div class='col-md-3'>
                                            <label>Horimetro Tra��o MA</label>
                                            <input type='text' class='form-control' value='{$materialRodante['horimetro_tracao_ma']}' name='horimetroTracaoMA'>
                                        </div>
                                        <div class='col-md-3'>
                                            <label>Horimetro Gerador MA</label>
                                            <input type='text' class='form-control' value='{$materialRodante['horimetro_gerador_ma']}' name='horimetroGeradorMA'>
                                        </div>
                                        <div class='col-md-3'>
                                            <label>Horimetro Tra��o MB</label>
                                            <input type='text' class='form-control' value='{$materialRodante['horimetro_tracao_mb']}' name='horimetroTracaoMB'>
                                        </div>
                                        <div class='col-md-3'>
                                            <label>Horimetro Gerador MB</label>
                                            <input type='text' class='form-control' value='{$materialRodante['horimetro_gerador_mb']}' name='horimetroGeradorMB'>
                                        </div>
                                    </div>");
                                }
                            ?>
                        </div>
                    </div>

                    <div class="row hidden-print">
                        <?php
                        require_once(ABSPATH . "/views/forms/os/modulos/btnNavegacao.php");
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>