<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 16/03/2016
 * Time: 11:59
 */
?>

<div class="page-header">
    <h1><?php echo $tituloOs; ?></h1>
</div>

<div class="row">
    <form class="form-group" action="<?php echo HOME_URI . "/" . $actionForm; ?>/salvarManobrasEletricas" method="post"
          id="osManobrasEletricas">
        <div class="col-md-12">
            <div class="panel panel-primary hidden-print">
                <div class="panel-heading"><label>Manobras El�tricas</label></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Identifica��o</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Local</label>
                                            <input type="text" class="form-control" name="localManobra"/>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Chave/Disjuntor</label>
                                            <input type="text" class="form-control" name="chaveManobra"/>
                                        </div>
                                        <div class="col-md-offset-4 col-md-2">
                                            <label>C�digo OS</label>
                                            <input name="codigoOs" class="form-control" readonly
                                                   value="<?php echo $_SESSION['refillOs']['codigoOs'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Desligamento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Data</label>
                                            <input type="text" class="form-control data validaData"
                                                   name="dataDesligamento"/>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Hora</label>
                                            <input class="form-control hora validaHora" name="horaDesligamento"/>
                                        </div>
                                        <div class="col-md-4">
                                            <label>P.M.</label>
                                            <input type="text" class="form-control" name="pm" data-placement="right"
                                                   rel="tooltip-wrapper" data-title="Pedido de Manobra"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Solicitado por [Matricula]</label>
                                            <input type="text" class="form-control number"
                                                   name="solicitadoDesligamento"/>
                                        </div>
                                        <div class="col-md-8">
                                            <label>Nome Solicitante</label>
                                            <input type="text" class="form-control" name="nomeSolicitadoDesligamento"
                                                   list="nomeSolicitanteDataList" autocomplete="off"/>

                                            <datalist id="nomeSolicitanteDataList">
                                                <?php
                                                $selectFuncionario = $this->medoo->select("funcionario", ["matricula", "nome_funcionario"], ["ORDER" => "nome_funcionario"]);

                                                foreach ($selectFuncionario as $key => $value) {
                                                    echo("<option value='{$value['matricula']}'>{$value['nome_funcionario']} - {$value['matricula']}</option>");
                                                }
                                                ?>
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Atendido por [Matricula]</label>
                                            <input type="text" class="form-control" name="atendidoDesligamento">
                                        </div>
                                        <div class="col-md-8">
                                            <label>Nome</label>
                                            <input type="text" class="form-control" name="nomeAtendidoDesligamento">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"><label>Ligamento</label></div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Data</label>
                                            <input type="text" class="form-control data validaData"
                                                   name="dataLigamento"/>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Hora</label>
                                            <input type="text" class="form-control hora validaHora"
                                                   name="horaLigamento"/>
                                        </div>
                                        <div class="col-md-4">
                                            <label>P.M.</label>
                                            <input type="text" class="form-control" disabled name="pmLigamento"
                                                   data-placement="right" rel="tooltip-wrapper"
                                                   data-title="Pedido de Manobra"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Solicitado por [Matricula]</label>
                                            <input type="text" class="form-control number" name="solicitadoLigamento"/>
                                        </div>
                                        <div class="col-md-8">
                                            <label>Nome Solicitante</label>
                                            <input type="text" class="form-control" name="nomeSolicitadoLigamento"
                                                   list="nomeSolicitanteDataList" autocomplete="off"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Atendido por [Matricula]</label>
                                            <input type="text" class="form-control" name="atendidoLigamento"/>
                                        </div>
                                        <div class="col-md-8">
                                            <label>Nome</label>
                                            <input type="text" class="form-control" name="nomeAtendidoLigamento"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2" style="margin-left: 1em; margin-bottom: 1em;">
                        <div class="btn-group-lg">
                            <button type="button" id="addManobraEletrica" class="btn btn-lg btn-default">
                                <i class="fa fa-plus fa-fw"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading"><label>Lista de Manobras</label></div>

                <div class="panel-body">
                    <div id="listaManobras">

                        <?php
                        $contadorManobra = 0;
                        if ($manobrasEletricas) {
                            foreach ($manobrasEletricas as $dados => $value) {
                                $selectSolicitanteDesl = $this->medoo->select("funcionario", "nome_funcionario", ["matricula" => (int)$value['matricula_desl']]);
                                $selectSolicitanteDesl = $selectSolicitanteDesl[0];
                                $selectSolicitanteLig = $this->medoo->select("funcionario", "nome_funcionario", ["matricula" => (int)$value['matricula_lig']]);
                                $selectSolicitanteLig = $selectSolicitanteLig[0];


                                $contadorManobra = $contadorManobra + 1;
                                echo('<div class="row">');
                                echo('<div class="col-md-2"><label>Local</label><input type="text" class="form-control" disabled value="' . $value['local'] . '"></div>');
                                echo('<div class="col-md-1"><label>Chave/Disj.</label><input type="text" class="form-control" disabled value="' . $value['chave'] . '"></div>');
                                echo('<div class="col-md-2"><label>Data/Hora Deslig.</label><input type="text" class="form-control" disabled value="' . $value['data_desl'] . ' ' . $value['hora_desl'] . '"></div>');
                                echo('<div class="col-md-1"><label>P.M. Deslig.</label><input type="text" class="form-control" disabled value="' . $value['pedido_manobra'] . '"></div>');
                                echo('<input type="hidden" name="pm' . $contadorManobra . '" class="form-control" value="' . $value['pedido_manobra'] . '">');
                                echo('<div class="col-md-3"><label>Solicitado Por</label><input type="text" class="form-control" disabled value="' . $value['matricula_desl'] . ' - ' . $selectSolicitanteDesl . '"></div>');
                                echo('<div class="col-md-3"><label>Atendido Por</label><input type="text" class="form-control" disabled value="' . $value['atendido_por_desl'] . '"></div>');
                                echo('</div>');
                                echo('<div class="row"  style="border-bottom: 2px solid #eee; padding-bottom: 10px; margin: 2px 0px;">');
                                echo('<div class="col-md-2"><label>Data/Hora Deslig.</label><input type="text" class="form-control" disabled value="' . $value['data_lig'] . ' ' . $value['hora_lig'] . '"></div>');
                                echo('<div class="col-md-2"><label>P.M. Ligam.</label><input type="text" class="form-control" disabled value="' . $value['pedido_manobra'] . '"></div>');
                                echo('<div class="col-md-3"><label>Solicitado Por</label><input type="text" class="form-control" disabled value="' . $value['matricula_lig'] . ' - ' . $selectSolicitanteLig . '"></div>');
                                echo('<div class="col-md-3"><label>Atendido Por</label><input type="text" class="form-control" disabled value="' . $value['atendido_por_lig'] . '"></div>');
                                echo('<div class="col-md-2"><label>Excluir</label><input type="checkbox" class="checkbox" name="retirarManobra' . $contadorManobra . '"></div>');
                                echo('</div>');
                            }
                        }
                        ?>
                        <input type="hidden" value="<?php echo($contadorManobra); ?>" name="contadorManobra">

                    </div>
                </div>
            </div>

            <div class="row hidden-print">
                <?php
                require_once(ABSPATH . "/views/forms/os/modulos/btnNavegacao.php");
                ?>
            </div>
        </div>
    </form>
</div>