<?php

$linha = $this->medoo->select("linha", "*");
$optionLinha="";
foreach ($linha as $dados=>$value){
    $optionLinha .= "<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>";
}

$tableLocal;
foreach ($local as $dados => $value) {
    $tableLocal .= "<tr>";
    $tableLocal .= "<td>{$value['cod_local']}</td>";
    $tableLocal .= "<td data-linha='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
    $tableLocal .= "<td data-estacao='{$value['cod_estacao']}'>{$value['nome_estacao']}</td>";
    $tableLocal .= "<td>{$value['grupo']}</td>";
    $tableLocal .= "<td data-value='{$value['cod_local']}'>{$value['nome_local']}</td>";
    $tableLocal .= "<td><button type='button' title='Editar Local' data-codigo='{$value['cod_local']}' class='btn btn-default btn-circle editLocal'><i class='fa fa-file-o fa-fw'></i></button>";
    $tableLocal .= "</tr>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de Local</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Dados de Locais</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/local/store" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Linha</label>
                            <select id="linha" name="cod_linha" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                                {$optionLinha}
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Esta��o</label>
                            <select id="estacao" name="cod_estacao" class="form-control" required>
                                <option value="">Selecione linha para filtrar</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Grupo</label>
                            <select id="grupo" name="grupo" class="form-control" required>
                                <option value="">Selecione o grupo</option>
                                <option value="E">Edifica��es</option>
                                <option value="S">Subesta��o</option>
                                <option value="T">Telecom</option>
                                <option value="R">Rede A�rea</option>
                                <option value="TV">Transportes Verticais</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Nome da Local</label>                            
                            <input id="local" type="text" class="form-control" name="nome_local" required />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de Locais</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="localTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Linha</th>
                                <th>Esta��o</th>
                                <th>Grupo</th>
                                <th>Local</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tableLocal}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
