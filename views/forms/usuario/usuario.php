<div class="page-header">
    <h1>Cadastro de Usu�rios</h1>
    <small>Cadastro de Usu�rios.</small>
</div>

<div class="row">
    <form id="formCadUser" method="post" action="<?= "{$this->home_uri}/usuario/store" ?>" class="form-group">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <label> Dados do Usu�rio</label></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5">
                            <label>Nome Completo</label>
                            <input id="nome_completo" name="nome_completo" class="form-control" type="text" value="" required>
                        </div>
                        <div class="col-md-4">
                            <label>Email</label>
                            <input id="email" name="email" class="form-control" type="email" value="">
                        </div>
                        <div class="col-md-3">
                            <label>Nome de Login</label>
                            <input id="login_usuario" name="usuario" class="form-control" type="text" required />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>N�vel</label>
                            <select id="nivel_acesso" name="nivel" class="form-control" required>
                                <option disabled selected="true" value="">SELECIONE</option>
                                <option value="7">Engenharia</option>
                                <option value="7.1">Engenharia Supervis�o</option>
                                <option value="7.2">Engenharia Material Rodante</option>
                                <option value="6">Diretoria</option>
                                <option value="6.1">GESIV</option>
                                <option value="5.2">Equipe Manuten��o Material Rodante</option>
                                <option value="5.1">Equipe Supervis�o Material Rodante</option>
                                <option value="5">Equipe Manuten��o</option>
                                <option value="4">Materiais</option>
                                <option value="3">TI</option>
                                <option value="2.3">Usuario</option>
                                <option value="2.1">Supervis�o</option>
                                <option value="2">CMM</option>
                                <option value="1">RH</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Permiss�o de Acesso</label>
                            <input id="permissao" class="form-control" type="checkbox" name="permissao">
                        </div>
                        <div class="col-md-3" style="display: <?= $validaterCheck ?>">
                            <label>Validar Servi�os</label>
                            <input id="validater" class="form-control" type="checkbox" name="validater">
                        </div>
                        <div id="resetPassField" class="col-md-3" style="display: none">
                            <label>Resetar senha para padr�o: admin </label>
                            <input id="passwordReset" class="form-control" type="checkbox" name="passwordReset">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <label>Equipes</label>
                            <select id='selectEquipes' name="equipes[]"
                                    class="form-control multiselect"
                                    multiple="multiple" disabled>
                                <?php
                                $sql = "SELECT * FROM v_equipe_unidade ORDER BY cod_unidade, cod_equipe";

                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                $unidades = array();
                                $equipes = array();


                                foreach ($result as $key => $value) {
                                    $equipes[$value['cod_unidade']] .= "<option value='{$value['cod_un_equipe']}'>{$value["nome_equipe"]}</option>";
                                    $unidades["{$value["nome_unidade"]}"] = $equipes[$value['cod_unidade']];
                                }


                                foreach ($unidades as $nome_unidade => $equipes_unidade) {
                                    echo "<optgroup id='' label='$nome_unidade'>";

                                    echo "$equipes_unidade";

                                    echo "</optgroup>";
                                }

                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

<!-- TABELAS DE PMP�s POR LINHA-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Usu�rios Cadastrados</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!-- Abas  TUE e VLT -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#AbaEngenharias" aria-controls="profile" role="tab" data-toggle="tab">Engenharia</a></li>
                            <li role="presentation"><a href="#AbaMaterialRodante" aria-controls="profile" role="tab" data-toggle="tab">Material Rodante</a></li>
                            <li role="presentation"><a href="#AbaDiretoria" aria-controls="profile" role="tab" data-toggle="tab">Diretoria</a></li>
                            <li role="presentation"><a href="#AbaGESIV" aria-controls="profile" role="tab" data-toggle="tab">GESIV</a></li>
                            <li role="presentation"><a href="#AbaEquipeManutencao" aria-controls="profile" role="tab" data-toggle="tab">EquipeManutencao</a></li>
                            <li role="presentation"><a href="#AbaMateriais" aria-controls="profile" role="tab" data-toggle="tab">Materiais</a></li>
                            <li role="presentation"><a href="#AbaTI" aria-controls="profile" role="tab" data-toggle="tab">TI</a></li>
                            <li role="presentation"><a href="#AbaUsuario" aria-controls="profile" role="tab" data-toggle="tab">Usu�rio</a></li>
                            <li role="presentation"><a href="#AbaSupervisao" aria-controls="profile" role="tab" data-toggle="tab">Supervis�o</a></li>
                            <li role="presentation"><a href="#AbaCCM" aria-controls="profile" role="tab" data-toggle="tab">CCM</a></li>
                            <li role="presentation"><a href="#AbaRH" aria-controls="profile" role="tab" data-toggle="tab">RH</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- ABA ENGENHARIA -->
                            <div role="tabpanel" class="tab-pane fade in active" id="AbaEngenharias">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#AbaEngenharias_Engenharia" aria-controls="profile" role="tab" data-toggle="tab">Engenharia</a></li>
                                            <li role="presentation"><a href="#AbaEngenharias_Supervisao" aria-controls="profile" role="tab" data-toggle="tab">Supervis�o</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="AbaEngenharias_Engenharia">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="" class="table table-striped table-responsive tableUsuario">

                                                        <thead>
                                                        <tr>
                                                            <th>Nome Completo</th>
                                                            <th>Nome Login</th>
                                                            <th>Email</th>
                                                            <th>N�vel</th>
                                                            <th>Permiss�o</th>
                                                            <th>A��es</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT
                                                                cod_usuario, nome_completo, usuario, email, nivel, permissao, validater
                                                                FROM usuario
                                                                WHERE nivel = '7' ORDER BY usuario";

                                                        $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($usuarios_rh)) {

                                                            foreach ($usuarios_rh as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                                echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                                echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                                echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                                echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                                $sql = "SELECT cod_un_equipe
                                                                        FROM eq_us_unidade
                                                                        WHERE cod_usuario = {$value['cod_usuario']}";

                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                                $un_equipes_usuario = array();

                                                                foreach ($result as $key => $val) {
                                                                    array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                                }

                                                                $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                                echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
                                                                echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                                echo "</tr>";



                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AbaEngenharias_Supervisao">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="" class="table table-striped table-responsive tableUsuario">

                                                        <thead>
                                                        <tr>
                                                            <th>Nome Completo</th>
                                                            <th>Nome Login</th>
                                                            <th>Email</th>
                                                            <th>N�vel</th>
                                                            <th>Permiss�o</th>
                                                            <th>A��es</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT
                                                                cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                                FROM usuario
                                                                WHERE nivel = '7.1' ORDER BY usuario";

                                                        $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($usuarios_rh)) {

                                                            foreach ($usuarios_rh as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                                echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                                echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                                echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                                echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                                $sql = "SELECT cod_un_equipe
                                                                        FROM eq_us_unidade
                                                                        WHERE cod_usuario = {$value['cod_usuario']}";

                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                                $un_equipes_usuario = array();

                                                                foreach ($result as $key => $val) {
                                                                    array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                                }

                                                                $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                                echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
                                                                echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                                echo "</tr>";



                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- ABA MATERIAL RODANTE -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaMaterialRodante">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#AbaMaterialRodante_Engenharia" aria-controls="profile" role="tab" data-toggle="tab">Engenharia</a></li>
                                            <li role="presentation"><a href="#AbaMaterialRodante_EquipeManutencao" aria-controls="profile" role="tab" data-toggle="tab">Equipe Manuten��o</a></li>
                                            <li role="presentation"><a href="#AbaMaterialRodante_EquipeSupervisao" aria-controls="profile" role="tab" data-toggle="tab">Equipe Supervis�o</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="AbaMaterialRodante_Engenharia">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="" class="table table-striped table-responsive tableUsuario">

                                                        <thead>
                                                        <tr>
                                                            <th>Nome Completo</th>
                                                            <th>Nome Login</th>
                                                            <th>Email</th>
                                                            <th>N�vel</th>
                                                            <th>Permiss�o</th>
                                                            <th>A��es</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT
                                                                cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                                FROM usuario
                                                                WHERE nivel = '7.2' ORDER BY usuario";

                                                        $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($usuarios_rh)) {

                                                            foreach ($usuarios_rh as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                                echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                                echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                                echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                                echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                                $sql = "SELECT cod_un_equipe
                                                                        FROM eq_us_unidade
                                                                        WHERE cod_usuario = {$value['cod_usuario']}";

                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                                $un_equipes_usuario = array();

                                                                foreach ($result as $key => $val) {
                                                                    array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                                }

                                                                $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                                echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                                echo "</tr>";



                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AbaMaterialRodante_EquipeManutencao">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="" class="table table-striped table-responsive tableUsuario">

                                                        <thead>
                                                        <tr>
                                                            <th>Nome Completo</th>
                                                            <th>Nome Login</th>
                                                            <th>Email</th>
                                                            <th>N�vel</th>
                                                            <th>Permiss�o</th>
                                                            <th>A��es</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT
                                                                cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                                FROM usuario
                                                                WHERE nivel = '5.2' ORDER BY usuario";

                                                        $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($usuarios_rh)) {

                                                            foreach ($usuarios_rh as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                                echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                                echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                                echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                                echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                                $sql = "SELECT cod_un_equipe
                                                                        FROM eq_us_unidade
                                                                        WHERE cod_usuario = {$value['cod_usuario']}";

                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                                $un_equipes_usuario = array();

                                                                foreach ($result as $key => $val) {
                                                                    array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                                }

                                                                $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                                echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                                echo "</tr>";



                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="AbaMaterialRodante_EquipeSupervisao">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table id="" class="table table-striped table-responsive tableUsuario">

                                                        <thead>
                                                        <tr>
                                                            <th>Nome Completo</th>
                                                            <th>Nome Login</th>
                                                            <th>Email</th>
                                                            <th>N�vel</th>
                                                            <th>Permiss�o</th>
                                                            <th>A��es</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="bodyHistoricoInfo">
                                                        <?php

                                                        $sql = "SELECT
                                                                cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                                FROM usuario
                                                                WHERE nivel = '5.1' ORDER BY usuario";

                                                        $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                        if (!empty($usuarios_rh)) {

                                                            foreach ($usuarios_rh as $key => $value) {

                                                                echo "<tr>";
                                                                echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                                echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                                echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                                echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                                echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                                $sql = "SELECT cod_un_equipe
                                                                        FROM eq_us_unidade
                                                                        WHERE cod_usuario = {$value['cod_usuario']}";

                                                                $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                                $un_equipes_usuario = array();

                                                                foreach ($result as $key => $val) {
                                                                    array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                                }

                                                                $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                                echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                                echo "</tr>";



                                                            }

                                                        }

                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- ABA DIRETORIA 6 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaDiretoria">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '6' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA GESIV 6.1 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaGESIV">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '6.1' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA EQUIPE MANUTENCAO 5 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaEquipeManutencao">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '5' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA MATERIAIS 4 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaMateriais">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '4' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA TI 3 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaTI">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '3' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";

                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA USUARIO 2.3 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaUsuario">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '2.3' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA SUPERVISAO 2.1 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaSupervisao">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '2.1' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA CCM 2 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaCCM">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '2' ORDER BY usuario";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";



                                                }

                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- ABA RH 1 -->
                            <div role="tabpanel" class="tab-pane fade" id="AbaRH">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <table id="" class="table table-striped table-responsive tableUsuario">

                                            <thead>
                                            <tr>
                                                <th>Nome Completo</th>
                                                <th>Nome Login</th>
                                                <th>Email</th>
                                                <th>N�vel</th>
                                                <th>Permiss�o</th>
                                                <th>A��es</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodyHistoricoInfo">
                                            <?php

                                            $sql = "SELECT
                                                    cod_usuario, nome_completo, usuario, email, nivel, permissao, validater 
                                                    FROM usuario
                                                    WHERE nivel = '1' ORDER BY usuario ";

                                            $usuarios_rh = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                            if (!empty($usuarios_rh)) {

                                                foreach ($usuarios_rh as $key => $value) {

                                                    echo "<tr>";
                                                    echo "<td data-value='{$value['nome_completo']}'>{$value['nome_completo']}</td>";
                                                    echo "<td data-value='{$value['usuario']}'>{$value['usuario']}</td>";
                                                    echo "<td data-value='{$value['email']}'>{$value['email']}</td>";
                                                    echo "<td data-value='{$value['nivel']}'>{$value['nivel']}</td>";
                                                    echo "<td data-value='{$value['permissao']}'>{$value['permissao']}</td>";

                                                    $sql = "SELECT cod_un_equipe
                                                            FROM eq_us_unidade
                                                            WHERE cod_usuario = {$value['cod_usuario']}";

                                                    $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

                                                    $un_equipes_usuario = array();

                                                    foreach ($result as $key => $val) {
                                                        array_push($un_equipes_usuario, $val['cod_un_equipe']);
                                                    }

                                                    $un_equipes_usuario = json_encode($un_equipes_usuario);
                                                    echo "<td><button type='button' data-value='{$value['cod_usuario']}' data-grupo='{$value['cod_usuario']}' data-validaters='{$value['validater']}' data-un_equipes='{$un_equipes_usuario}' title='Editar Usuario' class='btn btn-default btn-circle editUsuario'><i class='fa fa-file-o fa-fw'></i></button>";
echo "<button type='button' data-value='{$value['cod_usuario']}' title='resetar Senha' class='btn btn-default btn-circle resetarSenha'><i class='fa fa-key fa-fw'></i></button></td>";
                                                    echo "</tr>";
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>