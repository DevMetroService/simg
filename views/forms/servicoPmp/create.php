<?php

$grupo = $this->medoo->select("grupo" , ["cod_grupo" , "nome_grupo"]);
$optionGrupo;
foreach ($grupo as $dados => $value) {
    $optionGrupo .= "<option value='{$value["cod_grupo"]}'>{$value["nome_grupo"]}</option>";
}

$tableServico;
foreach ($servico_pmp as $dados => $value) {
    $tableServico .= "<tr>";
    $tableServico .= "<td>{$value['cod_servico_pmp']}</td>";
    $tableServico .= "<td>{$value['nome_grupo']}</td>";
    $tableServico .= "<td data-value='{$value['cod_sub_sistema']}'>{$value['nome_sistema']}</td>";
    $tableServico .= "<td data-value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</td>";
    $tableServico .= "<td>{$value['nome_servico_pmp']}</td>";
    $tableServico .= "<td><button type='button' title='Editar Amv' data-codigo='{$value['cod_servico_pmp']}' class='btn btn-default btn-circle editServico'><i class='fa fa-file-o fa-fw'></i></button>";
    $tableServico .= "</tr>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de Servi�os de PMP</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Cadastro de Servi�os</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/servicoPmp/store" method="post">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Grupo de Sistema</label>
                            <select id="grupo" name="cod_grupo" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                                {$optionGrupo}
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Sistema</label>
                            <select id="sistema" name="cod_sistema" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Subsistema</label>
                            <select id="subsistema" name="cod_subsistema" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Nome do Servi�o</label>                            
                            <input id="servico" type="text" class="form-control" name="nome_servico_pmp" required/>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de AMV's</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="amvTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Grupo de Sistema</th>
                                <th>Sistema</th>
                                <th>Subsistema</th>
                                <th>Nome do Servi�o de PMP</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tableServico}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
