<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Cadastro de Material</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">

						<form id="formulario" class="form-group" action="<?= "{$this->home_uri}/material/update/{$material['cod_material']}" ?>"
                            method="post">
							<div class="row">
								<div class="col-md-6">
									<label>Nome</label>
									<input type="text" class="form-control" name="nome_material"
									placeholder="Digite somente o nome do material, sem marca ou medida: Ex: Cabo de rede"
									value="<?= $material['nome_material']?>"
									>
								</div>
								<div class=" col-md-2">
									<label>Estoque Atual</label>
									<input class="form-control" name="quantidade" value="<?= $material['quantidade']?>" required>
								</div>
								<div class=" col-md-2">
									<label>Valor Unit�rio</label>
									<input class="form-control float" name="valor_unitario" value="<?= $material['valor_unitario']?>" required>
								</div>
								<div class=" col-md-2">
									<label>Ativo</label>
									<select class="form-control" name="ativo" required>
										<option value="s" <?= ($material['ativo']=='s') ? "selected" : "" ?> >Ativado</option>
										<option value="n" <?= ($material['ativo']=='n') ? "selected" : "" ?> >Desativado</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label>Unidade de Medida</label>
									<select name="cod_uni_medida" class="form-control"
										required data-validation-required-message=" Campo precisa ser preenchido"
										data-bv-row=".col-lg-4"
										data-fv-notempty="true"
										data-fv-notempty-message="Selecione a unidade de medida.">

										<option selected="selected" value="" disabled="disabled">Unidade de Medida</option>

										<?php
										$consultaUnidadeMedida = $this->medoo->select("unidade_medida", "*", ["ORDER" => "nome_uni_medida"]);

										foreach ($consultaUnidadeMedida as $key => $value) {
											if($material['cod_uni_medida'] == $value['cod_uni_medida'])
												echo "<option value='{$value['cod_uni_medida']}' selected>{$value['sigla_uni_medida']} - {$value['nome_uni_medida']}</option>";
											else
												echo "<option value='{$value['cod_uni_medida']}'>{$value['sigla_uni_medida']} - {$value['nome_uni_medida']}</option>";
										}
										?>
									</select>

								</div>
								<div class="col-md-2">
	                <label>Quantidade M�nima</label>
	                <input type="text" name="quant_min" class="form-control float" title="Quantidade m�nima para controle de estoque" required
									value="<?= $material['quant_min']?>">
								</div>
								<div class="col-md-3" id="selectRefreshModal_2">
		                <div id="divRefreshedModal_2">
		                    <label>Categoria</label>

		                    <div class="input-group">
								<select name="cod_categoria" class="form-control"
												required data-validation-required-message=" Campo precisa ser preenchido"
												data-bv-row=".col-lg-4"
												data-fv-notempty="true"
												data-fv-notempty-message="Selecione a Categoria.">
												<option selected="selected" value="" disabled="disabled">Categoria</option>
									<?php
									$consultaCategoriaMedida = $this->medoo->select("categoria_material", ["cod_categoria", "nome_categoria"], ["ORDER" => "nome_categoria",]);

									foreach ($consultaCategoriaMedida as $key => $value) {
										if($material['cod_categoria'] == $value['cod_categoria'])
										echo "<option value='{$value['cod_categoria']}' selected>{$value['nome_categoria']}</option>";
										else
										echo "<option value='{$value['cod_categoria']}' >{$value['nome_categoria']}</option>";
									}
									?>
								</select>
		                        <span class="input-group-btn">
                         			<button class="btn btn-default" type="button" data-toggle="modal" data-target="#categoriaModal"><i class="fa fa-plus fa-fw"></i></button>
		                        </span>
		                    </div>
		                </div>
								</div>
								<div class="col-md-4" id="selectRefreshModal_1">
                    <div id="divRefreshedModal_1">

                        <label for="marcaMaterial">Marca</label>

                        <div class="input-group" >
													<select name="cod_marca" id="marcaMenu" class="form-control"
																	required data-validation-required-message=" Campo precisa ser preenchido"
																	data-bv-row=".col-lg-4"
																	data-fv-notempty="true"
																	data-fv-notempty-message="Selecione a unidade de medida.">

																	<option selected="selected" value="" disabled="disabled">Marca</option>
                            <?php
                            $consultaMarcaMaterial = $this->medoo->select("marca_material", ["nome_marca", "cod_marca"], ["ORDER" => "nome_marca",]);

                            foreach ($consultaMarcaMaterial as $key => $value) {
															if($material['cod_marca'] == $value['cod_marca'])
                                echo "<option value={$value['cod_marca']} selected>{$value['nome_marca']}</option>";
															else
																echo "<option value={$value['cod_marca']} >{$value['nome_marca']}</option>";
                            }

                            ?>
														</select>
                            <span class="input-group-btn">
                                     <button class="btn btn-default" type="button" data-toggle="modal" data-target="#marcaModal"><i class="fa fa-plus fa-fw"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
							</div>

              <div class="row">
									<div class="col-md-6">
											<label>C�digo de Barras (EAN)</label>
											<input type="text" name="ean" class="form-control" value="<?= $material['ean']?>">
									</div>
									<div class="col-md-3">
                      <label>Grupo de Sistema</label>
                      <select class="form-control" name="cod_grupo"
											required data-validation-required-message=" Campo precisa ser preenchido"
											data-bv-row=".col-lg-4"
											data-fv-notempty="true"
											data-fv-notempty-message="Selecione a unidade de medida.">
												<option selected disabled> Selecione o Grupo</option>

												<?php
												$grupo = $this->medoo->select("grupo", "*", ["ORDER" => "nome_grupo"]);

												foreach($grupo as $dados=>$value)
												{
													if($material['cod_grupo'] == $value['cod_grupo'])
														echo ("<option value='{$value['cod_grupo']}' selected>{$value['nome_grupo']}</option>");
													else
														echo ("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
												}
												?>
											</select>
                  </div>
									<div class="col-md-3">
                      <label>Tipo de Material</label>
                      <select class="form-control" name="cod_tipo_material"
											required data-validation-required-message=" Campo precisa ser preenchido"
											data-bv-row=".col-lg-4"
											data-fv-notempty="true"
											data-fv-notempty-message="Selecione a unidade de medida.">
												<option selected disabled> Selecione o tipo de material</option>

												<?php
												$tipoMaterial = $this->medoo->select("tipo_material", "*", ["ORDER" => "nome_tipo_material"]);

												foreach($tipoMaterial as $dados=>$value)
												{
													if($material['cod_tipo_material'] == $value['cod_tipo_material'])
														echo ("<option value='{$value['cod_tipo_material']}' selected >{$value['nome_tipo_material']}</option>");
													else
														echo ("<option value='{$value['cod_tipo_material']}'>{$value['nome_tipo_material']}</option>");
												}
												?>
											</select>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <label>Descri��o</label>
                      <input type="text" name="descricao" class="form-control"
											value="<?= $material['descricao_material']?>">
                  </div>
              </div>
							<div class="row" style="margin-top: 2em">
								<div class="col-md-4">
									<button type="submit" class="btn btn-success btn-lg">Atualizar Material</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<!-- Modals -->
    <!-- Marca  -->
<div class="modal fade" id="marcaModal" tabindex="-1" role="form" aria-labelledby="marcaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="marcaModalLabel">Cadastrar Marca</h4>
            </div>

            <form id="formularioModal_1" method="post">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome da marca:</label>
                            <input type="text" name="nomeMarca" class="form-control"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Selecione a unidade de medida.">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Marca</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>

        </div>
    </div>
</div>

    <!-- Categoria -->
<div class="modal fade" id="categoriaModal" tabindex="-1" role="form" aria-labelledby="categoriaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="categoriaModalLabel">Cadastrar Marca</h4>
            </div>

            <form id="formularioModal_2" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome da Categoria:</label>
                            <input type="text" name="nomeCategoria" class="form-control"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Selecione a unidade de medida.">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Descri��o:</label>
                           <textarea class="form-control" name="descricaoCategoria"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Material</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
