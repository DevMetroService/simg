<?php

$linha = $this->medoo->select("linha" , ["cod_linha" , "nome_linha"]);
$optionLinha;
foreach ($linha as $dados => $value) {
    $optionLinha .= "<option value='{$value["cod_linha"]}'>{$value["nome_linha"]}</option>";
}

$tableAMV;
foreach ($amv as $dados => $value) {
    $tableAMV .= "<tr>";
    $tableAMV .= "<td>{$value['cod_amv']}</td>";
    $tableAMV .= "<td data-value='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
    $tableAMV .= "<td data-value='{$value['cod_estacao']}'>{$value['nome_estacao']}</td>";
    $tableAMV .= "<td>{$value['nome_amv']}</td>";
    $tableAMV .= "<td><button type='button' title='Editar Amv' data-codigo='{$value['cod_amv']}' class='btn btn-default btn-circle editAmv'><i class='fa fa-file-o fa-fw'></i></button>";
    $tableAMV .= "</tr>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de AMV</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Cadastro de AMV's</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/amv/store" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Linha</label>
                            <select id="linha" name="cod_linha" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                                {$optionLinha}
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Esta��o</label>
                            <select id="estacao" name="cod_estacao" class="form-control" required>
                                <option value="">Selecione linha para filtrar</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Nome do AMV</label>                            
                            <input id="amv" type="text" class="form-control" name="nome_amv" required/>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de AMV's</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="amvTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Linha</th>
                                <th>Esta��o</th>
                                <th>Nome do AMV</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tableAMV}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
