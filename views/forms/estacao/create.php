<?php

$linha = $this->medoo->select("linha", "*");
$optionLinha="";
foreach ($linha as $dados=>$value){
    $optionLinha .= "<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>";
}

$tableEstacao;
foreach ($estacao as $dados => $value) {
    $tableEstacao .= "<tr>";
    $tableEstacao .= "<td>{$value['cod_estacao']}</td>";
    $tableEstacao .= "<td data-linha='{$value['cod_linha']}'>{$value['nome_linha']}</td>";
    $tableEstacao .= "<td data-value='{$value['cod_estacao']}'>{$value['nome_estacao']}</td>";
    $tableEstacao .= "<td><button type='button' title='Editar Esta��o' data-codigo='{$value['cod_estacao']}' class='btn btn-default btn-circle editEstacao'><i class='fa fa-file-o fa-fw'></i></button>";
    $tableEstacao .= "</tr>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de Esta��o</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Dados de Esta��es</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/estacao/store" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Linha</label>
                            <select id="linha" name="cod_linha" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                                {$optionLinha}
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Nome da Esta��o</label>                            
                            <input id="estacao" type="text" class="form-control" name="nome_estacao" required />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de Esta��es</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="estacaoTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Linha</th>
                                <th>Esta��o</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tableEstacao}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
