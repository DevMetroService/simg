<?php

$tableProcedimento;

foreach ($procedimento as $dados => $value) {
    $tableProcedimento .= "<tr>";
    $tableProcedimento .= "<td>{$value['cod_procedimento']}</td>";
    $tableProcedimento .= "<td data-value='{$value['cod_grupo']}'>{$value['nome_grupo']}</td>";
    $tableProcedimento .= "<td>{$value['nome_procedimento']}</td>";
    $tableProcedimento .= "<td><button type='button' title='Editar Linha' data-codigo='{$value['cod_procedimento']}' class='btn btn-default btn-circle editProcedimento'><i class='fa fa-file-o fa-fw'></i></button>";
    $tableProcedimento .= "</tr>";
}

$grupo = $this->medoo->select("grupo", "*");
$groupOption = "";
foreach ($grupo as $dados => $value) {
    $groupOption .= "<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de Procedimentos</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Dados de Procedimentos</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/procedimento/store" method="post">
                    <div class="row">
                         <div class="col-md-4">
                            <label>Grupo de Sistema</label>                            
                            <select id="grupo" class="form-control" name="cod_grupo" required>
                                <option value="">Selecione a op��o</option>
                                {$groupOption}
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Nome do Procedimento</label>                            
                            <input id="procedimento" type="text" class="form-control" name="nome_procedimento" required />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de Linhas</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="amvTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Grupo</th>
                                <th>Procedimento</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tableProcedimento}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
