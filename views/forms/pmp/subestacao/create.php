<?php
require_once(ABSPATH . "/views/_includes/_header.php");
require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
require_once(ABSPATH . "/views/_includes/_body.php");

$linhas = $this->medoo->select("linha", "*");
?>
<div class="page-header">
    <h1>Gerador PMP - Subesta��o</h1>
</div>

<div class="panel panel-primary">
    <div class="panel-heading"><label>PMP - Subesta��o</label></div>

    <div class="panel-body">
        <div class="row">
            <?php echo moduloIdentificacaoGeral($refill, $sistemaSb, $subsistemaSb, $servicoPmpSb, $periodicidadeSb); ?>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Local (ais)</label></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Selecione</label>
                                <select name="local[]" class="form-control multiselect" multiple="multiple">
                                <?php
                                    foreach ($linhas as $dados=>$value){
                                        $pmp_estacao = $this->medoo->select('local', '*', 
                                        [
                                            "AND" =>
                                            [
                                                'cod_linha' => $value['cod_linha'], 
                                                'grupo' => ['S','']
                                                ]
                                        ]);
                                            
                                        echo var_dump($pmp_estacao);

                                        $options = "";
                                        foreach ($pmp_estacao as $data => $valor) {
                                            $options .= "<option value='{$valor['cod_local']}'>{$valor['nome_local']}</option>";
                                        }

                                        if($options != "")
                                        {
                                            echo("<optgroup label='{$value['nome_linha']}'></optgroup>");
                                            echo($options);
                                        }
                                    }
                                    ?>
                                </select>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
                <?php echo moduloRecursosHH($refill, 6); ?>
        </div>

        <div class="row">
            <div class="col-md-1">
                <div class="btn-group">
                    <button type="button" aria-label="right align" name="btnPlus" class="btn btn-default btn-lg">
                        <i class="fa fa-plus fa-2x"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 20px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Linhas Geradas</label></div>

                    <div class="panel-body over-x">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="tabelaPmpSb" class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Sistema</th>
                                        <th>SubSistema</th>
                                        <th>Servi�o</th>
                                        <th>Periodicidade</th>
                                        <th>Procedimento</th>
                                        <th>Qzn In�cio</th>
                                        <th>Local</th>
                                        <th>Turno</th>
                                        <th>M�o de obra</th>
                                        <th>Horas �teis</th>
                                        <th>HH</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Pmp - Armazenados</label></div>

                    <div class="panel-body over-x">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="tabelaPmpSbExec" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Cod</th>
                                        <th>Sistema</th>
                                        <th>SubSistema</th>
                                        <th>Servi�o</th>
                                        <th>Periodicidade</th>
                                        <th>Procedimento</th>
                                        <th>Qzn In�cio</th>
                                        <th>Linha</th>
                                        <th>Local</th>
                                        <th>Turno</th>
                                        <th>M�o de obra</th>
                                        <th>Horas �teis</th>
                                        <th>HH</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ctdDb = 1;
                                    if (!empty($pmpSubestacao)) {
                                        foreach ($pmpSubestacao as $dados) {
                                            if ($dados['turno'] == 'D') {
                                                $turno = 'Diurno';
                                            } else {
                                                $turno = 'Noturno';
                                            }

                                            echo '<tr>';
                                            echo("<td>{$dados['cod_pmp']}</td>");//<input type='hidden' name='codPmpSb{$ctdDb}' value='{$dados['cod_pmp']}'></td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_sistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_subsistema']}</td>");
                                            echo("<td>{$servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']]['nome_servico_pmp']}</td>");
                                            echo("<td>{$periodicidade[$dados['cod_tipo_periodicidade']]}</td>");
                                            echo("<td>{$procedimento[$dados['cod_procedimento']]}</td>");
                                            echo("<td>{$dados['quinzena']}</td>");
                                            echo("<td>{$local[$dados['cod_local']]['nome_linha']}</td>");
                                            echo("<td>{$local[$dados['cod_local']]['nome_local']}</td>");
                                            echo("<td>{$turno}</td>");
                                            echo("<td>{$dados['mao_obra']}</td>");
                                            echo("<td>{$dados['horas_uteis']}</td>");
                                            echo("<td>{$dados['homem_hora']}</td>");
                                            echo("<td><input type='checkbox' value='{$dados['cod_pmp']}' name='excluirLinhaPmpBanco{$ctdDb}' id='excluirLinhaPmpBanco{$ctdDb}' class='form-control'><label for='excluirLinhaPmpBanco{$ctdDb}'>Excluir</label></td>");
                                            echo '</tr>';
                                            $ctdDb++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php if(!empty($this->gerarPmpAnual)){ ?>
    <div class="col-md-2">
        <div class="panel panel-primary">
            <div class="panel-heading">Gerar CRONOGRAMA</div>

            <div class="panel-body over-x">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">
                            <div class="btn-group">
                                <button type="button" aria-label="right align" class="btn btn-success btn-lg" data-toggle="modal" data-target="#gerarCronograma" title="Autorizar PMP Anual">
                                    <i class="fa fa-calendar-check-o fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-offset-8 col-md-2">
    <?php }else{?>
    <div class="col-md-offset-10 col-md-2">
    <?php } ?>
        <div class="panel panel-primary">
            <div class="panel-heading">A��es</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group  btn-group-justified " role="group">
                            <div class="btn-group">
                                <button type="button" aria-label="right align" class="btn btn-default btn-lg salvarListaPmp" title="Salvar">
                                    <i class="fa fa-floppy-o fa-2x"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="gerarCronograma" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3 class="modal-title">GERAR CRONOGRAMA</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>ANO
                            <button data-toggle='tooltip' data-placement='top' title='Ano da gera��o do cronograma. Por seguran�a, quando for o ano vigente, n�o ser� permitido a gera��o de planos <strong>ATIVOS</strong>.'>
                                <i id="questionMaterial" class="fa fa-question-circle"></i>
                            </button>
                        </label>
                        <select id="pmpAno" class="form-control" name="pmpAno">
                        <?php 
                            $year = date('Y', time()); 
                            echo ("<option value='{$year}'>{$year} (ATUAL)</option>");
                            $year++;
                            echo ("<option value='{$year}'>{$year}</option>");
                        ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label style="display: block !important; text-align: center;">ATIVOS
                            <button data-toggle='tooltip' data-placement='top' title='Recadastra os itens do PMP j� ativos em cronograma.'>
                                <i id="questionMaterial" class="fa fa-question-circle"></i>
                            </button>
                        </label>
                        <input id="pmpAtivo" class="form-control" name="pmpAtivo" type="checkbox"  disabled/>
                    </div>
                    <div class="col-md-4">
                        <label style="display: block !important; text-align: center;">NOVOS
                            <button data-toggle='tooltip' data-placement='top' title='Cadastra somente os NOVOS itens do PMP que ainda n�o foram ativos em cronograma.'>
                                <i id="questionMaterial" class="fa fa-question-circle"></i>
                            </button>
                        </label>
                        <input id="pmpNovo" class="form-control" name="pmpNovo" type="checkbox"/>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success gerarPmpAnual" data-dismiss="modal">Gerar CRONOGRAMA</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


<?php 
echo "<input value='{$ctdDb}' name='ctd' type='hidden'>"; 

require_once(ABSPATH . "/views/_includes/_footer.php");
?>
