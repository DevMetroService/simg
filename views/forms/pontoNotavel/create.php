<?php

$linha = $this->medoo->select("linha", "*");
$optionLinha="";
foreach ($linha as $dados=>$value){
    $optionLinha .= "<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>";
}

$tablePontoNotavel;
foreach ($pontoNotavel as $dados => $value) {
    $tablePontoNotavel .= "<tr>";
    $tablePontoNotavel .= "<td>{$value['cod_ponto_notavel']}</td>";
    $tablePontoNotavel .= "<td data-value='{$value['cod_trecho']}' data-linha='{$value['cod_linha']}'>{$value['descricao_trecho']}</td>";
    $tablePontoNotavel .= "<td data-value='{$value['cod_ponto_notavel']}'>{$value['nome_ponto']}</td>";
    $tablePontoNotavel .= "<td><button type='button' title='Editar Ponto Notavel' data-codigo='{$value['cod_ponto_notavel']}' class='btn btn-default btn-circle editPontoNotavel'><i class='fa fa-file-o fa-fw'></i></button>";
    $tablePontoNotavel .= "</tr>";
}



echo <<<HTML

<div class="page-header">
    <h1>Ger�ncia de dados de Pontos Not�veis</h1>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Dados de Pontos Not�veis</div>

			<div class="panel-body">
                <form id="formulario" class="form-group" action="{$this->home_uri}/pontoNotavel/store" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Linha</label>
                            <select id="linha" name="cod_linha" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                                {$optionLinha}
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Trecho</label>
                            <select id="trecho" name="cod_trecho" class="form-control" required>
                                <option value="">Selecione a op��o</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Nome do Ponto Not�vel</label>                            
                            <input id="pontoNotavel" type="text" class="form-control" name="nome_ponto" required />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-4">
                            <button id="btnSubmit" type="submit" class="btn btn-default btn-lg">
                                <i class="fa fa-save fa-fw"></i> <span id="labelBtn">Salvar</span>
                            </button>
                            <button id="resetBtn" type="reset" class="btn btn-default btn-lg">Limpar Campos</button>
                        </div>
                    </div>
                </form>
			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Lista de Pontos Not�veis</div>

			<div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="amvTable" class="table table-striped table-responsive tableUsuario">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Trecho</th>
                                <th>Nome Ponto Not�vel</th>
                                <th>A��es</th>
                            </tr>
                            </thead>
                            <tbody>
                                {$tablePontoNotavel}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

		</div>
	</div>
</div>

HTML;
