<div class="row" xmlns="http://www.w3.org/1999/html">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Cadastro de Categorias</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<form method="post" class="form-group" action="<?= "{$this->home_uri}/categoria/store" ?>" role="form">
							<div class="row">
								<div class="col-md-4">
									<label>Nome da Categoria</label>
									<input type="text" class="form-control" name="nome_categoria">
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<label>Descri��o</label>
									<textarea rows="2" class="form-control" name="descricao"></textarea>
								</div>
							</div>

							</br>
							<div class="row">
								<div class="col-md-4">
									<button type="submit" class="btn btn-success btn-lg">Cadastrar Categoria</button>
								</div>

								<div class="col-md-4">
									<button type="reset" class="btn btn-warning btn-lg">Zerar Campos</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
