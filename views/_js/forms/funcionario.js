$(function () {
    var cpf_cnpj = $('#cpf_cnpj');
    cpf_cnpj.blur(function(){
        $.get(caminho+'/funcionario/hasSameCPF/'+ cpf_cnpj.val(),
            function(dados){
                if(cpf_cnpj.val().length == 14){
                    if(dados.nome_funcionario)
                    {
                        alertaJquery("CPF DUPLICADO", 
                        "CPF ou CNPJ Informado j� se encontra cadastrado em nosso banco de dados atrav�s do colaborador: "+dados.nome_funcionario, 
                        "info");
                        cpf_cnpj.val("");
                    } 
                }
            }
            , 'json');
    });

    var tipoFuncionario = $('select[name="cod_tipo_funcionario"]');
    tipoFuncionario.on("change",function(){
        if($(this).val() == '2'){
            $('.centro_lotacao').hide();
            $('select[name="cod_centro_resultado"]').removeAttr("required");
            $('select[name="cod_unidade"]').removeAttr("required");
        }else{
            $('.centro_lotacao').show();
            $('select[name="cod_centro_resultado"]').attr("required", "required");
            $('select[name="cod_unidade"]').attr("required", "required");
        }
    });
    
    if(tipoFuncionario.val() == '2')
    {
        $('.centro_lotacao').hide();
        $('select[name="cod_centro_resultado"]').removeAttr("required");
        $('select[name="cod_unidade"]').removeAttr("required");
    }else{
        $('.centro_lotacao').show();
        $('select[name="cod_centro_resultado"]').attr("required", "required");
        $('select[name="cod_unidade"]').attr("required", "required");
    }
});


  