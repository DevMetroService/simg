$(document).ready(function() {
    $codSaf = $("#codSaf");
    $('#cancelBtn').click(function(e) {
        e.preventDefault();

        if(!$codSaf.val()){
            alertaJquery("Aten��o","N�mero de SAF precisa ser informado", "error");
            return;
        }

        if (hasProcess(e)) return;

        alertaJquery("Cancelamento de SAF", "Ao cancelar a SAF, tamb�m estar� cancelando todas as sua SSM's e OSM's. Este processo n�o tem volta. </br> Deseja continuar?", 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "Sim") {
                $('#formCancel').submit();
            }
            if (res == "N�o") {
                $('body').removeClass('processing');
            }
        });
    });
});