tableSSM = $('#tableSSM').DataTable(configTable(0, false, 'desc'));

var grupo = $('#grupo');
var linha = $('#linha');
var sistema = $('#sistema');
var trecho = $('#trecho');
var form = $('#reworkForm');

grupo.change(function(){
  apiGrupoSistema(grupo, sistema);
});

linha.change(function(){
  apiLinhaTrecho(linha, trecho);
});

grupo.focus();

$(function () {
  $('[data-toggle="popover"]').popover( {html:true})
})

$(document).on("click",".clickPop", function(e){
  $(this).popover(
    {html:true}
  );
  $(this).popover('show');
});

$('#btnPrint').click(function()
{ 
  var action = form.attr('action');

  form.attr('target', '_blank');
  form.attr('action', caminho+'/report/printReport/reworkReport');
  form.submit();

  form.attr('action', action);
  form.removeAttr('target');
});

//Fun��o do bot�o de imprimir SAF
$(document).on("click", ".btnPrintSaf", function () {
  var codigoSaf = $(this).html();
  
  $.ajax({
      type: "POST",
      url: caminho + "/cadastroGeral/imprimirSaf",
      data: {codigoSaf: codigoSaf},
      success: function () {
          window.open(caminho + "/dashboardGeral/printSaf", "_blank");
      }
  });
});
