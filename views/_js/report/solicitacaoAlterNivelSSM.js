var tableSSM;

tableSSM = $('#tableSSM').DataTable(configTable(0, false, 'desc'));

$(document).on("click",".btnEditarSsm", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsm",
        data: {codigoSsm: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/ssm");
        }
    });
});
