/**
 * Created by josue.marques on 11/02/2016.
 *
 */

$('document').ready(function(){
    $('input').prop('readonly', true);
    $('select').prop('disabled', true);
    $('textarea').prop('readonly', true);

    $('.free').removeAttr('disabled');
    $('.free').removeAttr('readonly');
});