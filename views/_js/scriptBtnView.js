var urlAction;
var codigo;
var acao;

$(".btnAutorizarSaf").on("click", function(){
    var dados = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/gerarSsm/",
        data: {codigoSaf: dados},
        success: function() {
            window.close();
        }
    });
});

$(".btnDevolverSaf").on("click", function(){
    var dados = $(this).val();

    urlAction = caminho + "/cadastroGeral/devolverCancelarSaf/";
    codigo = dados;
    acao = "devolver";

    $('.modal-container').css("display","block");
});

$(".btnCancelarSaf").on("click", function(){
    var dados = $(this).val();

    urlAction = caminho + "/cadastroGeral/devolverCancelarSaf/";
    codigo = dados;
    acao = "cancelar";

    $('.modal-container').css("display","block");
});

$(".btnGerarOsm").on("click", function(){
    var dados = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/gerarOsm/",
        data: {codigoSsm: dados, viewSsm: true},
        success: function() {
            window.close();
        }
    });
});

$(".btnDevolverSsm").on("click", function(){
    var dados = $(this).val();

    urlAction = caminho + "/cadastroGeral/devolverCancelarSsm/";
    codigo = dados;
    acao = "devolver";

    $('.modal-container').css("display","block");
});

//btnCancelarSsm

$(document).on("click",".ok-modal-container", function(){
    var motivo = $('.body-modal textarea').val();

    if(motivo != "") {
        $.ajax({
            type: "POST",
            url: urlAction,
            data: {codigo: codigo, motivoAcao: motivo, acaoModal: acao},
            success: function () {
                window.close();
            }
        });
    }else{
        alert('Informe o motivo da a��o!');
    }
});

$(document).on("click",".close-modal-container", function(){
    $('.body-modal textarea').val("");
    $('.modal-container').css("display","none");
});