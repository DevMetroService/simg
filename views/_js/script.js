


// Verificar existencia de Notificação/RealTime
$('document').ready(function () {
    $.ajax({
        dataType: "json",
        url: caminho + "/notificacao/feedBackBD",
        success: function (json) {
            if (json.feedBack) {
                alertaJquery(json.titulo, json.mensagem, json.tipo);
            }
        }
    });
});

//####################### função sidebar de colapse ##############################
//####################### função sidebar de colapse ##############################
//####################### função sidebar de colapse ##############################
$(function () {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function () {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })
});


//################################# Escolaridade #################################
//################################# Escolaridade #################################
//################################# Escolaridade #################################
if ($('select[name="escolaridadeFuncionario"]').length) {
    switch ($('select[name="escolaridadeFuncionario"] option:selected').val()) {				//A cada função é mostrado a janela de acordo.
        case "2":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("cursando").style.display = "block";
            document.getElementById("escolaTecnica").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            break;

        case "4":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("cursando").style.display = "block";
            document.getElementById("escolaTecnica").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            break;

        case "5":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("escolaTecnica").style.display = "block";
            document.getElementById("cursando").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            break;

        case "6":
            document.getElementById("semestre").style.display = "block";
            document.getElementById("escolaTecnica").style.display = "block";
            document.getElementById("superiorFuncionario").style.display = "block";
            document.getElementById("cursando").style.display = "block";
            break;

        case "7":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("escolaTecnica").style.display = "block";
            document.getElementById("superiorFuncionario").style.display = "block";
            document.getElementById("cursando").style.display = "none";
            break;

        default:
            document.getElementById("semestre").style.display = "none";
            document.getElementById("escolaTecnica").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            document.getElementById("curso").style.display = "none";
            document.getElementById("cursando").style.display = "none";
            $("#cursoNao").prop("checked", true);
            break;
    }
}

$('select[name="escolaridadeFuncionario"]').on("change", function () {							//Dinâmica para visualizar campos de entrada para Ensino Superior e/ou Curso Técnico.
    switch ($('select[name="escolaridadeFuncionario"] option:selected').val()) {				//A cada função é mostrado a janela de acordo.
        case "2":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("cursando").style.display = "block";
            document.getElementById("escolaTecnica").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            break;

        case "4":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("cursando").style.display = "block";
            document.getElementById("escolaTecnica").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            break;

        case "5":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("escolaTecnica").style.display = "block";
            document.getElementById("cursando").style.display = "none";
            break;

        case "6":
            document.getElementById("semestre").style.display = "block";
            document.getElementById("escolaTecnica").style.display = "block";
            document.getElementById("superiorFuncionario").style.display = "block";
            document.getElementById("cursando").style.display = "block";
            break;

        case "7":
            document.getElementById("semestre").style.display = "none";
            document.getElementById("escolaTecnica").style.display = "block";
            document.getElementById("superiorFuncionario").style.display = "block";
            document.getElementById("cursando").style.display = "none";
            break;

        default:
            document.getElementById("semestre").style.display = "none";
            document.getElementById("escolaTecnica").style.display = "none";
            document.getElementById("superiorFuncionario").style.display = "none";
            document.getElementById("curso").style.display = "none";
            document.getElementById("cursando").style.display = "none";
            $("#cursoNao").prop("checked", true);
            break;
    }

})

//############################## Curso Tecnico ##################################

$('input[name="cursoTecnicoSimples"]').on("change", function () {										// Função para exibir div de campos para quem tem curso Técnico
    if(this.checked){
        $('#divNomeCursoTecnico').show();
    }else{
        $('#divNomeCursoTecnico').hide();
        $('input[name="nomeCursoTecnico"]').val('');
    }
});

//############################### Check Naturalidade e Estrangeiro ###########################
if ($('input[name="estrangeiro"]').length) {													//Verifica se existe campo na página
    if ($('input[name="estrangeiro"]:checked').val() == "true") {								//Exibe as informações caso necessite
        document.getElementById("dadosEstrangeiro").style.display = "block";

        if ($('input[name="naturalidade"]').length) {
            if ($('input[name="naturalidade"]:checked').val() == "true" && $('input[name="estrangeiro"]:checked').val() == "true") {
                document.getElementById("naturalizado").style.display = "block";
                document.getElementById("naoNaturalizado").style.display = "none";
            } else {
                document.getElementById("naturalizado").style.display = "none";
                document.getElementById("naoNaturalizado").style.display = "block";
            }
        }

    } else {
        document.getElementById("dadosEstrangeiro").style.display = "none";
        document.getElementById("naturalizado").style.display = "none";
        document.getElementById("naoNaturalizado").style.display = "none";
    }
}

$('input[name="estrangeiro"]').on("change", function () {										// Função para exebir Div de dados para pessoas Naturalizadas no Brasil
    if ($('input[name="estrangeiro"]:checked').val() == "true") {								//Exibe as informações caso necessite
        document.getElementById("dadosEstrangeiro").style.display = "block";

        if ($('input[name="naturalidade"]').length) {
            if ($('input[name="naturalidade"]:checked').val() == "true" && $('input[name="estrangeiro"]:checked').val() == "true") {
                document.getElementById("naturalizado").style.display = "block";
                document.getElementById("naoNaturalizado").style.display = "none";
            } else {
                document.getElementById("naturalizado").style.display = "none";
                document.getElementById("naoNaturalizado").style.display = "block";
            }
        }
    } else {
        document.getElementById("dadosEstrangeiro").style.display = "none";
        document.getElementById("naturalizado").style.display = "none";
        document.getElementById("naoNaturalizado").style.display = "none";
    }
});

$('input[name="naturalidade"]').on("change", function () {										// Função para exebir Div de dados para pessoas Naturalizadas no Brasil
    if ($('input[name="naturalidade"]:checked').val() == "true" && $('input[name="estrangeiro"]:checked').val() == "true") {
        document.getElementById("naturalizado").style.display = "block";
        document.getElementById("naoNaturalizado").style.display = "none";
    } else {
        document.getElementById("naturalizado").style.display = "none";
        document.getElementById("naoNaturalizado").style.display = "block";
    }
});

//############################### Documento de Classe ############################
//############################### Documento de Classe ############################
//############################### Documento de Classe ############################

if ($('input[name="possuiDocumentoClasse"]').length) {
    if ($('input[name="possuiDocumentoClasse"]:checked').val() == "true") {						//Exibe as informações caso seja verdadeiro
        document.getElementById("classeFuncionario").style.display = "block";
    } else {
        document.getElementById("classeFuncionario").style.display = "none";
    }
}

$('input[name="possuiDocumentoClasse"]').on("change", function () {								//Função para exibir Div de dados para quem tiver Crea ou outra classe
    if ($('input[name="possuiDocumentoClasse"]:checked').val() == "true") {						//Exibe as informações caso seja verdadeiro
        document.getElementById("classeFuncionario").style.display = "block";
    } else {
        document.getElementById("classeFuncionario").style.display = "none";
    }
});

//################################# Tabela Dinâmica Beneficiários ############################
//################################# Tabela Dinâmica Beneficiários ############################
//################################# Tabela Dinâmica Beneficiários ############################

var contadorBeneficiario = 0; //Contador de campos adicionados
var excluidos = new Array();

if ($("#contadorBeneficiario").val() >= 1) {
    contadorBeneficiario = Number($('#contadorBeneficiario').val());
}

$("#addBeneficiario").on("click", function (e) {		//Função para adicionar a div de beneficiários
    e.preventDefault();


    var disponivel = excluidos.pop();
    //alert(contadorBeneficiario);
    if (contadorBeneficiario < 15 && disponivel == undefined) {
        contadorBeneficiario = contadorBeneficiario + 1;
        $("#beneficiario").append('<div class="row" style="border-top: 1px dashed black; padding-top: 2em;"><div class="col-md-5 col-lg-5"><label>Nome Completo</label> <input name="nomeBeneficiarioFuncionario' + contadorBeneficiario + '" type="text" class="form-control"></div>' +
            '<div class="col-md-2 col-lg-2"><label>Sexo</label><div><label class="radio-inline"> <input type="radio" name="sexoBeneficiarioFuncionario' + contadorBeneficiario + '" value="m">Masculino</label> <label class="radio-inline"> <input type="radio" name="sexoBeneficiarioFuncionario' + contadorBeneficiario + '" checked="checked" value="f">Feminino</label></div>' +
            '</div><div class="col-md-2 col-lg-2"><label>Parentesco</label> <input name="parentescoBeneficiarioFuncionario' + contadorBeneficiario + '" type="text" class="form-control"></div>' +
            '<div class="col-md-2 col-lg-2"><label>Data de Nascimento</label> <input name="nascimentoBeneficiarioFuncionario' + contadorBeneficiario + '" type="text" class="form-control data"></div>' +
            '<div class="col-md-2 col-lg-2"><label>Para IRRF</label><div><label class="radio-inline"> <input type="radio" name="irrfBeneficiarioFuncionario' + contadorBeneficiario + '" value="s">Sim</label> <label class="radio-inline"> <input type="radio"	name="irrfBeneficiarioFuncionario' + contadorBeneficiario + '" checked="checked" value="n">Não </label>	</div> </div>' +
            '<button id="excluirBeneficiario" class="btn btn-danger btn-circle" value="' + contadorBeneficiario + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>');
    } else {
        contadorBeneficiario = contadorBeneficiario + 1;
        $("#beneficiario").append('<div class="row" style="border-top: 1px dashed black; padding-top: 2em;"><div class="col-lg-5"><label>Nome Completo</label> <input name="nomeBeneficiarioFuncionario' + disponivel + '" type="text" class="form-control"></div>' +
            '<div class="col-lg-2"><label>Sexo</label><div><label class="radio-inline"> <input type="radio" name="sexoBeneficiarioFuncionario' + disponivel + '" value="m">Masculino</label> <label class="radio-inline"> <input type="radio" name="sexoBeneficiarioFuncionario' + disponivel + '" checked="checked" value="f">Feminino</label></div></div>' +
            '<div class="col-lg-2"><label>Parentesco</label> <input name="parentescoBeneficiarioFuncionario' + disponivel + '" type="text" class="form-control"></div>' +
            '<div class="col-lg-2"><label>Data de Nascimento</label> <input name="nascimentoBeneficiarioFuncionario' + disponivel + '" type="text" class="form-control data"></div>' +
            '<div class="col-lg-2"><label>Para IRRF</label><div><label class="radio-inline"> <input type="radio" name="irrfBeneficiarioFuncionario' + disponivel + '" value="s">Sim</label> <label class="radio-inline"> <input type="radio"	name="irrfBeneficiarioFuncionario' + disponivel + '" checked="checked" value="n">Não </label>	</div> </div>' +
            '<button id="excluirBeneficiario" class="btn btn-danger btn-circle" value="' + disponivel + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>');
    }


    $(".data").inputmask("date", {yearrange: {minyear: 1900, maxyear: 2099}});
});

$("#beneficiario").on("click", "#excluirBeneficiario", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();
    $(this).parent('div').remove();
    excluidos.push($(this).val());
    contadorBeneficiario = contadorBeneficiario - 1;
});

//################################## CEP Auto ################################################
//################################## CEP Auto ################################################
//################################## CEP Auto ################################################

//Função de Limpeza do formulário
function limpa_formulario_cep() {
    $("#logradouro").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
    $("#ibge").val("");
}


//Verifica se campo existe na página
if ($(".cep").length) {

    //Verifica se há valor atual e executa função.
    if ($('.cep').val() != "") {

        var cep = $('.cep').val();

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{5}-?[0-9]{3}$/;

        if (validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            $("#rua").val("...")
            $("#bairro").val("...")
            $("#cidade").val("...")
            $("#uf").val("...")
            $("#ibge").val("...")

            //Consulta o webservice viacep.com.br/
            $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#logradouro").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                    $("#cidade").val(dados.localidade);
                    $("#uf").val(dados.uf);
                    //$("#ibge").val(dados.ibge);
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulario_cep();
                    alertaJquery("Alerta", "CEP não encontrado.", "alert");
                }
            });
        } //end if.
        else {
            //cep é inválido.
            limpa_formulario_cep();
            alertaJquery("CEP", $('.cep').val(), "alert");
            //alert("Formato de CEP inválido.");
        }
    }
}

//Quando o campo cep perde o foco função é executada
$(".cep").on("blur", (function () {
    //Nova variável com valor do campo "cep".
    var cep = $(this).val();

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{5}-?[0-9]{3}$/;

        //Valida o formato do CEP.
        if (validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            $("#rua").val("...")
            $("#bairro").val("...")
            $("#cidade").val("...")
            $("#uf").val("...")
            $("#ibge").val("...")

            //Consulta o webservice viacep.com.br/
            $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#logradouro").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                    $("#cidade").val(dados.localidade);
                    $("#uf").val(dados.uf);
                    //$("#ibge").val(dados.ibge);
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulario_cep();
                    alertaJquery("Alerta", "CEP não encontrado.", "alert");
                }
            });
        } //end if.
        else {
            //cep é inválido.
            limpa_formulario_cep();
            alertaJquery("Alerta", "Formato de CEP inválido.", "alert");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulario_cep();
    }
}));

//################################### Mascaras #################################################
//################################### Mascaras #################################################
//################################### Mascaras #################################################

$(document).ready(function () {
    $(".data").inputmask("d-m-y", {yearrange: {minyear: 1900}});  //static mask
    $(".cpf").inputmask("999.999.999-99");  //static mask
    $(".cnpj").inputmask("99.999.999/9999-99");  //static mask
    $(".cep").inputmask("99999-999");  //static mask
    $(".number").inputmask("999999999", {placeholder: ""});  //static mask
    $(".float").inputmask('decimal', {
        // groupSeparator: ',',
        // autoGroup: true,
        digits: 3,
        radixPoint: ".",
        // digitsOptional: false,
        // 'allowMinus': false,
        // 'prefix': 'R$ ',
        placeholder: ""
    });

    $(".timeStamp").inputmask("d-m-y h:s", {
        yearrange: {minyear: 1900},
        dateFormat: "24"
    });
    $(".hora").inputmask("h:s",
        {
            dateFormat: "24"
        });  //static mask
    $('.telefone').inputmask({mask: ['(99) 9999-9999', '(99) 99999-9999']});
    $('.cpfpj').inputmask({mask: ['999.999.999-99', '99.999.999/9999-99']}); //para quando não souber se será cpf ou cnpj
    $('.real').inputmask("decimal", {
        radixPoint: ",",
        digits: 2,
        autoGroup: true,
        groupSeparator: ".",
        groupSize: 3,
        prefix: "R$ ",
        placeholder: "0",
        digitsOptional: false
    });

    //$('.contaBancaria').inputmask('9999-9');
});


//################################## Validador CPF / CNPJ #####################################
//################################## Validador CPF / CNPJ #####################################
//################################## Validador CPF / CNPJ #####################################

function verifica_cpf_cnpj(valor) {

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');

    // Verifica CPF
    if (valor.length === 11) {
        return 'CPF';
    }

    // Verifica CNPJ
    else if (valor.length === 14) {
        return 'CNPJ';
    }

    // Não retorna nada
    else {
        return false;
    }

} // verifica_cpf_cnpj

/*
 calc_digitos_posicoes
 Multiplica dígitos vezes posições
 @param string digitos Os digitos desejados
 @param string posicoes A posição que vai iniciar a regressão
 @param string soma_digitos A soma das multiplicações entre posições e dígitos
 @return string Os dígitos enviados concatenados com o último dígito
 */

function calc_digitos_posicoes(digitos, posicoes, soma_digitos) {
    if (posicoes == undefined) {
        posicoes = 10;
    }
    if (soma_digitos == undefined) {
        soma_digitos = 0;
    }
    // Garante que o valor é uma string
    digitos = digitos.toString();

    // Faz a soma dos dígitos com a posição
    // Ex. para 10 posições:
    //   0    2    5    4    6    2    8    8   4
    // x10   x9   x8   x7   x6   x5   x4   x3  x2
    //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
    for (var i = 0; i < digitos.length; i++) {
        // Preenche a soma com o dígito vezes a posição
        soma_digitos = soma_digitos + ( digitos[i] * posicoes );

        // Subtrai 1 da posição
        posicoes--;

        // Parte específica para CNPJ
        // Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
        if (posicoes < 2) {
            // Retorno a posição para 9
            posicoes = 9;
        }
    }

    // Captura o resto da divisão entre soma_digitos dividido por 11
    // Ex.: 196 % 11 = 9
    soma_digitos = soma_digitos % 11;

    // Verifica se soma_digitos é menor que 2
    if (soma_digitos < 2) {
        // soma_digitos agora será zero
        soma_digitos = 0;
    } else {
        // Se for maior que 2, o resultado é 11 menos soma_digitos
        // Ex.: 11 - 9 = 2
        // Nosso dígito procurado é 2
        soma_digitos = 11 - soma_digitos;
    }

    // Concatena mais um dígito aos primeiro nove dígitos
    // Ex.: 025462884 + 2 = 0254628842
    var cpf = digitos + soma_digitos;

    // Retorna
    return cpf;

} // calc_digitos_posicoes

/*
 Valida CPF
 Valida se for CPF
 @param  string cpf O CPF com ou sem pontos e traço
 @return bool True para CPF correto - False para CPF incorreto
 */

function valida_cpf(valor) {

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Captura os 9 primeiros dígitos do CPF
    // Ex.: 02546288423 = 025462884
    var digitos = valor.substr(0, 9);

    // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
    var novo_cpf = calc_digitos_posicoes(digitos);

    // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
    var novo_cpf = calc_digitos_posicoes(novo_cpf, 11);

    // Verifica se o novo CPF gerado é idêntico ao CPF enviado
    if (novo_cpf === valor) {
        // CPF válido
        return true;
    } else {
        // CPF inválido
        return false;
    }

} // valida_cpf

/*
 valida_cnpj
 Valida se for um CNPJ
 @param string cnpj
 @return bool true para CNPJ correto
 */

function valida_cnpj(valor) {

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // O valor original
    var cnpj_original = valor;

    // Captura os primeiros 12 números do CNPJ
    var primeiros_numeros_cnpj = valor.substr(0, 12);

    // Faz o primeiro cálculo
    var primeiro_calculo = calc_digitos_posicoes(primeiros_numeros_cnpj, 5);

    // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
    var segundo_calculo = calc_digitos_posicoes(primeiro_calculo, 6);

    // Concatena o segundo dígito ao CNPJ
    var cnpj = segundo_calculo;

    // Verifica se o CNPJ gerado é idêntico ao enviado
    if (cnpj === cnpj_original) {
        return true;
    }

    // Retorna falso por padrão
    return false;

} // valida_cnpj

/*
 valida_cpf_cnpj
 Valida o CPF ou CNPJ
 @access public
 @return bool true para válido, false para inválido
 */

function valida_cpf_cnpj(valor) {

    // Verifica se é CPF ou CNPJ
    var valida = verifica_cpf_cnpj(valor);

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Valida CPF
    if (valida === 'CPF') {
        // Retorna true para cpf válido
        return valida_cpf(valor);
    }

    // Valida CNPJ
    else if (valida === 'CNPJ') {
        // Retorna true para CNPJ válido
        return valida_cnpj(valor);
    }

    // Não retorna nada
    else {
        return false;
    }

} // valida_cpf_cnpj

/*
 formata_cpf_cnpj
 Formata um CPF ou CNPJ

 @access public
 @return string CPF ou CNPJ formatado
 */

function formata_cpf_cnpj(valor) {

    // O valor formatado
    var formatado = false;

    // Verifica se é CPF ou CNPJ
    var valida = verifica_cpf_cnpj(valor);

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Valida CPF
    if (valida === 'CPF') {

        // Verifica se o CPF é válido
        if (valida_cpf(valor)) {

            // Formata o CPF ###.###.###-##
            formatado = valor.substr(0, 3) + '.';
            formatado += valor.substr(3, 3) + '.';
            formatado += valor.substr(6, 3) + '-';
            formatado += valor.substr(9, 2) + '';

        }

    }

    // Valida CNPJ
    else if (valida === 'CNPJ') {

        // Verifica se o CNPJ é válido
        if (valida_cnpj(valor)) {

            // Formata o CNPJ ##.###.###/####-##
            formatado = valor.substr(0, 2) + '.';
            formatado += valor.substr(2, 3) + '.';
            formatado += valor.substr(5, 3) + '/';
            formatado += valor.substr(8, 4) + '-';
            formatado += valor.substr(12, 14) + '';

        }

    }

    // Retorna o valor
    return formatado;

} // formata_cpf_cnpj


$(".cpf").on("blur", function () {
    if ($(this).val() != "") {
        if (!formata_cpf_cnpj($(this).val())) {
            $(this).val('');
            alertaJquery("Alerta", "CPF Inválidos", "alert");
        }
    }
});

$(".cpfpj").on("blur", function () {
    if ($(this).val() != "") {
        if (!formata_cpf_cnpj($(this).val())) {
            $(this).val('');
            alertaJquery("Alerta", "CPF ou CNPJ Inválidos", "alert");
        }
    }
});

//################################## Verificação de Duplicidade do CPF ############################
//################################## Verificação de Duplicidade do CPF ############################
//################################## Verificação de Duplicidade do CPF ############################

$('input[name="cpfFuncionario"]').on("blur", function () {
    var cpf = $("input[name='cpfFuncionario']");

    $.getJSON(
        caminho + "/apiSimg/valFunc-Cpf.php",
        {cpf: cpf.val().replace(/[^0-9]/g, '')},
        function (json) {
            if (json.validacao) {
                document.getElementById("btnSalvarContinuar").disabled = true;
                cpf.val('');
                alertaJquery("Alerta", json.descricao, "alert");
            } else {
                document.getElementById("btnSalvarContinuar").disabled = false;
            }
            $(nome).val(json.nome);
            $(cpf).val(json.cpf);
        }
    );

});

//#################################### Script de Validação de Campos #################################
//#################################### Script de Validação de Campos #################################
//#################################### Script de Validação de Campos #################################


$(document).ready(function () {
    $('#formulario').formValidation({

        framework: 'bootstrap',
        autoFocus: "true",
        live: 'submitted',
        message: 'Informação inválida.',

        row: {
            selector: null,
            valid: '',
            invalid: ''
        },

        icon: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: "fa fa-refresh"
        },

        fields: {
            salarioFuncionario: {
                icon: true,
                validators: {
                    notEmpty: {
                        message: 'Salário é necessário'
                    }
                }
            }
        }

    });

    $('.validaData').on("blur", function () {
        fctValidaData($(this));
    });

    $('.validaDataHora').on("blur", function () {
        fctValidaDataHora($(this));
    });

    $('.validaHora').on("blur", function () {
        fctValidaHora($(this));
    });

});

//################################# Modals #################################################
//################################# Modals #################################################
//################################# Modals #################################################

$(document).ready(function () {

    $("#formularioModal_1").submit(function () {                                   //seleciona o formulário do modal

        var dados = $(this).serialize();                                        //dados recebe o formulario em string

        $.ajax({                                                                //inicia o ajax
            type: "POST",                                                       //informa o metodo do formulario
            url: caminho + "/dashboardMateriais/cadastroMarcaModal",         //informa o caminho do processo
            data: dados,                                                        //informa os dados ao ajax
            success: function (data) {                                            //em caso de sucesso imprime o retorno
                alertaJquery("Dados", data, "alert");
            }
        });

        $("#selectRefreshModal_1").load(document.location.href + " #divRefreshedModal_1");

        return false;
    });

    $("#formularioModal_2").submit(function () {                                   //seleciona o formulário do modal

        var dados = $(this).serialize();                                        //dados recebe o formulario em string

        $.ajax({                                                                //inicia o ajax
            type: "POST",                                                       //informa o metodo do formulario
            url: caminho + "/dashboardMateriais/cadastroCategoriaModal",     //informa o caminho do processo

            data: dados,                                                        //informa os dados ao ajax
            success: function (data) {                                            //em caso de sucesso imprime o retorno
                alertaJquery("Dados", data, "alert");
            }
        });

        $("#selectRefreshModal_2").load(document.location.href +                //refresh em única div dentro da div
            " #divRefreshedModal_2");                                       // principal

        return false;
    });
});


//############################## Funções diversas ###############################
//############################## Funções diversas ###############################
//############################## Funções diversas ###############################

function changeActionForm(form, action) {
    $(form).attr("action", action);
    $(form).submit();
}
function hasProcess(e) {
    //e.preventDefault();
    //e.stopPropagation();

    if ($('body').hasClass('processing')) {
        //alert('Já processando');
        return true;
    }

    $('body').addClass('processing');
    //alert('processando');
    return false;
}

//Exibição de uma dica para qualquer div com rel=tooltip-wrapper
$('[rel="tooltip-wrapper"]').tooltip();


//Função de cálculo para diferente entre horário de HH:MM
//Função de cálculo para diferente entre horário de HH:MM
//Função de cálculo para diferente entre horário de HH:MM

function diferencaHoras(horaInicial, horaFinal) {

    // Tratamento se a hora inicial é menor que a final

    hIni = horaInicial.split(':');
    hFim = horaFinal.split(':');

    proxHora = 0;
    horaAtual = 0;
    proxDia = 0;

    horasTotal = 0;
    minutosTotal = 0;

    if (!isHoraInicialMenorHoraFinal(horaInicial, horaFinal)) {    //Verifica se hora inicial é maior que final(Virada de dia)
        if (parseInt(hIni[1], 10) != 0) {                             //Verifica se há minutos na hora inicial
            proxHora = 60 - parseInt(hIni[1], 10);                  //Recebe o necessário de miutos para próxima hora
            horaAtual = parseInt(hIni[0], 10) + 1;                   //Soma um a mais na hora caso aja minutos
        } else {
            horaAtual = hIni[0];
        }

        if (horaAtual != 24 || parseInt(hIni[0], 10) != 0) {          //Verifica se horaAtual é diferente de 24 ou hIni é diferente de 0
            proxDia = 24 - horaAtual;                               //proxDia recebe a quantidade de horas para o próximo dia
        } else {
            proxDia = 0;                                            //proxDia recebe zero, considerando então que não se passa horas, somente minutos
        }

        horasTotal = parseInt(hFim[0], 10) + proxDia;//Soma da quantidade de hora para o próximo dia e a hora final
        minutosTotal = parseInt(hFim[1], 10) + proxHora;//Soma os minutos necessario para o próximo com os minutos finais

        if (minutosTotal >= 60) {//Se minutoTotal passar dos 60 diminue esse valor e soma uma hora
            minutosTotal -= 60;
            horasTotal += 1;
        }


    } else {//realiza uma subtração normal

        horasTotal = parseInt(hFim[0], 10) - parseInt(hIni[0], 10);
        minutosTotal = parseInt(hFim[1], 10) - parseInt(hIni[1], 10);

        if (minutosTotal < 0) {
            minutosTotal += 60;
            horasTotal -= 1;
        }

    }

    horaTotal = completaZeroEsquerda(horasTotal) + ":" + completaZeroEsquerda(minutosTotal);
    return horaTotal;
}

//Complementar horário com zeros necessários após cálculos
//Complementar horário com zeros necessários após cálculos
//Complementar horário com zeros necessários após cálculos

function completaZeroEsquerda(numero) {

    number = numero + "";

    if (number.length == 1) {
        numero = "0" + numero;
    }

    return numero;
}

//Verifica se a hora inicial é menor que final
//Verifica se a hora inicial é menor que final
//Verifica se a hora inicial é menor que final

function isHoraInicialMenorHoraFinal(horaInicial, horaFinal) {
    horaIni = horaInicial.split(':');
    horaFim = horaFinal.split(':');

    // Verifica as horas. Se forem diferentes, é só ver se a inicial
    // é menor que a final.
    hInicial = parseInt(horaIni[0], 10);
    hFinal = parseInt(horaFim[0], 10);
    if (hInicial != hFinal)
        return hInicial < hFinal;

    // Se as horas são iguais, verifica os minutos então.
    mIni = parseInt(horaIni[1], 10);
    mFim = parseInt(horaFim[1], 10);
    if (mIni != mFim)
        return mIni < mFim;
}

//Função para contar quantidade de elementos Filhos de um elemento
//Função para contar quantidade de elementos Filhos de um elemento
//Função para contar quantidade de elementos Filhos de um elemento

window.getCount = function (parent, tag, getChildrensChildren) {
    var relevantChildren = 0;
    var children = parent.childNodes.length;
    for (var i = 0; i < children; i++) {
        if (parent.childNodes[i].nodeType != 3) {
            if (parent.childNodes[i].tagName == tag) {
                if (getChildrensChildren)
                    relevantChildren += getCount(parent.childNodes[i], tag, true);
                relevantChildren++;
            }
        }
    }
    return relevantChildren;
}

//########## Interação para MODAL de devolução e cancelamento ##########
//########## Interação para MODAL de devolução e cancelamento ##########
//########## Interação para MODAL de devolução e cancelamento ##########

$('#motivoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var acao = button.data('action');

    $('input[name="acaoModal"]').val(acao);
});

$('#form_modal').on('submit', function (e) {
    if (hasProcess(e)) return;

    var resultado = validacao_formulario($('#form_modal input'), $('#form_modal select'), $('#form_modal textarea'));

    if (resultado['permissao']) {
        return true;
    } else {
        alertaJquery("Alerta", 'Há campos obrigatórios vazios.', "alert");
        $('body').removeClass('processing');
        return false;
    }
});

//##########  Verificação de alterar senha ##########
//##########  Verificação de alterar senha ##########
//##########  Verificação de alterar senha ##########

$(".formAlterarSenha").submit(function (e) {
        e.preventDefault();

        var senha1 = $('input[name="senhaAntiga"]').val();
        var senha2 = $('input[name="novaSenha"]').val();
        var senha3 = $('input[name="confirmarSenha"]').val();
        var nomeUser = $('input[name="nomeUsuario"]').val();

        if (senha2 == senha3) {
            alertaJquery('Alterar Senha', 'Ao realizar esta ação, você irá retornar à tela de login. Deseja continuar?', 'confirm', [{value: "Sim"}, {value: "Não"}], function (res) {
                if (res == "Sim") {
                    $.ajax({
                        type: "POST",
                        url: caminho + "/cadastroGeral/alterarSenhaUsuario",
                        data: {senhaAntiga: senha1, novaSenha: senha2, nomeUsuario: nomeUser, confirmarSenha: senha3},
                        success: function (data) {
                            if (data) {
                                window.location.reload();
                            } else {
                                $('#erro').html('<div class="alert alert-danger alert-dismissible" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>' +
                                    '<h4>Senha atual incorreta</h4>' +
                                    '</div>');
                            }
                        },
                        failure: function () {
                            alertaJquery("Alerta", 'Função não encontrada. Entrar com contato com a T.I.', "alert");
                        }
                    });
                }
                if (res == "Não") {
                    $('#alteraSenhaModal').modal('hide');
                }
            });

        } else {
            $('#erro').html('<div class="alert alert-danger alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>' +
                '<h4>Nova senha não corresponde com a confimação!</h4>' +
                '</div>');
            return false;
        }
    }
);

//##########  Destaque para facilitar a visualização em tabelas ##########
//##########  Destaque para facilitar a visualização em tabelas ##########
//##########  Destaque para facilitar a visualização em tabelas ##########

$('table tbody tr').hover(
    function () {
        $(this).addClass('highlightTable');
    },
    function () {
        $(this).removeClass('highlightTable');
    }
);

//##########  Campo Justificar Alteração de Nível ##########
//##########  Campo Justificar Alteração de Nível ##########
//##########  Campo Justificar Alteração de Nível ##########

$('document').ready(function () {
    //Preencher a variavel nivel ao carregar pagina
    //Justificar Alteração de Nível
    //nivel = $('#nivel option:selected').val();

    //Tabulação Jquery UI
    $(".tabs").tabs();

    // ########### Corigir bug do dataTable ############
    // ########### Corigir bug do dataTable ############
    // ########### Corigir bug do dataTable ############
    $.fn.dataTable.moment('DD-MM-YYYY HH:mm:ss');
    $.extend( $.fn.dataTableExt.oStdClasses, {
        "sFilterInput": "form-control",
        "sLengthSelect": "form-control"
    });

    //########## Botão de retorno ao inicio da pagina ##########
    //########## Botão de retorno ao inicio da pagina ##########
    //########## Botão de retorno ao inicio da pagina ##########
    $('#boxSetaVolteAoTopo').hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#boxSetaVolteAoTopo').fadeIn();
        } else {
            $('#boxSetaVolteAoTopo').fadeOut();
        }
    });
    $('#boxSetaVolteAoTopo').click(function () {
        $('body').animate({
            scrollTop: 0
        });
    });

});

//####################### Funcao Validação de Dados javascript ###################################################
//####################### Funcao Validação de Dados javascript ###################################################
//####################### Funcao Validação de Dados javascript ###################################################
$(document).ready(function () {
    changeTime();

    setInterval(changeTime, 1000);

    function changeTime() {
        $.getJSON(caminho + "/cadastroGeral/getDateServer",
            function (json) {
                $('.dataRelogioNavBarSpan').html(json.tempo);
            });
    }
});

//##########  Função de getJson ##########
//##########  Função de getJson ##########
//##########  Função de getJson ##########

function getJson(url, callback)
{
    $.getJSON(
        caminho + url,
        function (json) {
             callback(json);
        }
    ).error(function(msg){
        alert("error");
        alert(JSON.stringify(msg));
    });
}

//####################### Funcao Validação de Dados javascript ###################################################
//####################### Funcao Validação de Dados javascript ###################################################
//####################### Funcao Validação de Dados javascript ###################################################

function tratamentoTempo(tempo) {
    var dia = tempo.substring(0, 2);
    var mes = tempo.substring(3, 5);
    var ano = tempo.substring(6, 10);

    var hor = tempo.substring(11, 13);
    var min = tempo.substring(14, 16);

    return new Date(ano, (mes - 1), dia, hor, min);
}

function fctValidaData(obj) {
    var data = obj.val();
    var dia = data.substring(0, 2);
    var mes = data.substring(3, 5);
    var ano = data.substring(6, 10);

    if (obj.val() == "") {
        return true;
    }

    if (ano > 2035) {
        alertaJquery("Alerta", "Data Inválida", "alert");
        obj.val("");
        return false;
    }

    //Criando um objeto Date usando os valores ano, mes e dia.
    var novaData = new Date(ano, (mes - 1), dia);

    var mesmoDia = parseInt(dia, 10) == parseInt(novaData.getDate());
    var mesmoMes = parseInt(mes, 10) == parseInt(novaData.getMonth()) + 1;
    var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());

    if (!((mesmoDia) && (mesmoMes) && (mesmoAno))) {
        alertaJquery("Alerta", "Data Inválida", "alert");
        obj.val("");
        return false;
    }
    return true;
}

function fctValidaHora(obj) {

    var data = obj.val();
    var hor = data.split(":");

    var min = hor[1];
    hor = hor[0];

    if (obj.val() == "") {
        return true;
    }

    //Criando um objeto Date usando os valores ano, mes e dia.
    var novaData = new Date();
    novaData.setHours(hor, min);

    var mesmohor = parseInt(hor) == parseInt(novaData.getHours());
    var mesmomin = parseInt(min) == parseInt(novaData.getMinutes());

    if (!((mesmohor) && (mesmomin))) {
        alertaJquery("Alerta", "Horário Inválido", "alert");
        obj.val("");
        return false;
    }
    return true;
}

function validarHoraProgramada(tempo) {

    var objDate = tratamentoTempo(tempo);

    var objAtual = tratamentoTempo($('.dataRelogioNavBarSpan').html());

    if (objDate.getTime() < objAtual.getTime()) {
        return false;
    }

    return true;
}

function fctValidaDataHora(obj) {

    var data = obj.val();
    var dia = data.substring(0, 2);
    var mes = data.substring(3, 5);
    var ano = data.substring(6, 10);

    var hor = data.substring(11, 13);
    var min = data.substring(14, 16);

    if (obj.val() == "") {
        return true;
    }

    if (ano > 2035) {
        alertaJquery("Alerta", "Data Inválida", "alert");
        obj.val("");
        return false;
    }

    //Criando um objeto Date usando os valores ano, mes e dia.
    var novaData;

    try {
        novaData = new Date(ano, (mes - 1), dia, hor, min);
    } catch (ex) {
        return false;
    }

    var mesmoDia = parseInt(dia, 10) == parseInt(novaData.getDate());
    var mesmoMes = parseInt(mes, 10) == parseInt(novaData.getMonth()) + 1;
    var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());

    var mesmohor = parseInt(hor) == parseInt(novaData.getHours());
    var mesmomin = parseInt(min) == parseInt(novaData.getMinutes());

    if (!((mesmoDia) && (mesmoMes) && (mesmoAno) && (mesmohor) && (mesmomin))) {

        alertaJquery("Alerta", "Data ou Hora Inválida", "alert");
        obj.val("");
        return false;
    }
    return true;
}
var checkRequiredField = function()
{
    $('[required]').on('blur', function () {
        var campo = $(this).val();
    
        if (campo == null || campo == '' || campo == undefined) {
            $(this).val("");
            $(this).parent('div').addClass('has-error');
        } else {
            $(this).parent('div').removeClass('has-error');
        }
    });
}
//Alerta Campos required em branco
$('[required]').on('blur', function () {
    var campo = $(this).val();

    if (campo == null || campo == '' || campo == undefined) {
        $(this).val("");
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

function validacao_formulario(inputs, selects, textareas) {
// function validacao_formulario() {
    var resultado = [];
    resultado['campos_vazios'] = [];
    var div = $('<div/>');

    resultado['permissao'] = true;

    inputs.each(function (index) {
        var name = $(this).attr('name');
        if (!$.trim($(this).val()).length && $(this).is(':required') && name != undefined) {
            resultado['campos_vazios'].push(name);
            resultado['permissao'] = false;

            $(this).parent('div').addClass('has-error');
        }
        else {
            div.html($(this).val());
            div.contents().filter(function () {
                return this.nodeType !== 3;
            }).after(function () {
                return $(this).text();
            }).remove();
            $(this).val(div.html());
        }
        $(this).val($.trim($(this).val()));
    });

    selects.each(function (index) {
        var name = $(this).attr('name');
        if (!$(this).val() && $(this).is(':required') && name != undefined) {
            resultado['campos_vazios'].push(name);
            resultado['permissao'] = false;

            $(this).parent('div').addClass('has-error');
        }
    });

    textareas.each(function (index) {
        var name = $(this).attr('name');

        if (!$.trim($(this).val()).length && $(this).is(':required') && name != undefined) {
            resultado['campos_vazios'].push(name);
            resultado['permissao'] = false;

            $(this).parent('div').addClass('has-error');
        } else {
            div.html($(this).val());
            div.contents().filter(function () {
                return this.nodeType !== 3;
            }).after(function () {
                return $(this).text();
            }).remove();
            $(this).val(div.html());
        }
        $(this).val($.trim($(this).val()));
    });

    return resultado;
}

// Imprimir div sem CSS
function printAreaImpressaoSemStyle() {
    $("#areaImpressao input").each(function () {
        attrValorInput($(this));
    });
    $("#areaImpressao textarea").each(function () {
        attrValorInput($(this));
    });

    var printContents = document.getElementById("areaImpressao").innerHTML;

    var newWindow = window.open('', 'impressao');
    newWindow.document.body.innerHTML = printContents;
    newWindow.print();
    newWindow.close();
}

// Imprimir div com Bootstrap
function printAreaImpressao() {
    $("#areaImpressao input").each(function () {
        attrValorInput($(this));
    });
    $("#areaImpressao textarea").each(function () {
        attrValorInput($(this));
    });

    var printContents = document.getElementById("areaImpressao").innerHTML;
    var newWindow = window.open(caminho + '/views/_includes/_print.php');
    $(newWindow).on("load", function () {
        newWindow.document.body.innerHTML = printContents;
        newWindow.print();
        newWindow.close();
    });
}

function attrValorInput(objeto) {
    objeto.attr("value", objeto.val());
}


//####################### DataListFuncao javascript ###################################################
//####################### DataListFuncao javascript ###################################################
//####################### DataListFuncao javascript ###################################################

function dataListFuncao(input, datalist, hidden) {
    var opt = datalist.find("option[value='" + input.val() + "']");
    if (opt.val()) {
        hidden.val(opt.val());
        input.val(opt.text());
    }
}

function dataListFuncao2(input, datalist, hidden) {
    var opt = datalist.find("option[value='" + input.val() + "']");
    if (opt.val()) {
        hidden.val(opt.attr("id"));
        input.val(opt.val());
    }
}



function daysInMonth (month, year) { // Use 1 for January, 2 for February, etc.
    return new Date(year, month, 0).getDate();
}


//####################### var_dump javascript ###################################################
//####################### var_dump javascript ###################################################
//####################### var_dump javascript ###################################################

function var_dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);
}

//#### Variavel/Objeto generica com função .val()####
//#### Variavel/Objeto generica com função .val()####
//#### Variavel/Objeto generica com função .val()####

var ObjetoVal = {
    text: function (value) {
        if (value)
            this.valor = value;
        else
            return this.valor;
    },
    valor: "",
    val: function (value) {
        if (value)
            this.valor = value;
        else
            return this.valor;
    }
};

//####################### Transform Sec to HH:MM:SS ###########################
//####################### Transform Sec to HH:MM:SS ###########################
//####################### Transform Sec to HH:MM:SS ###########################

function transformSecToHour (seconds, haveDay)
{
  var sec = seconds%60;
  var min = (seconds/60)%60;
  var hour = (seconds/60)/60;

  if(haveDay){
    var arrResult =
    {
      "segundos" : Math.round(sec),
      "minutos" : Math.round(min),
      "horas" : Math.round(hour%24),
      "dias" : Math.round(hour/24)
    }
  }else{
    var arrResult =
    {
      "segundos" : Math.round(sec),
      "minutos" : Math.round(min),
      "horas" : Math.round(hour)
    }
  }


  return arrResult;
}

//####################### Exibir PDF ###########################
//####################### Exibir PDF ###########################
//####################### Exibir PDF ###########################
$('.exibirPdfLink').click(function(e){
    e.preventDefault();

    var nomePdf = $(this).attr('value').split(":")[1] + '.pdf';
    var caminhoPdf = $(this).attr('value').split(":")[0];

    $.post(
        caminho + "/cadastroGeral/exibirPdf",
        {nomeArquivo: nomePdf}
    ).done(function() {
        window.open(caminho + "/" + caminhoPdf , "_blank");
    });

});
//####################### DatePicker e DateTimePicker ###########################
//####################### DatePicker e DateTimePicker ###########################
//####################### DatePicker e DateTimePicker ###########################

$.datetimepicker.setLocale('pt');

$('.data').datetimepicker({
    lang: 'pt-BR',
    timepicker: false,
    format: 'd/m/Y',
    formatDate: 'd/m/Y'
});

if ($("input[name='dataRecebimento']").val()) {
    $('.timeStamp').datetimepicker({
        defaultDate: $("input[name='dataRecebimento']").val().substring(0, 10),
        defaultTime: $("input[name='dataRecebimento']").val().substring(11, 16),
        lang: 'pt-BR',
        format: 'd/m/Y H:i',
        formatTime: 'H:i',
        formatDate: 'd/m/Y',
        step: 1
    });
} else {
    $('.timeStamp').datetimepicker({
        lang: 'pt-BR',
        format: 'd/m/Y H:i',
        formatTime: 'H:i',
        formatDate: 'd/m/Y'
    });
}



//####################### Websocket Conexão ###########################
//####################### Websocket Conexão ###########################
//####################### Websocket Conexão ###########################

var socket;
var realTimeOff;

$('document').ready(function(){
    try {
        //Conexão / User Connected
        var caminhoNP = window.location.hostname;
        socket = io('//'+caminhoNP+':4555');

        $('.iconeHistoricoRealTime').show();
        realTimeOff = false;

    } catch (ex) {
        $('.iconeHistoricoRealTime').hide();
        realTimeOff = true;
    }
});
