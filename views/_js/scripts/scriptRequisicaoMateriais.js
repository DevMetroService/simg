/**
 * Created by abner.andrade on 23/11/2017.
 */

/**
 * CONFIGURACOES INICIAIS
 */

$('#btnFinalizarRequisicao').attr('disabled', 'disabled');
$('#btnPrint').attr('disabled', 'disabled');


var tabelaDeMateriais;
$(document).ready(function () {
    tabelaDeMateriais = $('#tableListaMateriais').dataTable(configTable(0, false, 'desc'));
});

var dadosDaRequisicao = {}; //Um objeto que guarda os dados da Requisi??o

var listaMateriais = []; //Vetor de Objetos, Guarda a lista dos Materiais Requisitados

/**
 * var contaLinhas,
 *  ? utilizada junto com a fun??o
 *  ordenaItensDatabela() para realinhar a
 *  numeracao de itens na tabela. *
 */
var contaLinhasTabela = 0;


/**
 * EVENTOS
 */

$('#btnAddMaterial').click(function () {

    var resultado = validacao_formulario(
        $('#formAddMaterial input'),
        $('#formAddMaterial select'),
        $('#formAddMaterial textarea'));

    if (resultado['permissao']) {
        adicionarMaterial();

        $('#btnFinalizarRequisicao').removeAttr('disabled'); //  Desbloqueia o botao de finalizar pedido
        $('#btnPrint').removeAttr('disabled'); //  Desbloqueia o botao de Impressao

    } else {
        alertaJquery('Falta de Informa??es', 'H? campos obrigat?rios vazios.', 'alert');
    }

});


$('select[name="descMaterial"]').change(function () {
    $('#codigo').val($(this).val());
});


$(document).on('click', '.removerLinha', function (e) {

    e.preventDefault();

    $(this).closest('tr').fadeOut(350, function () {
        var row = $(this).closest('tr');
        tabelaDeMateriais.fnDeleteRow(row[0]);

        contaLinhasTabela--;
        if (contaLinhasTabela == 0) {
            $('#btnFinalizarRequisicao').attr('disabled', 'disabled');
            $('#btnPrint').attr('disabled', 'disabled');
        } else {
            ordenarItensDaTabela();
        }

    });


});


$(document).on('click', '#btnFinalizarRequisicao', function () {

    var resultado = validacao_formulario(
        $('#dadosDaRequisicao input'),
        $('#dadosDaRequisicao select'),
        $('#dadosDaRequisicao textarea')
    );

    if (resultado['permissao']) {

        pegaListaDeMateriais();
        pegaDadosDaRequisicao(listaMateriais);

        alert(JSON.stringify(dadosDaRequisicao));

        $.post(
            caminho + '/cadastroMateriais/vardumppost',
            dadosDaRequisicao,
            function (json) {
                alert(json);
                //$(this).html(json);
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });


    } else {
        alertaJquery('Falta de Informa??es', 'H? campos obrigat?rios vazios.', 'alert');
    }


});


$(document).on('click', '#btnPrint', function () {
    var resultado = validacao_formulario(
        $('#dadosDaRequisicao input'),
        $('#dadosDaRequisicao select'),
        $('#dadosDaRequisicao textarea')
    );

    if (resultado['permissao']) {
        window.print();
        //onclick="window.print()"

    } else {
        alertaJquery('Falta de Informa??es', 'H? campos obrigat?rios vazios.', 'alert');
    }

});


/**
 * FUNCOES
 */

function adicionarMaterial() {

    var dadosDoForm = {
        codigo: $('input[name="codigo"]').val(),
        descMaterial: $('select[name="descMaterial"] option:selected').html(),
        os: $('input[name="os"]').val(),
        qtdSolicitada: $('input[name="qtdSolicitada"]').val(),
        unidade: $('select[name="unidade"]').val(),
        qtdEntregue: $('input[name="qtdEntregue"]').val(),
        dataHoraEntrega: $('input[name="dataHoraEntrega"]').val()
    };

    tabelaDeMateriais.fnAddData([
        '',
        dadosDoForm.codigo + '-' + dadosDoForm.descMaterial,
        dadosDoForm.os,
        dadosDoForm.qtdSolicitada + '-' + dadosDoForm.unidade,
        dadosDoForm.qtdEntregue,
        dadosDoForm.dataHoraEntrega,
        '<button class="removerLinha btn btn-circle btn-danger"><i class="fa fa-close"></i></button>'
    ]);

    // Limpa os campos do Formulario que Adiciona os Materiais
    $('#formAddMaterial').each(function () {
        this.reset();
    });

    contaLinhasTabela++;
    ordenarItensDaTabela();

}


function ordenarItensDaTabela() {

    var table = $('#tableListaMateriais');

    var n_item = 0;
    var cols = 0;

    table.find('tr').each(function () {
        cols = 0;

        if (n_item > 0) {

            $(this).find('td').each(function () {

                if (cols == 0) {
                    $(this).html('' + n_item);
                }

                cols++;
            });
        }

        n_item++;
    });

}


function pegaListaDeMateriais() {

    // console.log(tabelaDeMateriais.fnGetData());


    var itemDaLista;    //Ira armazenar a lista de Materiais da tabela, na forma de um objeto

    var table = $('#tableListaMateriais');

    table.find('tr').each(function () {
        var cols = 0;

        // Para cada linha percorrida, um novo objeto ? criado para armazenar os dados
        itemDaLista = {
            codigo: '',
            descMaterial: '',
            os: '',
            qtdSolicitada: '',
            unidade: '',
            qtdEntregue: '',
            dataHoraEntrega: ''
        };

        // console.log($(this));

        $(this).find('td').each(function () {
            //Percorre as colunas da linha atual
            // console.log($(this));
            var dado;

            switch (cols) {
                case 1:
                    dado = $(this).html();
                    dado = dado.split("-");
                    itemDaLista.codigo = dado[0];
                    itemDaLista.descMaterial = dado[1];
                    break;
                case 2:
                    itemDaLista.os = $(this).html();
                    break;
                case 3:
                    dado = $(this).html();
                    dado = dado.split("-");
                    itemDaLista.qtdSolicitada = dado[0];
                    itemDaLista.unidade = dado[1];
                    break;
                case 4:
                    itemDaLista.qtdEntregue = $(this).html();
                    break;
                case 5:
                    itemDaLista.dataHoraEntrega = $(this).html();
                    break;
            }

            cols++;
        });

        console.log(itemDaLista);

        listaMateriais.push(itemDaLista);

        cols = 0;

    });

    listaMateriais.shift(); //Remove o indice referente ao cabecalho da tabela
    console.log(listaMateriais);


}


function pegaDadosDaRequisicao(listaMateriais) {
    dadosDaRequisicao = {
        dados: [
            {
                areaSolicitante: $('select[name="areaSolicitante"]').val(),
                funcionarioRequisitante: $('input[name="funcionarioRequisitante"]').val(),
                dataRequisicao: $('input[name="dataRequisicao"]').val(),
                aprovadorRequisicao: $('input[name="aprovadorRequisicao"]').val(),
                dataAprovacao: $('input[name="dataAprovacao"]').val(),
                txtObservacoes: $('textarea[name="txtObservacoes"]').val()
            }
        ],
        listaMaterial: listaMateriais
    };

    console.log(dadosDaRequisicao);

}
