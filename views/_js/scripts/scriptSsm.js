/**
 * Created by josue.marques on 24/02/2016.
 *
 */

//######### Linha / Trecho / PontoNotavel
//Auto preenchimento do trecho e via ap�s sele��o da linha
$('select[name="linhaSsm"]').on("change", function () {
    apiLinhaTrecho($('select[name="linhaSsm"]'), $('select[name="trechoSsm"]'));
    apiLinhaVia($('select[name="linhaSsm"]'), $('select[name="viaSsm"]'));

    $('select[name="pontoNotavelSsm"]').html('<option disabled="disabled" selected="selected" value="">Pontos Not�veis</option>');

    var grupo = $('select[name="grupoSistemaSsm"]').val();

    if (grupo == "22" || grupo == "23" || grupo == "26") {
        apiGetVeiculo(grupo, $('select[name="veiculoMrSsm"]'), $(this).val(), null, false);
    }
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoSsm"]').on("change", function () {
    apiTrechoPn($('select[name="trechoSsm"]'), $('select[name="pontoNotavelSsm"]'))
});

//######### Grupo / Sistema / SubSistema
//Auto preenchimento do sistema ap�s sele��o do grupo sistema
$('select[name="grupoSistemaSsm"]').on("change", function () {

    var divMr = $('#divMaterialRodante');
    var inputMr = $('#divMaterialRodante input');
    var selectMr = $('#divMaterialRodante select');

    var grupo = $(this);

    if( $(this).val() == "22" || $(this).val() == "23" || $(this).val() == "26"){
        divMr.show();
        inputMr.each(function () {
            $(this).attr("required", "required");
            $(this).val('');
        });
        selectMr.each(function () {
            $(this).attr("required", "required");
            $(this).val('');
        });

        $('select[name="prefixoMrSsm"]').removeAttr("required");

        $('select[name="carroMrSsm"]').html('');
        var linha = $('select[name="linhaSsm"]').val();
        apiGetVeiculo(grupo.val(), $('select[name="veiculoMrSsm"]'), linha, null, false);
    }else{
        divMr.hide();
        inputMr.each(function () {
            $(this).removeAttr("required");
        });
        selectMr.each(function () {
            $(this).removeAttr("required");
        });
    }

    var selectSublocal = $('#sublocal select');
    if($(this).val() == "28" || $(this).val() == "27"){
        $('#sublocal').show();
        selectSublocal.each(function(){
            $(this).attr("required", "required");
            $(this).val('');
        });
    }else{
        $('#sublocal').hide();
        selectSublocal.each(function(){
            $(this).removeAttr("required");
            $(this).val('');
        });
    }

    $('input[name="numeroGrupoSistemaSsm"]').val($(this).val());
    $('input[name="numeroSistemaSsm"]').val('');
    $('input[name="codSubSistema"]').val('');

    apiGrupoSistema($(this), $('select[name="sistemaSsm"]'));
    $('select[name="subSistemaSsm"]').html('<option disabled="disabled" selected="selected">Selecione o Sub-Sistema</option>');
});

$('select[name="grupoSistemaSsm"]').on("blur", function(){
    $('select[name="sistemaSsm"] option[value="174"]').each(function(){
        $(this).remove();
    });
});

$('select[name="localGrupo"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocalGrupo"]'));
});

$('select[name="sistemaSsm"]').on("change", function () {
    $('input[name="numeroSistemaSsm"]').val($(this).val());
    $('input[name="codSubSistema"]').val('');

    apiSistemaSub($(this), $('select[name="subSistemaSsm"]'));
});

//Auto preenchimento do codSubSistema
$('select[name="subSistemaSsm"]').on("change", function () {
    $('input[name="codSubSistema"]').val($(this).val());
});

//Material Rodante

var odometro = 0;
var odometroField = $('input[name="odometroMrSsm"]');
var carroField = $('select[name="carroMrSsm"]');

$('select[name="veiculoMrSsm"]').change(function () {

    apiCarro('veiculo', $(this).val(), carroField);

});

carroField.change(function () {
    if( $('select[name="grupoSistemaSsm"] option:selected').val() == 23 ){
        odometro = carroField.find("[data-sigla='MF1']").data("odometro");
    }else{
        if ($('select[name="carroMrSsm"] option:selected').data("sigla") == "RA"){
            odometro = carroField.find("[data-sigla='MA']").data("odometro");
        }else if ($('select[name="carroMrSsm"] option:selected').data("sigla") == "RB"){
            odometro = carroField.find("[data-sigla='MB']").data("odometro");
        }else{
            odometro = $('select[name="carroMrSsm"] option:selected').data("odometro");
        }
    }

    odometroField.val('');
    odometroField.attr("placeholder", odometro);
});


//############## Servi�o DataList
$('input[name="servicoSsmInput"]').on("input", function () {
    dataListFuncao($('input[name="servicoSsmInput"]'), $('#servicoSsmDataList'), $('input[name="servicoSsm"]'));
});

$('input[name="servicoSsmInput"]').on("blur", function () {
    if ($('input[name="servicoSsm"]').val() == "") {
        $(this).val("");
        alertaJquery("Alerta", 'Clique em um servi�o', "alert");
    } else {
        $('input[name="servicoSsmInput"]').val($("#servicoSsmDataList").find("option[value='" + $('input[name="servicoSsm"]').val() + "']").text());
    }
});

$('select[name="localSsm"]').on("change", function () {
    $('input[name="numeroLocalSsm"]').val($(this).val());
    apiLocalEquipe($(this), $('select[name="equipeSsm"]'));
});

$('select[name="equipeSsm"]').on("change", function () {
    $('input[name="siglaEquipeSsm"]').val($(this).val());
});

$('#nivel').change(function(){

    var fieldJust = $('#justificativaNivel');
    var inputJust = $('#justificativaAltNivel');

    if($('#nivelOriginal').val() != $(this).val() ){
        fieldJust.show();
        inputJust.attr('required', 'required');
    }else{
        fieldJust.hide();
        inputJust.removeAttr('required');
    }
});


//############### Encaminhamento

$(".salvarAlteracao").on("click", function (e) {
    if (hasProcess(e)) return;

    var resultado = validacao_formulario($('#formSsm input'), $('#formSsm select'), $('#formSsm textarea'));

    if (resultado['permissao']) {
        $("#formSsm").attr("action", caminho + "/cadastroGeral/alterarSsm/" + localReturn);
        $('#formSsm').submit();
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

//Verifica��o de cancelamento de Ssm programada.
var nSsp = $('input[name="nSsp"]');
var nSaf = $('input[name="nSaf"]');

// Caso o bot�o devolver e o cancelar estejam disponiveis ao mesmo tempo
$('.btnDevolverSSM').on('click', function () {
    $("#motivoCancelamentoSSM").hide();
    $("#motivoDevolucaoSSM").show();
    $('select[name="motivoCancelamento"]').removeAttr("required");
    $('select[name="motivoDevolucao"]').attr("required", "required");
    nSsp.removeAttr("required");
});

$('select[name="btnDevolverSSM"]').on('change', function () {
    if ($(this).val() == "1") {
        $('#submitButton').prop('disabled', true);
        $('#numeroSafDuplicada').show();
        nSaf.attr("required", "required");
    } else {
        $('#submitButton').removeAttr('disabled');
        $('#numeroSafDuplicada').hide();
        nSaf.removeAttr("required");
    }
});

$('.btnCancelarSSM').on('click', function () {
    $("#motivoCancelamentoSSM").show();
    $("#motivoDevolucaoSSM").hide();
    $('select[name="motivoDevolucao"]').removeAttr("required");
    $('select[name="motivoCancelamento"]').attr("required", "required");
    nSaf.removeAttr("required");
});

$('select[name="motivoCancelamento"]').on('change', function () {
    if ($(this).val() == "2" || $(this).val() == "3") {
        $('#submitButton').prop('disabled', true);
        $('#numeroSspProgramada').show();
        nSsp.attr("required", "required");
    } else {
        $('#submitButton').removeAttr('disabled');
        $('#numeroSspProgramada').hide();
        nSsp.removeAttr("required");
    }
});

nSsp.on('blur', function () {
    $('#submitButton').prop('disabled', true);

    if ($(this).val() != "") {
        if ($('select[name="motivoCancelamento"]').val() == "2"){ // Programa��o
            console.log(('Entrou Programa��o'));
            var apiFunction = "Programada";
            var nomeAlerta = "Programa��o";
        }else if ($('select[name="motivoCancelamento"]').val() == "3"){ // Servi�o
            console.log(('Entrou servi�o'));
            var apiFunction = "Servico";
            var nomeAlerta = "Servi�o";
        }
        $.getJSON(
            caminho + "/api/returnResult/ssp/ssm"+apiFunction,
            {ssp: $(this).val()},
            function (json) {
                console.log(json);
                if (json.validade == false) {
                    alertaJquery("Alerta", 'Ssp n�o existe ou SSP n�o � de origem '+nomeAlerta, "alert");
                    nSsp.val('');
                    $('#submitButton').prop('disabled', true);
                } else {
                    console.log('teste');
                    $('#submitButton').removeAttr('disabled');
                }
            }
        );
    }
});

$('.encaminharEquipeSsm').on('click', function (e) {
    if (hasProcess(e)) return;
    var resultado = validacao_formulario($('#formSsm input'), $('#formSsm select'), $('#formSsm textarea'));

    if (resultado['permissao']) {
        $('#formSsm').submit();
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

$('.gerarOsm').on('click', function (e) {
    if (hasProcess(e)) return;
    var resultado = validacao_formulario($('#formSsm input'), $('#formSsm select'), $('#formSsm textarea'));

    if (resultado['permissao']) {
        alertaJquery('Alerta', 'Deseja realmente gerar uma OSM?', 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "Sim") {
                $('#formSsm').submit();
            }
            if (res == "N�o") {
                $('body').removeClass('processing');
            }
        });
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var codigoSsm = $('input[name="codigoSsm"]').val();

$(".btnImprimirFormulario").on("click", function () {
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/imprimirSsm",
        data: {codigoSsm: codigoSsm},
        success: function () {
            window.open(caminho + "/dashboardGeral/printSsm", "_blank");
        }
    });
});

var novoNivel = $('#nNovo');
var atualNivel = $('#nAtual');
var motivoMudancaNivel = $('#motivoMudancaNivel');


$('.modal-AlterarNivel').on('shown.bs.modal', function () {
  $.getJSON(
      caminho+"/ssm/hasSolicitation/"+codigoSsm,
      function (json ){
        if(json == 1)
        {
          alertaJquery("Duplicidade",
              "<span style='font-size: small'>J� existe uma solicita��o de altera��o de n�vel em aberto para esta SSM.</br>Entre em contato com o setor respons�vel.",
              "alert");

              $('.modal-AlterarNivel').modal('hide');
        }
      }
  )
})

$('#btnFormAlterNivel').click(function(e){
  e.preventDefault();

  if( (novoNivel.val() == atualNivel.val())){
      alertaJquery('Redund�ncia', 'Novo n�vel n�o pode ser igual o n�vel atual para realiza��o desta solicita��o.</br>Por favor, analisar.', 'alert');
      return;
  }

  if(novoNivel.val() == null)
  {
    alertaJquery('Redund�ncia', 'Novo n�vel precisa ser informado.</br>Por favor, analisar.', 'alert');
    return;
  }

  var name = motivoMudancaNivel.attr('name');
  if (!$.trim(motivoMudancaNivel.val()).length && name != undefined) {
      $(this).parent('div').addClass('has-error');
      alertaJquery('Campo Vazio', 'Campo de motivo n�o pode ser vazio.</br>Por favor, analisar.', 'alert');
      return;
  }

  $('#formAlterNivel').submit();

});

//Material Rodante
$('select[name="identificacaoMrSsm"]').change(function(){
    apiGetComposicaoPorTipo($(this), $('select[name="composicaoMrSsm"]'));
});

$('select[name="composicaoMrSsm"]').change(function () {
    apiGetTueComposicao($('select[name="composicaoMrSsm"] option:selected'), $('select[name="veiculoMrSsm"]'));
});