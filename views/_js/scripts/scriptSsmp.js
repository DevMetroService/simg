/**
 * Created by josue.marques on 24/02/2016.
 *
 */

$("input[name='dataHoraProgramada']").on('blur', function(){
    if($(this).attr("readonly") != undefined){
    }else{
        var tempo = $(this).val();
        if(tempo != "") {
            if (!validarHoraProgramada(tempo)) {
                alertaJquery('Erro em Data', 'Data programada n�o pode ser anterior a data atual.', 'alert');
                $(this).val("");
            }
        }
    }

});

$(".btnGerarOsmp").on('click',function (e) {
    if(hasProcess(e)) return;

    var tempo = $("input[name='dataHoraProgramada']");

    // if((!validarHoraProgramada(tempo.val()) && $('input[name="status"]').length && $('input[name="status"]').val() != "Pendente") ){
    //     alertaJquery('Opera��o Inv�lida', "Data programada n�o pode ser anterior a data atual.<h4>Entre em contato com a Engenharia de Manuten��o.</h4>", 'error');
    //     $('body').removeClass('processing');
    //
    //     return false;
    // }

    alertaJquery('Gerar OSMP',
        'Deseja realmente gerar uma OSMP?',
        'confirm', [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "Sim") {
                $("#formSsmp").attr("action", caminho + "/cadastroGeral/gerarOsmp");
                $('#formSsmp').submit();
            } else {
                $('body').removeClass('processing');
            }
        }
    );
});

$(".salvarSsmp").on('click',function (e) {
    if(hasProcess(e)) return;

    var verific = validacao_formulario($('#formSsmp input'), $('#formSsmp select'), $('#formSsmp textarea'));

    if (verific['permissao']) {
        $("#formSsmp").attr("action", caminho + "/cadastroGeral/salvarSsmp");
        $('#formSsmp').submit();
    }else{
        $('body').removeClass('processing');
    }
});

$(".programarSsmp").on('click',function (e) {
    if(hasProcess(e)) return;

    var verific = validacao_formulario($('#formSsmp input'), $('#formSsmp select'), $('#formSsmp textarea'));

    if (verific['permissao']) {
        alertaJquery('Programar SSMP',
            'Tem certeza que deseja programar?<h4>Verifique se todas as informa��es est�o corretas. N�o ser� poss�vel alter�-las posteriormente.</h4>',
            'confirm', [{value: "Sim"}, {value: "N�o"}], function (res) {
                if (res == "Sim") {
                    var tempo = $("input[name='dataHoraProgramada']");

                    if ((!validarHoraProgramada(tempo.val()))) {
                        alertaJquery('Erro em Data', "Data programada n�o pode ser anterior a data atual.", 'alert');
                        tempo.val("");
                        $('body').removeClass('processing');
                    } else {
                        $('#formSsmp').submit();
                    }
                } else {
                    $('body').removeClass('processing');
                }
            }
        );
    }else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

$(".btnImprimirSsmp").on("click", function(){
    var codigoSsmp = $('input[name="codigoSsmp"]').val();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsmp",
        data: {codigoSsmp: codigoSsmp},
        success: function() {
            window.open(caminho + "/dashboardGeral/printSsmp", "_blank");
        }
    });
});