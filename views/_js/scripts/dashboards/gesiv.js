var tbMaterialWaitAprove = $('#tbMaterialWaitAprove').DataTable(configTable(0, true, 'asc'));
var tbWaitMaterial = $('#tbWaitMaterial').DataTable(configTable(0, true, 'asc'));
var tbAlterLevelSsm = $('#tbAlterLevelSsm').DataTable(configTable(0, true, 'asc'));

var ctdAlterLevel = $('#ctdAlterNivel');

$('.tableDashboard').DataTable(configTable(0, true, 'asc'));

$(document).on("click", ".clickPop",function () {
    $(this).popover('show');
});

//Aprova��o de compra de material
$(document).on("click","#btnAprovaCompra", function(e){
    loader();
    e.preventDefault;
    var cod_ssm = $(this).data("ssm");
    var timeOrigin = $(this).data("time");
    var rowOrigin = tbMaterialWaitAprove.row( $(this).parents('tr') );

    var ctdAguardandoCompra = $('#ctdAguardandoCompra');
    var ctdAguardandoMaterial = $('#ctdAguardandoMaterial');

    var d = new Date();
    var month = d.getMonth()+1;
    var timeNow = d.getDate()+"-"+month+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    
    $.get(caminho + "/ssm/aproveCompraMaterial/"+cod_ssm,
    function(){
        //Remove da tabela de origem
        rowOrigin.remove().draw();  
        //Diminui Contador da tabela de origem
        ctdAguardandoCompra.html(ctdAguardandoCompra.html()-1);

        //Soma Contador da tabela de destino
        ctdAguardandoMaterial.html(Number(ctdAguardandoMaterial.html())+1);

        //recebe as informa��es de materiais desta SSM
        $.get(caminho+'/ssm/getMaterialSolicitacao/'+cod_ssm, 
        {time: timeOrigin},
        function(data){
            //Adiciona as informa��es na tabela se persistido em banco de dados
            tbWaitMaterial.row.add(
                [
                    cod_ssm, timeNow, data.button, 0
                ]
            ).draw();
        }, 'json');       
    }).success(function()
    {
        $.get(caminho+"/ssm/sendEmailAprove/"+cod_ssm);
        
        alertaJquery('Confirma��o', 'A <strong>COMPRA DE MATERIAIS</strong> da SSM ' + cod_ssm + ' foi aprovada!', 'info',[{value:'Ok'}]);
    }).done(function()
    {
        loading();
    });
});

//Campo de Justificativa
//Bot�o de aprova��o da justiifcativa
var btnReprovaCompra = $("#btnCompraNegada");
var justificativa = $('#taJustificativaCompraNegada');
var rowOrigin;

$(document).on("click",".repBtnCompra", function(e){
    rowOrigin = tbMaterialWaitAprove.row( $(this).parents('tr') );
    justificativa.val('');
});

$(document).on("click","#btnCompraNegada", function(e){
    //Verifica se a justificativa do campo foi preenchida corretamente.
    if (!$.trim(justificativa.val())){
        alertaJquery('Justifique sua a��o.', "� necess�rio o preenchimento do campo de justificativa. </br>", 'alert');
        return;
    }

    loader();
    e.preventDefault;
    var cod_ssm = $(this).data("ssm");

    var ctdAguardandoCompra = $('#ctdAguardandoCompra');
    
    $.get(caminho + "/ssm/repCompraMaterial/"+cod_ssm,
    {justificativa: justificativa.val()},
    function(data){
        //Remove da tabela de origem
        rowOrigin.remove().draw();  
        //Diminui Contador da tabela de origem
        ctdAguardandoCompra.html(ctdAguardandoCompra.html()-1);

    }).success(function()
    {
        alertaJquery('Confirma��o', 'A compra de <strong>Materiais</strong> da SSM ' + cod_ssm + ' foi negada!', 'info',[{value:'Ok'}]);

        $.get(caminho+"/ssm/sendEmailDesaprove/"+cod_ssm, function(data){console.log(data)});
        rowOrigin = null;
    }).done(function()
    {
        loading();
    });
});

$('#tbMaterialWaitAprove tbody').on( 'click', '.repBtnCompra', function () 
{
    btnReprovaCompra.data('ssm', $(this).data('ssm'));    
} );

$(document).on('click', '.btnAproveChangeLevel', function(e)
{
    loader();
    e.preventDefault;

    var value = $(this).data('value');
    var rowOrigin = tbAlterLevelSsm.row( $(this).parents('tr') );

    $.get(caminho + "/ssm/aprovarAlterNivel/" + value, function ()
    {
        //remove linha da tabela
        rowOrigin.remove().draw(); 
    }).success(function()
    {
        ctdAlterLevel.html(ctdAlterLevel.html()-1);
        alertaJquery('Confirma��o', 'Altera��o de n�vel realizada com sucesso.', 'info',[{value:'Ok'}]);
    }).done(function()
    {
        loading();
    });

});

$(document).on('click', '.btnReproveChangeLevel', function(e)
{
    loader();
    e.preventDefault;

    var value = $(this).data('value');
    var rowOrigin = tbAlterLevelSsm.row( $(this).parents('tr') );

    $.get(caminho + "/ssm/reprovarAlterNivel/" + value, function ()
    {
        //remove linha da tabela
        rowOrigin.remove().draw(); 
    }).success(function()
    {
        ctdAlterLevel.html(ctdAlterLevel.html()-1);
        alertaJquery('Confirma��o', 'Solicita��o de altera��o de n�vel recusada.', 'info',[{value:'Ok'}]);
    }).done(function()
    {
        loading();
    });
});

$(document).on('click','.btnValidacaoOpenModal', function()
{
    $('#modal-validador').modal('show');
    $('.modal').css("overflow-y", "auto");
});

$('#modal-validador').on("hidden.bs.modal", function()
{
    $('.aguardandoValidacao').modal('show');
});