var tbMaterial;

$('document').ready(function(){
    tbMaterial = $(".tbMaterial").DataTable(configTable(0, false, 'asc'));
});

//Organiza as colunas das tabelas de outras abas do bootstrap tabs
$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
    $.fn.DataTable.tables({ visible: true, api: true }).columns.adjust();
});

