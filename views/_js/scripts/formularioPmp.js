/**
 * Created by iramar.junior on 08/09/2016.
 */

//Auto preenchimento do subsistema ap�s sele��o do sistema
$('select[name="sistema"]').on("change", function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});

//Auto preenchimento do servi�o ap�s sele��o do subsistema
$('select[name="subSistema"]').on("change", function () {
    apiSistemaSubSistema($('select[name="sistema"]'), $(this), $('select[name="servicoPmp"]'));
});

//Auto preenchimento do tipo de manuten��o ap�s sele��o do servi�o
$('select[name="servicoPmp"]').on("change", function () {
    apiServicoPeriodicidade($(this), $('select[name="periodicidade"]'));
});

$('select[name="periodicidade"]').on("change", function () {
    apiPeriodicidadeProcedimento($(this), $('select[name="servicoPmp"]'), $('input[name="procedimento"]'));
});

//=========================Valida��es============================//

$('select[name="sistema"]').on('blur', function () {
    var sis = $('select[name="sistema"]').val();
    if (sis == null || sis == '' || sis == undefined) {
        $('select[name="sistema"]').val("");
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('select[name="subSistema"]').on('blur', function () {
    var subsis = $('select[name="subSistema"]').val();
    if (subsis == null || subsis == '' || subsis == undefined) {
        $('select[name="subSistema"]').val("");
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('select[name="servicoPmp"]').on('blur', function () {
    var ser = $('select[name="servicoPmp"]').val();
    if (ser == null || ser == '' || ser == undefined) {
        $('select[name="servicoPmp"]').val("");
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('select[name="periodicidade"]').on('blur', function () {
    var per = $('select[name="periodicidade"]').val();
    if (per == null || per == '' || per == undefined) {
        $('select[name="periodicidade"]').val("");
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('select[name="turno"]').on('blur', function () {
    var tur = $('select[name="turno"]').val();
    if (tur == null || tur == '' || tur == undefined) {
        $('select[name="turno"]').val('');
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('input[name="quinzena"]').on('blur', function () {
    var qui = $('input[name="quinzena"]').val();
    if (qui == '' || qui == null || qui == undefined) {
        $('input[name="quinzena"]').val('');
        $(this).parent('div').addClass('has-error');
    } else if (qui != '' && (qui < 1 || qui > 24)) {
        $('input[name="quinzena"]').val('');
        $(this).parent('div').addClass('has-error');
        alertaJquery("Alerta", "O n�mero da Quinzena n�o pode ser menor que 1 ou maior que 24", "alert");
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('input[name="maoObra"]').on("blur", function () {
    var mao = $('input[name="maoObra"]').val();
    if (mao == '' || mao == null || mao == undefined) {
        $('input[name="maoObra"]').val('');
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('input[name="horasUteis"]').on("blur", function () {
    var hor = $('input[name="horasUteis"]').val();
    if (hor == '' || hor == null || hor == undefined) {
        $('input[name="horasUteis"]').val('');
        $(this).parent('div').addClass('has-error');
    } else {
        $(this).parent('div').removeClass('has-error');
    }
});

$('input[name="horasUteis"], [name="maoObra"]').on("change", function () {

    var hu = $('input[name="horasUteis"]').val();
    var mo = $('input[name="maoObra"]').val();

    var hh = $('input[name="homemHora"]').val();

    hh = hu * mo;
    if (hh == 0) {
        $('input[name="homemHora"]').val('');
    } else {
        $('input[name="homemHora"]').val(hh);
    }

});
//==============================================================//

var tabelaPmpEd;
var tabelaPmpEdExec;

$('document').ready(function () {
    tabelaPmpEd = $("#tabelaPmpEd").dataTable(configTable(0, false, 'asc'));
    tabelaPmpEdExec = $("#tabelaPmpEdExec").dataTable(configTable(0, false, 'asc'));
});

var excluidosLinhasPmpEd = new Array();
var contador = 1;
var arrPmp;

//================= On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {

    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
    var servico_pmp = $('select[name="servicoPmp"]').val();
    var periodicidade = $('select[name="periodicidade"]').val();
    var turno = $('select[name="turno"]').val();
    var qzn = $('input[name="quinzena"]').val();
    var maoObra = $('input[name="maoObra"]').val();
    var horasUteis = $('input[name="horasUteis"]').val();

    if (sistema == '' || sistema == null || sistema == undefined ||
        subSistema == '' || subSistema == null || subSistema == undefined ||
        servico_pmp == '' || servico_pmp == null || servico_pmp == undefined ||
        periodicidade == '' || periodicidade == null || periodicidade == undefined ||
        turno == '' || turno == null || turno == undefined ||
        qzn == '' || qzn == null || qzn == undefined ||
        maoObra == '' || maoObra == null || maoObra == undefined ||
        horasUteis == '' || horasUteis == null || horasUteis == undefined) {

        alertaJquery("Alerta", "Preencha todos os campos corretamente", "alert");

    } else {
        //================ Dados a serem adicionados na LISTA de PMP Edifica��o. ==========================//
        arrPmp = [
            $('select[name="sistema"] option:selected').text(),
            $('select[name="subSistema"] option:selected').text(),
            $('select[name="servicoPmp"] option:selected').text(),
            $('select[name="periodicidade"] option:selected').text(),
            $('input[name="procedimento"]').val(),
            $('input[name="quinzena"]').val(),
            $('select[name="turno"] option:selected').text(),
            $('input[name="maoObra"]').val(),
            $('input[name="horasUteis"]').val(),
            $('input[name="homemHora"]').val()
        ];

        var txt = '';
        $('select[name="local[]"] option:selected').each(function () {

            var duplicado = true;

            var col = tabelaPmpEdExec._('tr');

            //Tabela do Banco de Dados
            for (var i = 0; i < col.length; i++) {
                var dataTable = tabelaPmpEdExec.fnGetData(i);

                if ((dataTable[1] == $(this).text()) && (dataTable[4] == arrPmp[2]) && dataTable[5] == arrPmp[3]) {
                    txt = txt + ' - O registro cod - ' + dataTable[0].split("<input")[0] + " Local, Servi�o e Periodicidade j� existe.\n";
                    duplicado = false;
                    break;
                }
            }

            col = tabelaPmpEd._('tr');

            //Tabela de Lista
            for (var i = 0; i < col.length; i++) {

                var dataTable = tabelaPmpEd.fnGetData(i);
                if ((dataTable[1].split("<input")[0] == $(this).text()) && (dataTable[4].split("<input")[0] == arrPmp[2]) && (dataTable[5].split("<input")[0] == arrPmp[3])) {
                    txt = txt + ' - O registro id - ' + dataTable[0] + " Local, Servi�o e Periodicidade j� existe.\n";
                    duplicado = false;
                    break;
                }
            }

            if (duplicado) {

                var disponivel = excluidosLinhasPmpEd.pop();

                if (disponivel == undefined) {

                    //==========Turno==========//
                    if ($('select[name="turno"] option:selected').val() == 'D') {
                        var turno =
                            '<select name="turno' + contador + '" class="form-control" required>' +
                            '<option value="D" selected>Diurno</option>' +
                            '<option value="N">Noturno</option>' +
                            '</select>';
                    } else {
                        var turno =
                            '<select name="turno' + contador + '" class="form-control" required>' +
                            '<option value="N" selected>Noturno</option>' +
                            '<option value="D">Diurno</option>' +
                            '</select>';
                    }
                    tabelaPmpEd.fnAddData([
                        contador,
                        //==========Local==========//
                        $(this).text() + '<input type="hidden" name="local' + contador + '" value="' + $(this).val() + '" class="form-control number" readonly>',
                        //==========Sistema==========//
                        arrPmp[0] + '<input type="hidden" name="sistema' + contador + '" value="' + $('select[name="sistema"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========SubSistema==========//
                        arrPmp[1] + '<input type="hidden" name="subSistema' + contador + '" value="' + $('select[name="subSistema"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========Servi�o==========//
                        arrPmp[2] + '<input type="hidden" name="servicoPmp' + contador + '" value="' + $('select[name="servicoPmp"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========Periodicidade==========//
                        arrPmp[3] + '<input type="hidden" name="periodicidade' + contador + '" value="' + $('select[name="periodicidade"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========Procedimento==========//
                        arrPmp[4],
                        //==========Qzn In�cio==========//
                        '<input type="text" name="quinzena' + contador + '" value="' + arrPmp[5] + '" class="form-control number">',
                        //==========Turno==========//
                        turno,
                        //==========M�o de Obra==========//
                        '<input name="maoObra' + contador + '" type="text"  value="' + arrPmp[7] + '" class="form-control number maoObra" required>',
                        //==========Horas �teis==========//
                        '<input name="horasUteis' + contador + '" type="text"  value="' + arrPmp[8] + '" class="form-control number horasUteis" required>',
                        //==========Homem Hora==========//
                        '<input name="homemHora' + contador + '" type="text"  value="' + arrPmp[9] + '" class="form-control" readonly>',
                        //==========A��es==========//
                        '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
                    ]);

                    contador = contador + 1;

                } else {

                    //==========Turno==========//
                    if ($('select[name="turno"] option:selected').val() == 'D') {
                        var turno =
                            '<select name="turno' + disponivel + '" class="form-control" required>' +
                            '<option value="D" selected>Diurno</option>' +
                            '<option value="N">Noturno</option>' +
                            '</select>';
                    } else {
                        var turno =
                            '<select name="turno' + disponivel + '" class="form-control" required>' +
                            '<option value="N" selected>Noturno</option>' +
                            '<option value="D">Diurno</option>' +
                            '</select>';
                    }

                    tabelaPmpEd.fnAddData([
                        disponivel,
                        //==========Local==========//
                        $(this).text() + '<input type="hidden" name="local' + disponivel + '" value="' + $(this).val() + '" class="form-control number" readonly>',
                        //==========Sistema==========//
                        arrPmp[0] + '<input type="hidden" name="sistema' + disponivel + '" value="' + $('select[name="sistema"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========SubSistema==========//
                        arrPmp[1] + '<input type="hidden" name="subSistema' + disponivel + '" value="' + $('select[name="subSistema"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========Servi�o==========//
                        arrPmp[2] + '<input type="hidden" name="servicoPmp' + disponivel + '" value="' + $('select[name="servicoPmp"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========Periodicidade==========//
                        arrPmp[3] + '<input type="hidden" name="periodicidade' + disponivel + '" value="' + $('select[name="periodicidade"] option:selected').val() + '" class="form-control number" readonly>',
                        //==========Procedimento==========//
                        arrPmp[4],
                        //==========Qzn In�cio==========//
                        '<input type="text" name="quinzena' + disponivel + '" value="' + arrPmp[5] + '" class="form-control number">',
                        //==========Turno==========//
                        turno,
                        //==========M�o de Obra==========//
                        '<input name="maoObra' + disponivel + '" type="text"  value="' + arrPmp[7] + '" class="form-control number maoObra" required>',
                        //==========Horas �teis==========//
                        '<input name="horasUteis' + disponivel + '" type="text"  value="' + arrPmp[8] + '" class="form-control number horasUteis" required>',
                        //==========Homem Hora==========//
                        '<input name="homemHora' + disponivel + '" type="text"  value="' + arrPmp[9] + '" class="form-control" readonly>',
                        //==========A��es==========//
                        '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
                    ]);

                    contador = contador + 1;
                }
            }

        });
        if (txt != '') {
            alertaJquery("Alerta", "DUPLICADOS:\n\n" + txt, "alert");
        }
    }
});
//=================================================//

var CCounterExec = $("input[name='contadorExec']").val();

//=================================================//

$("#tabelaPmpEd").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpEd = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaPmpEd._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaPmpEd.fnGetData(i);
        if (dataTable[0] == idPmpEd) {
            tabelaPmpEd.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmpEd.push(idPmpEd);
    contador = contador - 1;
});

$(document).on("blur", ".maoObra", function () {

    var td_moJs = $(this);
    var td_huJs = $(this).parent("td").next('td').find('input');
    var td_hhJs = $(this).parent("td").next('td').next('td').find('input');

    td_hhJs.val(td_moJs.val() * td_huJs.val());
    if (td_hhJs.val() == 0) {
        td_hhJs.val('');
    }

});

$(document).on("blur", ".horasUteis", function () {

    var td_moJs = $(this).parent("td").prev('td').find('input');
    var td_huJs = $(this);
    var td_hhJs = $(this).parent("td").next('td').find('input');

    td_hhJs.val(td_moJs.val() * td_huJs.val());
    if (td_hhJs.val() == 0) {
        td_hhJs.val('');
    }

});

$('document').ready(function () {
    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"

    });
});

// $(window).load(function() {
//     document.getElementById("loading").style.display = "none";
//     document.getElementById("conteudo").style.display = "inline";
// })

$('.salvarListaPmp').click(function () {
    var data = tabelaPmpEd.$('input, select').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaPmpEdExec.$('input, select').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + CCounterExec;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    if (arrPmp) {
        $.post(
            caminho + "/cadastroGeral/refillPmpEd",
            {
                sistema: $('select[name="sistema"] option:selected').val(),
                subSistema: $('select[name="subSistema"] option:selected').val(),
                servicoPmp: $('select[name="servicoPmp"] option:selected').val(),
                periodicidade: $('select[name="periodicidade"] option:selected').val(),
                procedimento: $('input[name="procedimento"]').val(),
                quinzena: $('input[name="quinzena"]').val(),
                turno: $('select[name="turno"] option:selected').val(),
                maoObra: $('input[name="maoObra"]').val(),
                horasUteis: $('input[name="horasUteis"]').val(),
                homemHora: $('input[name="homemHora"]').val()
            }
        );
    }

    if (data) {
        $.post(
            caminho + "/cadastroGeral/gerarPmpEd",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }

    // Loading da p�gina
    // $(window).load(function() {
    //     document.getElementById("loading").style.display = "none";
    //     document.getElementById("conteudo").style.display = "inline";
    // })

});