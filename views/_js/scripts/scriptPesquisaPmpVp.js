$.getScript(caminho + "/views/_js/scripts/scriptPmp.js", function () {
    $(document).ready(function () {
        $("#tabelaPesquisaPmp").dataTable(configTable(0, false, 'asc'));
    });
});

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});

$('.btnResetarPesquisa').on('click', function () {
    window.location.href = caminho + "/dashboardPmp/resetarPesquisaPmpVp";
});

$('.btnAtivarDepreciarPmp').on('click', function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    var pmp     = dados.split('<input')[0];

    alertaJquery('Alerta', "Deseja realizar esta a��o com o PMP cod " + pmp + " ?", 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
        if (res == "Sim") {
            $.ajax({                                                                        //inicia o ajax
                type: "POST",
                url: caminho + "/cadastroGeral/refillPmp",                               //informa o caminho do processo
                data: {codPmp: pmp},                                                            //informa os dados ao ajax
                success: function () {                                                           //em caso de sucesso imprime o retorno
                    window.location.replace(caminho + "/dashboardPmp/depreciarPmpVp");
                }
            });
        }
    });
});

var tableModal = $(".tableRelModal").dataTable(configTable(0, false, 'asc'));

$(document).on("click",".abrirModal", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var pmp     = dados.split('<input')[0];
    var grupo   = dados.split('value="')[1].split('"')[0];

    apiPmpCronograma(pmp, grupo);
});