/**
 * Created by josue.marques on 11/02/2016.
 *
 */

//############## A��es SAF
$(document).on("click",".btnAutorizarSaf", function(e){
    if(hasProcess(e)) return;

    e.preventDefault();
    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();         //dados recebe o formulario em string
    $.ajax({                                                                 //inicia o ajax
        type: "POST",
        url: caminho+"/cadastroGeral/gerarSsm/",                        //informa o caminho do processo
        data: {codigoSaf: dados},                                            //informa os dados ao ajax
        success: function() {                                                //em caso de sucesso imprime o retorno
            window.location.replace(caminhoReturn);
        }
    });
});

$(document).on("click",".btnEditarSaf", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSaf",
        data: {codigoSaf: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/saf");
        }
    });
});

$(document).on("click",".btnImprimirSaf", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirSaf",
        data: {codigoSaf: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printSaf", "_blank");
        }
    });
});

//############## A��es SSM


$(document).on("click",".btnEditarSsm", function(e){
    if(hasProcess(e)) return;


    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsm",
        data: {codigoSsm: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/ssm");
        }
    });
});

$(document).on("click",".btnImprimirSsm", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirSsm",
        data: {codigoSsm: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printSsm", "_blank");
        }
    });
});

//############## A��es OSM
$(document).on("click",".btnEditarOsm", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsm",
        data: {codigoOs: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/osm");
        }
    });
});

$(document).on("click",".btnImprimirOsm", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirOsm",
        data: {codigoOs: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printOsm", "_blank");
        }
    });
});

//############## A��es SSP
$(document).on("click",".btnEditarSsp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsp",
        data: {codigoSsp: dados},
        success: function(data) {
            window.location.replace(caminho+"/dashboardGeral/ssp");
        }
    });
});

$(document).on("click",".btnImprimirSsp", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirSsp",
        data: {codigoSsp: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printSsp", "_blank");
        }
    });
});

//############## A��es OSP
$(document).on("click",".btnEditarOsp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsp",
        data: {codigoOs: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/osp");
        }
    });
});

$(document).on("click",".btnImprimirOsp", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirOsp",
        data: {codigoOs: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printOsp", "_blank");
        }
    });
});

//############## A��es SSMP
$(document).on("click",".btnEditarSsmp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');

    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsmp",
        data: {codigoSsmp: dados},
        success: function(data) {
            window.location.replace(caminho+"/dashboardGeral/ssmp/"+form);
        }
    });
});

$(document).on("click",".btnReprogramar", function(e){
    if(hasProcess(e)) return;

    var dados = $(this).parent("td").parent("tr").find('td').html().split('<input')[0];
    var form = $(this).val();

    alertaJquery('Alerta', 'Deseja realmente REPROGRAMAR o Cronograma?', 'alert', [{value: "Sim"}, {value: "Nao"}], function (res) {
        if (res == "Sim") {
            $(this).attr('disabled', 'disabled');

            $.ajax({
                type: "POST",
                url: caminho+"/cadastroGeral/gerarReprogramada",
                data: {codCronograma: dados, form: form},
                success: function(data) {
                    window.location.reload();
                }
            });
        }
        if (res == "Nao") {
            $('body').removeClass('processing');
        }
    });
});

$(document).on("click",".btnImprimirSsmp", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirSsmp",
        data: {codigoSsmp: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printSsmp/"+form, "_blank");
        }
    });
});

//############## A��es OSMP

$(document).on("click",".btnEditarOsmp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');

    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsmp",
        data: {codigoOs: dados},
        success: function(data) {
            window.location.replace(caminho+"/dashboardGeral/osmp/"+form);
        }
    });
});

$(document).on("click",".btnImprimirOsmp", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form = $(this).val();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirOsmp",
        data: {codigoOs: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printOsmp/"+form, "_blank");
        }
    });
});
