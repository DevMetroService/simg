
var tableSsp;
var tableOsp;

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tableSsp  = $("#indicadorSsp").dataTable(configTable(0, false, 'desc'));
        tableOsp  = $("#indicadorOspExecucao").dataTable(configTable(0, false, 'desc'));
    });
});

$(".btnAbrirSspCalendario").on("click", function(){
    var dados = $('input[name="codigoSspCallendar"]').val();                                            //dados recebe o formulario em string
    $.ajax({                                                               //inicia o ajax
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsp",                      //informa o caminho do processo
        data: { codigoSsp: dados},                                         //informa os dados ao ajax
        success: function() {                                          //em caso de sucesso imprime o retorno
            window.location.replace(caminho+"/dashboardGeral/Ssp");
        }
    });
    return false;
});

$(".btnAbrirOspCalendario").on("click", function(){
    var codigoSsp = $('input[name="codigoSspCallendar"]').val();                                            //dados recebe o formulario em string

    window.location.replace(caminho + "/dashboardGeral/pesquisaSspParaOsp/" + codigoSsp);
    return false;
});

$(document).ready(function() {
    $('#calendarioProgramacaoCCM').fullCalendar({
        events: caminho+'/api/getResult/getSspCalendario',
        header: {
            left: 'title',
            center: '',
            right: 'prev next today'
        },
        lang: 'pt-br',
        eventLimit: true,
        eventClick:  function(event, jsEvent, view) {
            $('#modalTitleCalendarioProgramacaoCCM').html(event.title);
            $('#modalBodyCalendarioProgramacaoCCM').html(event.description);
            $('#modalCalendarioProgramacaoCCM').modal();
        }
    });

});