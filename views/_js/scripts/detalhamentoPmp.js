/**
 * Created by ricardo.diego on 24/02/2016.
 */

$('document').ready(function () {
   $(".detalhamentoPmp").dataTable(configTable(1));
});

$('.local').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: caminho + "/dashboardSupervisao/filtroPmp",                                  //informa o caminho do processo
        data: {filtro: $(this).html(), where: "inicial.nome_estacao"},                          //informa os dados ao ajax
        success: function () {                                                          //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardSupervisao/detalhamentoPmp");
        }
    });
});

$('.servico').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: caminho + "/dashboardSupervisao/filtroPmp",                                //informa o caminho do processo
        data: {filtro: $(this).html(), where: "nome_servico_prev"},                   //informa os dados ao ajax
        success: function () {                                                        //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardSupervisao/detalhamentoPmp");
        }
    });
});

$('.procedimento').on('click', function (e) {
    e.preventDefault();
    $.ajax({                                                                         //inicia o ajax
        type: "POST",
        url: caminho + "/dashboardSupervisao/exibirPdf",                                 //informa o caminho do processo
        data: {nomeArquivo: $(this).html()+'.pdf'},                                        //informa os dados ao ajax
        success: function () {                                                        //em caso de sucesso imprime o retorno
            window.open(caminho + "/dashboardSupervisao/exibirPdf", "_blank");
        }
    });
});

$('.sistema').on('click', function (e) {
    e.preventDefault();
    $.ajax({                                                                            //inicia o ajax
        type: "POST",
        url: caminho + "/dashboardSupervisao/filtroPmp",                                    //informa o caminho do processo
        data: {filtro: $(this).html(), where: "sigla"},                         //informa os dados ao ajax
        success: function () {                                                           //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardSupervisao/detalhamentoPmp");
        }
    });
});

$('.periodicidade').on('click', function (e) {

    e.preventDefault();
    var href = $(this).attr('href');
    $.ajax({                                                                             //inicia o ajax
        type: "POST",
        url: caminho + "/dashboardSupervisao/filtroPmp",                                   //informa o caminho do processo
        data: {filtro: href, where: "quant_intervalos"},                                 //informa os dados ao ajax
        success: function () {                                                           //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardSupervisao/detalhamentoPmp");
        }
    });
});

$('#execModal').on('show.bs.modal', function(event){

    var dados = $(this).parent("td").parent("tr");
    var header =  $('#headerModalExec');
    var button  = $(event.relatedTarget);
    var acao    = button.data('action');
    if(acao == 18){
        header.html("Execu��o - 1� Quinzena de JANEIRO");
        $('#12').html('<a class=".osp" onclick="abrirOsm(8)" href="#">8</a>');
    }else{
        if(acao == 20 ){
            header.html("Execu��o - 1� Quinzena de JANEIRO");
            $('#12').html('<a class=".osp" onclick="abrirOsm(10)" href="#">10</a>');
        }else{
            header.html('');
            $('#12').html('');
        }
    }

});

function abrirOsm(dados) {
    $.ajax({                                                                             //inicia o ajax
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsp",                                  //informa o caminho do processo
        data: {codigoOsp: dados},
        success: function () {                                                           //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardGeral/osp");
        }
    });
}
