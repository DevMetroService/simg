/**
 * Created by ricardo.diego on 10/05/2016.
 */

$(".indicadorFuncionario").dataTable(configTable(1, false, 'asc'));

$(document).on('click','.btnEditarFuncionario', function(e){
    if(hasProcess(e)) return;

    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastro/cadastroSimplesFuncionarioRefill",
        data: {matricula: dados},
        success: function() {
            window.location.replace(caminho+"/cadastro/cadastroSimplesFuncionario");
        },
        error: function(){
            alertaJquery("Alerta", 'erro', 'error');
        }
    });
});

$(document).on('click','.btnEditarPj', function(e){
    if(hasProcess(e)) return;
    var dados = $(this).parent("td").parent("tr").find('td').html();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastro/cadastroExternoRefill",
        data: {matricula: dados},
        success: function() {
            window.location.replace(caminho+"/cadastro/cadastroExterno");
        },
        error: function(){
            alertaJquery("Alerta", 'erro', 'error');
        }
    });
});
