/**
 * Created by ricardo.diego on 04/12/2017.
 */

var tabelaPmpMr;
var tabelaPmpMrExec;

$.getScript(caminho + "/views/_js/scripts/scriptPmp.js", function(){ // include functions
    $(document).ready(function(){
        tabelaPmpMr = $("#tabelaPmpMr").dataTable(configTable(0, false, 'asc'));

        tabelaPmpMrExec = $("#tabelaPmpMrExec").dataTable(configTable(0, false, 'asc'));
    });
});

//***********************************Proprios do formularios

// MultiSelect Local
$('document').ready(function () {
    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"

    });
});

//***********************************Proprios do formularios


var excluidosLinhasPmp = new Array();
var contador = 1;

//================= On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {

    var sistema         = $('select[name="sistema"] option:selected');
    var subSistema      = $('select[name="subSistema"] option:selected');
    var servicoPmp      = $('select[name="servicoPmp"] option:selected');
    var periodicidade   = $('select[name="periodicidade"] option:selected');
    var procedimento    = $('input[name="procedimento"]');
    var qzn             = $('input[name="quinzena"]');

    if (sistema.val() == ''       || sistema.val() == null          || sistema.val() == undefined ||
        subSistema.val() == ''    || subSistema.val() == null       || subSistema.val() == undefined ||
        servicoPmp.val() == ''    || servicoPmp.val() == null       || servicoPmp.val() == undefined ||
        periodicidade.val() == '' || periodicidade.val() == null    || periodicidade.val() == undefined ||
        qzn.val() == ''           || qzn.val() == null              || qzn.val() == undefined)
    {

        alertaJquery("Alerta", "Preencha todos os campos corretamente", "alert");

    } else {
        //================ Dados a serem adicionados na LISTA de PMP Edifica��o. ==========================//
        var txt = '';
        $('select[name="local[]"] option:selected').each(function () {
            var duplicado = true;
            var col = tabelaPmpMrExec._('tr');

            //Tabela do Banco de Dados
            for (var i = 0; i < col.length; i++) {
                var dataTable = tabelaPmpMrExec.fnGetData(i);

                if ((dataTable[1] == $(this).text()) &&
                    (dataTable[2] == sistema.text()) &&
                    (dataTable[3] == subSistema.text()) &&
                    (dataTable[4] == servicoPmp.text()) &&
                    (dataTable[5] == periodicidade.text())) {

                    txt = txt + ' - cod - ' + dataTable[0].split("<input")[0] + " Duplicado no Banco de Dados.\n";
                    duplicado = false;
                    break;
                }
            }

            col = tabelaPmpMr._('tr');

            //Tabela de Lista
            for (var i = 0; i < col.length; i++) {

                var dataTable = tabelaPmpMr.fnGetData(i);

                if ((dataTable[1].split("<input")[0] == $(this).text()) &&
                    (dataTable[2].split("<input")[0] == sistema.text()) &&
                    (dataTable[3].split("<input")[0] == subSistema.text()) &&
                    (dataTable[4].split("<input")[0] == servicoPmp.text()) &&
                    (dataTable[5].split("<input")[0] == periodicidade.text())) {

                    txt = txt + ' - id - ' + dataTable[0] + " Duplicado na Lista.\n";
                    duplicado = false;
                    break;
                }
            }

            if (duplicado) {
                var disponivel = excluidosLinhasPmp.pop();

                if (disponivel == undefined)
                    var ctdAux = contador;
                else
                    var ctdAux = disponivel;

                tabelaPmpMr.fnAddData([
                    ctdAux,
                    //==========Local==========//
                    $(this).text() + '<input type="hidden" name="veiculo' + ctdAux + '" value="' + $(this).val() + '" class="form-control number" readonly>',
                    //==========Sistema==========//
                    sistema.text() + '<input type="hidden" name="sistema' + ctdAux + '" value="' + sistema.val() + '" class="form-control number" readonly>',
                    //==========SubSistema==========//
                    subSistema.text() + '<input type="hidden" name="subSistema' + ctdAux + '" value="' + subSistema.val() + '" class="form-control number" readonly>',
                    //==========Servi�o==========//
                    servicoPmp.text() + '<input type="hidden" name="servicoPmp' + ctdAux + '" value="' + servicoPmp.val() + '" class="form-control number" readonly>',
                    //==========Periodicidade==========//
                    periodicidade.text() + '<input type="hidden" name="periodicidade' + ctdAux + '" value="' + periodicidade.val() + '" class="form-control number" readonly>',
                    //==========Procedimento==========//
                    procedimento.val(),
                    //==========Qzn In�cio==========//
                    '<input type="text" name="quinzena' + ctdAux + '" value="' + qzn.val() + '" class="form-control number">' +
                    '<input type="hidden" name="grupo' + ctdAux + '" value="22" class="form-control number">',
                    //==========A��es==========//
                    '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
                ]);

                contador = contador + 1;
            }
        });
        if (txt != '')
            alertaJquery("Alerta", "DUPLICADOS:\n\n <textarea rows='20' cols='30'>" + txt + "</textarea>", "alert");
    }
});
//=================================================//

var contadorExec = $("input[name='ctd']").val();

//=================================================//

$("#tabelaPmpMr").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpMr = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaPmpMr._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaPmpMr.fnGetData(i);
        if (dataTable[0] == idPmpMr) {
            tabelaPmpMr.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmp.push(idPmpMr);
    contador = contador - 1;
});

$('.salvarListaPmp').click(function () {
    var data = tabelaPmpMr.$('input, select').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaPmpMrExec.$('input, select').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + contadorExec;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    $.post(
        caminho + "/cadastroGeral/refillPmp",
        {
            sistema:        $('select[name="sistema"] option:selected').val(),
            subSistema:     $('select[name="subSistema"] option:selected').val(),
            servicoPmp:     $('select[name="servicoPmp"] option:selected').val(),
            periodicidade:  $('select[name="periodicidade"] option:selected').val(),
            procedimento:   $('input[name="procedimento"]').val(),
            quinzena:       $('input[name="quinzena"]').val()
        }
    );

    if (data) {
        $.post(
            caminho + "/cadastroGeral/salvarListaPmp",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }
});

$('.gerarPmpAnual').click(function () {
    alertaJquery('Gerar PMP Anual',
        'Tem certeza que deseja finalizar o PMP Anual?<h4>Verifique antes se todas as altera��es foram salvas.</h4>',
        'alert', [{value:"Sim"},{value:"N�o"}], function (res) {
            if(res == "Sim") {
                alertaJquery('Gerar PMP Anual',
                    '<h3>Esta a��o n�o poder� ser desfeita!</h3>Deseja continuar?',
                    'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "Sim") {
                            loader();
                            window.location.replace(caminho + "/cadastroGeral/gerarPmpAnualMr");
                        }
                    }
                );
            }
        }
    );
});
