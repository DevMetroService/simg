/**
 * Created by abner.andrade on 27/12/2017.
 */


/**
 * CONFIGURACOES DO LAYOUT
 * */

//  Layout da Tabela
var tabela, tabela_modal_visualizar;
$(document).ready(function () {
    tabela = $('#tabela').dataTable(configTable(0, false, 'desc'));
    tabela_modal_visualizar = $('#tabela_modal_visualizar').dataTable(configTable(0, false, 'desc'));

    //  Legendas Tooltip para os botoes
    $('.btnToolTip').tooltip({ trigger: "hover" });
    
});





/**
 * CODE
 * */

$('#btnResetarPesquisa').click(function () {
    var filtros = ['nameMaterial','categoria', 'areaResponsavel', 'unidade'];
    var nfiltros = filtros.length;

    for(var i = 0; i <= nfiltros; i++) {
        var tipo = document.getElementById(filtros[i]).tagName;

        if (tipo == 'SELECT') {
            document.getElementById(filtros[i]).selectedIndex = -1;
        } else {
            document.getElementById(filtros[i]).value = "";
        }

    }
});
