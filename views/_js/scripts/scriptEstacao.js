$(document).ready(function(){
    $("#estacaoTable").DataTable(configTable(0, false, 'asc'));

    var linha = $('#linha');
    var estacao = $('#estacao');

    var form = $("#formulario");

    $(document).on("click",".editEstacao", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/estacao/update/'+$(this).data('codigo'));

        var row = $(this).parent("td").parent("tr").find('td').next();

        linha.val(row.data('linha'));

        estacao.val(row.next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/estacao/store');
    });

});