$(document).ready(function(){

    var form = $('#formDesaprovar');
    var btnAprove = $('#btnDesaprovar');

    btnAprove.click(function(e){
        e.preventDefault();

        if($('#motivoInvalidar').val().trim()){
            form.submit();
        }else{
            alertaJquery("Motivo", "O motivo da n�o valida��o � obrigat�rio o preenchimento.", "alert");
        }
    })

})
