
var tableSafD;
var tableSaf;
var tableSsmA;
var tableSsmD;
var tableSsmE;
var tableOsm;

$.getScript(caminho + "/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tableSafD  = $("#indicadorSafDevolvida").dataTable(configTable(0, false, 'desc'));

        tableSaf  = $("#indicadorSaf").dataTable(configTable(3, false, 'asc'));

        tableSsmA = $("#indicadorSsmAberta").dataTable(configTable(3, false, 'asc'));

        tableSsmD = $("#indicadorSsmDevolvida").dataTable(configTable(0, false, 'desc'));

        tableSsmE = $("#indicadorSsmEncaminhada").dataTable(configTable(0, false, 'desc'));

        tableOsm  = $("#indicadorOsmExecucao").dataTable(configTable(0, false, 'desc'));


        //$("#indicadorAuditoriaSaf").dataTable(configTable(0, false, 'desc'));
    });
});

//####################### Websocket Conex�o ###########################
//####################### Websocket Conex�o ###########################
//####################### Websocket Conex�o ###########################

$('document').ready(function(){
    try {
        socket.on("functionCcm", function (data) {
            //Inserir linha na tabela dinamicamente
            switch (data.tabela) {
                case "Saf":
                    atualizarTableSaf(data);
                    break;

                case "Ssm":
                    atualizarTableSsm(data);
                    break;

                case "Osm":
                    atualizarTableOsm(data);
                    break;
            }
            // Renovar Tooltip
            $('[rel="tooltip-wrapper"]').tooltip();
        });
    }catch(e){
        console.log(e);
    }
});

function atualizarTableSaf(data){
    var btn =  '<button class="btn btn-success btn-circle btnAutorizarSaf" type="button" id="btnAutorizarSaf" title="Autorizar"><i class="fa fa-check fa-fw"></i></button>';
        btn += '<button class="btn btn-default btn-circle btnEditarSaf" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ';
        btn += '<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Inserir"){
        tableSaf.fnAddData([
            data.numero,
            data.local,
            data.dataAbertura,
            data.nivel,
            btn
        ]);
        $('#quantSafCcm').html(parseInt($('#quantSafCcm').html()) + 1);

    }else {
        var col = tableSaf._('tr');
        for(var i=0; i < col.length ; i++){
            var dataTable = tableSaf.fnGetData(i);

            if(dataTable[0] == data.numero) {
                if(data.tipo == "Deletar") {
                    tableSaf.fnDeleteRow(i);

                    $('#quantSafCcm').html(parseInt($('#quantSafCcm').html()) - 1);
                    break;
                }else { // alterar
                    tableSaf.fnUpdate(
                        [
                            data.numero,
                            data.local,
                            data.dataAbertura,
                            data.nivel,
                            btn
                        ], i
                    );
                    break;
                }
            }
        }
    }
}

function atualizarTableSsm(data){

    var btn =  '<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ';
        btn += '<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Aberta"){
        var col = tableSaf._('tr');

        for(var i=0; i < col.length ; i++){
            var dataTable = tableSaf.fnGetData(i);
            if (dataTable[0] == data.numeroSaf) {
                tableSaf.fnDeleteRow(i);
                $('#quantSafCcm').html(parseInt($('#quantSafCcm').html()) - 1);
                break;
            }
        }

        tableSsmA.fnAddData([
            data.numero,
            data.numeroSaf,
            data.dataAbertura,
            data.nivel,
            btn
        ]);

        $('#quantSsmCcm').html(parseInt($('#quantSsmCcm').html()) + 1);

    }else if(data.tipo == "Alterar"){
        var col;

        switch (data.status){
            case "Aberta":
                col = tableSsmA._('tr');
                for(var i=0; i < col.length ; i++){
                    var dataTable = tableSsmA.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmA.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.dataAbertura,
                                data.nivel,
                                btn
                            ], i
                        );
                        break;
                    }
                }
                break;
            case "Devolvida":
            case "Transferida":
                col = tableSsmD._('tr');
                for(var i=0; i < col.length ; i++){
                    var dataTable = tableSsmD.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmD.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.dataAbertura,
                                data.statusMotivo,
                                data.nivel,
                                btn
                            ], i
                        );
                        break;
                    }
                }
                break;
            case "Encaminhado":
                col = tableSsmE._('tr');
                for(var i=0; i < col.length ; i++){
                    var dataTable = tableSsmE.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmE.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.encaminhamento,
                                data.servico,
                                data.equipe,
                                data.nivel,
                                btn
                            ], i
                        );
                        break;
                    }
                }
                break;
        }

    }else if(data.tipo == "Cancelar" || data.tipo == "Devolver"){
        var col;

        switch (data.statusAntigo) {
            case "Aberta":
                col = tableSsmA._('tr');
                for (var i = 0; i < col.length; i++) {
                    var dataTable = tableSsmA.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmA.fnDeleteRow(i);
                        $('#quantSsmCcm').html(parseInt($('#quantSsmCcm').html()) - 1);
                        break;
                    }
                }
                break;

            case "Devolvida":
            case "Transferida":
                col = tableSsmD._('tr');
                for (var i = 0; i < col.length; i++) {
                    var dataTable = tableSsmD.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmD.fnDeleteRow(i);
                        break;
                    }
                }
                break;
            case "Encaminhado":
                col = tableSsmE._('tr');
                for (var i = 0; i < col.length; i++) {
                    var dataTable = tableSsmE.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmE.fnDeleteRow(i);
                        break;
                    }
                }
                break;
        }

        if(data.tipo == "Devolver"){
            tableSsmD.fnAddData([
                data.numero,
                data.numeroSaf,
                data.dataAbertura,
                data.statusMotivo,
                data.nivel,
                btn
            ]);
        }

    }else if(data.tipo == "Encaminhar"){
        var col;

        switch (data.statusAntigo) {
            case "Aberta":
                col = tableSsmA._('tr');
                for (var i = 0; i < col.length; i++) {
                    var dataTable = tableSsmA.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmA.fnDeleteRow(i);
                        $('#quantSsmCcm').html(parseInt($('#quantSsmCcm').html()) - 1);
                        break;
                    }
                }
                break;

            case "Devolvida":
            case "Transferida":
                col = tableSsmD._('tr');
                for (var i = 0; i < col.length; i++) {
                    var dataTable = tableSsmD.fnGetData(i);
                    if (dataTable[0] == data.numero) {
                        tableSsmD.fnDeleteRow(i);
                        break;
                    }
                }
                break;
        }

        tableSsmE.fnAddData([
            data.numero,
            data.numeroSaf,
            data.encaminhamento,
            data.servico,
            data.equipe,
            data.nivel,
            btn
        ]);
    }
}

function atualizarTableOsm(data){
    var btn =  '<button class="btn btn-default btn-circle btnEditarOsm" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ';
        btn += '<button class="btn btn-default btn-circle btnImprimirOsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Aberta"){
        var col = tableSsmE._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableSsmE.fnGetData(i);
            if (dataTable[0] == data.numeroSsm) {
                tableSsmE.fnDeleteRow(i);
                break;
            }
        }

        tableOsm.fnAddData([
            data.numero,
            data.numeroSsm,
            data.dataAbertura,
            data.equipe,
            data.local,
            data.nivel,
            btn
        ]);
    }else if(data.tipo == "Deletar"){
        var col = tableOsm._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableOsm.fnGetData(i);
            if (dataTable[0] == data.numero) {
                tableOsm.fnDeleteRow(i);
                break;
            }
        }

        if(data.transferencia){
            var btn =  '<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ';
                btn += '<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

            tableSsmD.fnAddData([
                data.transferencia,
                data.transferenciaSaf,
                data.transferenciaData,
                data.transferenciaStatus,
                data.transferenciaNivel,
                btn
            ]);
        }
    }
}
