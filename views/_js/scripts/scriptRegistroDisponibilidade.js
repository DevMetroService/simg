var bodyTable = $(".table").DataTable( {
    "searching": false,
    "info":false,
    "language": {
        "lengthMenu": "Visualidado _MENU_ registros por p�gina",
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",

        "paginate": {
            "first": "Primeiro",
            "last": "Ultimo",
            "next": "Pr�ximo",
            "previous": "Anterior"
        },
        "loadingRecords": "Carregando...",
        "infoEmpty": "N�o h� informa��es"
    }
} );
var veiculo = $('#nomeVeiculo');
var linha = $('#nomeLinha');
var grupo = $('#nomeGrupo');
var frota = $('#disponibilidade');
var obs = $('#observacao');

frota.change(function(){
    if($(this).val() == 's'){
        obs.show();
    }else{
        obs.hide();
    }
});

$(document).on("click",".editFrota", function(e){
    e.preventDefault();
    var dados = $(this).parent("td").parent("tr").find('td');

    veiculo.val(dados.html());
    linha.val(dados.next().html());
    grupo.val(dados.next().next().html());
    if(dados.next().next().next().html() != "{Sem Registro}")
        frota.val(dados.next().next().next().html());
});

$('#btnSave').click(function(e){
    e.preventDefault();
    if(veiculo.val() !== '' && frota.val() !== ''){
        $('#formFrota').submit()
    }else{
        alertaJquery('Sem informa��o', 'N�o h� informa��o para registro em tabela', 'error');
    }
});

