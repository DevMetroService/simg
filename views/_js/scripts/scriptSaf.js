/**
 * Created by josue.marques on 24/02/2016.
 * Atualizado por Ricardo.Hernandez em 05/01/2021
 */
$(document).ready(function(){

    var dataListSolicitande = $("#nomeSolicitanteDataList");
    var tipoFuncionario = $('#codTipoFuncionario');
    var matriculaSolicitante = $('#matriculaSolicitante');
    var nomeSolicitante = $("#nomeSolicitante");
    var cpfSolicitante = $("#cpfSolicitante");
    var contatoSolicitante = $("#contatoSolicitante");
    var nivel = $('#nivel');

    //Origem de solicitante
    tipoFuncionario.on("change", function () {						//Din�mica para visualizar campos de entrada para Ensino Superior e/ou Curso T�cnico.
        //Atualiza datalist segundo origem solicitante
        var cod_origem = $(this).val();
        $.getJSON(
            caminho + "/api/returnResult/dados/getFuncionariosByTipo",
            {dados: cod_origem},
            function (json) {
                var option = "";
                $.each(json, function (key, value) {
                    var funcionario = value;
                    option += ('<option id="' + funcionario.matricula + '" value="' + funcionario.nome_funcionario + ' - ' + funcionario.matricula + '"/>').toUpperCase();
                });
                dataListSolicitande.html(option);
            }
        );
        matriculaSolicitante.val('');
        nomeSolicitante.val('');
        cpfSolicitante.val('');
        contatoSolicitante.val('');

    });

    //Consulta de matr�cula de funcionario.
    matriculaSolicitante.on("blur", function () {
        apiMatriculaFuncionario(matriculaSolicitante, nomeSolicitante
            , cpfSolicitante, contatoSolicitante, tipoFuncionario);
    });

    //AutoComplete nome solicitante
    nomeSolicitante.on("input", function () {
        var valueFinal = dataListSolicitande.find("option[value='" + nomeSolicitante.val() + "']");
        if (valueFinal.val() != null) {

            var matSolicitante = matriculaSolicitante;
            matSolicitante.val(valueFinal.attr('id'));

            apiMatriculaFuncionario(matriculaSolicitante, nomeSolicitante
                        , cpfSolicitante, contatoSolicitante, tipoFuncionario);
        }
    });

    cpfSolicitante.blur(function () {
        if(
            $(this).val() == '000.000.000-00' ||
            $(this).val() == '111.111.111-11' ||
            $(this).val() == '222.222.222-22' ||
            $(this).val() == '333.333.333-33' ||
            $(this).val() == '444.444.444-44' ||
            $(this).val() == '555.555.555-55' ||
            $(this).val() == '666.666.666-66' ||
            $(this).val() == '777.777.777-77' ||
            $(this).val() == '888.888.888-88' ||
            $(this).val() == '999.999.999-99'
        ){
            cpfSolicitante.val('');
        }
    });

    var linha = $('#linha');
    var trecho = $('#trechoSaf');
    var pontoNotavel = $('#pontoNotavel');
    var carro = $('#carroMrSaf');
    var veiculo = $('#veiculoMrSaf');
    var odometroField = $('#odometroMrSaf');
    var prefixo = $('#prefixoMrSaf');

    var grupo = $('#gSistema');
    var sistema = $('#sistema');
    var subsistema = $('#subSistemaSaf');
    var avaria = $('#avariaSaf');
    var inputAvaria = $('#avaria');

    //######### Linha / Trecho / PontoNotavel
    //Auto preenchimento do trecho e via ap�s sele��o da linha
    linha.on("change", function () {
        apiLinhaTrecho(linha, trecho);
        apiLinhaVia(linha, $('select[name="via"]'));
        apiLinhaPrefixo($(this), prefixo);

        pontoNotavel.html('<option disabled="disabled" selected="selected" value="">Pontos Not�veis</option>');

        var codGrupo = grupo.val();

        if (codGrupo == "22" || codGrupo == "23" || codGrupo == "26") {
            apiGetVeiculo(grupo.val(), veiculo, $(this).val(), null, false);
            carro.html("");
            odometroField.attr("placeholder", "");
        }
    });

    //Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
    trecho.on("change", function () {
        apiTrechoPn(trecho, pontoNotavel);
    });

    //######### Grupo / Sistema / SubSistema
    //Auto preenchimento do sistema ap�s sele��o do grupo sistema
    grupo.on("change", function () {
        var divMr = $('#divMaterialRodante');
        var inputMr = $('#divMaterialRodante input');
        var selectMr = $('#divMaterialRodante select');

        var selectSublocal = $('#sublocal select');

        if ($(this).val() == "22" || $(this).val() == "23" || $(this).val() == "26") {
            divMr.show();
            inputMr.each(function () {
                $(this).attr("required", "required");
                $(this).val('');
            });
            selectMr.each(function () {
                $(this).attr("required", "required");
                $(this).val('');
            });

            prefixo.removeAttr("required");

            carro.html('');
            odometroField.attr("placeholder", "");
            apiGetVeiculo($(this).val(), veiculo, linha.val(), null, false);

            var veiculoTxt = "Veiculo";

            if($(this).val() == "23") veiculoTxt = "TUE";

            $('.nivel-a').html(veiculoTxt+' bloqueando a via, necess�rio presen�a de t�cnico no campo');
            $('.nivel-b').html(veiculoTxt+' deve ser retirado da opera��o por causa da avaria');
            $('.nivel-c').html(veiculoTxt+' pode concluir a opera��o di�ria com esta avaria<br> sinalizada na SAF');
        } else {
            divMr.hide();
            inputMr.each(function () {
                $(this).removeAttr("required");
            });
            selectMr.each(function () {
                $(this).removeAttr("required");
            });

            $('.nivel-a').html('Atendimento Imediato');
            $('.nivel-b').html('Atendimento em at� 6 horas');
            $('.nivel-c').html('Atendimento em at� 24 horas');
        }

        if($(this).val() == "28" || $(this).val() == "27"){
            $('#sublocal').show();
            selectSublocal.each(function(){
                $(this).attr("required", "required");
                $(this).val('');
            });

        }else{
            $('#sublocal').hide();
            selectSublocal.each(function(){
                $(this).removeAttr("required");
                $(this).val('');
            });
        }

        avaria.val("");
        inputAvaria.val("");
        apiGetAvariaDataList($(this), $('#avariaSafDataList'));

        apiGrupoSistema($(this), sistema);

        subsistema.html('<option disabled="disabled" selected="selected">Selecione o Sub-Sistema</option>');
        checkRequiredField();
    });

    grupo.on("blur", function(){
        $('#sistema option[value="174"]').each(function(){
            $(this).remove();
        });
    });

    var localGrupo = $('#localGrupo');
    var sublocalGrupo = $('#subLocalGrupo');

    localGrupo.on("change", function(){
        apiLocalSubLocal($(this), sublocalGrupo);
    });

    //Auto preenchimento do subsistema ap�s sele��o do sistema
    sistema.on("change", function () {
        apiSistemaSub($(this), subsistema);
    });

    //############### Avaria
    avaria.on("input", function () {
        dataListFuncao2(avaria, $("#avariaSafDataList"), inputAvaria);
    });

    avaria.on("blur", function () {
        if (inputAvaria.val() == "") {
            $(this).val("");
            alertaJquery("Alerta", 'Clique em uma avaria', "alert");
        } else {
            var valueFinal = $("#avariaSafDataList").find("option[id='" + inputAvaria.val() + "']");
            avaria.val(valueFinal.val());
            inputAvaria.val(valueFinal.attr('id'));
            
            getJson('/avaria/getAvariaById/'+valueFinal.attr('id'),
            function(json)
            {
                if(json.sugestao_nivel != "" && json.sugestao_nivel != null)
                {
                    nivel.val(json.sugestao_nivel);
                }
                else
                    nivel.val("C");
                
            });
        }
    });

    //Fun��o do bot�o de imprimir SAF
    $(".btnImprimirFormulario").on("click", function () {
        var codigoSaf = $('input[name="cod_saf"]').val();

        $.ajax({
            type: "POST",
            url: caminho + "/cadastroGeral/imprimirSaf",
            data: {codigoSaf: codigoSaf},
            success: function () {
                window.open(caminho + "/dashboardGeral/printSaf", "_blank");
            }
        });
    });

    //Fun��o do bot�o de autoriza��o da SAF
    $('.autorizarSaf').click(function (e) {
        e.preventDefault();
        if (hasProcess(e)) return;

        var resultado = validacao_formulario($('#formSaf input'), $('#formSaf select'), $('#formSaf textarea'));

        var form = $('#formSaf');

        form.attr('action', caminho+'/ssm/store');

        if (resultado['permissao']) {

            var dadosSaf = [$('select[name="linhaSaf"]').val(), trecho.val(), $('input[name="avaria"]').val(), grupo.val(), sistema.val(), subsistema.val(), $('input[name="codigoSaf"]').val()];

            $.getJSON(
                caminho + "/api/returnResult/dados/getSemelhanteSaf",
                {dados: dadosSaf},
                function (json) {
                    if (json.semelhante) {
                        form.submit();
                    } else {
                        alertaJquery("SAF(s) semelhante(s) j� existe(m).", "Deseja prosseguir assim mesmo?  <br> Saf(s): " + json.saf, 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                            if (res == "Sim") {
                                form.submit();
                            }
                            if (res == "N�o") {
                                $('body').removeClass('processing');
                            }
                        });
                    }
                }
            );
        } else {
            alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
            $('body').removeClass('processing');
        }


    });

    $('#salvarSaf').on('click', function (e) {
        e.preventDefault();
        if (hasProcess(e)) return;

        var resultado = validacao_formulario($('#formSaf input'), $('#formSaf select'), $('#formSaf textarea'));

        if (resultado['permissao']) {

            var dadosSaf = [$('select[name="linhaSaf"]').val(), trecho.val(), $('input[name="avaria"]').val(), grupo.val(), sistema.val(), subsistema.val()];

            $.getJSON(
                caminho + "/api/returnResult/dados/getSemelhanteSaf",
                {dados: dadosSaf},
                function (json) {
                    if (json.semelhante) {
                        $('#formSaf').submit();
                    } else {
                        alertaJquery("SAF(s) semelhante(s) j� existe(m).", "Deseja prosseguir assim mesmo?  <br> Saf(s): " + json.saf, 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                            if (res == "Sim") {
                                $('#formSaf').submit();
                            }
                            if (res == "N�o") {
                                $('body').removeClass('processing');
                            }
                        });
                    }
                }
            );
        } else {
            alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
            $('body').removeClass('processing');
        }
    });


    //Material Rodante

    var odometroSaf = 0;

    veiculo.change(function () {
        odometroField.attr("placeholder", "");
        apiCarro('veiculo', $(this).val(), carro, false);
    });

    carro.change(function () {
        if( grupo.val() == 23 ){
            odometroSaf = carro.find("[data-sigla='MF1']").data("odometro");
        }else{
            if ($('#carroMrSaf option:selected').data("sigla") == "RA"){
                odometroSaf = carro.find("[data-sigla='MA']").data("odometro");
            }else if ($('#carroMrSaf option:selected').data("sigla") == "RB"){
                odometroSaf = carro.find("[data-sigla='MB']").data("odometro");
            }else{
                odometroSaf = $('#carroMrSaf option:selected').data("odometro");
            }
        }

        odometroField.attr("placeholder", odometroSaf);
    });

    odometroField.on("blur", function () {
        if( $(this).val() < odometroSaf ){
            $(this).val('');
            alertaJquery('Odometro', 'O odometro n�o pode ser inferior ao odometro atual.', 'alert');
        }
    });
});