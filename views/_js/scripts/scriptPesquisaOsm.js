$(document).ready(function() {
    $("#resultadoPesquisaOsm").dataTable(configTable(4, false, 'asc'));
});

//Fun��o para DataList
$('#materiaisPesquisa').on("input", function () {
    dataListFuncao2($('#materiaisPesquisa'), $("#materialDataList"), $('#materiaisPesquisaOsm'));
});
$('#maquinasPesquisa').on("input", function () {
    dataListFuncao2($('#maquinasPesquisa'), $("#maquinaDataList"), $('#maquinasEquipamentosPesquisaOsm'));
});

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaOsm"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaOsm"]'));
    } else {
        linha.val($(this).val());
        $('select[name="pnPesquisaOsm"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaOsm"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaOsm"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaOsm"]').val() == "") {
            trecho.val("*");
            apiTrechoPn(trecho, $('select[name="pnPesquisaOsm"]'));
        } else {
            $('select[name="pnPesquisaOsm"]').html('<option value="">Ponto Not�vel</option>');
        }
    } else {
        trecho.val($(this).val());
        apiTrechoPn(trecho, $('select[name="pnPesquisaOsm"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

if ($('select[name="grupoPesquisaOsm"]').val() == '22' || $('select[name="grupoPesquisaOsm"]').val() == '23' || $('select[name="grupoPesquisaOsm"]').val() == '26') {
    $('.mtRod').show();
}

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoPesquisaOsm"]').on('change', function () {

    var grupo = ObjetoVal;
    var selectLocalSublocal = $('#local_sublocal select');

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        $('.mtRod').show();
    } else {
        clearMtRod();
        $('.mtRod').hide();
    }

    if($(this).val() == "28" || $(this).val() == "27"){
        $('#local_sublocal').show();
        selectLocalSublocal.each(function(){
            $(this).attr("required", "required");
            $(this).val('');
        });

    }else {
        $('#local_sublocal').hide();
        selectLocalSublocal.each(function () {
            $(this).removeAttr("required");
        });
    }

    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaOsm"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisaOsm"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisaOsm"]'));
    $('select[name="localGrupoOsm"]').on("change", function(){
        apiLocalSubLocal($(this), $('select[name="subLocalGrupoOsm"]'));
    });
});

$('select[name="veiculoPesquisaOsm"]').on('change', function () {
    var veiculo = ObjetoVal;
    if ($(this).val() == "")
        veiculo.val("*");
    else
        veiculo.val($(this).val());

    apiCarro('veiculo', veiculo.val(), $('select[name="carroAvariadoPesquisaOsm"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisaOsm"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoPesquisaOsm"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        } else {
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="unidadePesquisaOsm"]').on("change", function () {
    var local = ObjetoVal;
    if ($(this).val() == "") {
        local.val("*");
    } else {
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisaOsm"]'));
});

//############### A��es ###############
//############### A��es ###############
//############### A��es ###############

$('.btnPesquisar').on('click', function () {
    if ($('input[name="dataPartirOsmAbertura"]').val() == '' && $('input[name="dataPartirOsmEncerramento"]').val() == '' && $('input[name="numeroPesquisaOsm"]').val() == '' && $('input[name="codigoSsm"]').val() == '') {
        alertaJquery("Sugest�o", "A pesquisa solicitada ir� necessitar de alguns minutos para que seja realizada.<h4> Deseja acrescentar um intervalo de Data para facilitar a consulta?</h4>", "confirm", [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "N�o") {
                $("#pesquisa").submit();
            }
        });
    } else {
        $("#pesquisa").submit();
    }
});

$('.btnResetarPesquisa').on('click', function () {
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaOsm";
});

$(".btnEditarOsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    $.ajax({                                                             //inicia o ajax
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsm",                      //informa o caminho do processo
        data: {codigoOs: dados},                                       //informa os dados ao ajax
        success: function () {                                            //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardGeral/Osm");
        }
    });
});

$(".btnImprimirOsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    $.ajax({                                                             //inicia o ajax
        type: "POST",
        url: caminho + "/cadastroGeral/imprimirOsm",                    //informa o caminho do processo
        data: {codigoOs: dados},                                          //informa os dados ao ajax
        success: function () {
            window.open(caminho + "/dashboardGeral/printOsm", "_blank");  //em caso de sucesso imprime o retorno
        }
    });
});

$(".btnAbrirPesquisaSsm").on("click", function(){
    var codigoSsm = $(this).parent("td").parent("tr").find('td').next('td').find('span').html();
    window.location.href = caminho + "/dashboardGeral/pesquisaOsmParaSsm/" + codigoSsm;
});

function clearMtRod() {
    $('select[name="veiculoPesquisaOsm"]').val('');
    $('select[name="carroAvariadoPesquisaOsm"]').val('');
    $('select[name="carroLiderPesquisaOsm"]').val('');
    $('select[name="prefixoPesquisaOsm"]').val('');
    $('input[name="odometroPartirPesquisaOsm"]').val('');
    $('input[name="odometroAtePesquisaOsm"]').val('');
}