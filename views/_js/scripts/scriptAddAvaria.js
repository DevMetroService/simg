var tabelaAvarias;

$(document).ready(function(){

    tabelaAvarias = $("#tabelaAvarias").dataTable(configTable(0, false, 'asc'));

    /*
    * Pesquisa os dados da avaria selecionada e preeche
    * os campos do formulario com os dados dessa avaria.
    * */

});

$(document).on("click",".btn_editar_avaria", function(e){

    // console.log($('#cod_grupo option:selected')[0]);
    // $('#cod_grupo option:selected').prop('selected', false);
    // console.log($('#cod_grupo option:selected')[0]);


    var cod_avaria = $(this).val();

    apiAvaria(cod_avaria, function (json) {
        $('input[name="cod_avaria"]').val(json.cod_avaria);
        $('input[name="nome_avaria"]').val(json.nome_avaria);
        $('select[name="cod_grupo"]').val(json.cod_grupo);
        $('select[name="sugestao_nivel"]').val(json.sugestao_nivel);
        $('select[name="ativo"]').val(json.ativo);
    });

    $('body').animate({
        scrollTop: 0
    });

});