

$(".tableMateriaisMr").DataTable();

var cod_material = $('input[name="cod_material"]');
var nomeMaterial = $('input[name="nomeMaterial"]');
var grupoSistema = $('select[name="grupoSistema"]');
var unidadeMedidaMaterial = $('select[name="unidadeMedidaMaterial"]');
var qtdMinimaMaterial = $('input[name="qtdMinimaMaterial"]');
var categoriaMaterial = $('select[name="categoriaMaterial"]');
var marcaMaterial = $('select[name="marcaMaterial"]');
var descricaoResumidaMaterial = $('input[name="descricaoResumidaMaterial"]');
var codigoBarrasMaterial = $('input[name="codigoBarrasMaterial"]');



$(document).on("click",".editMaterial", function(e){
    e.preventDefault();

    var row = $(this).parent("td").parent("tr").find('td');

    nomeMaterial.val(row.data('value')); // nome material
    unidadeMedidaMaterial.val(row.next().data('value')); // cod sigla unidade
    qtdMinimaMaterial.val(row.next().next().data('value')); // quantidade minima
    categoriaMaterial.val(row.next().next().next().data('value')); // cod categoria
    marcaMaterial.val(row.next().next().next().next().data('value')); // cod marca
    descricaoResumidaMaterial.val(row.next().next().next().next().next().data('value')); // descricao
    codigoBarrasMaterial.val(row.next().next().next().next().next().next().data('value')); // ean
    grupoSistema.val(row.next().next().next().next().next().next().next().data('value')); //cod grupo

    cod_material.val($(this).data('value')); //codigo material


    $("html, body").animate({scrollTop : 0},700);
});


$(document).on("click", 'button[type="reset"]' ,function () {
    cod_material.val('');
});
