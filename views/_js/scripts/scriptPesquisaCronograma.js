/**
 * Created by iramar.junior on 06/02/2017.
 */

//===Tabelas===//
//====================================================================================================================//
$(document).ready(function () {
    $("#resultadoPesquisaCronograma").dataTable(configTable(0, true, 'asc'));
});

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});


if ($('select[name="grupoPesquisaCronograma"]').val() == 21) {
    $("#estacaoInicial").hide();
    $("#estacaoFinal").hide();
}

$('select[name="grupoPesquisaCronograma"]').on('change', function () {
    switch ($(this).val()) {
        case '21'://Edifica��es
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '25'://Subesta��o
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '24'://Via Permanente
            $("#local").hide();
            $("#estacaoInicial").show();
            $("#estacaoFinal").show();
            break;
        case '20'://Rede A�rea
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '27'://Telecom
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '28'://Bilhetagem
            $("#local").hide();
            $("#estacaoInicial").show();
            $("#estacaoFinal").hide();
            break;
    }
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subsistemaPesquisaCronograma"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subsistemaPesquisaCronograma"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaCronograma"]'));
    apiGrupoServico(grupo, $('select[name="servicoPesquisaCronograma"]'));

    if ($('select[name="linhaPesquisaCronograma"]').val() != "") {
        apiLinhaLocalgrupo($('select[name="linhaPesquisaCronograma"]'), $('select[name="grupoPesquisaCronograma"]'), $('select[name="localPesquisaCronograma"]'));
    }
});

$('select[name="sistemaPesquisaCronograma"]').on('change', function () {
    sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoPesquisaCronograma"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subsistemaPesquisaCronograma"]'));
        } else {
            $('select[name="subsistemaPesquisaCronograma"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subsistemaPesquisaCronograma"]'));
    }
});

$('select[name="subsistemaPesquisaCronograma"]').on('change', function () {
    apiSistemaSubSistema($('select[name="sistemaPesquisaCronograma"]'), $(this), $('select[name="servicoPesquisaCronograma"]'));
});

$('select[name="linhaPesquisaCronograma"]').on('change', function () {
    apiLinhaLocalgrupo($(this), $('select[name="grupoPesquisaCronograma"]'), $('select[name="localPesquisaCronograma"]'));
    apiLinhaEstacao($(this), $('select[name="estacaoInicialPesquisaCronograma"]'));
    apiLinhaEstacao($(this), $('select[name="estacaoFinalPesquisaCronograma"]'));
});

//===A��es===//
//====================================================================================================================//

$('.btnPesquisar').on('click', function () {

    $("#pesquisa").submit();

});

$('.btnResetarPesquisa').on('click', function () {
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaCronograma";
});

//====================================================================================================================//

var tableModal = $(".tableRelModal").dataTable(configTable(0, false, 'asc'));

$(document).on("click", ".btnGerarSsmp", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();

    // alert(dados);

    var cronograma = dados.split('<input')[0];
    var grupo = dados.split('value="')[1].split('"')[0];

    // alert(cronograma);
    // alert(grupo);

    var arr = Array();

    switch (grupo) {
        case "Edifica��o":
            grupo = 'ed';
            break;
        case "Via Permanente":
            grupo = 'vp';
            break;
        case "Subesta��o":
            grupo = 'su';
            break;
        case "Rede Aerea":
            grupo = 'ra';
            break;
        case "Telecom":
            grupo = 'tl';
            break;
        case "Bilhetagem":
            grupo = 'bl';
            break;
    }

    arr.push(cronograma);

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/gerarSsmp",
        data: {codCronograma: arr, form: grupo},
        success: function () {
            window.location.reload();
        }
    });
});

$(document).on("click", ".abrirModalCronograma", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var cronograma = dados.split('<input')[0];
    var grupo = dados.split('value="')[1].split('"')[0];

    apiModalCronograma(cronograma, grupo, tableModal);
});


$(document).on("click", ".btnEditarOsmp", function (e) {
    if (hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');

    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsmp",
        data: {codigoOs: dados},
        success: function (data) {
            window.location.replace(caminho + "/dashboardGeral/osmp/" + form);
        }
    });
});
