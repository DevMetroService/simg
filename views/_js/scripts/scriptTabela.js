/**
 * Created by ricardo.diego on 26/04/2016.
 */

var tabela;
$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tabela = $("#indicadorTabela").dataTable(configTable(0, true, 'asc'));
    });
});


$('#tabelaPmpModal').on('show.bs.modal', function(event){

    var nPmp = $('input[name="nPmp"]');
    //var nSa = $('input[name="nSa"]');
    //var statusPmp = $('input[name="statusPmp"]');
    //var dataInicio = $('input[name="dataInicio"]');
    //var qtdDias = $('input[name="qtdDias"]');
    var qznExecucao = $('input[name="qznExecucao"]');
    var procedimento = $('input[name="procedimento"]');
    var periodicidade = $('input[name="periodicidadePmp"]');

    var dados  = $(event.relatedTarget);
    dados = dados.data('action');

    nPmp.val(dados);

    $.getJSON(
        caminho+"/api/returnResult/pmp",
        { pmp: dados },
        function( json )
        {
            qznExecucao.val(json.qzn);
            procedimento.val(json.procedimento);
            periodicidade.val(json.periodicidade);
        }
    );

});

$('.procedimento').on('click', function(){
    $.ajax({                                                                            //inicia o ajax
        type: "POST",
        url: caminho+"/dashboardPmp/exibirPdf",                            //informa o caminho do processo
        data: { nomeArquivo: $(this).val() },                                                       //informa os dados ao ajax
        success: function() {                                                           //em caso de sucesso imprime o retorno
            window.open(caminho+"/dashboardPmp/exibirPdf", "_blank");
        }
    });
});


