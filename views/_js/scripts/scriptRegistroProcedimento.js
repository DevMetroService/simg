$(".tableIS").dataTable(configTable(0, false, 'asc'));

var codigo = $('#cod_servico');
var grupo = $('#grupo');
var servico = $('#servico');
var procedimento = $('#procedimento');
var ativo = $('#ativo');

var labelBtn = $('#labelBtn');

$('#resetBtn').click(function(){
    labelBtn.html('Salvar');
});

$(document).on("click",".btnEditar", function(e){
    e.preventDefault();

    labelBtn.html('Editar');

    var dados = $(this).parent("td").parent("tr").find('td');

    codigo.val(dados.html());
    grupo.val(dados.next().data('value'));
    servico.val(dados.next().next().html());
    procedimento.val(dados.next().next().next().html());
    if(dados.next().next().next().next().data('value') == "s")
        ativo.prop("checked" ,  true);
    else{
        ativo.prop("checked" ,  false);
    }

});

$('#btnSave').click(function(e){
    e.preventDefault();
    if(procedimento.val() !== '' && servico.val() !== '' && grupo.val() !== ''){
        $('#formFrota').submit()
    }else{
        alertaJquery('Sem informa��o', 'N�o h� informa��o para registro em tabela', 'error');
    }
});