$(document).ready(function(){
    // Configura a Tabela de  Usuarios Cadastrados
    $("#amvTable").DataTable(configTable(0, false, 'asc'));

    var linha = $('#linha');
    var trecho = $('#trecho');
    var pontoNotavel = $('#pontoNotavel');

    var form = $("#formulario");

    $(document).on("click",".editPontoNotavel", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/pontoNotavel/update/'+$(this).data('codigo'));

        var row = $(this).parent("td").parent("tr").find('td').next();

        linha.val(row.data('linha'));

        apiLinhaTrecho(linha, trecho, row.data('value'))

        pontoNotavel.val(row.next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    linha.change(function(){
        apiLinhaTrecho(linha, trecho);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/pontoNotavel/store');
        trecho.html("<option value=''>Selecione linha para filtrar</option>");
    });

});