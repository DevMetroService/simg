/**
 * Created by josue.marques on 03/06/2016.
 */
//################################# Script para M�dulo Materiais Utilizados ###########
//################################# Script para M�dulo Materiais Utilizados ###########
//################################# Script para M�dulo Materiais Utilizados ###########

$('select[name="origemMaterial"]').on("change", function() {
    if($('select[name="origemMaterial"] option:selected').val() == 'o'){
        document.getElementById('outraOrigem').style.display = "block";
    }else{
        document.getElementById('outraOrigem').style.display = "none";
    }
});

var excluidosMateriais = new Array();

var contadorMaterial = 0;

if ($('input[name="contadorMaterial"]').val() >= 1) {
    contadorMaterial = Number($('input[name="contadorMaterial"]').val());
}

//Fun��o que adiciona funcionario na lista
var unidadeMedida = $('#optionUnidadeMedidaFixa');

$("#addMaterialOs").on("click", function (e) {		//Fun��o para adicionar a div de benefici�rios
    e.preventDefault();

    var codigoMaterial       = $('input[name="codigoMaterial"]').val();
    var nome                 = $('input[name="nomeMaterial"]').val();
    var unidadeMaterial      = $('select[name="unidadeMaterial"] option:selected').val();
    var unidadeMaterialTexto = $('select[name="unidadeMaterial"] option:selected').text();
    var qtdUtilizada         = $('input[name="qtdUtilizada"]').val();
    var estado               = $('select[name="estadoMaterial"]').val();
    var origem               = $('select[name="origemMaterial"]').val();
    var outroOrigem         = $('input[name="outroOrigem"]').val();

    var mensagem ="";
    if(codigoMaterial == ""){
        mensagem += "C�digo � obrigat�ria\n";
    }
    if(nome == ""){
        mensagem += "Nome � obrigat�rio\n";
    }
    if(qtdUtilizada == ""){
        mensagem += "Quantidade � obrigat�rio\n";
    }
    if(unidadeMaterial == ""){
        mensagem += "Unidade � obrigat�rio\n";
    }
    if(estado == null){
        mensagem += "Estado do produto � obrigat�rio\n";
    }
    if(origem == null){
        mensagem += "Origem � obrigat�rio\n";
    }

    var limit = 500;

    for(i = 0; i < limit; i++){
        nomeCampo = "codigoMaterialDelete" + i;
        if(codigoMaterial == $('input[name = '+nomeCampo+']').val()){
            mensagem += "Material j� Registrado\n";
        }
    }


    if(mensagem == "") {
        //Zerando os valores
        $('input[name="codigoMaterial"]').val("");
        $('input[name="nomeMaterial"]').val("");
        if(typeof unidadeMedida.val() !== "undefined") {
            unidadeMedida.text("");
        }else
            $('select[name="unidadeMaterial"]').val("");
        $('input[name="qtdUtilizada"]').val("");
        $('select[name="estadoMaterial"]').val("");
        $('select[name="origemMaterial"]').val("");
        $('input[name="outroOrigem"]').val("");

        var disponivel = excluidosMateriais.pop();

        if (contadorMaterial < limit && disponivel == undefined) {
            contadorMaterial = contadorMaterial + 1;
            $("#materialUtilizadoAdicionado").append('<div class="row" style="padding-top: 1em;">' +
                '<div class="col-md-2 col-lg-1"><label>C�digo</label> <input disabled type="text"  value="' + codigoMaterial + '" class="form-control">' +
                '<input type="hidden" name="codigoMaterial' +contadorMaterial + '"class="form-control" value="' + codigoMaterial + '"></div>' +
                '<div class="col-md-4 col-lg-3"><label>Nome</label> <input type="text" disabled value="'+nome+'" class="form-control"></div>' +
                '<div class="col-md-2 col-lg-1"><label>Unidade</label> <input type="text" disabled class="form-control" value="' + unidadeMaterialTexto + '"></div>' +
                '<input type="hidden" name="unidadeMedidaOs'+contadorMaterial+'" value="' + unidadeMaterial + '">' +
                '<div class="col-md-2 col-lg-1"><label>Qtd.</label> <input type="text" readonly class="form-control" value="' + qtdUtilizada + '" name="qtdUtilizado'+contadorMaterial+'"></div>' +
                '<div class="col-md-2 col-lg-2"><label>Estado</label> <input type="text" disabled class="form-control" value="' + estado + '"></div>' +
                '<input type="hidden" name="estadoMaterial'+contadorMaterial+'" class="form-control" value="' + estado + '">' +
                '<div class="col-md-2 col-lg-2"><label>Origem</label> <input type="text" disabled class="form-control" value="' + origem + '"></div>' +
                '<input type="hidden" name="origemMaterial'+contadorMaterial+'" class="form-control" value="' + origem + '">' +
                '<input type="hidden" name="outroOrigem'+contadorMaterial+'" class="form-control" value="' + outroOrigem + '">' +
                '<div style="padding-top: 1.5em"><button id="excluirMaterial" class="btn btn-danger btn-circle" value="' + contadorMaterial + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
                '</div>');
        } else {
            if(disponivel != undefined){
                contadorMaterial = contadorMaterial + 1;
                $("#materialUtilizadoAdicionado").append('<div class="row" style="padding-top: 1em;">' +
                    '<div class="col-md-2 col-lg-1"><label>C�digo</label> <input disabled type="text"  value="' + codigoMaterial + '" class="form-control">' +
                    '<input type="hidden" name="codigoMaterial' +disponivel + '"class="form-control" value="' + codigoMaterial + '"></div>' +
                    '<div class="col-md-4 col-lg-3"><label>Nome</label> <input type="text" disabled value="'+nome+'" class="form-control"></div>' +
                    '<div class="col-md-2 col-lg-1"><label>Unidade</label> <input type="text" disabled class="form-control" value="' + unidadeMaterialTexto + '"></div>' +
                    '<input type="hidden" name="unidadeMedidaOs'+disponivel+'" value="' + unidadeMaterial + '">' +
                    '<div class="col-md-2 col-lg-1"><label>Qtd.</label> <input type="text" readonly class="form-control" value="' + qtdUtilizada + '" name="qtdUtilizado'+disponivel+'"></div>' +
                    '<div class="col-md-2 col-lg-2"><label>Estado</label> <input type="text" disabled class="form-control" value="' + estado + '"></div>' +
                    '<input type="hidden" name="estadoMaterial'+disponivel+'" class="form-control" value="' + estado + '">' +
                    '<div class="col-md-2 col-lg-2"><label>Origem</label> <input type="text" disabled class="form-control" value="' + origem + '"></div>' +
                    '<input type="hidden" name="origemMaterial'+disponivel+'" class="form-control" value="' + origem + '">' +
                    '<input type="hidden" name="outroOrigem'+contadorMaterial+'" class="form-control" value="' + outroOrigem + '">' +
                    '<div style="padding-top: 1.5em"><button id="excluirMaterial" class="btn btn-danger btn-circle" value="' + disponivel + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
                    '</div>');

                /*/
                 $("#materialUtilizadoAdicionado").append('<div class="row" style="padding-top: 1em;">' +

                 '<div class="col-md-2 col-lg-1"><label>C�digo</label> <input disabled type="text"  value="' + codigoMaterial + '" class="form-control">' +
                 '<input type="hidden" name="codigoMaterial' + disponivel + '"class="form-control" value="' + codigoMaterial + '"></div>' +
                 '<div class="col-md-4 col-lg-3"><label>Nome</label> <input type="text" disabled value="' + nome + '" class="form-control"></div>' +
                 '<div class="col-md-2 col-lg-1"><label>Unidade</label> <input type="text" disabled class="form-control" value="' + unidadeMaterial + '"></div>' +
                 '<div class="col-md-2 col-lg-1"><label>Qtd.</label> <input type="text" class="form-control" value="' + qtdUtilizada + '" name="qtdUtilizado'+disponivel+'"></div>' +
                 '<div class="col-md-2 col-lg-2"><label>Estado</label> <input type="text" disabled class="form-control" value="' + estado + '"></div>' +
                 '<div class="col-md-2 col-lg-2"><label>Origem</label> <input type="text" disabled class="form-control" value="' + origem + '"></div>' +
                 '<div style="padding-top: 1.5em"><button id="excluirMaterial" class="btn btn-danger btn-circle" value="' + disponivel + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
                 '</div>');
                 */
            }else{
                alertaJquery("Alerta", "Limite m�ximo alcan�ado. \nCaso necess�rio entre em contato com a TI", "alert");
            }
        }
    }else{
        alertaJquery("Alerta", mensagem, "alert");
    }

});

$("#materialUtilizadoAdicionado").on("click", "#excluirMaterial", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();
    $(this).parent('div').parent('div').remove();
    excluidosMateriais.push($(this).val());
    contadorMaterial = contadorMaterial - 1;
});

$('#nomeMaterial').on("input", function () {
    dataListFuncao2($('#nomeMaterial'), $("#nomeMaterialDataList"), $('input[name="codigoMaterial"]'));
});

$('#nomeMaterial').on("blur", function () {
    if ($('input[name="codigoMaterial"]').val() == "") {
        $(this).val("");
    } else {
        var codMaterial = $('input[name="codigoMaterial"]');
        var valueFinal = $("#nomeMaterialDataList").find("option[value='" + $('input[name="nomeMaterial"]').val() + "']");
        $('#nomeMaterial').val(valueFinal.val());
        codMaterial.val(valueFinal.attr('id'));

        //Pega o select de unidade
        var unidadeMaterialId = $('#unidadeMaterial');

        //Verifica se � sistema Fixo a partir de uma �nica option.
        if(typeof unidadeMedida.val() !== "undefined")
        {
            unidadeMaterialId.attr("disabled", "disabled");

            getJson("/UnidadeMedida/getFromMaterial/"+codMaterial.val(),
                function(json){
                    unidadeMedida.text(json.sigla_uni_medida + " - " + json.nome_uni_medida);
                    unidadeMedida.val(json.cod_uni_medida);
                }
            );
        }
    }
});

$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#osMaterialUtilizado input'), $('#osMaterialUtilizado select'), $('#osMaterialUtilizado textarea'));

    if(resultado['permissao']){
        $('#osMaterialUtilizado').submit();
    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});