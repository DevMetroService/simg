$(document).ready(function(){
    // Configura a Tabela de  Usuarios Cadastrados
    $("#amvTable").DataTable(configTable(0, false, 'asc'));

    var linha = $('#linha');
    var estacao = $('#estacao');
    var amv = $('#amv');

    var form = $("#formulario");

    $(document).on("click",".editAmv", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/amv/update/'+$(this).data('codigo'));

        var row = $(this).parent("td").parent("tr").find('td').next();

        linha.val(row.data('value'));

        apiLinhaEstacao(linha, estacao, row.next().data('value'));

        amv.val(row.next().next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    linha.change(function(){
        apiLinhaEstacao(linha, estacao);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/amv/store');
        estacao.html("<option value=''>Selecione linha para filtrar</option>");
    });

});