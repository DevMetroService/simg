/**
 * Created by Rycker on 13/06/2016.
 */

$('document').ready(function(){
    $('input[name="matricula"]').on('blur', function () {
        var matricula = $(this);
        var tipoFuncionario = $('select[name="tipoFuncionario"]');
        var matriculaAntiga = $('input[name="matriculaAntiga"]');

        var dados = [tipoFuncionario.val(), matricula.val()];

        if(matricula.val() == matriculaAntiga.val())
            return;

        if(matricula.length) {
            $.getJSON(
                caminho + "/api/returnResult/matricula",
                {matricula: dados},
                function (json) {
                    if (json.statusPesquisa) {
                        alertaJquery("Alerta", 'Matr�cula j� cadastrada.<br />Pertence � <strong>' + json.nome + '</strong>', "alert");
                        matricula.val("");
                    }
                }
            );
        }
    });
});

$('.btnSalvarContinuar').on("click", function (e) {
    if (hasProcess(e)) return;

    var resultado = validacao_formulario($('#formFuncionario input'), $('#formFuncionario select'), $('#formFuncionario textarea'));

    if (resultado['permissao']) {
        var matricula = $('input[name="matricula"]');
        var matriculaAntiga = $('input[name="matriculaAntiga"]');

        if (matricula.val() == matriculaAntiga.val()) {
            loader();
            $("#formFuncionario").submit();
            return;
        }

        if(matricula.val()) {

            var tipoFuncionario = $('select[name="tipoFuncionario"]');

            var dados = [tipoFuncionario.val(), matricula.val()];

            $.getJSON(
                caminho + "/api/returnResult/matricula",
                {matricula: dados},
                function (json) {
                    if (json.statusPesquisa) {
                        alertaJquery("Alerta", 'Matr�cula j� cadastrada.<br />Pertence � <strong>' + json.nome + '</strong>', "alert");
                        matricula.val("");
                        $('body').removeClass('processing');
                    }else{
                        loader();
                        $("#formFuncionario").submit();
                    }
                }
            );
        }
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});




//############################## Centro de resultado e Lotacao ##################################

$('select[name="tipoFuncionario"]').on("change",function(){
    if($('select[name="tipoFuncionario"]').val() == '1'){
        $('.centro_lotacao').show();

    }else{
        $('.centro_lotacao').hide();
        $('select[name="centroResultado"]').removeAttr("required");
        $('select[name="unidade"]').removeAttr("required");

    }
});



