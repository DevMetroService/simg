/**
 * Created by ricardo.diego on 05/12/2016.
 */
var tableModal = $(".tableRelModal").dataTable(configTable(0, false, 'asc'));

var tableCronoEd;
var tableEdAb;
var tableEdPr;
var tableEdPe;
var tableOsEd;

var tableCronoVp;
var tableVpAb;
var tableVpPr;
var tableVpPe;
var tableOsVp;

var tableCronoRa;
var tableRaAb;
var tableRaPr;
var tableRaPe;
var tableOsRa;

var tableCronoSu;
var tableSuAb;
var tableSuPr;
var tableSuPe;
var tableOsSu;

var tableCronoTl;
var tableTlAb;
var tableTlPr;
var tableTlPe;
var tableOsTl;

var tableCronoBl;
var tableBlAb;
var tableBlPr;
var tableBlPe;
var tableOsBl;

var tableCronoVlt;
var tableVltAb;
var tableVltPr;
var tableVltPe;
var tableOsVlt;

var tableCronoTue;
var tableTueAb;
var tableTuePr;
var tableTuePe;
var tableOsTue;

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        $("#indicadorCronogramaPendenteEd").dataTable(configTable(0, false, 'desc'));
        tableCronoEd    = $("#indicadorCronogramaEd").dataTable(configTable(0, false, 'desc'));
        tableEdAb       = $("#indicadorSsmpEdAb").dataTable(configTable(0, false, 'desc'));
        tableEdPr       = $("#indicadorSsmpEdProg").dataTable(configTable(0, false, 'desc'));
        tableEdPe       = $("#indicadorSsmpEdPen").dataTable(configTable(0, false, 'desc'));
        tableOsEd       = $("#indicadorOsmpEdExec").dataTable(configTable(0, false, 'desc'));

        $("#indicadorCronogramaPendenteVp").dataTable(configTable(0, false, 'desc'));
        tableCronoVp    = $("#indicadorCronogramaVp").dataTable(configTable(0, false, 'desc'));
        tableVpAb       = $("#indicadorSsmpVpAb").dataTable(configTable(0, false, 'desc'));
        tableVpPr       = $("#indicadorSsmpVpProg").dataTable(configTable(0, false, 'desc'));
        tableVpPe       = $("#indicadorSsmpVpPen").dataTable(configTable(0, false, 'desc'));
        tableOsVp       = $("#indicadorOsmpVpExec").dataTable(configTable(0, false, 'desc'));

        $("#indicadorCronogramaPendenteRa").dataTable(configTable(0, false, 'desc'));
        tableCronoRa    = $("#indicadorCronogramaRa").dataTable(configTable(0, false, 'desc'));
        tableRaAb       = $("#indicadorSsmpRaAb").dataTable(configTable(0, false, 'desc'));
        tableRaPr       = $("#indicadorSsmpRaProg").dataTable(configTable(0, false, 'desc'));
        tableRaPe       = $("#indicadorSsmpRaPen").dataTable(configTable(0, false, 'desc'));
        tableOsRa       = $("#indicadorOsmpRaExec").dataTable(configTable(0, false, 'desc'));

        $("#indicadorCronogramaPendenteSu").dataTable(configTable(0, false, 'desc'));
        tableCronoSu    = $("#indicadorCronogramaSu").dataTable(configTable(0, false, 'desc'));
        tableSuAb       = $("#indicadorSsmpSuAb").dataTable(configTable(0, false, 'desc'));
        tableSuPr       = $("#indicadorSsmpSuProg").dataTable(configTable(0, false, 'desc'));
        tableSuPe       = $("#indicadorSsmpSuPen").dataTable(configTable(0, false, 'desc'));
        tableOsSu       = $("#indicadorOsmpSuExec").dataTable(configTable(0, false, 'desc'));

        $("#indicadorCronogramaPendenteTl").dataTable(configTable(0, false, 'desc'));
        tableCronoTl    = $("#indicadorCronogramaTl").dataTable(configTable(0, false, 'desc'));
        tableTlAb       = $("#indicadorSsmpTlAb").dataTable(configTable(0, false, 'desc'));
        tableTlPr       = $("#indicadorSsmpTlProg").dataTable(configTable(0, false, 'desc'));
        tableTlPe       = $("#indicadorSsmpTlPen").dataTable(configTable(0, false, 'desc'));
        tableOsTl       = $("#indicadorOsmpTlExec").dataTable(configTable(0, false, 'desc'));

        $("#indicadorCronogramaPendenteBl").dataTable(configTable(0, false, 'desc'));
        tableCronoBl    = $("#indicadorCronogramaBl").dataTable(configTable(0, false, 'desc'));
        tableBlAb       = $("#indicadorSsmpBlAb").dataTable(configTable(0, false, 'desc'));
        tableBlPr       = $("#indicadorSsmpBlProg").dataTable(configTable(0, false, 'desc'));
        tableBlPe       = $("#indicadorSsmpBlPen").dataTable(configTable(0, false, 'desc'));
        tableOsBl       = $("#indicadorOsmpBlExec").dataTable(configTable(0, false, 'desc'));

        $("#indicadorCronogramaPendenteVlt").dataTable(configTable(0, false, 'desc'));
        tableCronoVlt    = $("#indicadorCronogramaVlt").dataTable(configTable(0, false, 'desc'));
        tableVltAb       = $("#indicadorSsmpVltAb").dataTable(configTable(0, false, 'desc'));
        tableVltPr       = $("#indicadorSsmpVltProg").dataTable(configTable(0, false, 'desc'));
        tableVltPe       = $("#indicadorSsmpVltPen").dataTable(configTable(0, false, 'desc'));
        tableOsVlt       = $("#indicadorOsmpVltExec").dataTable(configTable(0, false, 'desc'));

        tableCronoTue    = $("#indicadorPreventivaTue").dataTable(configTable(0, false, 'desc'));
        tableTueAb       = $("#indicadorSsmpTueAb").dataTable(configTable(0, false, 'desc'));
        tableTuePr       = $("#indicadorSsmpTueProg").dataTable(configTable(0, false, 'desc'));
        tableTuePe       = $("#indicadorSsmpTuePen").dataTable(configTable(0, false, 'desc'));
        tableOsTue       = $("#indicadorOsmpTueExec").dataTable(configTable(0, false, 'desc'));
    });
});

//############## A��es CRONOGRAMA
var codCronogramas = new Array();

codCronogramas['ed'] = new Array();
codCronogramas['vp'] = new Array();
codCronogramas['ra'] = new Array();
codCronogramas['su'] = new Array();
codCronogramas['tl'] = new Array();
codCronogramas['bl'] = new Array();
codCronogramas['vlt'] = new Array();

$(document).on("click",".btnGerarSsmp", function(e){
    if(hasProcess(e)) return;

    var form = $(this).val();

    if(codCronogramas[form].length){
        $.ajax({
            type: "POST",
            url: caminho+"/cadastroGeral/gerarSsmp",
            data: {codCronograma: codCronogramas[form], form: form},
            success: function() {
                window.location.reload();
            }
        });
    }else{
        alertaJquery('Gerar Ssmp', '<h3>N�o existe cronogramas selecionados.</h3>','info')
        $('body').removeClass('processing');
    }
});

$(document).on("click",".btnGerarSsmpTue", function(e){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    var dadosTue  = dados.split('*');
    
    var veiculo   = dadosTue[1];
    var servico   = dadosTue[2];
    var odometro  = dadosTue[3];

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/gerarSsmpTue",
        data: {veiculo: veiculo, servico: servico, odometro: odometro},
        success: function() {
            window.location.reload();
        }
    });
});

$(document).on("click",".btnTodasOpn", function(e) {
    var form = $(this).val();
    var checks = $('#indicadorCronograma' + form + ' input[type=checkbox]');

    checks.each(function () {
        $(this).prop('checked', true);

        if(codCronogramas[form].indexOf($(this).val()) == -1){
            codCronogramas[form].push($(this).val());
        }
    });
});

$(document).on("click",".btnNenhumaOpn", function(e) {
    var form = $(this).val();
    var checks = $('#indicadorCronograma' + form + ' input[type=checkbox]');

    checks.each(function () {
        $(this).prop('checked', false);

        var index = codCronogramas[form].indexOf($(this).val());
        if(index != -1){
            codCronogramas[form].splice(index, 1);
        }
    });
});

$(document).on("click","input[class|='checkBoxGerarSsmp']", function(e) {
    var form = $(this).attr('class').split('-')[1];

    if($(this).is(":checked")){
        codCronogramas[form].push($(this).val());
    }else{
        var index = codCronogramas[form].indexOf($(this).val());
        if(index != -1){
            codCronogramas[form].splice(index, 1);
        }
    }
});

$(document).on("click",".abrirModalCronograma", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var cronograma  = dados.split('<input')[0];
    var grupo       = dados.split('value="')[1].split('"')[0];

    apiModalCronograma(cronograma, grupo, tableModal);
});