/**
 * Created by josue.santos on 10/10/2016.
 */

//Auto preenchimento do subsistema após seleção do sistema
$(document).on("change", 'select[name="sistema"]', function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
    $('select[name="servicoPmp"]').html('<option selected="selected" value="">Selecione o Serviço</option>');
});

//Auto preenchimento do serviço após seleção do subsistema
$(document).on("change", 'select[name="subSistema"]', function () {
    apiSistemaSubSistema($('select[name="sistema"]'), $(this), $('select[name="servicoPmp"]'));
});

//Auto preenchimento do tipo de manutenção após seleção do serviço
$(document).on("change", 'select[name="servicoPmp"]', function () {
    apiServicoPeriodicidade($(this), $('select[name="sistema"]'), $('select[name="subSistema"]'), $('select[name="periodicidade"]'));
});

$(document).on("change", 'select[name="periodicidade"]', function () {
    apiPeriodicidadeProcedimento($(this), $('select[name="servicoPmp"]'), $('select[name="sistema"]'), $('select[name="subSistema"]'), $('input[name="procedimento"]'));
});

$(document).on('blur', 'input[name="quinzena"]', function () {
    var qzn = $(this).val();
    if (qzn != '' && (qzn < 1 || qzn > 24)) {
        $(this).val('');
        $(this).parent('div').addClass('has-error');
        alertaJquery("Alerta", "O número da Quinzena não pode ser menor que 1 ou maior que 24", "alert");
    }
});

$(document).on("change", 'input[name="horasUteis"], input[name="maoObra"]', function () {

    var hu = $('input[name="horasUteis"]').val();
    var mo = $('input[name="maoObra"]').val();

    var hh = hu * mo;
    if (hh == 0) {
        $('input[name="homemHora"]').val('');
    } else {
        $('input[name="homemHora"]').val(hh);
    }

});

$(document).on("blur", ".maoObra", function () {

    var td_moJs = $(this);
    var td_huJs = $(this).parent("td").next('td').find('input');
    var td_hhJs = $(this).parent("td").next('td').next('td').find('input');

    td_hhJs.val(td_moJs.val() * td_huJs.val());
    if (td_hhJs.val() == 0) {
        td_hhJs.val('');
    }

});

$(document).on("blur", ".horasUteis", function () {

    var td_moJs = $(this).parent("td").prev('td').find('input');
    var td_huJs = $(this);
    var td_hhJs = $(this).parent("td").next('td').find('input');

    td_hhJs.val(td_moJs.val() * td_huJs.val());
    if (td_hhJs.val() == 0) {
        td_hhJs.val('');
    }

});