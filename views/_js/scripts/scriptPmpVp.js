/**
 * Created by iramar.junior on 10/10/2016.
 */

var tabelaPmpVp;
var tabelaPmpVpExec;

$.getScript(caminho + "/views/_js/scripts/scriptPmp.js", function () { // include functions''
    $(document).ready(function () {
        tabelaPmpVp     = $("#tabelaPmpVp").dataTable(configTable(0, false, 'asc'));

        tabelaPmpVpExec = $("#tabelaPmpVpExec").dataTable(configTable(0, false, 'asc'));
    });
});

//***********************************Proprios do formularios

var subsistema = $("select[name='subSistema']");
var amv_field =  $('select[name="amv"]');
var amv_required = false;

$("select[name='linha']").on("change", function () {
    apiLinhaEstacao($(this), $('select[name="trechoInicial"]'));
    apiLinhaEstacao($(this), $('select[name="trechoFinal"]'));
    apiLinhaVia($(this), $('select[name="via"]'));
});

$("select[name='trechoInicial']").on("change", function () {
    apiEstacaoAmv($(this), $('select[name="amv"]'));
});

subsistema.on("change", function () {
    console.log($(this).val());

    if($(this).val() == 15)
    amv_required = true;
    else
    amv_required = false;

    console.log(amv_required);
});

amv_field.on("change", function() {
    if($(this).val() == '' && subsistema.val() == 15)
        amv_required = true;
    else
        amv_required = false;
    
    console.log(amv_required);
});

//***********************************Proprios do formularios


var excluidosLinhasPmp = new Array();
var contador = 1;

//=================Come�o On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {

    var sistema         = $('select[name="sistema"] option:selected');
    var subSistema      = $('select[name="subSistema"] option:selected');
    var servicoPmp      = $('select[name="servicoPmp"] option:selected');
    var periodicidade   = $('select[name="periodicidade"] option:selected');
    var turno           = $('select[name="turno"] option:selected');
    var procedimento    = $('input[name="procedimento"]');
    var qzn             = $('input[name="quinzena"]');
    var maoObra         = $('input[name="maoObra"]');
    var horasUteis      = $('input[name="horasUteis"]');
    var homemHora       = $('input[name="homemHora"]');
    var linha           = $('select[name="linha"] option:selected');
    var trechoInicial   = $('select[name="trechoInicial"] option:selected');
    var trechoFinal     = $('select[name="trechoFinal"] option:selected');
    var kmInicial       = $('input[name="kmInicial"]');
    var kmFinal         = $('input[name="kmFinal"]');
    var amv             = $('select[name="amv"] option:selected');
    var via             = $('select[name="via"] option:selected');

    kmInicial.val(parseFloat(kmInicial.val()));
    kmFinal.val(parseFloat(kmFinal.val()));

    if (sistema.val() == ''         || sistema.val() == null        || sistema.val() == undefined ||
        subSistema.val() == ''      || subSistema.val() == null     || subSistema.val() == undefined ||
        servicoPmp.val() == ''      || servicoPmp.val() == null     || servicoPmp.val() == undefined ||
        periodicidade.val() == ''   || periodicidade.val() == null  || periodicidade.val() == undefined ||
        turno.val() == ''           || turno.val() == null          || turno.val() == undefined ||
        qzn.val() == ''             || qzn.val() == null            || qzn.val() == undefined ||
        maoObra.val() == ''         || maoObra.val() == null        || maoObra.val() == undefined ||
        horasUteis.val() == ''      || horasUteis.val() == null     || horasUteis.val() == undefined ||
        linha.val() == ''           || linha.val() == null          || linha.val() == undefined ||
        trechoInicial.val() == ''   || trechoInicial.val() == null  || trechoInicial.val() == undefined ||
        kmInicial.val() == ''       || kmInicial.val() == null      || kmInicial.val() == undefined ||
        kmFinal.val() == ''         || kmFinal.val() == null        || kmFinal.val() == undefined ||
        trechoFinal.val() == ''     || trechoFinal.val() == null    || trechoFinal.val() == undefined || amv_required) {

        if(amv.val() == "")
            alertaJquery("Alerta", "Selecione um amv", "alert");
        else
            alertaJquery("Alerta", "Preencha todos os campos corretamente", "alert");

    } else {
        //================ Dados a serem adicionados na LISTA de PMP Edifica��o. ==========================//
        if(amv.val() == ""){
            amv = ObjetoVal;
            amv.text(''); // Retirar o texto "Selecione o AMV" sem alterar o input original
            amv.val('');  // Manter o value nulo
        }

        var txt = '';
        var duplicado = true;

        var col = tabelaPmpVpExec._('tr');

        //Tabela do Banco de Dados
        for (var i = 0; i < col.length; i++) {
            var dataTable = tabelaPmpVpExec.fnGetData(i);

            if ((dataTable[12] == trechoInicial.text()) &&
                (dataTable[13] == trechoFinal.text()) &&
                (dataTable[16] == kmInicial.val()) &&
                (dataTable[17] == kmFinal.val()) &&
                (dataTable[14] == amv.text()) &&
                (dataTable[2]  == sistema.text()) &&
                (dataTable[3]  == subSistema.text()) &&
                (dataTable[4]  == servicoPmp.text()) &&
                (dataTable[5]  == periodicidade.text())) {

                txt = txt + ' - cod - ' + dataTable[0].split("<input")[0] + " Duplicado no Banco de Dados.\n";
                duplicado = false;
                break;
            }
        }

        col = tabelaPmpVp._('tr');

        //Tabela de Lista

        for (var i = 0; i < col.length; i++) {

            var dataTable = tabelaPmpVp.fnGetData(i);

            if ((dataTable[12].split("<input")[0] == trechoInicial.text()) &&
                (dataTable[13].split("<input")[0] == trechoFinal.text()) &&
                (dataTable[16].split("<input")[0] == kmInicial.val()) &&
                (dataTable[17].split("<input")[0] == kmFinal.val()) &&
                (dataTable[14].split("<input")[0] == amv.text()) &&
                (dataTable[2].split("<input")[0]  == sistema.text()) &&
                (dataTable[3].split("<input")[0]  == subSistema.text()) &&
                (dataTable[4].split("<input")[0]  == servicoPmp.text()) &&
                (dataTable[5].split("<input")[0]  == periodicidade.text())) {

                txt = txt + ' - id - ' + dataTable[0] + " Duplicado na Lista.\n";
                duplicado = false;
                break;
            }
        }
        if (duplicado) {
            var disponivel = excluidosLinhasPmp.pop();

            if (disponivel == undefined)
                var ctdAux = contador;
            else
                var ctdAux = disponivel;

            //==========Turno==========//
            if (turno.val() == 'D') {
                var turnoHTML =
                    '<select name="turno' + ctdAux + '" class="form-control" required>' +
                    '<option value="D" selected>Diurno</option>' +
                    '<option value="N">Noturno</option>' +
                    '</select>';
            } else {
                var turnoHTML =
                    '<select name="turno' + ctdAux + '" class="form-control" required>' +
                    '<option value="N" selected>Noturno</option>' +
                    '<option value="D">Diurno</option>' +
                    '</select>';
            }

            tabelaPmpVp.fnAddData([
                ctdAux,
                //==========Linha==========//
                linha.text() + '<input type="hidden" name="linha' + ctdAux + '" value="' + linha.val() + '" >' +
                '<input type="hidden" name="grupo' + ctdAux + '" value="24" class="form-control number" readonly>',
                //==========Sistema==========//
                sistema.text() + '<input type="hidden" name="sistema' + ctdAux + '" value="' + sistema.val() + '" >',
                //==========SubSistema==========//
                subSistema.text() + '<input type="hidden" name="subSistema' + ctdAux + '" value="' + subSistema.val() + '" >',
                //==========Servi�o==========//
                servicoPmp.text() + '<input type="hidden" name="servicoPmp' + ctdAux + '" value="' + servicoPmp.val() + '" >',
                //==========Periodicidade==========//
                periodicidade.text() + '<input type="hidden" name="periodicidade' + ctdAux + '" value="' + periodicidade.val() + '" >',
                //==========Procedimento==========//
                procedimento.val(),
                //==========Qzn In�cio==========//
                '<input type="text" name="quinzena' + ctdAux + '" value="' + qzn.val() + '" class="form-control number">',
                //==========Turno==========//
                turnoHTML,
                //==========M�o de Obra==========//
                '<input name="maoObra' + ctdAux + '" type="text"  value="' + maoObra.val() + '" class="form-control number maoObra" required>',
                //==========Horas �teis==========//
                '<input name="horasUteis' + ctdAux + '" type="text"  value="' + horasUteis.val() + '" class="form-control number horasUteis" required>',
                //==========Homem Hora==========//
                '<input name="homemHora' + ctdAux + '" type="text"  value="' + homemHora.val() + '" class="form-control" readonly>',
                //==========Trecho Inicial==========//
                trechoInicial.text() + '<input type="hidden" name="trechoInicial' + ctdAux + '" value="' + trechoInicial.val() + '">',
                //==========Trecho Final==========//
                trechoFinal.text() + '<input type="hidden" name="trechoFinal' + ctdAux + '" value="' + trechoFinal.val() + '" >',
                //==========AMV==========//
                amv.text() + '<input type="hidden" name="amv' + ctdAux + '" value="' + amv.val() + '" >',
                //==========VIA==========//
                via.text() + '<input type="hidden" name="via' + ctdAux + '" value="' + via.val() + '" >',
                //==========Km Inicial==========//
                kmInicial.val() + '<input type="hidden" name="kmInicial' + ctdAux + '" value="' + kmInicial.val() + '" >',
                //==========Km Final==========//
                kmFinal.val() + '<input type="hidden" name="kmFinal' + ctdAux + '" value="' + kmFinal.val() + '" >',
                //==========A��es==========//
                '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
            ]);

            contador = contador + 1;
        }
        if (txt != '')
            alertaJquery("Alerta", "DUPLICADOS:\n\n <textarea rows='20' cols='30'>" + txt + "</textarea>", "alert");
    }
});
//=================Fim do On Click Plus==================//

var contadorExec = $("input[name='ctd']").val();

//=================================================//

$("#tabelaPmpVp").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpVp = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaPmpVp._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaPmpVp.fnGetData(i);
        if (dataTable[0] == idPmpVp) {
            tabelaPmpVp.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmp.push(idPmpVp);
    contador = contador - 1;
});

$('.salvarListaPmp').click(function () {
    var data = tabelaPmpVp.$('input, select').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaPmpVpExec.$('input, select').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + contadorExec;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    $.post(
        caminho + "/cadastroGeral/refillPmp",
        {
            sistema:        $('select[name="sistema"] option:selected').val(),
            subSistema:     $('select[name="subSistema"] option:selected').val(),
            servicoPmp:     $('select[name="servicoPmp"] option:selected').val(),
            periodicidade:  $('select[name="periodicidade"] option:selected').val(),
            procedimento:   $('input[name="procedimento"]').val(),
            quinzena:       $('input[name="quinzena"]').val(),
            turno:          $('select[name="turno"] option:selected').val(),
            maoObra:        $('input[name="maoObra"]').val(),
            horasUteis:     $('input[name="horasUteis"]').val(),
            homemHora:      $('input[name="homemHora"]').val(),
            linha:          $('select[name="linha"] option:selected').val(),
            trechoInicial:  $('select[name="trechoInicial"] option:selected').val(),
            trechoFinal:    $('select[name="trechoFinal"] option:selected').val(),
            amv:            $('select[name="amv"] option:selected').val(),
            via:            $('select[name="via"] option:selected').val(),
            kmInicial:      $('input[name="kmInicial"]').val(),
            kmFinal:        $('input[name="kmFinal"]').val()
        }
    );

    if (data) {
        $.post(
            caminho + "/cadastroGeral/salvarListaPmp",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }
});

var ano = $('#pmpAno');
var ativo = $('#pmpAtivo');
var novo = $('#pmpNovo');

ano.change(function(){
    var d = new Date();
    var n = d.getFullYear();

    if(n == $(this).val())
    {
        ativo.prop('disabled', 'disabled');
        ativo.prop('checked', false);
    }else
    {
        ativo.removeAttr('disabled');
    }
});

$('.gerarPmpAnual').click(function () {
    var year = ano.val();
    var isAtivo = ativo.prop('checked');
    var isNovo = novo.prop('checked');

    alertaJquery('Gerar PMP Anual',
        'Tem certeza que deseja finalizar o PMP Anual?<h4>Verifique antes se todas as altera��es foram salvas.</h4>',
        'alert', [{value:"Sim"},{value:"N�o"}], function (res) {
            if(res == "Sim") {
                alertaJquery('Gerar PMP Anual',
                    '<h3>Esta a��o n�o poder� ser desfeita!</h3>Deseja continuar?</br><small>O Processo poder� levar alguns minutos. Por favor, Aguarde.</small>',
                    'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "Sim") {
                            loader();
                            window.location.replace(caminho + "/pmp/gerarPmpAnual/24/"+year+"/"+isAtivo+"/"+isNovo);
                        }
                        if(res == "N�o")
                        {
                            $('#gerarCronograma').modal('show');
                        }
                    }
                );
            }
            if(res == "N�o")
            {
                $('#gerarCronograma').modal('show');
            }
        }
    );
});

$('[data-toggle="tooltip"]').tooltip(
    {
        html: true
    }
);