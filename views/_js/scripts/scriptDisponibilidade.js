/**
 * Created by josue.santos on 28/09/2017.
 */

$('input[name="check"]').change(function () {
    if($(this).prop('checked')){
        $('.div' + $(this).val() + ' .panel').removeClass('panel-default');
        $('.div' + $(this).val() + ' .panel').removeClass('panel-danger');
        $('.div' + $(this).val() + ' .panel').removeClass('panel-warning');
        $('.div' + $(this).val() + ' .panel').removeClass('panel-success');

        $('.div' + $(this).val() + ' .panel').addClass('panel-success');

        $('.div' + $(this).val() + ' select').html('<option value="s" selected>Dispon�vel</option>');
    }else{
        $('.div' + $(this).val() + ' .panel').removeClass('panel-default');
        $('.div' + $(this).val() + ' .panel').removeClass('panel-danger');
        $('.div' + $(this).val() + ' .panel').removeClass('panel-warning');
        $('.div' + $(this).val() + ' .panel').removeClass('panel-success');

        $('.div' + $(this).val() + ' .panel').addClass('panel-danger');

        $('.div' + $(this).val() + ' select').html('<option value="i">Inoperante</option><option value="n" selected>Imobilizado</option>');
    }
});

$('select').change(function () {
    if($(this).val() == 'i'){
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-default');
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-danger');
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-warning');
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-success');

        $('.div' + $(this).attr('identific') + ' .panel').addClass('panel-default');

        $('.div' + $(this).attr('identific') + ' select').html('<option value="i" selected>Inoperante</option><option value="n">Imobilizado</option>');

    }else if($(this).val() == 'n'){
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-default');
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-danger');
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-warning');
        $('.div' + $(this).attr('identific') + ' .panel').removeClass('panel-success');

        $('.div' + $(this).attr('identific') + ' .panel').addClass('panel-danger');

        $('.div' + $(this).attr('identific') + ' select').html('<option value="i">Inoperante</option><option value="n" selected>Imobilizado</option>');
    }
});

// $('input[name^="odometro"]').blur(function () {
//     valorNovo   = parseInt($(this).val());
//     valorAntigo = parseInt($(this).attr('placeholder'));
//
//     if ((valorNovo - valorAntigo) < 0){
//         alertaJquery("Opera��o Inv�lida", "N�o � permitido inser��o de odometro inferior ao anterior", "Alert");
//         $(this).val('');
//     }
// });

// $('input[name^="hori"]').blur(function () {
//     valorNovo   = parseInt($(this).val());
//     valorAntigo = parseInt($(this).attr('placeholder'));
//
//     if ((valorNovo - valorAntigo) < 0){
//         alertaJquery("Opera��o Inv�lida", "N�o � permitido inser��o de horimetro inferior ao anterior", "Alert");
//         $(this).val('');
//     }
// });