/**
 * Created by josue.marques on 09/12/2021.
 */
$(document).ready(function(){
    var material = $('#nome_material');
    var grupo = $('#grupo');
    
    material.on("blur", function () {
        attr = dataListFuncaoDuplicidade(material, $("#materialDataList"));

        if (attr) {
            alertaJquery(
                "Este Material j� existe!", 
                "Este Material j� foi cadastrado, gostaria de continuar com esse cadastro?", 
                "alert", 
                [{ value: "SIM" },{ value: "N�O" }], 
                function(result){
                    if(result == "N�O")
                    {
                        material.val("");
                    }
                    if(result == "SIM" && grupo.val())
                    {
                        verificarGrupoDataList(grupo, material, attr);
                    }
                }
            );
        }
    });
    
    grupo.on("change", function () {
        attr = dataListFuncaoDuplicidade(material, $("#materialDataList"));

        if (attr)
        {
            verificarGrupoDataList(grupo, material, attr);
        }
    });

    function verificarGrupoDataList(grupo, material, datalist)
    {
        for(i = 0; datalist.length > i; i++)
        {
            if (grupo.val() == datalist[i].attributes.grupo.value)
            {
                alertaJquery("Alerta: Nome de Material Duplicado", "Este material j� foi cadastrado para este Grupo de Sistema!<br>Altere o nome do Material.", "error");
                material.val("");
                break;
            }
        }
    }
});