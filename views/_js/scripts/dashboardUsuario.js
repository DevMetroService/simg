
var tableSafD;
var tableSaf;

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tableSafD  = $("#indicadorSafDevolvida").dataTable(configTable(0, false, 'desc'));
        tableSaf   = $("#indicadorSaf").dataTable(configTable(3, false, 'asc'));
    });
});

$(document).ready(function(){
    var aberta      = $('input[name="qtdAberta"]').val();
    var analise     = $('input[name="qtdAnalise"]').val();
    var atendimento = $('input[name="qtdAtendimento"]').val();
    var finalizada  = $('input[name="qtdFinalizada"]').val();

    if(analise > 0){
        $('#actAnalise').addClass('fa fa-cog fa-spin fa-5x');
    }else{
        $('#actAnalise').addClass('fa fa-cog fa-5x');
    }

    if(atendimento > 0){
        $('#actAtendimento').addClass('fa fa-cog fa-spin fa-5x');
    }else{
        $('#actAtendimento').addClass('fa fa-cog fa-5x');
    }

    $('#ctdAberta').html(aberta);
    $('#ctdAnalise').html(analise);
    $('#ctdAtendimento').html(atendimento);
    $('#ctdFinalizada').html(finalizada);
});

function alertMessageBox(mensagem){
    inserirNotificacao(mensagem, "Alerta SIMG", "warning");
}

