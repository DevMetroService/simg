/**
 * Created by josue.santos on 27/09/2016.
 */
//################################# Dashboard Engenharia #######################################
//################################# Dashboard Engenharia #######################################
//################################# Dashboard Engenharia #######################################

$("#indicadorPMPED").dataTable(configTable(0, false, 'des'));

$("#indicadorPMPRA").dataTable(configTable(0, false, 'asc'));

$("#indicadorPMPSB").dataTable(configTable(0, false, 'asc'));

$("#indicadorPMPVP").dataTable(configTable(0, false, 'asc'));

$("#indicadorPMPMR").dataTable(configTable(0, false, 'asc'));

$("#indicadorPMPTL").dataTable(configTable(0, false, 'asc'));

$("#indicadorPMPBL").dataTable(configTable(0, false, 'asc'));

$("#indicadorPMPJD").dataTable(configTable(0, false, 'asc'));

var tableModal = $(".tableRelModal").dataTable(configTable(0, false, 'asc'));

$(document).on("click",".btnImprimirPMPEdificacao", function(){
    window.open(caminho+"/dashboardPmp/pdfEd", "_blank");
});

$(document).on("click",".btnImprimirPMPEdificacaoQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfEdQzn", "_blank");
});



$(document).on("click",".btnImprimirPMPRedeAerea", function(){
    window.open(caminho+"/dashboardPmp/pdfRa", "_blank");
});

$(document).on("click",".btnImprimirPMPRedeAereaQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfRaQzn", "_blank");
});



$(document).on("click",".btnImprimirPMPSubestacao", function(){
    window.open(caminho+"/dashboardPmp/pdfSb", "_blank");
});

$(document).on("click",".btnImprimirPMPSubestacaoQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfSbQzn", "_blank");
});



$(document).on("click",".btnImprimirPMPViaPermanente", function(){
    window.open(caminho+"/dashboardPmp/pdfVp", "_blank");
});

$(document).on("click",".btnImprimirPMPViaPermanenteQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfVpQzn", "_blank");
});



$(document).on("click",".btnImprimirPMPTelecom", function(){
    window.open(caminho+"/dashboardPmp/pdfTl", "_blank");
});

$(document).on("click",".btnImprimirPMPTelecomQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfTlQzn", "_blank");
});



$(document).on("click",".btnImprimirPMPBilhetagem", function(){
    window.open(caminho+"/dashboardPmp/pdfBl", "_blank");
});

$(document).on("click",".btnImprimirPMPBilhetagemQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfBlQzn", "_blank");
});



$(document).on("click",".btnImprimirPMPJardins", function(){
    window.open(caminho+"/dashboardPmp/pdfJd", "_blank");
});

$(document).on("click",".btnImprimirPMPJardinsQzn", function(){
    window.open(caminho+"/dashboardPmp/pdfJdQzn", "_blank");
});


$(document).on("click",".abrirModal", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var pmp     = dados.split('<input')[0];
    var grupo   = dados.split('value="')[1].split('"')[0];

    apiPmpCronograma(pmp, grupo);
});