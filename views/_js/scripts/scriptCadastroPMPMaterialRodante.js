$('.oculto').css({'display':'none'});

var tbPmpVLT = {
    Sul: $("#tablePmpVLT_Sul").DataTable(configTable(0, false, 'asc')),
    Oeste: $("#tablePmpVLT_Oeste").DataTable(configTable(0, false, 'asc')),
    Cariri: $("#tablePmpVLT_Cariri").DataTable(configTable(0, false, 'asc')),
    Sobral: $("#tablePmpVLT_Sobral").DataTable(configTable(0, false, 'asc')),
    MrParangMucuripe: $("#tablePmpVLT_ParangMucuripe").DataTable(configTable(0, false, 'asc')),
};

var tbPmpTUE = {
    Sul: $("#tablePmpTUE_Sul").DataTable(configTable(0, false, 'asc')),
    Oeste: $("#tablePmpTUE_Oeste").DataTable(configTable(0, false, 'asc')),
    Cariri: $("#tablePmpTUE_Cariri").DataTable(configTable(0, false, 'asc')),
    Sobral: $("#tablePmpTUE_Sobral").DataTable(configTable(0, false, 'asc')),
    MrParangMucuripe: $("#tablePmpTUE_ParangMucuripe").DataTable(configTable(0, false, 'asc')),
};

var pmp_mr      = $('#cod_pmp_mr');
var servico_mr  = $('#cod_servico_mr');
var linha       = $('#cod_linha');
var mes             = $('#mes');
var ano             = $('#ano');
var qtd_manutencao  = $('#qtd_manutencao');
var grupo_sistema   = $('#grupo');


$(document).on("click",".editFrota", function(e){
    e.preventDefault();

    var row = $(this).parent("td").parent("tr").find('td');

    pmp_mr.val($(this).data('value'));

    var grupo = $(this).data('grupo');
    grupo_sistema.val(grupo);

    mes.val(row.data('value'));
    ano.val(row.next().html());

    var servico = row.next().next().data('value');

    filterServicoPorGrupo(grupo, servico, servico_mr);

    linha.val(row.next().next().next().data('value'));
    qtd_manutencao.val(row.next().next().next().next().html());


    $("html, body").animate({scrollTop : 0},700);
});

$(document).on("click", "#btnSubmit", function(e){
    e.preventDefault();

    verificaPreenchimentoFormulario();


    //TODO: VERIFICACAO DE DUPLICIDADE E SUBMIT APOS VERIFICACAO
    //Verifica Duplicacao em BD
    function verificaDuplicidade (dataTable) {

        var duplicado = false;

        dataTable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if(!duplicado){
                if(
                    this.node().cells[2].dataset.value == servico_mr.val() &&
                    this.node().cells[3].dataset.value == linha.val() &&
                    this.node().cells[0].dataset.value == mes.val() &&
                    this.node().cells[1].dataset.value == ano.val()
                ){
                    duplicado = true;
                }
            }
        } );

        //
        if (duplicado) {
            console.log("NO SUBMIT -> Duplicado");
            alertaJquery (
                'Duplicacao em Banco',
                'Item PMP n�o Criado!\nExiste um Registro para este Servi�o, Linha, M�s e Ano',
                'error'
            );
        } else {
            console.log('YES Submiting ...')
            $("#formCadastroPMPMr").submit();
        }

    }


    var cod_grupo_pmp = pmp_mr.val();
    var cod_linha = linha.val();

    if (cod_grupo_pmp == 22) {

        switch (cod_linha) {
            case 1 : verificaDuplicidade (tbPmpVLT.Oeste); break; //Oeste
            case 2 : verificaDuplicidade (tbPmpVLT.Cariri); break; //Cariri
            case 5 : verificaDuplicidade (tbPmpVLT.Sul); break; //Sul
            case 6 : verificaDuplicidade (tbPmpVLT.Sobral); break; //Sobral
            case 7 : verificaDuplicidade (tbPmpVLT.MrParangMucuripe); break; //Parangaba-Mucuripe
        }

    }


    if (cod_grupo_pmp == 23) {

        switch (cod_linha) {
            case 1 : verificaDuplicidade (tbPmpTUE.Oeste); break; //Oeste
            case 2 : verificaDuplicidade (tbPmpTUE.Cariri); break; //Cariri
            case 5 : verificaDuplicidade (tbPmpTUE.Sul); break; //Sul
            case 6 : verificaDuplicidade (tbPmpTUE.Sobral); break; //Sobral
            case 7 : verificaDuplicidade (tbPmpTUE.MrParangMucuripe); break; //Parangaba-Mucuripe
        }

    }





});

$(document).on('change', '#cod_servico_mr', function () {
    /* verifica se ha um cod_pmp(no caso de uma edicao
    ter sido solicitada, se sim, nao limpa os campos.*/
    if(pmp_mr.val() != null) {
        return false;
    }

    //limpa os campos
    linha.val(null);
    mes.val(null);
    ano.val(null);
    qtd_manutencao.val(null);
});

$(document).on('change', '#grupo', function () {

    var grupo = $(this).val();


    /* verifica se ha um cod_pmp(no caso de uma edicao
    ter sido solicitada, se sim, limpa apenas o servico.*/
    if(pmp_mr.val() != null) {
        servico_mr.val(null);
        filterServicoPorGrupo(grupo, null, servico_mr);

        return false;
    }


    servico_mr.val(null);
    linha.val(null);
    mes.val(null);
    ano.val(null);
    qtd_manutencao.val(null);

    filterServicoPorGrupo(grupo, null, servico_mr);
});



$(document).on('click', '#resetBtn', function () {
    pmp_mr.val(null);
    servico_mr.val(null);
    linha.val(null);
    mes.val(null);
    ano.val(null);
    qtd_manutencao.val(null);
});

function filterServicoPorGrupo(grupo, cod_servico, servico_mr){
    $.getJSON(
        caminho + "/api/returnResult/grupo/getGrupoServicoMr",
        {grupo: grupo},
        function (json) {
            console.log(json);
            var options = "";
            options += '<option selected="selected" value="">TODOS</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            servico_mr.html(options);
            servico_mr.val(cod_servico);
        }
    );
}

function verificaPreenchimentoFormulario() {
    //Verifica Preenchimento do Formulario
    var ok;
    [
        grupo_sistema.val(),
        servico_mr.val(),
        linha.val(),
        mes.val(),
        ano.val(),
        qtd_manutencao.val()

    ].map(function (val) {

        if ( val == null || val == "" ) {
            ok = false;
        }

    });


    if(ok == false){
        alertaJquery (
            'Sem informa��o',
            'N�o h� informa��es suficientes para registro.',
            'error'
        );

        return false;
    }

}