/**
 * Created by josue.marques on 03/06/2016.
 */

//####################### Fun��es API javascript ###################################################
//####################### Fun��es API javascript ###################################################
//####################### Fun��es API javascript ###################################################


function apiMatriculaNomeFuncionario(tipo_funcionario, matricula, nome) {
    if (matricula.val() == '') return;

    nome.val("Procurando...");

    var dados = [tipo_funcionario.val(), matricula.val()];

    $.getJSON(
        caminho + "/api/returnResult/matricula",
        {matricula: dados},
        function (json) {
            nome.val(json.nome);
        }
    ).error(function () {
        matricula.val('');
        nome.val('');
        alertaJquery("Alerta", 'Matricula inv�lida', "alert");
    });
}

function apiListaOsmPorSsm(cod_ssm, infosModal) {
    loader();
    if (cod_ssm == '') return;

    $.getJSON(
        caminho + "/api/returnResult/getListaOsmPorSsm",
        {getListaOsmPorSsm: cod_ssm},
        function (json) {
            var opt="";

            json.forEach(function (opcao) {

                var contador = json[1];
                if (contador!= null) {opt += '<br> <div class="divider"></div>'};

                opt += '<div class="row">' +
                    '                                    <div class="col-md-6">' +
                    '                                        <label>C�digo da OSM</label><input id="codOsmModal" class="form-control" value="' + opcao.osm + '" readonly/>' +
                    '                                    </div>' +
                    '                                    <div class="col-md-6">' +
                    '                                        <label>Atua��o</label><input id="atuacaoModal" class="form-control" type="text" value="' + opcao.atuacao + '" readonly/>' +
                    '                                    </div>' +
                    '                                </div>' +
                    '                                <div class="row">' +
                    '                                    <div class="col-md-12">' +
                    '                                        <label>Descri��o da Atua��o</label>' +
                    '                                        <div>' +
                    '                                            <textarea id="descricaoModal" class="form-control" type="text" readonly>' + opcao.descricao + '</textarea>' +
                    '                                        </div>' +
                    '                                    </div>' +
                    '                                </div>';
            });
            infosModal.html(opt);

        }

    ).error(function () {
        alertaJquery("Alerta", 'Erro! Procure o Administrador do SIMG.', "alert");
    }).success(function(){
        loading();
    });
}

function apiValidaDevolveSsm(ssm, status, motivo) {
    $.get(
        caminho + "/ssm/aproveValidation/"+ssm,
        {cod_status: status, justify: motivo}
    ).done(function () {
        var text = "";
        if(status==36)
            text = "devolvida";
        else
            text = "aprovada";

        alertaJquery('Confirma��o', 'A SSM de n�mero ' + ssm + ' foi '+text+' com sucesso!', 'info',[{value:'Ok'}], function (res) {
            if (res == 'Ok') $('#modal-validador').modal('hide');
        });
    });
}

function apiPostePorLocal(local, postes) {
    if (local.val() == "") return;

    $.getJSON(caminho + "/api/returnResult/local/localPoste",
        {local: local.val()},
        function (json) {
            var opcoes = "<option disabled selected value=''>Selecione o Poste</option>" + json.opcao;
            postes.html(opcoes);
        }
    );
}

function validadeOsm(cod_os) {
    if (cod_os.val() == '') return;

    $.getJSON(caminho + "/api/returnResult/codOs/valDuplicidadeOsm",
        {codOs: cod_os.val()},
        function (json) {
            // alert(JSON.stringify(json));
            if (json.validation == 'false') {
                cod_os.val('');
                alertaJquery('Falha na valida��o', 'C�digo Osm n�o existe', 'Error');
            }
        });
}

function validadeOsp(cod_os) {
    if (cod_os.val() == '') return;

    $.getJSON(caminho + "/api/returnResult/codOs/valDuplicidadeOsp",
        {codOs: cod_os.val()},
        function (json) {
            // alert(JSON.stringify(json));
            // alert(cod_os.val());
            if (json.validation == 'false') {
                cod_os.val('');
                alertaJquery('Falha na valida��o', 'C�digo Osp n�o existe', 'Error');
            }
        });
}

function apiMatriculaFuncionario(matricula, nome, cpf, contato, tipo_funcionario) {
    if (matricula.val() == '') return;

    var dados = [tipo_funcionario.val(), matricula.val()];
    nome.val("Procurando...");

    $.getJSON(
        caminho + "/api/returnResult/matricula",
        {matricula: dados},
        function (json) {
            if(json.statusPesquisa == "false")
            {
                alertaJquery("N�o encontrado", "O solicitante n�o foi localizado em nosso banco de dados. Verifique as informa��es ou entrem em contato com a �rea respons�vel pelo cadastro.", "info");
                matricula.val('');
                nome.val('');
                cpf.val('');
                contato.val('');
            }else{
                nome.val(json.nome);
                cpf.val(json.cpf);
                contato.val(json.celular);

                if(nome.val() != "")
                    nome.parent('div').removeClass('has-error');
                if(cpf.val() != "")
                    cpf.parent('div').removeClass('has-error');
                if(contato.val() != "")
                    contato.parent('div').removeClass('has-error');
            }
        }
    ).error(function () {
        matricula.val('');
        nome.val('');

        cpf.val('');
        contato.val('');
        alertaJquery("Alerta", 'Matricula inv�lida', "alert");
    });
}

function apiGrupoSistema(grupo, sistema, valueDefault = null) {
    $.getJSON(
        caminho + "/api/returnResult/grupo/sistema",
        {grupo: grupo.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Sistema</option>';
            $.each(json, function (key, value) {
                if(valueDefault == value)
                    options += '<option value="' + value + '" selected>' + key + '</option>';
                else
                    options += '<option value="' + value + '">' + key + '</option>';
            });
            sistema.html(options);
        }
    );
}

function apiGrupoServico(grupo, servico) {
    $.getJSON(
        caminho + "/api/returnResult/grupo/getGrupoServico",
        {grupo: grupo.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">TODOS</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            servico.html(options);
        }
    );
}

function apiLocalSubLocal(local, sublocal){
    $.getJSON(
        caminho + "/api/returnResult/grupo/subLocal",
        {grupo: local.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">TODOS</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            sublocal.html(options);
        }
    );
}

function apiSistemaSub(sistema, sub, valueDefault = null) {
    $.getJSON(
        caminho + "/api/returnResult/sistema/subsistema",
        {sistema: sistema.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected " value="">Subsistema</option>';
            $.each(json, function (key, value) {
                if(valueDefault == value)
                    options += '<option value="' + value + '" selected>' + key + '</option>';
                else
                    options += '<option value="' + value + '">' + key + '</option>';
            });
            sub.html(options);
        }
    );
}

function apiLocalEquipe(local, equipe) {
    $.getJSON(
        caminho + "/api/returnResult/local/equipe",
        {local: local.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Equipe</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            equipe.html(options);
        }
    );
}

function apiLinhaTrecho(linha, trecho, default_value = null) {
    $.getJSON(
        caminho + "/api/returnResult/linha/linhaTrecho",
        {linha: linha.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Trecho</option>';
            $.each(json, function (key, value) {
                if(default_value == key)
                    options += '<option value="' + key + '" selected>' + value + '</option>';
                else
                    options += '<option value="' + key + '">' + value + '</option>';
            });

            trecho.html(options);
        }
    );
}

function apiLinhaEstacao(linha, estacao, default_value=null) {
    $.getJSON(
        caminho + "/api/returnResult/linha/linhaEstacao",
        {linha: linha.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Esta��o</option>';
            $.each(json, function (key, value) {
                if(default_value == key)
                    options += '<option value="' + key + '" selected>' + value + '</option>';
                else
                    options += '<option value="' + key + '">' + value + '</option>';
            });

            estacao.html(options);
        }
    );
}

function apiLinhaVia(linha, via) {
    $.getJSON(
        caminho + "/api/returnResult/linha/linhaVia",
        {
            linha: linha.val()
        },
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Via</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });

            via.html(options);
        }
    );
}

function apiLinhaPn(linha, pn) {
    $.getJSON(
        caminho + "/api/returnResult/linha/linhaPn",
        {linha: linha.val()},
        function (json) {
            var option = "";
            option += '<option selected="selected" value="">Pontos Not�veis</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            pn.html(option);
        }
    );
}

function apiTrechoPn(trecho, pn) {
    $.getJSON(
        caminho + "/api/returnResult/trecho/trechoPn",
        {trecho: trecho.val()},
        function (json) {
            var option = "";
            option += '<option selected="selected" value="">Pontos Not�veis</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            pn.html(option);
        }
    );
}

function apiSistemaSubSistema(sistema, subsistema, servico) {
    var arr = [sistema.val(), subsistema.val()];
    $.getJSON(
        caminho + "/api/returnResult/dados/getServicoPmpSubsistema",
        {dados: arr},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione o Servi�o</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            servico.html(options);
        }
    );
}

function apiServicoPeriodicidade(servico, sistema, subsistema, periodicidade) {
    var arr = [servico.val(), sistema.val(), subsistema.val()];
    $.getJSON(
        caminho + "/api/returnResult/dados/getServicoPeriodicidade",
        {dados: arr},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione uma Periodicidade</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            periodicidade.html(options);
        }
    );
}

function apiPeriodicidadeProcedimento(periodicidade, servico, sistema, subsistema, procedimento) {
    var arr = [periodicidade.val(), servico.val(), sistema.val(), subsistema.val()];
    $.getJSON(
        caminho + "/api/returnResult/dados/getPeriodicidadeProcedimento",
        {dados: arr},
        function (json) {
            procedimento.val(json.procedimento);
        }
    );
}

function apiLinhaLocal(linha, local) {
    $.getJSON(
        caminho + "/api/returnResult/linha/getLinhaLocal",
        {linha: linha.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione o Local</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            local.html(options);
        }
    );
}

function apiLinhaPrefixo(linha, prefixo) {
    $.getJSON(
        caminho + "/api/returnResult/linha/getLinhaPrefixo",
        {linha: linha.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione o Prefixo</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            prefixo.html(options);
        }
    );
}

function apiLinhaLocalgrupo(linha, grupo, local) {
    var arr = [linha.val(), grupo.val()];
    $.getJSON(
        caminho + "/api/returnResult/linha/getLinhaLocal",
        {linha: arr},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione o Local</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            local.html(options);
        }
    );
}

function apiGetServicoMr(grupo, servico, select) {
    $.getJSON(
        caminho + "/api/returnResult/grupo/getServicoMr",
        {grupo: grupo.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione o Servi�o</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            servico.html(options);
        }
    ).done(function(){
        if(select){
            servico.val(select);
        }
    });


}

function apiEstacaoAmv(estacao, amv) {
    $.getJSON(
        caminho + "/api/returnResult/estacao/getEstacaoAmv",
        {estacao: estacao.val()},
        function (json) {
            var options = "";
            options += '<option selected="selected" value="">Selecione o AMV</option>';
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            amv.html(options);
        }
    );
}

function apiOsmpCronograma(cronograma, grupo, tableModal) {
    $("#loading").show();

    $('.ExibirPmp input').val('');

    var arr = [cronograma, grupo.substring(0, 2)];

    $.getJSON(
        caminho + "/api/returnResult/dados/getOsmpCronograma",
        {dados: arr},
        function (json) {
            $('.ExibirPmp input[name="cronogramaModal"]').val(cronograma);
            $('.ExibirPmp input[name="grupoModal"]').val(grupo);
            $('.ExibirPmp input[name="sistemaModal"]').val(json.sistema);
            $('.ExibirPmp input[name="subsistemaModal"]').val(json.subsistema);
            $('.ExibirPmp input[name="servicoModal"]').val(json.servico);
            $('.ExibirPmp input[name="procedimentoModal"]').val(json.procedimento);
            $('.ExibirPmp input[name="periodicidadeModal"]').val(json.periodicidade);
            $('.ExibirPmp input[name="linhaModal"]').val(json.linha);
            $('.ExibirPmp textarea[name="localModal"]').val(json.local);
            $('.ExibirPmp input[name="hhModal"]').val(json.hh);
            $('.ExibirPmp input[name="quinzenaModal"]').val(json.quinzena);
            // $('.ExibirPmp input[name="inicioModal"]').val(json.inicio);
            // $('.ExibirPmp input[name="terminoModal"]').val(json.termino);
            $('.ExibirPmp input[name="statusModal"]').val(json.status);

            if (tableModal != null) {
                tableModal.fnClearTable();

                if (json.osmps.length) {
                    $.each(json.osmps, function (key, value) {
                        tableModal.fnAddData([
                            value.osmp,
                            value.ssmp,
                            value.data,
                            value.status,
                            "<button class='btnEditarOsmp btn btn-circle btn-success' value='" + json.form + "'><i class='fa fa-eye'></i></button>"
                        ]);
                    });
                }
            }

            $("#loading").delay(500).fadeOut("slow");
        }
    );
}

function apiModalCronograma(cronograma, grupo, tableModal) {
    $("#loading").show();

    $('.ExibirCronograma input').val('');

    var arr = [cronograma, grupo.substring(0, 2)];

    $.getJSON(
        caminho + "/api/returnResult/dados/getOsmpCronograma",
        {dados: arr},
        function (json) {
            // alert(JSON.stringify(json));
            $('.ExibirCronograma input[name="cronogramaModal"]').val(cronograma);
            $('.ExibirCronograma input[name="grupoModal"]').val(grupo);
            $('.ExibirCronograma input[name="sistemaModal"]').val(json.sistema);
            $('.ExibirCronograma input[name="subsistemaModal"]').val(json.subsistema);
            $('.ExibirCronograma input[name="servicoModal"]').val(json.servico);
            $('.ExibirCronograma input[name="procedimentoModal"]').val(json.procedimento);
            $('.ExibirCronograma input[name="periodicidadeModal"]').val(json.periodicidade);
            $('.ExibirCronograma input[name="linhaModal"]').val(json.linha);
            $('.ExibirCronograma textarea[name="localModal"]').val(json.local);
            $('.ExibirCronograma input[name="hhModal"]').val(json.hh);
            $('.ExibirCronograma input[name="quinzenaModal"]').val(json.quinzena);
            $('.ExibirCronograma input[name="statusModal"]').val(json.status);
            $('.ExibirCronograma .dadosSsmp').html("");

            if (json.ssmps.length) {
                $.each(json.ssmps, function (key, value) {
                    $('.ExibirCronograma .dadosSsmp').html($('.ExibirCronograma .dadosSsmp').html() +
                        "<div class='row'>" +
                        "<div class='col-md-3'>" +
                        "<label>Cod. SSMP</label>" +
                        "<input class='form-control' readonly name='ssmpModal' value='" + value.ssmp + "' />" +
                        "</div>" +
                        "<div class='col-md-3'>" +
                        "<label>Status Ssmp</label>" +
                        "<input class='form-control' readonly name='statusSsmpModal' value='" + value.status + "' />" +
                        "</div>" +
                        "<div class='col-md-3'>" +
                        "<label>In�cio</label>" +
                        "<input class='form-control' readonly name='inicioModal' value='" + value.inicio + "' />" +
                        "</div>" +
                        "<div class='col-md-3'>" +
                        "<label>T�rmino Previsto</label>" +
                        "<input class='form-control' readonly name='terminoModal' value='" + value.termino + "' />" +
                        "</div>" +
                        "</div>");
                });
            }

            if (tableModal != null) {
                tableModal.fnClearTable();

                if (json.osmps.length) {
                    $.each(json.osmps, function (key, value) {
                        tableModal.fnAddData([
                            value.osmp,
                            value.ssmp,
                            value.data,
                            value.status,
                            // "<button class='btnEditarOsmp btn btn-circle btn-success' value='" + json.form + "' data-dismiss='modal' data-toggle='modal' data-target='.osDetalhes'><i class='fa fa-eye'></i></button>"
                            "<button class='btnEditarOsmp btn btn-circle btn-success' value='" + json.form + "'><i class='fa fa-eye'></i></button>"
                        ]);
                    });
                }
            }

            $("#loading").delay(500).fadeOut("slow");
        }
    );
}

function apiGetSaf($dados) {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.getJSON(
        caminho + "/api/returnResult/codigoSaf/getSafByCod",
        {codigoSaf: dados},
        function (json) {
            // alert(JSON.stringify(json));
            // alert(json.cod_saf);
            $('#codSafModal').val(json.cod_saf);
            $('#responsavelModal').val(json.usuario);
            $('#solicitanteModal').val(json.nome);
            $('#contatoSolicitanteModal').val(json.contato);
            $('#linhaModal').val(json.nome_linha);
            $('#trechoModal').val(json.nome_trecho);
            $('#pontoNotavelModal').val(json.nome_ponto);
            $('#complementoLocalModal').val(json.complemento_local);
            $('#viaModal').val(json.nome_via);
            $('#posicaoModal').val(json.posicao);
            $('#kmInicialModal').val(json.km_inicial);
            $('#kmFinalModal').val(json.km_final);
            $('#grupoModal').val(json.nome_grupo);
            $('#sistemaModal').val(json.nome_sistema);
            $('#subsistemaModal').val(json.nome_subsistema);
            $('#avariaModal').val(json.nome_avaria);
            $('#nivelModal').val(json.nivel);
            $('#complementoAvariaModal').val(json.complemento_falha);
        }
    );
}

//Api espec�fico para Material Rodante

function apiGetComposicaoPorTipo(identificacaoMR, composicaoMR) {

    var tipo = identificacaoMR.val();

    $.getJSON(
        caminho + "/api/returnResult/tipo/getComposicaoPorTipo",
        {tipo: tipo},
        function (json) {
            // alert (JSON.stringify(json));
            var option = "";
            option += '<option selected="selected" value="">Composi��o</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            composicaoMR.html(option);
        }
    );
}

function apiGetAvariaGrupo(grupo, avaria) {

    var tipo = grupo.val();

    $.getJSON(
        caminho + "/api/returnResult/tipo/getAvariaGrupo",
        {tipo: tipo},
        function (json) {
            var option = "";
            $.each(json, function (key, value) {
                option += ('<option value="' + value + '">' + key + '</option>').toUpperCase();
            });
            avaria.html(option);
        }
    );
}

function apiGetAvariaDataList(grupo, avaria) {

    var tipo = grupo.val();

    $.getJSON(
        caminho + "/api/returnResult/tipo/getAvariaGrupo",
        {tipo: tipo},
        function (json) {
            var option = "";
            $.each(json, function (key, value) {
                option += ('<option id="' + value + '" value="' + key + '"/>').toUpperCase();
            });
            avaria.html(option);
        }
    );
}


function apiGetTueComposicao(composicaoMR, tueMR) {
    var composicao = composicaoMR.html();

    $.getJSON(
        caminho + "/api/returnResult/composicao/getTueComposicao",
        {composicao: composicao},
        function (json) {
            // alert (JSON.stringify(json));
            var option = "";
            option += '<option disabled selected="selected" value="">Ve�culo</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            tueMR.html(option);
        }
    );
}

function apiComposicaoConfigForm(composicao, veiculo, botaoAdd) {
    veiculo.val('');
    $.getJSON(
        caminho + "/api/returnResult/composicao/getVeiculoComp",
        {composicao: composicao},
        function (json) {
            veiculo.val(json[0]);
            veiculo.change();

            for (var i = 1; i < json.length; i++) {
                botaoAdd.trigger("click", [json[i]]);
            }
        }
    );
}

function apiGetVeiculo(tipo, veiculoMr, linha, select, all) {
    loader();

    if(linha != null)
        var dados = [tipo, linha];
    else{
        var dados = [tipo];
    }
    if(all != false) all = true;

    $.getJSON(
        caminho + "/api/returnResult/dados/getVeiculo",
        {dados: dados},
        function (json) {
            var option = "";

            if(all)
                option += '<option selected value="">TODOS</option>';
            else
                option += '<option selected value="">Selecione Veiculo</option>';

            $.each(json, function (key, value) {
                if (select == key)
                    option += '<option value="' + value + '" selected>' + key + '</option>';
                else
                    option += '<option value="' + value + '">' + key + '</option>';
            });
            veiculoMr.html(option);

            if (select)
                veiculoMr.change();
            loading(500);
        }
    );
}

function apiGetFrota(grupo, frota) {
    loader();
    $.getJSON(
        caminho + "/api/returnResult/grupo/getFrota",
        {grupo: grupo},
        function (json) {
            var option = "";
            option += '<option selected="selected" value="">Selecione</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + value + '">' + key + '</option>';
            });
            frota.html(option);

            if (frota)
                frota.change();
            loading(500);
        }
    );
}

function verificarCarro(veiculo, carro, multi) {
    $.getJSON(
        caminho + "/api/returnResult/carro/getVerificarCarro",
        {carro: carro},
        function (json) {
            if (json.cod) {
                if (json.cod != veiculo.val()) {
                    alertaJquery("Aviso", '<h4>Este Carro pertence ao ve�culo ' + json.nome + '.</h4>Ao salvar o processo o carro mudar� para o ve�culo ' + veiculo.text() + '.<h4>Deseja continuar?</h4>', "confirm", [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "N�o") {
                            multi.multiselect('deselect', [carro]);
                        }
                    });
                }
            }
        }
    );
}

function apiCarro(tipo, filtro, carroMr, all) {
    var dados = [tipo, filtro];
    if(all != false) all = true;
    $.getJSON(
        caminho + "/api/returnResult/dados/getCarroMr",
        {dados: dados},
        function (json) {
            var option = "";
            if(all)
                option += '<option selected value="">TODOS</option>';
            else
                option += '<option selected value="">Selecione Carro</option>';

            if(json.motor.toString()) {
                option += '<optgroup label="CARRO MOTOR"></optgroup>';
                $.each(json.motor, function (key, value) {
                    option += '<option value="' + key + '" data-odometro="' + value[1] + '" data-sigla="' + value[2] + '">' + value[0] + '</option>';
                });
            }

            if(json.sem.toString()) {
                option += '<optgroup label="CARRO SEM MOTOR"></optgroup>';
                $.each(json.sem, function (key, value) {
                    option += '<option value="' + key + '" data-odometro="' + value[1] + '" data-sigla="' + value[2] + '">' + value[0] + '</option>';
                });
            }
            carroMr.html(option);
        }
    );
}

function apiCarroMultiSelect(grupo, veiculo, carro) {
    var dados = ['multi', [grupo, veiculo]];

    $.getJSON(
        caminho + "/api/returnResult/dados/getCarroMr",
        {dados: dados},
        function (json) {
            optionSelect = new Array();
            childrenMotor = new Array();
            childrenSem = new Array();

            $.each(json.veiculo, function (key, value) {
                optionSelect.push(key);
            });

            $.each(json.motor, function (key, value) {
                childrenMotor.push({label: value[0], value: key});
            });

            $.each(json.sem, function (key, value) {
                childrenSem.push({label: value[0], value: key});
            });

            var data = [
                {
                    label: 'Carro Motor',
                    children: childrenMotor
                },
                {
                    label: 'Carro Sem Motor',
                    children: childrenSem
                }
            ];

            carro.multiselect('dataprovider', data);
            carro.multiselect('select', optionSelect);
        }
    );
}

function apiGetCarro(veiculoMr, carroMr) {
    var veiculo = veiculoMr.val();

    $.getJSON(
        caminho + "/api/returnResult/veiculo/getCarro",
        {veiculo: veiculo},
        function (json) {
            // alert (JSON.stringify(json));
            var option = "";
            option += '<option disabled selected="selected" value="">Carro</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            carroMr.html(option);
        }
    );
}

function apiGetCarroVlt(vltMr, carroMr) {
    var vlt = vltMr.val();

    $.getJSON(
        caminho + "/api/returnResult/vlt/getCarroVlt",
        {vlt: vlt},
        function (json) {
            // alert (JSON.stringify(json));
            var option = "";
            option += '<option disabled selected="selected" value="">Carro</option>';
            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            carroMr.html(option);
        }
    );
}

//fun��o de click espec�fica para gr�ficos barra do cronograma DIRETORIA
function apiListaCronograma_grafico(grupo, tableModal, status, mes) {
    if (tableModal != null) {
        tableModal.fnClearTable();
    }
    var sigla_grupo = grupo.substring(0, 2).toLowerCase();
    if (sigla_grupo == "vi")
        sigla_grupo = 'vp';

    var arr = [status, sigla_grupo, mes];

    $("#loading").show();
    // alert(arr);

    $.getJSON(
        caminho + "/api/returnResult/dados/getListaCronograma_grafico",
        {dados: arr},
        function (json) {
            // alert(JSON.stringify(json));
            if (json.length) {
                $.each(json, function (key, value) {
                    tableModal.fnAddData([
                        value.cod_cronograma_pmp + " <input type='hidden' value='" + grupo + "'>",
                        value.quinzena_exec,
                        value.nome_sistema,
                        value.nome_subsistema,
                        value.nome_servico_pmp,
                        value.nome_status,
                        "<button class='btnVisualizarCronograma btn btn-circle btn-success' value='" + json.form + "' data-dismiss='modal' data-toggle='modal' data-target='.ExibirCronograma'><i class='fa fa-eye'></i></button>"
                    ]);
                });
            }
            $("#loading").delay(500).fadeOut("slow");
        }
    ).fail(function () {
        alert("Ocorreu um problema ao tentar acesso aos dados. Entre em contato com a T.I.");
        $("#loading").delay(500).fadeOut("slow");
    }).always(function () {
        //atualiza o backdrop fade para se extender de acordo com a exten��o atual.
        $('.listaCronogramas').data('bs.modal').handleUpdate();
        $('select[name="indicadorListaCronograma_modal_length"]').change(function () {
            $('.listaCronogramas').data('bs.modal').handleUpdate();
        });
    });

}

function apiModalSaf(saf, tableModal) {
    $("#loading").show();

    $('.ExibirSaf input').val('');
    $('.ExibirSaf textarea').val('');

    $.getJSON(
        caminho + "/api/returnResult/dados/getOsmSaf",
        {dados: saf},
        function (json) {
            $('.ExibirSaf input[name="safModal"]').val(saf);
            $('.ExibirSaf input[name="solicitanteModal"]').val(json.solicitante);
            $('.ExibirSaf input[name="dataAberturaModal"]').val(json.dataAbertura);
            $('.ExibirSaf input[name="grupoModal"]').val(json.grupo);
            $('.ExibirSaf input[name="sistemaModal"]').val(json.sistema);
            $('.ExibirSaf input[name="subsistemaModal"]').val(json.subsistema);
            $('.ExibirSaf input[name="avariaModal"]').val(json.avaria);
            $('.ExibirSaf input[name="nivelModal"]').val(json.nivel);
            $('.ExibirSaf textarea[name="complementoAvariaModal"]').val(json.complementoAvaria);
            $('.ExibirSaf input[name="linhaModal"]').val(json.linha);
            $('.ExibirSaf input[name="trechoModal"]').val(json.trecho);
            $('.ExibirSaf input[name="pontoNotavelModal"]').val(json.pontoNotavel);
            $('.ExibirSaf textarea[name="complementoLocalModal"]').val(json.complementoLocal);
            $('.ExibirSaf input[name="statusSafModal"]').val(json.statusSaf);
            $('.ExibirSaf textarea[name="descricaoStatusModal"]').val(json.descricaoStatus);
            $('.ExibirSaf .dadosSsm').html("");

            if (json.ssms.length) {
                $.each(json.ssms, function (key, value) {
                    $('.ExibirSaf .dadosSsm').html($('.ExibirSaf .dadosSsm').html() +
                        "<div class='row'>" +

                        "<div class='col-md-12'>" +
                        "<hr style='height: 10px; box-shadow: 0 10px 10px -10px #8c8b8b inset; margin-bottom: 15px' />" +
                        "</div>" +

                        "</div>" +
                        "<div class='row'>" +

                        "<div class='col-xs-3'>" +
                        "<label>Cod. SSM</label>" +
                        "<input class='form-control' readonly name='ssmModal' value='" + value.ssm + "' />" +
                        "</div>" +
                        "<div class='col-xs-6'>" +
                        "<label>Servi�o</label>" +
                        "<input class='form-control' readonly name='servicoModal' value='" + value.servico + "' />" +
                        "</div>" +
                        "<div class='col-xs-3'>" +
                        "<label>Status Ssm</label>" +
                        "<input class='form-control' readonly name='statusSsmModal' value='" + value.status + "' />" +
                        "</div>" +

                        "</div>" +
                        "<div class='row'>" +

                        "<div class='col-md-12'>" +
                        "<label>Complemento Servi�o</label>" +
                        "<input class='form-control' readonly name='complementoServicoModal' value='" + value.complementoServico + "' />" +
                        "</div>" +

                        "</div>" +
                        "<div class='row'>" +

                        "<div class='col-md-6'>" +
                        "<label>Equipe</label>" +
                        "<input class='form-control' readonly name='equipeModal' value='" + value.equipe + "' />" +
                        "</div>" +
                        "<div class='col-md-6'>" +
                        "<label>Unidade</label>" +
                        "<input class='form-control' readonly name='unidadeModal' value='" + value.unidade + "' />" +
                        "</div>" +

                        "</div>");
                });
            }

            if (tableModal != null) {
                tableModal.fnClearTable();

                if (json.osms.length) {
                    $('.ExibirSaf .divTableRelModal').show();

                    $.each(json.osms, function (key, value) {
                        tableModal.fnAddData([
                            value.osm,
                            value.ssm,
                            value.data,
                            value.status,
                            "<button class='btnEditarOsm btn btn-circle btn-success hidden-print'><i class='fa fa-eye'></i></button>"
                        ]);
                    });
                } else {
                    $('.ExibirSaf .divTableRelModal').hide();
                }
            }

            $("#loading").delay(500).fadeOut("slow");
        }
    ).always(function () {
        //atualiza o backdrop fade para se extender de acordo com a exten��o atual.
        $('.ExibirSaf').data('bs.modal').handleUpdate();
    });
}

function apiPmpCronograma(pmp, grupo) {
    $("#loading").show();

    $('.ExibirPmp input').val('');

    var arr = [pmp, grupo.substring(0, 2)];

    $.getJSON(
        caminho + "/api/returnResult/dados/getPmpCronograma",
        {dados: arr},
        function (json) {
            $('.ExibirPmp input[name="pmpModal"]').val(pmp);
            $('.ExibirPmp input[name="grupoModal"]').val(grupo);
            $('.ExibirPmp input[name="sistemaModal"]').val(json.sistema);
            $('.ExibirPmp input[name="subsistemaModal"]').val(json.subsistema);
            $('.ExibirPmp input[name="servicoModal"]').val(json.servico);
            $('.ExibirPmp input[name="procedimentoModal"]').val(json.procedimento);
            $('.ExibirPmp input[name="periodicidadeModal"]').val(json.periodicidade);
            $('.ExibirPmp input[name="linhaModal"]').val(json.linha);
            $('.ExibirPmp textarea[name="localModal"]').val(json.local);
            $('.ExibirPmp input[name="hhModal"]').val(json.hh);
            $('.ExibirPmp input[name="quinzenaModal"]').val(json.quinzena);
            $('.ExibirPmp input[name="statusModal"]').val(json.ativo);

            tableModal.fnClearTable();

            if (json.cronogramas.length) {
                $.each(json.cronogramas, function (key, value) {
                    tableModal.fnAddData([
                        value.cronograma,
                        value.quinzena,
                        value.mes,
                        value.ano,
                        value.status
                    ]);
                });
            }

            $("#loading").delay(500).fadeOut("slow");
        }
    );
}

function apiCategoriasMaterial(codCategoria, material) {
    $.getJSON(
        caminho + "/api/returnResult/dados/getCategoriasMaterial",

        {dados: codCategoria},

        function (json) {
            var options = "";
            $.each(json, function (key, value) {
                options += '<option value="' + key + '">' + value + '</option>';
            });
            material.html(options);
        }
    );
}

function apiTodosDadosMaterial(codMaterial) {

    $.getJSON(
        caminho + "/api/returnResult/dados/getTodosDadosMaterial",

        {dados: codMaterial},

        function (json) {

            //$('#modal_debug').val(JSON.stringify(json));

            var textResposta = JSON.stringify(json);
            var obj = JSON.parse(textResposta);

            $(' input[name="codMaterial"]').val(obj.cod_material);

            $(' input[name="nomeMaterial"]').val(obj.nome_material);

            $('select[name="unidadeMedida"]').val(obj.cod_uni_medida);

            $('select[name="categoria"]').val(obj.cod_categoria);

            $('select[name="marcaMaterial"]').val(obj.cod_marca);

            $(' input[name="qtdMinima"]').val(obj.quant_min);

            $(' input[name="descricao"]').val(obj.descricao);


        }
    );
}

function apiTodosDadosFornecedor(cnpj) {

    $.getJSON(
        caminho + "/api/returnResult/dados/getTodosDadosFornecedor",

        {dados: cnpj},

        function (json) {

            var textResposta = JSON.stringify(json);

            //textResposta = textResposta.replace(/,/g, '\n');

            $('#modal_debug').val(textResposta);


            var obj = JSON.parse(textResposta);

            $('input[name="hidden_codFornecedor"]').val(obj.cod_fornecedor);

            $('input[name="nomeFornecedor"]').val(obj.nome_fantasia);
            $('input[name="razaoSocial"]').val(obj.razao_social);
            $('input[name="cnpjFornecedor"]').val(obj.cnpjcpf);
            $('select[name="simplesNacionalFornecedor"]').val(obj.optante_simples);
            $('input[name="inscricaoMunicipalFornecedor"]').val(obj.insc_municipal);
            $('input[name="inscricaoEstadualFornecedor"]').val(obj.insc_estadual);


            $('input[name="cepFornecedor"]').val(obj.cep);
            $('input[name="logradouroEnderecoFornecedor"]').val(obj.logradouro);
            $('input[name="numeroEnderecoFornecedor"]').val(obj.numero_endereco);
            $('input[name="complementoEnderecoFornecedor"]').val(obj.complemento);
            $('input[name="bairroEnderecoFornecedor"]').val(obj.bairro);
            $('input[name="municipioEnderecoFornecedor"]').val(obj.municipio);
            $('input[name="ufEnderecoFornecedor"]').val(obj.uf);


            $('input[name="nomeContatoFornecedor"]').val(obj.nome_contato);
            $('input[name="telefoneContatoFornecedor"]').val(obj.fone_fornecedor);
            $('input[name="emailContatoFornecedor"]').val(obj.email_fornecedor);
            $('input[name="siteFornecedor"]').val(obj.site);


            $('input[name="agenciaBancoFornecedor"]').val(obj.agencia);
            $('input[name="ccBancoFornecedor"]').val(obj.conta_corrente);
            $('input[name="nomeBancoFornecedor"]').val(obj.banco);
            $('input[name="numeroBancoFornecedor"]').val(obj.num_transferencia);


            $('input[name="avalicaoFornecedor"]').val(obj.avaliacao);

            $('textarea[name="descricaoFornecedor"]').val(obj.descricao);



        }
    );
}

function apiAvaria (cod_avaria, callback) {
    $.getJSON(
        caminho + "/api/returnResult/dados/getAvaria",

        {dados: cod_avaria},

        function (json) {
            callback(json);
        }
    );
}

function apiLocalGrupo (cod_local_grupo, callback) {
    $.getJSON(
        caminho + "/api/returnResult/dados/getLocalGrupo",

        {dados: cod_local_grupo},

        function (json) {
            callback(json);
        }
    );
}

function apiHistoricoOdometro(dados, element){
    $.getJSON(
        caminho + "/api/returnResult/dados/getHistoricoOdometro",
        {dados: dados},
        function (json) {
            var itens = "";
            element.clear().draw();

            $.each(json, function (key, value) {
                if(value.grupo == 22){
                    element.row.add([
                        value.usuario,
                        value.odometro_ma,
                        value.odometro_mb,
                        value.data_cadastro,
                        value.tracao_ma,
                        value.gerador_ma,
                        value.tracao_mb,
                        value.gerador_mb
                    ]).draw( false );
                    // itens += ('<tr>' +
                    //     '<td>' + value['usuario'] + '</td>' +
                    //     '<td>' + value['odometro_ma'] + '</td>' +
                    //     '<td>' + value['tracao_ma'] + '</td>' +
                    //     '<td>' + value['gerador_ma'] + '</td>' +
                    //     '<td>' + value['odometro_mb'] + '</td>' +
                    //     '<td>' + value['tracao_mb'] + '</td>' +
                    //     '<td>' + value['gerador_mb'] + '</td>' +
                    //     '<td>' + value['data_cadastro'] + '</td>' +
                    //     '</tr>').toUpperCase();
                }else {
                    element.row.add([
                        value.usuario,
                        value.odometro,
                        value.data_cadastro
                    ]).draw( false );
                    // itens += ('<tr>' +
                    //     '<td>' + value['usuario'] + '</td>' +
                    //     '<td>' + value['odometro'] + '</td>' +
                    //     '<td>' + value['data_cadastro'] + '</td>' +
                    //     '</tr>').toUpperCase();
                }
            });
            // element.html(itens);
        }
    );
}

function getVeiculosPorFrota(frota, veiculoField) {
    loader();
    $.getJSON(
        caminho + "/api/returnResult/frota/getVeiculoFrota",
        {frota: frota},
        function (json) {
            var option = "";
            option += '<option selected="selected" value="">TODOS</option>';
            $.each(json, function (key, value) {
                    option += '<option value="' + value + '">' + key + '</option>';
            });
            veiculoField.html(option);

            loading(500);
        }
    );
}
