$(document).ready(function () {
    $("#resultadoPesquisaSsm").dataTable(configTable(0, false, 'asc'));
});

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaSsm"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaSsm"]'));
    } else {
        linha.val($(this).val());
        $('select[name="pnPesquisaSsm"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaSsm"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaSsm"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaSsm"]').val() == "") {
            trecho.val("*");
            apiTrechoPn(trecho, $('select[name="pnPesquisaSsm"]'));
        } else {
            $('select[name="pnPesquisaSsm"]').html('<option value="">Ponto Not�vel</option>');
        }
    } else {
        trecho.val($(this).val());
        apiTrechoPn(trecho, $('select[name="pnPesquisaSsm"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

if ($('select[name="grupoSistemaPesquisa"]').val() == '22' || $('select[name="grupoSistemaPesquisa"]').val() == '23' || $('select[name="grupoSistemaPesquisa"]').val() == '26') {
    $('.mtRod').show();
}

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistemaPesquisa"]').on('change', function () {

    var grupo = ObjetoVal;
    var selectLocalSublocal = $('#local_sublocal select');

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        $('.mtRod').show();
    } else {
        clearMtRod();
        $('.mtRod').hide();
    }

    if($(this).val() == "28" || $(this).val() == "27"){
        $('#local_sublocal').show();
        selectLocalSublocal.each(function(){
            $(this).attr("required", "required");
            $(this).val('');
        });

    }else {
        $('#local_sublocal').hide();
        selectLocalSublocal.each(function () {
            $(this).removeAttr("required");
        });
    }

    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisa"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisa"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

$('select[name="localGrupo"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocalGrupo"]'));
});

$('select[name="veiculoPesquisa"]').on('change', function () {
    var veiculo = ObjetoVal;
    if ($(this).val() == "")
        veiculo.val("*");
    else
        veiculo.val($(this).val());

    apiCarro('veiculo', veiculo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisa"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistemaPesquisa"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        } else {
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="unidadeEncaminhadaPesquisa"]').on("change", function () {
    var local = ObjetoVal;
    if ($(this).val() == "") {
        local.val("*");
    } else {
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisa"]'));
});

//############### A��es ###############
//############### A��es ###############
//############### A��es ###############

$('.btnPesquisar').on('click', function () {
    if ($('input[name="dataPartir"]').val() == '' && $('input[name="numeroSsm"]').val() == '' && $('input[name="codigoSaf"]').val() == '' && $('input[name="dataPartirAbertura"]').val() == '') {
        alertaJquery("Sugest�o", "A pesquisa solicitada ir� necessitar de alguns minutos para que seja realizada.<h4> Deseja acrescentar um intervalo de Data para facilitar a consulta?</h4>", "confirm", [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "N�o") {
                $("#pesquisa").submit();
            }
        });
    } else {
        $("#pesquisa").submit();
    }
});

$(".btnResetarPesquisa").on('click', function () {
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaSsm";
});

$(".btnEditarSsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillSsm",
        data: {codigoSsm: dados},
        success: function () {
            window.location.replace(caminho + "/dashboardGeral/ssm");
            //window.open(caminho + "/dashboardGeral/ssm", "_blank");
        }
    });
});

$(".btnViewSsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillSsm",
        data: {codigoSsm: dados},
        success: function () {
            window.open(caminho + "/dashboardGeral/viewSsm", "_blank");
        }
    });
});

$(".btnImprimirSsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/imprimirSsm",
        data: {codigoSsm: dados},
        success: function () {
            window.open(caminho + "/dashboardGeral/printSsm", "_blank");
        }
    });
});

$(".btnAbrirPesquisaSaf").on("click", function () {
    var codigoSaf = $(this).parent("td").parent("tr").find('td').next('td').html();
    window.location.href = caminho + "/dashboardGeral/pesquisaSsmParaSaf/" + codigoSaf;
    // window.open(caminho + "/dashboardGeral/pesquisaSsmParaSaf/" + codigoSaf, "_blank");
});

$(".btnAbrirPesquisaOsm").on("click", function () {
    var codigoSsm = $(this).parent("td").parent("tr").find('td').html();
    window.location.href = caminho + "/dashboardGeral/pesquisaSsmParaOsm/" + codigoSsm;
    // window.open(caminho + "/dashboardGeral/pesquisaSsmParaOsm/" + codigoSsm, "_blank");
});

function clearMtRod() {
    $('select[name="veiculoPesquisa"]').val('');
    $('select[name="carroAvariadoPesquisa"]').val('');
    $('select[name="carroLiderPesquisa"]').val('');
    $('select[name="prefixoPesquisa"]').val('');
    $('input[name="odometroPartirPesquisa"]').val('');
    $('input[name="odometroAtePesquisa"]').val('');
}