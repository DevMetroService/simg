$(document).ready(function(){
    // Configura a Tabela de  Usuarios Cadastrados
    $("#amvTable").DataTable(configTable(0, false, 'asc'));

    var linha = $('#linha');

    var form = $("#formulario");

    $(document).on("click",".editLinha", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/linha/update/'+$(this).data('codigo'));

        var row = $(this).parent("td").parent("tr").find('td');

        linha.val(row.next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/linha/store');
    });

});