$(document).ready(function(){
    // Configura a Tabela de  Usuarios Cadastrados
    $("#amvTable").DataTable(configTable(0, false, 'asc'));

    var grupo = $('#grupo');
    var procedimento = $('#procedimento');

    var form = $("#formulario");

    $(document).on("click",".editProcedimento", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/procedimento/update/'+$(this).data('codigo'));

        var row = $(this).parent("td").parent("tr").find('td').next();

        grupo.val(row.data('value'));

        procedimento.val(row.next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/procedimento/store');
    });

});