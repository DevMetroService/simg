/**
 * Created by josue.marques on 03/06/2016.
 */
//################################# Script para M�dulo Registro Execu��o ############
//################################# Script para M�dulo Registro Execu��o ############
//################################# Script para M�dulo Registro Execu��o ############


// ############# Unidade - Equipe
$('select[name="unidadeExecucao"]').on("change", function(){
    $('input[name="codUnidadeExecucao"]').val($(this).val());
    apiLocalEquipe($(this), $('select[name="equipeExecucao"]'));
});

$('select[name="equipeExecucao"]').on("change", function(){
    $('input[name="codEquipeExecucao"]').val($(this).val());
});


//############## Descri��o da Causa DataList
$('input[name="causaAvariaInput"]').on("input",function() {
    dataListFuncao($('input[name="causaAvariaInput"]'), $('#causaAvariaDataList'), $('input[name="causaAvaria"]'));
});

$('input[name="causaAvariaInput"]').on("blur",function() {
    if($('input[name="causaAvaria"]').val() == ""){
        $(this).val("");
    }else{
        $('input[name="causaAvariaInput"]').val($("#causaAvariaDataList").find("option[value='" + $('input[name="causaAvaria"]').val() + "']").text());
    }
});

if($('input[name="causaAvaria"]').val() > 0){
    $('input[name="causaAvariaInput"]').val($("#causaAvariaDataList").find("option[value='" + $('input[name="causaAvaria"]').val() + "']").text());
}


//############## Descri��o da Atua��o DataList
$('input[name="atuacaoExecucaoInput"]').on("input",function() {
    dataListFuncao($('input[name="atuacaoExecucaoInput"]'), $('#atuacaoExecucaoDataList'), $('input[name="atuacaoExecucao"]'));
});

$('input[name="atuacaoExecucaoInput"]').on("blur",function() {
    if($('input[name="atuacaoExecucao"]').val() == ""){
        $(this).val("");
    }else{
        $('input[name="atuacaoExecucaoInput"]').val($("#atuacaoExecucaoDataList").find("option[value='" + $('input[name="atuacaoExecucao"]').val() + "']").text());
    }
});

if($('input[name="atuacaoExecucao"]').val() > 0){
    $('input[name="atuacaoExecucaoInput"]').val($("#atuacaoExecucaoDataList").find("option[value='" + $('input[name="atuacaoExecucao"]').val() + "']").text());
}


// ############ Agente Causador
$('select[name="agCausador"]').on("change", function(){
    $('input[name="codAgCausador"]').val($('select[name="agCausador"] option:selected').val());
});

// ############# Cautela
$('input[name="cautelaExecucao"]').on("change", function(){
    
    $('#cautelaRegistro').toggle();

    $('input[name="kmIniCautela"]').val("");
    $('input[name="kmFimCautela"]').val("");
    $('input[name="restringirVelocidadeExecucao"]').val("");
});

if($('input[name="cautelaExecucao"]:checked').length){
    $('#cautelaRegistro').show();
}else{
    $('#cautelaRegistro').hide();
}

$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#osRegistroExecucao input'), $('#osRegistroExecucao select'), $('#osRegistroExecucao textarea'));

    if(resultado['permissao']){
        $('#osRegistroExecucao').submit();
    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});