$(document).ready(function(){
    // Configura a Tabela de  Usuarios Cadastrados
    $("#amvTable").DataTable(configTable(0, false, 'asc'));

    var grupo = $('#grupo');
    var sistema = $('#sistema');
    var subsistema = $('#subsistema');
    var servico = $('#servico');

    var form = $("#formulario");

    $(document).on("click",".editServico", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        var row = $(this).parent("td").parent("tr").find('td');
        
        form.attr('action', caminho + '/servicoPmp/update/'+$(this).data('codigo')+'/'+row.data('value'));

        grupo.val(row.data('value'));

        apiGrupoSistema(grupo, sistema, row.next().next().data('value'));
        apiSistemaSub(sistema, subsistema, row.next().next().next().data('value'));

        servico.val(row.next().next().next().next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    grupo.change(function(){
        apiGrupoSistema(grupo, sistema);
    });

    sistema.change(function() {
        apiSistemaSub(sistema, subsistema);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/servicoPmp/store');
        sistema.html("<option value=''>Selecione linha para filtrar</option>");
        subsistema.html("<option value=''>Selecione linha para filtrar</option>");
    });

});