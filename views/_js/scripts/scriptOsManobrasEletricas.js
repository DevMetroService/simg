/**
 * Created by josue.marques on 03/06/2016.
 */
//################################# Script para Manobras El�tricas ####################
//################################# Script para Manobras El�tricas ####################
//################################# Script para Manobras El�tricas ####################

//Prenchimento dos campos de Pedido de Manobra
$('input[name="pm"]').on("blur", function(){
    $('input[name="pmLigamento"]').val($(this).val());
});

//Prenchimento dos campos de nome do Solicitante Desligamento
// ########### AutoComplete DataList Nome Matricula
$('input[name="solicitadoDesligamento"]').on("blur", function() {
    apiMatriculaNomeFuncionario($('input[name="solicitadoDesligamento"]'), $("input[name='nomeSolicitadoDesligamento']"));
});

$('input[name="nomeSolicitadoDesligamento"]').on("input",function() {
    $("#nomeSolicitanteDataList").find("option").each(function() {
        if ($(this).val() == $('input[name="nomeSolicitadoDesligamento"]').val()) {
            $('input[name="solicitadoDesligamento"]').val($(this).val());

            apiMatriculaNomeFuncionario($('input[name="solicitadoDesligamento"]'), $("input[name='nomeSolicitadoDesligamento']"));
        }
    });
});

$('input[name="nomeSolicitadoDesligamento"]').on("blur",function() {
    if($('input[name="solicitadoDesligamento"]').val() == ""){
        $(this).val("");
    }else{
        $('input[name="nomeSolicitadoDesligamento"]').val($("#nomeSolicitanteDataList").find("option[value='" + $('input[name="solicitadoDesligamento"]').val() + "']").text());
    }
});

//Prenchimento dos campos de nome do Solicitante Ligamento
// ########### AutoComplete DataList Nome Matricula
$('input[name="solicitadoLigamento"]').on("blur", function() {
    apiMatriculaNomeFuncionario($('input[name="solicitadoLigamento"]'), $("input[name='nomeSolicitadoLigamento']"));
});

$('input[name="nomeSolicitadoLigamento"]').on("input",function() {
    $("#nomeSolicitanteDataList").find("option").each(function() {
        if ($(this).val() == $('input[name="nomeSolicitadoLigamento"]').val()) {
            $('input[name="solicitadoLigamento"]').val($(this).val());

            apiMatriculaNomeFuncionario($('input[name="solicitadoLigamento"]'), $("input[name='nomeSolicitadoLigamento']"));
        }
    });
});

$('input[name="nomeSolicitadoLigamento"]').on("blur",function() {
    if($('input[name="solicitadoLigamento"]').val() == ""){
        $(this).val("");
    }else{
        $('input[name="nomeSolicitadoLigamento"]').val($("#nomeSolicitanteDataList").find("option[value='" + $('input[name="solicitadoLigamento"]').val() + "']").text());
    }
});


var excluidosManobras = new Array();

var contadorManobras = 0;

if ($('input[name="contadorManobra"]').val() >= 1) {
    contadorManobras = Number($('input[name="contadorManobra"]').val());
}


//Fun��o que adiciona funcionario na lista
$("#addManobraEletrica").on("click", function (e) {		//Fun��o para adicionar a div de benefici�rios
    e.preventDefault();

    var local                   = $('input[name="localManobra"]').val();
    var chave                   = $('input[name="chaveManobra"]').val();
    var dataDeslig              = $('input[name="dataDesligamento"]').val();
    var horaDeslig              = $('input[name="horaDesligamento"]').val();
    var pm                      = $('input[name="pm"]').val();
    var solicitadoDeslig        = $('input[name="solicitadoDesligamento"]').val();
    var nomeSolicitadoDeslig    = $('input[name="nomeSolicitadoDesligamento"]').val();
    var atendidoDeslig          = $('input[name="atendidoDesligamento"]').val();
    var nomeAtendidoDeslig      = $('input[name="nomeAtendidoDesligamento"]').val();
    var dataLig                 = $('input[name="dataLigamento"]').val();
    var horaLig                 = $('input[name="horaLigamento"]').val();
    var pmLig                   = $('input[name="pmLigamento"]').val();
    var solicitadoLig           = $('input[name="solicitadoLigamento"]').val();
    var nomeSolicitadoLig       = $('input[name="nomeSolicitadoLigamento"]').val();
    var atendidoLig             = $('input[name="atendidoLigamento"]').val();
    var nomeAtendidoLig         = $('input[name="nomeAtendidoLigamento"]').val();

    var mensagem ="";
    if(local == ""){
        mensagem += "Local � obrigat�ria\n";
    }
    if(chave == ""){
        mensagem += "Chave/Disjuntor � obrigat�rio\n";
    }
    if(dataDeslig == ""){
        mensagem += "Data de Desligamento � obrigat�rio\n";
    }
    if(horaDeslig == ""){
        mensagem += "Hora de Desligamento de s�rie � obrigat�rio\n";
    }
    if(pm == ""){
        mensagem += "Pedido de Manobra � obrigat�rio\n";
    }
    if(solicitadoDeslig == ""){
        mensagem += "Matricula do Solicitante de desligamento � obrigat�rio\n";
    }
    if(atendidoDeslig == ""){
        mensagem += "Matricula do Atendente de desligamento � obrigat�rio\n";
    }
    if(nomeAtendidoDeslig == ""){
        mensagem += "Nome do Atendente de desligamento � obrigat�rio\n";
    }
    if(dataLig == ""){
        mensagem += "Data de Ligamento � obrigat�rio\n";
    }
    if(horaLig == ""){
        mensagem += "Hora de Ligamento � obrigat�rio\n";
    }
    if(solicitadoLig == ""){
        mensagem += "Matricula do Solicitante de ligamento � obrigat�rio\n";
    }
    if(atendidoLig == ""){
        mensagem += "Matricula do Atendente de ligamento � obrigat�rio\n";
    }
    if(nomeAtendidoLig == ""){
        mensagem += "Nome do Atendente de ligamento � obrigat�rio\n";
    }

    if(mensagem == "") {
        //Zerando os valores
        $('input[name="localManobra"]').val("");
        $('input[name="chaveManobra"]').val("");
        /*
         $('input[name="dataDesligamento"]').val("");
         $('input[name="horaDesligamento"]').val("");
         $('input[name="pm"]').val("");
         $('input[name="solicitadoDesligamento"]').val("");
         $('input[name="nomeSolicitadoDesligamento"]').val("");
         $('input[name="atendidoDesligamento"]').val("");
         $('input[name="nomeAtendidoDesligamento"]').val("");
         $('input[name="dataLigamento"]').val("");
         $('input[name="horaLigamento"]').val("");
         $('input[name="pmLigamento"]').val("");
         $('input[name="solicitadoLigamento"]').val("");
         $('input[name="nomeSolicitadoLigamento"]').val("");
         $('input[name="atendidoLigamento"]').val("");
         $('input[name="nomeAtendidoLigamento"]').val("");
         */
        var disponivel = excluidosManobras.pop();

        if (contadorManobras < 20 && disponivel == undefined) {
            contadorManobras = contadorManobras + 1;
            $("#listaManobras").append('<div style="border-bottom: 1px solid #eee; padding-bottom: 10px; margin: 2px 0px;">' +
                '<div class="row" style="padding-top: 1em;">' +
                '<div class="col-md-2"><label>Local</label><input disabled type="text" class="form-control" value="'+local+'"></div>'+
                '<div class="col-md-1"><label>Chave/Disj.</label><input disabled type="text" class="form-control" value="'+chave+'"></div>'+
                '<div class="col-md-2"><label>Data/Hora Desligamento</label><input disabled type="text" class="form-control" value="'+dataDeslig +' '+ horaDeslig+'"></div>'+
                '<div class="col-md-1"><label>P.M. Manobras</label><input disabled type="text" class="form-control" value="'+pm+'"></div>'+
                '<div class="col-md-3"><label>Solicitado por</label><input disabled type="text" class="form-control" value="'+solicitadoDeslig+' - '+nomeSolicitadoDeslig+'"></div>'+
                '<div class="col-md-3"><label>Atendido por</label><input disabled type="text" class="form-control" value="'+atendidoDeslig+' - '+nomeAtendidoDeslig+'"></div>'+
                '</div>' +
                '<input type="hidden" value="'+local+'" name="localManobra'+contadorManobras+'">'+
                '<input type="hidden" value="'+chave+'" name="chaveManobra'+contadorManobras+'">'+
                '<input type="hidden" value="'+pm+'" name="pm'+contadorManobras+'">'+
                '<input type="hidden" value="'+dataDeslig+'" name="dataDesligamento'+contadorManobras+'">'+
                '<input type="hidden" value="'+horaDeslig+'" name="horaDesligamento'+contadorManobras+'">'+
                '<input type="hidden" value="'+solicitadoDeslig+'" name="solicitadoDesligamento'+contadorManobras+'">'+
                '<input type="hidden" value="'+atendidoDeslig+' - '+nomeAtendidoDeslig+'" name="atendidoDesligamento'+contadorManobras+'">'+
                '<div class="row">'+
                '<div class="col-md-2"><label>Data/Hora Ligamento</label><input disabled type="text" class="form-control" value="'+dataLig+' '+horaLig+'"></div>'+
                '<div class="col-md-2"><label>P.M. Manobras</label><input disabled type="text" class="form-control" value="'+pmLig+'"></div>'+
                '<div class="col-md-3"><label>Solicitado por</label><input disabled type="text" class="form-control" value="'+solicitadoLig+' - '+nomeSolicitadoLig+'"></div>'+
                '<div class="col-md-3"><label>Atendido por</label><input disabled type="text" class="form-control" value="'+atendidoLig+' - '+nomeAtendidoLig+'"></div>'+
                '<div style="padding-top: 1.5em"><button id="excluirManobra" class="btn btn-danger btn-circle" value="' + contadorManobras + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
                '</div>'+
                '<input type="hidden" value="'+dataLig+'" name="dataLigamento'+contadorManobras+'">'+
                '<input type="hidden" value="'+horaLig+'" name="horaLigamento'+contadorManobras+'">'+
                '<input type="hidden" value="'+solicitadoLig+'" name="solicitadoLigamento'+contadorManobras+'">'+
                '<input type="hidden" value="'+atendidoLig+' - '+nomeAtendidoLig+'" name="atendidoLigamento'+contadorManobras+'">'+
                '</div>'
            );
        } else {
            if(disponivel != undefined){
                contadorManobras = contadorManobras + 1;
                $("#listaManobras").append('<div style="border-bottom: 2px solid #eee; padding-bottom: 10px; margin: 2px 0px;">' +
                    '<div class="row" style="padding-top: 1em;">' +
                    '<div class="col-md-2"><label>Local</label><input disabled type="text" class="form-control" value="'+local+'"></div>'+
                    '<div class="col-md-1"><label>Chave/Disj.</label><input disabled type="text" class="form-control" value="'+chave+'"></div>'+
                    '<div class="col-md-2"><label>Data/Hora Desligamento</label><input disabled type="text" class="form-control" value="'+dataDeslig +' '+ horaDeslig+'"></div>'+
                    '<div class="col-md-1"><label>P.M. Manobras</label><input disabled type="text" class="form-control" value="'+pm+'"></div>'+
                    '<div class="col-md-3"><label>Solicitado por</label><input disabled type="text" class="form-control" value="'+solicitadoDeslig+' - '+nomeSolicitadoDeslig+'"></div>'+
                    '<div class="col-md-3"><label>Atendido por</label><input disabled type="text" class="form-control" value="'+atendidoDeslig+' - '+nomeAtendidoDeslig+'"></div>'+
                    '</div>' +
                    '<input type="hidden" value="'+local+'" name="localManobra'+disponivel+'">'+
                    '<input type="hidden" value="'+chave+'" name="chaveManobra'+disponivel+'">'+
                    '<input type="hidden" value="'+pm+'" name="pm'+disponivel+'">'+
                    '<input type="hidden" value="'+dataDeslig+'" name="dataDesligamento'+disponivel+'">'+
                    '<input type="hidden" value="'+horaDeslig+'" name="horaDesligamento'+disponivel+'">'+
                    '<input type="hidden" value="'+solicitadoDeslig+'" name="solicitadoDesligamento'+disponivel+'">'+
                    '<input type="hidden" value="'+atendidoDeslig+' - '+nomeAtendidoDeslig+'" name="atendidoDesligamento'+disponivel+'">'+
                    '<div class="row">'+
                    '<div class="col-md-2"><label>Data/Hora Ligamento</label><input disabled type="text" class="form-control" value="'+dataLig+' '+horaLig+'"></div>'+
                    '<div class="col-md-2"><label>P.M. Manobras</label><input disabled type="text" class="form-control" value="'+pmLig+'"></div>'+
                    '<div class="col-md-3"><label>Solicitado por</label><input disabled type="text" class="form-control" value="'+solicitadoLig+' - '+nomeSolicitadoLig+'"></div>'+
                    '<div class="col-md-3"><label>Atendido por</label><input disabled type="text" class="form-control" value="'+atendidoLig+' - '+nomeAtendidoLig+'"></div>'+
                    '<div style="padding-top: 1.5em"><button id="excluirManobra" class="btn btn-danger btn-circle" value="' + contadorManobras + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
                    '</div>' +
                    '<input type="hidden" value="'+dataLig+'" name="dataLigamento'+disponivel+'">'+
                    '<input type="hidden" value="'+horaLig+'" name="horaLigamento'+disponivel+'">'+
                    '<input type="hidden" value="'+solicitadoDeslig+'" name="solicitadoLigamento'+disponivel+'">'+
                    '<input type="hidden" value="'+atendidoDeslig+' - '+nomeAtendidoDeslig+'" name="atendidoLigamento'+disponivel+'">'+
                    '</div>'
                );
            }else{
                alertaJquery("Alerta", "Limite m�ximo alcan�ado. \nCaso necess�rio entre em contato com a TI", "alert");
            }
        }
    }else{
        alertaJquery("Alerta", mensagem, "alert");
    }

});

$("#listaManobras").on("click", "#excluirManobra", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();
    $(this).parent('div').parent('div').parent('div').remove();
    excluidosManobras.push($(this).val());
    contadorManobras = contadorManobras - 1;
});

$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#osManobrasEletricas input'), $('#osManobrasEletricas select'), $('#osManobrasEletricas textarea'));

    if(resultado['permissao']){
        $('#osManobrasEletricas').submit();
    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});