/**
 * Created by josue.marques on 24/02/2016.
 *
 */
var dataListSolicitante = $('#nomeSolicitanteDataList');
var tipoFuncionario = $('select[name="tipo_funcionario"]');
var nomeSolicitante =  $('input[name="nomeSolicitante"]');
var contatoSolicitante = $("input[name='contatoSolicitante']");
var matriculaSolicitante = $('input[name="matriculaSolicitante"]');

 //Origem de solicitante
 tipoFuncionario.on("change", function () {						//Din�mica para visualizar campos de entrada para Ensino Superior e/ou Curso T�cnico.
     //Atualiza datalist segundo origem solicitante
     var cod_origem = $(this).val();

     $.getJSON(
         caminho + "/api/returnResult/dados/getFuncionariosByTipo",
         {dados: cod_origem},
         function (json) {
             var option = "";
             $.each(json, function (key, value) {
                 var funcionario = value;
                 option += ('<option id="' + funcionario.matricula + '" value="' + funcionario.nome_funcionario + ' - ' + funcionario.matricula + '"/>').toUpperCase();
             });
             dataListSolicitante.html(option);
         }
     );

     nomeSolicitante.val('');
     contatoSolicitante.val('');
     matriculaSolicitante.val('');

 });

 //Consulta de matr�cula de funcionario.
 matriculaSolicitante.on("blur", function () {
     apiMatriculaFuncionario(matriculaSolicitante, $("input[name='nomeSolicitante']")
         , $("input[name='cpfSolicitante']"), contatoSolicitante, tipoFuncionario);
 });

 //AutoComplete nome solicitante
 nomeSolicitante.on("input", function () {
     var valueFinal = dataListSolicitante.find("option[value='" + nomeSolicitante.val() + "']");
     if (valueFinal.val() != null) {

         var matSolicitante = matriculaSolicitante;
         matSolicitante.val(valueFinal.attr('id'));

         apiMatriculaFuncionario(matriculaSolicitante, $("input[name='nomeSolicitante']")
                     , $("input[name='cpfSolicitante']"), contatoSolicitante, tipoFuncionario);
     }
 });


if ($('select[name="origemProgramacao"]').val() == '2') {
    $('#origemServico').show();
    $('input[name="numeroSsmPendente"]').attr("required", "required");
}

$('select[name="origemProgramacao"]').on("change", function () {
    if ($(this).val() == '2') {
        $('#origemServico').show();
        $('input[name="numeroSsmPendente"]').attr("required", "required");
    } else {
        $('#origemServico').hide();
        $('input[name="numeroSsmPendente"]').removeAttr("required");
    }
});

$('input[name="numeroSsmPendente"]').on('blur', function () {
    var input = $(this);
    if (input.val() != "") {
        $.getJSON(
            caminho + "/api/returnResult/ssm/ssmPendente",
            {ssm: input.val()},
            function (json) {
                if (json.validade) {
                } else {
                    alertaJquery("Alerta", "SSM n�o est� Pendente", "alert");
                    input.val("");
                }
            }
        );
    }
});

//######### Linha / Trecho / PontoNotavel
//Auto preenchimento do trecho e via ap�s sele��o da linha
$('select[name="linhaSsp"]').on("change", function () {
    apiLinhaTrecho($('select[name="linhaSsp"]'), $('select[name="trechoSsp"]'));
    apiLinhaVia($('select[name="linhaSsp"]'), $('select[name="viaSsp"]'));

    $('select[name="pontoNotavelSsp"]').html('<option disabled="disabled" selected="selected" value="">Pontos Not�veis</option>');

    var grupo = $('select[name="grupoSistemaSsp"]').val();

    if (grupo == "22" || grupo == "23" || grupo == "26") {
        apiGetVeiculo(grupo, $('select[name="veiculoMrSsp"]'), $(this).val());
    }
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoSsp"]').on("change", function () {
    apiTrechoPn($('select[name="trechoSsp"]'), $('select[name="pontoNotavelSsp"]'))
});

//######### Grupo / Sistema / SubSistema
//Auto preenchimento do sistema ap�s sele��o do grupo sistema
$('select[name="grupoSistemaSsp"]').on("change", function () {

    var divMr = $('#divMaterialRodante');
    var selectMr = $('select[name="veiculoMrSsp"]');

    if( $(this).val() == "22" || $(this).val() == "23" || $(this).val() == "26"){
        divMr.show();

        selectMr.attr('required', 'required');
        $('select[name="prefixoMrSsp"]').removeAttr("required");

        $('select[name="carroMrSsp"]').html('');

        var linha = $('select[name="linhaSsp"]').val();

        apiGetVeiculo($(this).val(), $('select[name="veiculoMrSsp"]'), linha );
    }else{
        divMr.hide();
        selectMr.removeAttr('required');
    }

    $('input[name="numeroGrupoSistemaSsp"]').val($(this).val());
    $('input[name="numeroSistemaSsp"]').val('');
    $('input[name="codSubSistema"]').val('');

    apiGrupoSistema($(this), $('select[name="sistemaSsp"]'));
    $('select[name="subSistemaSsp"]').html('<option disabled="disabled" selected="selected">Selecione o Sub-Sistema</option>');

});

$('select[name="veiculoMrSsp"]').change(function () {

    apiCarro('veiculo', $(this).val(), $('select[name="carroMrSsp"]'));

});

//Auto preenchimento do subsistema ap�s sele��o do sistema
$('select[name="sistemaSsp"]').on("change", function () {
    $('input[name="numeroSistemaSsp"]').val($(this).val());
    apiSistemaSub($(this), $('select[name="subSistemaSsp"]'));
});

//Auto preenchimento de sigla do SISTEMA
$('select[name="subSistemaSsp"]').on("change", function () {
    $('input[name="codSubSistema"]').val($(this).val());
});

//############## Servi�o DataList
$('input[name="servicoSspInput"]').on("input", function () {
    dataListFuncao($('input[name="servicoSspInput"]'), $('#servicoSspDataList'), $('input[name="servicoSsp"]'));
});

$('input[name="servicoSspInput"]').on("blur", function () {
    if ($('input[name="servicoSsp"]').val() == "") {
        $(this).val("");
        alertaJquery("Alerta", 'Clique em um servi�o', "alert");
    } else {
        $('input[name="servicoSspInput"]').val($("#servicoSspDataList").find("option[value='" + $('input[name="servicoSsp"]').val() + "']").text());
    }
});

//############### Encaminhamento
$('select[name="localSsp"]').on("change", function () {
    $('input[name="numeroLocalSsp"]').val($(this).val());
    apiLocalEquipe($(this), $('select[name="equipeSsp"]'));
});

$('select[name="equipeSsp"]').on("change", function () {
    $('input[name="siglaEquipeSsp"]').val($(this).val());
});

//Intera��o para MODAL de devolu��o e cancelamento
$('#motivoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var acao = button.data('action');

    $('input[name="acaoSsp"]').val(acao);
});

$(".btnImprimirFormulario").on("click", function () {
    var codigoSsp = $('input[name="codigoSsp"]').val();
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/imprimirSsp",
        data: {codigoSsp: codigoSsp},
        success: function () {
            window.open(caminho + "/dashboardGeral/printSsp", "_blank");
        }
    });
});

//Fun��o do bot�o de gerarOsm
$(".gerarOsp").on('click', function (e) {
    if (hasProcess(e)) return;

    var tempo = $("input[name='dataHoraSsp']");

    if ((!validarHoraProgramada(tempo.val()) && $('input[name="statusSsp"]').length && $('input[name="statusSsp"]').val() != "Pendente")) {
        alertaJquery("Alerta", "Data programada n�o pode ser anterior a data atual.", "alert");
        tempo.val("");
        $('body').removeClass('processing');
        return false;
    }


    var resultado = validacao_formulario($('#formSsp input'), $('#formSsp select'), $('#formSsp textarea'));

    if (resultado['permissao']) {
        alertaJquery('Alerta', 'Deseja realmente gerar uma OSP?', 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "Sim") {
                $("#formSsp").attr("action", caminho + "/cadastroGeral/gerarOsp");
                $('#formSsp').submit();
            }
            if (res == "N�o") {
                $('body').removeClass('processing');
            }
        });
        // if (confirm('Deseja realmente gerar uma OSP?')) {
        //     $("#formSsp").attr("action", caminho + "/cadastroGeral/gerarOsp");
        //     $('#formSsp').submit();
        // }
        // else {
        //     $('body').removeClass('processing');
        // }
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

//Fun��o do bot�o de Salvar
$(".salvarSsp").on('click', function (e) {
    if (hasProcess(e)) return;

/*    var tempo = $("input[name='dataHoraSsp']");

    if ((!validarHoraProgramada(tempo.val()) && $('input[name="statusSsp"]').length && $('input[name="statusSsp"]').val() != "Pendente")) {
        alertaJquery('Erro em Data', "Data programada n�o pode ser anterior a data atual.", 'alert');
        tempo.val("");
        return false;
    }
*/
    var resultado = validacao_formulario($('#formSsp input'), $('#formSsp select'), $('#formSsp textarea'));

    if (resultado['permissao']) {
        $('#formSsp').submit();
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

$("input[name='dataHoraSsp']").on('blur', function () {
    var tempo = $(this).val();
    if (!validarHoraProgramada(tempo)) {
        // alert('teste');
        alertaJquery('Erro em Data', 'Data programada n�o pode ser anterior a data atual.', 'alert');
        // alert("Data programada n�o pode ser anterior a data atual.");
        $(this).val("");
    }

});
