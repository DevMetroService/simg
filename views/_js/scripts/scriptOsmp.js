/**
 * Created by iramar.junior on 12/12/2016.
 */


$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#osDadosGerais input'), $('#osDadosGerais select'), $('#osDadosGerais textarea'));


    if(resultado['permissao']){
        $('#osDadosGerais').submit();

    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);
    console.log(isElementInView);
    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});

$(".btnImprimirFormulario").on("click", function () {
    var codigoOs = $('input[name="codigoOs"]').val();
    var tipo = $(this).data('form');

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/imprimir"+tipo,
        data: {codigoOs: codigoOs},
        success: function () {
            window.open(caminho + "/dashboardGeral/print"+tipo, "_blank");
        }
    });
});