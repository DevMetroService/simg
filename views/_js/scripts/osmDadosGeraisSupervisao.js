//################################# Script para OSM ###################################
//################################# Script para OSM ###################################
//################################# Script para OSM ###################################
$('document').ready(function(){
    $('input').prop('disabled', true);
    $('select').prop('disabled', true);
    $('textarea').prop('disabled', true);
});

var url = window.location.href;
var splitUrl = url.split("/");

var caminho = "http://"+splitUrl[2];

//Dica do motivo do bot�o de encerramento n�o estar funcionando
$('[rel="tooltip-wrapper"]').tooltip({position: "bottom"});

//Auto preenchimento do c�digo de grupo de sistema atuado

if($('select[name="grupoOsmAtuado"]').length){
    var sigla = $('select[name="grupoOsmAtuado"]').val();
    $('input[name="codGrupoOsmAtuado"]').val(sigla);
}

//Auto preenchimento do c�digo do sistema atuado

if($('select[name="sistemaOsmAtuado"]').length){
    var sigla = $('select[name="sistemaOsmAtuado"]').val();
    $('input[name="codSistemaOsmAtuado"]').val(sigla);
}

//Auto preenchimento do c�dgo do servi�o a ser executado

if($('select[name="servicoExecutadoOsm"]').length){
    var sigla = $('select[name="servicoExecutadoOsm"]').val();
    $('input[name="codigoServico"]').val(sigla);
}

//Auto sele��o do trecho a partir da linha selecionada

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.