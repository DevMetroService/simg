/**
 * Created by josue.santos on 12/07/2017.
 */

// MultiSelect
function multiselectCarro(i) {
    $('select[name="carro' + i + '[]"]').multiselect({
        enableClickableOptGroups: false,
        enableCollapsibleOptGroups: false,

        buttonContainer: '<div></div>',

        includeSelectAllOption: false,
        maxHeight: 350,
        nonSelectedText: "Selecione os Carros",
        nSelectedText: ' - op��es selecionadas',
        allSelectedText: 'Todas selecionadas',
        onChange: function(option, checked, select) {
            if(checked){
                verificarCarro($('select[name="veiculo-' + i + '"] option:selected'), option.val(), $('select[name="carro' + i + '[]"]'));
            }
        }
    });
}

$('document').ready(function () {
    $("#indicadorCarroVLT").dataTable(configTable(0, false, 'asc'));
    $("#indicadorCarroTUE").dataTable(configTable(0, false, 'asc'));
    $("#indicadorCarroLocomotiva").dataTable(configTable(0, false, 'asc'));

    multiselectCarro(1);
});

if($('select[name="tipoComposicao"]').val() != ''){
    $('#divComposicao').show();
} else {
    $('#divComposicao').hide();
    $('.addVeiculoComposicao').hide();
}

$('select[name="tipoComposicao"]').change(function () {
    apiGetVeiculo($(this).val(), $('select[name="veiculo-1"]'));
    $('select[name="carro1[]"]').multiselect('dataprovider', []);

    if($(this).val() != ''){
       $('#divComposicao').show();
        apiGetComposicaoPorTipo($(this), $('select[name="composicao"]'));
    } else {
        $('#divComposicao').hide();
        $('.addVeiculoComposicao').hide();
        $('select[name="composicao"]').html('');
    }

    $('.divAddComp').remove();
    $('.divComp2').remove();

    $('.addVeiculoComposicao').val(1);
    $('input[name="quantVeiculoComposicao"]').val(1);
});

if($('select[name="composicao"]').val() != ''){
    $('.addVeiculoComposicao').show();
} else {
    $('.addVeiculoComposicao').hide();
}

$('select[name="composicao"]').change(function () {
    $('.divAddComp').remove();
    $('.divComp2').remove();

    $('.addVeiculoComposicao').val(1);
    $('input[name="quantVeiculoComposicao"]').val(1);

    if($(this).val() != ''){
        $('.addVeiculoComposicao').show();
        apiComposicaoConfigForm($(this).val(), $('select[name="veiculo-1"]'), $('.addVeiculoComposicao'));
    } else {
        $('select[name="veiculo-1"]').val('');
        $('select[name="veiculo-1"]').change();

        $('.addVeiculoComposicao').hide();
    }
});

$(document).on( 'change', 'select[name|="veiculo"]', function () {
    var i = $(this).attr('name').split("-")[1];

    if($(this).val() != ''){
        apiCarroMultiSelect($('select[name="tipoComposicao"]').val(), $(this).val(), $('select[name="carro' + i + '[]"]'));
    } else {
        $('select[name="carro' + i + '[]"]').multiselect('dataprovider', []);
    }
});

$('.addComposicao').click(function () {
    var grupo = $('select[name="tipoComposicao"] option:selected');
    alertaJquery("Necess�rio Confirma��o", 'Deseja cadastrar uma nova Composi��o <strong>' + grupo.text() + '</strong>?', "confirm", [{value: "Sim"}, {value: "N�o"}], function (res) {
        if (res == "Sim") {
            window.location.href = caminho + "/cadastroGeral/cadastrarComposicao/" + grupo.val();
        }
    });
});

$('.addVeiculoComposicao').click(function (event, select) {
    var i = parseInt($(this).val()) + 1;

    var html = '<div class="row divAddComp">'+
                    '<div class="col-md-1">'+
                        '<button value="' + i + '" class="btn btn-danger deletarVeiculoComposicao" style="margin-top: 50%">' +
                            '<i class="fa fa-times fa-1x"></i>'+
                        '</button>'+
                    '</div>'+
                    '<div class="col-md-5">'+
                        '<label>Ve�culo</label>'+
                        '<select name="veiculo-' + i + '" class="form-control"></select>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                        '<label>Carro</label>'+
                        '<select name="carro' + i + '[]" class="form-control" multiple="multiple"></select>'+
                    '</div>'+
                '</div>'+
                '<div class="divComp' + i + '"></div>';

    $('.divComp' + $(this).val() ).html(html);
    multiselectCarro(i);
    apiGetVeiculo($('select[name="tipoComposicao"]').val(), $('select[name="veiculo-' + i + '"]'), select);

    $(this).val(i);
    $('input[name="quantVeiculoComposicao"]').val(i);
});

$(document).on( 'click', '.deletarVeiculoComposicao', function () {
    $(this).parent("div").parent("div").remove();
});