/**
 * Created by josue.marques on 03/06/2016.
 */
//################################# Script para M�dulo M�quinas Utilizados ############
//################################# Script para M�dulo M�quinas Utilizados ############
//################################# Script para M�dulo M�quinas Utilizados ############

var excluidosMaquinas = new Array();

var contadorMaquinas = 0;

if ($('input[name="contadorMaquina"]').val() >= 1) {
    contadorMaquinas = Number($('input[name="contadorMaquina"]').val());
}


//Fun��o que adiciona funcionario na lista
$("#addMaquinaOs").on("click", function (e) {		//Fun��o para adicionar a div de benefici�rios
    e.preventDefault();

    var codigoMaquina   = $('input[name="nomeMaquina"]').val();
    var nome            = $('input[name="nomeMaquinaOs"]').val();
    var descricao       = $('input[name="descricaoMaquina"]').val();
    var qtd             = $('input[name="qtdMaquina"]').val();
    var numSerie        = $('input[name="numeroSerieMaquina"]').val();
    var totalHoras      = $('input[name="totalHorasMaquina"]').val();

    var mensagem ="";
    if(codigoMaquina == ""){
        mensagem += "C�digo � obrigat�ria\n";
    }
    if(nome == ""){
        mensagem += "Nome � obrigat�rio\n";
    }
    if(qtd == ""){
        mensagem += "Quantidade � obrigat�rio\n";
    }
    if(numSerie == null){
        mensagem += "N�mero de s�rie � obrigat�rio\n";
    }
    if(totalHoras == ""){
        mensagem += "Total de Horas utilizados � obrigat�rio\n";
    }

    for(i = 0; i < 20; i++){
        nomeCampo = "codigoMaquina" + i;
        if(codigoMaquina == $('input[name = '+nomeCampo+']').val()){
            mensagem += "M�quina j� Registrada\n";
        }
    }

    if(mensagem == "") {
        //Zerando os valores
        $('input[name="nomeMaquina"]').val("");
        $('input[name="nomeMaquinaOs"]').val("");
        $('input[name="descricaoMaquina"]').val("");
        $('input[name="qtdMaquina"]').val("");
        $('select[name="numeroSerieMaquina"]').val("");
        $('select[name="totalHorasMaquina"]').val("");

        var disponivel = excluidosMaquinas.pop();


        if (contadorMaquinas < 20 && disponivel == undefined) {
            contadorMaquinas = contadorMaquinas + 1;
            $("#maquinaUtilizada").append('<div>' +
            '<div class="row" style="padding-top: 1em;">' +
            '<div class="col-md-2 col-lg-1"><label>C�digo</label> <input disabled type="text"  value="' + codigoMaquina + '" class="form-control">' +
            '<input type="hidden" name="codigoMaquina' +contadorMaquinas + '"class="form-control" value="' + codigoMaquina + '"></div>' +
            '<div class="col-md-4 col-lg-4"><label>Nome</label> <input type="text" disabled value="'+nome+'" class="form-control"></div>' +
            '<div class="col-md-2 col-lg-3"><label>N�mero de S�rie</label> <input type="text" disabled class="form-control" value="' + numSerie + '"></div>' +
            '<input type="hidden" name="numeroSerieMaquina'+contadorMaquinas+'" value="' + numSerie + '">' +
            '<div class="col-md-2 col-lg-1"><label>Qtd.</label> <input type="text" class="form-control" value="' + qtd + '" name="qtdMaquina'+contadorMaquinas+'"></div>' +
            '<div class="col-md-2 col-lg-2"><label>Total de Horas</label> <input type="text" name="totalHorasMaquina'+contadorMaquinas+'" class="form-control hora" value="' + totalHoras + '"></div>' +
            '<div style="padding-top: 1.5em"><button id="excluirMaquina" class="btn btn-danger btn-circle" value="' + contadorMaquinas + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-12"><label>Descri��o</label><input type="text" class="form-control" name="descricaoMaquina'+contadorMaquinas+'" value="'+descricao+'"> </div>' +
            '</div>'+
            '</div>');
        } else {
            if(disponivel != undefined){
                contadorMaquinas = contadorMaquinas + 1;
                $("#maquinaUtilizada").append('<div> ' +
                    '<div class="row" style="padding-top: 1em;">' +
                    '<div class="col-md-2 col-lg-1"><label>C�digo</label> <input disabled type="text"  value="' + codigoMaquina + '" class="form-control">' +
                    '<input type="hidden" name="codigoMaquina' + disponivel + '"class="form-control" value="' + codigoMaquina + '"></div>' +
                    '<div class="col-md-4 col-lg-4"><label>Nome</label> <input type="text" disabled value="'+nome+'" class="form-control"></div>' +
                    '<div class="col-md-2 col-lg-3"><label>N�mero de S�rie</label> <input type="text" disabled class="form-control" value="' + numSerie + '"></div>' +
                    '<input type="hidden" name="numeroSerieMaquina'+disponivel+'" value="' + numSerie + '">' +
                    '<div class="col-md-2 col-lg-1"><label>Qtd.</label> <input type="text" class="form-control" value="' + qtd + '" name="qtdMaquina'+disponivel+'"></div>' +
                    '<div class="col-md-2 col-lg-2"><label>Total de Horas</label> <input type="text" name="totalHorasMaquina'+disponivel+'" class="form-control hora" value="' + totalHoras + '"></div>' +
                    '<div style="padding-top: 1.5em"><button id="excluirMaquina" class="btn btn-danger btn-circle" value="' + disponivel + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>'+
                    '</div>'+
                    '<div class="row">' +
                    '<div class="col-md-12"><label>Descri��o</label><input type="text" class="form-control" name="descricaoMaquina'+contadorMaquinas+'" value="'+descricao+'"> </div>' +
                    '</div>'+
                    '</div>'
                );
            }else{
                alertaJquery("Alerta", "Limite m�ximo alcan�ado. \nCaso necess�rio entre em contato com a TI", "alert");
            }
        }
    }else{
        alertaJquery("Alerta", mensagem, "alert");
    }

});

$("#maquinaUtilizada").on("click", "#excluirMaquina", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();
    $(this).parent('div').parent('div').parent('div').remove();
    excluidosMaquinas.push($(this).val());
    contadorMaquinas = contadorMaquinas - 1;
});

/*/Auto preenchimento do campo nome m�quina pelo c�digo

 $("input[name='codigoMaquina']").on("blur",function(){

 var nome = $('input[name="nomeMaquina"]');
 $( nome ).val("Procurando...");

 var codigoMaterial = $(this).val();

 $.getJSON(
 caminho+"/apiSimg/getMate-Cod.php",
 { codMaterial: codigoMaterial },
 function( json )
 {
 $( nome ).val (json.nomeMaterial);
 }
 );
 });/*/

$('#nomeMaquinaOs').on("input", function () {
    dataListFuncao2($('#nomeMaquinaOs'), $("#nomeMaquinaDataList"), $('input[name="nomeMaquina"]'));
});

$('#nomeMaquinaOs').on("blur", function () {
    if ($('input[name="nomeMaquina"]').val() == "") {
        $(this).val("");
    } else {
        var valueFinal = $("#nomeMaquinaDataList").find("option[value='" + $('input[name="nomeMaquinaOs"]').val() + "']");
        $('#nomeMaquinaOs').val(valueFinal.val());
        $('input[name="nomeMaquina"]').val(valueFinal.attr('id'));
    }
});

$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#osMaquinaUtilizada input'), $('#osMaquinaUtilizada select'), $('#osMaquinaUtilizada textarea'));

    if(resultado['permissao']){
        $('#osMaquinaUtilizada').submit();
    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});