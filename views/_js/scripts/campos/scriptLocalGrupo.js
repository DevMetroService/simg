var tabela;

$(document).ready(function(){

    tabela = $("#tabela").dataTable(configTable(0, false, 'asc'));

    /*
    * Pesquisa os dados da avaria selecionada e preeche
    * os campos do formulario com os dados dessa avaria.
    * */

});

$(document).on("click",".btn_editar", function(e){

    var cod_item = $(this).val();
    apiLocalGrupo(cod_item, function (json) {
        $('#cod_local_grupo').val(json.cod_local_grupo);
        $('#nome_local_grupo').val(json.nome_local_grupo);
        $('select[name="cod_grupo"]').val(json.cod_grupo);
        $('select[name="ativo"]').val(json.ativo);
    });

    $('body').animate({
        scrollTop: 0
    });

});