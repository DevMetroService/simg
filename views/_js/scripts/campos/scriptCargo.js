$('.gerarRel').click(function (e) {
    e.preventDefault();
    
    if(hasProcess(e)) return;

    //alertaJquery('Cargo Inserido com Sucesso!','','alert');
    $('#formCargo').submit();
});

$(document).on("click",".btnEditarCargo", function(e){

    $('#subtitulo').html ("Dados do cargo a ser alterado");
    $('#inserirAtualizar').html ("Atualizar");
    $('#divCodigo').show();
    $('.resetarForm').show();

    var codigo_cargo = $(this).parent('td').parent('tr').find('td').html();
    var descricao = $(this).parent('td').parent('tr').find('td').next().html();
    $('input[name="cod_cargos"]').val(codigo_cargo);
    $('input[name="descricao"]').val(descricao);

    $('body').animate({
        scrollTop: 0
    });

$('.resetarForm').click(function (e) {
    e.preventDefault();

    $('#subtitulo').html("Dados do cargo a ser inserido");
    $('#inserirAtualizar').html("Inserir");
    $('#divCodigo').hide();
    $('.resetarForm').hide();
    $('input[name="descricao"]').val(" ");
    $('input[name="cod_cargos"]').val(" ");
});

    $('body').animate({
        scrollTop: 0
    });
});