/**
 * Created by josue.marques on 10/08/2016.
 *
 */

var formDG = $('#osDadosGerais');

//######### Grupo / Sistema / SubSistema
// Auto preenchimento do sistema ap�s sele��o do grupo sistema
$('select[name="grupoOsAtuado"]').on("change", function () {
    var divMr = $('#divMaterialRodante');
    var inputMr = $('#divMaterialRodante input');
    var selectMr = $('#divMaterialRodante select');

    if ($(this).val() == "22" || $(this).val() == "23" || $(this).val() == "26") {
        divMr.show();
        inputMr.each(function () {
            $(this).attr("required", "required");
            $(this).val('');
        });
        selectMr.each(function () {
            $(this).attr("required", "required");
            $(this).val('');
        });

        $('select[name="prefixoMrOsm"]').removeAttr("required");

        $('select[name="carroMrOsm"]').html('');
        apiGetVeiculo($(this).val(), $('select[name="veiculoMrOsm"]'), null, null, false);
    } else {
        divMr.hide();
        inputMr.each(function () {
            $(this).removeAttr("required");
        });
        selectMr.each(function () {
            $(this).removeAttr("required");
        });
    }

    $('input[name="codGrupoOsAtuado"]').val($(this).val());
    $('input[name="codSistemaOsAtuado"]').val('');
    $('input[name="codSubSistema"]').val('');

    apiGrupoSistema($(this), $('select[name="sistemaOsAtuado"]'));
    $('select[name="subSistemaOsAtuado"]').html('<option disabled="disabled" selected="selected">Selecione o Sub-Sistema</option>');
});

$('select[name="grupoOsAtuado"]').on("blur", function(){
    $('select[name="sistemaOsAtuado"] option[value="174"]').each(function(){
        $(this).remove();
    });
});
//Auto preenchimento do subsistema ap�s sele��o do sistema
$('select[name="sistemaOsAtuado"]').on("change", function () {
    $('input[name="codSistemaOsAtuado"]').val($(this).val());
    apiSistemaSub($(this), $('select[name="subSistemaOsAtuado"]'));
});

//Auto preenchimento do codSubSistema
$('select[name="subSistemaOsAtuado"]').on("change", function () {
    $('input[name="codSubSistema"]').val($(this).val());
});

//######### Linha / Trecho / PontoNotavel
//Auto preenchimento do trecho e via ap�s sele��o da linha
$('select[name="linhaOsAtuado"]').on("change", function () {
    apiLinhaTrecho($('select[name="linhaOsAtuado"]'), $('select[name="trechoOsAtuado"]'));
    apiLinhaVia($('select[name="linhaOsAtuado"]'), $('select[name="viaOsAtuada"]'));

    $('select[name="pontoNotavelOsAtuado"]').html('<option disabled="disabled" selected="selected" value="">Pontos Not�veis</option>');
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoOsAtuado"]').on("change", function () {
    apiTrechoPn($('select[name="trechoOsAtuado"]'), $('select[name="pontoNotavelOsAtuado"]'))
});

//Material Rodante
$('select[name="veiculoMrOsm"]').change(function () {
    apiCarro('veiculo', $(this).val(), $('select[name="carroMrOs"]'));
});

//######### Servi�o

var manobraEntrada = ['307', '362'];
var manobraSaida = ['308', '361'];
var service = $('input[name="servicoExecutadoOs"]');
var btnSalvar = $('#btnSalvarDadosGerais');
var selectGroup = $('select[name="grupoOsAtuado"]');
// Servi�o DataList AutoComplete
$('input[name="servicoInput"]').on("input", function () {
    dataListFuncao($('input[name="servicoInput"]'), $('#servicoDataList'), $('input[name="servicoExecutadoOs"]'));
});

var painelComplementoOs = $('.panelModulosOs');
//Verifica se servi�o � manobra ao carregar p�gina
if((selectGroup.val() == "22" || selectGroup.val() == "23" || selectGroup.val() == "26")){
    if ((manobraEntrada.includes(service.val()) || manobraSaida.includes(service.val())) ){
        painelComplementoOs.hide();
        btnSalvar.addClass('encerrarOsm');
        btnSalvar.html('<i class="fa fa-check fa-2x"></i>');
        if ($('#tipo').val() == "Corretiva")
            formDG.attr('action', caminho + '/OSM/encerrarDadosGerais/'+$('input[name="codigoOs"]').val());
        else
            formDG.attr('action', caminho + '/moduloOsp/encerrarDadosGerais');

    }else {
        painelComplementoOs.show();
        btnSalvar.removeClass('encerrarOsm');
        btnSalvar.removeClass('finalizarServico');
        btnSalvar.html('<i class="fa fa-floppy-o fa-2x"></i>');
        if ($('#tipo').val() == "Corretiva")
            formDG.attr('action', caminho + '/OSM/salvarDadosGerais/'+$('input[name="codigoOs"]').val());
        else
            formDG.attr('action', caminho + '/moduloOsp/salvarDadosGerais');
    }
}


$('input[name="servicoInput"]').on("blur", function () {
    //verifica se � MR e se � Manobra de Entrada ou sa�da
    if((selectGroup.val() == 22 || selectGroup.val() == 23 || selectGroup.val() == 26)) {
        if ((manobraEntrada.includes(service.val()) || manobraSaida.includes(service.val())) ){
            painelComplementoOs.hide();
            btnSalvar.addClass('encerrarOsm');
            btnSalvar.html('<i class="fa fa-check fa-2x"></i>');
            if ($('#tipo').val() == "Corretiva")
                formDG.attr('action', caminho + '/OSM/encerrarDadosGerais');
            else
                formDG.attr('action', caminho + '/moduloOsp/encerrarDadosGerais');

        }else {
            painelComplementoOs.show();
            btnSalvar.removeClass('encerrarOsm');
            btnSalvar.removeClass('finalizarServico');
            btnSalvar.html('<i class="fa fa-floppy-o fa-2x"></i>');
            if ($('#tipo').val() == "Corretiva")
                formDG.attr('action', caminho + '/OSM/salvarDadosGerais');
            else
                formDG.attr('action', caminho + '/moduloOsp/salvarDadosGerais');
        }
    }

    if ($('input[name="servicoExecutadoOs"]').val() == "") {
        $(this).val("");
        alertaJquery("Alerta", 'Clique em um servi�o', "alert");
    } else {
        $('input[name="servicoInput"]').val($("#servicoDataList").find("option[value='" + $('input[name="servicoExecutadoOs"]').val() + "']").text());


    }
});

$('.encerrarOsm').click(function () {

});

if ($('input[name="servicoExecutadoOs"]').val() != "") {
    $('input[name="servicoInput"]').val($("#servicoDataList").find("option[value='" + $('input[name="servicoExecutadoOs"]').val() + "']").text());
}


btnSalvar.click(function (e) {
    if (hasProcess(e)) return;

    var resultado = validacao_formulario($('#osDadosGerais input'), $('#osDadosGerais select'), $('#osDadosGerais textarea'));

    if (resultado['permissao']) {
        formDG.submit();
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

$('select[name="motivo"]').change(function () {
    if ($(this).val() == 24) {
        $('#campoCodOsDuplicado').show();
        $('input[name="codOsDuplicado"]').attr('required', 'required');
    } else {
        $('#campoCodOsDuplicado').hide();
        $('input[name="codOsDuplicado"]').removeAttr('required', 'required');
    }
});

$('input[name="codOsDuplicado"]').blur(function () {
    if ($('#formCancelamento').html() == 'Osm') {
        validadeOsm($(this));
    } else {
        validadeOsp($(this));
    }
});

$(document).on("click", ".btnOsmDuplicado", function (e) {
    if (hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsm",
        data: {codigoOs: $(this).val()},
        success: function () {
            window.location.replace(caminho + "/dashboardGeral/osm");
        }
    });
});
$(document).on("click", ".btnOspDuplicado", function (e) {
    if (hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsp",
        data: {codigoOs: $(this).val()},
        success: function () {
            window.location.replace(caminho + "/dashboardGeral/osp");
        }
    });
});


$(".btnImprimirFormulario").on("click", function () {
    var codigoOs = $('input[name="codigoOs"]').val();
    var tipo = $(this).data('form');

    if (tipo != 'osp' && tipo != 'osm'){
        tipo = 'osmp';
    }

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/imprimir"+tipo,
        data: {codigoOs: codigoOs},
        success: function () {
            window.open(caminho + "/dashboardGeral/print"+tipo, "_blank");
        }
    });
});

$(document).ready(function()
{
    $('#btnCancelarOs').click(function () {
        var confirm = function (result)
        {
            if(result == 'SIM')
                $('#motivoModal').modal('show')
        };

        alertaJquery("Cancelamento de OS", "Ap�s o cancelamento, n�o ser� poss�vel a edi��o deste documento. </br>Deseja continuar?", "alert", [{ value: "SIM" },{ value: "N�O" }], confirm);
    });
});




var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});
