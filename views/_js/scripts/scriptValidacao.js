var tableSsmValidacao = $(".indicadorAuditoriaSaf").DataTable(configTable(0, false, 'desc'));

var tableSsmValidacaoEd = $("#tbValidacaoEd").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoSu = $("#tbValidacaoSu").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoRa = $("#tbValidacaoRa").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoEn = $("#tbValidacaoEn").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoVp = $("#tbValidacaoVp").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoBi = $("#tbValidacaoBi").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoTl = $("#tbValidacaoTl").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoTUE = $("#tbValidacaoTUE").DataTable(configTable(0, false, 'desc'));
var tableSsmValidacaoVLT = $("#tbValidacaoVLT").DataTable(configTable(0, false, 'desc'));

var observacaoModal = $('#observacaoModal');
var btnFinalizarValidacao = $('#btnFinalizarValidacao');
var btnValidar = $('#spanValidar');
var numServicoValidacao = $("#numServicoValidacao");
var rowValidacaoModal;


$(document).on("click",".btnValidacaoOpenModal", function(e){
    var status = $(this).data('status');
    var codSsm = $(this).data('ssm');

    var tbID = $(this).parents('table').attr("id");
    switch(tbID)
    {
        case "tbValidacaoEd":
            rowValidacaoModal = tableSsmValidacaoEd.row( $(this).parents('tr') );
            break;

        case "tbValidacaoSu":
            rowValidacaoModal = tableSsmValidacaoSu.row( $(this).parents('tr') );
            break;

        case "tbValidacaoRa":
            rowValidacaoModal = tableSsmValidacaoRa.row( $(this).parents('tr') );
            break;

        case "tbValidacaoEn":
            rowValidacaoModal = tableSsmValidacaoEn.row( $(this).parents('tr') );
            break;

        case "tbValidacaoVp":
            rowValidacaoModal = tableSsmValidacaoVp.row( $(this).parents('tr') );
            break;

        case "tbValidacaoBi":
            rowValidacaoModal = tableSsmValidacaoBi.row( $(this).parents('tr') );
            break;

        case "tbValidacaoTl":
            rowValidacaoModal = tableSsmValidacaoTl.row( $(this).parents('tr') );
            break;

        case "tbValidacaoTUE":
            rowValidacaoModal = tableSsmValidacaoTUE.row( $(this).parents('tr') );
            break;

        case "tbValidacaoVLT":
            rowValidacaoModal = tableSsmValidacaoVLT.row( $(this).parents('tr') );
            break;

        default:
            rowValidacaoModal = tableSsmValidacao.row( $(this).parents('tr') );
            break;
    }

    numServicoValidacao.html(codSsm);
    // Recebe a linha clicada da tabela.
    
    // Limpa o campo de observa��o
    observacaoModal.val(''); 

    // Tenta pausar o tempo de carregamento da p�gina, se houver
    try {
        clearTimeout(atualizaDashboardSupervisao);
    }catch(e){
        console.log(e);
    }

    //Passa ao filho os dados do pai
    btnFinalizarValidacao.data('status', status );
    btnFinalizarValidacao.data('ssm', codSsm );

    //Seta diferenciais caso valida��o ou reprova��o.
    if (status == 36)
    {
        observacaoModal.attr("required", "required");
        btnValidar.html("Devolver");
    }else{
        observacaoModal.removeAttr("required");
        btnValidar.html("Validar");
    }

    infosModal = $('#infosModal');
    apiListaOsmPorSsm (codSsm, infosModal);
});

// $(document).on('click', '#btnFinalizarValidacao', function () {
btnFinalizarValidacao.click(function()
{
    var status = $(this).data('status');
    var codSsm = $(this).data('ssm');

    observacaoModal.val($.trim(observacaoModal.val()));

    if(status == 36)
    {
        if(observacaoModal.val() != "" )
        {
            alertaJquery('Confirma��o', 'Deseja realmente realizar a N�O VALIDA��O da SSM ' + codSsm + '? Essa a��o n�o poder� ser desfeita', 'success',[{value:'Sim'}, {value:'N�o'}], function (res) {

                if (res == 'Sim') {
                    rowValidacaoModal.remove().draw();

                    apiValidaDevolveSsm (codSsm, status, observacaoModal.val());
                }
            });
        }else{
            alertaJquery('Campos Requeridos', 'Campo de observa��o � obrigat�rio ao n�o validar um servi�o.', 'alert');
        }

    }else{
        alertaJquery('Confirma��o', 'Deseja realmente realizar a VALIDAR da SSM ' + codSsm + '? Essa a��o n�o poder� ser desfeita', 'success',[{value:'Sim'}, {value:'N�o'}], function (res) {

            if (res == 'Sim') {
                rowValidacaoModal.remove().draw();

                apiValidaDevolveSsm (codSsm, status, observacaoModal.val());
            }
        });
    }

    try{
        atualizaDashboardSupervisao = setTimeout(function () {
            window.location.reload(1);
        }, miliSegReload);
    }catch(e){
        console.log(e);
    }


});