/**
 * Created by josue.marques on 03/06/2016.
 **/
$('[rel="tempoTotal"]').tooltip();

//################################# M�dulo M�o de Obra ################################
//################################# M�dulo M�o de Obra ################################
//################################# M�dulo M�o de Obra ################################

var campoMatricula = $("input[name='matriculaMaoObraOs']");
var dataListSolicitande = $('#nomeDataList');
var codFuncioanario = $("input[name='codFuncionarioMaoObraOs']");
var tipoFuncionario = $('select[name="codTipoFuncionario"]');

//Origem de solicitante
tipoFuncionario.on("change", function () {
    campoMatricula.val('');
    var cod_origem = $(this).val();

    $.getJSON(
        caminho + "/api/returnResult/dados/getFuncionariosByTipo",
        {dados: cod_origem},
        function (json) {
            var option = "";
            $.each(json, function (key, value) {
                var funcionario = value;
                option += ('<option id="' + funcionario.cod_funcionario + '" data-mat="'+ value.matricula +'" value="' + funcionario.nome_funcionario + ' - ' + funcionario.matricula + '"/>').toUpperCase();
            });
            dataListSolicitande.html(option);
        }
    );

});

// ########### AutoComplete DataList Nome Matricula
$('input[name="matriculaMaoObraOs"]').on("blur", function() {
    apiMatriculaNomeFuncionario(tipoFuncionario, campoMatricula, $("input[name='nomeMaoObraOs']"));
});

$('input[name="nomeMaoObraOs"]').on("input",function() {

    var valueFinal = dataListSolicitande.find("option[value='" + $('input[name="nomeMaoObraOs"]').val() + "']");
    if (valueFinal.val() != null) {

        campoMatricula.val(valueFinal.data("mat"));
        codFuncioanario.val(valueFinal.attr('id'));

        // apiMatriculaNomeFuncionario($('select[name="codTipoFuncionario"]'), $('input[name="matriculaMaoObraOs"]'), $("input[name='nomeMaoObraOs']"));
    }
});

//Calculo do total de horas
$("input[name='terminoServicoMaoObraOs']").on("blur",function(){
    verificarDiferencaHoras();
});

function verificarDiferencaHoras() {
    var inicio  = $('input[name="inicioServicoMaoObraOs"]').val();
    var termino = $("input[name='terminoServicoMaoObraOs']").val();

    if(termino == '') return false;

    if(inicio == termino){
        alertaJquery("Alerta", "Hor�rio igual. Por favor, informe hor�rios diferentes", "alert");
        $("input[name='terminoServicoMaoObraOs']").val('');
        return false;
    }

    var tempoTotal = diferencaHoras(inicio, termino);
    $('input[name="totalHoraMaoObraOs"]').val(tempoTotal);
    return true;
}


var excluidosMaoObra = new Array();
var contadorMaoObra = 0;

if ($('input[name="contadorMaoObra"]').val() >= 1) {
    contadorMaoObra = Number($('input[name="contadorMaoObra"]').val());
}

$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $("#addMaoObraOs").trigger('click');
    }
});


//Fun��o que adiciona funcionario na lista
$("#addMaoObraOs").on("click", function (e) {		//Fun��o para adicionar a div de benefici�rios
    e.preventDefault();

    if(!verificarDiferencaHoras()) return;

    var matricula = $('input[name="matriculaMaoObraOs"]').val();
    var cod_funcionario = codFuncioanario.val();
    var nome      = $('input[name="nomeMaoObraOs"]').val();

    var inicio    = $('input[name="inicioServicoMaoObraOs"]').val();
    var termino   = $('input[name="terminoServicoMaoObraOs"]').val();
    var total     = $('input[name="totalHoraMaoObraOs"]').val();

    var mensagem ="";
    //if(matricula == ""){
      //  mensagem += "Matricula � obrigat�ria\n";
    //}
    if(inicio == ""){
        mensagem += "In�cio � obrigat�rio\n";
    }
    if(termino == ""){
        mensagem += "T�rmino � obrigat�rio\n";
    }

    // for(i = 0; i < 15; i++){
    //     nomeCampo = "matriculaFuncionarioMaoObraOs" + i;
    //     if(matricula == $('input[name = '+nomeCampo+']').val()){
    //         mensagem += "Matricula j� Registrada\n";
    //     }
    // }

    if(mensagem == "") {

        $('input[name="matriculaMaoObraOs"]').val("");
        $('input[name="nomeMaoObraOs"]').val("");

        var disponivel = excluidosMaoObra.pop();

        if (contadorMaoObra < 100 && disponivel == undefined) {
            contadorMaoObra = contadorMaoObra + 1;

            $("#maoObraOs").append('<div class="row" style="padding-top: 1em;">' +
                '<div class="col-md-2 col-lg-2"><label>Matr�cula</label> <input disabled type="text"  value="' + matricula + '" class="form-control">' +
                '<input type="hidden" name="matriculaFuncionarioMaoObraOs' + contadorMaoObra + '"class="form-control" value="' + cod_funcionario + '"></div>' +
                '<div class="col-md-4 col-lg-4"><label>Nome</label> <input type="text" disabled name="nomeLista'+ contadorMaoObra +'" value="' + nome + '" class="form-control"></div>' +
                '<div class="col-md-2 col-lg-2"><label>In�cio</label> <input type="text" disabled class="form-control" value="' + inicio + '">' +
                '<input type="hidden" name="InicioServicoMaoObraOs' + contadorMaoObra + '" value="' + inicio + '" class="form-control"></div>' +
                '<div class="col-md-2 col-lg-2"><label>T�rmino</label> <input type="text" disabled class="form-control" value="' + termino + '">' +
                '<input type="hidden" name="TerminoServicoMaoObraOs' + contadorMaoObra + '" value="' + termino + '" class="form-control"></div>' +
                '<input type="hidden" name="totalHoraMaoObraOs' + contadorMaoObra + '" value="' + total + '" class="form-control">' +
                '<div style="padding-top: 1.5em"><button id="excluirFuncionario" class="btn btn-danger btn-circle" value="' + contadorMaoObra + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div></div>');
            if(nome == ""){
                apiMatriculaNomeFuncionario($('input[name="matriculaFuncionarioMaoObraOs'+contadorMaoObra+'"]'), $('input[name="nomeLista'+ contadorMaoObra +'"]'));
            }

            $('input[name="matriculaMaoObraOs"]').focus();
        } else {
            if(disponivel != undefined){
                contadorMaoObra = contadorMaoObra + 1;
                $("#maoObraOs").append('<div class="row" style="padding-top: 1em;">' +
                    '<div class="col-md-2 col-lg-2"><label>Matr�cula</label> <input disabled type="text" value="' + matricula + '" class="form-control">' +
                    '<input type="hidden" name="matriculaFuncionarioMaoObraOs' + disponivel + '"class="form-control" value="' + cod_funcionario + '"></div>' +
                    '<div class="col-md-4 col-lg-4"><label>Nome</label> <input type="text" disabled name="nomeLista'+ disponivel +'" value="' + nome + '" class="form-control"></div>' +
                    '<div class="col-md-2 col-lg-2"><label>In�cio</label> <input type="text" disabled class="form-control" value="' + inicio + '">' +
                    '<input type="hidden" name="InicioServicoMaoObraOs' + disponivel + '" value="' + inicio + '" class="form-control"></div>' +
                    '<div class="col-md-2 col-lg-2"><label>T�rmino</label> <input type="text" disabled class="form-control" value="' + termino + '">' +
                    '<input type="hidden" name="TerminoServicoMaoObraOs' + disponivel + '" value="' + termino + '" class="form-control"></div>' +
                    '<input type="hidden" name="totalHoraMaoObraOs' + disponivel + '" value="' + total + '" class="form-control">' +
                    '<div style="padding-top: 1.5em"><button id="excluirFuncionario" class="btn btn-danger btn-circle" value="' + disponivel + '" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div></div>');

                if(nome == ""){
                    apiMatriculaNomeFuncionario($('input[name="matriculaFuncionarioMaoObraOs'+disponivel+'"]'), $('input[name="nomeLista'+ disponivel +'"]'));
                }

                $('input[name="matriculaMaoObraOs"]').focusin();
            }else{
                alertaJquery("Alerta", "Limite m�ximo alcan�ado. \nCaso necess�rio entre em contato com a TI", "alert");
            }
        }
    }else{
        alertaJquery("Alerta", mensagem, "alert");
    }

});

$("#maoObraOs").on("click", "#excluirFuncionario", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();
    $(this).parent('div').parent('div').remove();
    excluidosMaoObra.push($(this).val());
    contadorMaoObra = contadorMaoObra - 1;
});

$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#formMaoObra input'), $('#formMaoObra select'), $('#formMaoObra textarea'));

    if(resultado['permissao']){
        $('#formMaoObra').submit();
    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);
    console.log(isElementInView);
    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});
