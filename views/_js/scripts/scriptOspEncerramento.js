/**
 * Created by ricardo.diego on 16/12/2015.
 */

// ######## Valida��o data
$('input[name="dataHoraFechamento"]').on('blur', function () {
    if (!($(this).val() == '')) {
        if (fctValidaDataHora($(this))) {
            var dataEncerramento = transformaData($(this).val());
            var dataDesmobilizacao = transformaData($('#desmobilizacao').val());

            if (dataEncerramento < dataDesmobilizacao) {
                $(this).val('');
                alertaJquery("Alerta", 'Data de Encerramento n�o pode ser inferior a data de Desmobiliza��o', "alert");
            }

            var dataServer = new Date;

            if (dataEncerramento > dataServer) {
                $(this).val('');
                alertaJquery("Alerta", 'A data preenchida n�o pode ser superior a data e hor�rio atual', "alert");
            }
        }
    }
});


// ######## Hide - Show / Tipo de Fechamento
$('select[name="descricaoTipoFechamento"]').on("change", function () {
    retirarRequired();

    switch ($(this).val()) {
        case "1": //Sem pendencia
            $('#comPendencia').hide();
            $('#motivoNaoExecucao').hide();
            break;
        case "2": //Com pendencia
            $('#comPendencia').show();

            $('input[name="numeroTipoPendenciaFechamento"]').val("");

            $('select[name="tipoPendenciaFechamento"]').val("");
            $('select[name="tipoPendenciaFechamento"]').attr('required', 'required');

            $('input[name="numeroPendenciaFechamento"]').val("");

            $('select[name="pendenciaFechamento"]').val("");
            $('select[name="pendenciaFechamento"]').attr('required', 'required');

            $('#motivoNaoExecucao').hide();
            break;
        case "3": //N�o Executado
            $('#comPendencia').hide();

            $('#motivoNaoExecucao').show();
            $('input[name="motivoNaoExecucao"]').attr('required', 'required');
            break;
    }


    if ($('select[name="liberacaoTrafego"]').val() == "n") {
        if ($('select[name="descricaoTipoFechamento"]').val() == "1") {
            if (!$('input[name="form"]').val()){
                $('button[name="btnEncerrarOs"]').attr('disabled', 'disabled');
            }
        }
    } else {
        $('button[name="btnEncerrarOs"]').removeAttr('disabled');
    }
});

if ($('select[name="descricaoTipoFechamento"]').val()) {
    switch ($('select[name="descricaoTipoFechamento"] option:selected').val()) {
        case "2": //Com pendencia
            $('#comPendencia').show();

            $('select[name="tipoPendenciaFechamento"]').attr('required', 'required');
            $('select[name="pendenciaFechamento"]').attr('required', 'required');
            break;

        case "3": //N�o Executado
            $('#motivoNaoExecucao').show();

            $('input[name="motivoNaoExecucao"]').attr('required', 'required');
            break;
    }
}


// ######### Libera��o
$('select[name="liberacaoTrafego"]').on("change", function () {
    if ($('select[name="liberacaoTrafego"]').val() == "n") {
        if ($('select[name="descricaoTipoFechamento"]').val() == "1") {
            if (!$('input[name="form"]').val()){
                $('button[name="btnEncerrarOs"]').attr('disabled', 'disabled');
            }
        }
    } else {
        $('button[name="btnEncerrarOs"]').removeAttr('disabled');
    }
});


// ########### AutoComplete DataList Nome Matricula
var campoMatricula = $("input[name='matriculaResponsavelInformacoesOs']");
var dataListSolicitande = $('#nomeSolicitanteDataList');
var codFuncioanario = $("input[name='codFuncionario']");
var solicitante = $("input[name='responsavelInformacaoOs']");
var tipo_funcionario = $('select[name="codTipoFuncionario"]');

tipo_funcionario.on("change", function () {
    campoMatricula.val('');
    codFuncioanario.val('');

    var cod_origem = $(this).val();

    $.getJSON(
        caminho + "/api/returnResult/dados/getFuncionariosByTipo",
        {dados: cod_origem},
        function (json) {
            var option = "";
            $.each(json, function (key, value) {
                var funcionario = value;
                option += ('<option id="' + funcionario.cod_funcionario + '" data-mat="'+ value.matricula +'" value="' + funcionario.nome_funcionario + ' - ' + funcionario.matricula + '"/>').toUpperCase();
            });
            dataListSolicitande.html(option);
        }
    );

});

solicitante.on("input",function() {

    var valueFinal = dataListSolicitande.find("option[value='" + solicitante.val() + "']");
    if (valueFinal.val() != null) {

        campoMatricula.val(valueFinal.data("mat"));
        codFuncioanario.val(valueFinal.attr('id'));

        apiMatriculaNomeFuncionario(tipo_funcionario, campoMatricula, solicitante);
    }
});


// ############ Com Pendencia
$('select[name="tipoPendenciaFechamento"]').on("change", function () {
    var tipo_pendencia = $(this).val();
    $('input[name="numeroTipoPendenciaFechamento"]').val(tipo_pendencia);

    $.getJSON(
        caminho + "/api/returnResult/tipoPendencia",
        {tipoPendencia: tipo_pendencia},
        function (json) {
            var option = "";

            option += '<option selected disabled value="">Selecione a Pendencia</option>';

            $.each(json, function (key, value) {
                option += '<option value="' + key + '">' + value + '</option>';
            });
            $('select[name="pendenciaFechamento"]').html(option);

            $('input[name="numeroPendenciaFechamento"]').val($('select[name="pendenciaFechamento"]').val());

        }
    )
});

if ($('select[name="tipoPendenciaFechamento"]').length) {
    $('input[name="numeroTipoPendenciaFechamento"]').val($('select[name="tipoPendenciaFechamento"]').val());
}

$('select[name="pendenciaFechamento"]').on("change", function () {
    $('input[name="numeroPendenciaFechamento"]').val($(this).val());
});

if ($('select[name="pendenciaFechamento"]').length) {
    $('input[name="numeroPendenciaFechamento"]').val($('select[name="pendenciaFechamento"]').val());
}


// ############ btnEncerramento
$('button[name="btnEncerrarOs"]').on("click", function () {
    var data = $('input[name="dataHoraFechamento"]').val();
    var mat = $('input[name="matriculaResponsavelInformacoesOs"]').val();
    var permit = false;
    var txt = "";

    if (data != '' && mat != '') {
        permit = true;
        var tipo_fechamento = $('select[name="descricaoTipoFechamento"]');

        switch (tipo_fechamento.val()) {
            case "2":
                if ($('select[name="tipoPendenciaFechamento"] option:selected').val() == "" || $('select[name="pendenciaFechamento"] option:selected').val() == "") {
                    permit = false;
                    txt += "\nPreencha o tipo pendencia e a pendencia.";
                }
                break;
            case "3": // Verifica se motivo n�o execu��o esta preenchido e se os dias restantes s�o maiores que 0
                if ($('input[name="motivoNaoExecucao"]').val() == "") {
                    permit = false;
                    txt += "\nPreencha o Motivo.";
                }
            case "1":
                //Verifica��o se ainda h� dias para execu��o.
                if ($('#diasRestantes').val() > "0") {
                    // if (!(confirm("Ainda h� outros dias de execu��o. \nDeseja encerrar sem pend�ncia mesmo assim?"))) {
                    //     return;
                    // }
                    alertaJquery('Ainda h� outros dias de execu��o.', 'Deseja encerrar sem pend�ncia mesmo assim?', 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "N�o") {
                            return;
                        }
                    });
                }
                break;
            default:
                permit = false;
                txt += "\nSelecione o tipo de fechamento.";
                break;
        }

    }

    if (permit) {
        if ($('select[name="descricaoTipoFechamento"]').val() == "1") {
            alertaJquery("Alerta", 'Falha corrigida com sucesso.', "alert");
        } else {
            alertaJquery("Alerta", 'O.S. Encerrada c/ pend�ncia ou n�o executada.', "alert");
        }
        $('#formEncerramentoOs').submit();
    } else {
        alertaJquery("Alerta", 'H� campos obrigat�rios ainda n�o preenchidos!\n' + txt, "alert");
    }

});

function retirarRequired() {
    $('select[name="tipoPendenciaFechamento"]').removeAttr('required');
    $('select[name="pendenciaFechamento"]').removeAttr('required');

    $('input[name="motivoNaoExecucao"]').removeAttr('required');
}

function transformaData(data) {
    var dia = data.substring(0, 2);
    var mes = data.substring(3, 5);
    var ano = data.substring(6, 10);

    var hor = data.substring(11, 13);
    var min = data.substring(14, 16);

    return new Date(ano, (mes - 1), dia, hor, min);
}

$('button[name="btnSalvarOs"]').on('click', function (e) {
    if (hasProcess(e)) return;

    var resultado = validacao_formulario($('#formEncerramentoOs input'), $('#formEncerramentoOs select'), $('#formEncerramentoOs textarea'));

    if (resultado['permissao']) {
        $('#formEncerramentoOs').submit();
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});