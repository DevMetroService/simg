/**
 * Created by josue.santos on 10/10/2016.
 */

var tabelaPmpBl;
var tabelaPmpBlExec;

$.getScript(caminho + "/views/_js/scripts/scriptPmp.js", function(){ // include functions
    $(document).ready(function(){
        tabelaPmpBl = $("#tabelaPmpBl").dataTable(configTable(0, false, 'asc'));
        
        tabelaPmpBlExec = $("#tabelaPmpBlExec").dataTable(configTable(0, false, 'asc'));
    });
});

//***********************************Proprios do formularios

// MultiSelect Estacao
$('document').ready(function () {
    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"

    });
});

//***********************************Proprios do formularios

var excluidosLinhasPmp = new Array();
var contador = 1;

//================= On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {
    var sistema         = $('select[name="sistema"] option:selected');
    var subSistema      = $('select[name="subSistema"] option:selected');
    var servicoPmp      = $('select[name="servicoPmp"] option:selected');
    var periodicidade   = $('select[name="periodicidade"] option:selected');
    var turno           = $('select[name="turno"] option:selected');
    var procedimento    = $('input[name="procedimento"]');
    var qzn             = $('input[name="quinzena"]');
    var maoObra         = $('input[name="maoObra"]');
    var horasUteis      = $('input[name="horasUteis"]');
    var homemHora       = $('input[name="homemHora"]');

    if (sistema.val() == ''       || sistema.val() == null          || sistema.val() == undefined ||
        subSistema.val() == ''    || subSistema.val() == null       || subSistema.val() == undefined ||
        servicoPmp.val() == ''    || servicoPmp.val() == null       || servicoPmp.val() == undefined ||
        periodicidade.val() == '' || periodicidade.val() == null    || periodicidade.val() == undefined ||
        turno.val() == ''         || turno.val() == null            || turno.val() == undefined ||
        qzn.val() == ''           || qzn.val() == null              || qzn.val() == undefined ||
        maoObra.val() == ''       || maoObra.val() == null          || maoObra.val() == undefined ||
        horasUteis.val() == ''    || horasUteis.val() == null       || horasUteis.val() == undefined)
    {

        alertaJquery("Alerta", "Preencha todos os campos corretamente", "alert");

    } else {
        //================ Dados a serem adicionados na LISTA de PMP Telecom. ==========================//
        var txt = '';
        $('select[name="estacao[]"] option:selected').each(function () {
            var duplicado = true;
            var col = tabelaPmpBlExec._('tr');

            //Tabela do Banco de Dados
            for (var i = 0; i < col.length; i++) {
                var dataTable = tabelaPmpBlExec.fnGetData(i);

                if ((dataTable[1] == $(this).text()) && 
                    (dataTable[2] == sistema.text()) &&
                    (dataTable[3] == subSistema.text()) &&
                    (dataTable[4] == servicoPmp.text()) &&
                    (dataTable[5] == periodicidade.text())) {
                    
                    txt = txt + ' - cod - ' + dataTable[0].split("<input")[0] + " Duplicado no Banco de Dados.\n";
                    duplicado = false;
                    break;
                }
            }

            col = tabelaPmpBl._('tr');

            //Tabela de Lista
            for (var i = 0; i < col.length; i++) {
                var dataTable = tabelaPmpBl.fnGetData(i);
                
                if ((dataTable[1].split("<input")[0] == $(this).text()) && 
                    (dataTable[2].split("<input")[0] == sistema.text()) &&
                    (dataTable[3].split("<input")[0] == subSistema.text()) &&
                    (dataTable[4].split("<input")[0] == servicoPmp.text()) &&
                    (dataTable[5].split("<input")[0] == periodicidade.text())) {
                    
                    txt = txt + ' - id - ' + dataTable[0] + " Duplicado na Lista.\n";
                    duplicado = false;
                    break;
                }
            }

            if (duplicado) {
                var disponivel = excluidosLinhasPmp.pop();

                if (disponivel == undefined)
                    var ctdAux = contador;
                else
                    var ctdAux = disponivel;

                //==========Turno==========//
                if (turno.val() == 'D') {
                    var turnoHTML =
                        '<select name="turno' + ctdAux + '" class="form-control" required>' +
                        '<option value="D" selected>Diurno</option>' +
                        '<option value="N">Noturno</option>' +
                        '</select>';
                } else {
                    var turnoHTML =
                        '<select name="turno' + ctdAux + '" class="form-control" required>' +
                        '<option value="N" selected>Noturno</option>' +
                        '<option value="D">Diurno</option>' +
                        '</select>';
                }
                
                tabelaPmpBl.fnAddData([
                    ctdAux,
                    //==========Estacao==========//
                    $(this).text() + '<input type="hidden" name="estacao' + ctdAux + '" value="' + $(this).val() + '" class="form-control number" readonly>' +
                    '<input type="hidden" name="grupo' + ctdAux + '" value="28" class="form-control number" readonly>',
                    //==========Sistema==========//
                    sistema.text() + '<input type="hidden" name="sistema' + ctdAux + '" value="' + sistema.val() + '" class="form-control number" readonly>',
                    //==========SubSistema==========//
                    subSistema.text() + '<input type="hidden" name="subSistema' + ctdAux + '" value="' + subSistema.val() + '" class="form-control number" readonly>',
                    //==========Servi�o==========//
                    servicoPmp.text() + '<input type="hidden" name="servicoPmp' + ctdAux + '" value="' + servicoPmp.val() + '" class="form-control number" readonly>',
                    //==========Periodicidade==========//
                    periodicidade.text() + '<input type="hidden" name="periodicidade' + ctdAux + '" value="' + periodicidade.val() + '" class="form-control number" readonly>',
                    //==========Procedimento==========//
                    procedimento.val(),
                    //==========Qzn In�cio==========//
                    '<input type="text" name="quinzena' + ctdAux + '" value="' + qzn.val() + '" class="form-control number">',
                    //==========Turno==========//
                    turnoHTML,
                    //==========M�o de Obra==========//
                    '<input name="maoObra' + ctdAux + '" type="text"  value="' + maoObra.val() + '" class="form-control number maoObra" required>',
                    //==========Horas �teis==========//
                    '<input name="horasUteis' + ctdAux + '" type="text"  value="' + horasUteis.val() + '" class="form-control number horasUteis" required>',
                    //==========Homem Hora==========//
                    '<input name="homemHora' + ctdAux + '" type="text"  value="' + homemHora.val() + '" class="form-control" readonly>',
                    //==========A��es==========//
                    '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
                ]);

                contador = contador + 1;
            }
        });
        if (txt != '')
            alertaJquery("Alerta", "DUPLICADOS:\n\n <textarea rows='20' cols='30'>" + txt + "</textarea>", "alert");
    }
});
//=================================================//

var contadorExec = $("input[name='ctd']").val();

//=================================================//

$("#tabelaPmpBl").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpBl = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaPmpBl._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaPmpBl.fnGetData(i);
        if (dataTable[0] == idPmpBl) {
            tabelaPmpBl.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmp.push(idPmpBl);
    contador = contador - 1;
});

// $(window).load(function() {
//     document.getElementById("loading").style.display = "none";
//     document.getElementById("conteudo").style.display = "inline";
// })

$('.salvarListaPmp').click(function () {
    var data = tabelaPmpBl.$('input, select').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaPmpBlExec.$('input, select').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + contadorExec;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    $.post(
        caminho + "/cadastroGeral/refillPmp",
        {
            sistema:        $('select[name="sistema"] option:selected').val(),
            subSistema:     $('select[name="subSistema"] option:selected').val(),
            servicoPmp:     $('select[name="servicoPmp"] option:selected').val(),
            periodicidade:  $('select[name="periodicidade"] option:selected').val(),
            procedimento:   $('input[name="procedimento"]').val(),
            quinzena:       $('input[name="quinzena"]').val(),
            turno:          $('select[name="turno"] option:selected').val(),
            maoObra:        $('input[name="maoObra"]').val(),
            horasUteis:     $('input[name="horasUteis"]').val(),
            homemHora:      $('input[name="homemHora"]').val()
        }
    );

    if (data) {
        $.post(
            caminho + "/cadastroGeral/salvarListaPmp",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }
});

var ano = $('#pmpAno');
var ativo = $('#pmpAtivo');
var novo = $('#pmpNovo');

ano.change(function(){
    var d = new Date();
    var n = d.getFullYear();

    if(n == $(this).val())
    {
        ativo.prop('disabled', 'disabled');
        ativo.prop('checked', false);
    }else
    {
        ativo.removeAttr('disabled');
    }
});

$('.gerarPmpAnual').click(function () {
    var year = ano.val();
    var isAtivo = ativo.prop('checked');
    var isNovo = novo.prop('checked');

    alertaJquery('Gerar PMP Anual',
        'Tem certeza que deseja finalizar o PMP Anual?<h4>Verifique antes se todas as altera��es foram salvas.</h4>',
        'alert', [{value:"Sim"},{value:"N�o"}], function (res) {
            if(res == "Sim") {
                alertaJquery('Gerar PMP Anual',
                    '<h3>Esta a��o n�o poder� ser desfeita!</h3>Deseja continuar?</br><small>O Processo poder� levar alguns minutos. Por favor, Aguarde.</small>',
                    'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "Sim") {
                            loader();
                            window.location.replace(caminho + "/pmp/gerarPmpAnual/28/"+year+"/"+isAtivo+"/"+isNovo);
                        }
                        if(res == "N�o")
                        {
                            $('#gerarCronograma').modal('show');
                        }
                    }
                );
            }
            if(res == "N�o")
            {
                $('#gerarCronograma').modal('show');
            }
        }
    );
});

$('[data-toggle="tooltip"]').tooltip(
    {
        html: true
    }
);