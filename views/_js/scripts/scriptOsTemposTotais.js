var lib = 0;

if ($('.tempoTotais').val()) {
    lib = 11;
}

$('.tempoTotais').on('focus', function () {
    if ($(this).val() == '') {
        lib -= 1;
    } else {
        lib -= 2;
    }
});

$('.tempoTotais').on('blur', function () {

    //Recebe o DOM do hor�rio escrito.
    var inputDat = $(this);

    //Verifica se n�o est� vazio.
    if (!(inputDat.val() == '')) {
        if (fctValidaDataHora(inputDat)) {

            //Recebe o valor do "alvo", � a posi��o do input.
            var num = parseInt(inputDat.data('target'));
            var numAnterior = num - 1;

            //Verifica se o valor anterior � zero, logo, o alvo ir� verificar com a dataRecebimento (Data Abertura).
            if (numAnterior == 0) {
                var dataAnterior = $("input[name='dataRecebimento']").val();
            } else {
                //Recebe a data descrita no input Anterior
                var dataAnterior = $('.tt' + numAnterior).val();
            }

            //Verifica se a data anterior � vazia
            if (dataAnterior == '') {
                alertaJquery("Alerta", 'Preencha a data anterior.', "alert");
                //Zera o campo atual escrito.
                inputDat.val('');

                //Adiciona mais 1 ponto na variavel de libera��o.
                lib += 1;
            } else {

                //Trata os valores retirados do Input e retorna em formate DATE
                dataAnterior = tratamentoTempo(dataAnterior);
                var dataPreenchida = tratamentoTempo(inputDat.val());

//==========================================================================================

                //Recebe o valor do hor�rio atualizado pelos servidor.
                var dataServer = tratamentoTempo($('.dataRelogioNavBarSpan').html());

                //Verifica se a data preenchida � superior � data do servidor.
                if (dataPreenchida > dataServer) {
                    //Limpa o campo preenchido e alerta o usu�rio. Adiciona mais 1 � libera��o
                    inputDat.val('');
                    alertaJquery("Alerta", 'A data preenchida n�o pode ser superior a data e hor�rio atual do Servidor', "alert");
                    lib += 1;
                } else {

//==========================================================================================
                    //Verifica se a data preenchida � maior ou igual a data anterior.
                    if (dataPreenchida >= dataAnterior) {
                        //Recebe o n�mero do pr�ximo INPUT
                        var numProx = num + 1;

                        //Verifica se ainda h� INPUTS
                        if (numProx < 12) {
                            //Pega o valor da pr�xima Data
                            var dataProx = $('.tt' + numProx).val();

                            //Caso seja vazio, desbloqueia pr�ximo campo para preenchimento. Adiciona +2 a libera��o.
                            if (dataProx == '') {
                                $('.tt' + numProx).removeAttr('readonly');
                                lib += 2;
                            } else {
                                //Se estiver preenchido, campo � transformado em DATE
                                dataProx = tratamentoTempo(dataProx);

                                //Verifica se a data preenchida � maior que a pr�xima data.
                                if (dataPreenchida > dataProx) {
                                    //Retorna um alerta ao usu�rio. Adiciona +1 a Libera��o.
                                    alertaJquery("Alerta", 'A data preenchida n�o pode ser maior do que a pr�xima data', "alert");
                                    inputDat.val('');
                                    lib += 1;
                                } else {
                                    //Adiciona +2 a libera��o. Hor�rio est� correto.
                                    lib += 2;
                                }
                            }
                        } else {
                            lib += 2;
                        }
                    } else {
                        alertaJquery("Alerta", 'A data preenchida n�o pode ser menor do que data anterior', "alert");
                        inputDat.val('');
                        lib += 1;
                    }
                }
            }
        } else {
            lib += 1;
        }
    } else {
        lib += 1;
    }
});

$('button[name="btnSalvarOs"]').on('click', function (e) {
    if (hasProcess(e)) return;

    if (lib == 11) {
        $('#formTemposTotais').submit();
    } else {
        alertaJquery("Alerta", 'Preencha todos os campos', "alert");
        $('body').removeClass('processing');
        return false;
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    // $('#menuComplementoFixed').fadeOut(500);
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});