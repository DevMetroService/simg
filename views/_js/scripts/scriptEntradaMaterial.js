/**
 * Created by ricardo.diego on 20/01/2017.
 */


/*stdJS*/
//document.getElementById('teste').innerHTML = 'Apenas uma div de teste';



/*CODE*/
var dadosDaRequisicao = {}; //Um objeto que guarda os dados da Requisi??o
var listaMateriais = [];
var contaLinhasTabela = 0;

var tabelaDeMateriais;
$(document).ready(function () {
    tabelaDeMateriais = $('#tabelaListaDeMateriais').dataTable(configTable(0, false, 'desc'));
});

$('#btnCadastrarMateriais').attr('disabled', 'disabled');



/**
 *  EVENTOS
 */

$('#btnAddMaterial').click(function () {

    var resultado = validacao_formulario(
        $('#formPrincipal input'),
        $('#formPrincipal select'),
        $('#formPrincipal textarea'));

    if (resultado['permissao']) {

        addMaterialToList();


        $('#btnCadastrarMateriais').removeAttr('disabled');

    }else{
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
    }

});


$(document).on('click', '.removerLinha', function(e){

    e.preventDefault();


    $(this).closest('tr').fadeOut(350, function () {
        var row = $(this).closest('tr');
        tabelaDeMateriais.fnDeleteRow(row[0]);

        contaLinhasTabela--;
        if (contaLinhasTabela == 0) {
            $('#btnCadastrarMateriais').attr('disabled', 'disabled');
        } else {
            ordenarItensDaTabela();
            $('#btnCadastrarMateriais').removeAttr('disabled');
        }
    });


});


$('#btnCadastrarMateriais').click(function (e) {
    e.preventDefault();


    pegaListaDeMateriais();
    pegaDadosDaRequisicao(listaMateriais);

    alert(JSON.stringify(dadosDaRequisicao));

    $.post(
        caminho + '/cadastroMateriais/vardumppost',
        dadosDaRequisicao,
        function (json) {
            alert(json);
            //$(this).html(json);
        }
    ).error(function () {
        alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
    });

});



/**
 * FUNCOES
 */

function addMaterialToList() {

    var dadosDoForm = {
        descMaterial: $('select[name="descMaterial"] option:selected').html(),
        nameFornecedor: $('select[name="nomeFornecedor"] option:selected').html(),
        qtdSolicitada:$('input[name="qtdSolicitada"]').val(),
        unidade:$('select[name="unidade"] option:selected').html()
    };

    tabelaDeMateriais.fnAddData([
        '',
        dadosDoForm.descMaterial,
        dadosDoForm.nameFornecedor,
        '<input type="text" placeholder="Digite uma quantidade" class="form-control number" value="'+ dadosDoForm.qtdSolicitada+'" style="border: 0px; background-color: transparent; text-align: center;">',
        dadosDoForm.unidade,
        '<button class="removerLinha btn btn-circle btn-danger"><i class="fa fa-close"></i></button>'
    ]);


    // Limpa os campos do Formulario que Adiciona os Materiais
    $('#formPrincipal').each (function(){
        this.reset();
    });

    contaLinhasTabela++;
    ordenarItensDaTabela('#tabelaListaDeMateriais');

}


function ordenarItensDaTabela() {

    var table = $('#tabelaListaDeMateriais');

    var n_item = 0;
    var cols = 0;

    table.find('tr').each(function(indice){
        cols = 0;

        if (n_item > 0) {

            $(this).find('td').each(function(indice){

                if (cols == 0) {
                    $(this).html('' + n_item );
                }

                cols++;
            });
        }

        n_item++;
    });

}


function pegaListaDeMateriais() {

    var itemDaLista;    //Ira armazenar a lista de Materiais da tabela, na forma de um objeto

    var table = $('#tabelaListaDeMateriais');

    table.find('tr').each(function(){
        var cols = 0;

        // Para cada linha percorrida, um novo objeto ? criado para armazenar os dados
        itemDaLista = {
            descMaterial:'',
            fornecedor:'',
            qtdSolicitada:'',
            unidade:''
        };

        // console.log($(this));

        $(this).find('td').each(function(){
            //Percorre as colunas da linha atual
            // console.log($(this));
            var dado;

            switch (cols) {
                case 1:
                    itemDaLista.descMaterial = $(this).html();
                    break;
                case 2:
                    itemDaLista.fornecedor = $(this).html();
                    break;
                case 3:
                    itemDaLista.qtdSolicitada = $(this).find('.colQuantidade').val();
                    break;
                case 4:
                    itemDaLista.unidade = $(this).html();
                    break;
            }

            cols++;
        });

        console.log('Debug - Item da Lista');
        console.log(itemDaLista);

        listaMateriais.push(itemDaLista);

        cols = 0;

    });

    listaMateriais.shift(); //Remove o indice referente ao cabecalho da tabela
    console.log('Debug - Lista de Materiais');
    console.log(listaMateriais);

};


function pegaDadosDaRequisicao(listaMateriais) {
    dadosDaRequisicao = {
        listaMaterial: listaMateriais
    };

    console.log('Dados da Requisi��o: ')
    console.log(dadosDaRequisicao);

}