/**
 * Created by PhpStorm.
 * User: abner.andrade
 * Date: 25/07/2019
 * Time: 11:51
 */

 $(document).ready(function(){

    // Configura a Tabela de  Usuarios Cadastrados
    $(".tableUsuario").DataTable();

    var nome_usuario    = $('#nome_completo');
    var email           = $('#email');
    var nivel_acesso    = $('#nivel_acesso');
    var login_usuario   = $('#login_usuario');
    var permissao       = $('#permissao');
    var validater       = $('#validater');

    var form = $("#formCadUser");


    //Configura o Multiselect Equipes
    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"
    });

    // ===================== Eventos =======================

    /*
    * Para Editar os dados de um usuario
    * */

    var resetPassField = $('#resetPassField');

    $(document).on("click",".editUsuario", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/usuario/update/'+$(this).data('value'));

        resetPassField.show();

        var row = $(this).parent("td").parent("tr").find('td');

        nome_usuario.val(row.data('value'));
        login_usuario.val(row.next().data('value'));
        email.val(row.next().next().data('value'));
        nivel_acesso.val(row.next().next().next().data('value'));

        if(row.next().next().next().next().data('value') == 's')
        {
            permissao.prop("checked", true);
        }else
        {
            permissao.prop('checked', false);
        }

        if($(this).data('validaters') == 's')
        {
            validater.prop('checked', true);
        }else
        {
            validater.prop('checked', false);
        }

        //Pega os codigos un_equipe
        var un_equipes = $(this).data('un_equipes');

        //Percorre cada codigo un_equipe e vai selecionando o checkbox correspondente
        un_equipes.map(function (cod_un_equipe) {

            $('input[type="checkbox"]').each(function () {
                console.log($(this).attr("id"));
                if ( $(this)[0].value == cod_un_equipe && $(this).attr("id") != "permissao") {
                    $(this).click();
                }
            });
        });

        $("html, body").animate({scrollTop : 0},700);
    });

    /*
    * Desbloqueia o campo Equipe caso
    * nivel acesso Equipe Manutencao
    * seja selecionado.
    * */

    var nivel_equipe = ['5.2', '5.1', '5'];

    nivel_acesso.on("change", function () {

        var cod_nivel = $(this).val();
        var equipeSelect =$(".multiselect");

        if (nivel_equipe.includes(cod_nivel)) { //nivel Equipe Manutencao
            equipeSelect.attr('disabled', false);
            equipeSelect.removeClass('disabled');
            equipeSelect.attr('required', true);
        }
        else {
            $('input[type="checkbox"]').each(function () {
                if ( $(this)[0].checked == true ) {
                    $(this).click(); //desmarca o checkbox
                }
            });
            equipeSelect.attr('disabled', true);
            equipeSelect.addClass('disabled');
            equipeSelect.removeAttr('required');
            equipeSelect.attr('required', false);
        }
    });

    $(document).on("click", ".resetarSenha", function()
    {
        var user = $(this).data('value');

        alertaJquery("Resetar Senha", "Deseja resetar senha para: <strong>admin</strong> ?", "confirm",
        [{value: "Sim"}, {value: "N�o"}],
        function (res) {
            if (res == "Sim") {
                $.get(caminho+"/usuario/resetarSenha/"+user, function(msg){alertaJquery("", msg, "info")});
            }
        });
    });


    /*
    * Limpa os checkboxes do formulario
    * */
    $("#resetBtn").on("click", function (e) {

        form.attr('action', caminho + '/usuario/store');

        $('input[type="checkbox"]').each(function () {
            if ( $(this)[0].checked == true ) {
                $(this).click(); //desmarca o checkbox
            }
        });

        resetPassField.hide();

    });

});