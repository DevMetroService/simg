
var tableSafD;
var tableSsmR;
var tableSsmRep;
var tableSsmP;
var tableOsm;
var tableSsp;
var tableSspP;
var tableOsp;
var tablePmp;

var table;

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tablePmp = $('.tableData').DataTable(configTable(0, false, 'desc'));

        tableSafD  = $("#indicadorSafDevolvida").dataTable(configTable(0, false, 'desc'));

        tableSsmR  = $("#indicadorSsmRecebida").dataTable(configTable(4, false, 'desc'));

        tableSsmRep  = $("#indicadorSsmReprovada").dataTable(configTable(0, false, 'desc'));

        tableSsmP = $("#indicadorSsmPendente").dataTable(configTable(4, false, 'desc'));

        tableOsm = $("#indicadorOsmExecucao").dataTable(configTable(4, false, 'desc'));

        tableSsp = $("#indicadorSspExecucao").dataTable(configTable(4, false, 'desc'));

        tableSspP = $("#indicadorSspPendente").dataTable(configTable(1, false, 'desc'));

        tableOsp = $("#indicadorOspExecucao").dataTable(configTable(0, false, 'desc'));

        tablePmp = $("#indicadorPMP").dataTable(configTable(0, true, 'asc'));

        //$("#indicadorAuditoriaSaf").dataTable(configTable(0, false, 'desc'));
    });
});

//####################### Websocket Conex�o ###########################
//####################### Websocket Conex�o ###########################
//####################### Websocket Conex�o ###########################
$('document').ready(function(){
    try {
        socket.on("functionEquipe", function (data) {
            var nivel = ($('#nivelUserSimg').val() != '5.1' && $('#nivelUserSimg').val() != '5.2');

            // var_dump(data);

            for (var i = 0; i < arrayEquipes.length; i++) {
                if (data.subChannelEquipe == arrayEquipes[i]) {
                    //Inserir linha na tabela dinamicamente
                    switch (data.tabela) {
                        case "Ssm":
                            atualizarTableSsm(data, nivel);
                            break;

                        case "Osm":
                            atualizarTableOsm(data, nivel);
                            break;
                        case "Ssp":
                            atualizarTableSsp(data, nivel);
                            break;

                        case "Osp":
                            atualizarTableOsp(data, nivel);
                            break;
                    }
                    break;
                }
            }
            // Renovar Tooltip
            $(document).tooltip({selector: '[rel="tooltip-wrapper"]'});
        });
    }catch(e){
        console.log(e);
    }
});

function atualizarTableSsm(data, nivel){
    var btn =   '<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Editar/Visualizar"><i class="fa fa-file-o fa-fw"></i></button> '+
                '<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Encaminhar") {
        if (nivel) {
            tableSsmR.fnAddData([
                data.numero,
                data.numeroSaf,
                data.local,
                data.sistema,
                data.avaria,
                data.dataAbertura,
                // data.encaminhamento,
                data.nivel,
                btn
            ]);
        }else{
            tableSsmR.fnAddData([
                data.numero,
                data.numeroSaf,
                data.local,
                data.veiculo,
                data.sistema,
                data.avaria,
                data.dataAbertura,
                // data.encaminhamento,
                data.nivel,
                btn
            ]);
        }

        switch (data.nivel){
            case "A":
                $('#ssmRContadorNivelA').html(parseInt($('#ssmRContadorNivelA').html()) + 1);
                break;
            case "B":
                $('#ssmRContadorNivelB').html(parseInt($('#ssmRContadorNivelB').html()) + 1);
                break;
            default :
                $('#ssmRContadorNivelC').html(parseInt($('#ssmRContadorNivelC').html()) + 1);
        }

        var dataStatus = new Date(data.encaminhamento);
        var dataImpresa = new Date($('#ssmRContadorData').html());

        if(dataStatus < dataImpresa){
            $('#ssmRContadorData').html(data.encaminhamento);
            $('#ssmRContadorCod').html(data.numero);
        }

    }else if(data.tipo == "Alterar"){
        var col;
        if(data.status == "Pendente"){
            col = tableSsmP._('tr');
            for(var i=0; i < col.length ; i++){
                var dataTable = tableSsmP.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    if (nivel) {
                        tableSsmP.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.local,
                                data.servico,
                                data.dataAbertura,
                                // data.encaminhamento,
                                data.nivel,
                                btn
                            ], i
                        );
                    }else{
                        tableSsmP.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.local,
                                data.veiculo,
                                data.servico,
                                data.dataAbertura,
                                // data.encaminhamento,
                                data.nivel,
                                btn
                            ], i
                        );
                    }
                    break;
                }
            }
        }else{
            col = tableSsmR._('tr');
            for(var i=0; i < col.length ; i++){
                var dataTable = tableSsmR.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    if (nivel) {
                        tableSsmR.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.local,
                                data.sistema,
                                data.avaria,
                                data.dataAbertura,
                                // data.encaminhamento,
                                data.nivel,
                                btn
                            ], i
                        );
                    }else{
                        tableSsmR.fnUpdate(
                            [
                                data.numero,
                                data.numeroSaf,
                                data.local,
                                data.veiculo,
                                data.sistema,
                                data.avaria,
                                data.dataAbertura,
                                // data.encaminhamento,
                                data.nivel,
                                btn
                            ], i
                        );
                    }
                    break;
                }
            }
        }
    }else if(data.tipo == "Devolver" || data.tipo == "Cancelar"){
        var col;
        if(data.statusAntigo == "Pendente"){
            col = tableSsmP._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSsmP.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    tableSsmP.fnDeleteRow(i);

                    switch (data.nivel){
                        case "A":
                            $('#ssmPContadorNivelA').html(parseInt($('#ssmPContadorNivelA').html()) - 1);
                            break;
                        case "B":
                            $('#ssmPContadorNivelB').html(parseInt($('#ssmPContadorNivelB').html()) - 1);
                            break;
                        default :
                            $('#ssmPContadorNivelC').html(parseInt($('#ssmPContadorNivelC').html()) - 1);
                    }

                    break;
                }
            }
        }else{
            col = tableSsmR._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSsmR.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    tableSsmR.fnDeleteRow(i);

                    switch (data.nivel){
                        case "A":
                            $('#ssmRContadorNivelA').html(parseInt($('#ssmRContadorNivelA').html()) - 1);
                            break;
                        case "B":
                            $('#ssmRContadorNivelB').html(parseInt($('#ssmRContadorNivelB').html()) - 1);
                            break;
                        default :
                            $('#ssmRContadorNivelC').html(parseInt($('#ssmRContadorNivelC').html()) - 1);
                    }
                    break;
                }
            }
        }
    }
}

function atualizarTableOsm(data, nivel){
    var btn =   '<button class="btn btn-default btn-circle btnEditarOsm" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> '+
                '<button class="btn btn-default btn-circle btnImprimirOsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Aberta"){
        var col;
        if(data.statusAntigo == "Pendente"){
            col = tableSsmP._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSsmP.fnGetData(i);
                if (dataTable[0] == data.numeroSsm) {
                    tableSsmP.fnDeleteRow(i);

                    switch (data.nivel){
                        case "A":
                            $('#ssmPContadorNivelA').html(parseInt($('#ssmPContadorNivelA').html()) - 1);
                            break;
                        case "B":
                            $('#ssmPContadorNivelB').html(parseInt($('#ssmPContadorNivelB').html()) - 1);
                            break;
                        default :
                            $('#ssmPContadorNivelC').html(parseInt($('#ssmPContadorNivelC').html()) - 1);
                    }

                    break;
                }
            }
        }else{
            col = tableSsmR._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSsmR.fnGetData(i);
                if (dataTable[0] == data.numeroSsm) {
                    tableSsmR.fnDeleteRow(i);

                    switch (data.nivel){
                        case "A":
                            $('#ssmRContadorNivelA').html(parseInt($('#ssmRContadorNivelA').html()) - 1);
                            break;
                        case "B":
                            $('#ssmRContadorNivelB').html(parseInt($('#ssmRContadorNivelB').html()) - 1);
                            break;
                        default :
                            $('#ssmRContadorNivelC').html(parseInt($('#ssmRContadorNivelC').html()) - 1);
                    }

                    break;
                }
            }
        }
        if (nivel) {
            tableOsm.fnAddData([
                data.numero,
                data.numeroSsm,
                data.dataAbertura,
                data.equipe,
                data.local,
                data.nivel,
                btn
            ]);
        }else{
            tableOsm.fnAddData([
                data.numero,
                data.numeroSsm,
                data.dataAbertura,
                data.veiculo,
                data.equipe,
                data.local,
                data.nivel,
                btn
            ]);
        }
    }else if(data.tipo == "Deletar"){
        var col = tableOsm._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableOsm.fnGetData(i);
            if (dataTable[0] == data.numero) {
                tableOsm.fnDeleteRow(i);
                break;
            }
        }
        if(data.ssmPendente){
            btn =   '<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Editar/Visualizar"><i class="fa fa-file-o fa-fw"></i></button> '+
                    '<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';
            if (nivel) {
                tableSsmP.fnAddData([
                    data.ssmPendenteNumero,
                    data.ssmPendenteNumeroSaf,
                    data.ssmPendenteLocal,
                    data.ssmPendenteServico,
                    data.ssmPendenteDataAbertura,
                    // data.ssmPendenteEncaminhamento,
                    data.ssmPendenteNivel,
                    btn
                ]);
            }else{
                tableSsmP.fnAddData([
                    data.ssmPendenteNumero,
                    data.ssmPendenteNumeroSaf,
                    data.ssmPendenteLocal,
                    data.ssmPendenteVeiculo,
                    data.ssmPendenteServico,
                    data.ssmPendenteDataAbertura,
                    // data.ssmPendenteEncaminhamento,
                    data.ssmPendenteNivel,
                    btn
                ]);
            }

            switch (data.ssmPendenteNivel){
                case "A":
                    $('#ssmPContadorNivelA').html(parseInt($('#ssmPContadorNivelA').html()) + 1);
                    break;
                case "B":
                    $('#ssmPContadorNivelB').html(parseInt($('#ssmPContadorNivelB').html()) + 1);
                    break;
                default :
                    $('#ssmPContadorNivelC').html(parseInt($('#ssmPContadorNivelC').html()) + 1);
            }

            var dataStatus = new Date(data.ssmPendenteEncaminhamento);
            var dataImpresa = new Date($('#ssmPContadorData').html());

            if(dataStatus < dataImpresa){
                $('#ssmPContadorData').html(data.ssmPendenteEncaminhamento);
                $('#ssmPContadorCod').html(data.ssmPendenteNumero);
            }
        }
    }
}


function atualizarTableSsp(data, nivel){
    var btn =   '<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> '+
                '<button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Inserir"){
        if (nivel) {
            tableSsp.fnAddData([
                data.numero,
                data.servico,
                data.dataProgramada,
                data.qtdDias,
                btn
            ]);
        }else{
            tableSsp.fnAddData([
                data.numero,
                data.veiculo,
                data.servico,
                data.dataProgramada,
                data.qtdDias,
                btn
            ]);
        }
/*
        var dataStatus = new Date(data.dataProgramada);
        var dataImpresa = new Date($('#sspContadorData').html());

        if(dataStatus < dataImpresa){
            $('#sspContadorData').html(data.dataProgramada);
            $('#sspContadorCod').html(data.numero);
        }
*/
    }else if(data.tipo == "Alterar"){
        var col;
        if(data.status == "Pendente"){
            col = tableSspP._('tr');
            for(var i=0; i < col.length ; i++){
                var dataTable = tableSspP.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    if (nivel) {
                        tableSspP.fnUpdate(
                            [
                                data.numero,
                                data.servico,
                                data.dataProgramada,
                                data.qtdDias,
                                btn
                            ], i
                        );
                    }else{
                        tableSspP.fnUpdate(
                            [
                                data.numero,
                                data.veiculo,
                                data.servico,
                                data.dataProgramada,
                                data.qtdDias,
                                btn
                            ], i
                        );
                    }
                    break;
                }
            }
        }else{
            col = tableSsp._('tr');
            for(var i=0; i < col.length ; i++){
                var dataTable = tableSsp.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    if (nivel) {
                        tableSsp.fnUpdate(
                            [
                                data.numero,
                                data.servico,
                                data.dataProgramada,
                                data.qtdDias,
                                btn
                            ], i
                        );
                    }else{
                        tableSsp.fnUpdate(
                            [
                                data.numero,
                                data.veiculo,
                                data.servico,
                                data.dataProgramada,
                                data.qtdDias,
                                btn
                            ], i
                        );
                    }
                    break;
                }
            }
        }
    }else if(data.tipo == "Cancelar"){
        if(data.statusAntigo == "Pendente"){
            col = tableSspP._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSspP.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    tableSspP.fnDeleteRow(i);
                    break;
                }
            }
        }else{
            col = tableSsp._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSsp.fnGetData(i);
                if (dataTable[0] == data.numero) {
                    tableSsp.fnDeleteRow(i);
                    break;
                }
            }
        }
    }
}

function atualizarTableOsp(data, nivel){
    var btn = '<button class="btn btn-default btn-circle btnEditarOsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> '+
        '<button class="btn btn-default btn-circle btnImprimirOsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Aberta"){
        if(data.statusAntigo == "Pendente"){
            col = tableSspP._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSspP.fnGetData(i);
                if (dataTable[0] == data.numeroSsp) {
                    tableSspP.fnDeleteRow(i);
                    break;
                }
            }
        }else{
            col = tableSsp._('tr');
            for (var i = 0; i < col.length; i++) {
                var dataTable = tableSsp.fnGetData(i);
                if (dataTable[0] == data.numeroSsp) {
                    tableSsp.fnDeleteRow(i);
                    break;
                }
            }
        }
        if (nivel) {
            tableOsp.fnAddData([
                data.numero,
                data.numeroSsp,
                data.dataAbertura,
                data.equipe,
                data.local,
                btn
            ]);
        }else{
            tableOsp.fnAddData([
                data.numero,
                data.numeroSsp,
                data.dataAbertura,
                data.veiculo,
                data.equipe,
                data.local,
                btn
            ]);
        }
    }else if(data.tipo == "Deletar"){
        var col = tableOsp._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableOsp.fnGetData(i);
            if (dataTable[0] == data.numero) {
                tableOsp.fnDeleteRow(i);
                break;
            }
        }
        if(data.sspPendente){
            btn = '<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> '+
                '<button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

            if (nivel) {
                tableSspP.fnAddData([
                    data.sspPendenteNumero,
                    data.sspPendenteServico,
                    data.sspPendenteDataProgramada,
                    data.sspPendenteQtdDias,
                    btn
                ]);
            }else{
                tableSspP.fnAddData([
                    data.sspPendenteNumero,
                    data.sspPendenteVeiculo,
                    data.sspPendenteServico,
                    data.sspPendenteDataProgramada,
                    data.sspPendenteQtdDias,
                    btn
                ]);
            }

            /*
             var dataStatus = new Date(data.dataProgramada);
             var dataImpresa = new Date($('#sspContadorData').html());

             if(dataStatus < dataImpresa){
             $('#sspContadorData').html(data.dataProgramada);
             $('#sspContadorCod').html(data.numero);
             }
             */
        }
    }
}
