/**
 * Created by ricardo.diego on 22/02/2017.
 */


//  ############## Layout da MSG de alerta Status do Update ################
$(document).ready(function () {
    console.log();
    var msg = $('#msgUpdate').val();
    if (msg != '') {
        alertaJquery('Status da Atualiza��o', msg);
        $('#msgUpdate').val('');
    }
});


//  ############## Layout da Tabela ################
var tabela;
$(document).ready(function () {
    tabela = $('#tabelaMateriais').dataTable(configTable(0, false, 'desc'));
});


//  ################ Configuracoes do MODAL - EDITAR MATERIAL ###################
document.getElementById('btn-modalSalvar').style.marginRight = "20px";


//############# AutoComplete DataList #############
if ($('input[name="pesquisaMaterialInput"]').val() != "") {
    dataListFuncao($('input[name="pesquisaMaterialInput"]'), $("#pesquisaMaterialDataList"), $('input[name="pesquisaMaterial"]'));
}

$('input[name="pesquisaMaterialInput"]').on("input", function () {
    dataListFuncao($('input[name="pesquisaMaterialInput"]'), $("#pesquisaMaterialDataList"), $('input[name="pesquisaMaterial"]'));
});

$('input[name="pesquisaMaterialInput"]').on("blur", function () {
    if ($('input[name="pesquisaMaterial"]').val() == "" || $(this).val() == "") {
        $(this).val("");

        $('input[name="pesquisaMaterial"]').val("");
    }

    else
        $('input[name="pesquisaMaterialInput"]').val($("#pesquisaMaterialDataList").find("option[value='" + $('input[name="pesquisaMaterial"]').val() + "']").text());
});


// ################### CODIGOS DO Modal Editar Material ####################

$('#btn-modalSalvar').on('click', function () {
    $('#formEditarMaterial').submit();
});


// ################### CODIGOS DA TELA ####################

$('#formPesquisaMaterial').each(function () {
    this.reset();
});

$('#btnPesquisar').on('click', function () {
    $('#formPesquisaMaterial').submit();
});


$(document).on('change', '#categoria', function () {
    apiCategoriasMaterial($(this).val(), $('#pesquisaMaterialDataList'));
});


$(document).on('click', '.btnEditarMaterial', function (e) {

    $('#modal_editarMaterial').modal('show');

    var cod_material = '';

    var linhaSelecionada = $(this).parents('tr');

    var table = $('#tabelaMateriais');
    table.find(linhaSelecionada).each(function () {
        var cols = 0;

        //Percorre as colunas da linha atual
        $(this).find('td').each(function () {

            if (cols == 0) {
                cod_material = $(this).html();
            }

            cols++;
        });

        cols = 0;
    });

    //alert("O codigo do material �: \n" + cod_material);

    apiTodosDadosMaterial(cod_material);

    //$('input[name="modal_codigoMaterial"]').val(dadosRetornados['cod_material']);

});


