$('#odometroInfo').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) ;// Button that triggered the modal
    var recipient = button.data('info'); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
       var modal = $(this);

       modal.find('#responsavel').text(recipient['responsavel']);
       modal.find('#dataRegistro').text(recipient['data']);
});

var mesHistorico = $('#mesHistorico');


// var bodyTable = $('#bodyHistoricoInfo');
var bodyTable = $("#table").DataTable( {
    "searching": false,
    "info":false,
    "order":false,
    "language": {
        "lengthMenu": "Visualidado _MENU_ registros por p�gina",
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",

        "paginate": {
            "first": "Primeiro",
            "last": "Ultimo",
            "next": "Pr�ximo",
            "previous": "Anterior"
        },
        "loadingRecords": "Carregando...",
        "infoEmpty": "N�o h� informa��es"
    }
} );
var recipient;

$('#historicoInfo').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) ;// Button that triggered the modal
    recipient = button.data('info'); // Extract info from data-* attributes

    mesHistorico.val(parseInt(recipient.mes));

    apiHistoricoOdometro(recipient, bodyTable);
});

$('#mesHistorico').change(function(){
    recipient.mes = $(this).val();

    alert(recipient);

    apiHistoricoOdometro(recipient, bodyTable);
});

var campoOdometro = $('.inputOdometro');

campoOdometro.blur(function(){
    var placeholder = $(this).attr('placeholder');

    var campo = $(this);

    if(campo.val() < placeholder && campo.val() != ""){
        alertaJquery("Valor menor que o anterior.", "Deseja prosseguir assim mesmo?", 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "Sim") {
            }
            if (res == "N�o") {
                campo.val("");
            }
        });
    }
});