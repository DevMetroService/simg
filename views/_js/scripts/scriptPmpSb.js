/**
 * Created by josue.santos on 10/10/2016.
 */

var tabelaPmpSb;
var tabelaPmpSbExec;

$.getScript(caminho + "/views/_js/scripts/scriptPmp.js", function(){ // include functions
    $(document).ready(function(){
        tabelaPmpSb  = $("#tabelaPmpSb").dataTable(configTable(0, false, 'asc'));

        tabelaPmpSbExec = $("#tabelaPmpSbExec").dataTable(configTable(0, false, 'asc'));
    });
});

// MultiSelect Local
$('document').ready(function () {
    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"

    });
});

//***********************************Proprios do formularios

$('select[name="linha"]').on("change", function(){
    apiLinhaLocal($(this), $('select[name="local"]'));
});

//***********************************Proprios do formularios

var excluidosLinhasPmp = new Array();
var contador = 1;

//=================Come�o On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {

    var sistema         = $('select[name="sistema"] option:selected');
    var subSistema      = $('select[name="subSistema"] option:selected');
    var servicoPmp      = $('select[name="servicoPmp"] option:selected');
    var periodicidade   = $('select[name="periodicidade"] option:selected');
    var turno           = $('select[name="turno"] option:selected');
    var procedimento    = $('input[name="procedimento"]');
    var qzn             = $('input[name="quinzena"]');
    var local           = $('select[name="local[]"]');
    var maoObra         = $('input[name="maoObra"]');
    var horasUteis      = $('input[name="horasUteis"]');
    var homemHora       = $('input[name="homemHora"]');

    if (sistema.val() == ''         || sistema.val() == null        || sistema.val() == undefined ||
        subSistema.val() == ''      || subSistema.val() == null     || subSistema.val() == undefined ||
        servicoPmp.val() == ''      || servicoPmp.val() == null     || servicoPmp.val() == undefined ||
        periodicidade.val() == ''   || periodicidade.val() == null  || periodicidade.val() == undefined ||
        turno.val() == ''           || turno.val() == null          || turno.val() == undefined ||
        qzn.val() == ''             || qzn.val() == null            || qzn.val() == undefined ||
        local.val() == ''           || local.val() == null          || local.val() == undefined ||
        maoObra.val() == ''         || maoObra.val() == null        || maoObra.val() == undefined ||
        horasUteis.val() == ''      || horasUteis.val() == null     || horasUteis.val() == undefined) {

        alertaJquery("Preencha todos os campos corretamente", "Todos os campos precisam ser preenchidos.", "alert");

    } else {
        //================ Dados a serem adicionados na LISTA de PMP Edifica��o. ==========================//
        var txt = '';

        $('select[name="local[]"] option:selected').each(function () {
            var duplicado = true;
            var col = tabelaPmpSbExec._('tr');

            //Tabela do Banco de Dados
            for (var i = 0; i < col.length; i++) {
                var dataTable = tabelaPmpSbExec.fnGetData(i);

                if ((dataTable[1] == sistema.text()) &&
                    (dataTable[2] == subSistema.text()) &&
                    (dataTable[3] == servicoPmp.text()) &&
                    (dataTable[4] == periodicidade.text()) &&
                    (dataTable[7] ==  $(this).text())) {

                    txt = txt + ' - cod - ' + dataTable[0].split("<input")[0] + " Duplicado no Banco de Dados.\n";
                    duplicado = false;
                    break;
                }
            }

            col = tabelaPmpSb._('tr');

            //Tabela de Lista

            for (var i = 0; i < col.length; i++) {

                var dataTable = tabelaPmpSb.fnGetData(i);

                if ((dataTable[1].split("<input")[0] == sistema.text()) &&
                    (dataTable[2].split("<input")[0] == subSistema.text()) &&
                    (dataTable[3].split("<input")[0] == servicoPmp.text()) &&
                    (dataTable[4].split("<input")[0] == periodicidade.text()) &&
                    (dataTable[7].split("<input")[0] ==  $(this).text())) {

                    txt = txt + ' - id - ' + dataTable[0] + " Duplicado na Lista.\n";
                    duplicado = false;
                    break;
                }
            }
            if (duplicado) {
                var disponivel = excluidosLinhasPmp.pop();

                if (disponivel == undefined)
                    var ctdAux = contador;
                else
                    var ctdAux = disponivel;

                //==========Turno==========//
                if (turno.val() == 'D') {
                    var turnoHTML =
                        '<select name="turno' + ctdAux + '" class="form-control" required>' +
                        '<option value="D" selected>Diurno</option>' +
                        '<option value="N">Noturno</option>' +
                        '</select>';
                } else {
                    var turnoHTML =
                        '<select name="turno' + ctdAux + '" class="form-control" required>' +
                        '<option value="N" selected>Noturno</option>' +
                        '<option value="D">Diurno</option>' +
                        '</select>';
                }

                tabelaPmpSb.fnAddData([
                    ctdAux,
                    //==========Sistema==========//
                    sistema.text() + '<input type="hidden" name="sistema' + ctdAux + '" value="' + sistema.val() + '" class="form-control number" readonly>' +
                    '<input type="hidden" name="grupo' + ctdAux + '" value="25" class="form-control number" readonly>',
                    //==========SubSistema==========//
                    subSistema.text() + '<input type="hidden" name="subSistema' + ctdAux + '" value="' + subSistema.val() + '" class="form-control number" readonly>',
                    //==========Servi�o==========//
                    servicoPmp.text() + '<input type="hidden" name="servicoPmp' + ctdAux + '" value="' + servicoPmp.val() + '" class="form-control number" readonly>',
                    //==========Periodicidade==========//
                    periodicidade.text() + '<input type="hidden" name="periodicidade' + ctdAux + '" value="' + periodicidade.val() + '" class="form-control number" readonly>',
                    //==========Procedimento==========//
                    procedimento.val(),
                    //==========Qzn In�cio==========//
                    '<input type="text" name="quinzena' + ctdAux + '" value="' + qzn.val() + '" class="form-control number">',
                    //==========Local==========//
                    $(this).text() + '<input type="hidden" name="local' + ctdAux + '" value="' +  $(this).val() + '" class="form-control" readonly>',
                    //==========Turno==========//
                    turnoHTML,
                    //==========M�o de Obra==========//
                    '<input name="maoObra' + ctdAux + '" type="text"  value="' + maoObra.val() + '" class="form-control number maoObra" required>',
                    //==========Horas �teis==========//
                    '<input name="horasUteis' + ctdAux + '" type="text"  value="' + horasUteis.val() + '" class="form-control number horasUteis" required>',
                    //==========Homem Hora==========//
                    '<input name="homemHora' + ctdAux + '" type="text"  value="' + homemHora.val() + '" class="form-control" readonly>',
                    //==========A��es==========//
                    '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
                ]);

                contador = contador + 1;
            }
        });
        if (txt != '')
            alertaJquery("Alerta", "DUPLICADOS:\n\n <textarea rows='20' cols='30'>" + txt + "</textarea>", "alert");
    }
});
//=================Fim do On Click Plus==================//

var contadorExec = $("input[name='ctd']").val();

//=================================================//

$("#tabelaPmpSb").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpSb = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaPmpSb._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaPmpSb.fnGetData(i);
        if (dataTable[0] == idPmpSb) {
            tabelaPmpSb.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmp.push(idPmpSb);
    contador = contador - 1;
});

$('.salvarListaPmp').click(function () {
    var data = tabelaPmpSb.$('input, select').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaPmpSbExec.$('input, select').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + contadorExec;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    $.post(
        caminho + "/cadastroGeral/refillPmp",
        {
            sistema:        $('select[name="sistema"] option:selected').val(),
            subSistema:     $('select[name="subSistema"] option:selected').val(),
            servicoPmp:     $('select[name="servicoPmp"] option:selected').val(),
            periodicidade:  $('select[name="periodicidade"] option:selected').val(),
            procedimento:   $('input[name="procedimento"]').val(),
            quinzena:       $('input[name="quinzena"]').val(),
            linha:          $('select[name="linha"] option:selected').val(),
            local:          $('select[name="local"] option:selected').val(),
            turno:          $('select[name="turno"] option:selected').val(),
            maoObra:        $('input[name="maoObra"]').val(),
            horasUteis:     $('input[name="horasUteis"]').val(),
            homemHora:      $('input[name="homemHora"]').val()
        }
    );

    if (data) {
        $.post(
            caminho + "/cadastroGeral/salvarListaPmp",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }

});
var ano = $('#pmpAno');
var ativo = $('#pmpAtivo');
var novo = $('#pmpNovo');

ano.change(function(){
    var d = new Date();
    var n = d.getFullYear();

    if(n == $(this).val())
    {
        ativo.prop('disabled', 'disabled');
        ativo.prop('checked', false);
    }else
    {
        ativo.removeAttr('disabled');
    }
});

$('.gerarPmpAnual').click(function () {
    var year = ano.val();
    var isAtivo = ativo.prop('checked');
    var isNovo = novo.prop('checked');

    alertaJquery('Gerar PMP Anual',
        'Tem certeza que deseja finalizar o PMP Anual?<h4>Verifique antes se todas as altera��es foram salvas.</h4>',
        'alert', [{value:"Sim"},{value:"N�o"}], function (res) {
            if(res == "Sim") {
                alertaJquery('Gerar PMP Anual',
                    '<h3>Esta a��o n�o poder� ser desfeita!</h3>Deseja continuar?</br><small>O Processo poder� levar alguns minutos. Por favor, Aguarde.</small>',
                    'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "Sim") {
                            loader();
                            window.location.replace(caminho + "/pmp/gerarPmpAnual/25/"+year+"/"+isAtivo+"/"+isNovo);
                        }
                        if(res == "N�o")
                        {
                            $('#gerarCronograma').modal('show');
                        }
                    }
                );
            }
            if(res == "N�o")
            {
                $('#gerarCronograma').modal('show');
            }
        }
    );
});

$('[data-toggle="tooltip"]').tooltip(
    {
        html: true
    }
);