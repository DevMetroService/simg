$(document).ready(function() {
    $("#resultadoPesquisaOsp").dataTable(configTable(4, true, 'asc'));
});

//Fun��o para DataList
$('#materiaisPesquisa').on("input", function () {
    dataListFuncao2($('#materiaisPesquisa'), $("#materialDataList"), $("#materiaisPesquisaOsp"));
});

$('#maquinasPesquisa').on("input", function () {
    dataListFuncao2($('#maquinasPesquisa'), $("#maquinaDataList"), $('#maquinasEquipamentosPesquisaOsp'));
});

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaOsp"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == ""){
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaOsp"]'));
    }else{
        linha.val($(this).val());
        $('select[name="pnPesquisaOsp"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaOsp"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaOsp"]').on("change", function() {
    var trecho = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="linhaPesquisaOsp"]').val() == ""){
            trecho.val("*");
            apiTrechoPn(trecho,$('select[name="pnPesquisaOsp"]'));
        }else{
            $('select[name="pnPesquisaOsp"]').html('<option value="">Ponto Not�vel</option>');
        }
    }else{
        trecho.val($(this).val());
        apiTrechoPn(trecho,$('select[name="pnPesquisaOsp"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

var grupo = $('#grupoPesquisaOsp');

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
grupo.change(function(){

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        $('.mtRod').show();
    } else {
        clearMtRod();
        $('.mtRod').hide();
    }

    var grupo = ObjetoVal;
    if ($(this).val() == ""){
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    }else{
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaOsp"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisaOsp"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisaOsp"]'));
});

if(grupo.val()  == '22' || grupo.val() == '23' || grupo.val() == '26'){
    $('.mtRod').show();
} else {
    clearMtRod();
    $('.mtRod').hide();
}

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisaOsp"]').on('change', function(){
    var sistema = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="grupoPesquisaOsp"]').val() == ""){
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        }else{
            $('select[name="subSistemaPesquisa"]').html('<option value="">Subsistema</option>');
        }
    }else{
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="unidadePesquisaOsp"]').on("change", function(){
    var local = ObjetoVal;
    if($(this).val() == ""){
        local.val("*");
    }else{
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisaOsp"]'));
});

//############### A��es ###############
//############### A��es ###############
//############### A��es ###############

$('.btnPesquisar').on('click', function(){
    if($('input[name="dataPartirOspAbertura"]').val() == '' && $('input[name="dataPartirOspEncerramento"]').val() == '' && $('input[name="numeroPesquisaOsp"]').val() == '' && $('input[name="codigoSsp"]').val() == ''){
        alertaJquery("Sugest�o", "A pesquisa solicitada ir� necessitar de alguns minutos para que seja realizada.<h4> Deseja acrescentar um intervalo de Data para facilitar a consulta?</h4>", "confirm", [{value:"Sim"},{value:"N�o"}], function (res) {
            if(res == "N�o"){
                $("#pesquisa").submit();
            }
        });
    }else{
        $("#pesquisa").submit();
    }
});

$('.btnResetarPesquisa').on('click', function(){
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaOsp";
});

$(".btnEditarOsp").on("click", function(){

    var dados =$(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    $.ajax({                                                             //inicia o ajax
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsp",                      //informa o caminho do processo
        data: { codigoOs: dados},                                       //informa os dados ao ajax
        success: function() {                                            //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardGeral/Osp");
            // window.open(caminho + "/dashboardGeral/Osp", "_blank");
        }
    });
    return false;
});

$(".btnImprimirOsp").on("click", function(){

    var dados =$(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    $.ajax({                                                             //inicia o ajax
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirOsp",                    //informa o caminho do processo
        data: { codigoOs: dados},                                       //informa os dados ao ajax
        success: function() {
            window.open(caminho + "/dashboardGeral/printOsp", "_blank");  //em caso de sucesso imprime o retorno
        }
    });
    return false;
});

$(".btnAbrirPesquisaSsp").on("click", function(){
    var codigoSsp = $(this).parent("td").parent("tr").find('td').next('td').find('span').html();
    window.location.href = caminho + "/dashboardGeral/pesquisaOspParaSsp/" + codigoSsp;
});


function clearMtRod() {
    $('select[name="veiculoPesquisaOsp"]').val('');
    $('select[name="carroAvariadoPesquisaOsp"]').val('');
    $('select[name="carroLiderPesquisaOsp"]').val('');
    $('select[name="prefixoPesquisaOsp"]').val('');
    $('input[name="odometroPartirPesquisaOsp"]').val('');
    $('input[name="odometroAtePesquisaOsp"]').val('');
}