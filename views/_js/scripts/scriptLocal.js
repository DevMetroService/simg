$(document).ready(function(){
    $("#localTable").DataTable(configTable(0, false, 'asc'));

    var linha = $('#linha');
    var estacao = $('#estacao');
    var grupo = $('#grupo');
    var local = $('#local');

    var form = $("#formulario");

    $(document).on("click",".editLocal", function(e){
        e.preventDefault();
        $("#resetBtn").click();

        form.attr('action', caminho + '/local/update/'+$(this).data('codigo'));

        var row = $(this).parent("td").parent("tr").find('td').next();

        linha.val(row.data('linha'));
        
        row = row.next();

        apiLinhaEstacao(linha, estacao, row.data('estacao'));

        row = row.next();

        grupo.val(row.html());

        local.val(row.next().html());

        $("html, body").animate({scrollTop : 0}, 700);
    });

    linha.change(function(){
        apiLinhaEstacao(linha, estacao);
    });

    $("#resetBtn").on("click", function (e) {
        form.attr('action', caminho + '/local/store');
        estacao.html("<option value=''>Selecione linha para filtrar</option>");
    });

});