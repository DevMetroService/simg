
var tableSsp;
var tableOsp;

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tableSsp  = $("#indicadorSsp").dataTable(configTable(0, false, 'desc'));

        tableOsp  = $("#indicadorOspExecucao").dataTable(configTable(0, false, 'desc'));
    });
});

$(".btnAbrirSspCalendario").on("click", function(){
    var dados = $('input[name="codigoSspCallendar"]').val();                                            //dados recebe o formulario em string
    $.ajax({                                                               //inicia o ajax
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsp",                      //informa o caminho do processo
        data: { codigoSsp: dados},                                         //informa os dados ao ajax
        success: function() {                                          //em caso de sucesso imprime o retorno
           window.location.replace(caminho+"/dashboardGeral/Ssp");
        }
    });
    return false;
});

$(".btnAbrirOspCalendario").on("click", function(){
    var codigoSsp = $('input[name="codigoSspCallendar"]').val();                                            //dados recebe o formulario em string

    window.location.replace(caminho + "/dashboardGeral/pesquisaSspParaOsp/" + codigoSsp);
    return false;
});

function mesIntParaString(mes) {
    if(mes < 10){
        return "0" + mes;
    }else{
        return mes;
    }
}

$(document).ready(function() {
    var calend = new Array();
    var mesAno = new Date();

    var mes = mesAno.getMonth() + 1;
    var ano = mesAno.getFullYear();

    if(mes == 12){
        proxMes = 1;
        proxAno = ano + 1;

        antMes = 11;
        antAno = ano;
    }else if(mes == 1){
        proxMes = 2;
        proxAno = ano;

        antMes = 12;
        antAno = ano - 1;
    }else{
        proxMes = mes + 1;
        proxAno = ano;

        antMes = mes - 1;
        antAno = ano;
    }

    calend.push(mesIntParaString(antMes) + '-' + antAno);
    calend.push(mesIntParaString(mes) + '-' + ano);
    calend.push(mesIntParaString(proxMes) + '-' + proxAno);

    var calendario = $('#calendarioProgramacaoCCM').fullCalendar({
        events: caminho+'/api/parameterResult/3-meses/getSspCalendario',
        customButtons: {
            myButtonToday: {
                text: 'Hoje',
                click: function() {
                    calendario.fullCalendar('today');
                    var data = calendario.fullCalendar('getDate').format('MM-YYYY');

                    if(calend.indexOf(data) == -1){
                        calendario.fullCalendar('addEventSource', caminho+'/api/parameterResult/'+data+'/getSspCalendario');
                        calend.push(data);
                    }
                }
            },
            myButtonPrev: {
                text: 'Anterior',
                click: function() {
                    calendario.fullCalendar('prev');
                    var data = calendario.fullCalendar('getDate').format('MM-YYYY');

                    if(calend.indexOf(data) == -1){
                        calendario.fullCalendar('addEventSource', caminho+'/api/parameterResult/'+data+'/getSspCalendario');
                        calend.push(data);
                    }
                }
            },
            myButtonNext: {
                text: 'Pr�ximo',
                click: function() {
                    calendario.fullCalendar('next');
                    var data = calendario.fullCalendar('getDate').format('MM-YYYY');

                    if(calend.indexOf(data) == -1){
                        calendario.fullCalendar('addEventSource', caminho+'/api/parameterResult/'+data+'/getSspCalendario');
                        calend.push(data);
                    }
                }
            }
        },
        header: {
            left: 'title',
            center: 'listDay',
            right: 'myButtonPrev myButtonNext myButtonToday'
        },
        lang: 'pt-br',
        eventLimit: true,
        eventClick:  function(event, jsEvent, view) {
            $('#modalTitleCalendarioProgramacaoCCM').html(event.title);
            $('#modalBodyCalendarioProgramacaoCCM').html(event.description);
            $('#modalCalendarioProgramacaoCCM').modal();
        },
        loading: function (bool) {
            if(bool)
                $("#loading").fadeIn();
            else
                $("#loading").fadeOut();
        }
    });
});

//####################### Websocket Conex�o ###########################
//####################### Websocket Conex�o ###########################
//####################### Websocket Conex�o ###########################
socket.on("functionCcm", function (data) {
    //Inserir linha na tabela dinamicamente
    switch (data.tabela) {
        case "Ssp":
            atualizarTableSsp(data);
            break;

        case "Osp":
            atualizarTableOsp(data);
            break;
    }
    // Renovar Tooltip
    $('[rel="tooltip-wrapper"]').tooltip();
});

function atualizarTableSsp(data) {
    var btn =   '<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> '+
                '<button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Inserir"){
        tableSsp.fnAddData([
            data.numero,
            data.servico,
            data.dataProgramada,
            data.qtdDias,
            data.status,
            btn
        ]);

        var newEvent = new Object();

        newEvent.allDay          = true;
        newEvent.title           = data.title;
        newEvent.description     = data.description;
        newEvent.start           = data.start;
        newEvent.end             = data.end;
        newEvent.backgroundColor = data.backgroundColor;

        $('#calendarioProgramacaoCCM').fullCalendar( 'renderEvent', newEvent );

    }else if(data.tipo == "Alterar"){
        col = tableSsp._('tr');
        for(var i=0; i < col.length ; i++){
            var dataTable = tableSsp.fnGetData(i);
            if (dataTable[0] == data.numero) {
                tableSsp.fnUpdate(
                    [
                        data.numero,
                        data.servico,
                        data.dataProgramada,
                        data.qtdDias,
                        data.status,
                        btn
                    ], i
                );
                break;
            }
        }

    }else if(data.tipo == "Cancelar"){
        var col = tableSsp._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableSsp.fnGetData(i);
            if (dataTable[0] == data.numero) {
                tableSsp.fnDeleteRow(i);
                break;
            }
        }
    }
}

function atualizarTableOsp(data) {
    var btn =   '<button class="btn btn-default btn-circle btnEditarOsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> '+
                '<button class="btn btn-default btn-circle btnImprimirOsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ';

    if(data.tipo == "Aberta"){
        var col = tableSsp._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableSsp.fnGetData(i);
            if (dataTable[0] == data.numeroSsp) {
                tableSsp.fnDeleteRow(i);
                break;
            }
        }

        tableOsp.fnAddData([
            data.numero,
            data.numeroSsp,
            data.dataAbertura,
            data.equipe,
            data.local,
            btn
        ]);

    }else if(data.tipo == "Deletar"){
        var col = tableOsp._('tr');
        for (var i = 0; i < col.length; i++) {
            var dataTable = tableOsp.fnGetData(i);
            if (dataTable[0] == data.numero) {
                tableOsp.fnDeleteRow(i);
                break;
            }
        }

        if(data.sspPendente){
            tableSsp.fnAddData([
                data.sspPendenteNumero,
                data.sspPendenteServico,
                data.sspPendenteDataProgramada,
                data.sspPendenteQtdDias,
                "Pendente",
                btn
            ]);
        }
    }
}