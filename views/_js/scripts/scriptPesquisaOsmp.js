/**
 * Created by iramar.junior on 02/02/2017.
 */

//===Tabela===//
//====================================================================================================================//

$.getScript(caminho + "/views/_js/scripts/scriptDashboard.js", function () { // include functions
    $('document').ready(function () {
        $("#resultadoPesquisaOsmp").dataTable(configTable(4, true, 'asc'));
    });
});

//====================================================================================================================//

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});

//====================================================================================================================//
//Auto preenchimento do local ap�s sele��o da linha.
$('select[name="linhaPesquisaOsmp"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaOsmp"]').val() == "") {
            linha.val("*");
            apiLinhaLocal(linha, $('select[name="localPesquisaOsmp"]'));
        } else {
            $('select[name="localPesquisaOsmp"]').html('<option value="">TODOS</option>');
        }
    } else {
        linha.val($(this).val());
        apiLinhaLocal(linha, $('select[name="localPesquisaOsmp"]'));
    }
    
    apiLinhaEstacao($(this), $('select[name="estacaoInicialPesquisaOsmp"]'));
    apiLinhaEstacao($(this), $('select[name="estacaoFinalPesquisaOsmp"]'));
});

switch ($('select[name="grupoPesquisaOsmp"]').val()) {
    case '21'://Edifica��es
        $("#local").show();
        $("#estacaoInicial").hide();
        $("#estacaoFinal").hide();
        break;
    case '25'://Subesta��o
        $("#local").show();
        $("#estacaoInicial").hide();
        $("#estacaoFinal").hide();
        break;
    case '24'://Via Permanente
        $("#local").hide();
        $("#estacaoInicial").show();
        $("#estacaoFinal").show();
        break;
    case '20'://Rede A�rea
        $("#local").show();
        $("#estacaoInicial").hide();
        $("#estacaoFinal").hide();
        break;
}

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoPesquisaOsmp"]').on('change', function () {
    switch ($(this).val()) {
        case '21'://Edifica��es
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '25'://Subesta��o
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '24'://Via Permanente
            $("#local").hide();
            $("#estacaoInicial").show();
            $("#estacaoFinal").show();
            break;
        case '20'://Rede A�rea
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
    }
    
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisaOsmp"]'));
        // apiGrupoServico(grupo, $('select[name="servicoPesquisaOsmp"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisaOsmp"]').html('<option value="">TODOS</option>');
        // $('select[name="servicoPesquisaOsmp"]').html('<option value="">TODOS</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaOsmp"]'));
    apiGrupoServico(grupo, $('select[name="servicoPesquisaOsmp"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisaOsmp"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoPesquisaOsmp"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisaOsmp"]'));
        } else {
            $('select[name="subSistemaPesquisaOsmp"]').html('<option value="">TODOS</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisaOsmp"]'));
    }
});

//====================================================================================================================//

//===A��es===//
//====================================================================================================================//

$('.btnPesquisar').on('click', function () {
    if ($('input[name="dataPartirOsmpAbertura"]').val() == '' && $('input[name="dataPartirOsmpEncerramento"]').val() == '' && $('input[name="numeroPesquisaOsmp"]').val() == '' && $('input[name="codigoSsmp"]').val() == '') {
        alertaJquery("Sugest�o",
            "A pesquisa solicitada ir� necessitar de alguns minutos para que seja realizada.<h4> Deseja acrescentar um intervalo de Data para facilitar a consulta?</h4>",
            "confirm",
            [{value: "Sim"},
                {value: "N�o"}],
            function (res) {
                if (res == "N�o") {
                    $("#pesquisa").submit();
                }
            });
    } else {
        $("#pesquisa").submit();
    }
});

$('.btnResetarPesquisa').on('click', function () {
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaOsmp";
});

$(".btnAbrirPesquisaSsmp").on("click", function () {
    var codigoSsmp = $(this).parent("td").parent("tr").find('td').next('td').html();
    window.location.href = caminho + "/dashboardGeral/pesquisaOsmpParaSsmp/" + codigoSsmp;
    // window.open(caminho + "/dashboardGeral/pesquisaSafParaSsm/" + codigoSaf, "_blank");
});

//====================================================================================================================//