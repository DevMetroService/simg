
var tableSafD;
var tableSaf;
var tableSsmA;
var tableSsmD;
var tableSsmE;
var tableOsm;
var tableSsp;
var tableOsp;
var atualizaDashboardSupervisao;
var miliSegReload;

$(document).ready(function () {

    miliSegReload = 300000;
    atualizaDashboardSupervisao = setTimeout(function () {
        window.location.reload();
    }, miliSegReload); //tempo em milisegundos

});

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tableSafD  = $("#indicadorSafDevolvida").dataTable(configTable(0, false, 'desc'));
        
        tableSaf  = $("#indicadorSaf").dataTable(configTable(4, false, 'asc'));

        tableSsmA = $("#indicadorSsmAberta").dataTable(configTable(4, false, 'asc'));

        tableSsmD = $("#indicadorSsmDevolvida").dataTable(configTable(4, false, 'asc'));

        tableSsmE = $("#indicadorSsmEncaminhada").dataTable(configTable(4, false, 'asc'));

        tableOsm  = $("#indicadorOsmExecucao").dataTable(configTable(4, false, 'asc'));

        tableSsp  = $("#indicadorSsp").dataTable(configTable(0, false, 'desc'));

        tableOsp  = $("#indicadorOspExecucao").dataTable(configTable(0, false, 'desc'));

        //$("#indicadorAuditoriaSaf").dataTable(configTable(0, false, 'desc'));
    });
});

//Evento de a��o dos bot�es de a��o da tabela SAF
$(document).on("click",".btnVisualizarSaf", function(){
    var dados =$(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSaf",
        data: {codigoSaf: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/saf");
        }
    });
});

//Dashboard Supervis�o PMP
//Dashboard Supervis�o PMP
//Dashboard Supervis�o PMP
$('#btnPMP').on('click', function(){
    $('#tipoPmp').show();
    $('#selectTipo option:eq(0)').prop('selected', true);
    $('#sistemaPmp').hide();
    $('#searchPmp').hide();
});

$('#tipoPmp').on('change', function(){
    var tipo = $('#selectTipo option:selected');
    switch(tipo.val()){

        case "3":
            $('#sistemaPmp').show();
            $('#selectSistema').removeAttr('disabled');
            $('#selectSistema option:eq(0)').prop('selected', true);
            break;

        default:
            break;
    }
    $('#searchPmp').show();
});

var nomePdf;

$('#searchPmp').on('click', function(){
    var tipo = $('#selectTipo option:selected');
    switch(tipo.val()){
        case "3":
            switch($('#selectSistema option:selected').val()){
                case "51":
                    $.ajax({                                                                            //inicia o ajax
                        type: "POST",
                        url:caminho+"/dashboardSupervisao/pmp",                            //informa o caminho do processo
                        data: {sistema: 51},                                                       //informa os dados ao ajax
                        success: function() {                                                           //em caso de sucesso imprime o retorno
                            window.location.replace(caminho+"/dashboardSupervisao/pmp");
                        }
                    });
                    break;
                case "41":
                    $.ajax({                                                                            //inicia o ajax
                        type: "POST",
                        url: caminho+"/dashboardSupervisao/pmp",                            //informa o caminho do processo
                        data: {sistema: 55},                                                       //informa os dados ao ajax
                        success: function(data) {                                                           //em caso de sucesso imprime o retorno
                            window.location.replace(caminho+"/dashboardSupervisao/pmp");
                        }
                    });
                    break;
                case "13":
                    $.ajax({                                                                            //inicia o ajax
                        type: "POST",
                        url: caminho+"/dashboardSupervisao/pmp",                            //informa o caminho do processo
                        data: {sistema: 13},                                                       //informa os dados ao ajax
                        success: function() {                                                           //em caso de sucesso imprime o retorno
                            window.location.replace(caminho+"/dashboardSupervisao/pmp");
                        }
                    });
                    break;
                case "53":
                    $.ajax({                                                                            //inicia o ajax
                        type: "POST",
                        url: caminho+"/dashboardSupervisao/pmp",                            //informa o caminho do processo
                        data: {sistema: 53},                                                       //informa os dados ao ajax
                        success: function() {                                                           //em caso de sucesso imprime o retorno
                            window.location.replace(caminho+"/dashboardSupervisao/pmp");
                        }
                    });
                    break;

            }
            break;

        default :
            $.ajax({                                                                            //inicia o ajax
                type: "POST",
                url: caminho+"/dashboardPmp/exibirPdf",                            //informa o caminho do processo
                data: { nomeArquivo: nomePdf },                                                       //informa os dados ao ajax
                success: function() {                                                           //em caso de sucesso imprime o retorno
                    window.location.replace(caminho+"/dashboardPmp/exibirPdf", "_blank");
                }
            });
            break;
    }
});

$(".btnAbrirSspCalendario").on("click", function(){
    var dados = $('input[name="codigoSspCallendar"]').val();                                            //dados recebe o formulario em string
    $.ajax({                                                               //inicia o ajax
        type: "POST",
        url: caminho+"/cadastroGeral/refillSsp",                      //informa o caminho do processo
        data: { codigoSsp: dados},                                         //informa os dados ao ajax
        success: function() {                                          //em caso de sucesso imprime o retorno
            window.location.replace(caminho+"/dashboardGeral/Ssp");
        }
    });
    return false;
});

$(".btnAbrirOspCalendario").on("click", function(){
    var codigoSsp = $('input[name="codigoSspCallendar"]').val();                                            //dados recebe o formulario em string

    window.location.replace(caminho + "/dashboardGeral/pesquisaSspParaOsp/" + codigoSsp);
    return false;
});

$(document).ready(function() {
    $('#calendarioProgramacaoCCM').fullCalendar({
        events: caminho+'/api/getResult/getSspCalendario',
        header: {
            left: 'title',
            center: '',
            right: 'prev next today'
        },
        lang: 'pt-br',
        eventLimit: true,
        eventClick:  function(event, jsEvent, view) {
            $('#modalTitleCalendarioProgramacaoCCM').html(event.title);
            $('#modalBodyCalendarioProgramacaoCCM').html(event.description);
            $('#modalCalendarioProgramacaoCCM').modal();
        }
    });
});