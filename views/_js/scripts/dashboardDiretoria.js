//***********************//
//Feito Por Iramar Junior//
//***********************//

$("#tableSafAbertas").dataTable(configTable(0, false, 'asc'));
$("#tableSaf60").dataTable(configTable(0, false, 'asc'));
$(".indicadorFuncionario").dataTable(configTable(0, false, 'asc'));
$("#indicadorProcedimentoPadronizado").dataTable(configTable(1, false, 'asc'));
$('#indicadorTabelaMaterial').dataTable(configTable(0,false,'asc'));
$('#indicadorTabelaFornecedor').dataTable(configTable(0,false,'asc'));
$('#indicadorSaf').dataTable(configTable(0,false,'asc'));
$('.tableIS').dataTable(configTable(0,false,'asc'));

var tbl_listaCronograma = $('#indicadorListaCronograma_modal').dataTable(configTable(0,false,'asc'));
var tableRelModal = $('.tableRelModal').dataTable(configTable(0,false,'asc'));


//====================================================================================================================//

//Gr�ficos mini//
//====================================================================================================================//
var data = new Date();
var mes = data.getMonth();
loader();

$(document).on("click",".btnEditarSaf", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillSaf",
        data: {codigoSaf: dados},
        success: function() {
            window.location.replace(caminho+"/dashboardGeral/saf");
        }
    });
});

$(document).on("click",".btnImprimirSaf", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/imprimirSaf",
        data: {codigoSaf: dados},
        success: function() {
            window.open(caminho+"/dashboardGeral/printSaf", "_blank");
        }
    });
});

if(pagina == "dashboardDiretoria" && localReturn == undefined){

    $.getJSON(
        caminho + "/api/returnResult/mes/getValueApp",
        {mes: mes + 1},
        function (json) {

            var titulo = json.nomeMes;

            var totalEd = parseInt(json.totalEd);
            var encerradasEd = parseInt(json.encerradasEd);
            var abertasRestantesEd = totalEd - encerradasEd;

            var totalVp = parseInt(json.totalVp);
            var encerradasVp = parseInt(json.encerradasVp);
            var abertasRestantesVp = totalVp - encerradasVp;

            var totalRa = parseInt(json.totalRa);
            var encerradasRa = parseInt(json.encerradasRa);
            var abertasRestantesRa = totalRa - encerradasRa;

            var totalSb = parseInt(json.totalSb);
            var encerradasSb = parseInt(json.encerradasSb);
            var abertasRestantesSb = totalSb - encerradasSb;

            var totalBi = parseInt(json.totalBi);
            var encerradasBi = parseInt(json.encerradasBi);
            var abertasRestantesBi = totalBi - encerradasBi;

            var totalTe = parseInt(json.totalTe);
            var encerradasTe = parseInt(json.encerradasTe);
            var abertasRestantesTe = totalTe - encerradasTe;

            Highcharts.chart('boxEd', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'EDIFICACOES'
                },
                tooltip: {
                    pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: titulo + ' | Total - ' + totalEd
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            format: '{point.percentage:.1f} %',
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        showInLegend: true
                    }
                },
                colors: ['#2196F3', '#4CAF50'],
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'PENDENTES - ' + abertasRestantesEd,
                        y: abertasRestantesEd
                    }, {
                        name: 'ENCERRADAS - ' + encerradasEd,
                        y: encerradasEd,
                        sliced: true,
                        selected: true
                    }]
                }]
            });
            Highcharts.chart('boxVp', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'VIA PERMANENTE'
                },
                tooltip: {
                    pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: titulo + ' | Total - ' + totalVp
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            format: '{point.percentage:.1f} %',
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        showInLegend: true
                    }
                },
                colors: ['#2196F3', '#4CAF50'],
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'PENDENTES - ' + abertasRestantesVp,
                        y: abertasRestantesVp
                    }, {
                        name: 'ENCERRADAS - ' + encerradasVp,
                        y: encerradasVp,
                        sliced: true,
                        selected: true
                    }]
                }]
            });
            Highcharts.chart('boxRa', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'REDE AEREA'
                },
                tooltip: {
                    pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: titulo + ' | Total - ' + totalRa
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            format: '{point.percentage:.1f} %',
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        showInLegend: true
                    }
                },
                colors: ['#2196F3', '#4CAF50'],
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'PENDENTES - ' + abertasRestantesRa,
                        y: abertasRestantesRa
                    }, {
                        name: 'ENCERRADAS - ' + encerradasRa,
                        y: encerradasRa,
                        sliced: true,
                        selected: true
                    }]
                }]
            });
            Highcharts.chart('boxSb', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'SUBESTACAO'
                },
                tooltip: {
                    pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: titulo + ' | Total - ' + totalSb
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            format: '{point.percentage:.1f} %',
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        showInLegend: true
                    }
                },
                colors: ['#2196F3', '#4CAF50'],
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'PENDENTES - ' + abertasRestantesSb,
                        y: abertasRestantesSb
                    }, {
                        name: 'ENCERRADAS - ' + encerradasSb,
                        y: encerradasSb,
                        sliced: true,
                        selected: true
                    }]
                }]
            });
            Highcharts.chart('boxBi', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'BILHETAGEM'
                },
                tooltip: {
                    pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: titulo + ' | Total - ' + totalBi
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            format: '{point.percentage:.1f} %',
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        showInLegend: true
                    }
                },
                colors: ['#2196F3', '#4CAF50'],
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'PENDENTES - ' + abertasRestantesBi,
                        y: abertasRestantesBi
                    }, {
                        name: 'ENCERRADAS - ' + encerradasBi,
                        y: encerradasBi,
                        sliced: true,
                        selected: true
                    }]
                }]
            });
            Highcharts.chart('boxTe', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'TELECOM'
                },
                tooltip: {
                    pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                    text: titulo + ' | Total - ' + totalTe
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            format: '{point.percentage:.1f} %',
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        showInLegend: true
                    }
                },
                colors: ['#2196F3', '#4CAF50'],
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'PENDENTES - ' + abertasRestantesTe,
                        y: abertasRestantesTe
                    }, {
                        name: 'ENCERRADAS - ' + encerradasTe,
                        y: encerradasTe,
                        sliced: true,
                        selected: true
                    }]
                }]
            });
        }
    ).success(function () {
        loading();
    });

    $('select[name="mesApp"]').change(function () {

        var mes = $(this).val();
        loader();
        $.getJSON(
            caminho + "/api/returnResult/mes/getValueApp",
            {mes: mes},
            function (json) {

                var titulo = json.nomeMes;

                var totalEd = parseInt(json.totalEd);
                var encerradasEd = parseInt(json.encerradasEd);
                var abertasRestantesEd = totalEd - encerradasEd;

                var totalVp = parseInt(json.totalVp);
                var encerradasVp = parseInt(json.encerradasVp);
                var abertasRestantesVp = totalVp - encerradasVp;

                var totalRa = parseInt(json.totalRa);
                var encerradasRa = parseInt(json.encerradasRa);
                var abertasRestantesRa = totalRa - encerradasRa;

                var totalSb = parseInt(json.totalSb);
                var encerradasSb = parseInt(json.encerradasSb);
                var abertasRestantesSb = totalSb - encerradasSb;

                var totalBi = parseInt(json.totalBi);
                var encerradasBi = parseInt(json.encerradasBi);
                var abertasRestantesBi = totalBi - encerradasBi;

                var totalTe = parseInt(json.totalTe);
                var encerradasTe = parseInt(json.encerradasTe);
                var abertasRestantesTe = totalTe - encerradasTe;

                Highcharts.chart('boxEd', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'EDIFICACOES'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: titulo + ' | Total - ' + totalEd
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            showInLegend: true
                        }
                    },
                    colors: ['#2196F3', '#4CAF50'],
                    series: [{
                        name: 'Porcentagem',
                        colorByPoint: true,
                        data: [{
                            name: 'PENDENTES - ' + abertasRestantesEd,
                            y: abertasRestantesEd
                        }, {
                            name: 'ENCERRADAS - ' + encerradasEd,
                            y: encerradasEd,
                            sliced: true,
                            selected: true
                        }]
                    }]
                });
                Highcharts.chart('boxVp', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'VIA PERMANENTE'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: titulo + ' | Total - ' + totalVp
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            showInLegend: true
                        }
                    },
                    colors: ['#2196F3', '#4CAF50'],
                    series: [{
                        name: 'Porcentagem',
                        colorByPoint: true,
                        data: [{
                            name: 'PENDENTES - ' + abertasRestantesVp,
                            y: abertasRestantesVp
                        }, {
                            name: 'ENCERRADAS - ' + encerradasVp,
                            y: encerradasVp,
                            sliced: true,
                            selected: true
                        }]
                    }]
                });
                Highcharts.chart('boxRa', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'REDE AEREA'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: titulo + ' | Total - ' + totalRa
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            showInLegend: true
                        }
                    },
                    colors: ['#2196F3', '#4CAF50'],
                    series: [{
                        name: 'Porcentagem',
                        colorByPoint: true,
                        data: [{
                            name: 'PENDENTES - ' + abertasRestantesRa,
                            y: abertasRestantesRa
                        }, {
                            name: 'ENCERRADAS - ' + encerradasRa,
                            y: encerradasRa,
                            sliced: true,
                            selected: true
                        }]
                    }]
                });
                Highcharts.chart('boxSb', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'SUBESTACAO'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: titulo + ' | Total - ' + totalSb
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            showInLegend: true
                        }
                    },
                    colors: ['#2196F3', '#4CAF50'],
                    series: [{
                        name: 'Porcentagem',
                        colorByPoint: true,
                        data: [{
                            name: 'PENDENTES - ' + abertasRestantesSb,
                            y: abertasRestantesSb
                        }, {
                            name: 'ENCERRADAS - ' + encerradasSb,
                            y: encerradasSb,
                            sliced: true,
                            selected: true
                        }]
                    }]
                });
                Highcharts.chart('boxBi', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'SUBESTACAO'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: titulo + ' | Total - ' + totalBi
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            showInLegend: true
                        }
                    },
                    colors: ['#2196F3', '#4CAF50'],
                    series: [{
                        name: 'Porcentagem',
                        colorByPoint: true,
                        data: [{
                            name: 'PENDENTES - ' + abertasRestantesBi,
                            y: abertasRestantesBi
                        }, {
                            name: 'ENCERRADAS - ' + encerradasBi,
                            y: encerradasBi,
                            sliced: true,
                            selected: true
                        }]
                    }]
                });
                Highcharts.chart('boxTe', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: 'SUBESTACAO'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
                    },
                    credits: {
                        enabled: false
                    },
                    subtitle: {
                        text: titulo + ' | Total - ' + totalTe
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            showInLegend: true
                        }
                    },
                    colors: ['#2196F3', '#4CAF50'],
                    series: [{
                        name: 'Porcentagem',
                        colorByPoint: true,
                        data: [{
                            name: 'PENDENTES - ' + abertasRestantesTe,
                            y: abertasRestantesTe
                        }, {
                            name: 'ENCERRADAS - ' + encerradasTe,
                            y: encerradasTe,
                            sliced: true,
                            selected: true
                        }]
                    }]
                });
            }
        ).success(function () {
            loading();
        });
    });


    //====================================================================================================================//

    //=======================================Acompanhamento Cronograma (Barra)============================================//

    //Fun��o de chamada de dados do cronograma
    $(document).on("click",".btnVisualizarCronograma", function(){
        var dados = $(this).parent("td").parent("tr").find('td').html();

        var cronograma  = dados.split('<input')[0];
        var grupo       = dados.split('value="')[1].split('"')[0];

        apiModalCronograma(cronograma, grupo, tableRelModal);
    });

    $(document).on("click",".btnImprimirOsmp", function(){
        var dados = $(this).parent("td").parent("tr").find('td').html();
        var form = $(this).val();
        $.ajax({
            type: "POST",
            url: caminho+"/cadastroGeral/imprimirOsmp",
            data: {codigoOs: dados},
            success: function() {
                window.open(caminho+"/dashboardGeral/printOsmp/"+form, "_blank");
            }
        });
    });





    var data = new Date();
    var mes = data.getMonth();
    loader();
    $.getJSON(
        caminho + "/api/returnResult/mes/getNumberEngSupervisao",
        {mes: mes + 1},
        function (json) {

            $('#quantidadeEd').html(json.ativosEd);
            $('#quantidadeVp').html(json.ativosVp);
            $('#quantidadeRa').html(json.ativosRa);
            $('#quantidadeSb').html(json.ativosSu);
            $('#quantidadeBi').html(json.ativosBl);
            $('#quantidadeTe').html(json.ativosTl);

            var abertas = [json.abertaEd, json.abertaVp, json.abertaRa, json.abertaSu, json.abertaBl, json.abertaTl];
            abertas = abertas.map(Number);
            var execucao = [json.execucaoEd, json.execucaoVp, json.execucaoRa, json.execucaoSu, json.execucaoBl, json.execucaoTl];
            execucao = execucao.map(Number);
            var autorizadas = [json.autorizadaEd, json.autorizadaVp, json.autorizadaRa, json.autorizadaSu, json.autorizadaBl, json.autorizadaTl];
            autorizadas = autorizadas.map(Number);
            var pendentes = [json.pendenteEd, json.pendenteVp, json.pendenteRa, json.pendenteSu, json.pendenteBl, json.pendenteTl];
            pendentes = pendentes.map(Number);
            var programadas = [json.programadaEd, json.programadaVp, json.programadaRa, json.programadaSu, json.programadaBl, json.programadaTl];
            programadas = programadas.map(Number);
            var executadas = [json.executadaEd, json.executadaVp, json.executadaRa, json.executadaSu, json.executadaBl, json.executadaTl];
            executadas = executadas.map(Number);
            var canceladas = [json.canceladaEd, json.canceladaVp, json.canceladaRa, json.canceladaSu, json.canceladaBl, json.canceladaTl];
            canceladas = canceladas.map(Number);
            var reprogramadas = [json.reprogramadaEd, json.reprogramadaVp, json.reprogramadaRa, json.reprogramadaSu, json.reprogramadaBl, json.reprogramadaTl];
            reprogramadas = reprogramadas.map(Number);

            var _abertas = [abertas[0] + execucao[0] + autorizadas[0] + pendentes[0] + programadas[0] + reprogramadas[0],
                abertas[1] + execucao[1] + autorizadas[1] + pendentes[1] + programadas[1] + reprogramadas[1],
                abertas[2] + execucao[2] + autorizadas[2] + pendentes[2] + programadas[2] + reprogramadas[2],
                abertas[3] + execucao[3] + autorizadas[3] + pendentes[3] + programadas[3] + reprogramadas[3],
                abertas[4] + execucao[4] + autorizadas[4] + pendentes[4] + programadas[4] + reprogramadas[4],
                abertas[5] + execucao[5] + autorizadas[5] + pendentes[5] + programadas[5] + reprogramadas[5]];
            var _encerradas = executadas;
            var _canceladas = canceladas;
            var _total = [abertas[0] + execucao[0] + autorizadas[0] + pendentes[0] + programadas[0] + reprogramadas[0] + executadas[0] + canceladas[0],
                abertas[1] + execucao[1] + autorizadas[1] + pendentes[1] + programadas[1] + reprogramadas[1] + executadas[1] + canceladas[1],
                abertas[2] + execucao[2] + autorizadas[2] + pendentes[2] + programadas[2] + reprogramadas[2] + executadas[2] + canceladas[2],
                abertas[3] + execucao[3] + autorizadas[3] + pendentes[3] + programadas[3] + reprogramadas[3] + executadas[3] + canceladas[3],
                abertas[4] + execucao[4] + autorizadas[4] + pendentes[4] + programadas[4] + reprogramadas[4] + executadas[4] + canceladas[4],
                abertas[5] + execucao[5] + autorizadas[5] + pendentes[5] + programadas[5] + reprogramadas[5] + executadas[5] + canceladas[5]];

            subtitleAno = $("input[name='title']").val();

            if($("select[name='mesBarDashboard'] option:selected").html() != null)
                subtitleMes = $("select[name='mesBarDashboard'] option:selected").html().replace(/[�]/g, "c");
            else{
                subtitleMes = mes;
            }

            Highcharts.chart('cronograma', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Acompanhamento Cronograma'
                },
                subtitle: {
                    text: subtitleMes + ' ' + subtitleAno
                },
                xAxis: {
                    categories: ["Edificacoes", "Via Permanente", "Rede Aerea", "Subestacao", "Bilhetagem", "Telecom"],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantidade - (un)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: plotOptionsBar('click_modal_cronograma'),
                credits: {
                    enabled: false
                },
                colors: ['#607D8B', '#B0BEC5', '#4CAF50', '#d50000'],
                series: [
                    {
                        name: 'Total',
                        data: _total
                    }, {
                        name: 'Abertas',
                        data: _abertas
                    }, {
                        name: 'Executadas',
                        data: _encerradas
                    }, {
                        name: 'Canceladas',
                        data: _canceladas
                    }
                ]
            });

        }
    ).success(function () {
        loading();
    });

    $('select[name="mesBarDashboard"]').change(function () {

        var mes = $(this).val();
        loader();
        $.getJSON(
            caminho + "/api/returnResult/mes/getNumberEngSupervisao",
            {mes: mes},
            function (json) {

                $('#quantidadeEd').html(json.ativosEd);
                $('#quantidadeVp').html(json.ativosVp);
                $('#quantidadeSb').html(json.ativosSu);
                $('#quantidadeRa').html(json.ativosRa);

                var abertas = [json.abertaEd, json.abertaVp, json.abertaSu, json.abertaRa];
                abertas = abertas.map(Number);
                var execucao = [json.execucaoEd, json.execucaoVp, json.execucaoSu, json.execucaoRa];
                execucao = execucao.map(Number);
                var autorizadas = [json.autorizadaEd, json.autorizadaVp, json.autorizadaSu, json.autorizadaRa];
                autorizadas = autorizadas.map(Number);
                var pendentes = [json.pendenteEd, json.pendenteVp, json.pendenteSu, json.pendenteRa];
                pendentes = pendentes.map(Number);
                var programadas = [json.programadaEd, json.programadaVp, json.programadaSu, json.programadaRa];
                programadas = programadas.map(Number);
                var executadas = [json.executadaEd, json.executadaVp, json.executadaSu, json.executadaRa];
                executadas = executadas.map(Number);
                var canceladas = [json.canceladaEd, json.canceladaVp, json.canceladaSu, json.canceladaRa];
                canceladas = canceladas.map(Number);
                var reprogramadas = [json.reprogramadaEd, json.reprogramadaVp, json.reprogramadaSu, json.reprogramadaRa];
                reprogramadas = reprogramadas.map(Number);

                var _abertas = [abertas[0] + execucao[0] + autorizadas[0] + pendentes[0] + programadas[0] + reprogramadas[0],
                    abertas[1] + execucao[1] + autorizadas[1] + pendentes[1] + programadas[1] + reprogramadas[1],
                    abertas[2] + execucao[2] + autorizadas[2] + pendentes[2] + programadas[2] + reprogramadas[2],
                    abertas[3] + execucao[3] + autorizadas[3] + pendentes[3] + programadas[3] + reprogramadas[3]]
                var _encerradas = executadas;
                var _canceladas = canceladas;
                var _total = [abertas[0] + execucao[0] + autorizadas[0] + pendentes[0] + programadas[0] + reprogramadas[0] + executadas[0] + canceladas[0],
                    abertas[1] + execucao[1] + autorizadas[1] + pendentes[1] + programadas[1] + reprogramadas[1] + executadas[1] + canceladas[1],
                    abertas[2] + execucao[2] + autorizadas[2] + pendentes[2] + programadas[2] + reprogramadas[2] + executadas[2] + canceladas[2],
                    abertas[3] + execucao[3] + autorizadas[3] + pendentes[3] + programadas[3] + reprogramadas[3] + executadas[3] + canceladas[3]]

                subtitleAno = $("input[name='title']").val();
                subtitleMes = $("select[name='mesBarDashboard'] option:selected").html().replace(/[�]/g, "c");

                Highcharts.chart('cronograma', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Acompanhamento Cronograma'
                    },
                    subtitle: {
                        text: subtitleMes + ' ' + subtitleAno
                    },
                    xAxis: {
                        categories: ["Edificacoes", "Via Permanente", "Subestacao", "Rede Aerea"],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Quantidade - (un)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: plotOptionsBar('click_modal_cronograma'),
                    credits: {
                        enabled: false
                    },
                    colors: ['#607D8B', '#B0BEC5', '#4CAF50', '#d50000'],
                    series: [
                        {
                            name: 'Total',
                            data: _total
                        }, {
                            name: 'Abertas',
                            data: _abertas
                        }, {
                            name: 'Executadas',
                            data: _encerradas
                        }, {
                            name: 'Canceladas',
                            data: _canceladas
                        }
                    ]
                });

            }
        ).success(function () {
            loading();
            $('html, body').animate({
                scrollTop: $("#section_cronograma").offset().top
            });
        });
    });
}
//====================================================================================================================//

//Fun��es de p�gina//
//====================================================================================================================//

//M�dulo ALmoxarifado
$('#materialUtilizadoRelatorio').click(function(){

});



//====================================================================================================================//

//A��es//
//====================================================================================================================//

$('.btnPesquisar').on('click', function () {
    if ($('input[name="dataPartir"]').val() == '' && $('input[name="codigoSaf"]').val() == '') {
        alertaJquery("Sugest�o", "A pesquisa solicitada ir� necessitar de alguns minutos para que seja realizada.<h4> Deseja acrescentar um intervalo de Data para facilitar a consulta?</h4>", "confirm", [{value: "Sim"}, {value: "N�o"}], function (res) {
            if (res == "N�o") {
                $("#pesquisa").submit();
            }
        });
    } else {
        $("#pesquisa").submit();
    }
});

$('.btnResetarPesquisa').on('click', function () {
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaSaf";
});

$(".btnEditarOsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    $.ajax({                                                             //inicia o ajax
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsm",                      //informa o caminho do processo
        data: {codigoOs: dados},                                       //informa os dados ao ajax
        success: function () {                                            //em caso de sucesso imprime o retorno
            window.location.replace(caminho + "/dashboardGeral/Osm");
            // window.open(caminho + "/dashboardGeral/Osm", "_blank");
        }
    });
});

$(".btnImprimirOsm").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();      //dados recebe o formulario em string
    $.ajax({                                                             //inicia o ajax
        type: "POST",
        url: caminho + "/cadastroGeral/imprimirOsm",                    //informa o caminho do processo
        data: {codigoOs: dados},                                          //informa os dados ao ajax
        success: function () {
            window.open(caminho + "/dashboardGeral/printOsm", "_blank");  //em caso de sucesso imprime o retorno
        }
    });
});

$(".btnAbrirPesquisaSsm").on("click", function(){
    // var codigoSsm =$(this).parent("td").parent("tr").find('td').next('td').html();
    var codigoSsm =$('#cod_ssm').html();
    // codigoSsm = codigoSsm.split('-')[0];
    // alert($(this));
    // alert(codigoSsm);
    window.location.href = caminho + "/dashboardGeral/pesquisaOsmParaSsm/" + codigoSsm;
    // window.open(caminho + "/dashboardGeral/pesquisaSafParaSsm/" + codigoSaf, "_blank");
});

//Caso Extra:Supervis�o
$(".btnVisualizarSaf").on("click", function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillSaf",
        data: {codigoSaf: dados},
        success: function () {
            window.location.href = caminho + "/dashboardGeral/saf";
        }
    });
});

$('.btnDadosFuncionario').click(function(e){
    // e.preventDefault();

    var matricula = $(this).parent("td").parent("tr").find('td').html();        //dados recebe o formulario em string

    $.getJSON(
        caminho + "/api/returnResult/matricula/homemHora",
        {matricula: matricula},
        function (json) {
            // alert(JSON.stringify(json));
            $('#matriculaModal').val(json.matricula);
            $('#nomeModal').val(json.nome);
            $('#escolaridade').val(json.escolaridade);
            $('#cursoTecnico').val(json.cursoTecnico);
            $('#cpfModal').val(json.cpf);
            $('#funcaoModal').val(json.funcao);
            $('#funcaoModal').attr("data-original-title",json.funcao);
            $('#centroResultadoModal').val(json.centroResultado);
            $('#centroResultadoModal').attr("data-original-title",json.centroResultado);
            $('#lotacaoModal').val(json.unidade);
            $('#lotacaoModal').attr("data-original-title",json.unidade);
        }
    ).always(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
});


//============Disponibilidade em m�dulo FROTA
if(localReturn == "moduloEstadoFrota"){
    var tableOeste = $('#tableRelOeste');
    var tablePM = $('#tableRelPM');
    var tableSobral = $('#tableRelSobral');
    var tableCariri = $('#tableRelCariri');

    var confSul = $('#confSul');
    var columnDispoResultSul = $('#tableRelSul tbody tr td:nth-child(3)');
    var columnDispoDiaSul = $('#tableRelSul tbody tr td:nth-child(1)');
    var resultDisplaySul = $('#resultMensalSul');
    var graphConfSul = $('#graficoSul');

    var confOeste = $('#confOeste');
    var columnDispoResultOeste = $('#tableRelOeste tbody tr td:nth-child(3)');
    var columnDispoDiaOeste = $('#tableRelOeste tbody tr td:nth-child(1)');
    var resultDisplayOeste = $('#resultMensalOeste');

    var confPM = $('#confPM');
    var columnDispoResultPM = $('#tableRelPM tbody tr td:nth-child(3)');
    var columnDispoDiaPM = $('#tableRelPM tbody tr td:nth-child(1)');
    var resultDisplayPM = $('#resultMensalPM');

    var confSobral = $('#confSobral');
    var columnDispoResultSobral = $('#tableRelSobral tbody tr td:nth-child(3)');
    var columnDispoDiaSobral = $('#tableRelSobral tbody tr td:nth-child(1)');
    var resultDisplaySobral = $('#resultMensalSobral');

    var confCariri = $('#confCariri');
    var columnDispoResultCariri = $('#tableRelCariri tbody tr td:nth-child(3)');
    var columnDispoDiaCariri = $('#tableRelCariri tbody tr td:nth-child(1)');
    var resultDisplayCariri = $('#resultMensalCariri');


    var item_y=[];
    var item_x=[];

    var soma = 0;
    var total = 0;
    var result;

    var graphSul;
    var graphOeste;
    var graphPM;
    var graphSobral;
    var graphCariri;

    $('document').ready(function(){

        //Gr�fico SUL
        columnDispoResultSul.each( function(){
            var aux = parseFloat($(this).text().replace('%', ''));
            item_y.push( aux );

            soma += aux;
            total += 1;
        });
        columnDispoDiaSul.each( function(){
            item_x.push( $(this).text().replace('%', '') );
        });

        result = (soma/total).toFixed(2);

        resultDisplaySul.html(result + "%");

        graphSul = Highcharts.chart('graficoSul', {
            xAxis: {
                categories: item_x
            },
            yAxis: {
                min: 0,
                max: 120,
                title: {
                    text: 'Disponibilidade (%)'
                }
            },
            title: {
                text: 'Gr�fico Disponibilidade Mensal'
            },
            series: [{
                type: 'line',
                name: 'M�dia Di�ria',
                data: item_y
            }]
        });

        //Iguala o tamanho das tabelas para simetria.
        confSul.height(graphConfSul.height());


        //Gr�fico Oeste
        columnDispoResultOeste.each( function(){
            var aux = parseFloat($(this).text().replace('%', ''));
            item_y.push( aux );

            soma += aux;
            total += 1;
        });
        columnDispoDiaOeste.each( function(){
            item_x.push( $(this).text().replace('%', '') );
        });

        result = (soma/total).toFixed(2);

        resultDisplayOeste.html(result + "%");

        graphOeste = Highcharts.chart('graficoOeste', {
            xAxis: {
                categories: item_x
            },
            yAxis: {
                min: 0,
                max: 120,
                title: {
                    text: 'Disponibilidade (%)'
                }
            },
            title: {
                text: 'Gr�fico Disponibilidade Mensal'
            },
            series: [{
                type: 'line',
                name: 'M�dia Di�ria',
                data: item_y
            }]
        });

        //Iguala o tamanho das tabelas para simetria.
        confOeste.height(graphConfSul.height());


        //Gr�fico PM
        columnDispoResultPM.each( function(){
            var aux = parseFloat($(this).text().replace('%', ''));
            item_y.push( aux );

            soma += aux;
            total += 1;
        });
        columnDispoDiaPM.each( function(){
            item_x.push( $(this).text().replace('%', '') );
        });

        result = (soma/total).toFixed(2);

        resultDisplayPM.html(result + "%");

        graphPM = Highcharts.chart('graficoPM', {
            xAxis: {
                categories: item_x
            },
            yAxis: {
                min: 0,
                max: 120,
                title: {
                    text: 'Disponibilidade (%)'
                }
            },
            title: {
                text: 'Gr�fico Disponibilidade Mensal'
            },
            series: [{
                type: 'line',
                name: 'M�dia Di�ria',
                data: item_y
            }]
        });

        //Iguala o tamanho das tabelas para simetria.
        confPM.height(graphConfSul.height());

        //Gr�fico Sobral
        columnDispoResultSobral.each( function(){
            var aux = parseFloat($(this).text().replace('%', ''));
            item_y.push( aux );

            soma += aux;
            total += 1;
        });
        columnDispoDiaSobral.each( function(){
            item_x.push( $(this).text().replace('%', '') );
        });

        result = (soma/total).toFixed(2);

        resultDisplaySobral.html(result + "%");

        graphSobral = Highcharts.chart('graficoSobral', {
            xAxis: {
                categories: item_x
            },
            yAxis: {
                min: 0,
                max: 120,
                title: {
                    text: 'Disponibilidade (%)'
                }
            },
            title: {
                text: 'Gr�fico Disponibilidade Mensal'
            },
            series: [{
                type: 'line',
                name: 'M�dia Di�ria',
                data: item_y
            }]
        });

        //Iguala o tamanho das tabelas para simetria.
        confSobral.height(graphConfSul.height());

        //Gr�fico Cariri
        columnDispoResultCariri.each( function(){
            var aux = parseFloat($(this).text().replace('%', ''));
            item_y.push( aux );

            soma += aux;
            total += 1;
        });
        columnDispoDiaCariri.each( function(){
            item_x.push( $(this).text().replace('%', '') );
        });

        result = (soma/total).toFixed(2);

        resultDisplayCariri.html(result + "%");

        graphCariri = Highcharts.chart('graficoCariri', {
            xAxis: {
                categories: item_x
            },
            yAxis: {
                min: 0,
                max: 120,
                title: {
                    text: 'Disponibilidade (%)'
                }
            },
            title: {
                text: 'Gr�fico Disponibilidade Mensal'
            },
            series: [{
                type: 'line',
                name: 'M�dia Di�ria',
                data: item_y
            }]
        });

        //Iguala o tamanho das tabelas para simetria.
        confCariri.height(graphConfSul.height());
    });
}
