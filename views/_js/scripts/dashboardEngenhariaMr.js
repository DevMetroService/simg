$('document').ready(function(){
    var graphFalha = $('#graphFalha');
    var confiabilidade = $('#confiabilidade');
    var tbConfiabilidade = $('#tbConfiabilidade');

//Inicializa��o das DataTables.
    tbConfiabilidade.DataTable(configTable(0, true, 'asc', false, false));
    $('.tbManut').DataTable(configTable(0, false, 'asc'));

    var month = new Date().getMonth();
    var year = new Date().getFullYear();

    month += 1;

    var days = daysInMonth(month, year);
    var x_category = [];

    for (var i = 0; i< days; i++){
        x_category.push(i+1);
    }
    var lineGraph;
    var pieTUE;
    var pieVLT;

    var grafFrotaFalha = $('#grafFrotaFalha');
    var grafMesFalha = $('#grafMesFalha');


//Inicializa os gr�ficos
    lineGraph = Highcharts.chart('graphFalha', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Falhas por M�s'
        },
        subtitle: {
            text: 'Quantidade de falhas por m�s/N�vel'
        },
        xAxis: {
            categories: x_category
        },
        yAxis: {
            title: {
                text: 'Quantidade'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });

    pieTUE = Highcharts.chart('appTUE', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'App Material Rodante TUE'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        }
    });

    pieVLT = Highcharts.chart('appVLT', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'App Material Rodante VLT'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Porcentagem',
            colorByPoint: true,
            data: [{
                name: 'Executadas',
                y: 85,
                color: "green"
            }, {
                name: 'N�o Executadas',
                y: 15,
                color: "red"
            }]
        }]
    });

//Adiciona os primeiros valores ao carregar a p�gina da linha de falhas p/ m�s.
    addSerieLineGraph({mes: month, ano: year, grupo: [22,23], nivel: 'A', frota: 'T1'}, lineGraph);
    addSerieLineGraph({mes: month, ano: year, grupo: [22,23], nivel: 'B', frota: 'T1'}, lineGraph);
    addSerieLineGraph({mes: month, ano: year, grupo: [22,23], nivel: 'C', frota: 'T1'}, lineGraph);

//Preenche a tabela de confiabilidade
    setRowTableConfiabilidade(month, year, null ,tbConfiabilidade);

//Iguala o tamanho das tabelas para simetria.
    confiabilidade.height(graphFalha.height());

    console.log("Teste " + month + year +" yeeei");
    updateSeriesLineGraph({mes: month, ano: year, frota: grafFrotaFalha.val()}, lineGraph);

//Eventos de atualiza��o de gr�ficos.
    grafMesFalha.change(function(){
        var mes = $(this).val().split('-');

        var ano = mes[1];
        mes = mes[0];

        var grafFrota = grafFrotaFalha.val();

        updateSeriesLineGraph({mes: mes, ano: ano, frota: grafFrota}, lineGraph);
        setRowTableConfiabilidade(mes, ano, grafFrota, tbConfiabilidade);
    });

    grafFrotaFalha.change(function(){
        var mes = grafMesFalha.val().split('-');

        var ano = mes[1];
        mes = mes[0];

        var grafFrota = grafFrotaFalha.val();

        updateSeriesLineGraph({mes: mes, ano: ano, frota: grafFrota}, lineGraph);
        setRowTableConfiabilidade(mes, ano, grafFrota, tbConfiabilidade);
    });

    $('#grafApp').change(function(){
        var mes = $(this).val().split('-');

        var ano = mes[1];
        mes = mes[0];

        updateSeriesLineGraph({mes: mes, ano: ano}, lineGraph);
    });

    function addSerieLineGraph(options, lineChart){

        var dt = new Date();
        if(options.mes == null){
            options.mes = dt.getDate();
        }
        if(options.ano == null){
            options.ano = dt.getFullYear();
        }
        if(options.grupo == null){
            options.grupo = 21;
        }
        if(options.nivel == null){
            options.nivel = 'C';
        }
        if(options.frota == null){
            options.frota= 'T1';
        }

        var arr = [];

        $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo = "+options.grupo+" AND vsaf.nivel = '"+options.nivel+"' AND vsaf.frota = '"+options.frota+"' "},
            function (json) {
                prepareDataGraph(json);
            }
        ).done(function(){
            lineChart.addSeries({
                name: options.nivel,
                data: arr
            });
        }).error(function(msg){
            alert(msg.responseText);
        });
    }

    async function updateSeriesLineGraph(options, lineChart){
        var nC = await $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo IN (22,23) AND vsaf.nivel = 'C' AND vsaf.frota = '"+options.frota+"'  "}
        ).error(function(msg){
            console.log(msg.responseText);
        });

        var nA = await $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo IN (22,23) AND vsaf.nivel = 'A'  AND vsaf.frota = '"+options.frota+"' "}
        ).error(function(msg){
            console.log(msg.responseText);
        });

        var nB = await $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo IN (22,23) AND vsaf.nivel = 'B'  AND vsaf.frota = '"+options.frota+"' "}
        ).error(function(msg){
            console.log(msg.responseText);
        });


        nC = {
            name: 'C',
            data: prepareDataGraph(nC)
        };
        nB = {
            name: 'B',
            data: prepareDataGraph(nB)
        };
        nA = {
            name: 'A',
            data: prepareDataGraph(nA)
        };

        lineChart.update(
            {
                series: [nA, nB, nC]
            }, true, true);
    }

    async function updateSeriesPieGraph(options, pieChart){
        var resposta = await $.getJSON(
            caminho + "/api/returnResult/dados/getAppMr",

            {dados: [month, year]},

            function (json) {
                if (json != null) { return json; }
            }
        ).error(function(msg){
            alert(msg.responseText);
        });

        pieChart.update(
            {
                series: [{
                    name: 'Porcentagem',
                    colorByPoint: true,
                    data: [{
                        name: 'Executadas',
                        y: 85,
                        color: "green"
                    }, {
                        name: 'N�o Executadas',
                        y: 15,
                        color: "red"
                    }]
                }]
            }, true, true);
    }

    async function updateSeriesBarGraph(options, barChart){
        var nC = await $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo IN (22,23) AND vsaf.nivel = 'C' "}
        ).error(function(msg){
            console.log(msg.responseText);
        });

        var nA = await $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo IN (22,23) AND vsaf.nivel = 'A' "}
        ).error(function(msg){
            console.log(msg.responseText);
        });

        var nB = await $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDia",
            {dados: "AND vsaf.mes ="+options.mes+" AND vsaf.ano = "+options.ano+
            " AND vsaf.cod_grupo IN (22,23) AND vsaf.nivel = 'B' "}
        ).error(function(msg){
            console.log(msg.responseText);
        });


        nC = {
            name: 'C',
            data: prepareDataGraph(nC)
        };
        nB = {
            name: 'B',
            data: prepareDataGraph(nB)
        };
        nA = {
            name: 'A',
            data: prepareDataGraph(nA)
        };

        lineChart.update(
            {
                series: [nA, nB, nC]
            }, true, true);
    }

    function setRowTableConfiabilidade(mes, ano, frota, table){

        if(mes == null){
            mes = 2;
        }
        if(ano == null){
            ano = 2018;
        }
        if(frota == null){
            frota = 'T1';
        }

        $.getJSON(
            caminho + "/api/returnResult/dados/getQtdFalhaDiaPorNivel",
            {dados: "AND vsaf.mes ="+mes+" AND vsaf.ano = "+ano+
            " AND vsaf.cod_grupo IN (22,23) AND frota = '"+frota+"' "},
            function (json) {
                if (json != null) {
                    var rows = "";
                    for (var i = 0; i < days; i++) {
                        var day = i+1;
                        if(json[day] === undefined){
                            rows += "<tr><td>"+day+"</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>";
                        }else{
                            var ctd = 0;
                            if(json[day].A == null){
                                json[day].A = 0;
                            }else{
                                ctd += json[day].A;
                            }
                            if(json[day].B == null){
                                json[day].B = 0;
                            }else{
                                ctd += json[day].B;
                            }
                            if(json[day].C == null){
                                json[day].C = 0;
                            }else{
                                ctd += json[day].C;
                            }

                            rows += "<tr><td>"+day+"</td><td>"+  json[day].A+"</td><td>"+  json[day].B+"</td><td>"+  json[day].C+"</td><td>"+ctd+"</td></tr>";
                        }
                    }

                    table.find("tbody").html(rows);
                }
            }
        ).done(function(){
        }).error(function(msg){
            alert(msg.responseText);
        });


    }

//Busca dados da api e retorna a resposta
    async function getAppMr ( month, year){

        var resposta = await $.getJSON(
            caminho + "/api/returnResult/dados/getAppMr",

            {dados: [month, year]},

            function (json) {
                if (json != null) { return json; }
            }
        ).done(function(){
        }).error(function(msg){
            alert(msg.responseText);
        });


        return resposta;
    }

//Fun��o que percorre um array analisando todos os dias do m�s e preenchendo os dias vazios com zero.
    function prepareDataGraph (json){
        var arr = [];
        if (json.length) {
            var ctdJ = 0;
            var countResult = json.length;

            for (var i = 0; i < days-1; i++) {

                var day = i+1;

                if(ctdJ < countResult) {
                    for(var j = ctdJ; j < countResult; j++) {
                        var diaSql = json[j]['dia'];
                        if ( day < diaSql) {
                            arr.push([ i, 0]);
                            break;
                        } else {
                            if (diaSql == day) {
                                arr.push([i, json[j]['valor']]);
                                ctdJ = j+1;
                                break;
                            }else{
                                arr.push ([ i, 0] );
                                break;
                            }
                        }
                    }
                }else{
                    arr.push([ i, 0]);
                }
            }
        }

        return arr;
    }

});
