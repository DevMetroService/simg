/**
 * Created by abner.andrade on 06/12/2017.
 */


/**
 * CONFIGURACOES INICIAIS
 * */

$('#btnAcaoFinalizar').attr('disabled', 'disabled');
$('#btnPrint').attr('disabled', 'disabled');


var tabelaDeCompra;
$(document).ready(function () {
    tabelaDeCompra = $('#tableListaCompras').dataTable(configTable(0, false, 'desc'));
});

/**
 * var contaLinhas,
 *  É utilizada junto com a função
 *  ordenaItensDatabela() para realinhar a
 *  numeracao de itens na tabela. *
 */
var contaLinhasTabela = 0;

var dadosDaRequisição = {}; //Um objeto que guarda os dados da Requisição

var listaMateriais = []; //Vetor de Objetos, Guarda a lista dos Materiais Requisitados


/**
 * EVENTOS
 * */

$('select[name="descMaterial"]').change(function () {
    $('#codMaterial').val($(this).val());
});


$('select[name="nomeFornecedor"]').change(function () {
    $('#codFornecedor').val($(this).val());
});


$('#btnAddPedido').click(function () {

    var resultado = validacao_formulario(
        $('#addPedidoCompra input'),
        $('#addPedidoCompra select'),
        $('#addPedidoCompra textarea'));

    if (resultado['permissao']) {

        adicionarPedidosCompra();

        $('#btnAcaoFinalizar').removeAttr('disabled'); //  Desbloqueia o botao de finalizar pedido
        $('#btnPrint').removeAttr('disabled'); //  Desbloqueia o botao de Impressao

    } else {
        alertaJquery('Falta de Informações', 'Há campos obrigatórios vazios.', 'alert');
    }

});


$(document).on('click', '.removerLinha', function (e) {

    e.preventDefault();

    $(this).closest('tr').fadeOut(350, function () {
        var row = $(this).closest('tr');
        tabelaDeCompra.fnDeleteRow(row[0]);

        contaLinhasTabela--;
        if (contaLinhasTabela == 0) {
            $('#btnAcaoFinalizar').attr('disabled', 'disabled');
            $('#btnPrint').attr('disabled', 'disabled');
        } else {
            ordenarItensDaTabela();
        }

    });


});


$(document).on('click', '#btnFinalizarPedidoCompra', function () {


    var resultado = validacao_formulario(
        $('#dadosDaRequisicao input'),
        $('#dadosDaRequisicao select'),
        $('#dadosDaRequisicao textarea')
    );

    if (resultado['permissao']) {

        pegaListaDeMateriais();
        pegaDadosDaRequisicao(listaMateriais);

        alert(JSON.stringify(dadosDaRequisição));

        $.post(
            caminho + '/cadastroMateriais/vardumppost',
            dadosDaRequisição,
            function (json) {
                alert(json);
                //$(this).html(json);
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });


    } else {
        alertaJquery('Falta de Informações', 'Há campos obrigatórios vazios.', 'alert');
    }


});


/**
 * FUNCOES
 * */

function adicionarPedidosCompra() {

    var quantSolicitada = $('input[name="qtdSolicitada"]').val();
    var valUnitario = $('input[name="valorUnitarioMaterial"]').val();

    var aux = valUnitario;
    aux = aux.replace('R$ ', '');
    aux = aux.replace(/[.]/g, '');
    aux = aux.replace(',', '.');


    var calcValTotal = parseInt(quantSolicitada) * parseFloat(aux);


    $('input[name="valorUnitarioMaterial"]').val(calcValTotal); //Coloca o valor novamente no campo para ser re-formatado de acordo com a mascara
    aux = $('input[name="valorUnitarioMaterial"]').val(); //pega o valor re-formatado

    var dadosDoForm = {
        codMaterial: $('input[name="codMaterial"]').val(),
        descMaterial: $('select[name="descMaterial"] option:selected').html(),//$('select[name="descMaterial"] option:selected').html(),
        qtdSolicitada: $('input[name="qtdSolicitada"]').val(),
        unidade: $('select[name="unidade"]').val(),
        valorUnitarioMaterial: valUnitario,
        valorTotalMateriais: aux,
        prazoEntregaMaterial: $('input[name="prazoEntregaMaterial"]').val()
    };

    tabelaDeCompra.fnAddData([
        '',
        dadosDoForm.codMaterial + '-' + dadosDoForm.descMaterial,
        dadosDoForm.qtdSolicitada,
        dadosDoForm.unidade,
        dadosDoForm.valorUnitarioMaterial,
        dadosDoForm.valorTotalMateriais,
        dadosDoForm.prazoEntregaMaterial,
        '<button class="removerLinha btn btn-circle btn-danger"><i class="fa fa-close"></i></button>'
    ]);


    // Limpa os campos do Formulario que Adiciona os Materiais
    $('#addPedidoCompra').each(function () {
        this.reset();
    });

    contaLinhasTabela++;
    ordenarItensDaTabela();

}


function ordenarItensDaTabela() {

    var table = $('#tableListaCompras');

    var n_item = 0;
    var cols = 0;

    table.find('tr').each(function (indice) {
        cols = 0;

        if (n_item > 0) {

            $(this).find('td').each(function (indice) {

                if (cols == 0) {
                    $(this).html('' + n_item);
                }

                cols++;
            });
        }

        n_item++;
    });

}


function pegaListaDeMateriais() {

    // console.log(tabelaDeMateriais.fnGetData());


    var itemDaLista;    //Ira armazenar a lista de Materiais da tabela, na forma de um objeto

    var table = $('#tableListaCompras');

    table.find('tr').each(function (indice) {
        var cols = 0;

        // Para cada linha percorrida, um novo objeto é criado para armazenar os dados

        itemDaLista = {
            codMaterial: '',
            descMaterial: '',
            qtdSolicitada: '',
            unidade: '',
            valorUnitarioMaterial: '',
            valorTotalMateriais: '',
            prazoEntregaMaterial: ''
        };


        // console.log($(this));

        $(this).find('td').each(function (indice) {
            //Percorre as colunas da linha atual
            // console.log($(this));
            var dado;
            var nChars;

            switch (cols) {
                case 1:
                    dado = $(this).html();
                    dado = dado.split("-");
                    itemDaLista.codMaterial = dado[0];
                    itemDaLista.descMaterial = dado[1];
                    break;
                case 2:
                    itemDaLista.qtdSolicitada = $(this).html();
                    break;
                case 3:
                    itemDaLista.unidade = $(this).html();
                    break;
                case 4:
                    itemDaLista.valorUnitarioMaterial = $(this).html();
                    break;
                case 5:
                    itemDaLista.valorTotalMateriais = $(this).html();
                    break;
                case 6:
                    itemDaLista.prazoEntregaMaterial = $(this).html();
                    break;
            }

            cols++;
        });

        console.log(itemDaLista);

        listaMateriais.push(itemDaLista);

        cols = 0;

    });

    listaMateriais.shift(); //Remove o indice referente ao cabecalho da tabela
    console.log(listaMateriais);

}


function pegaDadosDaRequisicao(listaMateriais) {
    dadosDaRequisição = {
        dados: [
            {
                codPedidoCompra: $('input[name="codPedidoCompra"]').val(),
                dataPedido: $('input[name="dataPedido"]').val(),
                nomeFornecedor: $('select[name="nomeFornecedor"] option:selected').html(),
                codFornecedor: $('input[name="codFornecedor"]').val(),
                contatoFornecedor: $('input[name="contatoFornecedor"]').val(),
                departContatoFornecedor: $('input[name="departContatoFornecedor"]').val(),
                telefoneContatoFornecedor: $('input[name="telefoneContatoFornecedor"]').val(),
                emailContatoFornecedor: $('input[name="emailContatoFornecedor"]').val(),
                enderecoFornecedor: $('input[name="enderecoFornecedor"]').val(),
            }
        ],
        listaMaterial: listaMateriais
    };

    console.log(dadosDaRequisição);

}