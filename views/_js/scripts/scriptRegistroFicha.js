$(".tableIS").dataTable(configTable(0, false, 'asc'));

var codigo = $('#cod_ficha');
var grupo = $('#grupo');
var sistema = $('#sistema');
var subsistema = $('#subsistema');
var servico = $('#servico');
var denominacao = $('#denominacao');
var ficha = $('#ficha');

var labelBtn = $('#labelBtn');

grupo.change(function () {
    apiGrupoSistema($(this), sistema);
    apiGetServicoMr($(this), servico);

    $('select[name="subSistemaSaf"]').html('<option disabled="disabled" selected="selected">Selecione o Sub-Sistema</option>');
});

sistema.change(function () {
    apiSistemaSub($(this), subsistema);
});

$('#resetBtn').click(function(){
    labelBtn.html('Salvar');
});

$(document).on("click",".btnEditar", function(e){
    e.preventDefault();

    labelBtn.html('Editar');

    var dados = $(this).parent("td").parent("tr").find('td');

    codigo.val(dados.html());
    denominacao.val(dados.next().html());
    grupo.val(dados.next().next().data('value'));

    apiGetServicoMr(grupo, servico, dados.next().next().next().data('value'));

    ficha.val(dados.next().next().next().next().html());
    // if(dados.next().next().next().next().data('value') == "s")
    //     ativo.prop("checked" ,  true);
    // else{
    //     ativo.prop("checked" ,  false);
    // }
});


$('#btnSave').click(function(e){
    e.preventDefault();
    if(sistema.val() !== '' && subsistema.val() !== '' && servico.val() !== '' && denominacao !== '' && ficha !== ''){
        $('#formRegFicha').submit()
    }else{
        alertaJquery('Sem informa��o', 'N�o h� informa��o para registro em tabela', 'error');
    }
});
