var tableModal = $(".tableRelModal").dataTable(configTable(0, false, 'asc'));

$(document).ready(function () {
    $("#indicadorCronogramaEd").dataTable(configTable(3, false, 'asc'));

    $("#indicadorCronogramaRa").dataTable(configTable(2, false, 'asc'));

    $("#indicadorCronogramaSu").dataTable(configTable(2, false, 'asc'));

    $("#indicadorCronogramaVp").dataTable(configTable(2, false, 'asc'));

    $("#indicadorCronogramaTl").dataTable(configTable(2, false, 'asc'));

    $("#indicadorCronogramaBl").dataTable(configTable(2, false, 'asc'));

    $("#indicadorAnaliseSsm").dataTable(configTable(2, false, 'asc'));
});

//========================================Acmpanhamento Cronograma (Barra)============================================//

var data = new Date();
var mesAtual = data.getMonth();

$.getJSON(
    caminho + "/api/returnResult/mes/getNumberEngSupervisao",
    {mes: mesAtual + 1},
    function (json) {
        $('#quantidadeEd').html(json.ativosEd);
        $('#quantidadeVp').html(json.ativosVp);
        $('#quantidadeSb').html(json.ativosSu);
        $('#quantidadeRa').html(json.ativosRa);
        $('#quantidadeTl').html(json.ativosTl);
        $('#quantidadeBl').html(json.ativosBl);
        $('#quantidadeVlt').html(json.ativosVlt);

        var abertas = [json.abertaEd, json.abertaVp, json.abertaSu, json.abertaRa, json.abertaTl, json.abertaBl];//, json.abertaVlt];
        abertas = abertas.map(Number);
        var execucao = [json.execucaoEd, json.execucaoVp, json.execucaoSu, json.execucaoRa, json.execucaoTl, json.execucaoBl];//, json.execucaoVlt];
        execucao = execucao.map(Number);
        var autorizadas = [json.autorizadaEd, json.autorizadaVp, json.autorizadaSu, json.autorizadaRa, json.autorizadaTl, json.autorizadaBl];//, json.autorizadaVlt];
        autorizadas = autorizadas.map(Number);
        var pendentes = [json.pendenteEd, json.pendenteVp, json.pendenteSu, json.pendenteRa, json.pendenteTl, json.pendenteBl];//, json.pendenteVlt];
        pendentes = pendentes.map(Number);
        var programadas = [json.programadaEd, json.programadaVp, json.programadaSu, json.programadaRa, json.programadaTl, json.programadaBl];//, json.programadaVlt];
        programadas = programadas.map(Number);
        var executadas = [json.executadaEd, json.executadaVp, json.executadaSu, json.executadaRa, json.executadaTl, json.executadaBl];//, json.executadaVlt];
        executadas = executadas.map(Number);
        var canceladas = [json.canceladaEd, json.canceladaVp, json.canceladaSu, json.canceladaRa, json.canceladaTl, json.canceladaBl];//, json.canceladaVlt];
        canceladas = canceladas.map(Number);
        var reprogramadas = [json.reprogramadaEd, json.reprogramadaVp, json.reprogramadaSu, json.reprogramadaRa, json.reprogramadaTl, json.reprogramadaBl];//, json.reprogramadaVlt];
        reprogramadas = reprogramadas.map(Number);

        subtitleAno = $("input[name='title']").val();
        subtitleMes = $("select[name='mesBarDashboard'] option:selected").html().replace(/[�]/g,"c");

        Highcharts.chart('cronograma', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Acompanhamento Cronograma'
            },
            subtitle: {
                text: subtitleMes + ' ' +subtitleAno
            },
            xAxis: {
                categories: ["Edificacoes", "Via Permanente", "Subestacao", "Rede Aerea", "Telecom", "Bilhetagem"],//, "MR - VLT"],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Quantidade - (un)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: plotOptionsBar(''),
            credits: {
                enabled: false
            },
            colors: ['#B0BEC5', '#03A9F4', '#FFEB3B', '#FF5722', '#263238', '#795548', '#4CAF50'],//, '#d50000'],
            series: [{
                name: 'Abertas',
                data: abertas

            }, {
                name: 'Execucao',
                data: execucao

            }, {
                name: 'Autorizadas',
                data: autorizadas

            }, {
                name: 'Pendentes',
                data: pendentes

            }, {
                name: 'Programadas',
                data: programadas

            }, {
                name: 'Reprogramadas',
                data: reprogramadas

            }, {
                name: 'Executadas',
                data: executadas

            }, {
                name: 'Canceladas',
                data: canceladas

            }]
        });

    }
);

$('select[name="mesBarDashboard"]').change(function () {

    var mes = $(this).val();
    $.getJSON(
        caminho + "/api/returnResult/mes/getNumberEngSupervisao",
        {mes: mes},
        function (json) {
            var abertas = [json.abertaEd, json.abertaVp, json.abertaSu, json.abertaRa, json.abertaTl, json.abertaBl];//, json.abertaVlt];
            abertas = abertas.map(Number);
            var execucao = [json.execucaoEd, json.execucaoVp, json.execucaoSu, json.execucaoRa, json.execucaoTl, json.execucaoBl];//, json.execucaoVlt];
            execucao = execucao.map(Number);
            var autorizadas = [json.autorizadaEd, json.autorizadaVp, json.autorizadaSu, json.autorizadaRa, json.autorizadaTl, json.autorizadaBl];//, json.autorizadaVlt];
            autorizadas = autorizadas.map(Number);
            var pendentes = [json.pendenteEd, json.pendenteVp, json.pendenteSu, json.pendenteRa, json.pendenteTl, json.pendenteBl];//, json.pendenteVlt];
            pendentes = pendentes.map(Number);
            var programadas = [json.programadaEd, json.programadaVp, json.programadaSu, json.programadaRa, json.programadaTl, json.programadaBl];//, json.programadaVlt];
            programadas = programadas.map(Number);
            var executadas = [json.executadaEd, json.executadaVp, json.executadaSu, json.executadaRa, json.executadaTl, json.executadaBl];//, json.executadaVlt];
            executadas = executadas.map(Number);
            var canceladas = [json.canceladaEd, json.canceladaVp, json.canceladaSu, json.canceladaRa, json.canceladaTl, json.canceladaBl];//, json.canceladaVlt];
            canceladas = canceladas.map(Number);
            var reprogramadas = [json.reprogramadaEd, json.reprogramadaVp, json.reprogramadaSu, json.reprogramadaRa, json.reprogramadaTl, json.reprogramadaBl];//, json.reprogramadaVlt];
            reprogramadas = reprogramadas.map(Number);

            subtitleAno = $("input[name='title']").val();
            subtitleMes = $("select[name='mesBarDashboard'] option:selected").html().replace(/[�]/g,"c");

            Highcharts.chart('cronograma', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Acompanhamento Cronograma'
                },
                subtitle: {
                    text: subtitleMes + ' ' +subtitleAno
                },
                xAxis: {
                    categories: ["Edificacoes", "Via Permanente", "Subestacao", "Rede Aerea", "Telecom", "Bilhetagem"],//, "MR - VLT"],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantidade - (un)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: plotOptionsBar(''),
                credits: {
                    enabled: false
                },
                colors: ['#B0BEC5', '#03A9F4', '#FFEB3B', '#FF5722', '#263238', '#795548', '#4CAF50'],//, '#d50000'],
                series: [{
                    name: 'Abertas',
                    data: abertas,

                }, {
                    name: 'Execucao',
                    data: execucao

                }, {
                    name: 'Autorizadas',
                    data: autorizadas

                }, {
                    name: 'Pendentes',
                    data: pendentes

                }, {
                    name: 'Programadas',
                    data: programadas

                }, {
                    name: 'Reprogramadas',
                    data: reprogramadas

                }, {
                    name: 'Executadas',
                    data: executadas

                }, {
                    name: 'Canceladas',
                    data: canceladas

                }]
            });

        }
    );
});

//====================================================================================================================//

$(document).on("click",".abrirModalCronograma", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var cronograma  = dados.split('<input')[0];
    var grupo       = dados.split('value="')[1].split('"')[0];

    apiModalCronograma(cronograma, grupo, tableModal);
});

$(document).on("change","#ModalCronogramaEd select[name='mes']", function(){
    if($(this).val() == '1' && $("#ModalCronogramaEd input[name='ano']").val() == 2017){
        $("#ModalCronogramaEd select[name='servico']").val('');

        $("#ModalCronogramaEd select[name='servico']").attr('disabled', 'disabled');
    }else{
        $("#ModalCronogramaEd select[name='servico']").removeAttr('disabled');
    }
});
$(document).on("change","#ModalCronogramaVp select[name='mes']", function(){
    if($(this).val() == '1' && $("#ModalCronogramaVp input[name='ano']").val() == 2017){
        $("#ModalCronogramaVp select[name='servico']").val('');

        $("#ModalCronogramaVp select[name='servico']").attr('disabled', 'disabled');
    }else{
        $("#ModalCronogramaVp select[name='servico']").removeAttr('disabled');
    }
});
$(document).on("change","#ModalCronogramaSu select[name='mes']", function(){
    if(($(this).val() == '1' || $(this).val() == '2' || $(this).val() == '3') && $("#ModalCronogramaSu input[name='ano']").val() == 2017){
        $("#ModalCronogramaSu select[name='servico']").val('');

        $("#ModalCronogramaSu select[name='servico']").attr('disabled', 'disabled');
    }else{
        $("#ModalCronogramaSu select[name='servico']").removeAttr('disabled');
    }
});
$(document).on("change","#ModalCronogramaRa select[name='mes']", function(){
    if(($(this).val() == '1' || $(this).val() == '2' || $(this).val() == '3' || $(this).val() == '4' || $(this).val() == '5') && $("#ModalCronogramaRa input[name='ano']").val() == 2017){
        $("#ModalCronogramaRa select[name='servico']").val('');

        $("#ModalCronogramaRa select[name='servico']").attr('disabled', 'disabled');
    }else{
        $("#ModalCronogramaRa select[name='servico']").removeAttr('disabled');
    }
});


$(document).on("click",".btnEditarOsmp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');

    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form  = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsmp",
        data: {codigoOs: dados},
        success: function(data) {
            window.location.replace(caminho+"/dashboardGeral/osmp/"+form);
        }
    });
});