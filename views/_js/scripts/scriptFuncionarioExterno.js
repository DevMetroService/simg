/**
 * Created by Rycker on 13/06/2016.
 */

$('document').ready(function(){
    $('input[name="matriculaPj"]').on('blur', function () {
        var matricula = $(this);
        var matriculaAntiga = $('input[name="matriculaAntiga"]');
        var tipo = $('select[name="tipoPj"]').val();
        if(!tipo)
            tipo = $('input[name="tipoPj"]').val();

        if(matricula.val() == matriculaAntiga.val())
            return;

        var prefix = matricula.val().substring(0, 3);
        switch (tipo){
            case '3': // MPE
                if(prefix != '800'){
                    alertaJquery("Alerta", 'Prefixo ou tipo de Cadastro incorreto', "alert");
                    matricula.val("");
                    return;
                }
                break;
            case '4': // CRJ
                if(prefix != '900'){
                    alertaJquery("Alerta", 'Prefixo ou tipo de Cadastro incorreto', "alert");
                    matricula.val("");
                    return;
                }
                break;
            case '5': // PJ
                if(prefix != '700'){
                    alertaJquery("Alerta", 'Prefixo ou tipo de Cadastro incorreto', "alert");
                    matricula.val("");
                    return;
                }
                break;
            default:
                alertaJquery("Alerta", 'Selecione o tipo de Cadastro', "alert");
                matricula.val("");
                return;
        }

        $.getJSON(
            caminho + "/api/returnResult/matricula",
            {matricula: matricula.val()},
            function (json) {
                if (json.statusPesquisa) {
                    alertaJquery("Alerta", 'Matr�cula j� cadastrada.<br />Pertence � <strong>' + json.nome + '</strong>', "alert");
                    matricula.val("");
                }
            }
        );
    });
});

$('.btnSalvarContinuar').on("click", function (e) {
    if (hasProcess(e)) return;

    var resultado = validacao_formulario($('#formFuncionario input'), $('#formFuncionario select'), $('#formFuncionario textarea'));

    if (resultado['permissao']) {
        var matricula = $('input[name="matriculaPj"]');
        var matriculaAntiga = $('input[name="matriculaAntiga"]');
        var tipo = $('select[name="tipoPj"]').val();
        if(!tipo)
            tipo = $('input[name="tipoPj"]').val();

        if(matricula.val() == matriculaAntiga.val()) {
            loader();
            $("#formFuncionario").submit();
            return;
        }

        var prefix = matricula.val().substring(0, 3);
        switch (tipo){
            case '3': // MPE
                if(prefix != '800'){
                    alertaJquery("Alerta", 'Prefixo ou tipo de Cadastro incorreto', "alert");
                    matricula.val("");
                    $('body').removeClass('processing');
                    return;
                }
                break;
            case '4': // CRJ
                if(prefix != '900'){
                    alertaJquery("Alerta", 'Prefixo ou tipo de Cadastro incorreto', "alert");
                    matricula.val("");
                    $('body').removeClass('processing');
                    return;
                }
                break;
            case '5': // PJ
                if(prefix != '700'){
                    alertaJquery("Alerta", 'Prefixo ou tipo de Cadastro incorreto', "alert");
                    matricula.val("");
                    $('body').removeClass('processing');
                    return;
                }
                break;
            default:
                alertaJquery("Alerta", 'Selecione o tipo de Cadastro', "alert");
                matricula.val("");
                $('body').removeClass('processing');
                return;
        }

        $.getJSON(
            caminho + "/api/returnResult/matricula",
            {matricula: matricula.val()},
            function (json) {
                if (json.statusPesquisa) {
                    alertaJquery("Alerta", 'Matr�cula j� cadastrada.<br />Pertence � <strong>' + json.nome + '</strong>', "alert");
                    matricula.val("");
                    $('body').removeClass('processing');
                }else{
                    loader();
                    $("#formFuncionario").submit();
                }
            }
        );
    } else {
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});