//====================================================================================================================//
//=== Dados Gerais ===//

$('#footer').hide();

//====================================================================================================================//

//====================================================================================================================//
//=== Button Impress�o ===//

$(".imprimir").click(function () {
    $('.highcharts-button-box').hide();
    $('.highcharts-button-symbol').hide();
    window.print();
    window.close();
});

//====================================================================================================================//

function retira_acentos(palavra) {
    com_acento = '|����������������������������������������������';
    sem_acento = '|aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    nova = '';
    for (i = 0; i < palavra.length; i++) {
        if (com_acento.search(palavra.substr(i, 1)) >= 0) {
            nova += sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1);
        }
        else {
            nova += palavra.substr(i, 1);
        }
    }
    return nova;
}

//====================================================================================================================//
//=== Gr�fico Dados ===//

//T�tulo//
var titulo = $('select[name = "Titulo"]').val();
var subtitulo = $('input[name = "Subtitulo"]').val();

//Form//
var form = $('input[name = "Form"]').val();

//Status//
var aberta = parseInt($('input[name="Aberta"]').val());
var autorizada = parseInt($('input[name="Autorizada"]').val());
var cancelada = parseInt($('input[name="Cancelada"]').val());
var encerrada = parseInt($('input[name="Encerrada"]').val());
var programada = parseInt($('input[name="Programada"]').val());
var programado = parseInt($('input[name="Programado"]').val());
var devolvida = parseInt($('input[name="Devolvida"]').val());
var encaminhado = parseInt($('input[name="Encaminhado"]').val());
var pendente = parseInt($('input[name="Pendente"]').val());
var duplicado = parseInt($('input[name="Duplicado"]').val());
var execucao = parseInt($('input[name="Execu��o"]').val());
var transferida = parseInt($('input[name="Transferida"]').val());
var agendada = parseInt($('input[name="Agendada"]').val());
var naoConfiguraFalha = parseInt($('input[name="N�o Configura falha"]').val());
var naoExecutado = parseInt($('input[name="N�o Executado"]').val());

if (isNaN(aberta) == true) {
    aberta = 0;
}
if (isNaN(autorizada) == true) {
    autorizada = 0;
}
if (isNaN(cancelada) == true) {
    cancelada = 0;
}
if (isNaN(encerrada) == true) {
    encerrada = 0;
}
if (isNaN(programada) == true) {
    programada = 0;
}
if (isNaN(programado) == true) {
    programado = 0;
}
if (isNaN(devolvida) == true) {
    devolvida = 0;
}
if (isNaN(encaminhado) == true) {
    encaminhado = 0;
}
if (isNaN(pendente) == true) {
    pendente = 0;
}
if (isNaN(duplicado) == true) {
    duplicado = 0;
}
if (isNaN(execucao) == true) {
    execucao = 0;
}
if (isNaN(transferida) == true) {
    transferida = 0;
}
if (isNaN(agendada) == true) {
    agendada = 0;
}
if (isNaN(naoConfiguraFalha) == true) {
    naoConfiguraFalha = 0;
}
if (isNaN(naoExecutado) == true) {
    naoExecutado = 0;
}

//Dados dos Status//
switch (form) {
    case 'saf':
        titulo = " - SAF";
        subtitulo = retira_acentos(subtitulo);
        var colors = ['#B0BEC5', '#FFEB3B', '#d50000', '#4CAF50', '#263238']
        var series = [{
            name: 'ABERTAS',
            data: [aberta]
        }, {
            name: 'AUTORIZADAS',
            data: [autorizada]
        }, {
            name: 'CANCELADAS',
            data: [cancelada]
        }, {
            name: 'ENCERRADAS',
            data: [encerrada]
        }, {
            name: 'PROGRAMADAS',
            data: [programada]
        }]
        break;
    case 'ssm':
        titulo = " - SSM";
        subtitulo = retira_acentos(subtitulo);
        var colors = ['#B0BEC5', '#FFEB3B', '#d50000', '#EC407A', '#9C27B0', '#4CAF50', '#FF5722', '#607D8B', '#03A9F4', '#263238']
        var series = [{
            name: 'ABERTAS',
            data: [aberta]
        }, {
            name: 'AUTORIZADAS',
            data: [autorizada]
        }, {
            name: 'CANCELADAS',
            data: [cancelada]
        }, {
            name: 'DEVOLVIDAS',
            data: [devolvida]
        }, {
            name: 'ENCAMINHADOS',
            data: [encaminhado]
        }, {
            name: 'ENCERRADAS',
            data: [encerrada]
        }, {
            name: 'PENDENTES',
            data: [pendente]
        }, {
            name: 'TRANFERIDAS',
            data: [transferida]
        }, {
            name: 'AGENDADAS',
            data: [agendada]
        }, {
            name: 'PROGRAMADAS',
            data: [programada]
        }]
        break;
    case 'osm':
        titulo = " - OSM";
        subtitulo = retira_acentos(subtitulo);
        var colors = ['#FF5722', '#4CAF50', '#03A9F4', '#3F51B5', '#d50000']
        var series = [{
            name: 'DUPLICADOS',
            data: [duplicado]
        }, {
            name: 'ENCERRADOS',
            data: [encerrada]
        }, {
            name: 'EXECUCAO',
            data: [execucao]
        }, {
            name: 'NAO CONFIGURA FALHA',
            data: [naoConfiguraFalha]
        }, {
            name: 'NAO EXECUTADOS',
            data: [naoExecutado]
        }]
        break;
    case 'ssp':
        titulo = " - SSP";
        subtitulo = retira_acentos(subtitulo);
        var colors = ['#FFEB3B', '#d50000', '#4CAF50', '#FF5722', '#263238']
        var series = [{
            name: 'AUTORIZADAS',
            data: [autorizada]
        }, {
            name: 'CANCELADAS',
            data: [cancelada]
        }, {
            name: 'ENCERRADAS',
            data: [encerrada]
        }, {
            name: 'PENDENTES',
            data: [pendente]
        }, {
            name: 'PROGRAMADAS',
            data: [programado]
        }]
        break;
    case 'osp':
        titulo = " - OSP";
        subtitulo = retira_acentos(subtitulo);
        var colors = ['#FF5722', '#4CAF50', '#03A9F4', '#d50000']
        var series = [{
            name: 'DUPLICADOS',
            data: [duplicado]
        }, {
            name: 'ENCERRADAS',
            data: [encerrada]
        }, {
            name: 'EXECUCAO',
            data: [execucao]
        }, {
            name: 'NAO EXECUTADOS',
            data: [naoExecutado]
        }]
        break;
}

//====================================================================================================================//

//====================================================================================================================//
//=== Gr�fico Status ===//

Highcharts.chart('chart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Relatorio de Status' + titulo
    },
    subtitle: {
        text: subtitulo
    },
    xAxis: {
        categories: [],
        crosshair: false,
        labels: {
            enabled: false
        }
    },
    yAxis: {
        min: 0,
        labels: {
            format: '{value}'
        },
        title: {
            text: 'Quantidade de Ocorrencia(s) - (un)'
        }
    },
    credits: {
        enabled: false
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: plotOptionsBar(''),
    colors: colors,
    series: series
});

//====================================================================================================================//