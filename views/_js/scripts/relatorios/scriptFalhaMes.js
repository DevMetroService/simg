/**
 * Created by ricardo.diego on 30/10/2017.
 */

$('select[name="linha"]').change(function () {
    apiLinhaTrecho($(this), $('select[name="trecho"]'));
});

$('select[name="trecho"]').change(function () {
    apiTrechoPn($(this), $('select[name="pontoNotavel"]'));
});

$('select[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('select[name="sistema"]'))
});

$('select[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});

$('#gerarGrafico').click(function () {

    $.post(
        caminho + "/api/returnResult/getFalhaMes",
        $('#formRelatorio').serialize(),
        function (json) {

            var dados = json.qtdFalha;
            Highcharts.chart('grafico', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Falha por M�s'
                },
                subtitle: {
                    text: json.subtitulo
                },
                xAxis: {
                    categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
                },
                yAxis: {
                    title: {
                        text: 'Quantidade Falhas'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Falhas',
                    data: [dados.Jan, dados.Fev, dados.Mar, dados.Abr, dados.Mai, dados.Jun, dados.Jul, dados.Ago,
                        dados.Set, dados.Out, dados.Nov, dados.Dez]
                }]
            });
        }, 'json'
    ).error(function(data){
        alert(JSON.stringify(data));
    });
});