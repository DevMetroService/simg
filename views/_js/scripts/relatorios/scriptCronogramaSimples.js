/**
 * Created by ricardo.diego on 21/03/2017.
 */

$('select[name="grupo"]').change(function(){
    var dir;
    switch($(this).val()){
        case "21":
            dir = "Ed";
            break;
        case "24":
            dir = "Vp";
            break;
        case "25":
            dir = "Su";
            break;
        case "20":
            dir = "Ra";
        case "27":
            dir = "Tl";
            break;
        case "28":
            dir = "Bl";
            break;
    }
    $('form').attr('action', caminho+'/dashboardPmp/pdf'+dir+'CronoSimples');

    apiGrupoSistema($(this), $('select[name="sistema"]'));
    $('select[name="subSistema"]').html('<option disabled="disabled" selected="selected">Selecione o Sub-Sistema</option>');
});

$('select[name="sistema"]').on("change", function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});