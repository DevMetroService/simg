$(".tableRel").dataTable(configTable(5, false, 'dsc'));
var tableModal = $(".tableRelModal").dataTable({
    "language": {
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
        "search": "Filtrar ",
        "loadingRecords": "Carregando...",
        "info": "_MAX_ itens na tabela",
        "infoEmpty": "N�o h� informa��es",
        "infoFiltered": "(Filtrado de um total de _MAX_ dados)",
    },
    "paging": false
});

var acima0 = $('input[name="cat0"]').val();
var acima2 = $('input[name="cat2"]').val();
var acima5 = $('input[name="cat5"]').val();
var acima7 = $('input[name="cat7"]').val();
var acima30 = $('input[name="cat30"]').val();
var acima60 = $('input[name="cat60"]').val();

acima0 = parseInt(acima0);
acima2 = parseInt(acima2);
acima5 = parseInt(acima5);
acima7 = parseInt(acima7);
acima30 = parseInt(acima30);
acima60 = parseInt(acima60);

var total = acima0 + acima2 + acima5 + acima7 + acima30 + acima60;

$('#tabCat0').html(acima0);
$('#tabCat2').html(acima2);
$('#tabCat5').html(acima5);
$('#tabCat7').html(acima7);
$('#tabCat30').html(acima30);
$('#tabCat60').html(acima60);
$('#total').html(total);

//Gr�fico
var tituloGraf = '';

concatenarTitulo($("select[name='linha'] option:selected"));
concatenarTitulo($("select[name='trecho'] option:selected"));
concatenarTitulo($("select[name='pn'] option:selected"));

if (tituloGraf != '') {
    $separador = ' - ';
} else {
    $separador = '';
}

if ($("select[name='grupoSistema'] option:selected").val() == "") {
    tituloGraf = tituloGraf + $separador + "GRUPO GERAL";
} else {
    tituloGraf = tituloGraf + $separador + "GRUPO " + validaCaracteres($("select[name='grupoSistema'] option:selected").text().toUpperCase());
}

concatenarTitulo($("select[name='sistema'] option:selected"));
concatenarTitulo($("select[name='subSistema'] option:selected"));

function concatenarTitulo(select) {
    if (select.val() != "") {
        txt = validaCaracteres(select.text().toUpperCase());

        if (tituloGraf == '')
            tituloGraf = txt;
        else
            tituloGraf = tituloGraf + ' - ' + txt;
    }
}

function validaCaracteres(strToReplace) {
    strSChar = "����������������������������������������������";
    strNoSChars = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    var newStr = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (strSChar.indexOf(strToReplace.charAt(i)) != -1) {
            newStr += strNoSChars.substr(strSChar.search(strToReplace.substr(i, 1)), 1);
        } else {
            newStr += strToReplace.substr(i, 1);
        }
    }
    return newStr.replace(/[^a-zA-Z 0-9]/g, '').toUpperCase();
}


Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: "Acompanhamento do prazo de atendimento das SAF's"
    },
    subtitle: {
        text: tituloGraf
    },
    xAxis: {
        categories: [],
        labels: {
            enabled: false
        }
    },
    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Quantidade de SAF(s)'
        }
    },
    credits: {
        enabled: false
    },
    colors: ['#0b97c4', '#4CAF50', '#FFEB3B', '#FF9800', '#FF5722', '#D50000', '#B0BEC5'],
    tooltip: {
        headerFormat: ' ',
        pointFormat: '<span style="color:{series.color}; text-shadow: #000 0px 0px 1.5px; padding:0"> {series.name} :</span> {point.y}',
        useHTML: true
    },
    plotOptions: plotOptionsBar(''),
    series: [{
        name: 'de 0 a 2 dias',
        data: [acima0]
    }, {
        name: 'de 3 a 5 dias',
        data: [acima2]
    }, {
        name: 'de 6 a 7 dias',
        data: [acima5]
    }, {
        name: 'de 8 a 30 dias',
        data: [acima7]
    }, {
        name: 'de 31 a 60 dias',
        data: [acima30]
    }, {
        name: 'Acima de 60 dias',
        data: [acima60]
    }, {
        name: 'Total',
        data: [total]
    }]
});

Highcharts.chart('containerPrint', {
    chart: {
        type: 'column'
    },
    title: {
        text: "Acompanhamento do prazo de atendimento das SAF's"
    },
    subtitle: {
        text: tituloGraf
    },
    xAxis: {
        categories: [],
        labels: {
            enabled: false
        }
    },
    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Quantidade de SAF(s)'
        }
    },
    credits: {
        enabled: false
    },
    colors: ['#0b97c4', '#4CAF50', '#FFEB3B', '#FF9800', '#FF5722', '#D50000', '#B0BEC5'],
    tooltip: {
        headerFormat: ' ',
        pointFormat: '<span style="color:{series.color}; text-shadow: #000 0px 0px 1.5px; padding:0"> {series.name} :</span> {point.y}',
        useHTML: true
    },
    plotOptions: plotOptionsBar(''),
    series: [{
        name: 'de 0 a 2 dias',
        data: [acima0]
    }, {
        name: 'de 3 a 5 dias',
        data: [acima2]
    }, {
        name: 'de 6 a 7 dias',
        data: [acima5]
    }, {
        name: 'de 8 a 30 dias',
        data: [acima7]
    }, {
        name: 'de 31 a 60 dias',
        data: [acima30]
    }, {
        name: 'Acima de 60 dias',
        data: [acima60]
    }, {
        name: 'Total',
        data: [total]
    }]
});

//====================================================================================================================//

$(document).on("click", ".abrirModalSaf", function () {
    var saf = $(this).parent("td").parent("tr").find('td').html();

    apiModalSaf(saf, tableModal);
});

$(document).on("click", ".btnEditarOsm", function (e) {
    if (hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/refillOsm",
        data: {codigoOs: dados},
        success: function () {
            window.location.replace(caminho + "/dashboardGeral/osm");
        }
    });
});


//Auto preenchimento do trecho e via ap�s sele��o da linha
$('select[name="linha"]').on("change", function () {
    apiLinhaTrecho($('select[name="linha"]'), $('select[name="trecho"]'));

    $('select[name="pn"]').html('<option disabled="disabled" selected="selected" value="">Pontos Not�veis</option>');
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trecho"]').on("change", function () {
    apiTrechoPn($('select[name="trecho"]'), $('select[name="pn"]'))
});

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistema"]').on('change', function () {
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistema"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistema"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistema"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistema"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistema"]'));
        } else {
            $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistema"]'));
    }
});

$('.limpaFiltro').on('click', function () {
    $('select[name="linha"]').val('');
    $('select[name="trecho"]').val('');
    $('select[name="pn"]').val('');
    $('select[name="grupoSistema"]').val('');
    $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    $('select[name="sistema"]').html('<option value="">Sistema</option>');
});