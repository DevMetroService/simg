try{
    $(".tableResultRel").dataTable(configTable(2, false, 'asc', true));
}catch (err){
    alert(err.message);
}

$('select[name="grupo"]').change(function () {
    var dir;
    switch ($(this).val()) {
        case "22":
            dir = "Vlt";
            break;
        case "23":
            dir = "Tue";
            break;
        case "26":
            dir = "Loc";
            break;
    }

    apiGrupoSistema($(this), $('select[name="sistema"]'));
    $('select[name="subSistema"]').html('<option value="" selected="selected" disabled="disabled">SELECIONE UM SUBSISTEMA</option>');
    apiGetVeiculo($(this).val(), $('select[name="veiculo"]'));
});

$('select[name="sistema"]').on("change", function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});

$('.limpaFiltro').on('click', function () {
    // $('select[name="tipoOs"]').val('');
    $('select[name="status"]').val('');
    $('select[name="grupo"]').val('');
    $('select[name="sistema"]').html('<option value="">SISTEMA</option>');
    $('select[name="subSistema"]').html('<option value="">SUBSISTEMA</option>');
    $('input[name="aPartir"]').val('');
    $('input[name="ate"]').val('');
    $('select[name="veiculo"]').val('');
});

$('.filtrarRel').click(function(){
    var grupo = $('select[name="grupo"]');
    var sistema = $('select[name="sistema"]');
    var subSistema = $('select[name="subSistema"]');

    if(grupo.val() == null || sistema.val() == null || subSistema.val() == null){
        alert("H� campos obrigat�rios em branco");
    }else{
        $('#formPage').submit();
    }
});