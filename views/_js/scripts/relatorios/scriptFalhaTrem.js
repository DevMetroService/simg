/**
 * Created by ricardo.diego on 24/10/2017.
 */

$('SELECT[name="grupo"]').change(function(){
    apiGrupoSistema($(this), $('SELECT[name="sistema"]') );
});


$('#gerarGrafico').click(function(e){
    e.preventDefault();

    var dados = $('#formRelFalhaTrem').serialize();

    if($('SELECT[name="grupo"]').val() == null){
        alertaJquery('Aten��o','� necess�rio escolher um Grupo de Sistema.', 'Info');
        return;
    }

    $.post(
        caminho + "/api/returnResult/getFalhaTrem",
        dados,
        function(json){
            Highcharts.chart('grafico', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: "Falha por Sistema"
                },
                subtitle: {
                    text: json.subtitulo
                },
                xAxis: {
                    // categories: [],
                    type:'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Quantidade de SAF(s)'
                    }
                },
                credits: {
                    enabled: false
                },
                colors:['#0b97c4'],
                // colors: ['#0b97c4', '#4CAF50', '#FFEB3B', '#FF5722', '#D50000', '#B0BEC5'],
                // colors: json.cores
                tooltip: {
                    headerFormat: ' ',
                    pointFormat: '<span style="color:{series.color}; padding:0"> {series.name} :</span> {point.y}',
                    useHTML: true
                },
                plotOptions: plotOptionsBar(''),
                series: [{
                    name:'Sistemas',
                    data: json.valorX
                }]
            });
        },'json'
    )

});
