/**
 * Created by iramar.junior on 31/01/2017.
 */

//====================================================================================================================//
//===Gerar Gr�fico e Tabelas===//

$('#gerar').on('click', function () {
    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var linha = $('select[name="linha"]').val();
    var grupoSistema = $('select[name="grupoSistema"]').val();
    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
});

//====================================================================================================================//

//====================================================================================================================//
//===Gr�fico do Relat�rio OS(s)===//

$(function () {
    Highcharts.chart('resultGraph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Relat�rio OS(s)'
        },
        subtitle: {
            text: 'Edifica��es - Linha Sul - Dezembro - 2016'
        },
        xAxis: {
            categories: [
                'Preventivas',
                'Corretivas'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Quantidade de OS(s) - (Unidade)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Lan�adas',
            data: [49.9, 71.5],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }

        }, {
            name: 'Atendidas',
            data: [83.6, 78.8],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }

        }, {
            name: 'Em Atendimento',
            data: [48.9, 38.8],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
//====================================================================================================================//

//====================================================================================================================//
//===DataTable===//

$("#resultTable").dataTable(configTable(1, false, 'asc'));

//====================================================================================================================//