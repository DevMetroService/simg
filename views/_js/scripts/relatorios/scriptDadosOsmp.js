/**
 * Created by ricardo.diego on 14/09/2017.
 */
/**
 * Created by iramar.junior on 02/02/2017.
 */


//====================================================================================================================//
//Auto preenchimento do local ap�s sele��o da linha.
$('select[name="linhaPesquisaOsmp"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaOsmp"]').val() == "") {
            linha.val("*");
            apiLinhaLocal(linha, $('select[name="localPesquisaOsmp"]'));
        } else {
            $('select[name="localPesquisaOsmp"]').html('<option value="">TODOS</option>');
        }
    } else {
        linha.val($(this).val());
        apiLinhaLocal(linha, $('select[name="localPesquisaOsmp"]'));
    }
});

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoPesquisaOsmp"]').on('change', function () {
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisaOsmp"]'));
        // apiGrupoServico(grupo, $('select[name="servicoPesquisaOsmp"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisaOsmp"]').html('<option value="">TODOS</option>');
        // $('select[name="servicoPesquisaOsmp"]').html('<option value="">TODOS</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaOsmp"]'));
    apiGrupoServico(grupo, $('select[name="servicoPesquisaOsmp"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisaOsmp"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoPesquisaOsmp"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisaOsmp"]'));
        } else {
            $('select[name="subSistemaPesquisaOsmp"]').html('<option value="">TODOS</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisaOsmp"]'));
    }
});
