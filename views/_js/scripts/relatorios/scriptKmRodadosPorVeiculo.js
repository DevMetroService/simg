var dadosGraf;


$(document).on('ready', function () {

    //Script to Multiselect
    var vlt = $('#selectVeiculos #vlt option').size();
    $('#selectVeiculos #vlt').attr('label', "VLT(" + vlt + ")");
    var tue = $('#selectVeiculos #tue option').size();
    $('#selectVeiculos #tue').attr('label', "TUE(" + tue + ")");

    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"
    });

    var dados = JSON.parse( $('#dbug').val() );
   dadosGraf = dados;


    /*
    *
    * Preenche o Gr�fico
    *
    * */
    var totalDias = $('#totalDias').val();

    var series = function () {
        var array_series = [];
        dadosGraf.map(function (objeto) {
            var aux ={
                name: objeto.nome_veiculo,
                data: objeto.odometros
            };

            array_series.push(aux);
        });

        return array_series;

    };

    var mes = $('#mes option:selected').html();
    var ano = $('#ano').val();

    Highcharts.chart('container', {

        title: {
            text: 'Gr�fico de Km Rodados por Ve�culo, '+mes+' de ' +ano
        },

        subtitle: {
            text: ''
        },

        xAxis: {
            categories: Array.from({length:totalDias}, (v, k) => k+1) //retorna um array enumerado de 1 a x
        },

        yAxis: {
            title: {
                text: 'km Rodados'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },

        series: series(),

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });



    /*
    *
    * Preenche a Lista de Dias
    *
    *  */


    var listaDeDias_html = "";
    dadosGraf.map(function (objeto) {

        listaDeDias_html += criaDia(objeto.nome_veiculo, objeto.odometros);

    });
    $('#listaDeDias').html( listaDeDias_html );



    var tableModal = $(".tableKmRodados").dataTable({
        "language": {
            "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
            "search": "Filtrar ",
            "loadingRecords": "Carregando...",
            "info": "_MAX_ itens na tabela",
            "infoEmpty": "N�o h� informa��es",
            "infoFiltered": "(Filtrado de um total de _MAX_ dados)",
        },
        "paging": false
    });

});


//====================================================================================================================//



$('.limpaFiltro').on('click', function () {
    $('input[name="dataPartir"]').val('');
    $('input[name="dataAte"]').val('');
    $('select[name="linha"]').val('');
    $('select[name="trecho"]').val('');
});

$('.btn_print_rel').on('click', function(){

    var form = $('#form_rel');

    form.attr("action", caminho + "/dashboardGeral/printRelatorio/Corretivas60Dias");
    form.attr("target", "_blank");

    form.submit();

    form.removeAttr("action");
    form.removeAttr("target");
});


// ===================================================================================================================//

//FUNCOES

function criaDia (nome_veiculo, array_odometros) {

    var odometros_html = '';
    var totalOdometros = array_odometros[array_odometros.length -1] - array_odometros[0];

    x = array_odometros.map(function (odometro) {
        odometros_html += `<td>${odometro}</td>`;
    });

    var tbody = `
                <tr>
                    <td>${nome_veiculo}</td>
                    <td><b>${totalOdometros}</b></td>
                    ${odometros_html}                    
                </tr>
    `;



    var total_de_dias = array_odometros.length;
    var dias_html = '';
    for (var dia = 1; dia <= total_de_dias; dia++) {
        dias_html += `<th>DIA ${dia}</th>`;
    }

    var thead = `
        <tr>
            <th>VEICULO</th>
            <th>TOTAL</th>
            ${dias_html}            
        </tr>
    `;

    var html = `
        <!-- Dia 1 -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingDia1">
                        <a href="#dia_${nome_veiculo}" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseDia${nome_veiculo}">
                            <button class="btn btn-default"><label>${nome_veiculo}</label></button>
                        </a>
                    </div>

                    <div id="dia_${nome_veiculo}" role="tabpanel" aria-labelledby="headingDia${nome_veiculo}" class="panel-collapse out collapse" aria-expanded="false" style="height: 0px;">

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class=" table table-bordered">
                                        <thead>
                                            ${thead}
                                        </thead>
                                        <tbody>
                                            ${tbody}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    
    `;

    return html;

}