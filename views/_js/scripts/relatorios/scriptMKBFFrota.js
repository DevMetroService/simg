/**
 * Created by iramar.junior on 28/03/2017.
 */

$('select[name="grupo"]').change(function () {
    apiGetFrota($(this).val(), $('#frota'));
});

$('select[name="sistema"]').on("change", function () {
    apiSistemaSub($(this), $('select[name="subsistema"]'));
});

$('select[name="veiculo"]').on('change', function () {
    apiCarro('veiculo', $(this).val(), $('select[name="carro"]'));
});

$('.limpaFiltro').on('click', function () {
    $('input[name="aPartir"]').val('');
    $('input[name="ate"]').val('');
    $('select[name="grupo"]').val('');
    $('select[name="nivel"]').val('');
    $('select[name="subsistema"]').html('<option value="">TODOS</option>');
    $('select[name="sistema"]').html('<option value="">TODOS</option>');
    $('select[name="veiculo"]').html('<option value="">TODOS</option>');
    $('select[name="carro"]').html('<option value="">TODOS</option>');
    $('select[name="prefixo"]').val('');
});

$(".btnVisualizarSaf").click(function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.getJSON(
        caminho + "/api/returnResult/codigoSaf/getSafByCod",
        {codigoSaf: dados},
        function (json) {
            // alert(JSON.stringify(json));
            // alert(json.cod_saf);
            $('#codSafModal').val(json.cod_saf);
            $('#responsavelModal').val(json.usuario);
            $('#solicitanteModal').val(json.nome);
            $('#contatoSolicitanteModal').val(json.contato);
            $('#linhaModal').val(json.nome_linha);
            $('#trechoModal').val(json.nome_trecho);
            $('#pontoNotavelModal').val(json.nome_ponto);
            $('#complementoLocalModal').val(json.complemento_local);
            $('#viaModal').val(json.nome_via);
            $('#posicaoModal').val(json.posicao);
            $('#kmInicialModal').val(json.km_inicial);
            $('#kmFinalModal').val(json.km_final);
            $('#grupoModal').val(json.nome_grupo);
            $('#sistemaModal').val(json.nome_sistema);
            $('#subsistemaModal').val(json.nome_subsistema);
            $('#avariaModal').val(json.nome_avaria);
            $('#nivelModal').val(json.nivel);
            $('#veiculoModal').val(json.nome_veiculo);
            $('#carroModal').val(json.nome_carro);
            $('#prefixoModal').val(json.cod_prefixo);
            $('#odometroModal').val(json.odometro);
        }
    );

});

window.onload = valores;

function valores() {

    $("#tableMkbf").dataTable(configTable(0, false, 'asc')).show();

    var total = $("input[name='total']").val();
    var mkbf = $("input[name='mkbf']").val();

    $('#total').html(total);
    $('#mkbf').html(mkbf);

}