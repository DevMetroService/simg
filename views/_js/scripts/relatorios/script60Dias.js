$("#tableRel").dataTable(configTable(3, false, 'asc'));

$(".imprimir").click(function () {
    alertaJquery('Imprimir','Deseja Imprimir o relat�rio com o gr�fico?','confirm',[{value: 'Sim'},{value: 'N�o'},{value: 'Cancelar'}],
        function (resposta) {
            if(resposta == "Sim"){
                $('.msgBox').addClass('hidden-print');
                $('.dataTables_filter').addClass('hidden-print');

                window.print();
                //window.close();
            }else if(resposta == "N�o"){
                $('.chartRel').addClass('hidden-print');

                $('.msgBox').addClass('hidden-print');
                $('.dataTables_filter').addClass('hidden-print');

                window.print();
                //window.close();
            }
        });
});

var emAviso  = $('input[name="emAviso"]').val();
var emAlerta  = $('input[name="emAlerta"]').val();
var emMulta  = $('input[name="emMulta"]').val();

Chart.defaults.global.defaultFontSize = 25;

var chart = new Chart($('canvas[name="chart"]'), {
    type: 'horizontalBar',
    data: {
        labels: ['Em Aviso: Mais de 30 DIAS','Em Alerta: Mais de 50 DIAS','Passivel de MULTA'],
        datasets: [{
            label: "Quant. Falhas",
            data: [emAviso, emAlerta, emMulta],
            backgroundColor: "rgba(180,0,0,0.5)"
        }]
    },
    credits: {
        enabled: false
    },
    options: {
        legend: {
            display: true
        }
    }
});