var graficos = {};

var grupo = $('select[name="grupoSistema"]');

graficos["PizzaStatus"] = "<h3>Status</h3>Gr�fico barra. Compara��o das quantidades por <strong>status</strong> nos par�metros requeridos.";
graficos["LancadasAtendidas"] = "<h3>Lan�adas, Atendidas e Em Atendimento</h3>Gr�fico barra. Comparando a quantidade de registros com <strong><em>data de abertura</em></strong> ou <strong><em>programada</em></strong> (caso seja uma SSP), no <em>per�odo</em> estipulado com as <strong><em>Encerradas e Canceladas</em></strong> no mesmo per�odo.";
graficos["MateriaisUtilizados"] = "<h3>Materiais Utilizados</h3>Relat�rio de <strong>Materiais utlizados</strong> nas <strong>Ordem de Servi�os</strong> nos par�metros definidos.";
graficos["MaquinasEquipamentosUtilizados"] = "<h3>Maquinas e Equipametos Utilizados</h3>Relat�rio de <strong>Maquinas e Equipametos Utilizados</strong> nas <strong>Ordem de Servi�os</strong> nos par�metros definidos.";
graficos["HomemHora"] = "<h3>Homem Hora</h3>Gr�fico barra. Exibi��o da soma dos hor�rios de cada funcion�rio envolvido na <strong>M�o de Obra</strong> das <strong>Ordem de Servi�os</strong> nos par�metros definidos.";

var rel = $('#rel').val();

switch (rel){
    case "mu":
        $('select[name="grafico"]').val('MateriaisUtilizados');
        break;
    case "meu":
        $('select[name="grafico"]').val('MaquinasEquipamentosUtilizados');
        break;
    case "hh":
        $('select[name="grafico"]').val('HomemHora');
        break;
}

$(".ssmpDivRel, .datFiltro, .dataEncerrada, .osmpDivRel, .todosDivRel, .maquinasEquipamentosDiv, .materialRodanteDiv").hide();

$('select[name="grafico"]').on('change', function () {
    $("#formRel").attr("action", caminho + "/dashboardGeral/gerarRelatorio/" + $(this).val());
    $("#descricaoRel").html(graficos[$(this).val()]);

    $(".safDivRel, .ssmDivRel, .osmDivRel, .sspDivRel, .ospDivRel, .local, .trecho, .pn").show();
    $(".ssmpDivRel, .osmpDivRel, .todosDivRel").hide();

    switch ($(this).val()) {
        case "LancadasAtendidas":
            $(".periodo, .periodoAPartirAte, .trecho, .pn, .osmpDivRel").show();
            $(".periodoMesAno, .dataAbertura, .funcionarioDivRel, .maquinasEquipamentosDiv, .materialRodanteDiv, .datFiltro, .dataEncerrada").hide();

            $('button[name="saf"]').click();
            break;
        case "MateriaisUtilizados":
            $(".datFiltro, .dataEncerrada, .osmpDivRel, .todosDivRel, .trecho, .sistema, .subSistema, .maquinasEquipamentosDiv, .materiaisDiv, .dataAbertura").show();
            $(".periodo, .funcionarioDivRel, .safDivRel, .ssmDivRel, .servicoDiv, .encaminhamnetoDiv, .sspDivRel, .nivelDiv, .pn, .solicitanteOrigem, .avariaRelDiv, .maquinaEquipamentoDiv, .materialRodanteDiv, .datAbert, .datProg").hide();

            $('button[name="osm"]').click();
            break;
        case "MaquinasEquipamentosUtilizados":
            $(".dataAbertura, .datAbert, .osmpDivRel, .todosDivRel, .trecho, .sistema, .subSistema, .maquinasEquipamentosDiv, .maquinaEquipamentoDiv").show();
            $(".periodo, .funcionarioDivRel, .safDivRel, .ssmDivRel, .servicoDiv, .encaminhamnetoDiv, .sspDivRel, .nivelDiv, .pn, .solicitanteOrigem, .avariaRelDiv, .materiaisDiv, .materialRodanteDiv, .datFiltro, .dataEncerrada").hide();

            $('button[name="osm"]').click();
            break;
        case "HomemHora":
            $(".datAbert , .dataAbertura, .funcionarioDivRel, .osmpDivRel, .todosDivRel, .trecho, .pn").show();
            $(".periodo, .safDivRel, .ssmDivRel, .sspDivRel, .sistema, .subSistema, .maquinasEquipamentosDiv, .materialRodanteDiv, .datFiltro, .dataEncerrada").hide();

            $('button[name="osm"]').click();
            break;
        default:
            $(".dataAbertura, .trecho, .pn").show();
            $(".datFiltro, .periodo, .funcionarioDivRel, .osmpDivRel, .maquinasEquipamentosDiv, .materialRodanteDiv, .dataEncerrada").hide();

            $('button[name="saf"]').click();
    }
});

$('button[name="saf"]').click(function () {
    retirarSelecaoBotoesForm();
    selecionarFormBotton("saf");
    elementShowHide(
        ".solicitanteOrigem, .datAbert, .nivelDiv, .avariaRelDiv, .local, .trecho, .pn, .sistema, .subSistema",
        ".datProg, .origemProgRel, .servicoDiv, .encaminhamnetoDiv, .materialRodanteDiv"
    );

    grupo.html(
        "<option value=''>Grupos de Sistemas</option>" +
        "<option value='23'>MATERIAL RODANTE - TUE</option>" +
        "<option value='22'>MATERIAL RODANTE - VLT</option>" +
        "<option value='21'>EDIFICA��ES</option>" +
        "<option value='20'>REDE A�REA</option>" +
        "<option value='29'>ENERGIA</option>" +
        "<option value='25'>SUBESTA��O</option>" +
        "<option value='24'>VIA PERMANENTE</option>" +
        "<option value='31'>JARDINS E �REAS VERDES</option>" +
        "<option value='30'>GRUPO DE TRANSPORTE VERTICAL</option>" +
        "<option value='28'>BILHETAGEM</option>" +
        "<option value='27'>TELECOM</option>"
    );
});

$('button[name="ssm"]').click(function () {
    grupo.html(
        "<option value=''>Grupos de Sistemas</option>" +
        "<option value='23'>MATERIAL RODANTE - TUE</option>" +
        "<option value='22'>MATERIAL RODANTE - VLT</option>" +
        "<option value='21'>EDIFICA��ES</option>" +
        "<option value='20'>REDE A�REA</option>" +
        "<option value='29'>ENERGIA</option>" +
        "<option value='25'>SUBESTA��O</option>" +
        "<option value='24'>VIA PERMANENTE</option>" +
        "<option value='31'>JARDINS E �REAS VERDES</option>" +
        "<option value='30'>GRUPO DE TRANSPORTE VERTICAL</option>" +
        "<option value='28'>BILHETAGEM</option>" +
        "<option value='27'>TELECOM</option>"
    );
    retirarSelecaoBotoesForm();
    selecionarFormBotton("ssm");
    elementShowHide(
        ".datAbert, .nivelDiv, .servicoDiv, .encaminhamnetoDiv, .tipoIntervencaoDiv, .local, .trecho, .pn, .sistema, .subSistema",
        ".solicitanteOrigem, .datProg, .origemProgRel, .avariaRelDiv, .materialRodanteDiv"
    );
});

$('button[name="osm"]').click(function () {
    grupo.html(
        "<option value=''>Grupos de Sistemas</option>" +
        "<option value='23'>MATERIAL RODANTE - TUE</option>" +
        "<option value='22'>MATERIAL RODANTE - VLT</option>" +
        "<option value='21'>EDIFICA��ES</option>" +
        "<option value='20'>REDE A�REA</option>" +
        "<option value='29'>ENERGIA</option>" +
        "<option value='25'>SUBESTA��O</option>" +
        "<option value='24'>VIA PERMANENTE</option>" +
        "<option value='31'>JARDINS E �REAS VERDES</option>" +
        "<option value='30'>GRUPO DE TRANSPORTE VERTICAL</option>" +
        "<option value='28'>BILHETAGEM</option>" +
        "<option value='27'>TELECOM</option>"
    );
    if($('select[name="grafico"]').val() == 'MaquinasEquipamentosUtilizados'){
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osm");
        elementShowHide(
            ".datAbert, .trecho, .local, .maquinasEquipamentosDiv",
            ".materialRodanteDiv, .datFiltro, .dataEncerrada, .dataEncerrada"
        );
    }else if ($('select[name="grafico"]').val() == 'MateriaisUtilizados') {
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osm");
        elementShowHide(
            ".trecho, .local, .maquinasEquipamentosDiv, .datFiltro, .dataEncerrada, .dataEncerrada",
            ".datAbert, .materialRodanteDiv"
        );
    } else {
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osm");
        elementShowHide(
            " .nivelDiv, .servicoDiv, .local, .trecho, .pn, .sistema, .subSistema",
            ".encaminhamnetoDiv, .solicitanteOrigem, .datProg, .origemProgRel, .avariaRelDiv, .tipoIntervencaoDiv, .materialRodanteDiv, .datFiltro, .dataEncerrada, .dataEncerrada"
        );
    }
});

$('button[name="ssp"]').click(function () {
    grupo.html(
        "<option value=''>Grupos de Sistemas</option>" +
        "<option value='23'>MATERIAL RODANTE - TUE</option>" +
        "<option value='22'>MATERIAL RODANTE - VLT</option>" +
        "<option value='21'>EDIFICA��ES</option>" +
        "<option value='20'>REDE A�REA</option>" +
        "<option value='29'>ENERGIA</option>" +
        "<option value='25'>SUBESTA��O</option>" +
        "<option value='24'>VIA PERMANENTE</option>" +
        "<option value='31'>JARDINS E �REAS VERDES</option>" +
        "<option value='30'>GRUPO DE TRANSPORTE VERTICAL</option>" +
        "<option value='28'>BILHETAGEM</option>" +
        "<option value='27'>TELECOM</option>"
    );
    retirarSelecaoBotoesForm();
    selecionarFormBotton("ssp");
    elementShowHide(
        ".datProg, .origemProgRel, .servicoDiv, .encaminhamnetoDiv, .tipoIntervencaoDiv, .local, .trecho, .pn, .sistema, .subSistema",
        ".solicitanteOrigem, .datAbert, .nivelDiv, .avariaRelDiv, .materialRodanteDiv, .materialRodanteDiv"
    );
});

$('button[name="osp"]').click(function () {
    grupo.html(
        "<option value=''>Grupos de Sistemas</option>" +
        "<option value='23'>MATERIAL RODANTE - TUE</option>" +
        "<option value='22'>MATERIAL RODANTE - VLT</option>" +
        "<option value='21'>EDIFICA��ES</option>" +
        "<option value='20'>REDE A�REA</option>" +
        "<option value='29'>ENERGIA</option>" +
        "<option value='25'>SUBESTA��O</option>" +
        "<option value='24'>VIA PERMANENTE</option>" +
        "<option value='31'>JARDINS E �REAS VERDES</option>" +
        "<option value='30'>GRUPO DE TRANSPORTE VERTICAL</option>" +
        "<option value='28'>BILHETAGEM</option>" +
        "<option value='27'>TELECOM</option>"
    );

    if($('select[name="grafico"]').val() == 'MaquinasEquipamentosUtilizados'){
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osp");
        elementShowHide(
            ".datAbert, .trecho, .local, .maquinasEquipamentosDiv",
            ".materialRodanteDiv, .datFiltro, .dataEncerrada"
        );
    }else if ($('select[name="grafico"]').val() == 'MateriaisUtilizados'){
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osp");
        elementShowHide(
            ".trecho, .local, .maquinasEquipamentosDiv, .datFiltro, .dataEncerrada",
            ".datAbert, .materialRodanteDiv"
        );
    } else {
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osp");
        elementShowHide(
            ".datAbert, .origemProgRel, .servicoDiv, .local, .trecho, .pn, .sistema, .subSistema",
            ".encaminhamnetoDiv, .solicitanteOrigem, .datProg, .nivelDiv, .avariaRelDiv, .tipoIntervencaoDiv, .materialRodanteDiv"
        );
    }
});

$('button[name="osmp"]').click(function () {
    grupo.html(
        "<option value=''>Grupos de Sistemas</option>" +
        "<option value='21'>EDIFICA��ES</option>" +
        "<option value='20'>REDE A�REA</option>" +
        "<option value='25'>SUBESTA��O</option>" +
        "<option value='24'>VIA PERMANENTE</option>" +
        "<option value='28'>BILHETAGEM</option>" +
        "<option value='27'>TELECOM</option>"
    );

    if($('select[name="grafico"]').val() == 'MaquinasEquipamentosUtilizados')
    {
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osmp");
        elementShowHide(
            ".maquinasEquipamentosDiv, .datAbert",
            ".trecho, .local, .materialRodanteDiv, .datFiltro, .dataEncerrada"
        );
        grupo.removeAttr('required');

    }else if ($('select[name="grafico"]').val() == 'MateriaisUtilizados') {
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osmp");
        elementShowHide(
            ".maquinasEquipamentosDiv, .datFiltro, .dataEncerrada",
            ".datAbert, .trecho, .local, .materialRodanteDiv"
        );
        grupo.removeAttr('required');

    } else if($('select[name="grafico"]').val() == 'LancadasAtendidas'){
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osmp");
        elementShowHide(
            ".datAbert, .local, .sistema, .subSistema",
            ".solicitanteOrigem, .datProg, .nivelDiv, .avariaRelDiv, .tipoIntervencaoDiv, .encaminhamnetoDiv, .origemProgRel, .servicoDiv, .trecho, .pn, .materialRodanteDiv, .datFiltro, .dataEncerrada"
        );
        grupo.attr('required', 'required');

    } else{
        retirarSelecaoBotoesForm();
        selecionarFormBotton("osmp");
        elementShowHide(
            ".datAbert",
            ".solicitanteOrigem, .datProg, .nivelDiv, .avariaRelDiv, .tipoIntervencaoDiv, .encaminhamnetoDiv, .origemProgRel, .servicoDiv, .local, .trecho, .pn, .sistema, .subSistema, .materialRodanteDiv, .datFiltro, .dataEncerrada"
        );
        grupo.removeAttr('required');
    }
});

$('button[name="todos"]').click(function () {

    if ($('select[name="grafico"]').val() == 'MateriaisUtilizados') {
        retirarSelecaoBotoesForm();
        selecionarFormBotton("todos");
        elementShowHide(
            ".trecho, .local, .maquinasEquipamentosDiv, .datFiltro, .dataEncerrada",
            ".datAbert, .solicitanteOrigem, .datProg, .nivelDiv, .avariaRelDiv, .tipoIntervencaoDiv, .encaminhamnetoDiv, .origemProgRel, .servicoDiv, .local, .trecho, .pn, .materialRodanteDiv"
        );
    }else{
        retirarSelecaoBotoesForm();
        selecionarFormBotton("todos");
        elementShowHide(
            ".datAbert",
            ".solicitanteOrigem, .datProg, .nivelDiv, .avariaRelDiv, .tipoIntervencaoDiv, .encaminhamnetoDiv, .origemProgRel, .servicoDiv, .local, .trecho, .pn, .materialRodanteDiv, .datFiltro, .dataEncerrada"
        );
    }
});

$('select[name="grupoSistema"]').on('change', function () {
    if ($(this).val() == "" && $('button[name="osmp"]').hasClass('btn-primary') || $(this).val() == "" && $('button[name="todos"]').hasClass('btn-primary')) {
        $(".local").hide();
        $(".sistema").hide();
        $(".subSistema").hide();
        $('select[name="linha"]').val('');
    } else {
        $(".local").show();
        $(".sistema").show();
        $(".subSistema").show();
    }
});

$('.btn-default').on('click', function () {
    if (!($(this).hasClass('btn-lg'))) {
        limparCampos();
    }
});

///######  Bot�es de A��o
///######  Bot�es de A��o
///######  Bot�es de A��o

$('button[type="reset"]').click(function () {
    // $('button[name="saf"]').click();
    $('select[name="grafico"]').val('PizzaStatus');
    $('select[name="grafico"]').change();
    // $('input[name="avaria"]').val('');
    limparCampos();
});

///////////////////////////////////////////////////// PREENCHIMENTO - FORM
///////////////////////////////////////////////////// PREENCHIMENTO - FORM
///////////////////////////////////////////////////// PREENCHIMENTO - FORM


$('document').ready(function () {
    var edificacao = $('#selectFuncionario #edificacao option').size();
    $('#selectFuncionario #edificacao').attr('label', "Edifica��o(" + edificacao + ")");
    var energia = $('#selectFuncionario #energia option').size();
    $('#selectFuncionario #energia').attr('label', "Energia(" + energia + ")");
    var viaPermanente = $('#selectFuncionario #viaPermanente option').size();
    $('#selectFuncionario #viaPermanente').attr('label', "Via Permanente(" + viaPermanente + ")");
    var biTel = $('#selectFuncionario #telecom option').size();
    $('#selectFuncionario #telecom').attr('label', "Bilhetagem/Telecom(" + biTel + ")");

    // Funcionarios
    $('#selectFuncionario').multiselect({
        enableClickableOptGroups: true,

        // buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"
    });
});

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linha"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pn"]'));
    } else {
        linha.val($(this).val());
        $('select[name="pn"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trecho"]'));

    var grupo = $('select[name="grupoSistema"]').val();

    if (grupo == "22" || grupo == "23" || grupo == "26") {
        apiGetVeiculo(grupo, $('select[name="veiculo"]'), $(this).val());
    }
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trecho"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linha"]').val() == "") {
            trecho.val("*");
            apiTrechoPn(trecho, $('select[name="pn"]'));
        } else {
            $('select[name="pn"]').html('<option value="">Ponto Not�vel</option>');
        }
    } else {
        trecho.val($(this).val());
        apiTrechoPn(trecho, $('select[name="pn"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistema"]').on('change', function () {
    var grupo = ObjetoVal;

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {

        if ($('button[name="saf"]').hasClass('btn-primary') || $('button[name="ssm"]').hasClass('btn-primary') || $('button[name="osm"]').hasClass('btn-primary') || $('button[name="ssp"]').hasClass('btn-primary') || $('button[name="osp"]').hasClass('btn-primary')) {
            $('.materialRodanteDiv').show();
            clearMtRod();
        }
    } else {
        $('.materialRodanteDiv').hide();
        clearMtRod();
    }

    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistema"]'));

    } else {
        grupo.val($(this).val());
        $('select[name="subSistema"]').html('<option value="">Subsistema</option>');

    }

    apiGrupoSistema(grupo, $('select[name="sistema"]'));
    var linha = $('select[name="linha"]').val();
    apiGetVeiculo(grupo.val(), $('select[name="veiculo"]'), linha);

});

$('select[name="veiculo"]').on('change', function () {
    apiCarro('veiculo', $(this).val(), $('select[name="carro"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistema"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistema"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistema"]'));
        } else {
            $('select[name="subSistema"]').html('<option value="">Subsistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistema"]'));
    }
});

//############# AutoComplete DataList #############
if ($('input[name="avariaInput"]').val() != "") {
    dataListFuncao($('input[name="avariaInput"]'), $("#avariaDataList"), $('input[name="avaria"]'));
}

$('input[name="avariaInput"]').on("input", function () {
    dataListFuncao($('input[name="avariaInput"]'), $("#avariaDataList"), $('input[name="avaria"]'));
});

$('input[name="avariaInput"]').on("blur", function () {
    if ($('input[name="avaria"]').val() == "")
        $(this).val("");
    else
        $('input[name="avariaInput"]').val($("#avariaDataList").find("option[value='" + $('input[name="avaria"]').val() + "']").text());
});

//################ Encaminhamento ################
$('select[name="unidade"]').on("change", function () {
    var local = ObjetoVal;
    if ($(this).val() == "") {
        local.val("*");
    } else {
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipe"]'));
});


/////////////////////////////////////////// FUN��ES

function retirarSelecaoBotoesForm() {
    $('button[name="saf"], button[name="ssm"], button[name="osm"], button[name="ssp"], button[name="osp"], button[name="osmp"], button[name="todos"]').removeClass("btn-default");
    $('button[name="saf"], button[name="ssm"], button[name="osm"], button[name="ssp"], button[name="osp"], button[name="osmp"], button[name="todos"]').removeClass("btn-primary");
}

function selecionarFormBotton(form) {
    var saf = "default";
    var ssm = "default";
    var osm = "default";
    var ssp = "default";
    var osp = "default";
    var osmp = "default";
    var todos = "default";

    switch (form) {
        case "saf":
            saf = "primary";
            break;
        case "ssm":
            ssm = "primary";
            break;
        case "osm":
            osm = "primary";
            break;
        case "ssp":
            ssp = "primary";
            break;
        case "osp":
            osp = "primary";
            break;
        case "osmp":
            osmp = "primary";
            break;
        case "todos":
            todos = "primary";
            break;
    }

    $('button[name="saf"]').addClass("btn-" + saf);
    $('button[name="ssm"]').addClass("btn-" + ssm);
    $('button[name="osm"]').addClass("btn-" + osm);
    $('button[name="ssp"]').addClass("btn-" + ssp);
    $('button[name="osp"]').addClass("btn-" + osp);
    $('button[name="osmp"]').addClass("btn-" + osmp);
    $('button[name="todos"]').addClass("btn-" + todos);

    $('input[name="form"]').val(form);
}

function elementShowHide(show, hide) {
    $(show).show();
    $(hide).hide();
}

function limparCampos() {

    $('select[name="mes"]').val('');
    $('select[name="ano"]').val('');
    $('select[name="linha"]').val('');
    $('select[name="trecho"]').val('');
    $('select[name="pn"]').val('');
    $('select[name="grupoSistema"]').val('');
    $('select[name="sistema"]').val('');
    $('select[name="subSistema"]').val('');
    $('select[name="solicitanteOrigem"]').val('');
    $('select[name="origemProgRel"]').val('');
    $('select[name="nivel"]').val('');
    $('select[name="tipoIntervencao"]').val('');
    $('select[name="servico"]').val('');
    $('select[name="unidade"]').val('');
    $('select[name="equipe"]').val('');
    $('select[name="maquinasEquipamentos"]').val('');
    $('select[name="materiais"]').val('');
    // $('select[name="estado"]').val('');
    // $('select[name="origem"]').val('');
    $('select[name="veiculo"]').val('');
    $('select[name="carro"]').val('');
    $('select[name="prefixo"]').val('');

    $('input[name="dataPartirPeriodo"]').val('');
    $('input[name="dataRetrocederPeriodo"]').val('');
    $('input[name="dataPartirDatAber"]').val('');
    $('input[name="dataRetrocederDatAber"]').val('');
    $('input[name="dataPartirDatEnc"]').val('');
    $('input[name="dataRetrocederDatEnc"]').val('');
    $('input[name="avaria"]').val('');
    $('input[name="avariaInput"]').val('');
    $('input[name="odometroPartir"]').val('');
    $('input[name="odometroAte"]').val('');

}

function clearMtRod() {
    $('select[name="veiculo"]').val('');
    $('select[name="carro"]').val('');
    $('select[name="prefixo"]').val('');
    $('input[name="odometroPartir"]').val('');
    $('input[name="odometroAte"]').val('');
}

// $('select[name="grafico"]').change();