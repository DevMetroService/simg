$(".tableRel").dataTable(configTable(0, false, 'asc'));

var corEd  = parseInt($('input[name="corEd"]').val());
var corVp  = parseInt($('input[name="corVp"]').val());
var corSb  = parseInt($('input[name="corSb"]').val());
var corRa  = parseInt($('input[name="corRa"]').val());

var progEd  = parseInt($('input[name="progEd"]').val());
var progVp  = parseInt($('input[name="progVp"]').val());
var progSb  = parseInt($('input[name="progSb"]').val());
var progRa  = parseInt($('input[name="progRa"]').val());

var prevEd  = parseInt($('input[name="prevEd"]').val());
var prevVp  = parseInt($('input[name="prevVp"]').val());
var prevSb  = parseInt($('input[name="prevSb"]').val());
var prevRa  = parseInt($('input[name="prevRa"]').val());

var ed = corEd + progEd + prevEd;
var vp = corVp + progVp + prevVp;
var sb = corSb + progSb + prevSb;
var ra = corRa + progRa + prevRa;

Highcharts.setOptions({
    lang: {
        drillUpText: '<- Voltar para {series.name}'
    }
});

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Ocorrencias 60 Dias'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Quantidade de Ocorrencia(s)'
        }
    },
    legend: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }
    },
    series: [{
        name: 'Ocorrencias',
        colorByPoint: true,
        data: [{
            name: 'Edificacoes',
            y: ed,
            drilldown: 'Edificacoes'
        }, {
            name: 'Via Permanente',
            y: vp,
            drilldown: 'Via Permanente'
        }, {
            name: 'Rede Aerea',
            y: ra,
            drilldown: 'Rede Aerea'
        }, {
            name: 'Subestacao',
            y: sb,
            drilldown: 'Subestacao'
        }]
    }],
    drilldown: {
        series: [{
            name: 'Edificacoes',
            id: 'Edificacoes',
            data: [
                [
                    'Corretiva',
                    corEd
                ],
                [
                    'Programacao',
                    progEd
                ],
                [
                    'Preventivo',
                    prevEd
                ]
            ]
        }, {
            name: 'Via Permanente',
            id: 'Via Permanente',
            data: [
                [
                    'Corretiva',
                    corVp
                ],
                [
                    'Programacao',
                    progVp
                ],
                [
                    'Preventivo',
                    prevVp
                ]
            ]
        }, {
            name: 'Rede Aerea',
            id: 'Rede Aerea',
            data: [
                [
                    'Corretiva',
                    corRa
                ],
                [
                    'Programacao',
                    progRa
                ],
                [
                    'Preventivo',
                    prevRa
                ]
            ]
        }, {
            name: 'Subestacao',
            id: 'Subestacao',
            data: [
                [
                    'Corretiva',
                    corSb
                ],
                [
                    'Programacao',
                    progSb
                ],
                [
                    'Preventivo',
                    prevSb
                ]
            ]
        }]
    }
});