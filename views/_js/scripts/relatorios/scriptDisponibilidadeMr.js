var qtdMinVeiculo = $('#qtdMinVeiculo');
var totalHoras = $('#horaTotalOperacao');
var divInfo = $('#info');
var resultadoDisponibilidade = $('#resultDisp');
var salvarDados = $('#salvar');

var linha = $('#linha').data('linha');


var btnExecutar = $('.execRel');

var info = "";
var dispDiaria;


totalHoras.blur(function(){
});

qtdMinVeiculo.blur(function(){
    divInfo.html('');
    var i;
    var ctdRow = 0;
    for (i = qtdMinVeiculo.val(); i > 0 ; i--){
        if(ctdRow === 0){
            divInfo.append('<div class="row">');
        }
        divInfo.append('<div class="col-md-2">'+
            '<label>Composi��o ativos: '+ i +'</label>'+
            '<input id="horaOperacional'+ i +'" class="form-control hora" data-comp="'+i+'" type="text" name="horaTotalOperacao" placeholder="00:00">'+
            '</div>' +
            '<div class="col-md-2">' +
            '<label>Composi��o ativos:'+ (i-0.5) +'</label>' +
            '<input id="horaOperacional'+ i +'.5" class="form-control hora" data-comp="'+(i-0.5)+'" type="text" name="horaTotalOperacao" placeholder="00:00">' +
            '</div>');

        if(ctdRow >= 4 || (i - 1) === 0){
            divInfo.append('</div>');
            ctdRow = 0;
        }else{
            ctdRow += 2;
        }
    }

    $(".hora").inputmask("h:s",
        {
            dateFormat: "24"
        });  //static mask
});

var exec = function(e){
    e.preventDefault();

    info = "";

    if( qtdMinVeiculo.val() === '' || totalHoras.val() === '')
    {
        alert('Preencha corretamente os valores nos campos de NTp e Tempo Total de Opera��o');
        return false;
    }

    info += "Composi��o Programada = " +  qtdMinVeiculo.val() +" Tempo de Opera��o = " + totalHoras.val() + "</br>";

    var denominador = (qtdMinVeiculo.val() * getSec(totalHoras.val()));
    var nominador = 0;

    var ctdAux = 0;
    var ctdAuxHora = 0;
    var nti = $('#info :input');
    $.each(nti, function(index, value){
        var comp = value.dataset.comp;
        var timeSec = getSec(value.value);

        if(value.value !== ""){
            info += "Qtd em Opera��o = " + comp +", Momento Operacional = "+ value.value + "</br>";
            nominador += ( parseFloat(comp * timeSec ));
            ctdAux++;
            ctdAuxHora += timeSec;
        }
    });

    if(ctdAux === 0){
        alert("Necess�rio informar pelo menos um campo de hora operacional.");
        return false;
    }
    if(ctdAuxHora > getSec(totalHoras.val())){
        alert("Valor total de tempo de opera��o di�ria n�o pode ser maior do que o valor total de opera��o programada.");
        return false;
    }

    dispDiaria = ((nominador/denominador)*100).toFixed(2)+"%";

    resultadoDisponibilidade.html(dispDiaria);
    return true
};

btnExecutar.click(function(e){
    exec(e);
});

salvarDados.click(function(e){
    e.preventDefault();

    if(!exec(e)){
        return;
    }

    var data = $('#dataRegistro').val();

    if(!data){
        alert("Preencha a data de cadastro");
        return;
    }

    data = data.split('/');

    dia = data[0];
    mes = data[1];
    ano = data[2];

    var arr = {
        mode:           '1',
        info:           info,
        resultado:      dispDiaria,
        dia:            dia,
        mes:            mes,
        ano:            ano,
        linha:          linha
    };

    $.getJSON(
        caminho + "/api/returnResult/dados/apiDisponibilidadeMR",
        {dados: arr},
        function (json) {
            if(!json.hasDisp){
                alertaJquery("Data j� registrada.","Data informada j� possui registro. Deseja Substituir?", 'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                    if (res == "Sim") {
                        $.post(
                            caminho + "/cadastroGeral/registrarDisponibilidadeMr",
                            arr,
                            function(json){
                                alertaJquery("Informa��o", json.msg, "info");
                            }, "json"
                        ).error(function (msg) {
                            var_dump(msg);
                        });
                    }
                    if (res == "N�o") {
                        return;
                    }
                });
            }else{
                $.post(
                    caminho + "/cadastroGeral/registrarDisponibilidadeMr",
                    arr,
                    function(json){
                        alertaJquery("Informa��o", json.msg, "info");
                    }, "json"
                ).error(function (msg) {
                    var_dump(msg);
                });
            }
    }).error(function (msg) {
        var_dump(msg);
    });

});


var getSec = function(horaMin){
    var dados = horaMin.split(':');

    var hora = dados[0];
    var min = dados[1];

    return (hora*60)*60 + min*60;
};