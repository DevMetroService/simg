$(".tableRel").DataTable({
    "language": {
        "lengthMenu": "Visualidado _MENU_ registros por p�gina",
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
        "search": "Filtrar ",

        "paginate": {
            "first": "Primeiro",
            "last": "Ultimo",
            "next": "Pr�ximo",
            "previous": "Anterior"
        },
        "loadingRecords": "Carregando...",
        "info": "Um total de _MAX_ resultados",
        "infoEmpty": "N�o h� informa��es"
    },
    "paging":   false
});