/**
 * Created by iramar.junior on 28/03/2017.
 */

var selectNivel = $("#nivel");
var selectMes = $("#mes");
var selectAno = $("#ano");


selectNivel.change(function(){
    if($(this).val() == 'A'){
        selectMes.attr("disabled", "disabled");
        selectAno.attr("disabled", "disabled");
    }else{
        selectMes.removeAttr("disabled");
        selectAno.removeAttr("disabled");
    }
});
$('select[name="linha"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
    } else {
        linha.val($(this).val());
    }

    apiLinhaTrecho(linha, $('select[name="trecho"]'));
});

$('select[name="trecho"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaSaf"]').val() == "") {
            trecho.val("*");
        }
    } else {
        trecho.val($(this).val());
    }
});

// $('select[name="grupo"]').change(function () {
//     apiGrupoSistema($(this), $('select[name="sistema"]'));
//     $('select[name="subsistema"]').html('<option selected value="">TODOS</option>');
//     apiGetVeiculo($(this).val(), $('select[name="veiculo"]'), false);
// });

$('select[name="sistema"]').on("change", function () {
    apiSistemaSub($(this), $('select[name="subsistema"]'));
});

// $('select[name="veiculo"]').on('change', function () {
//     apiCarro('veiculo', $(this).val(), $('select[name="carro"]'));
// });

$('.limpaFiltro').on('click', function () {
    $('#formMTBF input').val('');
    $('#formMTBF select').val('');
    $('select[name="subsistema"]').html('<option value="">TODOS</option>');
    // $('select[name="sistema"]').html('<option value="">TODOS</option>');
    // $('select[name="carro"]').html('<option value="">TODOS</option>');
    $('select[name="trecho"]').html('<option value="">TODOS</option>');
    // $('select[name="pnPesquisaSaf"]').html('<option value="">TODOS</option>');
});


$(".btnVisualizarSaf").click(function () {
    var dados = $(this).parent("td").parent("tr").find('td').html();
    $.getJSON(
        caminho + "/api/returnResult/codigoSaf/getSafByCod",
        {codigoSaf: dados},
        function (json) {
            $('#codSafModal').val(json.cod_saf);
            $('#responsavelModal').val(json.usuario);
            $('#solicitanteModal').val(json.nome);
            $('#contatoSolicitanteModal').val(json.contato);
            $('#linhaModal').val(json.nome_linha);
            $('#trechoModal').val(json.nome_trecho);
            $('#pontoNotavelModal').val(json.nome_ponto);
            $('#complementoLocalModal').val(json.complemento_local);
            $('#viaModal').val(json.nome_via);
            $('#posicaoModal').val(json.posicao);
            $('#kmInicialModal').val(json.km_inicial);
            $('#kmFinalModal').val(json.km_final);
            $('#grupoModal').val(json.nome_grupo);
            $('#sistemaModal').val(json.nome_sistema);
            $('#subsistemaModal').val(json.nome_subsistema);
            $('#avariaModal').val(json.nome_avaria);
            $('#nivelModal').val(json.nivel);
            $('#complementoAvariaModal').val(json.complemento_falha);
        }
    );

});

window.onload = valores;

function valores() {

    $("#tableMtbf").dataTable(configTable(4, false, 'asc')).show();

    var total = $("input[name='total']").val();
    var mtbf = $("input[name='mtbf']").val();

    $('#total').html(total);
    $('#mtbf').html(mtbf);

}

$('#avaria').on("input", function () {
    dataListFuncao($('#avaria'), $("#avariaDataList"), $('input[name="avaria"]'));
});