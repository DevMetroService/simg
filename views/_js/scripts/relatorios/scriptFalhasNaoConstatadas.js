/**
 * Created by abner.andrade on 24/10/2017.
 */

var tabelaResultado;
$('document').ready(function(){
    tabelaResultado = $('#tabelaResultado').DataTable(configTable(0,false,'desc', true));
});

$('SELECT[name="linha"]').change(function () {
    apiLinhaTrecho($(this), $('SELECT[name="trecho"]'));

    if($(this).val() == "") {
        $('SELECT[name="trecho"]').html("<option value=''></option>");
    }
});


$('SELECT[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('SELECT[name="sistema"]'));

    if( ($(this).val() == "") || ($(this).val() != "") ){
        $('SELECT[name="subsistema"]').html("<option value=''>TODOS</option>");
    }
});


$('SELECT[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('SELECT[name="subsistema"]'));
});



$('#gerarGrafico').click(function (e) {
    e.preventDefault();

    $.post(
        caminho + "/api/returnResult/getNaoConstatada",
        $('#formRelFalhaNaoConstatada').serialize(),
        function (json) {
            if(!$.isEmptyObject(json)) {
                tabelaResultado.clear().draw();
                tabelaResultado.rows.add(json.tabela).draw();
            }else{
                tabelaResultado.clear().draw();
            }
            
        },'json'
    ).error(function(msg){
        console.log(msg);
    });

});

