/**
 * Created by ricardo.diego on 14/09/2017.
 */

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaOsp"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == ""){
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaOsp"]'));
    }else{
        linha.val($(this).val());
        $('select[name="pnPesquisaOsp"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaOsp"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaOsp"]').on("change", function() {
    var trecho = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="linhaPesquisaOsp"]').val() == ""){
            trecho.val("*");
            apiTrechoPn(trecho,$('select[name="pnPesquisaOsp"]'));
        }else{
            $('select[name="pnPesquisaOsp"]').html('<option value="">Ponto Not�vel</option>');
        }
    }else{
        trecho.val($(this).val());
        apiTrechoPn(trecho,$('select[name="pnPesquisaOsp"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

var mtRod = $('.mtRod');

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoPesquisaOsp"]').on('change', function () {

    var grupo = ObjetoVal;

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        mtRod.show();
    } else {
        clearMtRod();
        mtRod.hide();
    }

    if ($(this).val() == ""){
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    }else{
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaOsp"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisaOsp"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisa"]'));
});


function clearMtRod() {
    $('select[name="veiculoPesquisaOsp"]').val('');
    $('select[name="carroAvariadoPesquisaOsp"]').val('');
    $('select[name="carroLiderPesquisaOsp"]').val('');
    $('select[name="prefixoPesquisaOsp"]').val('');
    $('input[name="odometroPartirPesquisaOsp"]').val('');
    $('input[name="odometroAtePesquisaOsp"]').val('');
}

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisaOsp"]').on('change', function(){
    var sistema = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="grupoPesquisaOsp"]').val() == ""){
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        }else{
            $('select[name="subSistemaPesquisa"]').html('<option value="">Subsistema</option>');
        }
    }else{
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="unidadePesquisaOsp"]').on("change", function(){
    var local = ObjetoVal;
    if($(this).val() == ""){
        local.val("*");
    }else{
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisaOsp"]'));
});
