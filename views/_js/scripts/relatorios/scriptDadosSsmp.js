/**
 * Created by ricardo.diego on 14/09/2017.
 */
/**
 * Created by iramar.junior on 02/02/2017.
 */

if($('select[name="grupoSistemaPesquisa"]').val() == 21){
    $("#estacaoInicial").hide();
    $("#estacaoFinal").hide();
}

$('select[name="linha"]').change(function(){
    apiLinhaLocalgrupo($(this), $('select[name="grupoSistemaPesquisa"]'), $('select[name="localPesquisa"]'));


    apiLinhaEstacao($(this), $('select[name="estacaoInicialPesquisa"]'));
    apiLinhaEstacao($(this), $('select[name="estacaoFinalPesquisa"]'));
});

$('select[name="grupoSistemaPesquisa"]').change(function(){
    switch ($(this).val()) {
        case '21'://Edifica��es
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '25'://Subesta��o
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '24'://Via Permanente
            $("#local").hide();
            $("#estacaoInicial").show();
            $("#estacaoFinal").show();
            break;
        case '20'://Rede A�rea
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
    }

    apiGrupoServico($(this), $('select[name="servico"]') );

    var grupo = ObjetoVal;
    if ($(this).val() == ""){
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    }else{
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisa"]'));
});

$('select[name="sistemaPesquisa"]').on('change', function(){
    var sistema = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="grupoSistemaPesquisa"]').val() == ""){
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        }else{
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    }else{
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});
