$(document).ready(function(){

    var previstas = parseInt($('#totalPrevistas').val());
    var realizadas = parseInt($('#totalRealizadas').val());

    previstas = previstas - realizadas;

    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var frota = $('select[name="frota"]').val();

    var tituloGraf = mes + "/" + ano + " da frota " + frota;

    Highcharts.chart('container', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'APP - Aproveitamento de Programacoes Preventivas TUE'
        },
        tooltip: {
            pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
        },
        subtitle: {
            text: tituloGraf
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    format: '{point.percentage:.1f} %',
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                showInLegend: true
            }
        },
        colors: ['#4CAF50', '#2196F3'],
        series: [{
            name: 'Porcentagem',
            colorByPoint: true,
            data: [{
                name: 'PREVISTAS',
                y: previstas
            }, {
                name: 'REALIZADAS',
                y: realizadas,
                sliced: true,
                selected: true
            }]
        }]
    });
});

$('#mes').change(function(){
    var d = new Date();

    var fieldAprox = $('#rodadoAproximadamente');

    if($(this).val() == d.getMonth()+1 ){
        fieldAprox.show();
        fieldAprox.attr('required');
    }else{
        fieldAprox.hide();
        fieldAprox.removeAttr('required');
    }
});