/**
 * Created by ricardo.diego on 14/09/2017.
 */

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaSaf"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaSaf"]'));
    } else {
        linha.val($(this).val());
        $('select[name="pnPesquisaSaf"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaSaf"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaSaf"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaSaf"]').val() == "") {
            trecho.val("*");
            apiTrechoPn(trecho, $('select[name="pnPesquisaSaf"]'));
        } else {
            $('select[name="pnPesquisaSaf"]').html('<option value="">Ponto Not�vel</option>');
        }
    } else {
        trecho.val($(this).val());
        apiTrechoPn(trecho, $('select[name="pnPesquisaSaf"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

var mtRod = $('.mtRod');

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistemaPesquisa"]').on('change', function () {

    var grupo = ObjetoVal;

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        mtRod.show();
    } else {
        clearMtRod();
        mtRod.hide();
    }

    if($(this).val() == "28" || $(this).val() == "27"){
        $('#local_sublocal').show();
    }else {
        $('#local_sublocal').hide();
    }

    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisa"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisa"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

$('select[name="localGrupo"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocalGrupo"]'));
});

$('select[name="veiculoPesquisa"]').on('change', function () {
    var veiculo = ObjetoVal;
    if ($(this).val() == "")
        veiculo.val("*");
    else
        veiculo.val($(this).val());

    apiCarro('veiculo', veiculo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisa"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistemaPesquisa"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        } else {
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//############# AutoComplete DataList #############
if ($('input[name="avariaPesquisaInput"]').val() != "") {
    dataListFuncao($('input[name="avariaPesquisaInput"]'), $("#avariaPesquisaDataList"), $('input[name="avariaPesquisa"]'));
}

$('input[name="avariaPesquisaInput"]').on("input", function () {
    dataListFuncao($('input[name="avariaPesquisaInput"]'), $("#avariaPesquisaDataList"), $('input[name="avariaPesquisa"]'));
});

$('input[name="avariaPesquisaInput"]').on("blur", function () {
    if ($('input[name="avariaPesquisa"]').val() == "")
        $(this).val("");
    else
        $('input[name="avariaPesquisaInput"]').val($("#avariaPesquisaDataList").find("option[value='" + $('input[name="avariaPesquisa"]').val() + "']").text());
});

function clearMtRod() {
    $('select[name="veiculoPesquisaOsm"]').val('');
    $('select[name="carroAvariadoPesquisaOsm"]').val('');
    $('select[name="carroLiderPesquisaOsm"]').val('');
    $('select[name="prefixoPesquisaOsm"]').val('');
    $('input[name="odometroPartirPesquisaOsm"]').val('');
    $('input[name="odometroAtePesquisaOsm"]').val('');
}