/**
 * Created by iramar.junior on 08/02/2017.
 */

//====================================================================================================================//
//===Gerar Gr�fico e Tabelas===//

$('#gerar').on('click', function () {
    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var linha = $('select[name="linha"]').val();
    var grupoSistema = $('select[name="grupoSistema"]').val();
    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
});

//====================================================================================================================//

//====================================================================================================================//
//=== Par�metros Pesquisa ===//
$('select[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('select[name="sistema"]'));
});

$('select[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});

//===Gr�fico do Relat�rio OS(s)===//

$('.filtrarRel').click(function () {
    loader();
    $.post(caminho + "/api/getResult/getValueAnualOcorrencias",
        $('#parametros').serialize(),
        function (json) {

            // alert(JSON.stringify(json));

            var abertas = json.abertas;
            var encerradas = json.encerradas;
            var titulo = json.tituloFiltro;

            Highcharts.chart('resultGraph', {
                chart: {
                    type: 'column',
                    zoomType: 'x'
                },
                title: {
                    text: 'Relatorio Anual de Ocorrencias'
                },
                subtitle: {
                    text: titulo
                },
                xAxis: {
                    categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
                        'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantidade de Ocorrencia(s) - (un)'
                    }
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:15px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                colors: ['#B0BEC5', '#4CAF50'],
                plotOptions: plotOptionsBar(''),
                series: [{
                    name: 'Abertas',
                    data: abertas

                }, {
                    name: 'Encerradas',
                    data: encerradas

                }]
            });


        }, "json"
    ).success(function () {
        loading();
    });
});

//====================================================================================================================//

//====================================================================================================================//
//===DataTable===//

$("#resultTable").dataTable(configTable(1, false, 'asc'));

//====================================================================================================================//