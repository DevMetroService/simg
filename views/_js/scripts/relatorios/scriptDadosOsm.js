/**
 * Created by ricardo.diego on 14/09/2017.
 */

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaOsm"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaOsm"]'));
    } else {
        linha.val($(this).val());
        $('select[name="pnPesquisaOsm"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaOsm"]'));

    var grupo = $('select[name="grupoPesquisaOsm"]').val();

    if (grupo == "22" || grupo == "23" || grupo == "26") {
        apiGetVeiculo(grupo, $('select[name="veiculoPesquisaOsm"]'), $(this).val());
    }
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaOsm"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaOsm"]').val() == "") {
            trecho.val("*");
            apiTrechoPn(trecho, $('select[name="pnPesquisaOsm"]'));
        } else {
            $('select[name="pnPesquisaOsm"]').html('<option value="">Ponto Not�vel</option>');
        }
    } else {
        trecho.val($(this).val());
        apiTrechoPn(trecho, $('select[name="pnPesquisaOsm"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

var mtRod = $('.mtRod');

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoPesquisaOsm"]').on('change', function () {

    var grupo = ObjetoVal;

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        mtRod.show();
    } else {
        clearMtRod();
        mtRod.hide();
    }

    if($(this).val() == "28" || $(this).val() == "27"){
        $('#local_sublocal').show();
        selectLocalSublocal.each(function(){
            $(this).attr("required", "required");
            $(this).val('');
        });

    }else {
        $('#local_sublocal').hide();
        selectLocalSublocal.each(function () {
            $(this).removeAttr("required");
        });
    }

    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisaOsm"]'));

    var linha = $('select[name="linhaPesquisaOsm"]').val();
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisaOsm"]'), linha);


    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisaOsm"]'));
});

$('select[name="localGrupo"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocalGrupo"]'));
});

$('select[name="veiculoPesquisaOsm"]').on('change', function () {
    var veiculo = ObjetoVal;
    if ($(this).val() == "")
        veiculo.val("*");
    else
        veiculo.val($(this).val());

    apiCarro('veiculo', veiculo.val(), $('select[name="carroAvariadoPesquisaOsm"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisaOsm"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoPesquisaOsm"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        } else {
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="unidadePesquisaOsm"]').on("change", function () {
    var local = ObjetoVal;
    if ($(this).val() == "") {
        local.val("*");
    } else {
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisaOsm"]'));
});

function clearMtRod() {
    $('select[name="veiculoPesquisaOsm"]').val('');
    $('select[name="carroAvariadoPesquisaOsm"]').val('');
    $('select[name="carroLiderPesquisaOsm"]').val('');
    $('select[name="prefixoPesquisaOsm"]').val('');
    $('input[name="odometroPartirPesquisaOsm"]').val('');
    $('input[name="odometroAtePesquisaOsm"]').val('');
}