/**
 * Created by iramar.junior on 08/02/2017.
 */

/**
 * Created by iramar.junior on 08/02/2017.
 */

//====================================================================================================================//
//===Gerar Gr�fico e Tabelas===//

$('#gerar').on('click', function () {
    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var linha = $('select[name="linha"]').val();
    var grupoSistema = $('select[name="grupoSistema"]').val();
    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
});

//====================================================================================================================//

//====================================================================================================================//
//===Gr�fico do Relat�rio OS(s)===//

$(function () {
    Highcharts.chart('resultGraph', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Relat�rio de Preventivas (Planejadas x Executadas)'
        },
        subtitle: {
            text: 'Edifica��es - Linha Sul - Dezembro - 2016'
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Quantidade de OS(s) - (Unidade)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Abertas',
            data: [50],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }, {
            name: 'Em Aberto',
            data: [30],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }, {
            name: 'Executados',
            data: [10],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }, {
            name: 'Cancelados',
            data: [10],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
//====================================================================================================================//

//====================================================================================================================//
//===DataTable===//

$("#resultTable").dataTable(configTable(1, false, 'asc'));

//====================================================================================================================//