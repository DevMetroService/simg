/**
 * Created by iramar.junior on 16/03/2017.
 */

$(window).load(function () {
    loading();
});

function loader() {
    $("#loading").show();
}

function loading() {
    $("#loading").delay(1000).fadeOut("slow");
}

loading();

$(document).ready(function () {

    var v1;
    var v2;
    var v3;
    var v4;
    var v5;

    //REDE A�REA
    v1 = parseInt($("#exec205").html());
    v2 = parseInt($("#exec201").html());
    v3 = parseInt($("#exec202").html());
    v4 = parseInt($("#exec203").html());
    v5 = parseInt($("#exec204").html());
    $('#prevRaExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#nexec205").html());
    v2 = parseInt($("#nexec201").html());
    v3 = parseInt($("#nexec202").html());
    v4 = parseInt($("#nexec203").html());
    v5 = parseInt($("#nexec204").html());
    $('#prevRaNExec').html(v1 + v2 + v3 + v4 + v5);

    //EDIFICA��ES
    v1 = parseInt($("#exec215").html());
    v2 = parseInt($("#exec211").html());
    v3 = parseInt($("#exec212").html());
    v4 = parseInt($("#exec213").html());
    v5 = parseInt($("#exec214").html());
    $('#prevEdExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#nexec215").html());
    v2 = parseInt($("#nexec211").html());
    v3 = parseInt($("#nexec212").html());
    v4 = parseInt($("#nexec213").html());
    v5 = parseInt($("#nexec214").html());
    $('#prevEdNExec').html(v1 + v2 + v3 + v4 + v5);

    //VIA PERMANENTE
    v1 = parseInt($("#exec245").html());
    v2 = parseInt($("#exec241").html());
    v3 = parseInt($("#exec242").html());
    v4 = parseInt($("#exec243").html());
    v5 = parseInt($("#exec244").html());
    $('#prevVpExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#nexec245").html());
    v2 = parseInt($("#nexec241").html());
    v3 = parseInt($("#nexec242").html());
    v4 = parseInt($("#nexec243").html());
    v5 = parseInt($("#nexec244").html());
    $('#prevVpNExec').html(v1 + v2 + v3 + v4 + v5);

    //SUBESTA��O
    v1 = parseInt($("#exec255").html());
    v2 = parseInt($("#exec251").html());
    v3 = parseInt($("#exec252").html());
    v4 = parseInt($("#exec253").html());
    v5 = parseInt($("#exec254").html());
    $('#prevSbExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#nexec255").html());
    v2 = parseInt($("#nexec251").html());
    v3 = parseInt($("#nexec252").html());
    v4 = parseInt($("#nexec253").html());
    v5 = parseInt($("#nexec254").html());
    $('#prevSbNExec').html(v1 + v2 + v3 + v4 + v5);

//====================================================================================================================//

    //REDE A�REA
    v1 = parseInt($("#cexec205").html());
    v2 = parseInt($("#cexec201").html());
    v3 = parseInt($("#cexec202").html());
    v4 = parseInt($("#cexec203").html());
    v5 = parseInt($("#cexec204").html());
    $('#corrRaExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#cnexec205").html());
    v2 = parseInt($("#cnexec201").html());
    v3 = parseInt($("#cnexec202").html());
    v4 = parseInt($("#cnexec203").html());
    v5 = parseInt($("#cnexec204").html());
    $('#corrRaNExec').html(v1 + v2 + v3 + v4 + v5);

    //EDIFICA��ES
    v1 = parseInt($("#cexec215").html());
    v2 = parseInt($("#cexec211").html());
    v3 = parseInt($("#cexec212").html());
    v4 = parseInt($("#cexec213").html());
    v5 = parseInt($("#cexec214").html());
    $('#corrEdExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#cnexec215").html());
    v2 = parseInt($("#cnexec211").html());
    v3 = parseInt($("#cnexec212").html());
    v4 = parseInt($("#cnexec213").html());
    v5 = parseInt($("#cnexec214").html());
    $('#corrEdNExec').html(v1 + v2 + v3 + v4 + v5);

    //VIA PERMANENTE
    v1 = parseInt($("#cexec245").html());
    v2 = parseInt($("#cexec241").html());
    v3 = parseInt($("#cexec242").html());
    v4 = parseInt($("#cexec243").html());
    v5 = parseInt($("#cexec244").html());
    $('#corrVpExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#cnexec245").html());
    v2 = parseInt($("#cnexec241").html());
    v3 = parseInt($("#cnexec242").html());
    v4 = parseInt($("#cnexec243").html());
    v5 = parseInt($("#cnexec244").html());
    $('#corrVpNExec').html(v1 + v2 + v3 + v4 + v5);

    //SUBESTA��O
    v1 = parseInt($("#cexec255").html());
    v2 = parseInt($("#cexec251").html());
    v3 = parseInt($("#cexec252").html());
    v4 = parseInt($("#cexec253").html());
    v5 = parseInt($("#cexec254").html());
    $('#corrSbExec').html(v1 + v2 + v3 + v4 + v5);

    v1 = parseInt($("#cnexec255").html());
    v2 = parseInt($("#cnexec251").html());
    v3 = parseInt($("#cnexec252").html());
    v4 = parseInt($("#cnexec253").html());
    v5 = parseInt($("#cnexec254").html());
    $('#corrSbNExec').html(v1 + v2 + v3 + v4 + v5);

}).success(function () {
    loading();
});
