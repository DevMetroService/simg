var table = $('#tableRel');
var columnDispoResult = $('#tableRel tbody tr td:nth-child(3)');
var columnDispoDia = $('#tableRel tbody tr td:nth-child(1)');
var resultDisplay = $('#resultMensal');

var item_y=[];
var item_x=[];

var soma = 0;
var total = 0;

$('document').ready(function(){
    columnDispoResult.each( function(){
        var aux = parseFloat($(this).text().replace('%', ''))
        item_y.push( aux );

        soma += aux;
        total += 1;
    });
    columnDispoDia.each( function(){
        item_x.push( $(this).text().replace('%', '') );
    });

    var result = (soma/total).toFixed(2);

    resultDisplay.html(result + "%");

    Highcharts.chart('grafico', {
        xAxis: {
            categories: item_x
        },
        yAxis: {
            min: 0,
            max: 120,
            title: {
                text: 'Disponibilidade (%)'
            }
        },
        title: {
            text: 'Gr�fico Disponibilidade Mensal'
        },
        series: [{
            type: 'line',
            name: 'M�dia Di�ria',
            data: item_y
        }]
    });
});

