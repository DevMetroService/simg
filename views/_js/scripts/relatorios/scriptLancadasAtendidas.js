//=== Geral ===//
//====================================================================================================================//

$('#footer').hide();
//====================================================================================================================//

//=== Impress�o ===//
//====================================================================================================================//

$(".imprimir").click(function () {
    $('.highcharts-button-box').hide();
    $('.highcharts-button-symbol').hide();
    window.print();
    window.close();
});

//====================================================================================================================//

//=== Gr�fico ===//
//====================================================================================================================//

var form = $('input[name="form"]').val();
var dataDe = $('input[name="dataDe"]').val();
var dataAte = $('input[name="dataAte"]').val();

var lancadas = $('input[name="lancadas"]').val().split(',');
var atendidas = $('input[name="atendidas"]').val().split(',');
var emAtendimento = $('input[name="emAtendimento"]').val().split(',');
var canceladas = $('input[name="canceladas"]').val().split(',');

var grupoSistema = $('input[name="grupoSistema"]').val();
var categories = "";

var titulo = '';
if(dataDe != '' && dataAte == ''){
    titulo = " - A partir de " + dataDe;
}
if(dataDe == '' && dataAte != ''){
    titulo += " - At� " + dataAte;
}
if(dataDe != '' && dataAte != ''){
    titulo += " - A partir de " + dataDe + " at� " + dataAte;
}


if (grupoSistema == "") {
    var categories = $('input[name="label"]').val();
    categories = categories.split(',');
} else {
    switch (grupoSistema) {
        case '21':
            categories = ['Edificacoes'];
            break;
        case '24':
            categories = ['Via Permanente'];
            break;
        case '25':
            categories = ['Subestacao'];
            break;
        case '20':
            categories = ['Rede Aerea'];
            break;
        case '29':
            categories = ['Energia'];
            break;
        case '22':
            categories = ['Mt Rodante VLT'];
            break;
        case '23':
            categories = ['Mt Rodante TUE'];
            break;
        case '26':
            categories = ['Mt Rodante Locomotiva'];
            break;
    }
}

valLancadas = lancadas.map(Number);
valAtendidas = atendidas.map(Number);
valEmAtendimento = emAtendimento.map(Number);
valCanceladas = canceladas.map(Number);

Highcharts.chart('chart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Relatorio Lancadas Atendidas'
    },
    subtitle: {
        text: form.toUpperCase() + titulo
    },
    colors: ['#B0BEC5', '#4CAF50', '#03A9F4', '#FF0000'],
    xAxis: {
        categories: categories,
        crosshair: true
    },
    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Quantidade - (un)'
        },
        labels: {
            formatter: function () {
                return Highcharts.numberFormat(this.value, 0, '', '');
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:15px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0;font-size: 10px">{series.name}: </td>' +
        '<td style="padding:0;font-size: 10px">{point.y}</td></tr>',
        footerFormat: '</table>',
        useHTML: true,
    },
    plotOptions: plotOptionsBar(''),
    credits: {
        enabled: false
    },
    series: [{
        name: 'LANCADAS',
        data: valLancadas,
        categories: 'Subestacao'
    }, {
        name: 'ATENDIDAS',
        data: valAtendidas
    }, {
        name: 'EM ATENDIMENTO',
        data: valEmAtendimento
    },{
        name: 'CANCELADAS',
        data: valCanceladas
    }]
});

//====================================================================================================================//