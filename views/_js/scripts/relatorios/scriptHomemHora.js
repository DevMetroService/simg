$('#footer').hide();

$(".tableRel").DataTable({
    "paging": false
});

$(".imprimir").click(function () {
    alertaJquery('Imprimir', 'Deseja Imprimir o relat�rio com o gr�fico?<br/>Acima de 40 itens o gr�fico se torna invi�vel visualmente.', 'confirm', [{value: 'Sim'}, {value: 'N�o'}, {value: 'Cancelar'}],
        function (resposta) {
            if (resposta == "Sim") {
                $('.msgBox').addClass('hidden-print');
                $('.dataTables_filter').addClass('hidden-print');

                $('.highcharts-button-symbol').hide();
                window.print();
                window.close();
            } else if (resposta == "N�o") {
                $('.chartRel').addClass('hidden-print');

                $('.msgBox').addClass('hidden-print');
                $('.dataTables_filter').addClass('hidden-print');

                $('.highcharts-button-symbol').hide();
                window.print();
                window.close();
            }
        });
});

$('.btnDadosFuncionario').click(function(e){
    e.preventDefault();

    var cod_funcionario = $(this).data('cod');        //dados recebe o formulario em string

    $.getJSON(
        caminho + "/api/returnResult/matricula/homemHora",
        {matricula: cod_funcionario},
        function (json) {
            // alert(JSON.stringify(json));
            $('#matricula').val(json.matricula);
            $('#nome').val(json.nome);
            $('#funcao').val(json.funcao);
            $('#cpfCnpj').val(json.cpf);
            $('#centroResultado').val(json.centroResultado);
            $('#unidade').val(json.unidade);
        }
    );
});

$('.chartHH').each(function () {
    var id = $(this).children('input[name="id"]').val();
    var nomes = $(this).children('input[name="nome"]').val();
    var horarios = $(this).children('input[name="hh"]').val();
    var horariosString = $(this).children('input[name="horariosString"]').val();
    var subTitulo = $('input[name="subTitulo"]').val();

    var categories = nomes.split(',');
    var dataHH = horarios.split(',');
    var datahorariosString = horariosString.split(',');

    dataHH = dataHH.map(Number);

    var data = [];
    for(var i = 0; i < dataHH.length; i++){
        data.push({ name:datahorariosString[i] , y : dataHH[i]});
    }

    graficoBarra(id, categories, data, subTitulo);
});

function graficoBarra(id, nomes, horarios, subTitulo) {

    var tituloExtra = $('#tituloChart').val();
    Highcharts.chart(id, {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Relatorio Homem Hora '+tituloExtra.toUpperCase()
        },
        xAxis: {
            categories: nomes
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            },
            visible: false
        },
        legend: {
            reversed: false
        },
        credits: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none',
                    rotation: 0,
                    color: '#000000',
                    align: 'top',
                    format: '{point.name}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }
        },
        series: [{
            name: 'Horas (h:m:s)',
            data: horarios
        }]
    });
}