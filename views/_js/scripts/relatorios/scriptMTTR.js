// var tabelaResultado;
// $('document').ready(function(){
//     tabelaResultado = $('#tableMTTR').DataTable(configTable(0,false,'desc', true));
// });

$('SELECT[name="linha"]').change(function () {
    apiLinhaTrecho($(this), $('SELECT[name="trecho"]'));

    if($(this).val() == "") {
        $('SELECT[name="trecho"]').html("<option value=''></option>");
    }
});


$('SELECT[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('SELECT[name="sistema"]'));

    if( ($(this).val() == "") || ($(this).val() != "") ){
        $('SELECT[name="subsistema"]').html("<option value=''>TODOS</option>");
    }
});


$('SELECT[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('SELECT[name="subsistema"]'));
});



$('#gerarGrafico').click(function (e) {
    e.preventDefault();

    $.post(
        caminho + "/api/returnResult/getMTTR",
        $('#formRelMTTR').serialize(),
        function (json) {
            $('#total').html(json[0].total_falha);
            $('#mttr').html(json[0].mttr.split('.')[0]);
        },'json'
    ).error(function(msg){
        alert(JSON.stringify(msg));
    });
});


$('.limpaFiltro').on('click', function () {
    $('#formRelMTTR input').val('');
    $('#formRelMTTR select').val('');
    $('select[name="subsistema"]').html('<option value="">TODOS</option>');
    $('select[name="sistema"]').html('<option value="">TODOS</option>');
});
