/* OPCOES PARA O SELECT DE OPCOES DE GRAFICO */
// Grafico - Corretiva
var opcoes_select_corretiva = [
    '<option disabled selected>Selecione Op��es</option>',
    '<option value="nome_grupo">Grupo</option>',
    '<option value="nome_sistema">Sistema</option>',
    '<option value="nome_subsistema">Sub-Sistema</option>',
    '<option value="nome_linha">Linha</option>',
    '<option value="descricao_trecho">Trecho</option>',
    '<option value="nome_avaria">Avaria</option>',
    '<option value="nivel">N�vel</option>',
    '<option value="nome_agente">Agente Causador</option>',
    '<option value="nome_causa">Causa</option>',

    '<optgroup id="mr-opt" label="Material Rodante">',
    '<option value="nome_veiculo">Ve�culo</option>',
    '</optgroup>',
    '<optgroup id="blht-telec-opt" label="Bilhetagem/Telecom">',
    '<option value="nome_local_grupo">Local</option>',
    '<option value="nome_sublocal_grupo">Sub-Local</option>',
    '</optgroup>',
];

// Grafico - Preventiva
var opcoes_select_programacao = [
    '<option disabled selected>Selecione Op��es</option>',
    '<option value="nome_grupo">Grupo</option>',
    '<option value="nome_sistema">Sistema</option>',
    '<option value="nome_subsistema">Sub-Sistema</option>',
    '<option value="nome_linha">Linha</option>',
    '<option value="nome_servico">Servico</option>',
    '<option value="nome_equipe">Equipe</option>',
    '<optgroup label="Material Rodante">',
    '<option value="nome_veiculo">Ve�culo</option>',
    '</optgroup>',
];

// Grafico - Plano_de_Manutencao
var opcoes_select_plano_de_manutencao = [
    '<option disabled selected>Selecione Op��es</option>',
    '<option value="nome_grupo">Grupo</option>',
    '<option value="nome_sistema">Sistema</option>',
    '<option value="nome_subsistema">Sub-Sistema</option>',
    '<option value="nome_linha">Linha</option>',
    '<option value="nome_servico">Servico</option>',
];

/* GERADOR DE GRAFICOS */
//Construtor
var CriaGraficos = function ( id_div_grafico ) {
    this.local_grafico = id_div_grafico;

    return this;
};

//Metodos

/*
    Gera qualquer tipo de grafico,
    apenas informando o tipo de grafico
    e as configuracoes
 */
CriaGraficos.prototype.graficoX = function (config) {
    var tipo_grafico = config.tipo_grafico;
    var titulo = config.titulo;
    var subTitulo = config.subTitulo;
    var ocorrencia = config.series_name;
    var array_dados = config.array_dados;

    try {
        switch (tipo_grafico) {
            case 'column':
                this.graficoColunas(titulo, subTitulo, ocorrencia, array_dados);
                break;
            case 'pie':
                this.graficoPizza  (titulo, subTitulo, ocorrencia, array_dados);
                break;
            case 'line':
                this.graficoLinhas (titulo, subTitulo, ocorrencia, array_dados);
                break;
            default:
                throw 'Grafico [[ ' + tipo_grafico + ' ]] Indisponivel';
        }

    }catch (erro) {
        console.log(erro.message);
        alert('Erro: '+erro.message);
    }
};

CriaGraficos.prototype.graficoColunas = function ( titulo, subTitulo, ocorrencia, array_dados ) {
    Highcharts.chart(this.local_grafico, {
        chart: {
            type: 'column'
        },
        title: {
            text: titulo
        },
        subtitle: {
            text: subTitulo
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Qtd. Ocorr�ncias'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Qtd. Ocorrencias: <b>{point.y}</b>'
        },
        series: [{
            name: ocorrencia,
            data: array_dados,
            dataLabels: {
                enabled: true,
                // rotation: -90,
                // color: '#FFFFFF',
                // align: 'right',
                format: '{point.y}', // sem decimal
                // y: 10, // 10 pixels down from the top
                // style: {
                //     fontSize: '13px',
                //     fontFamily: 'Verdana, sans-serif'
                // }
            }
        }]
    });
};

CriaGraficos.prototype.graficoLinhas = function ( titulo, subTitulo, ocorrencia, array_dados ) {

    var series = [];
    array_dados.forEach(function (key) {
        series.push({
            name: [key[0]], //obtem o nome
            data: [key[1]],   //obtem quantidade
        });
    });

    //Teste isso

    // var coluna = [key[0]]; //obtem a coluna
    // var linha = key.slice(1, (key.length-1)); //obtem as linhas da coluna acima

    Highcharts.chart(this.local_grafico, {
        chart: {
            type: 'line'
        },

        title: {
            text: titulo
        },

        subtitle: {
            text: subTitulo
        },

        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },

        yAxis: {
            title: {
                text: 'Qtd. Ocorrencias'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 1
            }
        },

        series: series,


    });


};

CriaGraficos.prototype.graficoPizza = function ( titulo, subTitulo, ocorrencia, array_dados ) {
    Highcharts.chart(this.local_grafico, {
        chart: {
            type: 'pie'
        },
        title: {
            text: titulo
        },
        subtitle: {
            text: subTitulo
        },
        tooltip: {
            pointFormat: 'Qtd. Ocorrencias: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer'
            }
        },
        series: [{
            name: ocorrencia,
            data: array_dados,
            dataLabels: {
                enabled: true,
                format: '<b>{point.percentage:.1f}% - {point.name}</b><br/><br/>{point.y}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }]
    });
};

// ----------------------------------
//       Acoes para Componentes
// ----------------------------------
//Variavel para armazenar um dataTable
var tableGraf;

//Configura a pagina automaticamente
$('document').ready(function () {
    //ESCONDE TUDO
    $('.all_').hide();
    $('.select_form').hide();
    $('.filtros').hide();
    $('.resetarRel').hide();

    //Cria uma instancia de dataTables
    tableGraf = $("#area_tabela").dataTable();
});

var valueGraf;
//Ocorre quando o tipo de Grafico for selecionado
$('#select_tipo_grafico').change(function () {
    valueGraf = $(this).val();
    //Exibe as opcoes para o grafico selecionado
    $('.select_form').show();
    $('.resetarRel').show();

    $('button[name="corretiva"]').click();
});


// ----------------------------------
//      Acoes Ao Clicar nos Botoes
//      CORRETIVA, PROGRAMA��O, PLANO DE MANUTEN��O
// ----------------------------------

//ACAO PARA BOTAO CORRETIVA
//EXIBE OS FILTROS RELACIONADOS A CORRETIVA
$('button[name="corretiva"]').click(function () {
    //Altera a cor dos bot�es
    $('._btnForm').removeClass('btn-primary');
    $('._btnForm').addClass('btn-default');

    $(this).addClass('btn-primary');
    $(this).removeClass('btn-default');

    //ESCONDE TUDO
    $('.all_').hide();

    //reseta os campos dos formularios
    $('#formRel')[0].reset();

    $('#select_tipo_grafico').val(valueGraf);

    //Exibe Campos Necessarios
    $('.filtros').show();

    $('.dados_gerais').show();
    $('.avariaRelDiv').show();
    $('.nivelDiv').show();
    $('.dataAbertura').show();
    $('.datAbert').show();
    $('.local').show();

    //Exibe  op��es grafico
    $('.select_opcoes_grafico').show();

    //Muda as opcoes para o grafico
    $('#select_opcoes_grafico').html('');
    var opcoes = opcoes_select_corretiva;

    //Monta um codigo html com as <options> do select
    var html = '';
    opcoes.forEach(function (opcao, index) {
        html += opcao;
    });

    // Adiciona ao select o codigo html com os <options> do select
    $('#select_opcoes_grafico').html(html);

    $('input[name="_table"]').val('v_saf');
});


//ACAO PARA BOTAO PROGRAMA��O
//EXIBE OS FILTROS RELACIONADOS A PROGRAMA��O
$('button[name="programacao"]').click(function () {
    //Altera a cor dos bot�es
    $('._btnForm').removeClass('btn-primary');
    $('._btnForm').addClass('btn-default');

    $(this).addClass('btn-primary');
    $(this).removeClass('btn-default');

    //ESCONDE TUDO
    $('.all_').hide();

    //reseta os campos dos formularios
    $('#formRel')[0].reset();

    //Exibe Campos Necessarios
    $('.filtros').show();

    $('.dados_gerais').show();
    $('.origemProgRel').show();
    $('.dataAbertura').show();
    $('.datProg').show();
    $('.local').show();
    $('.servicoDiv').show();
    $('.encaminhamnetoDiv').show();

    //Exibe  select tipo grafico
    $('.select_tipo_grafico').show();

    //Muda as opcoes para o grafico
    $('#select_opcoes_grafico').html('');
    var opcoes = opcoes_select_programacao;

    //Monta um codigo html com as <options> do select
    var html = '';
    opcoes.forEach(function (opcao, index) {
        html += opcao;
    });

    // Adiciona ao select o codigo html com os <options> do select
    $('#select_opcoes_grafico').html(html);

    $('input[name="_table"]').val('v_ssp');
});


//ACAO PARA BOTAO PLANO DE MANUTENCAO
//EXIBE OS FILTROS RELACIONADOS A PMP
$('button[name="plano_de_manutencao"]').click(function () {
    //Altera a cor dos bot�es
    $('._btnForm').removeClass('btn-primary');
    $('._btnForm').addClass('btn-default');

    $(this).addClass('btn-primary');
    $(this).removeClass('btn-default');

    //ESCONDE TUDO
    $('.all_').hide();

    //reseta os campos dos formularios
    $('#formRel')[0].reset();

    //Exibe Campos Necessarios
    $('.filtros').show();

    $('.dados_gerais').show();
    $('.periodo').show();
    $('.periodoMesAno').show();
    $('.local').show();
    $('.servicoDiv').show();

    //Exibe  select tipo grafico
    $('.select_tipo_grafico').show();

    //Muda as opcoes para o grafico
    $('#select_opcoes_grafico').html('');
    var opcoes = opcoes_select_plano_de_manutencao;

    //Monta um codigo html com as <options> do select
    var html = '';
    opcoes.forEach(function (opcao, index) {
        html += opcao;
    });

    // Adiciona ao select o codigo html com os <options> do select
    $('#select_opcoes_grafico').html(html);

    $('input[name="_table"]').val('v_ssp');
});


$('select[name="grupoSistema"]').on("change", function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_grupo"]').show();
        $('#select_opcoes_grafico [value="nome_sistema"]').show();
        $('#select_opcoes_grafico [value="nome_subsistema"]').show();
    }else {
        $('#select_opcoes_grafico [value="nome_grupo"]').hide();

        $('#select_opcoes_grafico #mr-opt').hide();
        $('#select_opcoes_grafico #blht-telec-opt').hide();
    }

    $('select[name="subSistema"]').html('');
    apiGrupoSistema($(this), $('select[name="sistema"]'));

    if($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26'){
        $('#select_opcoes_grafico #mr-opt').show();

        $('.materialRodanteDiv').show();
        apiGetVeiculo($(this).val(), $('select[name="veiculo"]'));
        $('select[name="carro"]').html('');
    }else{
        $('.materialRodanteDiv').hide();
        $('select[name="veiculo"]').html('');
        $('select[name="carro"]').html('');
    }

    if($('input[name="_table"]').val() == 'v_saf') {
        if ($(this).val() == "28" || $(this).val() == "27") {
            $('#select_opcoes_grafico #blht-telec-opt').show();

            $('.bilhetagemTelecomDiv').show();
            $('select[name="subLocalGrupo"]').val('');
        } else {
            $('.bilhetagemTelecomDiv').hide();
            $('select[name="localGrupo"]').val('');
            $('select[name="subLocalGrupo"]').html('');
        }
    }
});

$('select[name="sistema"]').on('change', function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_sistema"]').show();
        $('#select_opcoes_grafico [value="nome_subsistema"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_sistema"]').hide();
    }

    apiSistemaSub($(this), $('select[name="subSistema"]'));
});

$('select[name="subSistema"]').on("change", function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_subsistema"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_subsistema"]').hide();
    }
});

$('select[name="linha"]').on("change", function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_linha"]').show();
        $('#select_opcoes_grafico [value="descricao_trecho"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_linha"]').hide();
    }

    apiLinhaTrecho($('select[name="linha"]'), $('select[name="trecho"]'));
});

$('select[name="trecho"]').on("change", function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="descricao_trecho"]').show();
    }else{
        $('#select_opcoes_grafico [value="descricao_trecho"]').hide();
    }
});

$('#avaria').on("input", function () {
    dataListFuncao2($('#avaria'), $("#avariaDataList"), $('input[name="avaria"]'));

    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_avaria"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_avaria"]').hide();
    }
});

$('#avaria').on("blur", function () {
    var valueFinal = $("#avariaDataList").find("option[value='" + $('input[name="avaria"]').val() + "']");
    $('#avaria').val(valueFinal.val());

    $('input[name="cod_avaria"]').val(valueFinal.attr('id'));
});

$('select[name="nivel"]').on("change", function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nivel"]').show();
    }else{
        $('#select_opcoes_grafico [value="nivel"]').hide();
    }
});

$('select[name="unidade"]').on("change", function(){
    var local = ObjetoVal;
    if($(this).val() == ""){
        local.val("*");
    }else{
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipe"]'));
});

$('select[name="veiculo"]').change(function () {
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_veiculo"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_veiculo"]').hide();
    }

    apiCarro('veiculo', $(this).val(), $('select[name="carro"]'));
});

$('select[name="localGrupo"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocalGrupo"]'));

    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_local_grupo"]').show();
        $('#select_opcoes_grafico [value="nome_sublocal_grupo"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_local_grupo"]').hide();
    }
});

$('select[name="subLocalGrupo"]').on("change", function(){
    if($(this).val() == ""){
        $('#select_opcoes_grafico [value="nome_sublocal_grupo"]').show();
    }else{
        $('#select_opcoes_grafico [value="nome_sublocal_grupo"]').hide();
    }
});

// ----------------------------------
//       Acoes para Graficos
// ----------------------------------
$('#select_opcoes_grafico').change(function () {
    //Exibe Os Bot�es
    $('.botoes_acao').show();
});

//BOTAO PARA GERAR RELATORIO
$('#btn_gerar_relatorio').click(function () {
    loader();

    $('#area_grafico').html('');

    //Monta o Titulo do Grafico
    var titulo_grafico = $('input[name="titulo_grafico"]').val();

    if (titulo_grafico == "")
        titulo_grafico = $('#select_tipo_grafico option:selected').text() + " - " + $('#select_opcoes_grafico option:selected').text();

    //Monta o Subtitulo do Grafico
    var textoSubtitulo = subtitulo();

    var dados = $('#formRel').serialize();

    $.post(
        caminho + "/api/returnResult/getRetornoOcorrenciaPorFrequencia",
        dados,
        function(json){

            // -- UTIL PARA DEBUG --
            // console.log(json);
            // console.log("Resposta:\n" + JSON.stringify(json));
            var dados_grafico;
            if ( $('#select_limit_grafico option:selected').val() ) {
                dados_grafico = json.valorX.slice(0, $('#select_limit_grafico option:selected').val());
            }else{
                dados_grafico = json.valorX;
            }

            //Cria os Graficos
            var graficos = new CriaGraficos('area_grafico');

            var configuracao = {
                tipo_grafico: $('#select_tipo_grafico option:selected').val(),
                titulo: titulo_grafico,
                subTitulo: textoSubtitulo,
                ocorrencia: $('#select_opcoes_grafico option:selected').text(),
                array_dados: dados_grafico
            };

            graficos.graficoX(configuracao);

            //Exibe a tabela
            $('.area_tabela_relatorio').show();
            $('.area_tabela_titulo').html(titulo_grafico);

            tableGraf.fnDestroy();
            $('#area_tabela').empty();

            $('#area_tabela').html("<thead><tr><th>"+ $('#select_opcoes_grafico option:selected').text() +"</th><th>Quantidade</th></tr></thead><tbody></tbody>");

            tableGraf = $('#area_tabela').dataTable(configTable(1, false, 'desc'));

            $.each(json.valorX, function (key, value) {
                tableGraf.fnAddData([
                    value[0],
                    value[1]
                ]);
            });
        },'json'
    ).done(function(){
        loading();
    }).error(function(msg){
        console.log(msg);
    });


});

$('.resetarRel').click(function () {
    $('.all_').hide();
    $('.select_form').hide();
    $('.filtros').hide();
    $('.resetarRel').hide();
});

//Acoes para preencher subtitulos nos graficos
function subtitulo(){
    var textoSubtitulo = "";

    if ($('select[name="grupoSistema"]').val() )
        textoSubtitulo += ' | ' + $('select[name="grupoSistema"] option:selected').text();

    if ($('select[name="sistema"]').val() )
        textoSubtitulo += ' > ' + $('select[name="sistema"] option:selected').text();

    if ($('select[name="subSistema"]').val() )
        textoSubtitulo += ' > ' + $('select[name="subSistema"] option:selected').text();

    if ($('select[name="nivel"]').val() )
        textoSubtitulo += ' | Nivel: ' + $('select[name="nivel"] option:selected').text();

    if ($('select[name="linha"]').val() )
        textoSubtitulo += ' | ' + $('select[name="linha"] option:selected').text();

    if ($('select[name="trecho"]').val())
        textoSubtitulo += ' > ' + $('select[name="trecho"] option:selected').text();

    if ( $('input[name="dataPartirDatAber"]').val())
        textoSubtitulo += " | Data a partir de "+ $('input[name="dataPartirDatAber"]').val();

    if ( $('input[name="dataRetrocederDatAber"]').val())
        textoSubtitulo += " at� "+ $('input[name="dataRetrocederDatAber"]').val();

    if ($('select[name="origemProgramacao"]').val())
        textoSubtitulo += ' | Origem: ' + $('select[name="origemProgramacao"] option:selected').text();

    if ( $('input[name="dataPartirPeriodo"]').val())
        textoSubtitulo += " | Data a partir de "+ $('input[name="dataPartirPeriodo"]').val();

    if ( $('input[name="dataRetrocederPeriodo"]').val())
        textoSubtitulo += " at� " + $('input[name="dataRetrocederPeriodo"]').val();

    if ($('select[name="periodicidadePesquisaCronograma"]').val())
        textoSubtitulo += ' | Periodicidade: ' + $('select[name="periodicidadePesquisaCronograma"] option:selected').text();

    if ($('input[name="quinzenaCronograma"]').val())
        textoSubtitulo += ' | Quinzena: ' + $('input[name="quinzenaCronograma"]').val();

    if ($('select[name="mes"]').val())
        textoSubtitulo += ' | ' + $('select[name="mes"] option:selected').text();

    if ($('select[name="ano"]').val())
        textoSubtitulo += ' | ' + $('select[name="ano"] option:selected').text();

    if ($('input[name="avaria"]').val())
        textoSubtitulo += ' | Avaria: ' + $('input[name="avaria"]').val();

    if ($('select[name="tipoIntervencao"]').val())
        textoSubtitulo += ' | Interven��o: ' + $('select[name="tipoIntervencao"] option:selected').text();

    if ($('select[name="servico"]').val())
        textoSubtitulo += ' | Servi�o: ' + $('select[name="servico"] option:selected').text();

    if ($('select[name="unidade"]').val())
        textoSubtitulo += ' | Unidade: ' + $('select[name="unidade"] option:selected').text();

    if ($('select[name="equipe"]').val())
        textoSubtitulo += ' | Equipe: ' + $('select[name="equipe"] option:selected').text();

    if ($('select[name="veiculo"]').val())
        textoSubtitulo += ' | Ve�culo: ' + $('select[name="veiculo"] option:selected').text();

    if ($('select[name="carro"]').val())
        textoSubtitulo += ' | Carro: ' + $('select[name="carro"] option:selected').text();

    if ($('select[name="localGrupo"]').val())
        textoSubtitulo += ' | Local: ' + $('select[name="localGrupo"] option:selected').text();

    if ($('select[name="subLocalGrupo"]').val())
        textoSubtitulo += ' | SubLocal: ' + $('select[name="subLocalGrupo"] option:selected').text();

    return textoSubtitulo;
}