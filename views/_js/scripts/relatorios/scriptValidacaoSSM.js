$(".tableRel").dataTable(configTable(6, false, 'dsc'));
var tableModal = $(".tableRelModal").dataTable({
    "language": {
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
        "search": "Filtrar ",
        "loadingRecords": "Carregando...",
        "info": "_MAX_ itens na tabela",
        "infoEmpty": "N�o h� informa��es",
        "infoFiltered": "(Filtrado de um total de _MAX_ dados)",
    },
    "paging": false
});

//Gr�fico
var tituloGraf = '';

concatenarTitulo($("select[name='linha'] option:selected"));
concatenarTitulo($("select[name='trecho'] option:selected"));

if (tituloGraf != '') {
    $separador = ' - ';
} else {
    $separador = '';
}

if ($("select[name='grupoSistema'] option:selected").val() == "") {
    tituloGraf = tituloGraf + $separador + "GRUPO GERAL";
} else {
    tituloGraf = tituloGraf + $separador + "GRUPO " + validaCaracteres($("select[name='grupoSistema'] option:selected").text().toUpperCase());
}

concatenarTitulo($("select[name='sistema'] option:selected"));
concatenarTitulo($("select[name='subSistema'] option:selected"));

$('input[name="tituloGraf"]').val(tituloGraf);

function concatenarTitulo(select) {
    if (select.val() != "") {
        txt = validaCaracteres(select.text().toUpperCase());

        if (tituloGraf == '')
            tituloGraf = txt;
        else
            tituloGraf = tituloGraf + ' - ' + txt;
    }
}

function validaCaracteres(strToReplace) {
    strSChar = "����������������������������������������������";
    strNoSChars = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    var newStr = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (strSChar.indexOf(strToReplace.charAt(i)) != -1) {
            newStr += strNoSChars.substr(strSChar.search(strToReplace.substr(i, 1)), 1);
        } else {
            newStr += strToReplace.substr(i, 1);
        }
    }
    return newStr.replace(/[^a-zA-Z 0-9]/g, '').toUpperCase();
}


$('document').ready(function(){

    var container = $('#container');

    var validada =  parseInt(container.data('validada'));
    var reprovada =  parseInt(container.data('reprovada'));
    var total = validada + reprovada;

    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: "SSM�s Validadas e N�o Validadas"
        },
        subtitle: {
            text: tituloGraf
        },
        xAxis: {
            categories: [],
            labels: {
                enabled: false
            }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Quantidade de SAF(s)'
            }
        },
        credits: {
            enabled: false
        },
        colors: [ '#4CAF50', '#FF5722', '#B0BEC5'],
        tooltip: {
            headerFormat: ' ',
            pointFormat: '<span style="color:{series.color}; text-shadow: #000 0px 0px 1.5px; padding:0"> {series.name} :</span> {point.y}',
            useHTML: true
        },
        plotOptions: plotOptionsBar(''),
        series: [
            {
                name: 'Validadas',
                data: [validada]
            },
            {
                name: 'N�o Validadas',
                data: [reprovada]
            },
            {
                name: 'Total',
                data: [total]
            }
        ]
    });
});

//====================================================================================================================//

//Auto preenchimento do trecho e via ap�s sele��o da linha
$('select[name="linha"]').on("change", function () {
    apiLinhaTrecho($('select[name="linha"]'), $('select[name="trecho"]'));
});

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistema"]').on('change', function () {
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistema"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistema"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistema"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistema"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistema"]'));
        } else {
            $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistema"]'));
    }
});

$('.limpaFiltro').on('click', function () {
    $('select[name="linha"]').val('');
    $('select[name="trecho"]').val('');
    $('select[name="grupoSistema"]').val('');
    $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    $('select[name="sistema"]').html('<option value="">Sistema</option>');
});