/**
 * Created by ricardo.diego on 10/10/2017.
 */
var acima0 = $('input[name="cat0"]').val();
var acima10 = $('input[name="cat10"]').val();
var acima30 = $('input[name="cat30"]').val();
var acima50 = $('input[name="cat50"]').val();
var acima60 = $('input[name="cat60"]').val();

acima0 = parseInt(acima0);
acima10 = parseInt(acima10);
acima30 = parseInt(acima30);
acima50 = parseInt(acima50);
acima60 = parseInt(acima60);

var total = acima0 + acima10 + acima30 + acima50 + acima60;

$('#tabCat0').html(acima0);
$('#tabCat10').html(acima10);
$('#tabCat30').html(acima30);
$('#tabCat50').html(acima50);
$('#tabCat60').html(acima60);
$('#total').html(total);

//Gr�fico

var tituloGraf = $('input[name="tituloGraf"]').val();

Highcharts.chart('containerPrintRel', {
    chart: {
        type: 'column'
    },
    title: {
        text: "Acompanhamento do prazo de atendimento das SAF's"
    },
    subtitle: {
        text: tituloGraf
    },
    xAxis: {
        categories: [],
        labels: {
            enabled: false
        }
    },
    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Quantidade de SAF(s)'
        }
    },
    credits: {
        enabled: false
    },
    colors: ['#0b97c4', '#4CAF50', '#FFEB3B', '#FF5722', '#D50000', '#B0BEC5'],
    tooltip: {
        headerFormat: ' ',
        pointFormat: '<span style="color:{series.color}; text-shadow: #000 0px 0px 1.5px; padding:0"> {series.name} :</span> {point.y}',
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        },
        series: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none',
                rotation: 0,
                color: '#000000',
                align: 'center',
                format: '{point.y}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }
    },
    series: [{
        name: 'de 0 a 10 dias',
        data: [acima0]
    }, {
        name: 'de 11 a 20 dias',
        data: [acima10]
    }, {
        name: 'de 21 a 40 dias',
        data: [acima30]
    }, {
        name: 'de 41 a 60 dias',
        data: [acima50]
    }, {
        name: 'Acima de 60 dias',
        data: [acima60]
    }, {
        name: 'Total',
        data: [total]
    }]
});
