/**
 * Created by ricardo.diego on 14/09/2017.
 */

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaSsp"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == ""){
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaSsp"]'));
    }else{
        linha.val($(this).val());
        $('select[name="pnPesquisaSsp"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaSsp"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaSsp"]').on("change", function() {
    var trecho = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="linhaPesquisaSsp"]').val() == ""){
            trecho.val("*");
            apiTrechoPn(trecho,$('select[name="pnPesquisaSsp"]'));
        }else{
            $('select[name="pnPesquisaSsp"]').html('<option value="">Ponto Not�vel</option>');
        }
    }else{
        trecho.val($(this).val());
        apiTrechoPn(trecho,$('select[name="pnPesquisaSsp"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

var mtRod = $('.mtRod');

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistemaPesquisa"]').on('change', function () {

    var grupo = ObjetoVal;

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        mtRod.show();
    } else {
        clearMtRod();
        mtRod.hide();
    }

    var grupo = ObjetoVal;
    if ($(this).val() == ""){
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    }else{
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisa"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisa"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisa"]').on('change', function(){
    var sistema = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="grupoSistemaPesquisa"]').val() == ""){
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        }else{
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    }else{
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="localPesquisa"]').on("change", function(){
    var local = ObjetoVal;
    if($(this).val() == ""){
        local.val("*");
    }else{
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisa"]'));
});


function clearMtRod() {
    $('select[name="veiculoPesquisaOsm"]').val('');
    $('select[name="carroAvariadoPesquisaOsm"]').val('');
    $('select[name="carroLiderPesquisaOsm"]').val('');
    $('select[name="prefixoPesquisaOsm"]').val('');
    $('input[name="odometroPartirPesquisaOsm"]').val('');
    $('input[name="odometroAtePesquisaOsm"]').val('');
}