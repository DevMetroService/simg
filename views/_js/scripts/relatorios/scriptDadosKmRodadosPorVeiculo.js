$(document).on('ready', function () {

    //Script to Multiselect
    var vlt = $('#selectVeiculos #vlt option').size();
    $('#selectVeiculos #vlt').attr('label', "VLT(" + vlt + ")");
    var tue = $('#selectVeiculos #tue option').size();
    $('#selectVeiculos #tue').attr('label', "TUE(" + tue + ")");

    // Configura o multiselect Veixulos
    $('.multiSelect').multiselect({
        enableClickableOptGroups: true,

        buttonContainer: '<div></div>',

        maxHeight: 250,
        includeSelectAllOption: true,

        selectAllText: "Selecionar Todos"
    });

    // Torna os select invisiveis na pagina apos o carregamento
    $('#multiSelectVeiculosTUE').css({display: 'none'});
    $('#multiSelectVeiculosVLT').css({display: 'none'});
    $('#selectLinha').css({display: 'none'});

});


$('select[name="grupo"]').on('change', function () {
   var cod_grupo = $(this).val();

   // Limpa os checkboxes que talvez foram selecionados
    $('input[type="checkbox"]').each(function () {
        if ( $(this)[0].checked == true ) {
            $(this).click(); //simula o click para desmarcar
        }
    });

   // Torna os selects invisiveis novamente
   if (!cod_grupo) {
       $('#multiSelectVeiculosTUE').css({display: 'none'});
       $('#multiSelectVeiculosVLT').css({display: 'none'});
       $('#selectLinha').css({display: 'none'});
   }

   // TUE -- Exibe apenas o select Veiculos TUE
   if (cod_grupo == 23) {
       $('#selectLinha').css({display: 'none'});
       $('#multiSelectVeiculosTUE').css({display: 'block'});
       $('#multiSelectVeiculosVLT').css({display: 'none'});
   }

   // VLT -- Exibe apenas os selects Linha e Veiculos VLT
   if (cod_grupo == 22) {
       $('#selectLinha').css({display: 'block'});
       $('#multiSelectVeiculosVLT').css({display: 'block'});
       $('#multiSelectVeiculosTUE').css({display: 'none'});
   }

});


$('button[type="submit"]').on('click', function(e){
    e.preventDefault();

    var hasGrupo = $('select[name="grupo"]').val() ? true : false;

    if ( !hasGrupo ) {
        alertaJquery("Alerta", 'Voce deve selecionar um Grupo', "alert");
    }
    else {
        $('#formCsvKmRodadosPorVeiculo').submit();
    }


});