$('SELECT[name="linha"]').change(function () {
    apiLinhaTrecho($(this), $('SELECT[name="trecho"]'));

    if($(this).val() == "") {
        $('SELECT[name="trecho"]').html("<option value=''></option>");
    }
});


$('SELECT[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('SELECT[name="sistema"]'));
    $('SELECT[name="subsistema"]').html("<option value=''>TODOS</option>");

    if ($(this).val() == "22" || $(this).val() == "23" || $(this).val() == "26") {
        $('#divMaterialRodante').show();
        apiGetVeiculo($(this).val(), $('select[name="veiculo"]'));
    }else{
        $('#divMaterialRodante').hide();
        $('select[name="veiculo"]').val('');
    }
    if($(this).val() == "28" || $(this).val() == "27"){
        $('#sublocal').show();
    }else{
        $('#sublocal').hide();
        $('select[name="local"]').val('');
        $('select[name="subLocal"]').val('');
    }
});


$('SELECT[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('SELECT[name="subsistema"]'));
});

$('select[name="local"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocal"]'));
});


$('#gerarGrafico').click(function (e) {
    e.preventDefault();

    $.post(
        caminho + "/api/returnResult/getTML",
        $('#formRelTML').serialize(),
        function (json) {
            console.log(json);
            $('#total').html(json[0].total_falhas);
            $('#tml').html(json[0].tml.split('.')[0]);
        },'json'
    ).error(function(msg){
        alert(JSON.stringify(msg));
    });

});

$('.limpaFiltro').on('click', function () {
    $('#formRelTML input').val('');
    $('#formRelTML select').val('');
    $('select[name="subsistema"]').html('<option value="">TODOS</option>');
    $('select[name="sistema"]').html('<option value="">TODOS</option>');
});

