//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupo"]').on('change', function () {
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistema"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistema"]').html('<option value="">SUBSISTEMA</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistema"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistema"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupo"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistema"]'));
        } else {
            $('select[name="subSistema"]').html('<option value="">SUBSISTEMA</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistema"]'));
    }
});

$('.limpaFiltro').on('click', function () {
    // $('select[name="tipoOs"]').val('');
    $('select[name="status"]').val('');
    $('select[name="grupo"]').val('');
    $('select[name="sistema"]').html('<option value="">SISTEMA</option>');
    $('select[name="subSistema"]').html('<option value="">SUBSISTEMA</option>');
});