//====================================================================================================================//
//=== Dados Gerais ===//

$('#footer').hide();

//====================================================================================================================//

//====================================================================================================================//
//=== Button Impress�o ===//

$(".imprimir").click(function () {
    $('.highcharts-button-box').hide();
    $('.highcharts-button-symbol').hide();
    window.print();
    window.close();
});

//====================================================================================================================//

function retira_acentos(palavra) {
    com_acento = '|����������������������������������������������';
    sem_acento = '|aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    nova = '';
    for (i = 0; i < palavra.length; i++) {
        if (com_acento.search(palavra.substr(i, 1)) >= 0) {
            nova += sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1);
        }
        else {
            nova += palavra.substr(i, 1);
        }
    }
    return nova;
}

//====================================================================================================================//
//=== Gr�fico Dados ===//

//T�tulo//
// var titulo = $('select[name = "Titulo"]').val();
// var subtitulo = $('input[name = "Subtitulo"]').val();

//Form//
// var form = $('input[name = "Form"]').val();

//Status//
var edificacoes = parseInt($('input[name="21"]').val());
var viaPermanente = parseInt($('input[name="24"]').val());
var subestacao = parseInt($('input[name="25"]').val());
var redeAerea = parseInt($('input[name="20"]').val());

if (isNaN(edificacoes) == true) {
    edificacoes = 0;
}
if (isNaN(viaPermanente) == true) {
    viaPermanente = 0;
}
if (isNaN(subestacao) == true) {
    subestacao = 0;
}
if (isNaN(redeAerea) == true) {
    redeAerea = 0;
}

titulo = "Aproveitamento de Ordens de Servico Corretivas";
var colors = ['#B0BEC5', '#FFEB3B', '#d50000', '#4CAF50'];
var series = [{
    name: 'Grupo',
    colorByPoint: true,
    data: [{
        name: 'Edificacoes',
        y: edificacoes
    }, {
        name: 'Via Permanente',
        y: viaPermanente
    }, {
        name: 'Subesta��o',
        y: subestacao
    }, {
        name: 'Rede A�rea',
        y: redeAerea
    }]
}];

//====================================================================================================================//

//====================================================================================================================//
//=== Gr�fico Status ===//

Highcharts.chart('resultApc', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
        enabled: false
    },
    title: {
        text: titulo
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                distance: -50,
                format: '{point.percentage:.1f} %',
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            showInLegend: true
        }
    },
    series: series
});

//====================================================================================================================//