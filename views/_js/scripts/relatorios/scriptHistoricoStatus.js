$(".tableRel").DataTable( {
    "language": {
        "lengthMenu": "Visualidado _MENU_ registros por p�gina",
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
        "search": "Filtrar ",

        "paginate": {
            "first": "Primeiro",
            "last": "Ultimo",
            "next": "Pr�ximo",
            "previous": "Anterior"
        },
        "loadingRecords": "Carregando...",
        "info": "Mostrando p�gina _PAGE_ de _PAGES_",
        "infoEmpty": "N�o h� informa��es",
        "infoFiltered": "(Filtrado de um total de _MAX_ dados)"
    },
    "order": false
    // ,
    // dom: 'Bfrtip',
    // buttons: [
        // {
        //     text: 'My button',
        //     className: 'btnTable',
        //     action: function () {
        //         alert( 'Coisa' );
        //     }
        // },
    //     { extend: 'excel', text:'<i class="fa fa-file-excel-o fa-fw"></i> Excel'     , className: 'btnTable' },
    //     { extend: 'pdf'  , text:'<i class="fa fa-file-pdf-o fa-fw"></i> PDF'       , className: 'btnTable' },
    //     { extend: 'print', text:'<i class="fa fa-print fa-fw"></i> Imprimir'  , className: 'btnTable' }
    // ]
});

$(".botaoVerForm").on("click", function () {
    var codigo = $(this).data('codigo');
    var dados = $(this).data('form');

    $.ajax({
        type: "POST",
        url: caminho + "/cadastroGeral/imprimir" + dados,
        data: { codigoForm : codigo},
        success: function () {
            window.open(caminho + "/dashboardGeral/print" + dados, "_blank");
        }
    });
});

    /*
    "/cadastroGeral/imprimirOsm"
    "/dashboardGeral/printOsm"

    "/cadastroGeral/imprimirSsm"
    "/dashboardGeral/printSsm"

    "/cadastroGeral/imprimirOsp"
    "/dashboardGeral/printOsp"

    "/cadastroGeral/imprimirSsp"
    "/dashboardGeral/printSsp"

    "/cadastroGeral/imprimirSsmp"
    "/dashboardGeral/printSsmp/Ed"

    "/cadastroGeral/imprimirOsmp"
    "/dashboardGeral/printOsmp"

    "/cadastroGeral/imprimirOsp"
    "/dashboardGeral/printOsp"

    "/cadastroGeral/imprimirOsp"
    "/dashboardGeral/printOsp"

    "/cadastroGeral/imprimirOsp"
    "/dashboardGeral/printOsp"

    "/cadastroGeral/imprimirOsp"
    "/dashboardGeral/printOsp"




    */