$(".imprimir").click(function () {
    window.print();
    window.close();
});

var corretivas  = $('canvas[name="chart"] input[name="corretivas"]').val().split(',');
var preventivas = $('canvas[name="chart"] input[name="preventivas"]').val().split(',');

new Chart($('canvas[name="chart"]'), {
    type: 'bar',
    data: {
        labels: [''],
        datasets: [{
            label: 'Corretivas',
            data: corretivas,
            backgroundColor: "rgba(180,0,0,0.5)"
        }, {
            label: 'Preventivas',
            data: preventivas,
            backgroundColor: "rgba(0,0,180,0.5)"
        }]
    }
});