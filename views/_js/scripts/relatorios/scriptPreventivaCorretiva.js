/**
 * Created by iramar.junior on 08/02/2017.
 */

//====================================================================================================================//
//===Gerar Gr�fico e Tabelas===//

$('#gerar').on('click', function () {
    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var linha = $('select[name="linha"]').val();
    var grupoSistema = $('select[name="grupoSistema"]').val();
    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
});

//====================================================================================================================//

//====================================================================================================================//
//=== Par�metros Pesquisa ===//
$('select[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('select[name="sistema"]'));
});

$('select[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});
//===Gr�fico do Relat�rio OS(s)===//

var prevTotal;
var prevAtendimento;
var prevEncerrada;
var prevCancelada;

var corrTotal;
var corrAtendimento;
var corrEncerrada;
var corrCancelada;

var gerTitulo;

var grupo = $('select[name="grupo"]');

$('.filtrarRel').click(function () {

    if(grupo.val() == ""){
        alert("Grupo de sistema obritat�rio.");
        return;
    }

    // loader();

    $.post(
        caminho + "/api/returnResult/getValueAnualOcorrencias",
        $('#parametros').serialize(),
        function (json) {
            console.log(json);

            gerTitulo = json.tituloFiltro;

            if(json.resultado[0]) {
                Highcharts.chart('resultGraph', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Relatorio Preventivas x Corretivas'
                    },
                    subtitle: {
                        text: gerTitulo
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: 'Quantidade de Ocorrencias - (un)'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    colors: ['#4CAF50', '#03A9F4'],
                    plotOptions: plotOptionsBar(''),
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> no total<br/>'
                    },
                    series: [{
                        name: 'Dados',
                        colorByPoint: true,
                        data: [{
                            name: 'Preventivas',
                            y: json.resultado[0].preventivas
                        }, {
                            name: 'Corretivas',
                            y: json.resultado[0].corretivas
                        }]
                    }]
                });
            }
        }, "json").done(function(){
        loading();
    }).error(function(msg){
        console.log(msg);
    });

});


//====================================================================================================================//

//====================================================================================================================//
//===DataTable===//

$("#resultTable").dataTable(configTable(1, false, 'asc'));

//====================================================================================================================//
