/**
 * Created by ricardo.diego on 15/02/2017.
 */

//====================================================================================================================//
//===Gerar Gr�fico e Tabelas===//

$('#gerar').on('click', function () {
    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var linha = $('select[name="linha"]').val();
    var grupoSistema = $('select[name="grupoSistema"]').val();
    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
});

//====================================================================================================================//

//====================================================================================================================//
//=== Par�metros Pesquisa ===//
$('select[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('select[name="sistema"]'));
});

$('select[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});
//===Gr�fico do Relat�rio OS(s)===//

$('.filtrarRel').click(function () {
    $.post(caminho + "/api/getResult/getValueGrafPreventivo",
        $('#parametros').serialize(),
        function(json){

            var abertas = json.totalAbertas;
            var encerradas = json.totalEncerrada;
            var canceladas = json.totalCancelada;
            var atendimento = json.totalAtendimento;
            // var retroativas = json.totalRetroativa;
            var titulo = json.tituloFiltroGrupo + " | " + json.tituloFiltro;

            Highcharts.chart('resultGraph', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Relatorio de Preventivas (Abertas x Encerradas)'
                },
                subtitle: {
                    text: titulo
                },
                xAxis: {
                    categories: [],
                    labels:
                    {
                        enabled: false
                    }
                },
                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Quantidade de Ocorrencia(s) - (un)'
                    }
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: plotOptionsBar(''),
                series: [{
                    name: 'Total',
                    data: [abertas]
                }, {
                    name: 'Em Aberto',
                    data: [atendimento]
                }, {
                    name: 'Executados',
                    data: [encerradas]
                }, {
                    name: 'Cancelados',
                    data: [canceladas]
                }]
            });
        }, "json"
    );
});

//====================================================================================================================//

//====================================================================================================================//
//===DataTable===//

$("#resultTable").dataTable(configTable(1, false, 'asc'));

//====================================================================================================================//
