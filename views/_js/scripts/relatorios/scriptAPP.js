//Gr�fico
var tituloGraf = '';

concatenarTitulo($("select[name='mes'] option:selected"), false);
// concatenarTitulo($("select[name='ano'] option:selected"), false);
concatenarTitulo($("select[name='linha'] option:selected"), '');

if ($("select[name='grupoSistema'] option:selected").val() == "") {
    tituloGraf = tituloGraf + " - GRUPO GERAL";
} else {
    tituloGraf = tituloGraf + " - GRUPO " + validaCaracteres($("select[name='grupoSistema'] option:selected").text().toUpperCase());
}

concatenarTitulo($("select[name='sistema'] option:selected"), '');
concatenarTitulo($("select[name='subSistema'] option:selected"), '');

function concatenarTitulo(select, titulo) {
    if (select.val() != "") {
        txt = validaCaracteres(select.text().toUpperCase());

        if (tituloGraf == '')
            tituloGraf = txt;
        else
            tituloGraf = tituloGraf + ' - ' + txt;
    }
}

function validaCaracteres(strToReplace) {
    strSChar = "����������������������������������������������";
    strNoSChars = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    var newStr = "";
    for (var i = 0; i < strToReplace.length; i++) {
        if (strSChar.indexOf(strToReplace.charAt(i)) != -1) {
            newStr += strNoSChars.substr(strSChar.search(strToReplace.substr(i, 1)), 1);
        } else {
            newStr += strToReplace.substr(i, 1);
        }
    }
    return newStr.replace(/[^a-zA-Z 0-9]/g, '').toUpperCase();
}

var total = parseInt($("input[name='previstas']").val());
var encerradas = parseInt($("input[name='concluidas']").val());
var abertasRestantes = total - encerradas;

Highcharts.chart('container', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'APP - Aproveitamento de Programacoes Preventivas'
    },
    tooltip: {
        pointFormat: '{series.name}: <strong>{point.percentage:.1f}%</strong>'
    },
    subtitle: {
        text: tituloGraf
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                distance: -50,
                format: '{point.percentage:.1f} %',
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            showInLegend: true
        }
    },
    colors: ['#4CAF50', '#2196F3'],
    series: [{
        name: 'Porcentagem',
        colorByPoint: true,
        data: [{
            name: 'PREVISTAS',
            y: abertasRestantes
        }, {
            name: 'ENCERRADAS',
            y: encerradas,
            sliced: true,
            selected: true
        }]
    }]
});

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistema"]').on('change', function () {
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistema"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistema"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistema"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistema"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistema"]'));
        } else {
            $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistema"]'));
    }
});

$('.limpaFiltro').on('click', function () {
    $('select[name="mes"]').val(new Date().getMonth() + 1);
    $('select[name="ano"]').val(new Date().getFullYear());

    $('select[name="linha"]').val('');
    $('select[name="grupoSistema"]').val('');
    $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    $('select[name="sistema"]').html('<option value="">Sistema</option>');
});

$(".tableRel").dataTable(configTable(0, false, 'asc'));
var tableModal = $(".tableRelModal").dataTable(configTable(0, false, 'asc'));

//TODO: Adicionar l�gica para observar OSP caso o grupo de sistema pesquisado seja Material Rodante.
$(document).on("click",".abrirModalCronograma", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var grupo  = dados.split('<input')[0];
    var cronograma       = dados.split('value="')[1].split('"')[0];

    apiModalCronograma(cronograma, grupo, tableModal);
});


$(document).on("click",".btnEditarOsmp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');

    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form  = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsmp",
        data: {codigoOs: dados},
        success: function(data) {
            window.location.replace(caminho+"/dashboardGeral/osmp/"+form);
        }
    });
});

//====================================================================================================================//
if(total != 0){
    var percnt = (encerradas * 100) / total;
}


$('#previstos').html(total);
$('#encerrados').html(encerradas);
$('#porcentagem').html(percnt.toFixed(1) + "%");