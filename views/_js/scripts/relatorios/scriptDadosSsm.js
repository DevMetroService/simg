/**
 * Created by ricardo.diego on 14/09/2017.
 */

//############## Local ##############
//############## Local ##############
//############## Local ##############

//Auto preenchimento do trecho ap�s sele��o da linha.
$('select[name="linhaPesquisaSsm"]').on("change", function () {
    var linha = ObjetoVal;
    if ($(this).val() == "") {
        linha.val("*");
        apiLinhaPn(linha, $('select[name="pnPesquisaSsm"]'));
    } else {
        linha.val($(this).val());
        $('select[name="pnPesquisaSsm"]').html('<option value="">Ponto Not�vel</option>');
    }

    apiLinhaTrecho(linha, $('select[name="trechoPesquisaSsm"]'));
});

//Auto preenchimento do ponto not�vel ap�s sele��o do trecho.
$('select[name="trechoPesquisaSsm"]').on("change", function () {
    var trecho = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="linhaPesquisaSsm"]').val() == "") {
            trecho.val("*");
            apiTrechoPn(trecho, $('select[name="pnPesquisaSsm"]'));
        } else {
            $('select[name="pnPesquisaSsm"]').html('<option value="">Ponto Not�vel</option>');
        }
    } else {
        trecho.val($(this).val());
        apiTrechoPn(trecho, $('select[name="pnPesquisaSsm"]'));
    }
});

//################ Falha ################
//################ Falha ################
//################ Falha ################

var mtRod = $('.mtRod');

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistemaPesquisa"]').on('change', function () {

    var grupo = ObjetoVal;

    if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {
        clearMtRod();
        mtRod.show();
    } else {
        clearMtRod();
        mtRod.hide();
    }

    if($(this).val() == "28" || $(this).val() == "27"){
        $('#local_sublocal').show();
        selectLocalSublocal.each(function(){
            $(this).attr("required", "required");
            $(this).val('');
        });

    }else {
        $('#local_sublocal').hide();
        selectLocalSublocal.each(function () {
            $(this).removeAttr("required");
        });
    }

    if ($(this).val() == "") {
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    } else {
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisa"]'));
    apiGetVeiculo(grupo.val(), $('select[name="veiculoPesquisa"]'));
    apiCarro('grupo', grupo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

$('select[name="localGrupo"]').on("change", function(){
    apiLocalSubLocal($(this), $('select[name="subLocalGrupo"]'));
});

$('select[name="veiculoPesquisa"]').on('change', function () {
    var veiculo = ObjetoVal;
    if ($(this).val() == "")
        veiculo.val("*");
    else
        veiculo.val($(this).val());

    apiCarro('veiculo', veiculo.val(), $('select[name="carroAvariadoPesquisa"]'));
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistemaPesquisa"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        if ($('select[name="grupoSistemaPesquisa"]').val() == "") {
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        } else {
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//################ Encaminhamento ################
//################ Encaminhamento ################
//################ Encaminhamento ################

//Auto preenchimento da equipe ap�s sele��o da unidade
$('select[name="unidadeEncaminhadaPesquisa"]').on("change", function () {
    var local = ObjetoVal;
    if ($(this).val() == "") {
        local.val("*");
    } else {
        local.val($(this).val());
    }
    apiLocalEquipe(local, $('select[name="equipePesquisa"]'));
});


function clearMtRod() {
    $('select[name="veiculoPesquisaOsm"]').val('');
    $('select[name="carroAvariadoPesquisaOsm"]').val('');
    $('select[name="carroLiderPesquisaOsm"]').val('');
    $('select[name="prefixoPesquisaOsm"]').val('');
    $('input[name="odometroPartirPesquisaOsm"]').val('');
    $('input[name="odometroAtePesquisaOsm"]').val('');
}