/**
 * Created by iramar.junior on 28/03/2017.
 */

$('document').ready(function(){
    var linha = $('select[name="linha"]');
    var trecho  = $('select[name="trecho"]');
    var pn = $('select[name="pn"]');
    var grupo = $('select[name="grupo"]');
    var sistema = $('select[name="sistema"]');
    var subsistema = $('select[name="subsistema"]');
    var veiculo = $('select[name="veiculo"]');


    linha.on("change", function () {
        if ($(this).val() == "") {
            trecho.html('<option value="">TODOS</option>');
        }

        apiLinhaTrecho(linha, trecho);
    });

    trecho.on("change", function () {
        if ($(this).val() == "") {
            pn.html('<option value="">TODOS</option>');
        } else {
            apiTrechoPn(trecho, pn);
        }
    });

    grupo.change(function () {
        if ($(this).val() == '22' || $(this).val() == '23' || $(this).val() == '26') {

            $('.materialRodanteDiv').show();
            $('.materialRodanteDiv select').val('');
            apiGetVeiculo($(this).val(), veiculo);

        } else {
            $('.materialRodanteDiv').hide();
            $('.materialRodanteDiv select').val('');
        }

        apiGrupoSistema($(this), sistema);
        subsistema.html('<option selected value="">TODOS</option>');

    });

    veiculo.on('change', function () {
        apiCarro('veiculo', $(this).val(), $('select[name="carro"]'));
    });

    sistema.on("change", function () {
        apiSistemaSub($(this), subsistema);
    });

    $('.limpaFiltro').on('click', function () {
        $('#formMTBF input').val('');
        $('#formMTBF select').val('');
        $('select[name="subsistema"]').html('<option value="">TODOS</option>');
        $('select[name="sistema"]').html('<option value="">TODOS</option>');
    });

    $('#gerarRelatorio').click(function (e) {
        e.preventDefault();

        $.post(
            caminho + "/api/returnResult/getMTBF",
            $('#formRelMTBF').serialize(),
            function (json) {
                console.log(json);
                var mtbfResultado = transformSecToHour(json[0].mtbf, true);
                console.log(transformSecToHour(json[0].mtbf));
                $('#total').html(json[0].count);
                $('#mtbf').html(mtbfResultado.dias+" dias "+mtbfResultado.horas+" horas "+mtbfResultado.minutos+" minutos");
            },'json'
        ).error(function(msg){
            console.log(msg);
        });
    });

    $('#avaria').on("input", function () {
        dataListFuncao($('#avaria'), $("#avariaDataList"), $('input[name="avaria"]'));
    });
});
