/**
 * Created by iramar.junior on 08/02/2017.
 */

//====================================================================================================================//
//===Gerar Gr�fico e Tabelas===//

$('#gerar').on('click', function () {
    var mes = $('select[name="mes"]').val();
    var ano = $('select[name="ano"]').val();
    var linha = $('select[name="linha"]').val();
    var grupoSistema = $('select[name="grupoSistema"]').val();
    var sistema = $('select[name="sistema"]').val();
    var subSistema = $('select[name="subSistema"]').val();
});

//====================================================================================================================//

//====================================================================================================================//
//=== Par�metros Pesquisa ===//
$('select[name="grupo"]').change(function () {
    apiGrupoSistema($(this), $('select[name="sistema"]'));
});

$('select[name="sistema"]').change(function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
});

//===Gr�fico do Relat�rio OS(s)===//

$('.filtrarRel').click(function () {

    $.post(caminho + "/api/getResult/getValueGrafCorretivo",
        $('#parametros').serialize(),
        function (json) {
            var abertas = json.totalAbertas;
            var encerradas = json.totalEncerrada;
            var canceladas = json.totalCancelada;
            var atendimento = json.totalAtendimento;
            var validacao = json.totalValidacao;
            var titulo = json.tituloFiltro;

            Highcharts.chart('resultGraph', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Relatorio de Corretivas (Abertas x Encerradas)'
                },
                subtitle: {
                    text: titulo
                },
                xAxis: {
                    categories: [],
                    crosshair: false,
                    labels: {
                        enabled: false
                    }
                },
                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Quantidade de Ocorrencia(s) - (un)'
                    }
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: plotOptionsBar(''),
                series: [{
                    name: 'Total',
                    data: [abertas]
                }, {
                    name: 'Em Aberto',
                    data: [atendimento]
                }, {
                    name: 'Executados',
                    data: [encerradas]
                }, {
                    name: 'Cancelados',
                    data: [canceladas]
                }, {
                    name: 'Aguardando Validacao',
                    data: [validacao]
                }]
            });
        }, "json"
    );

});

//====================================================================================================================//

//====================================================================================================================//
//===DataTable===//

$("#resultTable").dataTable(configTable(1, false, 'asc'));

//====================================================================================================================//