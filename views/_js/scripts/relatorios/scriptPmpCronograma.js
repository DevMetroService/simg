$(".tableRel").dataTable({
    "language": {
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
        "search": "Filtrar ",
        "loadingRecords": "Carregando...",
        "info": "_MAX_ itens na tabela",
        "infoEmpty": "N�o h� informa��es",
        "infoFiltered": "(Filtrado de um total de _MAX_ dados)",
    },
    "paging": false
});

var tableModal = $(".tableRelModal").dataTable({
    "language": {
        "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
        "search": "Filtrar ",
        "loadingRecords": "Carregando...",
        "info": "_MAX_ itens na tabela",
        "infoEmpty": "N�o h� informa��es",
        "infoFiltered": "(Filtrado de um total de _MAX_ dados)",
    },
    "paging": false
});

if($('input[name="pmp"]').val() != ""){
    $('#formRel input').removeAttr('required');
    $('#formRel input').attr('disabled', 'disabled');
    $('#formRel select').removeAttr('required');
    $('#formRel select').attr('disabled', 'disabled');

    $('#formRel input[name="pmp"]').removeAttr('disabled');
    $('#formRel select[name="ano"]').removeAttr('disabled');
}

$('input[name="pmp"]').on('blur', function () {
    if($(this).val()){
        $('#formRel input').removeAttr('required');
        $('#formRel input').attr('disabled', 'disabled');
        $('#formRel select').removeAttr('required');
        $('#formRel select').attr('disabled', 'disabled');

        $('#formRel input[name="pmp"]').removeAttr('disabled');
        $('#formRel select[name="ano"]').removeAttr('disabled');
    }else{
        $('#formRel input').removeAttr('disabled');
        $('#formRel select').removeAttr('disabled');

        $('select[name="grupoSistema"]').attr('required', 'required');
        $('select[name="sistema"]').attr('required', 'required');
        $('select[name="servicoPmp"]').attr('required', 'required');
        $('select[name="linha"]').attr('required', 'required');

        $('select[name="grupoSistema"]').val('');
        $('select[name="linha"]').val('');
        $('#formRel input').val('');

        $('select[name="sistema"]').html('<option value="">Sistema</option>');
        $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
        $('select[name="servicoPmp"]').html('<option selected="selected" value="">Servi�o</option>');
        $('select[name="local"]').html('<option selected="selected" value="">Local</option>');
        $('select[name="estacaoInicial"]').html('<option selected="selected" value="">Esta��o Inicial</option>');
        $('select[name="estacaoFinal"]').html('<option selected="selected" value="">Esta��o Final</option>');
        $('select[name="amv"]').html('<option selected="selected" value=""></option>');
    }
});

if($('select[name="grupoSistema"]').val() != ""){
    switch ($('select[name="grupoSistema"]').val()){
        case '21': //Edificacao
        case '25': //Subestacao
            $('.local').show();
            $('.conjuntoVp').hide();

            $('select[name="local"]').attr('required', 'required');
            $('.conjuntoVp select').removeAttr('required');
            $('.conjuntoVp input').removeAttr('required');
            break;
        case '24'://ViaPermanente
            $('.local').hide();
            $('.conjuntoVp').show();

            $('select[name="local"]').removeAttr('required');
            $('.conjuntoVp select').attr('required', 'required');
            $('.conjuntoVp input').attr('required', 'required');
            $('.conjuntoVp select[name="amv"]').removeAttr('required');
            break;
        case '20': //Rede Aerea
            //$('.local').hide();
            //$('.conjuntoVp').show();
            break;
    }
}else{
    $('.local').hide();
    $('.conjuntoVp').hide();

    $('select[name="local"]').removeAttr('required');
    $('.conjuntoVp select').removeAttr('required');
    $('.conjuntoVp input').removeAttr('required');
}

//Auto preenchimento do sistema ap�s sele��o do grupo sistema.
$('select[name="grupoSistema"]').on('change', function () {
    switch ($(this).val()){
        case '21': //Edificacao
        case '25': //Subestacao
            $('.local').show();
            $('.conjuntoVp').hide();

            $('select[name="local"]').attr('required', 'required');
            $('.conjuntoVp select').removeAttr('required');
            $('.conjuntoVp input').removeAttr('required');
            break;
        case '24'://ViaPermanente
            $('.local').hide();
            $('.conjuntoVp').show();

            $('select[name="local"]').removeAttr('required');
            $('.conjuntoVp select').attr('required', 'required');
            $('.conjuntoVp input').attr('required', 'required');
            $('.conjuntoVp select[name="amv"]').removeAttr('required');
            break;
        //case '20': //Rede Aerea
            //$('.local').hide();
            //$('.conjuntoVp').show();
        //    break;
        default:
            $('.local').hide();
            $('.conjuntoVp').hide();

            $('select[name="local"]').removeAttr('required');
            $('.conjuntoVp select').removeAttr('required');
            $('.conjuntoVp input').removeAttr('required');
            break;
    }
    var grupo = ObjetoVal;
    if ($(this).val() == "") {
        $('select[name="sistema"]').html('<option value="">Sistema</option>');
    } else {
        grupo.val($(this).val());
        apiGrupoSistema(grupo, $('select[name="sistema"]'));
        if($('select[name="linha"]').val() != "")
            $('select[name="linha"]').change();
    }
    $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    $('select[name="servicoPmp"]').html('<option selected="selected" value="">Servi�o</option>');
});

//Auto preenchimento do subSistema ap�s sele��o do sistema.
$('select[name="sistema"]').on('change', function () {
    var sistema = ObjetoVal;
    if ($(this).val() == "") {
        $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    } else {
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistema"]'));
    }
    $('select[name="servicoPmp"]').html('<option selected="selected" value="">Servi�o</option>');
});

$(document).on("change", 'select[name="subSistema"]', function () {
    if ($(this).val() == "") {
        $('select[name="servicoPmp"]').html('<option selected="selected" value="">Servi�o</option>');
    } else {
        apiSistemaSubSistema($('select[name="sistema"]'), $(this), $('select[name="servicoPmp"]'));
    }
});

$("select[name='linha']").on("change", function () {
    if ($(this).val() == "") {
        $('select[name="local"]').html('<option selected="selected" value="">Local</option>');
        $('select[name="estacaoInicial"]').html('<option selected="selected" value="">Esta��o Inicial</option>');
        $('select[name="estacaoFinal"]').html('<option selected="selected" value="">Esta��o Final</option>');
    } else {
        if($('select[name="grupoSistema"]').val() != "")
            apiLinhaLocalgrupo($(this), $('select[name="grupoSistema"]'), $('select[name="local"]'));
        apiLinhaEstacao($(this), $('select[name="estacaoInicial"]'));
        apiLinhaEstacao($(this), $('select[name="estacaoFinal"]'));
    }
});

$("select[name='estacaoInicial']").on("change", function () {
    if ($(this).val() == "") {
        $('select[name="amv"]').html('<option selected="selected" value="">AMV</option>');
    }else {
        apiEstacaoAmv($(this), $('select[name="amv"]'));
    }
});

$('.limpaFiltro').on('click', function () {
    $('#formRel select').val('');
    $('#formRel input').val('');

    $('#formRel input').removeAttr('disabled');
    $('#formRel select').removeAttr('disabled');

    $('select[name="sistema"]').html('<option value="">Sistema</option>');
    $('select[name="subSistema"]').html('<option value="">Sub-Sistema</option>');
    $('select[name="servicoPmp"]').html('<option selected="selected" value="">Servi�o</option>');
    $('select[name="local"]').html('<option selected="selected" value="">Local</option>');
    $('select[name="estacaoInicial"]').html('<option selected="selected" value="">Esta��o Inicial</option>');
    $('select[name="estacaoFinal"]').html('<option selected="selected" value="">Esta��o Final</option>');
    $('select[name="amv"]').html('<option selected="selected" value=""></option>');

    $('select[name="ano"]').val(new Date().getFullYear());
});

//====================================================================================================================//

$(document).on("click",".abrirModalCronograma", function(){
    var dados = $(this).parent("td").parent("tr").find('td').html();

    var cronograma  = dados.split('<input')[0];
    var grupo       = dados.split('value="')[1].split('"')[0];

    apiModalCronograma(cronograma, grupo, tableModal);
});


$(document).on("click",".btnEditarOsmp", function(e){
    if(hasProcess(e)) return;

    $(this).attr('disabled', 'disabled');

    var dados = $(this).parent("td").parent("tr").find('td').html();
    var form  = $(this).val();

    $.ajax({
        type: "POST",
        url: caminho+"/cadastroGeral/refillOsmp",
        data: {codigoOs: dados},
        success: function(data) {
            window.location.replace(caminho+"/dashboardGeral/osmp/"+form);
        }
    });
});