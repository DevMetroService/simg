/**
 * Created by iramar.junior on 02/02/2017.
 */

//===Tabela===//
//====================================================================================================================//
$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function() { // include functions
    $('document').ready(function () {
        $("#resultadoPesquisaSsmp").dataTable(configTable(0, true, 'asc'));
    });
});

//===Fun��o Enter para pesquisar===//
$('body').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $(".btnPesquisar").trigger('click');
    }
});

if($('select[name="grupoSistemaPesquisa"]').val() == 21){
    $("#estacaoInicial").hide();
    $("#estacaoFinal").hide();
}

$('select[name="linha"]').change(function(){
    apiLinhaLocalgrupo($(this), $('select[name="grupoSistemaPesquisa"]'), $('select[name="localPesquisa"]'));


    apiLinhaEstacao($(this), $('select[name="estacaoInicialPesquisa"]'));
    apiLinhaEstacao($(this), $('select[name="estacaoFinalPesquisa"]'));
});

$('select[name="grupoSistemaPesquisa"]').change(function(){
    switch ($(this).val()) {
        case '21'://Edifica��es
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '25'://Subesta��o
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
        case '24'://Via Permanente
            $("#local").hide();
            $("#estacaoInicial").show();
            $("#estacaoFinal").show();
            break;
        case '20'://Rede A�rea
            $("#local").show();
            $("#estacaoInicial").hide();
            $("#estacaoFinal").hide();
            break;
    }

    apiGrupoServico($(this), $('select[name="servico"]') );

    var grupo = ObjetoVal;
    if ($(this).val() == ""){
        grupo.val("*");
        apiSistemaSub(grupo, $('select[name="subSistemaPesquisa"]'));
    }else{
        grupo.val($(this).val());
        $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
    }

    apiGrupoSistema(grupo, $('select[name="sistemaPesquisa"]'));
});

$('select[name="sistemaPesquisa"]').on('change', function(){
    var sistema = ObjetoVal;
    if($(this).val() == ""){
        if($('select[name="grupoSistemaPesquisa"]').val() == ""){
            sistema.val("*");
            apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
        }else{
            $('select[name="subSistemaPesquisa"]').html('<option value="">Sub-Sistema</option>');
        }
    }else{
        sistema.val($(this).val());
        apiSistemaSub(sistema, $('select[name="subSistemaPesquisa"]'));
    }
});

//===A��es===//
//====================================================================================================================//

$('.btnPesquisar').on('click', function () {
    // if ($('input[name="dataPartir"]').val() == '' && $('input[name="numeroSsmp"]').val() == '') {
    //     alertaJquery("Sugest�o", "A pesquisa solicitada ir� necessitar de alguns minutos para que seja realizada.<h4> Deseja acrescentar um intervalo de Data para facilitar a consulta?</h4>",
    //         "confirm",
    //         [{value: "Sim"},
    //             {value: "N�o"}],
    //         function (res) {
    //             if (res == "N�o") {
    //                 $("#pesquisa").submit();
    //             }
    //         });
    // } else {
    //     $("#pesquisa").submit();
    // }
    $("#pesquisa").submit();
});

$('.btnResetarPesquisa').on('click', function () {
    window.location.href = caminho + "/dashboardGeral/resetarPesquisaSsmp";
});

$(".btnAbrirPesquisaOsmp").on("click", function () {
    var codigoSsmp = $(this).parent("td").parent("tr").find('td').html();
    window.location.href = caminho + "/dashboardGeral/pesquisaSsmpParaOsmp/" + codigoSsmp;
    //window.open(caminho + "/dashboardGeral/pesquisaSspParaOsp/" + codigoSsp, "_blank");
});