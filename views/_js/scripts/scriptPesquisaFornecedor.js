/**
 * Created by abner.andrade on 22/12/2017.
 */


//  -----------------  CONFIGURA��ES DO LAYOUT ---------------------


//  Legendas Tooltip para os botoes
$(document).ready(function() {
    $('.btnToolTip').tooltip({ trigger: "hover" });
});


//  Exibe uma msg ao salvar os dados editados do fornecedor
$(document).ready(function () {
    console.log();
    var msg = $('#msgUpdateFornecedor').val();
    if (msg != '') {
        alertaJquery('Status da Atualiza��o', msg);
        $('#msgUpdate').val('');
    }
});


//  Layout da Tabela
var tabela;
$(document).ready(function () {
    tabela = $('#tabelaFornecedor').dataTable(configTable(0, false, 'desc'));
});


//  -----------------  EVENTOS ---------------------

$('#btnResetarPesquisa').click(function () {

    var filtros = [
        'nomeFantasia',
        'razaoSocial',
        'cnpj',
        'nomeContato',
        'avaliacao',
        'ramoAtividade',
        'municipio',
        'uf'
    ];

    var nfiltros = filtros.length;

    for(var i = 0; i <= nfiltros; i++) {
        var tipo = document.getElementById(filtros[i]).tagName;

        if (tipo == 'SELECT') {
            document.getElementById(filtros[i]).selectedIndex = -1;
        } else {
            document.getElementById(filtros[i]).value = "";
        }

    }
});

$('#btnPesquisar').on('click', function () {
    $('input[name="cnpj"]').val( desformataCnpj($('#cnpjFormatado').val()) );
    $('#formPesquisaFornecedor').submit();
});

$('#btn-modalSalvar').on('click', function () {
    var cnpj, telefone, cep;

    cnpj = $('input[name="cnpjFornecedor"]').val();
    telefone = $('input[name="telefoneContatoFornecedor"]').val();
    cep = $('input[name="cepFornecedor"]').val();

    $('input[name="hiddenCnpj"]').val(desformataCnpj(cnpj));
    $('input[name="hiddenTelefone"]').val(desformataTelefone(telefone));
    $('input[name="hiddenCep"]').val(desformataCep(cep));

    $('#formEditarFornecedor').submit();
});


$(document).on('change', '#uf', function () {
    if ($(this).val() != '') {
        document.getElementById('municipio').removeAttribute('disabled');
    } else {
        document.getElementById('municipio').setAttribute('disabled', '');
    }

    //Aqui o Ajax para mudar o municipio conforme estado selecionado
});


$(document).on('click', '.btnEditarMaterial', function (e) {

    $('#modal-editarFornecedor').modal('show');

    var cnpj = '';

    var linhaSelecionada = $(this).parents('tr'); //seleciona a linha da tabela que possui o botao clicado

    var table = $('#tabelaFornecedor');
    table.find(linhaSelecionada).each(function () {
        var cols = 0;

        //Percorre as colunas da linha atual
        $(this).find('td').each(function () {

            if (cols == 3) {
                cnpj = $(this).html();
            }

            cols++;
        });

        cols = 0;
    });

    //alert("Cnpj do Fornecedor �: \n" + cnpj);
    
    apiTodosDadosFornecedor(cnpj);

    //$('input[name="modal_codigoMaterial"]').val(dadosRetornados['cod_material']);

});


//  -----------------  FUN��ES ---------------------


/*
* ------------------------------------------------------
*      Fun��es para retirar Mascaras de Formata��o
* ------------------------------------------------------
*       desformataTelefone(telefone)
*       desformataCnpj(cnpj)
*       desformataCep(cep)
* ------------------------------------------------------
* */

function desformataCnpj(valor) {

    // Garante que o valor � uma string
    valor = valor.toString();

    // Remove caracteres inv�lidos do valor
    valor = valor.replace(/[^0-9]/g, '');

    return valor;
}

function desformataCep(valor) {

    // Garante que o valor � uma string
    valor = valor.toString();

    // Remove caracteres inv�lidos do valor
    valor = valor.replace(/-/g, '');

    return valor;
}

function desformataTelefone(valor) {

    // Garante que o valor � uma string
    valor = valor.toString();

    // Remove caracteres inv�lidos do valor
    valor = valor.replace('(', '');
    valor = valor.replace(')', '');
    valor = valor.replace('-', '');
    valor = valor.replace(' ', '');

    return valor;
}
