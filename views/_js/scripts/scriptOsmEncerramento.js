/**
 * Created by ricardo.diego on 16/12/2015.
 */

var tipoFechamento = $('select[name="descricaoTipoFechamento"]');
var tipoPendencia = $('#tipoPendencia');
var pendencia = $('#pendencia');
var materialTipoPendencia = $('#materialTipoPendencia');
var materialPendencia = $('#materialPendencia');
var btnListaMaterial = $('#listaMaterial');
var codigoOs = $('input[name="codigoOs"]');

var tableMaterial = $('.tableMaterial').DataTable(configTable(0, false, 'desc'));
// ######## Valida��o data
$('input[name="dataHoraFechamento"]').on('blur', function () {
    if (!($(this).val() == '')) {
        if (fctValidaDataHora($(this))) {
            var dataEncerramento    = transformaData($(this).val());
            var dataDesmobilizacao  = transformaData($('#desmobilizacao').val());

            if(dataEncerramento < dataDesmobilizacao){
                $(this).val('');
                alertaJquery("Alerta", 'Data de Encerramento n�o pode ser inferior a data de Desmobiliza��o', "alert");
            }

            var dataServer = new Date;

            if (dataEncerramento > dataServer) {
                $(this).val('');
                alertaJquery("Alerta", 'A data preenchida n�o pode ser superior a data e hor�rio atual', "alert");
            }
        }
    }
});

var showFixPendency = function()
{
  tipoPendencia.hide();
  pendencia.hide();
  materialTipoPendencia.show();
  materialPendencia.show();
  btnListaMaterial.show();
};

var hideFixPendency = function()
{
  tipoPendencia.show();
  pendencia.show();
  materialTipoPendencia.hide();
  materialPendencia.hide();
  btnListaMaterial.hide();
};

var retirarRequired = function() {
    $('select[name="tipoPendenciaFechamento"]').removeAttr('required');
    $('select[name="pendenciaFechamento"]').removeAttr('required');

    $('input[name="motivoNaoExecucao"]').removeAttr('required');

    $('select[name="unidadeContinuidade"]').removeAttr('required');
    $('select[name="equipeContinuidade"]').removeAttr('required');
    $('textarea[name="recomendacoesContinuidade"]').removeAttr('required');
};

// ######## Hide - Show / Tipo de Fechamento
tipoFechamento.on("change", function(){
    retirarRequired();

    $('button[name="btnEncerrarOs"]').removeAttr('disabled');

    switch($(this).val()) {
        case "1": //Sem pendencia
            hideFixPendency();
            $('#comPendencia').hide();
            $('#motivoNaoExecucao').hide();

            if ($('select[name="liberacaoTrafego"]').val() == "s") {
                $('#dadosTransferencia').hide();

                $('select[name="transferenciaAtividadeOs"]').attr('disabled', 'disabled');
                $('select[name="transferenciaAtividadeOs"] option:eq(0)').prop('selected', true);
                $('select[name="transferenciaAtividadeOs"]').val('n');
            } else {
                $('select[name="transferenciaAtividadeOs"]').removeAttr('disabled');
                if($('select[name="transferenciaAtividadeOs"] option:selected').val() == "n" ){
                    $('button[name="btnEncerrarOs"]').attr('disabled', 'disabled');
                }
            }
            break;

        case "2": //Com pendencia
            hideFixPendency();
            $('#comPendencia').show();

            $('input[name="numeroTipoPendenciaFechamento"]').val("");

            $('select[name="tipoPendenciaFechamento"]').val("");
            $('select[name="tipoPendenciaFechamento"]').attr('required', 'required');

            $('input[name="numeroPendenciaFechamento"]').val("");

            $('select[name="pendenciaFechamento"]').val("");
            $('select[name="pendenciaFechamento"]').attr('required', 'required');

            $('select[name="transferenciaAtividadeOs"]').prop('disabled', true);
            $('select[name="transferenciaAtividadeOs"] option:eq(0)').prop('selected', true);
            $('select[name="transferenciaAtividadeOs"]').val('n');

            $('#dadosTransferencia').hide();

            $('#motivoNaoExecucao').hide();
            break;

        case "3": //N�o Executado
            hideFixPendency();
            $('#comPendencia').hide();

            $('#motivoNaoExecucao').show();
            $('input[name="motivoNaoExecucao"]').attr('required', 'required');

            $('select[name="transferenciaAtividadeOs"]').removeAttr('disabled');
            break;

        case "4": // Com Pend�ncia de Material N�o Contratual
            alertaJquery("Pend�ncia de Material N�o Contratual",
                "<span style='font-size: small'>Esta pend�ncia far� com que o servi�o(SSM) fique <strong>AGUARDANDO LIBERA��O</strong> do setor respons�vel para aquisi��o do <strong>MATERIAL N�O CONTRATUAL</strong>.</br>Deseja selecionar esta pend�ncia?</span>",
                "confirm",
                [{value: "Sim"}, {value: "N�o"}],
                function (res){
                    if(res=="N�o")
                    {
                      tipoFechamento.val(1);
                      hideFixPendency();
                    }
                }
            );
          $('#comPendencia').show();

          showFixPendency();

          $('input[name="numeroTipoPendenciaFechamento"]').val("");

          tipoPendencia.val("");
          tipoPendencia.attr('required', 'required');

          $('input[name="numeroPendenciaFechamento"]').val("");

          pendencia.val("");
          pendencia.attr('required', 'required');

          $('select[name="transferenciaAtividadeOs"]').prop('disabled', true);
          $('select[name="transferenciaAtividadeOs"] option:eq(0)').prop('selected', true);
          $('select[name="transferenciaAtividadeOs"]').val('n');

          $('#dadosTransferencia').hide();

          $('#motivoNaoExecucao').hide();
          break;
    }

    $("#selectTransferencia").val($('select[name="transferenciaAtividadeOs"] option:selected').val());
});

if($('select[name="descricaoTipoFechamento"]').val()) {
    switch($('select[name="descricaoTipoFechamento"] option:selected').val()) {
        case "2": //Com pendencia
            hideFixPendency();
            $('#comPendencia').show();

            $('select[name="tipoPendenciaFechamento"]').attr('required', 'required');
            $('select[name="pendenciaFechamento"]').attr('required', 'required');
            break;

        case "3": //N�o Executado
            hideFixPendency();
            $('#motivoNaoExecucao').show();

            $('input[name="motivoNaoExecucao"]').attr('required', 'required');
            break;

        case "4": // Com Pend�ncia de Material N�o Contratual
            $('#comPendencia').show();

            showFixPendency();

            $('input[name="numeroTipoPendenciaFechamento"]').val("");

            tipoPendencia.val("");

            $('input[name="numeroPendenciaFechamento"]').val("");

            pendencia.val("");

            $('select[name="transferenciaAtividadeOs"]').prop('disabled', true);
            $('select[name="transferenciaAtividadeOs"] option:eq(0)').prop('selected', true);
            $('select[name="transferenciaAtividadeOs"]').val('n');

            $('#dadosTransferencia').hide();

            $('#motivoNaoExecucao').hide();
            break;
    }
}


// ######### Libera��o
$('select[name="liberacaoTrafego"]').on("change", function(){
    if($('select[name="liberacaoTrafego"]').val() == "n" ){
        if($('select[name="descricaoTipoFechamento"]').val() == "1" && $('select[name="transferenciaAtividadeOs"] option:selected').val() == "n" ){
            $('select[name="transferenciaAtividadeOs"]').removeAttr('disabled');
            $('button[name="btnEncerrarOs"]').attr('disabled', 'disabled');
        }else{
            $('button[name="btnEncerrarOs"]').removeAttr('disabled');
        }
    }else{
        if($('select[name="descricaoTipoFechamento"]').val() == "1"){
            $('select[name="transferenciaAtividadeOs"]').attr('disabled', 'disabled');
            $('select[name="transferenciaAtividadeOs"] option:eq(0)').prop('selected', true);
            $('select[name="transferenciaAtividadeOs"]').val('n');

            $('#dadosTransferencia').hide();

            $('select[name="unidadeContinuidade"]').removeAttr('required');
            $('select[name="equipeContinuidade"]').removeAttr('required');
            $('textarea[name="recomendacoesContinuidade"]').removeAttr('required');
        }

        $('button[name="btnEncerrarOs"]').removeAttr('disabled');
    }

    $("#selectTransferencia").val( $('select[name="transferenciaAtividadeOs"] option:selected').val() );
});

if($('select[name="liberacaoTrafego"]').val() == "s" ){
    if($('select[name="descricaoTipoFechamento"]').val() == "1"){
        $('select[name="transferenciaAtividadeOs"]').attr('disabled', 'disabled');

        $('#dadosTransferencia').hide();

        $('select[name="unidadeContinuidade"]').removeAttr('required');
        $('select[name="equipeContinuidade"]').removeAttr('required');
        $('textarea[name="recomendacoesContinuidade"]').removeAttr('required');
    }
}


// ########## Transferencia
$('select[name="transferenciaAtividadeOs"]').on("change", function(){
    if($('select[name="transferenciaAtividadeOs"] option:selected').val() == "s"){
        $('#dadosTransferencia').show();

        $('select[name="unidadeContinuidade"]').attr('required', 'required');
        $('select[name="equipeContinuidade"]').attr('required', 'required');
        $('textarea[name="recomendacoesContinuidade"]').attr('required', 'required');
    }else{
        $('#dadosTransferencia').hide();

        $('select[name="unidadeContinuidade"]').removeAttr('required');
        $('select[name="equipeContinuidade"]').removeAttr('required');
        $('textarea[name="recomendacoesContinuidade"]').removeAttr('required');
    }

    if( $('select[name="liberacaoTrafego"]').val() == "n" &&
        $('select[name="descricaoTipoFechamento"]').val() == "1" &&
        $('select[name="transferenciaAtividadeOs"] option:selected').val() == "n" ){

        $('button[name="btnEncerrarOs"]').attr('disabled', 'disabled');
    }else{
        $('button[name="btnEncerrarOs"]').removeAttr('disabled');
    }

    $("#selectTransferencia").val( $('select[name="transferenciaAtividadeOs"] option:selected').val() );
});

// ########### AutoComplete DataList Nome Matricula
var campoMatricula = $("input[name='matriculaResponsavelInformacoesOs']");
var dataListSolicitande = $('#nomeSolicitanteDataList');
var codFuncioanario = $("input[name='codFuncionario']");
var solicitante = $("input[name='responsavelInformacaoOs']");
var tipo_funcionario = $('select[name="codTipoFuncionario"]');

tipo_funcionario.on("change", function () {
    campoMatricula.val('');
    codFuncioanario.val('');

    var cod_origem = $(this).val();

    $.getJSON(
        caminho + "/api/returnResult/dados/getFuncionariosByTipo",
        {dados: cod_origem},
        function (json) {
            var option = "";
            $.each(json, function (key, value) {
                var funcionario = value;
                option += ('<option id="' + funcionario.cod_funcionario + '" data-mat="'+ value.matricula +'" value="' + funcionario.nome_funcionario + ' - ' + funcionario.matricula + '"/>').toUpperCase();
            });
            dataListSolicitande.html(option);
        }
    );

});

solicitante.on("input",function() {

    var valueFinal = dataListSolicitande.find("option[value='" + solicitante.val() + "']");
    if (valueFinal.val() != null) {

        codFuncioanario.val(valueFinal.attr('id'));

        apiMatriculaNomeFuncionario(tipo_funcionario, campoMatricula, solicitante);
    }
});

// ############ Com Pendencia
var tipoPendencia = $('select[name="tipoPendenciaFechamento"]');
var pendenciaFechamento = $('select[name="pendenciaFechamento"]');

//Fun��o de preenchimento de pendencias pelo tipo de pendencias.
var preenchimentoPendencia =  function()
{
    var tipo_pendencia = tipoPendencia.val();
    $('input[name="numeroTipoPendenciaFechamento"]').val(tipo_pendencia);

    $.getJSON(
        caminho+"/api/returnResult/tipoPendencia",
        { tipoPendencia: tipo_pendencia},
        function (json ){
            var option = "";

            option += '<option selected disabled value="">Selecione a Pendencia</option>';

            $.each(json, function(key, value){
                option += '<option value="' + key + '">' + value + '</option>';
            });

            pendenciaFechamento.html(option);

            $('input[name="numeroPendenciaFechamento"]').val(pendenciaFechamento.val());

        }
    )
};

tipoPendencia.change(function(){
  preenchimentoPendencia();
})

// ############# Transferencia
$('select[name="unidadeContinuidade"]').on("change", function(){
    $('input[name="numeroUnidadeContinuidade"]').val($(this).val());
    apiLocalEquipe($(this), $('select[name="equipeContinuidade"]'));
});

$('select[name="equipeContinuidade"]').on("change", function(){
    $('input[name="numeroEquipeContinuidade"]').val($(this).val());
});


// ############ btnEncerramento
$('button[name="btnEncerrarOs"]').click(function(){
    var data    = $('input[name="dataHoraFechamento"]').val();
    var mat     = $('input[name="matriculaResponsavelInformacoesOs"]').val();
    var permit  = false;
    var txt     = "";

    if( data != ''){
        permit = true;

        //TODO::Check if has non contratual material in modal

        switch ($('select[name="descricaoTipoFechamento"]').val()){
            case "2":
                if($('select[name="tipoPendenciaFechamento"] option:selected').val() == "" || $('select[name="pendenciaFechamento"] option:selected').val() == ""){
                    permit = false;
                    txt += "\nPreencha o tipo de pendencia e a pendencia.";
                }
                break;
            case "3":
                if($('input[name="motivoNaoExecucao"]').val() == ""){
                    permit = false;
                    txt += "\nPreencha o Motivo.";
                }
                break;
            case "1":
                break;

            case "4":
                var totalLinhas = tableMaterial.rows().count();

                if(totalLinhas == 0)
                {
                  permit = false;
                  txt += "\nNecess�rio informar quais materiais s�o necess�rios.";
                }
                break;
            default:
                permit = false;
                txt += "\nSelecione o tipo de fechamento.";
                break;
        }

        if($('select[name="transferenciaAtividadeOs"]').val() == "s"){
            if(($('select[name="unidadeContinuidade"] option:selected').val() == "" || $('select[name="equipeContinuidade"] option:selected').val() == "" || $('textarea[name="recomendacoesContinuidade"]').val() == "")){
                permit = false;
                txt += "\nPreencha a unidade, a equipe e as recomenda��es.";
            }
        }
    }

    if(permit){
        var confirm = function (result)
        {
            if(result == 'SIM')
                $('#formEncerramentoOs').submit();
        };

        if($('select[name="descricaoTipoFechamento"]').val() == "1"){
            if($('select[name="transferenciaAtividadeOs"]').val() == "s"){
                alertaJquery("Encerramento sem pend�ncia c/ transfer�ncia.", "Ap�s o encerramento, n�o ser� poss�vel a edi��o deste documento. </br>Deseja continuar?", "alert", [{ value: "SIM" },{ value: "N�O" }], confirm);
            }else{
                alertaJquery("Encerramento sem pend�ncia.", "Ap�s o encerramento, n�o ser� poss�vel a edi��o deste documento. </br>Deseja continuar?", "alert", [{ value: "SIM" },{ value: "N�O" }], confirm);
            }
        }else{
            if($('select[name="transferenciaAtividadeOs"]').val() == "s"){
                alertaJquery("Encerramento c/ pend�ncia.", "Ap�s o encerramento, n�o ser� poss�vel a edi��o deste documento. </br>Deseja continuar?", "alert", [{ value: "SIM" },{ value: "N�O" }], confirm);
            }else{
                alertaJquery("Encerramento c/ pend�ncia e transfer�ncia de atividade.", "Ap�s o encerramento, n�o ser� poss�vel a edi��o deste documento. </br>Deseja continuar?", "alert", [{ value: "SIM" },{ value: "N�O" }], confirm);
            }
        }

    }else{
        alertaJquery("Alerta", 'H� campos obrigat�rios ainda n�o preenchidos'+txt, "alert");
    }
});



//Tabela de lista de materiais!
var qtdMaterial =  $('#qtdMaterialNContratual');
var btnExcludeLine = "<button class='btn btn-circle btn-danger btnExcludeLine' title='Excluir Material'><i class='fa fa-close fa-fw'></i></button>"

$('#btnAddMat').click(function()
{
    var material = $('#materialNContratual option:selected');
    var nomeMaterial = $('#materialNContratual');

  //Verifica se h� informa��es em campos
  if(material.val() != "" && qtdMaterial.val() != "")
  {
    var dataMaterial = {
      cod_osm: codigoOs.val(),
      cod_material: material.val(),
      quantidade:  qtdMaterial.val()
    };
    // Persiste informa��o em banco de dados
    $.post(caminho + "/OSM/registrarMaterialNaoContratual/",
        dataMaterial,
        function (json) {

          // adiciona as informa��es na tabela se persistido em banco de dados
          tableMaterial.row.add(
            [
              json, material.text(), qtdMaterial.val(), btnExcludeLine
            ]
          ).draw();

          //Limpa
          nomeMaterial.val("");
          qtdMaterial.val("");
        }
    );

  }else{
    alertaJquery("Campos obrigat�rios vazios", "H� campos obrigat�rios vazios. </br> Por gentileza, verificar.", "alert");
  }
});

$('#tableMaterial tbody').on( 'click', '.btnExcludeLine', function () {

    var listRow = $(this).parents('tr');
    var dataMaterial = {
      cod_osm_material_solicitado: parseInt( listRow.find('td').html() )
    };

    $.post(caminho + "/OSM/removerMaterialNaoContratual/",
        dataMaterial,
        function (json) {
          console.log(json);
          // adiciona as informa��es na tabela se persistido em banco de dados
          tableMaterial.row( listRow )
              .remove()
              .draw();
        }
    );


} );

//Fun��o ao se fechar o modal de materiais solicitados.
$('#materialAguardandoLiberacao').on('hide.bs.modal', function()
{
    var btnLista = $('#btnListaMaterial');
    var totalLinhas = tableMaterial.rows().count();
    if(totalLinhas > 0)
    {
      btnLista.html(totalLinhas + " - Lista de Materiais");
      btnLista.removeClass('btn-danger');
      btnLista.addClass('btn-success');
    }else{
      btnLista.html("Lista de Materiais");
      btnLista.addClass('btn-danger');
      btnLista.removeClass('btn-success');
    }
});

function transformaData(data) {
    var dia = data.substring(0, 2);
    var mes = data.substring(3, 5);
    var ano = data.substring(6, 10);

    var hor = data.substring(11, 13);
    var min = data.substring(14, 16);

    return new Date(ano, (mes - 1), dia, hor, min);
}

$('button[name="btnSalvarOs"]').on('click', function(e){
    if(hasProcess(e)) return;

    var resultado = validacao_formulario($('#formEncerramentoOs input'), $('#formEncerramentoOs select'), $('#formEncerramentoOs textarea'));

    if(resultado['permissao']){
        $('#formEncerramentoOs').submit();
    }else{
        console.log(resultado);
        alertaJquery('Falta de Informa��es', 'H� campos obrigat�rios vazios.', 'alert');
        $('body').removeClass('processing');
    }
});

var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

if (isElementInView) {
    $('#menuComplementoFixed').hide();
} else {
    $('#menuComplementoFixed').show();
}

$(window).scroll(function(){
    var isElementInView = Utils.isElementInView($('#menuComplementoStatic'), false);

    if (isElementInView) {
        // $('#menuComplementoFixed').fadeOut(500);
        $('#menuComplementoFixed').hide();
    } else {
        $('#menuComplementoFixed').show();
    }

});


$(function () {
    $('[data-toggle="popover"]').popover(
        {
            "html": true
        }
    )
})