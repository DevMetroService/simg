
var tableSafD;
var tableSaf;
var tableSsmA;
var tableSsmD;
var tableSsmE;
var tableOsm;

$.getScript(caminho+"/views/_js/scripts/scriptDashboard.js", function(){ // include functions
    $('document').ready(function(){
        tableSafD  = $("#indicadorSafDevolvida").dataTable(configTable(0, false, 'desc'));
        
        tableSaf  = $("#indicadorSaf").dataTable(configTable(4, false, 'asc'));

        tableSsmA = $("#indicadorSsmAberta").dataTable(configTable(4, false, 'asc'));

        tableSsmD = $("#indicadorSsmDevolvida").dataTable(configTable(4, false, 'asc'));

        tableSsmE = $("#indicadorSsmEncaminhada").dataTable(configTable(4, false, 'asc'));

        tableOsm  = $("#indicadorOsmExecucao").dataTable(configTable(4, false, 'asc'));

        //$("#indicadorAuditoriaSaf").dataTable(configTable(0, false, 'desc'));
    });
});