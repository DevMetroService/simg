/**
 * Created by josue.santos on 10/10/2016.
 */

var tabelaPmpRa;
var tabelaPmpRaExec;

$.getScript(caminho + "/views/_js/scripts/scriptPmp.js", function(){ // include functions
    $(document).ready(function(){
        tabelaPmpRa = $("#tabelaPmpRa").dataTable(configTable(0, false, 'asc'));

        tabelaPmpRaExec = $("#tabelaPmpRaExec").dataTable(configTable(0, false, 'asc'));
    });
});

//***********************************Proprios do formularios

$("select[name='local']").change(function(){
    apiPostePorLocal($(this), $("select[name='poste']"));
});

//***********************************Proprios do formularios

var excluidosLinhasPmp = new Array();
var contador = 1;

//=================Come�o On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {
    var sistema         = $('select[name="sistema"] option:selected');
    var subSistema      = $('select[name="subSistema"] option:selected');
    var servicoPmp      = $('select[name="servicoPmp"] option:selected');
    var periodicidade   = $('select[name="periodicidade"] option:selected');
    var turno           = $('select[name="turno"] option:selected');
    var procedimento    = $('input[name="procedimento"]');
    var qzn             = $('input[name="quinzena"]');
    var maoObra         = $('input[name="maoObra"]');
    var horasUteis      = $('input[name="horasUteis"]');
    var homemHora       = $('input[name="homemHora"]');
    var linha           = $('input[name="linha"]');
    var local           = $('select[name="local"] option:selected');
    var poste           = $('select[name="poste"] option:selected');

    //Especial
    var linhaTxt      = $('input[name="linhaTxt"]');

    if (sistema.val() == ''         || sistema.val() == null        || sistema.val() == undefined ||
        subSistema.val() == ''      || subSistema.val() == null     || subSistema.val() == undefined ||
        servicoPmp.val() == ''      || servicoPmp.val() == null     || servicoPmp.val() == undefined ||
        periodicidade.val() == ''   || periodicidade.val() == null  || periodicidade.val() == undefined ||
        turno.val() == ''           || turno.val() == null          || turno.val() == undefined ||
        qzn.val() == ''             || qzn.val() == null            || qzn.val() == undefined ||
        maoObra.val() == ''         || maoObra.val() == null        || maoObra.val() == undefined ||
        horasUteis.val() == ''      || horasUteis.val() == null     || horasUteis.val() == undefined ||
        local.val() == ''           || local.val() == null          || local.val() == undefined){

        alertaJquery("Alerta", "Preencha todos os campos corretamente", "alert");

    } else
    {
        //================ Dados a serem adicionados na LISTA de PMP Edifica��o. ==========================//
        var txt = '';
        var duplicado = true;
        var col = tabelaPmpRaExec._('tr');

        //Tabela do Banco de Dados
        for (var i = 0; i < col.length; i++) {
            var dataTable = tabelaPmpRaExec.fnGetData(i);
            if ((dataTable[1] == sistema.text()) &&
                (dataTable[2] == subSistema.text()) &&
                (dataTable[3] == servicoPmp.text()) &&
                (dataTable[4] == periodicidade.text()) &&
                (dataTable[12] == local.text()) &&
                (dataTable[13] == poste.text() || dataTable[13] == poste.val()) ) {

                txt = txt + ' - cod - ' + dataTable[0].split("<input")[0] + " Duplicado no Banco de Dados.\n";
                duplicado = false;
                break;
            }
        }

        col = tabelaPmpRa._('tr');

        //Tabela de Lista

        for (var i = 0; i < col.length; i++) {
            var dataTable = tabelaPmpRa.fnGetData(i);
            
            if ((dataTable[1].split("<input")[0] == sistema.text()) &&
                (dataTable[2].split("<input")[0] == subSistema.text()) &&
                (dataTable[3].split("<input")[0] == servicoPmp.text()) &&
                (dataTable[4].split("<input")[0] == periodicidade.text()) &&
                (dataTable[12].split("<input")[0] == local.text()) &&
                (dataTable[13].split("<input")[0] == poste.text() || dataTable[13].split("<input")[0] == poste.val()) ) {

                txt = txt + ' - id - ' + dataTable[0] + " Duplicado na Lista.\n";
                duplicado = false;
                break;
            }
        }

        if (duplicado) {
            var disponivel = excluidosLinhasPmp.pop();

            if (disponivel == undefined)
                var ctdAux = contador;
            else
                var ctdAux = disponivel;

            //==========Turno==========//
            if (turno.val() == 'D') {
                var turnoHTML =
                    '<select name="turno' + ctdAux + '" class="form-control" required>' +
                    '<option value="D" selected>Diurno</option>' +
                    '<option value="N">Noturno</option>' +
                    '</select>';
            } else {
                var turnoHTML =
                    '<select name="turno' + ctdAux + '" class="form-control" required>' +
                    '<option value="N" selected>Noturno</option>' +
                    '<option value="D">Diurno</option>' +
                    '</select>';
            }

            tabelaPmpRa.fnAddData([
                ctdAux,
                //==========Sistema==========//
                sistema.text() + '<input type="hidden" name="sistema' + ctdAux + '" value="' + sistema.val() + '" class="form-control number" readonly>' +
                '<input type="hidden" name="grupo' + ctdAux + '" value="20" class="form-control number" readonly>',
                //==========SubSistema==========//
                subSistema.text() + '<input type="hidden" name="subSistema' + ctdAux + '" value="' + subSistema.val() + '" class="form-control number" readonly>',
                //==========Servi�o==========//
                servicoPmp.text() + '<input type="hidden" name="servicoPmp' + ctdAux + '" value="' + servicoPmp.val() + '" class="form-control number" readonly>',
                //==========Periodicidade==========//
                periodicidade.text() + '<input type="hidden" name="periodicidade' + ctdAux + '" value="' + periodicidade.val() + '" class="form-control number" readonly>',
                //==========Procedimento==========//
                procedimento.val(),
                //==========Qzn In�cio==========//
                '<input type="text" name="quinzena' + ctdAux + '" value="' + qzn.val() + '" class="form-control number">',
                //==========Turno==========//
                turnoHTML,
                //==========M�o de Obra==========//
                '<input name="maoObra' + ctdAux + '" type="text"  value="' + maoObra.val() + '" class="form-control number maoObra" required>',
                //==========Horas �teis==========//
                '<input name="horasUteis' + ctdAux + '" type="text"  value="' + horasUteis.val() + '" class="form-control number horasUteis" required>',
                //==========Homem Hora==========//
                '<input name="homemHora' + ctdAux + '" type="text"  value="' + homemHora.val() + '" class="form-control" readonly>',
                //==========Linha==========//
                linhaTxt.val() + '<input name="linha' + ctdAux + '" type="hidden"  value="' + linha.val() + '" class="form-control" readonly>',
                //==========Via==========//
                local.text() + '<input name="local' + ctdAux + '" type="hidden"  value="' + local.val() + '" class="form-control" readonly>',
                //==========Poste==========//
                '<input name="poste' + ctdAux + '" type="text"  value="' + poste.val() + '" class="form-control" readonly>',
                //==========A��es==========//
                '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
            ]);

            contador = contador + 1;
        }
        if (txt != '')
            alertaJquery("Alerta", "DUPLICADOS:\n\n <textarea rows='20' cols='30'>" + txt + "</textarea>", "alert");
    }
});
//=================Fim do On Click Plus==================//

var contadorExec = $("input[name='ctd']").val();

//=================================================//

$("#tabelaPmpRa").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpRa = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaPmpRa._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaPmpRa.fnGetData(i);
        if (dataTable[0] == idPmpRa) {
            tabelaPmpRa.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmp.push(idPmpRa);
    contador = contador - 1;
});

$('.salvarListaPmp').click(function () {
    var data = tabelaPmpRa.$('input, select').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaPmpRaExec.$('input, select').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + contadorExec;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    $.post(
        caminho + "/cadastroGeral/refillPmp",
        {
            sistema:        $('select[name="sistema"] option:selected').val(),
            subSistema:     $('select[name="subSistema"] option:selected').val(),
            servicoPmp:     $('select[name="servicoPmp"] option:selected').val(),
            periodicidade:  $('select[name="periodicidade"] option:selected').val(),
            procedimento:   $('input[name="procedimento"]').val(),
            quinzena:       $('input[name="quinzena"]').val(),
            turno:          $('select[name="turno"] option:selected').val(),
            maoObra:        $('input[name="maoObra"]').val(),
            horasUteis:     $('input[name="horasUteis"]').val(),
            homemHora:      $('input[name="homemHora"]').val(),
            linha:          $('input[name="linha"]').val(),
            local:          $('select[name="local"] option:selected').val(),
            poste:          $('select[name="poste"] option:selected').val(),
        }
    );

    if (data) {
        $.post(
            caminho + "/cadastroGeral/salvarListaPmp",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }
});

var ano = $('#pmpAno');
var ativo = $('#pmpAtivo');
var novo = $('#pmpNovo');

ano.change(function(){
    var d = new Date();
    var n = d.getFullYear();

    if(n == $(this).val())
    {
        ativo.prop('disabled', 'disabled');
        ativo.prop('checked', false);
    }else
    {
        ativo.removeAttr('disabled');
    }
});

$('.gerarPmpAnual').click(function () {
    var year = ano.val();
    var isAtivo = ativo.prop('checked');
    var isNovo = novo.prop('checked');

    alertaJquery('Gerar PMP Anual',
        'Tem certeza que deseja finalizar o PMP Anual?<h4>Verifique antes se todas as altera��es foram salvas.</h4>',
        'alert', [{value:"Sim"},{value:"N�o"}], function (res) {
            if(res == "Sim") {
                alertaJquery('Gerar PMP Anual',
                    '<h3>Esta a��o n�o poder� ser desfeita!</h3>Deseja continuar?</br><small>O Processo poder� levar alguns minutos. Por favor, Aguarde.</small>',
                    'alert', [{value: "Sim"}, {value: "N�o"}], function (res) {
                        if (res == "Sim") {
                            loader();
                            window.location.replace(caminho + "/pmp/gerarPmpAnual/20/"+year+"/"+isAtivo+"/"+isNovo);
                        }
                        if(res == "N�o")
                        {
                            $('#gerarCronograma').modal('show');
                        }
                    }
                );
            }
            if(res == "N�o")
            {
                $('#gerarCronograma').modal('show');
            }
        }
    );
});

$('[data-toggle="tooltip"]').tooltip(
    {
        html: true
    }
);