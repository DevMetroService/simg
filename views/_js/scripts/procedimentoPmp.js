/**
 * Created by ricardo.diego on 06/10/2016.
 */

$(document).on("change", 'select[name="sistema"]', function () {
    apiSistemaSub($(this), $('select[name="subSistema"]'));
    $('select[name="servicoPmp"]').html('<option selected="selected" value="">Selecione o Servi�o</option>');
});

//Auto preenchimento do servi�o ap�s sele��o do subsistema
$(document).on("change", 'select[name="subSistema"]', function () {
    apiSistemaSubSistema($('select[name="sistema"]'), $(this), $('select[name="servicoPmp"]'));
});

var tabelaLista;
var tabelaProcedimento;

$('document').ready(function () {
    tabelaLista = $("#indicadorLista").dataTable(configTable(0, false, 'asc'));

    tabelaProcedimento = $("#indicadorProcedimento").dataTable(configTable(0, false, 'asc'));
});

var excluidosLinhasPmpEd = new Array();
var contador = 1;

//================= On Click Plus =================//
$('button[name="btnPlus"]').on("click", function () {

    var sistema       = $('select[name="sistema"] option:selected');
    var subSistema    = $('select[name="subSistema"] option:selected');
    var servico_pmp   = $('select[name="servicoPmp"] option:selected');
    var periodicidade = $('select[name="periodicidadePmp"] option:selected');
    var procedimento  = $('select[name="procedimentoPmp"] option:selected');

    if (sistema.val() == ''         || sistema.val() == null        || sistema.val() == undefined ||
        subSistema.val() == ''      || subSistema.val() == null     || subSistema.val() == undefined ||
        servico_pmp.val() == ''     || servico_pmp.val() == null    || servico_pmp.val() == undefined ||
        periodicidade.val() == ''   || periodicidade.val() == null  || periodicidade.val() == undefined ||
        procedimento.val() == ''    || procedimento.val() == null   || procedimento.val() == undefined) {

        alertaJquery("Alerta", "Selecione todos os campos corretamente", "alert");

    } else {

        //================ Dados a serem adicionados na LISTA de PMP Edifica��o. ==========================//
        var txt = '';
        var duplicado = true;

        var col = tabelaProcedimento._('tr');

        //Tabela do Banco de Dados
        for (var i = 0; i < col.length; i++) {
            var dataTable = tabelaProcedimento.fnGetData(i);

            if ((dataTable[1] == sistema.text()) &&
                (dataTable[2] == subSistema.text()) &&
                (dataTable[3] == servico_pmp.text()) &&
                (dataTable[4] == periodicidade.text())) {

                txt = txt + ' - O registro ID - ' + dataTable[0].split("<input")[0] + " Sistema, SubSistema, Servi�o e Periodicidade j� existe no Banco de Dados.\n";
                duplicado = false;
                break;
            }
        }

        col = tabelaLista._('tr');

        //Tabela de Lista
        for (var i = 0; i < col.length; i++) {
            var dataTable = tabelaLista.fnGetData(i);

            if ((dataTable[1].split("<input")[0] == sistema.text()) &&
                (dataTable[2].split("<input")[0] == subSistema.text()) &&
                (dataTable[3].split("<input")[0] == servico_pmp.text()) &&
                (dataTable[4].split("<input")[0] == periodicidade.text())) {

                txt = txt + ' - O registro ID - ' + dataTable[0] + " Sistema, SubSistema, Servi�o e Periodicidade j� existe na Lista.\n";
                duplicado = false;
                break;
            }
        }

        if (duplicado) {

            var disponivel = excluidosLinhasPmpEd.pop();

            if (disponivel == undefined)
                var ctdAux = contador;
            else
                var ctdAux = disponivel;

            tabelaLista.fnAddData([
                ctdAux,
                //==========Sistema==========//
                sistema.text() + '<input type="hidden" name="sistema' + ctdAux + '" value="' + sistema.val() + '" class="form-control" readonly>',
                //==========SubSistema==========//
                subSistema.text() + '<input type="hidden" name="subSistema' + ctdAux + '" value="' + subSistema.val() + '" class="form-control" readonly>',
                //==========Servi�o==========//
                servico_pmp.text() + '<input type="hidden" name="servicoPmp' + ctdAux + '" value="' + servico_pmp.val() + '" class="form-control" readonly>',
                //==========Periodicidade==========//
                periodicidade.text() + '<input type="hidden" name="periodicidade' + ctdAux + '" value="' + periodicidade.val() + '" class="form-control" readonly>',
                //==========Procedimento==========//
                procedimento.text() + '<input type="hidden" name="procedimento' + ctdAux + '" value="' + procedimento.val() + '" class="form-control" readonly>',
                //==========A��es==========//
                '<button type="button" class="btn btn-danger apagarLinha" title="apagar">Apagar</button>'
            ]);

            contador = contador + 1;
        }

        if (txt != '') {
            alertaJquery("Alerta", "DUPLICADOS:\n\n" + txt, "alert");
        }
    }
});
//=================================================//

var ctdTabelaDb = $("input[name='ctd']").val();

//=================================================//

$("#indicadorLista").on("click", ".apagarLinha", function (e) { //Exclui a div de beneficiario respectivo.
    e.preventDefault();

    var idPmpEd = $(this).parent('td').parent('tr').find('td').html();

    col = tabelaLista._('tr');

    for (var i = 0; i < col.length; i++) {
        var dataTable = tabelaLista.fnGetData(i);
        if (dataTable[0] == idPmpEd) {
            tabelaLista.fnDeleteRow(i);
            break;
        }
    }

    excluidosLinhasPmpEd.push(idPmpEd);
    contador = contador - 1;
});


$('.salvarListaPmp').click(function () {
    var data = tabelaLista.$('input').serialize();
    if (data)
        data += "&counter=" + contador;

    var dataExec = tabelaProcedimento.$('input').serialize();
    if (dataExec)
        dataExec += "&counterExec=" + ctdTabelaDb;

    if (data) {
        data += "&" + dataExec;
    } else {
        data = dataExec;
    }

    $.post(
        caminho + "/cadastroGeral/refillPmp",
        {
            sistema:    $('select[name="sistema"] option:selected').val(),
            subSistema: $('select[name="subSistema"] option:selected').val()
        }
    );

    if (data) {
        $.post(
            caminho + "/cadastroGeral/cadastrarProcedimento",
            data, function () {
                window.location.reload();
            }
        ).error(function () {
            alertaJquery("Alerta", 'Ocorreu um erro ao tentar adicionar os dados no Banco de dados. Por favor, contate a TI.', "alert");
        });
    }
});