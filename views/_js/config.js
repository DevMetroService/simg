/**
 * Created by ricardo.diego on 10/10/2017.
 */
var SistemasFixos = ["20","21","24","25","27","28"];
//==================================================================//
$(window).load(function () {
    loading();
});

function loader() {
    $("#loading").show();
}

function loading(tempo) {
    if(tempo)
        $("#loading").delay(tempo).fadeOut("slow");
    else
        $("#loading").delay(250).fadeOut("slow");
}

//==================================================================//
var url = window.location.href;

url = url.split("#"); // Limpeza de ancoras de link <a>
url = url[0];

var splitUrl = url.split("/");

caminho = "http://" + splitUrl[2];
pagina = splitUrl[3];
localReturn = splitUrl[4];
caminhoReturn = caminho + "/" + splitUrl[3] + "/" + splitUrl[4];

//##########  Configura��o de tabelas dinamicas dataTable ##########
//##########  Configura��o de tabelas dinamicas dataTable ##########
//##########  Configura��o de tabelas dinamicas dataTable ##########

function configTable(organizar, headerFix, org, ocult, paging) {
    return {
        "language": {
            "lengthMenu": "Visualizado _MENU_ registros por página",
            "zeroRecords": "Tabela vazia. Nenhum cadastro visualizado.",
            "search": "Filtrar ",

            "paginate": {
                "first": "Primeiro",
                "last": "Ultimo",
                "next": "Próximo",
                "previous": "Anterior"
            },
            "loadingRecords": "Carregando...",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Não há informações",
            "infoFiltered": "(Filtrado _TOTAL_ de um total de _MAX_ dados)",
            buttons: {
                copyTitle: 'Copiar (Cole no Excel)',
                copySuccess: {
                    _: 'Copiado %d linhas',
                    1: 'Copiado 1 linha'
                },
                copyKeys: '<h3>Precione "Ctrl" + "C" para copiar<br/>ou clique na mensagem para cancelar.</h3>'
            }
        },
        paging: paging,
        // paging: paging,
        "order": [organizar, org],
        fixedHeader: {
            header: headerFix,
            footer: false
        },dom: '<"row"<"btnDataTable"B>><"row"<"form-inline"lfrtip>>',
        // DOM
        // B - Buttons
        // l - length
        // f - filter
        // r - processing
        // t - table
        // i - information
        // p - pagination
        // stateSave: true,
        buttons: [
            {
                extend: 'copy',
                text: 'Copiar Tabela'
            }
            // ,
            // {
            //     extend: 'colvis',
            //     collectionLayout: 'fixed two-column',
            //     text: 'Colunas'
            // }
        ],
        columnDefs: [
            {
                targets: ocult,
                visible: false
            }
        ]
    };
}

//##########  Propriedades de configura��es para Highcharts ##########
//##########  Propriedades de configura��es para Highcharts ##########
//##########  Propriedades de configura��es para Highcharts ##########

function plotOptionsBar(evento){

    if(evento == 'click_modal_cronograma'){
        return {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none',
                    rotation: 0,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
                point: {
                    events: {
                        click: function () {
                            $('.listaCronogramas').modal('show');
                            apiListaCronograma_grafico(this.category, tbl_listaCronograma, this.series.columnIndex, $('select[name="mesBarDashboard"]').val());
                            // alert('Category: ' + this.category + ', value: ' + this.series.columnIndex);
                        }

                    }
                }
            }
        }
    }else{
        return {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none',
                    rotation: 0,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }
        }
    }


}

//##########  Fun��es de Alerta ##########
//##########  Fun��es de Alerta ##########
//##########  Fun��es de Alerta ##########

function alertaJquery(titulo, msg, typeAlertConfirmErrorInfo, buttons, functionSuccess) {
    $.msgBox({
        title: titulo,
        content: msg,
        type: typeAlertConfirmErrorInfo,
        buttons: buttons,
        success: functionSuccess
    });
}

//##########  Fun��es de An�lise se conte�do � visto em tela ##########
//##########  Fun��es de An�lise se conte�do � visto em tela ##########
//##########  Fun��es de An�lise se conte�do � visto em tela ##########

function Utils() {

}

Utils.prototype = {
    constructor: Utils,
    isElementInView: function (element, fullyInView) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
            return ((pageTop < elementTop) && (pageBottom > elementBottom));
        } else {
            return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
    }
};

var Utils = new Utils();
