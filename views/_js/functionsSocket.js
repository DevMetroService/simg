/**
 * Created by josue.santos on 02/09/2016.
 */

//################################## Inserir Notificação ########################################
//################################## Inserir Notificação ########################################
//################################## Inserir Notificação ########################################
function inserirNotificacao(mensag, titulo, corAlert) {
    $.notify({
        title: titulo,
        message: mensag
    }, {
        template: '<div style="min-width: 500px" class="alert alert-' + corAlert + ' alert-dismissible" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button>' +
        '<h5>{1}</h5>' +
        '<h4>{2}</h4>' +
        '</div>',

        delay: 7000,         // Some em 7 segundos
        newest_on_top: true, // Proximo aparece por cima
        mouse_over: 'pause', // Para ao passar o mouse
        animate: {
            exit: "animated fadeOutDown"
        },
        placement: {
            from: "bottom",  // Onde? -- Abaixo
            align: "right"   // Alinhamneto? -- Direita
        }
    });
}

$('document').ready(function(){
  if (realTimeOff) {
      $.ajax({
          dataType: "json",
          url: caminho + "/notificacao/notificarNoRealTime",
          success: function (json) {
              if (json)
                  inserirNotificacao(json, '', 'warning');
          },
          erro: function (json) {
              alert('error');
          }
      });
  } else {

      var arrayEquipes = $('#cod_Equipes_Usuario').val(); // Pego no navegador.php

      if (arrayEquipes) {
          arrayEquipes = arrayEquipes.split("/");
          arrayEquipes.shift();
      }

      var nomeUsuario = $('input[name="nomeUsuario"]').val(); // Pego no Modal Alterar Senha

      var channelNotificacao;
      var channelListNotificacao;

      var direcionamento = $('#direcionamentoLocal').val(); // Pego no navegador.php

      if (direcionamento == "ccm") {
          channelNotificacao = "notificationCcm";
          channelListNotificacao = "listNotificationCcm";

      } else if (direcionamento == "Equipe" || direcionamento == "equipeMr") {
          channelNotificacao = "notificationEquipe";
          channelListNotificacao = "listNotificationEquipe";
      }

      socket.on(channelNotificacao, function (data) {
          var permitir = false;

          if (direcionamento == 'ccm') {
              permitir = true;
          } else if (nomeUsuario == data.nomeUsuario) {
              permitir = true;
          } else {
              for (var i = 0; i < arrayEquipes.length; i++) {
                  if (data.subChannelEquipe == arrayEquipes[i]) {
                      permitir = true;
                      break;
                  }
              }
          }

          if (permitir) {
              if (typeof data.mensag != 'undefined') {
                  var corAlert = "warning";
                  switch (data.nivel) {
                      case "A":
                          corAlert = "danger";
                          break;
                      case "B":
                          corAlert = "success";
                          break;
                  }
                  inserirNotificacao(data.mensag, data.titulo, corAlert);
              }
          }
      });

      if (channelListNotificacao) {
          socket.on(channelListNotificacao, function (data) {
              var htmlTxt = "";
              var quant = 0;
              var permitir = false;

              for (var i = 0; i < data.length; i++) {
                  if (quant >= 10) break;

                  if (direcionamento == 'ccm') {
                      permitir = true;
                  } else if (nomeUsuario == data[i].nomeUsuario) {
                      permitir = true;
                  } else {
                      for (var j = 0; j < arrayEquipes.length; j++) {
                          if (data[i].subChannelEquipe == arrayEquipes[j]) {
                              permitir = true;
                              break;
                          }
                      }
                  }

                  if (permitir) {
                      quant += 1;

                      //htmlTxt += "<li><a href='" + caminho + "/dashboardGeral/pesquisaSafParaSsm/" + data[i].numero + "' >" + data[i].mensag + " <strong>de nivel " + data[i].nivel + "</strong></a></li><li class='divider'></li>";
                      if (data[i].nivel)
                          htmlTxt += "<li><a>" + data[i].mensag + " <strong>de nivel " + data[i].nivel + "</strong></a></li><li class='divider'></li>";
                      else
                          htmlTxt += "<li><a>" + data[i].mensag + "</a></li><li class='divider'></li>";
                  }
              }

              if (quant > 0) {
                  $('.listaNotificacao').html(htmlTxt);
                  $('.iconNavegadorQuant').html(quant);
                  $('.iconNavegadorQuant').show();
              } else {
                  $('.listaNotificacao').html("<li><a>Não existe histórico de notificações</a></li>");
                  $('.iconNavegadorQuant').hide();
              }
          });
      } else {
          $('.iconeHistoricoRealTime').hide();
      }

      // Verificar existencia de Notificação/RealTime
      $('document').ready(function () {
          socket.emit(channelListNotificacao);

          $.ajax({
              dataType: "json",
              url: caminho + "/notificacao/notificar",
              success: function (json) {
                  if (json.statusEmit) {
                      $.each(json.channel, function (key, value) {
                          socket.emit(value, json);
                      });
                      if (json.usuarioCriador)
                          inserirNotificacao(json.mensag, '', 'warning');
                  }
              },
              erro: function (json) {
                  alert('error');
              }
          });
      });
  }
})
