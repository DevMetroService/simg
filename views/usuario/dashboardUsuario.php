<?php
$this->dashboard->modalUserForm($this->dadosUsuario);
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">SIMG</h1>
    </div>

    <div class="row">
        <div class="col-md-12" id="messagebox">
            <?php
            if(isset($_SESSION['alertaAcao'])) {
                echo '<div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <h4>
                            ' . $_SESSION['alertaAcao'] . '
                        </h4>
                        </div>';
                unset($_SESSION['alertaAcao']);
            }
            ?>
        </div>
    </div>

    <div class="row scrollAuto">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-file-text-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="ctdAberta">
                            </div>
                            <div>SAF(s) Aberta(s)</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i id="actAnalise" class=""></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="ctdAnalise">
                            </div>
                            <div>SAF(s) em an�lise</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i id="actAtendimento" class=""></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="ctdAtendimento">
                            </div>
                            <div>SAF(s) em atendimento</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="ctdFinalizada">
                            </div>
                            <div>SAF(s) Finalizadas</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $this->dashboard->auditoriaSaf($this->dadosUsuario['cod_usuario']);
    ?>

    <!--    modal de valida��o-->


    <div class="row" id="safDevolvida">
        <div class="col-md-12">
            <?php
                $saf = $this->medoo->select("v_saf", "*", [
                    "AND" => [
                        "nome_status" => "Devolvida",
                        "usuario" => $this->dadosUsuario['usuario']
                    ]
                ]);
                $quantDevolvida = count($saf);
                if($quantDevolvida > 0){
                    echo "<div class='panel-danger panel'>";
                }else{
                    echo "<div class='panel-default panel'>";
                }
            ?>
                <div class="panel-heading" role="tab" id="headingSafDevolvida">
                    <div class="row">
                        <div class="col-xs-3">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorSafDevolvida" aria-expanded="false" aria-controls="collapseSafDevolvida">
                                <button class="btn btn-default"><label>SAF's Devolvidas</label></button>
                            </a>
                        </div>
                        <?php
                        if ($quantDevolvida > 0)
                            echo"<div class='col-xs-9'><h4>".$quantDevolvida." Saf(s) Devolvida(s)</h4></div>";
                        ?>
                    </div>
                </div>

                <div id="divIndicadorSafDevolvida" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingSafDevolvida">

                    <div class="panel-body"  id="tabelaSafDevolvida">
                        <table id="indicadorSafDevolvida" class="table table-striped table-bordered no-footer">
                            <thead id="headIndicadorSaf">
                                <tr role="row">
                                    <th>N�</th>
                                    <th>Local</th>
                                    <th>Data de Abertura</th>
                                    <th>Motivo</th>
                                    <th>N�vel</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>
                            <tbody id="bodyIndicadorSafDevolvida">
                            <?php

                            if (!empty($saf)) {
                                foreach ($saf as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_saf'] . '</td>');
                                    echo('<td>' . $dados['nome_linha'] . ' ' . $dados['nome_trecho'] . '</td>');
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                    echo('<td>' . $dados['descricao_status'] . '</td>');
                                    echo('<td>' . $dados['nivel'] . '</td>');
                                    echo('<td>');
                                    echo('<button class="btn btn-primary btn-circle btnEditarSaf" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                                    echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</td>');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="SAF">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <label>SAF</label>
                </div>
                <div class="panel-body" id="divIndicadorSaf">
                    <table id="indicadorSaf" class="table table-striped table-bordered no-footer">
                        <thead id="headIndicadorSaf">
                            <tr role="row">
                                <th>N�</th>
                                <th>Local</th>
                                <th>Data de Abertura</th>
                                <th>Avaria</th>
                                <th>Status</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                        </thead>
                        <tbody id="bodyIndicadorSaf">
                        <?php
                        $saf = $this->medoo->select("v_saf", "*", [
                            "AND" => [
                                "usuario" => $this->dadosUsuario['usuario']
                            ]
                        ]);

                        $qtdAberta = 0;
                        $qtdAnalise = 0;
                        $qtdAtendimento = 0;
                        $qtdFinalizada = 0;

                        if (!empty($saf)) {
                            foreach ($saf as $dados) {
                                if ($dados['cod_status'] == 4) {
                                    $statusSaf = "Aberta";
                                    addInTable($dados, $statusSaf);
                                    $qtdAberta += 1;

                                } else {
                                    $ssm = $this->medoo->select("v_ssm", "*", ["cod_saf" => $dados['cod_saf']]);

                                    if(!empty($ssm)){
                                        $statusSaf = "";

                                        foreach ($ssm as $value) {
                                            switch ($value['nome_status']){
                                                case "Encerrada":
                                                    if(empty($statusSaf))
                                                        $statusSaf = "Finalizada";
                                                    break;
                                                case "Cancelada":
                                                    if(empty($statusSaf))
                                                        $statusSaf = "Finalizada";
                                                    break;
                                                case "Autorizada":
                                                    $statusSaf = "Em atendimento";
                                                    break;
                                                default :
                                                    $statusSaf = "Em analise";
                                            }

                                        }

                                        switch ($statusSaf){
                                            case "Em atendimento":
                                                $qtdAtendimento += 1;
                                                break;
                                            case "Finalizada":
                                                $qtdFinalizada += 1;
                                                break;
                                            default :
                                                $qtdAnalise += 1;
                                        }

                                        addInTable($dados, $statusSaf);
                                    }
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo <<<HTML
    <input type="hidden" name="qtdAberta" value="{$qtdAberta}">
    <input type="hidden" name="qtdAnalise" value="{$qtdAnalise}">
    <input type="hidden" name="qtdAtendimento" value="{$qtdAtendimento}">
    <input type="hidden" name="qtdFinalizada" value="{$qtdFinalizada}">
HTML;

function addInTable($dados, $statusSaf){
    $dataAbt = MainController::parse_timestamp_static($dados['data_abertura']);

    echo <<<HTML
    <tr>
        <td>{$dados['cod_saf']}</td>
        <td>{$dados['nome_linha']} {$dados['nome_trecho']}</td>
        <td>{$dataAbt}</td>
        <td>{$dados['nome_avaria']}</td>
        <td>{$statusSaf}</td>
        <td>{$dados['nivel']}</td>
        <td>
            <button class="btn btn-primary btn-circle btnEditarSaf" type="button" title="Ver Dados Gerais"><i class="fa fa-eye fa-fw"></i></button>
            <button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
        </td>
    </tr>
HTML;
}
?>