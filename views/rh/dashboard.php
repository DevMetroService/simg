<div class="page-header">
    <h1>Gerenciador de Recursos Humanos</h1>
</div>

<div class="row">
    <div class="col-md-12" id="messagebox">
        <?php
        if(isset($_SESSION['feedback_banco'])) {
            echo '<div class="alert alert-' . $_SESSION['feedback_banco']['alert'] . ' alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <h4>
                        ' . $_SESSION['feedback_banco']['msg'] . '
                    </h4>
                </div>';
            unset($_SESSION['feedback_banco']);
        }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Funcion�rios Cadastrados</label></div>

                    <div class="panel-body">
                        <?php
                        $func = $this->medoo->select("funcionario(f)",
                            [
                                "[>]dados_empresa_funcionario" => "cod_funcionario",
                                "[>]centro_resultado(cr)"      => "cod_centro_resultado",
                                "[><]funcionario_empresa(fe)"  => "cod_funcionario_empresa",
                                "[>]cargos_funcionarios(cg)"   => ["fe.cod_cargos" => "cod_cargos"]
                            ], [
                                "fe.matricula",
                                "f.cod_funcionario",
                                "f.nome_funcionario",
                                "cg.descricao(cargo)",
                                "cr.descricao(centro_resultado)"
                            ],[
                                "fe.cod_tipo_funcionario"  => 1
                            ]);
                        ?>

                        <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                            <strong>Total de Itens da tabela: <?php echo (!empty($func)) ? count($func) : 0; ?><br/><br/></strong>
                            <thead>
                                <tr>
                                    <th>Matricula</th>
                                    <th>Nome Funcion�rio</th>
                                    <th>Cargo</th>
                                    <th>Centro de Resultado</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($func)){
                                foreach($func as $dados){
                                    echo('<tr>');
                                    echo('<td>'.$dados['matricula'].'</td>');
                                    echo('<td>'.$dados['nome_funcionario'].'</td>');
                                    echo('<td>'.$dados['cargo'].'</td>');
                                    echo('<td>'.$dados['centro_resultado'].'</td>');
                                    echo("<td>
                                            <a href='{$this->home_uri}/funcionario/edit/{$dados['cod_funcionario']}'>
                                                <button class='btn btn-primary btn-circle' type='button' title='Editar'><i class='fa fa-file-o fa-fw'></i></button>
                                            </a> 
                                        </td>");
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Consorciados CRJ</label></div>

                    <div class="panel-body">
                        <?php

                        $func = $this->medoo->select("funcionario(f)",
                            [
                                "[>]dados_empresa_funcionario" => "cod_funcionario",
                                "[>]centro_resultado(cr)"      => "cod_centro_resultado",
                                "[><]funcionario_empresa(fe)"  => "cod_funcionario_empresa",
                                "[>]cargos_funcionarios(cg)"   => ["fe.cod_cargos" => "cod_cargos"]
                            ], [
                                "fe.matricula",
                                "f.cod_funcionario",
                                "f.nome_funcionario",
                                "cg.descricao(cargo)",
                                "cr.descricao(centro_resultado)"
                            ],[
                                "fe.cod_tipo_funcionario"  => 4
                            ]);
                        ?>

                        <strong>Total de Itens da tabela: <?php echo count($func); ?><br/><br/></strong>
                        <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                            <thead>
                                <tr>
                                    <th>Matricula</th>
                                    <th>Nome Funcionario</th>
                                    <th>Cargo</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($func)){
                                foreach($func as $dados){
                                    echo('<tr>');
                                    echo('<td>'.$dados['matricula'].'</td>');
                                    echo('<td>'.$dados['nome_funcionario'].'</td>');
                                    echo('<td>'.$dados['cargo'].'</td>');
                                    echo("<td>
                                            <a href='{$this->home_uri}/funcionario/edit/{$dados['cod_funcionario']}'>
                                                <button class='btn btn-primary btn-circle' type='button' title='Editar'><i class='fa fa-file-o fa-fw'></i></button>
                                            </a> 
                                        </td>");
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Consorciados MPE</label></div>
                    <div class="panel-body">
                        <?php

                        $func = $this->medoo->select("funcionario(f)",
                            [
                                "[>]dados_empresa_funcionario" => "cod_funcionario",
                                "[>]centro_resultado(cr)"      => "cod_centro_resultado",
                                "[><]funcionario_empresa(fe)"  => "cod_funcionario_empresa",
                                "[>]cargos_funcionarios(cg)"   => ["fe.cod_cargos" => "cod_cargos"]
                            ], [
                                "fe.matricula",
                                "f.cod_funcionario",
                                "f.nome_funcionario",
                                "cg.descricao(cargo)",
                                "cr.descricao(centro_resultado)"
                            ],[
                                "fe.cod_tipo_funcionario"  => 3
                            ]);
                        ?>

                        <strong>Total de Itens da tabela: <?php echo count($func); ?><br/><br/></strong>
                        <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                            <thead>
                                <tr>
                                    <th>Matricula</th>
                                    <th>Nome Funcionario</th>
                                    <th>Cargo</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($func)){
                                foreach($func as $dados){
                                    echo('<tr>');
                                    echo('<td>'.$dados['matricula'].'</td>');
                                    echo('<td>'.$dados['nome_funcionario'].'</td>');
                                    echo('<td>'.$dados['cargo'].'</td>');
                                    echo("<td>
                                            <a href='{$this->home_uri}/funcionario/edit/{$dados['cod_funcionario']}'>
                                                <button class='btn btn-primary btn-circle' type='button' title='Editar'><i class='fa fa-file-o fa-fw'></i></button>
                                            </a> 
                                        </td>");
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Pessoas Jur�dicas</label></div>

                    <div class="panel-body">
                        <?php

                        $func = $this->medoo->select("funcionario(f)",
                            [
                                "[>]dados_empresa_funcionario" => "cod_funcionario",
                                "[>]centro_resultado(cr)"      => "cod_centro_resultado",
                                "[><]funcionario_empresa(fe)"  => "cod_funcionario_empresa",
                                "[>]cargos_funcionarios(cg)"   => ["fe.cod_cargos" => "cod_cargos"]
                            ], [
                                "fe.matricula",
                                "f.cod_funcionario",
                                "f.nome_funcionario",
                                "cg.descricao(cargo)",
                                "cr.descricao(centro_resultado)"
                            ],[
                                "fe.cod_tipo_funcionario"  => 5
                            ]);
                        ?>

                        <strong>Total de Itens da tabela: <?php echo count($func); ?><br/><br/></strong>
                        <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                            <thead>
                                <tr>
                                    <th>Matricula</th>
                                    <th>Nome Funcionario</th>
                                    <th>Cargo</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($func)){
                                foreach($func as $dados){
                                    echo('<tr>');
                                    echo('<td>'.$dados['matricula'].'</td>');
                                    echo('<td>'.$dados['nome_funcionario'].'</td>');
                                    echo('<td>'.$dados['cargo'].'</td>');
                                    echo("<td>
                                            <a href='{$this->home_uri}/funcionario/edit/{$dados['cod_funcionario']}'>
                                                <button class='btn btn-primary btn-circle' type='button' title='Editar'><i class='fa fa-file-o fa-fw'></i></button>
                                            </a> 
                                        </td>");
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>