<div class="page-header">
    <h1>Tabelas de Recursos Humanos</h1>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            echo $this->quantidadeMS
                            ?>
                        </div>
                        <div>Funcion�rios</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="<?php echo HOME_URI; ?>dashboardGeral/csv/MS"><button class="btn btn-default btn-lg btn-block"><i class="fa fa-file"></i> Metro Service</button></a>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            echo $this->quantidadeMPE
                            ?>
                        </div>
                        <div>Funcion�rios MPE</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="<?php echo HOME_URI; ?>dashboardGeral/csv/MPE"><button class="btn btn-default btn-lg btn-block"><i class="fa fa-file"></i> MPE</button></a>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            echo $this->quantidadeCRJ
                            ?>
                        </div>
                        <div>Funcion�rios CRJ</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="<?php echo HOME_URI; ?>dashboardGeral/csv/CRJ"><button class="btn btn-default btn-lg btn-block"><i class="fa fa-file"></i> CRJ</button></a>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            echo $this->quantidadePJ
                            ?>
                        </div>
                        <div>Funcion�rios PJ</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="<?php echo HOME_URI; ?>dashboardGeral/csv/PJ"><button class="btn btn-default btn-lg btn-block"><i class="fa fa-file"></i> PJ</button></a>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class='row'>
    <div class='col-sm-12'>
        <div class='panel panel-primary'>
            <div class='panel-heading' role='tab' id='heading'>
                <div class='row'>
                    <div class='col-md-4'>
                        <a class='collapsed' role='button' data-toggle='collapse' href='#tabelaCargos' aria-expanded='false'>
                            <button class='btn btn-default'><label>Cargos Cadastrados</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id='tabelaCargos' class='panel-collapse collapse out' role='tabpanel'>
                <div class='panel-body'>
                    <a href="<?php echo HOME_URI; ?>dashboardGeral/csv/Cargos">
                        <button class='btn btn-primary'>
                            <i class='fa fa-file fa-fw fa-1x'></i> Exportar
                        </button>
                    </a>
                </div>
                <div class="panel-body">
                    <?php
                    $cargo_funcionario = $this->medoo->select("cargos_funcionarios", "*", ["ORDER" => "descricao"]);
                    ?>

                    <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                        <strong>Total de Itens da tabela: <?php echo count($cargo_funcionario); ?><br/><br/></strong>
                        <thead>
                        <tr>
                            <th>N�</th>
                            <th>Nome do Cargo</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($cargo_funcionario)){
                            foreach($cargo_funcionario as $dados){
                                echo('<tr>');
                                echo('<td>'.$dados['cod_cargos'].'</td>');
                                echo('<td>'.$dados['descricao'].'</td>');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='row'>
    <div class='col-sm-12'>
        <div class='panel panel-primary'>
            <div class='panel-heading' role='tab' id='heading'>
                <div class='row'>
                    <div class='col-md-4'>
                        <a class='collapsed' role='button' data-toggle='collapse' href='#tabelaCentro' aria-expanded='false'>
                            <button class='btn btn-default'><label>Lista de Centros de Resultado</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id='tabelaCentro' class='panel-collapse collapse out' role='tabpanel'>
                <div class="panel-body">
                    <a href="<?php echo HOME_URI; ?>dashboardGeral/csv/CentroResultado">
                        <button class='btn btn-primary'>
                            <i class='fa fa-file fa-fw fa-1x'></i> Exportar
                        </button>
                    </a>
                </div>
                <div class='panel-body'>
                    <?php
                    $centroCusto = $this->medoo->select("centro_resultado", "*", ["ORDER" => "descricao"]);
                    ?>

                    <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                        <strong>Total de Itens da tabela: <?php echo count($centroCusto); ?><br/><br/></strong>
                        <thead>
                        <tr>
                            <th>C�digo</th>
                            <th>Centro de Resultado</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($centroCusto)){
                            foreach($centroCusto as $dados){
                                echo('<tr>');
                                echo('<td>'.$dados['num_centro_resultado'].'</td>');
                                echo('<td>'.$dados['descricao'].'</td>');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>