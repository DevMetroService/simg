<div class="page-header" style="margin-top: 0px;">
    <h1>Pesquisa</h1>
</div>


<div id="pesquisaFiltro" class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Dados da Pesquisa</div>

            <div class="panel-body">
                <form class="form-group" action="<?php echo(HOME_URI); ?>pesquisa/executaConsulta" method="post"
                      role="form">

                    <div class="row">
                        <div class="col-lg-2">
                            <label>Por Matr�cula:</label> <input name="pesquisaMatricula" type="text"
                                                                 class="form-control"
                                                                 value="<?php if (!empty($_POST['pesquisaMatricula'])) {
                                                                     echo($_POST['pesquisaMatricula']);
                                                                 } ?>">
                        </div>
                        <div class="col-lg-2">
                            <label>Por Nome:</label> <input name="pesquisaNome" type="text" class="form-control"
                                                            value="<?php if (!empty($_POST['pesquisaNome'])) {
                                                                echo($_POST['pesquisaNome']);
                                                            } ?>">
                        </div>
                        <div class="col-lg-2">
                            <label>Por CPF:</label> <input name="pesquisaCpf" type="text" class="form-control"
                                                           value="<?php if (!empty($_POST['pesquisaCpf'])) {
                                                               echo($_POST['pesquisaCpf']);
                                                           } ?>">
                        </div>
                         <div class="col-lg-2">
                            <label>Por PIS:</label> <input name="pesquisaPis" type="text" class="form-control"
                                                           value="<?php if (!empty($_POST['pesquisaPis'])) {
                                                               echo($_POST['pesquisaPis']);
                                                           } ?>">
                        </div>
                        <div class="col-lg-2">
                            <label>Por Rg:</label> <input name="pesquisaRg" type="text" class="form-control"
                                                          value="<?php if (!empty($_POST['pesquisaRg'])) {
                                                              echo($_POST['pesquisaRg']);
                                                          } ?>">
                        </div>
                       
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <p>
                                <small>Filtros:</small>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <label>Data de Nascimento</label>
                            <input name="pesquisaDataNascimento" type="text" class="form-control data"
                                   value="<?php if (!empty($_POST['pesquisaDataNascimento'])) {
                                       echo($_POST['pesquisaDataNascimento']);
                                   } ?>"/>
                        </div>
                        <div class="col-lg-2">
                            <label>Centro de Custo</label>
                            <input name="pesquisaCentroCusto" type="text" class="form-control"
                                   value="<?php if (!empty($_POST['pesquisaCentroCusto'])) {
                                       echo($_POST['pesquisaCentroCusto']);
                                   } ?>"/>
                        </div>
                        <div class="col-lg-2">
                            <label>Data de Admiss�o</label>
                            <input name="pesquisaDataAdmissao" type="text" class="form-control data"
                                   value="<?php if (!empty($_POST['pesquisaDataAdmissao'])) {
                                       echo($_POST['pesquisaDataAdmissao']);
                                   } ?>"/>
                        </div>

                        <div class="col-lg-2">
                            <div class="checkbox" style="padding-top: 1.5em">
                                <label>
                                    <?php
                                    if (!empty($_POST['mostrarDesativado']) && $_POST['mostrarDesativado'] === "on") {
                                        echo('<input type="checkbox" checked="checked" name="mostrarDesativado"/>Exibir Funcion�rios Desativados');
                                    } else {
                                        echo('<input type="checkbox" name="mostrarDesativado"/>Exibir Funcion�rios Desativados');
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <label>Data de Demiss�o</label>
                            <input name="pesquisaDataDemissao" type="text" class="form-control data"
                                   value="<?php if (!empty($_POST['pesquisaDataDemissao'])) {
                                       echo($_POST['pesquisaDataDemissao']);
                                   } ?>"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <a href="">
                                <button id="btnPesquisar" class="btn btn-success">Pesquisar</button>
                            </a>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-10">
                        <h4>Resultado</h4>
                    </div>

                    <div class="col-lg-2">
                       <a target="_blank" href="<?php echo(HOME_URI); ?>pesquisa/imprimirPesquisaRh">
							<button title="Imprimir lista da pesquisa" class="btn btn-default btn-lg">
								<i class="fa fa-print fa-fw"></i>
							</button> 
						</a>
                       
                    </div>
                </div>
            </div>

            <div class="panel-body" id="campoResultadoPesquisa">
                <div class="row">
                    <div class="col-lg-12">

                        <table id="resultadoPesquisa" class="table table-striped table-bordered">
                            <thead>
                                <tr role="row">
                                    <th>Matr�cula</th>
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>Centro de Custo</th>
                                    <th>Telefone</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th>Matr�cula</th>
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>Centro de Custo</th>
                                    <th>Telefone</th>
                                    <th>A��o</th>
                                </tr>
                            </tfoot>

                            <tbody>

                                    <?php
                                    $contador = 0;
                                    //foreach para cada resultado da pesquisa encontrado.
                                    //Sera impressora em suas respectivas celulas
                                        if ($_SESSION['resultadoPesquisaFuncionario']) {
                                            foreach ($_SESSION['resultadoPesquisaFuncionario'] as $dados) {
                                                echo('<tr>');
                                                echo('<td>' . $dados['matricula_funcionario'] .
                                                    '<input type="hidden" value="'.$dados['matricula_funcionario'].' name="matriculaAux'. contador .'"> "</td>');
                                                echo('<td>' . $dados['nome_funcionario'] . '</td>');
                                                echo('<td>' . $dados['cpf'] . '</td>');
                                                echo('<td>123</td>');
                                                echo('<td>' . $dados['cf_fone_res'] . '</td>');
                                                echo('<td>
                                                        <div class="btn-group" role="group" style="text-align: center;">');
                                                        echo('<form action="'.HOME_URI.'pesquisa/editarDados" method="post">
                                                                <button type="submit" title="Editar Dados" class="btn btn-primary btn-circle" value="'.$dados['matricula_funcionario'].'"><i class="fa fa-list fa-fw"></i></button></a>
                                                            </form>');
                                                        echo('<form action="'.HOME_URI.'pesquisa/perfilFuncionario" method="post">
                                                                <button type="submit" title="Ver Perfil" name="matriculaAux" class="btn btn-warning btn-circle" value="'.$dados['matricula_funcionario'].'"><i class="fa fa-user fa-fw"></i></button>
                                                            </form>');
                                                        echo('<form action="'.HOME_URI.'pesquisa/desligarFuncionario" method="post">
                                                                <button type="submit" title="Desligar Funcionario" class="btn btn-danger btn-circle" value="'.$dados['matricula_funcionario'].'"><i class="fa fa-times fa-fw"></i></button></a>
                                                            </form>
                                                        </div>
                                                   </td>');
                                                echo('</form>');
                                           }
                                        }
                                    ?>

                            </tbody>
                        </table>

                        <div class="modal fade" id="confirmacaoDesligarFuncionario" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title">
                                            Confirmar desligamento
                                        </h1>
                                    </div>

                                    <div class="modal-body">
                                        <label>Voc� tem certeza de que deseja desligar este funcionario?</label>
                                        <p>A a��o s� poder� ser desfeita pelo coordenador do Departamento Pessoal</p>
                                    </div>

                                    <div class="modal-footer">
                                        <a href="<?php echo (HOME_URI)?>/dashboardRh">
                                            <button type="button" class="btn btn-default">Cancelar</button>
                                        </a>
                                        <button type="submit" class="btn btn-primary">Desligar Funcionario</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>