
<!-- Iniciado T�tulo -->
<div class="page-header" style="margin-top: 0px;">
	<h1>Cadastro de Funcion�rio</h1>
</div>


<!-- Criado ordem de preenchimento -->
<div class="row">
	<div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a  href="<?php echo(HOME_URI); ?>cadastro/indexRefill">Dados Pessoais</a>

            <li><a href="<?php echo(HOME_URI); ?>cadastro/documentacaoFuncionarioRefill"> Documenta��o e Dados Banc�rios</a>

            <li class="active">Filia��o, Dependentes e Benefici�rios

            <li><?php
                if(!empty ($_SESSION['filiacaoFuncionario'])){
                    echo('<a href="' .HOME_URI. 'cadastro/empresaFuncionarioRefill">'.
                        'Dados da Empresa</a>');
                }else{
                    echo ('Dados da Empresa');
                }

                ?>

            <li><?php
                if(!empty ($_SESSION['empresaFuncionario'])){
                    echo('<a href="' .HOME_URI. 'cadastro/confirmaCadastroRefill">'.
                        'Confirma do Cadastro</a>');
                }else{
                    echo ('Confirma��o do Cadastro');
                }

                ?>


        </ol>

	</div>
</div>
<div id="dadosPessoais" class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">3- Filia��o / Dependentes / Benefici�rios</div>

			<div class="panel-body">

				<form id="formulario" class="form-group" method="post" role="form"	action="<?php echo(HOME_URI); ?>cadastro/empresaFuncionario">

					<div class="row">
						<div class="col-lg-5">
							<label>Nome do Pai</label> <input class="form-control" type="text" name="nomePaiCompleto"  value="<?php if (! empty($_SESSION['filiacaoFuncionario'])) { echo ($_SESSION['filiacaoFuncionario']['nomePaiCompleto']); }?>" />
						</div>
						<div class="col-lg-2">
							<label>Nascimento</label> <input class="form-control data" type="text" name="nascimentoPaiFuncionario" value="<?php if (! empty($_SESSION['filiacaoFuncionario'])) { echo ($_SESSION['filiacaoFuncionario']['nascimentoPaiFuncionario']); }?>" />
						</div>
						<div class="col-lg-2">
							<label>Para IRRF</label>
							<div>
							<?php 
								if (! empty($_SESSION['filiacaoFuncionario'])) { 
									if ($_SESSION['filiacaoFuncionario']['irrfPaiFuncionario'] === "s" ) {
										echo
											('<label class="radio-inline"><input type="radio" name="irrfPaiFuncionario" checked="checked" value="s"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="irrfPaiFuncionario" value="n"/>N�o</label>');
									} else {
										echo
										  	('<label class="radio-inline"><input type="radio" name="irrfPaiFuncionario" value="s"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="irrfPaiFuncionario" checked="checked" value="n">N�o</label>');
									}
										
								} else {
									echo
										('<label class="radio-inline"><input type="radio" name="irrfPaiFuncionario" value="s"/>Sim</label>
										  <label class="radio-inline"><input type="radio" name="irrfPaiFuncionario" checked="checked" value="n" />N�o</label>');
								}																		
									?>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-5">
							<label>Nome da M�e</label> <input class="form-control" type="text" name="nomeMaeCompleto" value="<?php if (! empty($_SESSION['filiacaoFuncionario'])) { echo ($_SESSION['filiacaoFuncionario']['nomeMaeCompleto']); }?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o nome."/>
						</div>
						<div class="col-lg-2">
							<label>Nascimento</label> <input class="form-control data" type="text" name="nascimentoMaeFuncionario" value="<?php if (! empty($_SESSION['filiacaoFuncionario'])) { echo ($_SESSION['filiacaoFuncionario']['nascimentoMaeFuncionario']); }?>"/>
						</div>
						<div class="col-lg-2">
							<label>Para IRRF</label>
							<div>
							<?php 
								if (! empty($_SESSION['filiacaoFuncionario'])) { 
									if ($_SESSION['filiacaoFuncionario']['irrfMaeFuncionario'] === "s" ) {
										echo
											('<label class="radio-inline"><input type="radio" name="irrfMaeFuncionario" checked="checked" value="s"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="irrfMaeFuncionario" value="n"/>N�o</label>');
									} else {
										echo
										  	('<label class="radio-inline"><input type="radio" name="irrfMaeFuncionario" value="s"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="irrfMaeFuncionario" checked="checked" value="n">N�o</label>');
									}
										
								} else {
									echo
										('<label class="radio-inline"><input type="radio" name="irrfMaeFuncionario" value="s"/>Sim</label>
										  <label class="radio-inline"><input type="radio" name="irrfMaeFuncionario" checked="checked" value="n" />N�o</label>');
								}																		
									?>
							</div>
						</div>
					</div>

                    <div class="row">
                        <div class="col-lg-5">
                            <label>Nome do Conjuge</label> <input class="form-control" type="text" name="nomeConjugeCompleto"  value="<?php if (! empty($_SESSION['filiacaoFuncionario'])) { echo ($_SESSION['filiacaoFuncionario']['nomeConjugeCompleto']); }?>" />
                        </div>
                        <div class="col-lg-2">
                            <label>Nascimento</label> <input class="form-control data" type="text" name="nascimentoConjugeFuncionario" value="<?php if (! empty($_SESSION['filiacaoFuncionario'])) { echo ($_SESSION['filiacaoFuncionario']['nascimentoConjugeFuncionario']); }?>" />
                        </div>
                        <div class="col-lg-2">
                            <label>Para IRRF</label>
                            <div>
                                <?php
                                if (! empty($_SESSION['filiacaoFuncionario'])) {
                                    if ($_SESSION['filiacaoFuncionario']['irrfConjugeFuncionario'] === "s" ) {
                                        echo
                                        ('<label class="radio-inline"><input type="radio" name="irrfConjugeFuncionario" checked="checked" value="s"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="irrfConjugeFuncionario" value="n"/>N�o</label>');
                                    } else {
                                        echo
                                        ('<label class="radio-inline"><input type="radio" name="irrfConjugeFuncionario" value="s"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="irrfConjugeFuncionario" checked="checked" value="n">N�o</label>');
                                    }
                                } else {
                                    echo
                                    ('<label class="radio-inline"><input type="radio" name="irrfConjugeFuncionario" value="s"/>Sim</label>
										  <label class="radio-inline"><input type="radio" name="irrfConjugeFuncionario" checked="checked" value="n" />N�o</label>');
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Sexo:</label>
                            <?php
                                echo('<select class="form-control" name="sexoConjugeFuncionario">');
                                    if(! empty($_SESSION['filiacaoFuncionario'])){
                                        if($_SESSION['filiacaoFuncionario']['sexoConjugeFuncionario'] === 'm'){
                                            echo('<option value="m" selected="selected">Masculino</option>');
                                            echo('<option value="f">Feminino</option>');
                                        }else{
                                            echo('<option value="m">Masculino</option>');
                                            echo('<option value="f" selected="selected">Feminino</option>');
                                        }
                                    }else{
                                        echo('<option selected="selected" disabled="disabled">Informe o sexo</option>');
                                        echo('<option value="m">Masculino</option>');
                                        echo('<option value="f">Feminino</option>');
                                    }
                                echo('</select>')
                            ?>
                        </div>
                    </div>

					<div class="row" style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

					<div class="row" style="padding-top: 2em;">
					
						<div class="col-lg-3">
							<label>Adicionar Benefici�rio</label>
							<button id="addBeneficiario" class="btn btn-info btn-circle" type="button" ><i class="fa fa-plus"></i></button>
						</div>

					</div>

					<div id="beneficiario">
					<?php
						$cont = 1;
						
						for ($i = 1; $i < 16; $i = $i + 1 )  {
							if(! empty($_SESSION['filiacaoFuncionario'] ) ) {
								if (! empty($_SESSION['filiacaoFuncionario']['nomeBeneficiarioFuncionario'. $i ])) {
									echo '<div class="row" style="border-top: 1px dashed black; padding-top: 2em;"><div class="col-lg-5"><label>Nome Completo</label>'; 
									echo '<input name="nomeBeneficiarioFuncionario'. $cont .'" type="text" class="form-control" value="'. ($_SESSION['filiacaoFuncionario']['nomeBeneficiarioFuncionario'. $i ]) . '"></div>'; 
									echo '<div class="col-lg-2"><label>Sexo</label><div><label class="radio-inline">'; 
									if( $_SESSION['filiacaoFuncionario']['sexoBeneficiarioFuncionario'. $i ] == 'm'){
										echo '<input type="radio" name="sexoBeneficiarioFuncionario'. $cont .'" checked="checked" value="m">Masculino</label> <label class="radio-inline"> <input type="radio" name="sexoBeneficiarioFuncionario'. $cont .'" value="f">Feminino</label></div></div>';
									}else {
										echo '<input type="radio" name="sexoBeneficiarioFuncionario'. $cont .'" value="m">Masculino</label> <label class="radio-inline"> <input type="radio" name="sexoBeneficiarioFuncionario'. $cont .'"  checked="checked" value="f">Feminino</label></div></div>';
									}
									echo '<div class="col-lg-2"><label>Parentesco</label> <input name="parentescoBeneficiarioFuncionario'. $cont .'" type="text" class="form-control" value="'. $_SESSION['filiacaoFuncionario']['parentescoBeneficiarioFuncionario'. $i ] .'"></div>';
									echo '<div class="col-lg-2"><label>Data de Nascimento</label> <input name="nascimentoBeneficiarioFuncionario'. $cont .'" type="text" class="form-control data" value="'.$_SESSION['filiacaoFuncionario']['nascimentoBeneficiarioFuncionario'. $i ].'"></div>';
									if( $_SESSION['filiacaoFuncionario']['irrfBeneficiarioFuncionario'. $i ] == 's'){
										echo '<div class="col-lg-2"><label>Para IRRF</label><div><label class="radio-inline"> <input type="radio" name="irrfBeneficiarioFuncionario'. $cont .'" value="s" checked="checked">Sim</label> <label class="radio-inline"> <input type="radio" name="irrfBeneficiarioFuncionario'. $cont .'" value="n">N�o </label></div> </div>';
									}else{
										echo '<div class="col-lg-2"><label>Para IRRF</label><div><label class="radio-inline"> <input type="radio" name="irrfBeneficiarioFuncionario'. $cont .'" value="s">Sim</label> <label class="radio-inline"> <input type="radio" name="irrfBeneficiarioFuncionario'. $cont .'" value="n" checked="checked">N�o </label></div> </div>';
									}
									echo '<button id="excluirBeneficiario" class="btn btn-danger btn-circle" value="'. $cont .'" type="button"><i class="fa fa-times"></i></button><label>Excluir</label></div>';
									$cont = $cont +1;
								}							
							}						
						}
						if($cont > 1){
							$cont = $cont -1;
							echo ('<input type="hidden" id="contadorBeneficiario" value="'. $cont. '">');
						}else{
							echo ('<input type="hidden" id="contadorBeneficiario" value="0">');
						}
					?>					
					</div>
					

					<div class="row" style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

					<div class="row" style="padding-top: 2em">
						<div class="col-lg-4">
							<button value="formulario/documentacaoFuncionario.php" class="btn btn-success btn-lg btn-block" type="submit">Salvar e Continuar</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>