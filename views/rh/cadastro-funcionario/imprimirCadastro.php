<!-- panel panel-default  -->

<div>
	<img src="<?php HOME_URI?>views/_images/metroservice_logo.png" width="150" height="60">
</div>
<div>
	<h3>Ficha Cadastral Cons�rcio Metro Service</h3>
</div>



<div class="row" style="border-top: 1px solid black">
	<div class="col-lg-12">
		<h4>
			<strong>Dados Pessoais</strong>
		</h4>
	</div>
</div>
					
<table class="table">
	<tbody>
		<tr>
			<td colspan="3">
				<label>Nome Completo:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper( $_SESSION ['dadosPessoais'] ['nomeCompleto'])."</h5>" );
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?> 
			</td>
			<td colspan="1">
                <?php
                $fotoID = $this->medoo->select('fotostemp', "nome");
                $contadorFoto  	= count($fotoID);
                if( $contadorFoto > 0 )
                {
                    $lastIDFuncionario 	= $fotoID[$contadorFoto-1];
                }

                $id = $this->pgLastInsertId("fotostemp_cod_fotostemp_seq");
                $nomeImagem = $this->medoo->select ("fotostemp", "nome", ['cod_fotostemp'=>$id]);
                echo ('<img src="'. HOME_URI .'/views/_images/rh/' . $lastIDFuncionario . '" alt="" width="177px" height="236px">');

                ?>
			
			</td>				
		</tr>
		
		<tr>
			<td colspan="1">
			<label>Idade:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						$dataAtual = DateTime::createFromFormat ( 'd-m-Y', date ( 'd-m-Y' ) );
						$dataNascimento = DateTime::createFromFormat ( 'd-m-Y', $_SESSION ['dadosPessoais'] ['dataNascimentoFuncionario'] );
						$idade = $dataAtual->diff ( $dataNascimento );
						echo ("<h5 style='font-family:verdana; margin-top: 0'>".$idade->format ( "%y" )." ANOS </h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				
				?>				
			</td>
			<td colspan="1">
				<label>Sexo:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						switch ($_SESSION ['dadosPessoais'] ['sexoFuncionario']) {
							default :
								echo ("N/A");
								break;
							
							case "m" :
								echo ("<h5 style='font-family:verdana; margin-top: 0'>MASCULINO</h5>");
								break;
							
							case "f" :
								echo ("<h5 style='font-family:verdana; margin-top: 0'>FEMININO</h5>");
								break;
						}
					} else {
						echo ("<br>N�O INFORMADO.");
					}

				?>
			</td>
			<td colspan="1">
				<label>Data Nascimento:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>".$_SESSION ['dadosPessoais'] ['dataNascimentoFuncionario']."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td colspan="1">
				<label>Estado Civil:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						$codigoEstadoCivil = $this->medoo->select("estado_civil", "descricao", ["descricao" => $_SESSION ['dadosPessoais'] ['estadoCivilFuncionario']]);
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($codigoEstadoCivil[0]) ."</h5>");
						unset($codigoEstadoCivil);
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			
		</tr>
		<tr>
			<td colspan="1">
				<label>Local de Nascimento:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['localNascimentoFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td colspan="1">
				<?php
					if (! empty ( $_SESSION ['dadosPessoais']['nacionalidadeFuncionario'] )) {
                        echo("<label>Nacionalidade:</label>");
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['nacionalidadeFuncionario']) ."</h5>");
					}
				?>
			</td>
			<td colspan="1">
				<label>Naturalizado:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						switch ($_SESSION ['dadosPessoais'] ['naturalidade']) {
							default :
								echo ("<br>N/A");
								break;
							
							case "true" :
								echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
								break;
							
							case "false" :
								echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
								break;
						}
					}
				?>
			</td>
			<td colspan="1">

				<?php
					if (! empty ( $_SESSION ['dadosPessoais']['tipoVistoFuncionario'] )) {
                        echo('<label>Tipo de Visto:</label>');
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['tipoVistoFuncionario']) ."</h5>");
					}
				?>
			</td>
		</tr>

		<tr>
			<td colspan="4"><br></td>
		</tr>
		
		<tr>
	
			<td colspan='3'>
				<label>Logradouro:</label> 
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['logradouroFuncionario']) .",  ".mb_strtoupper($_SESSION ['dadosPessoais'] ['numeroEnderecoFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?> 
			</td>
			<td>
				<label>Complemento:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						if ($_SESSION ['dadosPessoais'] ['complementoFuncionario']) {
							echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['complementoFuncionario']) ."</h5>");
						} else {
							echo ("N/A");
						}
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
		</tr>
		<tr>
			<td>
				<label>Bairro:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['bairroFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td>
				<label>Cidade / Munic�pio:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['municipioFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td>
				<label>UF:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['estadoFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td>
				<label>CEP:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['cepFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
		</tr>
		
		<tr>
			<td colspan="4"><br></td>
		</tr>
		
		<tr>
			<td>
				<label>Telefone Residencial:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['residencialFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td>
				<label>Telefone Celular:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['celularFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td>
				<label>Nome para Contato:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['nomeContatoFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
			<td>
				<label>Telefone para Contato:</label>
				<?php
					if (! empty ( $_SESSION ['dadosPessoais'] )) {
						echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['dadosPessoais'] ['contatoFuncionario']) ."</h5>");
					} else {
						echo ("<br>N�O INFORMADO.");
					}
				?>
			</td>
		</tr>
		
		<tr>
			<td>
				<label>Escolaridade:</label>
				<?php
                if (! empty ( $_SESSION ['dadosPessoais'] )) {
                    $codigoEscolaridade = $this->medoo->select("tipo_escolaridade", "descricao", ["cod_tipo_escolaridade" => (int)$_SESSION ['dadosPessoais'] ['escolaridadeFuncionario']]);
                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($codigoEscolaridade[0]) . "</h5>");
                    unset($codigoEscolaridade);
                }
                ?>
                    </td>
                    <?php
                    if (!empty ($_SESSION ['dadosPessoais'])) {
                        if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '7')                        # Se tem curso superior completo
                        {
                            echo('<td>');
                            echo('<label>Curso Superior:</label>');
                            echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario']) . "</h5>");
                            echo("</td>");

                            if (!empty($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario'])) {
                                echo('<td>');
                                echo('<label>Curso Superior:</label>');
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['cursoSuperiorFuncionario']) . "</h5>");
                                echo("</td>");
                            }
                        }

                        if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '6') {

                            echo('<td>
								<label>Curso Superior:</label>');
                            echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['cursoSuperiorFuncionario']) . "</h5>");
                            echo("</td>");

                            echo('<td>
								<label>Semestre:</label>');
                            echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['semestreFuncionario']) . "</h5>");
                            echo("</td>");

                            if (!empty($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario'])) {
                                echo('<td>
									<label>Curso T�cnico:</label>');
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario']) . "</h5>");
                                echo("</td>");
                            }

                            if (!empty($_SESSION ['dadosPessoais'] ['emCurso'])) {
                                echo('<td>
										<label>Situa��o: </label>
										<h5 style="font-family:verdana; margin-top: 0">CURSANDO ENSINO SUPERIOR</h5>');
                                echo("</td>");
                            }
                        }

                        if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '5') {
                            if (!empty($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario'])) {
                                echo('<td>
									<label>Curso T�cnico:</label>');
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario']) . "</h5>");
                                echo("</td>");
                            }
                        }


                        if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '4') {

                            if (!empty($_SESSION ['dadosPessoais'] ['emCurso'])) {
                                echo('<td>
									<label>Situa��o: </label>
									<h5 style="font-family:verdana; margin-top: 0">CURSANDO ENSINO M�DIO</h5>');
                                echo("</td>");
                            }

                        }


                        if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '2') {

                            if (!empty($_SESSION ['dadosPessoais'] ['emCurso'])) {
                                echo('<td>
									<label>Situa��o: </label>
									<h5 style="font-family:verdana; margin-top: 0">CURSANDO ENSINO FUNDAMENTAL</h5>');
                                echo("</td>");
                            }
                        }
                    }

                    ?>
                    </tr>
                    <tr>
                        <td>
                            <label>Cor dos Olhos:</label>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['corOlhosFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Cor do Cabelo:</label><br>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['corCabeloFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Cor da Pele:</label><br>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['corPeleFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <label>N�mero do Cal�ado:</label><br>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['numeroCalcadoFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Tamanho da Camisa:</label><br>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['tamanhoCamisaFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>

                        <td>
                            <label>Tamanho da Cal�a:</label><br>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['tamanhoCalcaFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>
                    </tbody>

                    </table>

                    <!-- DIVIS�O COM A DOCUMENTA��O -->

                    <div class="row" style="border-top: 1px solid black">
                        <div class="col-lg-12">
                            <h4>
                                <strong>Documenta��o</strong>
                            </h4>
                            <br>
                        </div>
                    </div>

                    <table class="table">
                    <tr>
                        <td>
                            <label>N�mero da Carteira Profissional:</label><br>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['carteiraProfissionalFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>N�mero de S�rie:</label><br>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['serieCarteiraProfissionalFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Data de Emiss�o:</label><br>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['emissaoCarteiraProfissionalFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>UF:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {

                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['ufCarteiraProfissionalFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>C�dula de Identidade:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['rgFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>

                        <td>
                            <label>Org�o Emissor:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['orgaoEmissorFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Data de Expedi��o:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['dataExpedicaoFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                        </td>


                    </tr>

                    <tr>
                        <td colspan="4"><br></td>
                    </tr>

                    <tr>
                        <td>
                            <label>N�mero do CPF:</label>
                            <?php
                            if (!empty ($_SESSION ['dadosPessoais'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['dadosPessoais'] ['cpfFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><br></td>
                    </tr>
                    <tr>
                        <td>
                            <label>N�mero da Reservista:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['reservistaFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>PIS:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['pisFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Banco:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['bancoPisFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Ag�ncia:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['agenciaPisFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>CNH:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['cnhFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Categoria:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['categoriaCnhFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Primeira Habilita��o:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['primeiraCnhFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Data de Vencimento:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['vencimentoCnhFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>T�tulo de Eleitor:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['tituloEleitorFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Zona:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['zonaEleitoralFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Se��o:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['secaoEleitoralFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Munic�pio / Cidade:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['municipioEleitoralFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>UF:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['ufEleitoralFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                        <td>
                            <label>Data de Emiss�o:</label>
                            <?php
                            if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['dataEmissaoEleitoralFuncionario']) . "</h5>");
                            } else {
                                echo("<br>N�O INFORMADO.");
                            }
                            ?>
                        </td>
                    </tr>

                    <tr>

                        <?php
                        if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                            if ($_SESSION ['documentacaoFuncionario'] ['possuiDocumentoClasse'] === 'true') {
                                echo('<td>
						<label>Documento de Classe:</label>');
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['documentoClasseFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }
                                echo('</td>');

                                echo('<td>
							<label>Conselho:</label>');
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['conselhoClasseFuncionario']) . "</h5>");
                                } else {
                                    echo("N�o Informado.");
                                }
                                echo('</td>');

                                echo('<td>
							<label>Conselho:</label>');
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['numeroClasseFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }

                                echo('</td>');

                                echo('<td>
							<label>Regi�o:</label>');
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['regiaoClasseFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }
                                echo('</td>');
                            }
                        }
                        ?>

                    </tr>
                    </table>




                    <div class="row" style="border-top: 1px solid black">
                        <div class="col-lg-12">
                            <h4>
                                <strong>Dados Banc�rios</strong>
                            </h4>
                        </div>
                    </div>


                    <table class="table">
                        <tr>
                            <td>
                                <label>Banco:</label>
                                <?php
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['bancoFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }
                                ?>
                            </td>
                            <td>
                                <label>N�mero da Ag�ncia:</label>
                                <?php
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['agenciaNumeroFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }
                                ?>
                            </td>
                            <td>
                                <label>N�mero da Conta Corrente:</label><br>
                                <?php
                                if (!empty ($_SESSION ['documentacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['documentacaoFuncionario'] ['ccNumeroFuncionario']) . "</h5>");
                                } else {
                                    echo("N�O INFORMADO.");
                                }
                                ?>
                            </td>
                            <td>
                            </td>
                        </tr>


                    </table>

                    <div class="row" style="border-top: 1px solid black">
                        <div class="col-lg-12">
                            <h4>
                                <strong>Filia��o / Dependentes / Benefici�rios</strong>
                            </h4>
                        </div>
                    </div>

                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <label>Nome do Pai:</label>
                                <?php
                                if (!empty ($_SESSION ['filiacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['nomePaiCompleto']) . "</h5>");
                                } else {
                                    echo("N�O INFORMADO.");
                                }
                                ?>
                            </td>
                            <td>
                                <label>Data de Nascimento:</label>
                                <?php
                                if (!empty ($_SESSION ['filiacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoPaiFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }
                                ?>
                            </td>
                            <td>
                                <label>IRRF:</label>
                                <?php
                                if (!empty ($_SESSION ['filiacaoFuncionario'])) {

                                    switch ($_SESSION ['filiacaoFuncionario'] ['irrfPaiFuncionario']) {
                                        default :
                                            echo("N/A");
                                            break;

                                        case "s" :
                                            echo("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
                                            break;

                                        case "n" :
                                            echo("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
                                            break;
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label>Nome da M�e:</label>
                                <?php
                                if (!empty ($_SESSION ['filiacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['nomeMaeCompleto']) . "</h5>");
                                } else {
                                    echo("N�O INFORMADO.");
                                }
                                ?>
                            </td>

                            <td>
                                <label>Data de Nascimento:</label>
                                <?php
                                if (!empty ($_SESSION ['filiacaoFuncionario'])) {
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoMaeFuncionario']) . "</h5>");
                                } else {
                                    echo("<br>N�O INFORMADO.");
                                }
                                ?>
                            </td>
                            <td>
                                <label>IRRF:</label>
                                <?php
                                if (!empty ($_SESSION ['filiacaoFuncionario'])) {
                                    switch ($_SESSION ['filiacaoFuncionario'] ['irrfMaeFuncionario']) {
                                        default :
                                            echo("<br>N/A");
                                            break;

                                        case "s" :
                                            echo("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
                                            break;

                                        case "n" :
                                            echo("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
                                            break;
                                    }
                                }
                                ?>
                            </td>
                        </tr>

                    </table>

                    <?php

                    for ($contador = 0; $contador < 16; $contador++) {

                        if ((!empty($_SESSION['filiacaoFuncionario']['nomeBeneficiarioFuncionario' . $contador]))) {
                            echo("
				<tr>				
					<td>
						<label>Nome do Benefici�rio " . $contador . ":</label>
						<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['nomeBeneficiarioFuncionario' . $contador]) . "</h5>
					</td>
					<td>
						<label>Data de Nascimento:</label>
						<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoBeneficiarioFuncionario' . $contador]) . "</h5>
					</td>
				</tr>

				<tr>
					<td>
						<label>Sexo:</label>
						<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['sexoBeneficiarioFuncionario' . $contador]) . "</h5>
					</td>
					<td>
						<label>Parentesco:</label>
					<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($_SESSION ['filiacaoFuncionario'] ['parentescoBeneficiarioFuncionario' . $contador]) . "</h5>
					</td>
					<td>
						<label>IRRF:</label>");
                            switch ($_SESSION ['filiacaoFuncionario'] ['irrfBeneficiarioFuncionario' . $contador]) {
                                default :
                                    echo("N/A");
                                    break;

                                case "s" :
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
                                    break;

                                case "n" :
                                    echo("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
                                    break;
                            }
                            echo("
					</td>
				</tr>");

                        }
                    }

                    ?>

                    <div class="row" style="border-top: 1px solid black">
                        <div class="col-lg-12">
                            <h4>
                                <strong>Dados Empresariais</strong>
                            </h4>
                        </div>
                    </div>

                    <table class="table table-bordered">
                    <tr>
                    <td>
                    <label>Cargo:</label>
                    <?php
                    if (!empty ($_SESSION ['empresaFuncionario'])) {
                        $codigoCargo = $this->medoo->select("cargos_funcionarios", "descricao", ["cod_cargos" => $_SESSION ['empresaFuncionario'] ['cargoFuncionario']]);
                        echo("<h5 style='font-family:verdana; margin-top: 0'>" . mb_strtoupper($codigoCargo[0]) . "</h5>");
                        unset($codigoCargo);
                    }


			?>
		</td>
		<td>
			<label>Centro de Custo:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					$codigoCentroCusto = $this->medoo->select("centro_resultado", "descricao", ["cod_centro_resultado" => $_SESSION ['empresaFuncionario'] ['centroCustoFuncionario']]);
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($codigoCentroCusto[0]) ."</h5>");
					unset($codigoCentroCusto[0]);
				}
			?>
		</td>
		<td>
			<label>Data de Admiss�o:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['dataAdmissaoFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
		<td>
			<label>Sal�rio:</label><br>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['salarioFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
	</tr>
	<tr>
		<td>
			<label>Categoria:</label><br>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
				switch ($_SESSION ['empresaFuncionario'] ['categoriaSalarioFuncionario']) {
						default :
							echo ("N/A");
							break;
						
						case "m" :
							echo ("<h5 style='font-family:verdana; margin-top: 0'>Mensalista</h5>");
							break;
						
						case "b" :
							echo ("<h5 style='font-family:verdana; margin-top: 0'>Bolsista</h5>");
							break;
					}
				} else {
					echo ("N�O INFORMADO.");
				}
			?>
		</td>
		<td>
			<label>Data de Admiss�o:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['dataAdmissaoFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
		<td>
			<label>Periculosidade:</label>
			<?php
				if(! empty($_SESSION ['empresaFuncionario'] ['periculosidadeFuncionario'])){
					echo('
						<h5 style="font-family:verdana; margin-top: 0">30%</h5>'
						);
				}
				
			?>
		</td>
		<td>
			<?php
				if(! empty($_SESSION ['empresaFuncionario'] ['insalubridadeFuncionario'])){
                    echo('<label>Insalubridade:</label>');
					echo('<h5 style="font-family:verdana; margin-top: 0">30%</h5>');
				}
			?>
		</td>
	</tr>
	<tr>
		<td>
			<label>A.S.O (Atestado de Sa�de Ocupacional):</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['dataAtestadoSaudeOperacionalFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
		<td>
			<label>Sindicato:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['sindicatoFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
		<td>
			<label>Situa��o Sindical:</label>
			<?php 
				if(! empty ( $_SESSION ['empresaFuncionario'] )) {
					switch ($_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario']) {
						case '1':
							echo ("<h5 style='font-family:verdana; margin-top: 0'>J� PAGOU</h5>");
						break;
						case '2':
							echo ("<h5 style='font-family:verdana; margin-top: 0'>A DESCONTAR</h5>");
						break;
						case '3':
							echo ("<h5 style='font-family:verdana; margin-top: 0'>PROFISSIONAL LIBERAL</h5>");
						break;
						
						default:
							echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O INFORMADO</h5>");
						break;
					}
				}
			?>
		</td>
		<td>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario']['registroSindicatoFuncionario'] )) {
                    echo('<label>Registro:</label>');
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['registroSindicatoFuncionario']) ."</h5>");
				}
			?>
		</td>
	</tr>
	
	<tr>
		<td>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario']['chapaSindicatoFuncionario'] )) {
                    echo('<label>Chapa:</label>');
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['chapaSindicatoFuncionario']) ."</h5>");
				}
			?>
		</td>
		<td>
			
		</td>
		<td>
			<label>Unidade:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					$codigoUnidade = $this->medoo->select("unidade", "nome_unidade", ["cod_unidade" => $_SESSION ['empresaFuncionario'] ['unidadeFuncionario']]);
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($codigoUnidade[0]) ."</h5>");
					unset($codigoUnidade);
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<label>E-mail Corporativo:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['emailFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
	</tr>
	<tr>
		<td>
			<label>Data de Demiss�o:</label>
			<?php
				if (! empty ( $_SESSION ['empresaFuncionario'] )) {
					echo ("<h5 style='font-family:verdana; margin-top: 0'>". mb_strtoupper($_SESSION ['empresaFuncionario'] ['dataDemissaoFuncionario']) ."</h5>");
				} else {
					echo ("<br>N�O INFORMADO.");
				}
			?>
		</td>
		
	</tr>
		
		
</table>