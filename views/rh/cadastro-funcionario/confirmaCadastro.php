

<!-- Iniciado T�tulo -->
<div class="page-header" style="margin-top: 0px;">
	<h1>Confirma��o de Dados Cadastrais</h1>
</div>
<!-- Criado ordem de preenchimento -->
<div class="row">

	<div class="col-lg-12">

        <ol class="breadcrumb">

            <li><a  href="<?php echo(HOME_URI); ?>cadastro/indexRefill">Dados Pessoais</a>

            <li><a href="<?php echo(HOME_URI); ?>cadastro/documentacaoFuncionarioRefill"> Documenta��o e Dados Banc�rios</a>

            <li><a href="<?php echo(HOME_URI); ?>cadastro/filiacaoFuncionarioRefill">Filia��o, Dependentes e Benefici�rios</a>

            <li><a href="<?php echo(HOME_URI); ?>cadastro/empresaFuncionarioRefill">Dados da Empresa</a>

            <li class="active">Confirma��o Cadastro

        </ol>

	</div>
</div>


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">

			<div class="panel-heading">
				<div class="row">
					<div class="col-lg-10">
						<h4>Confirma��o de Dados Cadastrais</h4>
					</div>

					<div class="col-lg-2">
						<a href="<?php echo HOME_URI?>cadastro/imprimirCadastro" target="_blank">
							<button class="btn btn-default btn-lg">
								<i class="fa fa-print fa-fw"></i>
							</button> </a>
					</div>
				</div>
			</div>

			<div class="panel-body">

				<!-- panel panel-default  -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-10">
								<h3>Ficha Cadastral Cons�rcio Metro Service</h3>
							</div>

						</div>
					</div>
					<!-- panel panel-default  -->

					<div class="panel-body">
					
						<div class="row">
							<div class="col-lg-12">
								<h4>
									<strong>Dados Pessoais</strong>
								</h4>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-8">
								<label>Nome Completo:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper( $_SESSION ['dadosPessoais'] ['nomeCompleto'])."</h5>" );
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<?php
									$fotoID = $this->medoo->select('fotostemp', "nome",["ORDER" => "cod_fotostemp"]);
									$contadorFoto  	= count($fotoID);
									if( $contadorFoto > 0 )
									{
										$lastIDFuncionario 	= $fotoID[$contadorFoto-1];
									}

									$id = $this->pgLastInsertId("fotostemp_cod_fotostemp_seq");
									$nomeImagem = $this->medoo->select ("fotostemp", "nome", ['cod_fotostemp'=>$id]);
								echo ('<img src="'. HOME_URI .'/views/_images/rh/' . $lastIDFuncionario . '" alt="" width="177px" height="236px">');

								?>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">
								<label>Idade:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										$dataAtual = DateTime::createFromFormat ( 'd-m-Y', date ( 'd-m-Y' ) );
										$dataNascimento = DateTime::createFromFormat ( 'd-m-Y', $_SESSION ['dadosPessoais'] ['dataNascimentoFuncionario'] );
										$idade = $dataAtual->diff ( $dataNascimento );
										echo ("<h5 style='font-family:verdana; margin-top: 0'>".$idade->format ( "%y" )." ANOS </h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								
								?>
							</div>
							<div class="col-lg-2">
								<label>Sexo:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										switch ($_SESSION ['dadosPessoais'] ['sexoFuncionario']) {

											case "m" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>MASCULINO</h5>");
												break;
											
											case "f" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>FEMININO</h5>");
												break;

                                            default :
                                                echo ("N/E");
                                                break;
										}
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								
								?>
							</div>
							<div class="col-lg-2">
								<label>Data Nascimento:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>".$_SESSION ['dadosPessoais'] ['dataNascimentoFuncionario']."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Estado Civil:</label>

								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". $_SESSION['dadosPessoais']['estadoCivilFuncionario'] ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Local de Nascimento:</label><br>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['localNascimentoFuncionario']) ."</h5>");
									}
								?>
							</div>
							<div class="col-lg-2">

                                <label>Estrangeiro:</label>
                                <?php
                                if (! empty ( $_SESSION ['dadosPessoais'] )) {
                                    switch ($_SESSION ['dadosPessoais'] ['estrangeiro']) {
                                        default :
                                            echo ("<br>N/E");
                                            break;

                                        case "true" :
                                            echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
                                            break;

                                        case "false" :
                                            echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
                                            break;
                                    }
                                } else {
                                    echo ("<br>N�O INFORMADO.");
                                }
                                ?>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">
								<label>Naturalizado:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										switch ($_SESSION ['dadosPessoais'] ['naturalidade']) {
											default :
												echo ("<br>N/E");
												break;
											
											case "true" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
												break;
											
											case "false" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
												break;
										}
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">

								<?php
								if (! empty ( $_SESSION ['dadosPessoais']['tipoVistoFuncionario'] )) {
                                    echo ('<label>Tipo de Visto:</label>');
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['tipoVistoFuncionario']) ."</h5>");
								}
								?>
							</div>
							<div class="col-lg-2">
								<?php
									if (! empty ( $_SESSION ['dadosPessoais']['dataChegadaBrasilFuncionario'] )) {
                                            echo ("<label>Data de Chegada ao Brasil:</label><br>");
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['dataChegadaBrasilFuncionario']) ."</h5>");
									}
								?>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-4">
								<label>Logradouro:</label> 
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['logradouroFuncionario']) .",  ".strtoupper($_SESSION ['dadosPessoais'] ['numeroEnderecoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?> 
							</div>
							
							<div class="col-lg-2">
								<label>Complemento:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									if ($_SESSION ['dadosPessoais'] ['complementoFuncionario']) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['complementoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N/E");
									}
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
						</div>
						<div class="row">

							<div class="col-lg-2">
								<label>Bairro:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['bairroFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							
							<div class="col-lg-2">
								<label>Cidade / Munic�pio:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['municipioFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
								
							</div>

							<div class="col-lg-2">
								<label>UF:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['estadoFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							
							<div class="col-lg-2">
								<label>CEP:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['cepFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>

						</div>
						<br>
						<div class="row">

							<div class="col-lg-2">
								<label>Telefone Residencial:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['residencialFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							
							<div class="col-lg-2">
								<label>Telefone Celular:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['celularFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-3">
								<label>Nome do Contato:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['nomeContatoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Telefone do Contato:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['contatoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
								
							</div>
						</div>
					</div>

					<div class="panel-body">
						<div class="row">
							<div class="col-lg-2">
								<label>Escolaridade:</label>
								<?php
									if (! empty ( $_SESSION ['dadosPessoais'] )) {
										$codigoEscolaridade = $this->medoo->select("tipo_escolaridade", "descricao", [ "cod_tipo_escolaridade" => (int) $_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] ] );
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($codigoEscolaridade[0]) ."</h5>");
										unset($codigoEscolaridade);
									}
								?>
							</div>
							<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '7') 						# Se tem curso superior completo
									{ 				
										echo('<div class="col-lg-2">
												<label>Curso Superior:</label>');
										echo("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario']) ."</h5>");
										echo("</div>");
										
										if (! empty($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario'])) {
											echo('<div class="col-lg-2">
												<label>Curso Superior:</label>');
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario']) ."</h5>");
											echo("</div>");
										}
									}
									
									if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '6') 
									{
										
										echo('<div class="col-lg-2">
												<label>Curso Superior:</label>');
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['cursoSuperiorFuncionario']) ."</h5>");
										echo("</div>");
	
										echo('<div class="col-lg-2">
												<label>Semestre:</label>');
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['semestreFuncionario']) ."</h5>");
										echo("</div>");
											
											if (! empty($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario'])) {
												echo('<div class="col-lg-2">
													<label>Curso T�cnico:</label>');
												echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario']) ."</h5>");
												echo("</div>");
											}
											
											if(! empty($_SESSION ['dadosPessoais'] ['emCurso'])){
												echo('<div class="col-lg-3">
														<label>Situa��o: </label>
													<h5 style="font-family:verdana; margin-top: 0">CURSANDO ENSINO SUPERIOR</h5>');
												echo("</div>");
											}
									}
									
									if ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '5')
									{
										if (! empty($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario'])) {
											echo('<div class="col-lg-2">
													<label>Curso T�cnico:</label>');
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['nomeCursoTecnicoFuncionario']) ."</h5>");
											echo("</div>");
										}
									}
									
									
									if($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '4' )
									{
										
										if(! empty($_SESSION ['dadosPessoais'] ['emCurso'])){
											echo('<div class="col-lg-3">
													<label>Situa��o: </label>
													<h5 style="font-family:verdana; margin-top: 0">CURSANDO ENSINO M�DIO</h5>');
											echo("</div>");
										}
											
									}
									
									
									if($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'] === '2' )
									{
											
										if(! empty($_SESSION ['dadosPessoais'] ['emCurso'])){
											echo('<div class="col-lg-3">
													<label>Situa��o: </label>
													<h5 style="font-family:verdana; margin-top: 0">CURSANDO ENSINO FUNDAMENTAL</h5>');
											echo("</div>");
										}
									}
								} 
							?>
					</div>
				</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-2">
								<label>Cor dos Olhos:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['corOlhosFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>Cor do Cabelo:</label><br>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['corCabeloFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>Cor da Pele:</label><br>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['corPeleFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>N�mero da Cal�a:</label><br>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['tamanhoCalcaFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>N�mero do Cal�ado:</label><br>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['numeroCalcadoFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>Tamanho da Camisa:</label><br>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['tamanhoCamisaFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
						</div>
					</div>

					
					<!-- DIVIS�O COM A DOCUMENTA��O -->
					
					<div class="panel-body">
						<div class="row" style="border-top: 1px solid black">
							<div class="col-lg-12">
								<h4>
									<strong>Documenta��o</strong>
								</h4>
								<br>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-3">
								<label>N�mero da Carteira Profissional:</label><br>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['carteiraProfissionalFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>N�mero de S�rie:</label><br> 
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['serieCarteiraProfissionalFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Emiss�o:</label><br>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['emissaoCarteiraProfissionalFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>UF:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['ufCarteiraProfissionalFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
                            <div class="col-lg-3">
                                <label>PIS:</label>
                                <?php
                                if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
                                    echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['pisFuncionario']) ."</h5>");
                                } else {
                                    echo ("<br>N�O INFORMADO.");
                                }
                                ?>
                            </div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-3">
								<label>C�dula de Identidade:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['rgFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2"> 
								<label>Org�o Emissor:</label> 
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['orgaoEmissorFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Expedi��o:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['dataExpedicaoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>N�mero do CPF:</label>
								<?php
								if (! empty ( $_SESSION ['dadosPessoais'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['dadosPessoais'] ['cpfFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-3">
								<label>N�mero da Reservista:</label>
								<?php
								if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['reservistaFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
						</div>
					</div>
					<br>

					<div class="panel-body">
						<div class="row">
							<div class="col-lg-3">
								<label>N�mero da Carteira Nacional de Habilita��o (CNH):</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['cnhFuncionario']) ."</h5>");
									} else {
											echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Categoria:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['categoriaCnhFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Primeira Habilita��o:</label>
								<?php
								if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['primeiraCnhFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Vencimento:</label>
								<?php
								if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['vencimentoCnhFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-2">
								<label>T�tulo de Eleitor:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['tituloEleitorFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Zona:</label> 
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['zonaEleitoralFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Se��o:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['secaoEleitoralFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Munic�pio / Cidade:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['municipioEleitoralFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>UF:</label> 
								<?php
								if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['ufEleitoralFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Emiss�o:</label>
								<?php
								if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['dataEmissaoEleitoralFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>

						</div>
					</div>
					
					<?php
						if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
							if ( $_SESSION ['documentacaoFuncionario'] ['possuiDocumentoClasse'] === 'true' ) 
							{
								echo
								('<div class="panel-body">
									<div class="row">
										<div class="col-lg-2">
											<label>Documento de Classe:</label>');
											if (! empty ( $_SESSION ['documentacaoFuncionario'] )) 
											{
												echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['documentoClasseFuncionario']) ."</h5>");
											} else {
												echo ("<br>N�O INFORMADO.");
											}
								echo 		('</div>');
								
								echo('<div class="row">
										<div class="col-lg-2">
											<label>Conselho:</label>');
										if (! empty ( $_SESSION ['documentacaoFuncionario'] )) 
										{
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['conselhoClasseFuncionario']) ."</h5>");
										} else {
											echo ("N�o Informado.");
										}
								echo 		('</div>');
								echo('<div class="row">
										<div class="col-lg-2">
											<label>Conselho:</label>');
										if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['numeroClasseFuncionario']) ."</h5>");
										} else {
											echo ("<br>N�O INFORMADO.");
										}
										
								echo 		('</div>');
	
								echo('<div class="row">
										<div class="col-lg-2">
											<label>Regi�o:</label>');
										if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
											echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['regiaoClasseFuncionario']) ."</h5>");
										} else {
											echo ("<br>N�O INFORMADO.");
										}
								echo ('
										</div>
											</div>
												</div>
													</div>
										');
								
							} 
						}
					?>
					<div class="panel-body">
						<div class="row" style="border-top: 1px solid black">
							<div class="col-lg-12">
								<h4>
									<strong>Dados Banc�rios</strong>
								</h4>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">
								<label>Banco:</label>
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['bancoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>N�mero da Ag�ncia:</label> 
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['agenciaNumeroFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-3">
								<label>N�mero da Conta Corrente:</label><br> 
								<?php
									if (! empty ( $_SESSION ['documentacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['documentacaoFuncionario'] ['ccNumeroFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
						</div>
					</div>

					<div class="panel-body">
						<div class="row" style="border-top: 1px solid black">
							<div class="col-lg-12">
								<h4>
									<strong>Filia��o / Dependentes / Benefici�rios</strong>
								</h4>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-4">
								<label>Nome do Pai:</label>
								<?php
									if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nomePaiCompleto']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Nascimento:</label>
								<?php
									if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoPaiFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-3">
								<label>IRRF:</label>
								<?php
									if (! empty ( $_SESSION ['filiacaoFuncionario'] )) 
									{
									switch ($_SESSION ['filiacaoFuncionario'] ['irrfPaiFuncionario']) {
											default :
												echo ("<br>N/E");
												break;
											
											case "s" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
												break;
											
											case "n" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
												break;
										}
									}
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<label>Nome da M�e:</label>
								<?php
									if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nomeMaeCompleto']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Nascimento:</label>
								<?php
									if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoMaeFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-3">
								<label>IRRF:</label>
								<?php
									if (! empty ( $_SESSION ['filiacaoFuncionario'] )) 
									{
									switch ($_SESSION ['filiacaoFuncionario'] ['irrfMaeFuncionario']) {
											default :
												echo ("<br>N/E");
												break;
											
											case "s" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
												break;
											
											case "n" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
												break;
										}
									}
								?>
							</div>
						</div>

                        <div class="row">
                            <div class="col-lg-4">
                                <label>Nome do C�njuge:</label>
                                <?php
                                if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
                                    echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nomeConjugeCompleto']) ."</h5>");
                                } else {
                                    echo ("<br>N�O INFORMADO.");
                                }
                                ?>
                            </div>
                            <div class="col-lg-2">
                                <label>Data de Nascimento:</label>
                                <?php
                                if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
                                    echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoConjugeFuncionario']) ."</h5>");
                                } else {
                                    echo ("<br>N�O INFORMADO.");
                                }
                                ?>
                            </div>
                            <div class="col-lg-2">
                                <label>IRRF:</label>
                                <?php
                                if (! empty ( $_SESSION ['filiacaoFuncionario'] ))
                                {
                                    switch ($_SESSION ['filiacaoFuncionario'] ['irrfConjugeFuncionario']) {
                                        default :
                                            echo ("<br>N/E");
                                            break;

                                        case "s" :
                                            echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
                                            break;

                                        case "n" :
                                            echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
                                            break;
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-lg-3">
                                <label>Sexo:</label>
                                <?php
                                if (! empty ( $_SESSION ['filiacaoFuncionario'] )) {
                                    if($_SESSION['filiacaoFuncionario']['sexoConjugeFuncionario'] == "m"){
                                        echo("<h5 style='font-family:verdana; margin-top: 0'>Masculino</h5>");
                                    }else{
                                        if($_SESSION['filiacaoFuncionario']['sexoConjugeFuncionario'] == "f"){
                                            echo("<h5 style='font-family:verdana; margin-top: 0'>Feminino</h5>");
                                        }else{
                                            echo ("<br>N�O INFORMADO.");
                                        }
                                    }
                                } else {
                                    echo ("<br>N�O INFORMADO.");
                                }
                                ?>
                            </div>
                        </div>
					
						
					<?php 
					
						for ($contador = 0; $contador < 16; $contador++) 
						{
							
							if ( (! empty($_SESSION['filiacaoFuncionario']['nomeBeneficiarioFuncionario' . $contador ] ) ) )
							{
								echo (
									"<div class='row'>
										<div class='col-lg-4'>
											<label>Nome do Benefici�rio ". $contador .":</label>
											<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nomeBeneficiarioFuncionario' . $contador]) ."</h5>
										</div>
										<div class='col-lg-2'>
											<label>Data de Nascimento:</label>
											<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['nascimentoBeneficiarioFuncionario' . $contador]) ."</h5>
										</div>
										<div class='col-lg-2'>
											<label>Sexo:</label>
											<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['sexoBeneficiarioFuncionario' . $contador]) ."</h5>
										</div>
										<div class='col-lg-2'>
											<label>Parentesco:</label>
										<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['filiacaoFuncionario'] ['parentescoBeneficiarioFuncionario' . $contador]) ."</h5>
										</div>
										<div class='col-lg-1'>
											<label>IRRF:</label>");
										switch ($_SESSION ['filiacaoFuncionario'] ['irrfBeneficiarioFuncionario' . $contador]) {
											default :
												echo ("<br>N/E");
												break;
													
											case "s" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>SIM</h5>");
												break;
													
											case "n" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O</h5>");
												break;
										}
										echo ("</div>");
										

									echo("</div>");
							}
						}
					?>
					</div>

					<div class="panel-body">
						<div class="row" style="border-top: 1px solid black">
							<div class="col-lg-12">
								<h4>
									<strong>Dados Empresariais</strong>
								</h4>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">
								<label>Cargo:</label> 
								<?php
									if(! empty ( $_SESSION ['empresaFuncionario'] )) {
										$codigoCargo = $this->medoo->select("cargos_funcionarios", "descricao", ["cod_cargos" => $_SESSION ['empresaFuncionario'] ['cargoFuncionario']]);
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($codigoCargo[0]) ."</h5>");
									unset($codigoCargo);
									}
								?>
							</div>
							
							<div class="col-lg-2">
								<label>Centro de Custo:</label>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										$codigoCentroCusto = $this->medoo->select("centro_resultado", "descricao", ["cod_centro_resultado" => $_SESSION ['empresaFuncionario'] ['centroCustoFuncionario']]);
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($codigoCentroCusto[0]) ."</h5>");
										unset($codigoCentroCusto[0]);
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Admiss�o:</label>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['dataAdmissaoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Sal�rio:</label><br>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['salarioFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Categoria Salario:</label><br>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
									switch ($_SESSION ['empresaFuncionario'] ['categoriaSalarioFuncionario']) {
											default :
												echo ("<br>N/E");
												break;
											
											case "m" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>Mensalista</h5>");
												break;
											
											case "b" :
												echo ("<h5 style='font-family:verdana; margin-top: 0'>Bolsista</h5>");
												break;
										}
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">
								<label>Periculosidade:</label>
								<?php
									if(! empty($_SESSION ['empresaFuncionario'] ['periculosidadeFuncionario'])){
										echo('
											<h5 style="font-family:verdana; margin-top: 0">30%</h5>'
											);
									}
									
								?>
							</div>
							<div class="col-lg-2">
								<label>Insalubridade:</label>
								<?php
									if(! empty($_SESSION ['empresaFuncionario'] ['insalubridadeFuncionario'])){
										echo('
											<h5 style="font-family:verdana; margin-top: 0">20%</h5>'
											);
									}
									
								?>
							</div>
							<div class="col-lg-4">
								<label>A.S.O (Atestado de Sa�de Ocupacional):</label>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['dataAtestadoSaudeOperacionalFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2">
								<label>Sindicato:</label>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['sindicatoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Situa��o Sindical:</label>
								<?php 
									if(! empty ( $_SESSION ['empresaFuncionario'] )) {
										switch ($_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario']) {
											case '1':
												echo ("<h5 style='font-family:verdana; margin-top: 0'>J� PAGOU</h5>");
											break;
											case '2':
												echo ("<h5 style='font-family:verdana; margin-top: 0'>A DESCONTAR</h5>");
											break;
											case '3':
												echo ("<h5 style='font-family:verdana; margin-top: 0'>PROFISSIONAL LIBERAL</h5>");
											break;
											
											default:
												echo ("<h5 style='font-family:verdana; margin-top: 0'>N�O INFORMADO</h5>");
											break;
										}
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Registro:</label>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['registroSindicatoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Chapa:</label>
								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['chapaSindicatoFuncionario']) ."</h5>");
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
							<div class="col-lg-2">
								<label>Unidade:</label>

								<?php
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										$codigoUnidade = $this->medoo->select("unidade", "nome_unidade", ["cod_unidade" => $_SESSION ['empresaFuncionario'] ['unidadeFuncionario']]);
										echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($codigoUnidade[0]) ."</h5>");
										unset($codigoUnidade);
									} else {
										echo ("<br>N�O INFORMADO.");
									}
								?>
							</div>
						</div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label>E-mail Corporativo:</label>
                                <?php
                                if (! empty ( $_SESSION ['empresaFuncionario'] )) {
                                    echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['emailFuncionario']) ."</h5>");
                                } else {
                                    echo ("<br>N�O INFORMADO.");
                                }
                                ?>
                            </div>
                        </div>
						<div class="row">
							<div class="col-lg-2" style="display: none;" >
								<label>Data de Demiss�o:</label>
								<?php
								if (! empty ( $_SESSION ['empresaFuncionario'] )) {
									echo ("<h5 style='font-family:verdana; margin-top: 0'>". strtoupper($_SESSION ['empresaFuncionario'] ['dataDemissaoFuncionario']) ."</h5>");
								} else {
									echo ("<br>N�O INFORMADO.");
								}
								?>
							</div>
						</div>

					</div>

				</div>

				<div class="row">
					<div class="col-lg-2">
						<a href="<?php echo(HOME_URI); ?>cadastro/inserirFuncionario"><button type="button" class="btn btn-warning btn-lg">Cadastrar Funcion�rio</button></a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>