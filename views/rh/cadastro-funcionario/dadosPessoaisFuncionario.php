<!-- Iniciado T�tulo -->
<div class="page-header">
    <h1>Cadastro de Funcion�rio</h1>
</div>

<!-- Criado ordem de preenchimento -->
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a>Dados Pessoais</a></li>

            <li role="presentation"<?php
                if(!empty ($_SESSION['dadosPessoais'])){
                    echo('><a href="'. HOME_URI . 'cadastro/documentacaoFuncionarioRefill">Documenta��o e Dados Banc�rios</a>');
                }else{
                    echo (' style="background-color: #cacaca"><a>Documenta��o e Dados Banc�rios</a>');
                }
                ?></li>


            <li role="presentation"<?php
                if(!empty ($_SESSION['documentacaoFuncionario'])){
                    echo('><a href="'.HOME_URI. 'cadastro/filiacaoFuncionarioRefill">Filia��o, Dependentes e Benefici�rios</a>');
                }else{
                    echo (' style="background-color: #cacaca"><a>Filia��o, Dependentes e Benefici�rios</a>');
                }
                ?></li>

            <li role="presentation"<?php
                if(!empty ($_SESSION['filiacaoFuncionario'])){
                    echo('><a href="' .HOME_URI. 'cadastro/empresaFuncionarioRefill">Dados da Empresa</a>');
                }else{
                    echo (' style="background-color: #cacaca"><a>Dados da Empresa</a>');
                }
                ?></li>

            <li role="presentation"<?php
                if(!empty ($_SESSION['empresaFuncionario'])){
                    echo('><a href="' .HOME_URI. 'cadastro/confirmaCadastroRefill">Confirma do Cadastro</a>');
                }else{
                    echo (' style="background-color: #cacaca"><a>Confirma��o do Cadastro</a>');
                }
                ?>
            </li>


        </ul>
    </div>
</div>

<div id="dadosPessoais" class="row">
    <div class="col-lg-12 ">
        <div class="panel panel-primary">
            <div class="panel-heading">Dados Pessoais</div>

            <div class="panel-body">

                <form id="formulario" class="form-group" method="post" role="form" action="<?php echo(HOME_URI); ?>cadastro/documentacaoFuncionario">
<!--                    has-error-->

                    <div class="row">

                        <div class="col-lg-6">

                            <label>Nome Completo</label>
                            <input class="form-control" type="text" name="nomeCompleto" required
                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) echo($_SESSION['dadosPessoais']['nomeCompleto']); ?>"
                                   data-validation-required-message=" Nome do funcion�rio � necess�rio"
                                   data-fv-row=".col-xs-4" data-fv-notempty="true" data-fv-notempty-message="Nome do funcion�rio � necess�rio"/>

                        </div>

                        <div class="col-lg-2">

                            <label>Sexo</label>
                            <select name="sexoFuncionario" class="form-control" required data-validation-required-message=" Campo precisa ser preenchido"
                                    data-bv-row=".col-lg-4" data-fv-notempty="true" data-fv-notempty-message="Selecione o sexo.">
                                <option selected value="" disabled="disabled">Informe o Sexo</option>

                                <option value="m" <?php echo ($_SESSION ['dadosPessoais'] ['sexoFuncionario'] == "m")? 'selected':''?>>Masculino</option>
                                <option value="f" <?php echo ($_SESSION ['dadosPessoais'] ['sexoFuncionario'] == "f")? 'selected':''?>>Feminino</option>
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <label>CPF:</label>
                            <input name="cpfFuncionario" type="text" class="form-control cpf" required data-bv-row=".col-lg-4"
                                   value="<?php if (! empty($_SESSION['dadosPessoais'])) {	echo ($_SESSION['dadosPessoais']['cpfFuncionario']);}?>"
                                   data-validation-required-message=" Campo precisa ser preenchido" data-fv-notempty="true" data-fv-notempty-message="CPF Obrigat�rio"/>
                        </div>

                        <div class="col-lg-2">
                            <label>Estrangeiro</label>

                            <div>
                                <label class="radio-inline">
                                    <input type="radio" name="estrangeiro"
                                        <?php echo ($_SESSION ['dadosPessoais'] ['estrangeiro'] == "true")?'checked="checked"':'' ?> value="true"/>Sim
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="estrangeiro"
                                        <?php echo ($_SESSION ['dadosPessoais'] ['estrangeiro'] == "false" || empty ($_SESSION ['dadosPessoais']))?'checked="checked"':'' ?> value="false"/>N�o
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <label>Data de Nascimento</label>
                            <input id="data" pattern=".{10,}" name="dataNascimentoFuncionario" type="text" placeholder="Somente n�meros" required
                                   class="form-control data validaData" value="<?php if(!empty($_SESSION['dadosPessoais'])){ echo($_SESSION['dadosPessoais']['dataNascimentoFuncionario']);}?>"
                                   data-validation-required-message=" Campo precisa ser preenchido" data-bv-row=".col-lg-4" data-fv-notempty="true" data-fv-notempty-message="Informe a data de nascimento."/>
                        </div>

                        <div class="col-lg-3">
                            <label>Local de Nascimento</label>
                            <input name="localNascimentoFuncionario" type="text" class="form-control" required
                                value="<?php if(!empty($_SESSION['dadosPessoais'])){ echo($_SESSION['dadosPessoais']['localNascimentoFuncionario']);} ?>"
                                data-validation-required-message=" Campo precisa ser preenchido" data-bv-row=".col-lg-4"
                                data-fv-notempty="true" data-fv-notempty-message="Informe o local de nascimento." />
                        </div>

                        <div class="col-lg-3">
                            <label>Estado Civil</label>
                            <select name="estadoCivilFuncionario" class="form-control" required data-validation-required-message=" Campo precisa ser preenchido"
                                    data-bv-row=".col-lg-4" data-fv-notempty="true" data-fv-notempty-message="Selecione o Campo.">
                                <option selected="selected" value="" disabled="disabled">INFORME O ESTADO CIVIL</option>
                            <?php
                            $codigoEstadoCivil = $this->medoo->select("estado_civil", "*");

                            foreach ($codigoEstadoCivil as $key => $value) {
                                if(!empty($_SESSION['dadosPessoais']) && $_SESSION['dadosPessoais']['estadoCivilFuncionario'] == $value['cod_estado_civil'])
                                    echo "<option value='{$value['cod_estado_civil']}' selected>{$value['descricao']}</option>";
                                else
                                    echo "<option value='{$value['cod_estado_civil']}' >{$value['descricao']}</option>";
                            }
                            ?>
                            </select>
                        </div>
                        <div class="col-lg-2" id="dadosEstrangeiro" style="display: none">
                            <label>Naturalizado</label>
                            <div>
                                <?php
                                if (!empty ($_SESSION ['dadosPessoais'])) {
                                    if ($_SESSION ['dadosPessoais'] ['naturalidade'] === "true") {
                                        echo('<label class="radio-inline"><input type="radio" name="naturalidade" checked="checked" value="true"/>Sim</label>
												  <label class="radio-inline"><input type="radio" name="naturalidade" value="false"/>N�o</label>');
                                    } else {
                                        echo('<label class="radio-inline"><input type="radio" name="naturalidade" value="true"/>Sim</label>
												  <label class="radio-inline"><input type="radio" name="naturalidade" checked="checked" value="false">N�o</label>');
                                    }
                                } else {
                                    echo('<label class="radio-inline"><input type="radio" name="naturalidade" value="true"/>Sim</label>
											  <label class="radio-inline"><input type="radio" name="naturalidade" checked="checked" value="false" />N�o</label>');
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="naturalizado" style="display: none">

                        <div class="col-lg-3">
                            <label for="dataNaturalizacaoFuncionario">Data de Naturaliza��o</label>
                            <input id="data" class="form-control data"
                                                      type="text" name="dataNaturalizacaoFuncionario"
                                                      value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                          echo($_SESSION['dadosPessoais']['dataNaturalizacaoFuncionario']);
                                                      } ?>"/>
                        </div>

                        <div class="col-lg-3">
                            <label>Nacionalidade</label> <input
                                name="nacionalidadeFuncionario" type="text" class="form-control"
                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                    echo($_SESSION['dadosPessoais']['nacionalidadeFuncionario']);
                                } ?>"
                                required data-validation-required-message=" Campo precisa ser preenchido"
                                data-bv-row=".col-lg-4"
                                data-fv-notempty="true"
                                data-fv-notempty-message="Informe o n�mero da carteira."/>
                        </div>

                    </div>

                    <div class="row" id="naoNaturalizado" style="display: none">
                        <div class="col-lg-4">
                            <label>Tipo de Visto</label>
                            <input name="tipoVistoFuncionario"
                                   type="text" class="form-control"
                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                       echo($_SESSION['dadosPessoais']['tipoVistoFuncionario']);
                                   } ?>"/>
                        </div>
                        <div class="col-lg-4">
                            <label>Data de Validade</label>
                            <input name="dataValidadeVistoFuncionario"
                                   type="text" class="form-control"
                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                       echo($_SESSION['dadosPessoais']['dataValidadeVistoFuncionario']);
                                   } ?>"/>
                        </div>
                        <div class="col-lg-4">
                            <label>N�mero Passaporte</label>
                            <input name="numeroPassaporteFuncionario"
                                   type="text" class="form-control"
                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                       echo($_SESSION['dadosPessoais']['numeroPassaporteFuncionario']);
                                   } ?>"/>
                        </div>
                    </div>

                    <div class="row"
                         style="border-bottom: 1px dashed black; padding-top: 2em;"></div>


                    <div class="row" style="padding-top: 2em;">

                        <div class="col-lg-3">
                            <label>CEP</label><input name="cepFuncionario" type="text"
                                                     class="form-control cep" placeholder="Digite somente os n�meros"
                                                     value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                         echo($_SESSION['dadosPessoais']['cepFuncionario']);
                                                     } ?>"
                                                     required
                                                     data-validation-required-message=" Campo precisa ser preenchido"
                                                     data-bv-row=".col-lg-4"
                                                     data-fv-notempty="true"
                                                     data-fv-notempty-message="Preencha o CEP."/>
                        </div>

                        <div class="col-lg-7">
                            <label>Endere�o</label> <input name="logradouroFuncionario"
                                                           type="text" id="logradouro"
                                                           placeholder="Informe somente o endere�o. EX: Av. Oliveira Paiva"
                                                           class="form-control"
                                                           value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                               echo($_SESSION['dadosPessoais']['logradouroFuncionario']);
                                                           } ?>"/>
                        </div>

                        <div class="col-lg-2">
                            <label>N�mero</label> <input id="numero"
                                                         name="numeroEnderecoFuncionario" type="text"
                                                         class="form-control"
                                                         value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                             echo($_SESSION['dadosPessoais']['numeroEnderecoFuncionario']);
                                                         } ?>"
                                                         required
                                                         data-validation-required-message=" Campo precisa ser preenchido"
                                                         data-bv-row=".col-lg-4"
                                                         data-fv-notempty="true"
                                                         data-fv-notempty-message="Informe o n�mero."/>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-4    ">
                            <label>Complemento</label> <input name="complementoFuncionario"
                                                              type="text" class="form-control"
                                                              value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                                  echo($_SESSION['dadosPessoais']['complementoFuncionario']);
                                                              } ?>"/>
                        </div>

                        <div class="col-lg-3">
                            <label>Bairro</label> <input name="bairroFuncionario" type="text"
                                                         id="bairro" class="form-control"
                                                         value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                             echo($_SESSION['dadosPessoais']['bairroFuncionario']);
                                                         } ?>"/>
                        </div>

                        <div class="col-lg-3">
                            <label>Cidade / Munic�pio</label> <input
                                name="municipioFuncionario" id="cidade" type="text"
                                class="form-control"
                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                    echo($_SESSION['dadosPessoais']['municipioFuncionario']);
                                } ?>"/>
                        </div>
                        <div class="col-lg-2">
                            <label>UF</label>
                            <input type="text" name="estadoFuncionario" id="uf" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">



                        <div class="col-lg-2">
                            <label>Telefone Residencial</label> <input
                                name="residencialFuncionario" type="text"
                                class="form-control telefone"
                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                    echo($_SESSION['dadosPessoais']['residencialFuncionario']);
                                } ?>"
                                required data-validation-required-message=" Campo precisa ser preenchido"
                                data-bv-row=".col-lg-4"
                                data-fv-notempty="true"
                                data-fv-notempty-message="Informe o telefone residencial."/>
                        </div>

                        <div class="col-lg-2">
                            <label>Telefone Celular</label> <input name="celularFuncionario"
                                                                   type="text" class="form-control telefone"
                                                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                                       echo($_SESSION['dadosPessoais']['celularFuncionario']);
                                                                   } ?>"/>
                        </div>

                        <div class="col-lg-2">
                            <label>Nome do Contato</label> <input
                                name="nomeContatoFuncionario" type="text" class="form-control"
                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                    echo($_SESSION['dadosPessoais']['nomeContatoFuncionario']);
                                } ?>"/>
                        </div>

                        <div class="col-lg-2">
                            <label>Telefone Contato</label> <input name="contatoFuncionario"
                                                                   type="text" class="form-control telefone"
                                                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                                       echo($_SESSION['dadosPessoais']['contatoFuncionario']);
                                                                   } ?>"/>
                        </div>

                    </div>

                    <div class="row"
                         style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

                    <div class="row" style="padding-top: 2em;">
                        <div class="col-lg-4" id="escolaridade">

                            <label>Escolaridade</label>

                            <?php
                            $escolaridadeArray = $this->medoo->select("tipo_escolaridade", "descricao");
                            $escolaridadeArray = array_combine(range(1, count($escolaridadeArray)), $escolaridadeArray);

                            if (!empty ($_SESSION ['dadosPessoais'])) {

                                if (empty ($_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'])) {

                                    echo('<select name="escolaridadeFuncionario" class="form-control"
												required data-validation-required-message=" Campo precisa ser preenchido" 
												data-bv-row=".col-lg-4"
               									data-fv-notempty="true"
               									data-fv-notempty-message="Selecione a escolaridade.">');
                                    echo('<option selected="selected" value="" disabled="disabled">INFORME A ESCOLARIDADE</option>');

                                    foreach ($escolaridadeArray as $key => $value) {
                                        echo "<option value=\"$key\" >$value</option>";
                                    }

                                    echo('</select>');

                                } else {

                                    echo('<select name="escolaridadeFuncionario" class="form-control"
												required data-validation-required-message=" Campo precisa ser preenchido" 
												data-bv-row=".col-lg-4"
               									data-fv-notempty="true"
               									data-fv-notempty-message="Selecione a escolaridade.">');
                                    $valor_selecionado = $_SESSION ['dadosPessoais'] ['escolaridadeFuncionario'];
                                    foreach ($escolaridadeArray as $key => $value) {
                                        $selected = ($valor_selecionado == $key) ? "selected=\"selected\"" : null;
                                        echo "<option value=\"$key\" $selected >$value</option>";

                                    }
                                    echo('</select>');
                                }

                            } else {

                                echo('<select name="escolaridadeFuncionario" class="form-control"
												required data-validation-required-message=" Campo precisa ser preenchido" 
												data-bv-row=".col-lg-4"
               									data-fv-notempty="true"
               									data-fv-notempty-message="Selecione a escolaridade.">');
                                echo('<option selected="selected" value="" disabled="disabled">INFORME A ESCOLARIDADE</option>');

                                foreach ($escolaridadeArray as $key => $value) {
                                    echo "<option value=\"$key\" >$value</option>";
                                }
                                echo('</select>');
                            }
                            ?>

                        </div>

                        <div class="col-lg-2">
                            <div id="cursando" class="checkbox"
                                 style="padding-top: 1.5em; display: none;">
                                <label>


                                    <?php
                                    if (!empty ($_SESSION ['dadosPessoais'])) {

                                        if (!empty ($_SESSION ['dadosPessoais'] ['emCurso']) && $_SESSION ['dadosPessoais'] ['emCurso'] === "on") {
                                            echo('<input type="checkbox" checked="checked" name="emCurso"/>Cursando');
                                        } else {
                                            echo('<input type="checkbox" name="emCurso"/>Cursando');
                                        }
                                    } else {
                                        echo('<input type="checkbox" name="emCurso"/>Cursando');
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>

                        <div class="col-lg-3" id="superiorFuncionario"
                             style="display: none">

                            <label>Informe o Curso Superior</label> <input
                                name="cursoSuperiorFuncionario" type="text" class="form-control"
                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                    echo($_SESSION['dadosPessoais']['cursoSuperiorFuncionario']);
                                } ?>"
                                required data-validation-required-message=" Campo precisa ser preenchido"
                                data-bv-row=".col-lg-4"
                                data-fv-notempty="true"
                                data-fv-notempty-message="Informe o curso superior."/>

                        </div>
                        <div id="semestre" class="col-lg-1" style="display: none">
                            <label>Semestre</label> <input name="semestreFuncionario"
                                                           type="text" class="form-control"
                                                           value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                               echo($_SESSION['dadosPessoais']['semestreFuncionario']);
                                                           } ?>"/>
                        </div>

                        <div class="col-lg-2" id="escolaTecnica" style="display: none">

                            <label for="cursoTecnico"> Curso T�cnico </label>

                            <div>
                                <?php
                                if (!empty ($_SESSION ['dadosPessoais'])) {
                                    if ($_SESSION ['dadosPessoais'] ['cursoTecnico'] === "s") {
                                        echo('<label class="radio-inline"> <input type="radio" name="cursoTecnico" id="cursoSim" checked="checked" value="s"/> Sim </label>
											   <label class="radio-inline"> <input type="radio" name="cursoTecnico" id="cursoNao" value="n"/> N�o </label>');
                                    } else {
                                        echo('<label class="radio-inline"> <input type="radio" name="cursoTecnico" id="cursoSim" value="s"> Sim </label>
											   <label class="radio-inline"> <input type="radio" name="cursoTecnico" id="cursoNao" checked="checked" value="n" /> N�o </label>');
                                    }
                                } else {
                                    echo('<label class="radio-inline"> <input type="radio" name="cursoTecnico" id="cursoSim" value="s"> Sim </label>
										   <label class="radio-inline"> <input type="radio" name="cursoTecnico" id="cursoNao" checked="checked" value="n" /> N�o </label>');
                                }
                                ?>

                            </div>

                        </div>

                        <div class="col-lg-3" id="curso" style="display: none">
                            <label>Informe o Curso</label> <input
                                name="nomeCursoTecnicoFuncionario" type="text"
                                class="form-control"
                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                    echo($_SESSION['dadosPessoais']['nomeCursoTecnicoFuncionario']);
                                } ?>"
                                required data-validation-required-message=" Campo precisa ser preenchido"
                                data-bv-row=".col-lg-4"
                                data-fv-notempty="true"
                                data-fv-notempty-message="Informe o curso."/>
                        </div>
                    </div>

                    <div class="row"
                         style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

                    <div class="row" style="padding-top: 2em;">
                        <div class="col-lg-3">
                            <label>Cor dos Olhos</label> <input name="corOlhosFuncionario"
                                                                type="text" class="form-control"
                                                                value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                                    echo($_SESSION['dadosPessoais']['corOlhosFuncionario']);
                                                                } ?>"
                                                                required
                                                                data-validation-required-message=" Campo precisa ser preenchido"
                                                                data-bv-row=".col-lg-4"
                                                                data-fv-notempty="true"
                                                                data-fv-notempty-message="Informe o cor dos olhos."/>
                        </div>
                        <div class="col-lg-3">
                            <label>Cabelo</label>
                            <input name="corCabeloFuncionario" type="text"
                                   class="form-control"
                                   value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                       echo($_SESSION['dadosPessoais']['corCabeloFuncionario']);
                                   } ?>"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Informe a cor do cabelo."/>
                        </div>
                        <div class="col-lg-3">
                            <label>Pele</label> <input name="corPeleFuncionario" type="text"
                                                       class="form-control"
                                                       value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                           echo($_SESSION['dadosPessoais']['corPeleFuncionario']);
                                                       } ?>"
                                                       required
                                                       data-validation-required-message=" Campo precisa ser preenchido"
                                                       data-bv-row=".col-lg-4"
                                                       data-fv-notempty="true"
                                                       data-fv-notempty-message="Informe a cor da pele."/>
                        </div>
                        <div class="col-lg-1">
                            <label>Cal�a</label> <input name="tamanhoCalcaFuncionario"
                                                        type="text" class="form-control" placeholder="N�mero"
                                                        value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                            echo($_SESSION['dadosPessoais']['tamanhoCalcaFuncionario']);
                                                        } ?>"
                                                        required
                                                        data-validation-required-message=" Campo precisa ser preenchido"
                                                        data-bv-row=".col-lg-4"
                                                        data-fv-notempty="true"
                                                        data-fv-notempty-message="Informe o tamanho da cal�a."/>
                        </div>
                        <div class="col-lg-1">
                            <label>Camisa</label> <input name="tamanhoCamisaFuncionario"
                                                         type="text" class="form-control" placeholder="N�mero"
                                                         value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                             echo($_SESSION['dadosPessoais']['tamanhoCamisaFuncionario']);
                                                         } ?>"
                                                         required
                                                         data-validation-required-message=" Campo precisa ser preenchido"
                                                         data-bv-row=".col-lg-4"
                                                         data-fv-notempty="true"
                                                         data-fv-notempty-message="Informe o tamanho da camisa."/>
                        </div>
                        <div class="col-lg-1">
                            <label>Cal�ado</label> <input name="numeroCalcadoFuncionario"
                                                          type="text" class="form-control" placeholder="N�mero"
                                                          value="<?php if (!empty($_SESSION['dadosPessoais'])) {
                                                              echo($_SESSION['dadosPessoais']['numeroCalcadoFuncionario']);
                                                          } ?>"
                                                          required
                                                          data-validation-required-message=" Campo precisa ser preenchido"
                                                          data-bv-row=".col-lg-4"
                                                          data-fv-notempty="true"
                                                          data-fv-notempty-message="Informe o tamanho do cal�ado."/>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 2em">
                        <div class="col-md-3">
                            <button id="btnSalvarContinuar" class="btn btn-success btn-lg btn-block" type="submit">Salvar
                                e Continuar
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>