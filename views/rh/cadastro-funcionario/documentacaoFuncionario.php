!-- Iniciado T�tulo -->
<div class="page-header" style="margin-top: 0px;">
	<h1>Cadastro de Funcion�rio</h1>
</div>


<!-- Criado ordem de preenchimento -->
<div class="row">
		<div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a  href="<?php echo(HOME_URI); ?>cadastro/indexRefill">Dados Pessoais</a>

                <li class="active">Documenta��o e Dados Banc�rios


                <li><?php
                    if(!empty ($_SESSION['documentacaoFuncionario'])){
                        echo('<a href="'.HOME_URI. 'cadastro/filiacaoFuncionarioRefill">'.
                            'Filia��o, Dependentes e Benefici�rios</a>');
                    }else{
                        echo ('Filia��o, Dependentes e Benefici�rios');
                    }

                    ?>

                <li><?php
                    if(!empty ($_SESSION['filiacaoFuncionario'])){
                        echo('<a href="' .HOME_URI. 'cadastro/empresaFuncionarioRefill">'.
                            'Dados da Empresa</a>');
                    }else{
                        echo ('Dados da Empresa');
                    }

                    ?>

                <li><?php
                    if(!empty ($_SESSION['empresaFuncionario'])){
                        echo('<a href="' .HOME_URI. 'cadastro/confirmaCadastroRefill">'.
                            'Confirma do Cadastro</a>');
                    }else{
                        echo ('Confirma��o do Cadastro');
                    }

                    ?>


            </ol>

		</div>
</div>


<div id="dadosPessoais" class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">2-Documenta��o e Dados Banc�rios</div>
			
			<div class="panel-body">

				<form id="formulario" class="form-group" method="post" role="form" action="<?php echo(HOME_URI); ?>cadastro/filiacaoFuncionario">

					<div class="row">
						<div class="col-lg-4">
							<label>Carteira Profissional</label> 
							<input class="form-control" type="text" name="carteiraProfissionalFuncionario" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['carteiraProfissionalFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o n�mero da carteira."/>
						</div>
						<div class="col-lg-2">
							<label>N�mero de S�rie</label> <input class="form-control" type="text" name="serieCarteiraProfissionalFuncionario"  value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['serieCarteiraProfissionalFuncionario']); }?>" 
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o n�mero de s�rie."/>
						</div>
						<div class="col-lg-2">
							<label>Data de Emiss�o</label> <input name="emissaoCarteiraProfissionalFuncionario" type="text"	class="form-control data"  value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['emissaoCarteiraProfissionalFuncionario']);	}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
							data-fv-notempty="true"
               				data-fv-notempty-message="Informe a data de emiss�o."/>
						</div>

						<div class="col-lg-1">
							<label>UF</label>
                            <?php
                                $consultaUf = $this->medoo->select("uf", "sigla_uf", ["ORDER" => "sigla_uf",]);

                                echo('<select name="ufCarteiraProfissionalFuncionario" class="form-control"
                                                    required data-validation-required-message=" Campo precisa ser preenchido"
                                                    data-bv-row=".col-lg-4"
                                                    data-fv-notempty="true"
                                                    data-fv-notempty-message="Informe a Unidade Federativa.">');

                                echo('<option selected="selected" value="" disabled="disabled">UF</option>');

                                if(!empty ($_SESSION['documentacaoFuncionario'])){
                                    foreach ($consultaUf as $key => $value) {
                                        if($_SESSION['documentacaoFuncionario']['ufCarteiraProfissionalFuncionario'] === $value){
                                            echo "<option selected='selected' value=\"$value\" >$value</option>";
                                        }else{
                                            echo "<option value=\"$value\" >$value</option>";
                                        }

                                    }
                                }else{
                                    foreach ($consultaUf as $key => $value) {
                                        echo "<option value=\"$value\" >$value</option>";
                                    }
                                }

                                echo('</select>');
                            ?>
						</div>
						<div class="col-lg-2">
							<label>PIS</label> <input name="pisFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['pisFuncionario']); } ?>" />
						</div>
					</div>

					<div class="row">
						<div class="col-lg-3">
							<label>C�dula de Identidade</label> <input name="rgFuncionario" type="text" placeholder="Somente n�meros" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {echo ($_SESSION['documentacaoFuncionario']['rgFuncionario']);	}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o RG.">
						</div>
						<div class="col-lg-1">
							<label>UF</label>
                            <?php
                            $consultaUf = $this->medoo->select("uf", "sigla_uf", ["ORDER" => "sigla_uf",]);

                            echo('<select name="ufRGFuncionario" class="form-control"
                                                    required data-validation-required-message=" Campo precisa ser preenchido"
                                                    data-bv-row=".col-lg-4"
                                                    data-fv-notempty="true"
                                                    data-fv-notempty-message="Selecione a Unidade Federativa.">');

                            echo('<option selected="selected" value="" disabled="disabled">UF</option>');

                            if(!empty ($_SESSION['documentacaoFuncionario'])){
                                foreach ($consultaUf as $key => $value) {
                                    if($_SESSION['documentacaoFuncionario']['ufRGFuncionario'] === $value){
                                        echo "<option selected='selected' value=\"$value\" >$value</option>";
                                    }else{
                                        echo "<option value=\"$value\" >$value</option>";
                                    }

                                }
                            }else{
                                foreach ($consultaUf as $key => $value) {
                                    echo "<option value=\"$value\" >$value</option>";
                                }
                            }

                            echo('</select>');
                            ?>
						</div>
						<div class="col-lg-2">
							<label>�rg�o Emissor</label> <input name="orgaoEmissorFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {echo ($_SESSION['documentacaoFuncionario']['orgaoEmissorFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido"
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o org�o emissor."/>
						</div>
						<div class="col-lg-2">
							<label>Data de Expedi��o</label> <input name="dataExpedicaoFuncionario" type="text" class="form-control data" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {	echo ($_SESSION['documentacaoFuncionario']['dataExpedicaoFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe a data de expedi��o."/>
						</div>

						<div class="col-lg-3">
							<label>N� Reservista</label> <input name="reservistaFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {	echo ($_SESSION['documentacaoFuncionario']['reservistaFuncionario']);}?>"
							<?php if($_SESSION['dadosPessoais']['sexoFuncionario'] == "m"){
                                echo('required data-validation-required-message=" Campo precisa ser preenchido"
							        data-bv-row=".col-lg-4"
               				        data-fv-notempty="true"
               				        data-fv-notempty-message="Informe a data de nascimento."');
                            }
                            ?>/>
						</div>

					</div>
					<div class="row"style="border-bottom: 1px dashed black; padding-top: 2em;">	</div>

					<div class="row"style="border-bottom: 1px dashed black; padding-top: 2em;">	</div>

					<div class="row" style="padding-top: 2em;">
						<div class="col-lg-4">
							<label>Carteira Nacional de Habilita��o (CNH)</label> <input
								name="cnhFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {
										echo ($_SESSION['documentacaoFuncionario']['cnhFuncionario']);}?>">
						</div>
						<div class="col-lg-2">
							<label>Categoria</label> <input class="form-control" type="text"
								name="categoriaCnhFuncionario" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {
										echo ($_SESSION['documentacaoFuncionario']['categoriaCnhFuncionario']);}?>">
						</div>
						<div class="col-lg-3">
							<label>Data da Primeira Habilita��o</label> <input class="form-control data" type="text" name="primeiraCnhFuncionario" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['primeiraCnhFuncionario']);}?>">
						</div>
						<div class="col-lg-2">
							<label>Vencimento</label> <input class="form-control data" type="text"
								name="vencimentoCnhFuncionario" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {
										echo ($_SESSION['documentacaoFuncionario']['vencimentoCnhFuncionario']);}?>">
						</div>
					</div>

					<div class="row"
						style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

					<div class="row" style="padding-top: 2em;">
						<div class="col-lg-2">
							<label>T�tulo de Eleitor</label> <input	name="tituloEleitorFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {	echo ($_SESSION['documentacaoFuncionario']['tituloEleitorFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Campo obrigat�rio. Informe o T�tulo de Eleitor."/>
						</div>
						<div class="col-lg-1">
							<label>Zona</label> <input name="zonaEleitoralFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['zonaEleitoralFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Campo obrigat�rio. Informe a zona."/>
						</div>
						<div class="col-lg-1">
							<label>Se��o</label> <input name="secaoEleitoralFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {	echo ($_SESSION['documentacaoFuncionario']['secaoEleitoralFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Campo obrigat�rio. Informe a se��o."/>
						</div>
						<div class="col-lg-2">
							<label>Cidade / Munic�pio</label> 
							<input name="municipioEleitoralFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['municipioEleitoralFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Campo obrigat�rio. Informe a cidade."/>
						</div>
						<div class="col-lg-1">
							<label>UF</label>
                            <?php
                            $consultaUf = $this->medoo->select("uf", "sigla_uf", ["ORDER" => "sigla_uf",]);

                            echo('<select name="ufEleitoralFuncionario" class="form-control"
                                                    required data-validation-required-message=" Campo precisa ser preenchido"
                                                    data-bv-row=".col-lg-4"
                                                    data-fv-notempty="true"
                                                    data-fv-notempty-message="Selecione a Unidade Federativa.">');

                            echo('<option selected="selected" value="" disabled="disabled">UF</option>');

                            if(!empty ($_SESSION['documentacaoFuncionario'])){
                                foreach ($consultaUf as $key => $value) {
                                    if($_SESSION['documentacaoFuncionario']['ufEleitoralFuncionario'] === $value){
                                        echo "<option selected='selected' value=\"$value\" >$value</option>";
                                    }else{
                                        echo "<option value=\"$value\" >$value</option>";
                                    }

                                }
                            }else{
                                foreach ($consultaUf as $key => $value) {
                                    echo "<option value=\"$value\" >$value</option>";
                                }
                            }

                            echo('</select>');
                            ?>
                        </div>
						<div class="col-lg-2">
							<label>Data de Emiss�o</label> <input name="dataEmissaoEleitoralFuncionario" type="text" class="form-control data" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['dataEmissaoEleitoralFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe a data de emiss�o."/>
						</div>

					</div>

					<div class="row">
						<div class="col-lg-2">
							<label>Possui Documento de Classe?</label>
							<div>
								<?php 
									if (! empty($_SESSION['documentacaoFuncionario'])) { 
										if ($_SESSION['documentacaoFuncionario']['possuiDocumentoClasse'] === "true" ) {
											echo
												('<label class="radio-inline"><input type="radio" name="possuiDocumentoClasse" checked="checked" value="true"/>Sim</label>
												  <label class="radio-inline"><input type="radio" name="possuiDocumentoClasse" value="false"/>N�o</label>');
										} else {
											echo
											  	('<label class="radio-inline"><input type="radio" name="possuiDocumentoClasse" value="true"/>Sim</label>
												  <label class="radio-inline"><input type="radio" name="possuiDocumentoClasse" checked="checked" value="false">N�o</label>');
										}
										
									} else {
										echo
											('<label class="radio-inline"><input type="radio" name="possuiDocumentoClasse" value="true"/>Sim</label> 
											  <label class="radio-inline"><input type="radio" name="possuiDocumentoClasse" checked="checked" value="false" />N�o</label>');									
								}																		
									?>
							</div>
						</div>
					</div>


					<div class="row" id="classeFuncionario" style="display: none">
						<div class="col-lg-2">
							<label>Documento de Classe</label> <input name="documentoClasseFuncionario" type="text"	class="form-control" placeholder="CREA/OAB , etc." value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['documentoClasseFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o documento de classe."/>
						</div>
						<div class="col-lg-1">
							<label>Conselho</label> <input name="conselhoClasseFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['conselhoClasseFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe conselho.">
						</div>


						<div class="col-lg-3">
							<label>N�mero</label> <input name="numeroClasseFuncionario"	type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['numeroClasseFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe o n�mero de classe.">
						</div>
						<div class="col-lg-3">
							<label>Regi�o</label> <input name="regiaoClasseFuncionario"	type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['regiaoClasseFuncionario']);}?>"
							required data-validation-required-message=" Campo precisa ser preenchido" 
							data-bv-row=".col-lg-4"
               				data-fv-notempty="true"
               				data-fv-notempty-message="Informe a regi�o.">
						</div>
					</div>
					<div class="row"
						style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

					<div class="row" style="padding-top: 2em;">
						<div class="col-lg-2">
							<label>Banco</label><input name="bancoFuncionario" type="text" placeholder="Informe o nome do banco" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) {	echo ($_SESSION['documentacaoFuncionario']['bancoFuncionario']);}?>"/>

						</div>
						<div class="col-lg-2">
							<label>Ag�ncia n�</label><input name="agenciaNumeroFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['agenciaNumeroFuncionario']);}?>"/>
						</div>
						<div class="col-lg-2">
							<label>Conta Corrente n�</label><input name="ccNumeroFuncionario" type="text" class="form-control" value="<?php if (! empty($_SESSION['documentacaoFuncionario'])) { echo ($_SESSION['documentacaoFuncionario']['ccNumeroFuncionario']);}?>"/>
						</div>
					</div>

					<div class="row" style="padding-top: 2em">
						<div class="col-lg-2">
							<button class="btn btn-success btn-lg btn-block" type="submit">Salvar e Continuar</button>
						</div>
					</div>

				</form>
			</div>
			<!-- End Painel -->



		</div>
	</div>
</div>
