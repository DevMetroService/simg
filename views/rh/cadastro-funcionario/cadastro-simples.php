<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/05/2016
 * Time: 17:18
 */
$dadosFunc = $_SESSION['refillSimples'];
unset($_SESSION['refillSimples']);
?>

<div class="page-header">
    <h2>Cadastro de Funcionario</h2>
</div>

<div class="row">
    <?php
    if($dadosFunc)
        echo ('<form id="formFuncionario" method="post" action="'.HOME_URI.'/cadastro/alterarCadastroSimples" class="form-group">');
    else
        echo ('<form id="formFuncionario" method="post" action="'.HOME_URI.'/cadastro/cadastroSimples" class="form-group">');
    ?>
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><label>Cadastro de Funcionario</label></div>

                        <div class="panel-body">
                            <div class="row">

                                <div class="col-md-5 col-lg-2" >
                                    <label>Tipo Funcion�rio</label>
                                    <select name="tipoFuncionario" class="form-control" >
                                        <?php
                                        $tipo_funcionario = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);

                                        foreach ($tipo_funcionario as $dados => $value) {
                                            if (!empty($saf) && $saf['codTipoFuncionario'] == $value["cod_tipo_funcionario"]) {
                                                echo("<option value='{$value["cod_tipo_funcionario"]}' selected >{$value["descricao"]}</option>");
                                            } else {
                                                echo("<option value='{$value["cod_tipo_funcionario"]}'>{$value["descricao"]}</option>");
                                            }
                                        }

                                        ?>
                                    </select>

                                </div>
                                <div class="col-md-2">


                                    <label>Matricula</label>
                                    <?php
                                    if($dadosFunc){
                                        echo "<input name='matricula' required class='form-control number' value='{$dadosFunc['matricula']}'/>";

                                        echo "<input type='hidden' name='matriculaAntiga' required value='{$dadosFunc['matricula']}'/>";
                                        echo "<input type='hidden' name='codFuncionario' required value='{$dadosFunc['cod_funcionario']}'/>";
                                    }else{
                                        echo "<input name='matricula' required class='form-control number'/>";
                                    }
                                    ?>

                                </div>





                                <div class="col-md-5">
                                    <label>Nome Completo</label>
                                    <input type="text" name="nome" value="<?php echo $dadosFunc['nome_funcionario']?>" required class="form-control">

                                </div>


                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <label>Data de Nascimento <?php echo $dadosFunc['data_nasc']; ?></label>
                                    <input type="text" class="form-control data" value="<?php echo $this->parse_timestamp($dadosFunc['data_nasc']); ?>" name="dataNascimento" required>
                                </div>

                                <div class="col-md-2 col-lg-3">
                                    <label>Fun��o</label>
                                    <select name="funcao" required class="form-control">
                                        <option disabled selected>Selecione a Fun��o</option>
                                        <?php
                                        $cargo_funcionario = $this->medoo->select("cargos_funcionarios", "*", ["ORDER" => "descricao"]);
                                        foreach ($cargo_funcionario as $dados => $value) {
                                            if($dadosFunc['cargo'] == $value['descricao'])
                                                echo('<option value="' . $value['cod_cargos'] . '" selected>' . $value['descricao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_cargos'] . '">' . $value['descricao'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-3">

                                    <label>Escolaridade</label>
                                    <select class="form-control" name="escolaridade" required>
                                        <?php
                                        $escolaridade = $this->medoo->select("tipo_escolaridade", "*", ["ORDER" => "cod_tipo_escolaridade"]);

                                        foreach ($escolaridade as $dados => $value){
                                            if($dadosFunc['escolaridade'] == $value['descricao'])
                                                echo ("<option value='{$value["cod_tipo_escolaridade"]}' selected>{$value['descricao']}</option>");
                                            else{
                                                echo ("<option value='{$value["cod_tipo_escolaridade"]}'>{$value['descricao']}</option>");
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>


                                <div class="col-md-2">
                                    <label>Em andamento</label>
                                    <input type="checkbox" class="form-control" <?php if($dadosFunc['ef_andamento'] == 's') echo("checked"); ?> name="emAndamento" required>
                                </div>
                                <div class="col-md-2">
                                    <label>Curso t�cnico</label>
                                    <input type="checkbox" class="form-control" <?php if($dadosFunc['ef_curso_tecnico'] == 's'){ echo("checked"); $displayCt = "block"; }else{$displayCt = "none";}?> name="cursoTecnicoSimples" required>
                                </div>
                                <div class="col-md-3" id="divNomeCursoTecnico" style = "display: <?php echo $displayCt; ?>;">
                                    <label>Nome do Curso T�cnico</label>
                                    <input type="text" class="form-control" value="<?php echo $dadosFunc['ef_nome_curso_tecnico']?>" name="nomeCursoTecnico">
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-3">
                                    <label>CPF</label>
                                    <input type="text" name="cpfCnpj" required class="form-control cpfpj" value="<?php echo $dadosFunc['cpf']?>">
                                </div>

                                <div class="col-md-4 centro_lotacao"  >
                                    <label>Centro de Resultado</label>
                                    <select class="form-control " required name="centroResultado">
                                        <option disabled selected>Selecione a Ger�ncia</option>
                                        <?php
                                        $centroCusto = $this->medoo->select("centro_resultado", "*", ["ORDER" => "descricao"]);

                                        foreach ($centroCusto as $dados => $value) {
                                            if($dadosFunc['num_centro_resultado'] == $value['num_centro_resultado'])
                                                echo('<option value="' . $value['cod_centro_resultado'] . '" selected>' . $value['num_centro_resultado'] . " - " . $value['descricao'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_centro_resultado'] . '">' . $value['num_centro_resultado'] . " - " . $value['descricao'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-5 centro_lotacao"  >


                                    <label>Lota��o</label>
                                    <select class="form-control" required name="unidade">
                                        <option disabled selected> Selecione a Lota��o</option>
                                        <?php
                                        $unidade = $this->medoo->select("unidade", "*", ["ORDER" => "nome_unidade"]);

                                        foreach ($unidade as $dados => $value) {
                                            if($dadosFunc['nome_unidade'] == $value['nome_unidade'])
                                                echo('<option value="' . $value['cod_unidade'] . '" selected>' . $value['nome_unidade'] . '</option>');
                                            else
                                                echo('<option value="' . $value['cod_unidade'] . '">' . $value['nome_unidade'] . '</option>');
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>E-mail Corporativo ou Principal</label>
                                    <input type="email" name="emailCorp" class="form-control" value="<?php echo $dadosFunc['cf_email']?>">
                                </div>

                                <div class="col-md-3">
                                    <label>E-mail Pessoal</label>
                                    <input type="email" name="emailPes" class="form-control" value="<?php echo $dadosFunc['email_pessoal']?>">
                                </div>

                                <div class="col-md-3">
                                    <label>Telefone Corporativo ou Principal</label>
                                    <input type="text" name="telefoneCorp" required class="form-control telefone" required value="<?php echo $dadosFunc['celular']?>">
                                </div>

                                <div class="col-md-3">
                                    <label>Telefone Pessoal</label>
                                    <input type="text" name="telefonePes" class="form-control telefone" value="<?php echo $dadosFunc['cf_fone_res']?>">
                                </div>
                            </div>

                            <div class="row hidden-print" style="padding-top: 2em">
                                <div class="col-md-3">
                                    <button class="btn btn-success btn-lg btn-block btnSalvarContinuar" type="button">
                                        <i class="fa fa-edit fa-fw"></i>
                                        <?php
                                        if($dadosFunc)
                                            echo (' Atualizar');
                                        else
                                            echo(' Cadastrar');
                                        ?>
                                    </button>
                                </div>

                                <div class="col-md-3">
                                    <button class="btn btn-primary btn-lg btn-block" onclick="window.print()"><i class="fa fa-print"></i> Imprimir</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>