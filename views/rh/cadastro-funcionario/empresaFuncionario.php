
	<!-- Iniciado T�tulo -->
<div class="page-header" style="margin-top: 0px;">
	<h1>Cadastro de Funcion�rio</h1>
</div>
<!-- Criado ordem de preenchimento -->
<div class="row">
		<div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a  href="<?php echo(HOME_URI); ?>cadastro/indexRefill">Dados Pessoais</a>

                <li><a href="<?php echo(HOME_URI); ?>cadastro/documentacaoFuncionarioRefill"> Documenta��o e Dados Banc�rios</a>

                <li><a href="<?php echo(HOME_URI); ?>cadastro/filiacaoFuncionarioRefill">Filia��o, Dependentes e Benefici�rios</a>

                <li class="active">Dados da Empresa

                <li><?php
                    if(!empty ($_SESSION['empresaFuncionario'])){
                        echo('<a href="' .HOME_URI. 'cadastro/confirmaCadastroRefill">'.
                            'Confirma do Cadastro</a>');
                    }else{
                        echo ('Confirma��o do Cadastro');
                    }

                    ?>


            </ol>
		</div>
</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">4- Preenchimento da Empresa</div>

				<div class="panel-body">

					<form id="formulario" class="form-group" enctype="multipart/form-data" method="post"action="<?php echo(HOME_URI); ?>cadastro/confirmaCadastro">

						<div class="row">
							<div class="col-lg-2">
								<label>Cargo</label>
								<?php
									$codigoCargo = $this->medoo->select("cargos_funcionarios", ["descricao", "cod_cargos"]);
									$codigoCargo = array_combine(range(1, count($codigoCargo)), $codigoCargo);
									
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										
										if (empty ( $_SESSION ['empresaFuncionario'] ['cargoFuncionario'] )) {
											
											echo ('<select name="cargoFuncionario" class="form-control" required data-validation-required-message=" Campo precisa ser preenchido" 
													data-bv-row=".col-lg-4"
	               									data-fv-notempty="true"
	               									data-fv-notempty-message="Informe o n�mero da carteira.">');
											echo ('<option selected="selected" value="" disabled="disabled">INFORME O CARGO</option>');
											
											foreach ($codigoCargo as $key => $value) {
												echo ('<option value="'.$value["cod_cargos"].'" >'.$value['descricao'].'</option>');
											}
											echo ('</select>');
											
										} else {
											
											echo ('<select name="cargoFuncionario" class="form-control">');
																					
											$valor_selecionado = $_SESSION ['empresaFuncionario'] ['cargoFuncionario'];
											
											foreach ($codigoCargo as $key => $value) {
												$selected = ($valor_selecionado == $key) ? "selected=\"selected\"" : null;
                                                echo ('<option value="'.$value["cod_cargos"] .'"'.$selected.' >'.$value['descricao'].'</option>');
											}
											echo ('</select>');
											
										}
										
									} else {
										
											echo ('<select name="cargoFuncionario" class="form-control"
													required data-validation-required-message=" Campo precisa ser preenchido" 
													data-bv-row=".col-lg-4"
	               									data-fv-notempty="true"
	               									data-fv-notempty-message="Informe o n�mero da carteira.">');
											echo ('<option selected="selected" value="" disabled="disabled">INFORME O CARGO</option>');
											
											foreach ($codigoCargo as $key => $value) {
                                                echo ('<option value="'.$value["cod_cargos"].'" >'.$value['descricao'].'</option>');
											}
											echo ('</select>');
											
								}
								unset($codigoCargo);
								
								?>
							</div>
							<div class="col-lg-2">
								<label>Data de Admiss�o</label> <input class="form-control data" type="text" name="dataAdmissaoFuncionario" value="<?php if (! empty($_SESSION['empresaFuncionario'])) { echo ($_SESSION['empresaFuncionario']['dataAdmissaoFuncionario']); }?>"
								required data-validation-required-message=" Nome do funcion�rio � necess�rio" 
								data-bv-row=".col-lg-4"
               					data-fv-notempty="true"
               					data-fv-notempty-message="Informe a data de admiss�o."/>
							</div>
							<div class="col-lg-2">
								<label>Sal�rio</label>
								<input type="text" name="salarioFuncionario" class="form-control real" value="<?php if (! empty($_SESSION['empresaFuncionario'])) { echo ($_SESSION['empresaFuncionario']['salarioFuncionario']); }?>"
								required data-validation-required-message=" Nome do funcion�rio � necess�rio" 
								data-fv-row=".col-lg-4"
               					data-fv-notempty="true"
               					data-fv-notempty-message="Campo Obrigat�rio."/>
							</div>
							<div class="col-lg-3">
								<label>Categoria</label>
								<div>
								<?php
									if (! empty($_SESSION['empresaFuncionario'])) {
										
										if ( $_SESSION['empresaFuncionario']['categoriaSalarioFuncionario'] === "m" ) {
											echo('<label class="radio-inline"> <input type="radio" name="categoriaSalarioFuncionario" checked="checked" value="m" /> Mensalista </label>');
											echo('<label class="radio-inline"> <input type="radio" name="categoriaSalarioFuncionario" value="b" /> Bolsista </label>');
										}else{
											echo('<label class="radio-inline"> <input type="radio" name="categoriaSalarioFuncionario" value="m" /> Mensalista </label>');
											echo('<label class="radio-inline"> <input type="radio" name="categoriaSalarioFuncionario" checked="checked" value="b" /> Bolsista </label>');
										}
									}else {
										echo('<label class="radio-inline"> <input type="radio" name="categoriaSalarioFuncionario" checked="checked" value="m" /> Mensalista </label>');
										echo('<label class="radio-inline"> <input type="radio" name="categoriaSalarioFuncionario" value="b" /> Bolsista </label>');
									}
								?>

								</div>
							</div>
							
							<div class="col-lg-2">
								<label>Carga Hor�ria Semanal</label> <input class="form-control" type="text" name="cargaHorariaFuncionario" value="<?php if (! empty($_SESSION['empresaFuncionario'])) { echo ($_SESSION['empresaFuncionario']['cargaHorariaFuncionario']); }?>"/>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-2">
								<label>Centro de Custo</label>
								<?php
									$codigoCentroCusto = $this->medoo->select("centro_resultado", ["num_centro_resultado", "descricao"],["ORDER" => "cod_centro_resultado"]);
									$codigoCentroCusto = array_combine(range(1, count($codigoCentroCusto)), $codigoCentroCusto);
									
									
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										
										if (empty ( $_SESSION ['empresaFuncionario'] ['centroCustoFuncionario'] )) {
											
											echo ('<select name="centroCustoFuncionario" class="form-control" required data-validation-required-message=" Campo precisa ser preenchido" 
													data-bv-row=".col-lg-4"
	               									data-fv-notempty="true"
	               									data-fv-notempty-message="Informe o n�mero da carteira.">');
											echo ('<option selected="selected" value="" disabled="disabled">INFORME O CENTRO DE CUSTO</option>');
											
											foreach ($codigoCentroCusto as $key => $value) {
												$codigoDescricao = implode(" - ", $value);
												echo "<option value=\"$key\" >" . $codigoDescricao . "</option>";
											}
											echo ('</select>');
											
										} else {
											
											echo ('<select name="centroCustoFuncionario" class="form-control">');
																					
											$valor_selecionado = $_SESSION ['empresaFuncionario'] ['centroCustoFuncionario'];
											
											foreach ($codigoCentroCusto as $key => $value) {
												$selected = ($valor_selecionado == $key) ? "selected=\"selected\"" : null;
												$codigoDescricao = implode(" - ", $value);
												echo "<option value=\"$key\" $selected >$codigoDescricao</option>";
											}
											echo ('</select>');
											
										}
										
									} else {
											echo ('<select name="centroCustoFuncionario" class="form-control"
													required data-validation-required-message=" Campo precisa ser preenchido" 
													data-bv-row=".col-lg-4"
	               									data-fv-notempty="true"
	               									data-fv-notempty-message="Informe o n�mero da carteira.">');
											echo ('<option selected="selected" value="" disabled="disabled">INFORME O CENTRO DE CUSTO</option>');
										
											foreach ($codigoCentroCusto as $key => $value) {
												$codigoDescricao = implode(" - ", $value);
												echo "<option value=\"$key\" >$codigoDescricao</option>";
											}
											echo ('</select>');
								}
								unset($codigoCentroCusto);
								
								?>
								
							</div>
							
							<div class="col-lg-2">
								<div class="checkbox">
									<?php 
									if (! empty($_SESSION['empresaFuncionario']) AND ! empty($_SESSION['empresaFuncionario']['periculosidadeFuncionario'])) {
										if($_SESSION['empresaFuncionario']['periculosidadeFuncionario'] == "on") {
											echo ('<input type="checkbox" name="periculosidadeFuncionario" checked="checked"/>Periculosidade');
										}else {
											echo ('<input type="checkbox" name="periculosidadeFuncionario"/>Periculosidade');
										}
										
									} else {
											echo ('<input type="checkbox" name="periculosidadeFuncionario"/>Periculosidade');
									}
									?>
								</div>
							</div>
							
							<div class="col-lg-2">
								<div class="checkbox">
									<?php 
									if (! empty($_SESSION['empresaFuncionario']) AND ! empty($_SESSION['empresaFuncionario']['insalubridadeFuncionario'])) {
										if($_SESSION['empresaFuncionario']['insalubridadeFuncionario'] == "on") {
											echo ('<input type="checkbox" name="insalubridadeFuncionario" checked="checked"/> Insalubridade');
										} else {
											echo ('<input type="checkbox" name="insalubridadeFuncionario"/> Insalubridade');
										}
									} else {
											echo ('<input type="checkbox" name="insalubridadeFuncionario"/> Insalubridade');
									}
									?>
								</div>
							</div>
							
							<div class="col-lg-2">
								<label>Data do ASO</label>
								<input class="form-control data" type="text" name="dataAtestadoSaudeOperacionalFuncionario"
                                       value="<?php if (! empty($_SESSION['empresaFuncionario'])) {
                                       echo ($_SESSION['empresaFuncionario']['dataAtestadoSaudeOperacionalFuncionario']); }?>"/>
							</div>
						</div>

						<div class="row"
							style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

						<div class="row" style="padding-top: 2em;">

							<div class="col-lg-3">
								<label>Sindicato</label> <input class="form-control" type="text" name="sindicatoFuncionario" value="SINTEPAV"/>
							</div>
							<div class="col-lg-4">
								<label>Situa��o Sindical</label> 
								
								<?php
								
									if (! empty ( $_SESSION ['empresaFuncionario'] )) {
										
										if (empty ( $_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario'] )) {
											echo ('<select name="situacaoSindicalFuncionario" class="form-control"
													required data-validation-required-message=" Campo precisa ser preenchido" 
													data-bv-row=".col-lg-4"
	               									data-fv-notempty="true"
	               									data-fv-notempty-message="Selecione o sexo.">
														<option selected="selected" value="" disabled="disabled">Informe a Situa��o Sindical</option>
														<option value="1">Pago</option>
														<option value="2">Descontar</option>
														<option value="3">Profissional Liberal</option>	
												</select>');

										} else {
											
											if ($_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario'] === "1") {
												echo ('<select name="situacaoSindicalFuncionario" class="form-control">
														<option value=""disabled="disabled">Informe a Situa��o Sindical</option>
														<option value="1" selected="selected" >Pago</option>
														<option value="2">A Descontar</option>
														<option value="3">Profissional Liberal</option>	
													</select>');
											} 
											if ($_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario'] === "2") {
												echo ('<select name="situacaoSindicalFuncionario" class="form-control">
														<option value="" disabled="disabled">Informe a Situa��o Sindical</option>
														<option value="1">Pago</option>
														<option value="2" selected="selected">A Descontar</option>
														<option value="3">Profissional Liberal</option>	
													</select>');
											} 
											if ($_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario'] === "3") {
												echo ('<select name="situacaoSindicalFuncionario" class="form-control">
														<option value="" disabled="disabled">Informe a Situa��o Sindical</option>
														<option value="1">Pago</option>
														<option value="2">A Descontar</option>
														<option value="3" selected="selected">Profissional Liberal</option>	
													</select>');
											} 
										}
									} else {
										echo ('<select name="situacaoSindicalFuncionario" class="form-control"
												required data-validation-required-message=" Campo precisa ser preenchido" 
												data-bv-row=".col-lg-4"
	               								data-fv-notempty="true"
	               								data-fv-notempty-message="Selecione a situa��o Sindical."> 							
													<option value="" selected="selected" disabled="disabled">Informe a situa��o sindical</option>
													<option value="1">Pago</option>
													<option value="2">Descontar</option>
													<option value="3">Profissional Liberal</option>	
											</select>');
									}
								?>
							</div>
							<div class="col-lg-3">
								<label>Registro</label> <input class="form-control" type="text" name="registroSindicatoFuncionario" value="<?php if (! empty($_SESSION['empresaFuncionario'])) { echo ($_SESSION['empresaFuncionario']['registroSindicatoFuncionario']); }?>"/>
							</div>
							<div class="col-lg-2">
								<label>Chapa</label> <input class="form-control" type="text" name="chapaSindicatoFuncionario" value="<?php if (! empty($_SESSION['empresaFuncionario'])) { echo ($_SESSION['empresaFuncionario']['chapaSindicatoFuncionario']); }?>"/>
							</div>
                        </div>
                        <div class="row">
							<div class="col-lg-4">
								<label>E-mail Corporativo</label> <input class="form-control" type="text" name="emailFuncionario"  value="<?php if (! empty($_SESSION['empresaFuncionario'])) { echo ($_SESSION['empresaFuncionario']['emailFuncionario']); }?>"
								required data-validation-required-message=" Nome do funcion�rio � necess�rio"
								data-fv-row=".col-lg-4"
               					data-fv-notempty="true"
               					data-fv-emailaddress="true"
               					data-fv-emailaddress-message="Preencha um e-mail v�lido"
               					data-fv-notempty-message="Campo Obrigat�rio."/>
							</div>
							<div class="col-lg-4">
								<label>Unidadade</label>
								<?php
									$codigoUnidade = $this->medoo->select("unidade", ["nome_unidade","cod_unidade"]);
									$codigoUnidade = array_combine(range(1, count($codigoUnidade)), $codigoUnidade);

									if (! empty ( $_SESSION ['empresaFuncionario'] )) {

											if (empty ( $_SESSION ['empresaFuncionario'] ['unidadeFuncionario'] )) {

												echo ('<select name="unidadeFuncionario" class="form-control" required data-validation-required-message=" Campo precisa ser preenchido"
														data-bv-row=".col-lg-4"
														data-fv-notempty="true"
														data-fv-notempty-message="Informe o n�mero da carteira.">');
												echo ('<option selected="selected" value="" disabled="disabled">INFORME A UNIDADE</option>');

												foreach ($codigoUnidade as $key => $value) {
													echo '<option value="'.$value['cod_unidade'].'" >'.$value['nome_unidade'].'</option>';
												}
												echo ('</select>');

											} else {

												echo ('<select name="unidadeFuncionario" class="form-control">');

												$valor_selecionado = $_SESSION ['dadosPessoais'] ['unidadeFuncionario'];

												foreach ($codigoUnidade as $key => $value) {
													$selected = ($valor_selecionado == $key) ? "selected=\"selected\"" : null;
                                                    echo '<option value="'.$value['cod_unidade'].'"  '.$selected.'>'.$value['nome_unidade'].'</option>';
												}
												echo ('</select>');

											}

										} else {

											echo ('<select name="unidadeFuncionario" class="form-control"
														required data-validation-required-message=" Campo precisa ser preenchido"
														data-bv-row=".col-lg-4"
														data-fv-notempty="true"
														data-fv-notempty-message="Informe o n�mero da carteira.">');
											echo ('<option selected="selected" value="" disabled="disabled">INFORME A UNIDADE</option>');

											foreach ($codigoUnidade as $key => $value) {
                                                echo '<option value="'.$value['cod_unidade'].'" >'.$value['nome_unidade'].'</option>';
											}
											echo ('</select>');
										}

								?>
                                </div>
                            <div class="col-md-4">
                                <label>V�nculo</label>

                                <?php

                                if (! empty ( $_SESSION ['empresaFuncionario'] )) {

                                    if (empty ( $_SESSION ['empresaFuncionario'] ['vinculoEmpresaFuncionario'] )) {
                                        echo ('<select name="vinculoEmpresaFuncionario" class="form-control"
                                                required data-validation-required-message=" Campo precisa ser preenchido"
                                                data-bv-row=".col-lg-4"
                                                data-fv-notempty="true"
                                                data-fv-notempty-message="Selecione uma op��o.">
                                                    <option selected="selected" value="" disabled="disabled">Informe a Situa��o Sindical</option>
                                                    <option value="Diretor">Direto</option>
                                                    <option value="CLT">CLT</option>
                                                    <option value="Estagiario">Estagi�rio</option>
                                            </select>');

                                    } else {

                                        if ($_SESSION ['empresaFuncionario'] ['vinculoEmpresaFuncionario'] === "Diretor") {
                                            echo ('<select name="vinculoEmpresaFuncionario" class="form-control">
                                                    <option value="" disabled="disabled">Informe a Situa��o Sindical</option>
                                                    <option selected="selected" value="Diretor">Direto</option>
                                                    <option value="CLT">CLT</option>
                                                    <option value="Estagiario">Estagi�rio</option>
                                            </select>');
                                        }
                                        if ($_SESSION ['empresaFuncionario'] ['vinculoEmpresaFuncionario'] === "CLT") {
                                            echo ('<select name="vinculoEmpresaFuncionario" class="form-control">
                                                    <option value="" disabled="disabled">Informe a Situa��o Sindical</option>
                                                    <option value="Diretor">Direto</option>
                                                    <option selected="selected" value="CLT">CLT</option>
                                                    <option value="Estagiario">Estagi�rio</option>
                                            </select>');
                                        }
                                        if ($_SESSION ['empresaFuncionario'] ['situacaoSindicalFuncionario'] === "Estagiario") {
                                            echo ('<select name="vinculoEmpresaFuncionario" class="form-control">
                                                    <option value="" disabled="disabled">Informe a Situa��o Sindical</option>
                                                    <option value="Diretor">Direto</option>
                                                    <option value="CLT">CLT</option>
                                                    <option selected="selected" value="Estagiario">Estagi�rio</option>
                                            </select>');
                                        }
                                    }
                                } else {
                                    echo ('<select name="vinculoEmpresaFuncionario" class="form-control"
                                                required data-validation-required-message=" Campo precisa ser preenchido"
                                                data-bv-row=".col-lg-4"
                                                data-fv-notempty="true"
                                                data-fv-notempty-message="Selecione uma op��o.">
                                                    <option selected="selected" value="" disabled="disabled">Informe a Situa��o Sindical</option>
                                                    <option value="Diretor">Direto</option>
                                                    <option value="CLT">CLT</option>
                                                    <option value="Estagiario">Estagi�rio</option>
                                            </select>');
                                }
                                ?>

                                </div>
						</div>


						<div class="row"style="border-bottom: 1px dashed black; padding-top: 2em;"></div>

						<div class="row">
							<div class="col-lg-3">
								<label>Adicionar Foto</label>
                                <?php
                                    if(! empty($_FILES['fotoFuncionario'])){
                                        echo('<input name="fotoFuncionario" type="file" id="fotoFuncionario"
								                required data-validation-required-message="Selecione uma foto."
								                data-bv-row=".col-lg-4"
                                                data-fv-notempty="true"
               					                data-fv-notempty-message="Selecione uma foto.
               					                value=" ' .$_FILES['fotoFuncionario']['']. ' ">');
                                    }else{
                                        echo('<input name="fotoFuncionario" type="file" id="fotoFuncionario"
								                required data-validation-required-message="Selecione uma foto."
								                data-bv-row=".col-lg-4"
               					                data-fv-notempty="true"
               					                data-fv-notempty-message="Selecione uma foto."/>');
                                    }

                                ?>

							</div>
						</div>
						<div class="row"
							style="border-bottom: 1px dashed black; padding-top: 2em;">
						</div>

						<div class="row" style="padding-top: 2em">
							<div class="col-lg-4">
								<button class="btn btn-success btn-lg btn-block" type="submit">Salvar e Confirmar</button>
							</div>
						</div>
					</form>
				</div>
				<!-- End Painel -->

			</div>
		</div>

	</div>
