<div class="page-header">
	<h1>Desligamento de Funcionario</h1>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-red">
			<div class="panel-heading">Dados para demiss�o</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<form class="form-group" id="formulario" action="<?php echo (HOME_URI)?>/dashboardRh" role="form">
							<div class="row">
								<div class="col-md-6">
									<label>Nome do Funcionario:</label>
									<?php echo ('<h5>Fulano de Tal</h5>')?>
								</div>
								<div class="col-md-2">
									<label>Cargo:</label>
									<?php echo ('<h5>Estagi�rio</h5>')?>
								</div>
								<div class="col-md-2">
									<label>Sal�rio:</label>
									<?php echo ('<h5>R$ 750,00</h5>')?>
								</div>
								<div class="col-md-2">
									<label>Funcionario desde:</label>
									<?php echo ('<h5>09/05/214</h5>')?>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-2">
									<label>Centro de Custo:</label>
									<?php echo ('<h5>T.I.</h5>')?>
								</div>
								<div class="col-md-2">
									<label>Ger�ncia:</label>
									<?php echo ('<h5>T.I.</h5>')?>
								</div>
								
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<label>Motivo da Demiss�o:</label>
									<textarea rows="2" class="form-control" ></textarea>
								</div>
								<div class="col-md-2">
									<label>Data da Demiss�o:</label>
									<input class="form-control data" type="text" name="data_demiss�o">
								</div>
								<div class="col-md-2">
									<div class="checkbox">
										<label><input type="checkbox" name="avisoPrevio">Aviso Pr�vio</label>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4" style="margin-top: 1em"> 	
									<button type="button" id="btnDemissao" class="btn btn-danger btn-lg" data-toggle="modal"
									data-target="#confirmacaoDesligarFuncionario">
										Desligar Funcionario
									</button>
								</div>
							</div>
							
							<div class="modal fade" id="confirmacaoDesligarFuncionario" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h1 class="modal-title">
												Confirmar desligamento
											</h1>
										</div>
										
										<div class="modal-body">
											<label>Voc� tem certeza de que deseja desligar este funcionario?</label>
											<p>A a��o s� poder� ser desfeita pelo coordenador do Departamento Pessoal</p>
										</div>
										
										<div class="modal-footer">
											<a href="<?php echo (HOME_URI)?>/dashboardRh">
												<button type="button" class="btn btn-default">Cancelar</button>
											</a>
       										<button type="submit" class="btn btn-primary">Desligar Funcionario</button>
										</div>					
									</div>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>