<div class="page-header">
    <h1>Controle de Folha de Frequ�ncia</h1>
</div>

<!-- http://10.0.0.183/simg//dashboardrh/controlePonto -->

<div class="row">
    <div class="col-lg-12 col-md-6">
        <form action="" id="pontoForm">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Dia</th>
                    <th colspan="2" style="text-align: center">1� Expediente</th>
                    <th colspan="2" style="text-align: center">2� Expediente</th>
                    <th colspan="2" style="text-align: center">Extras</th>
                    <th style="text-align: center">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>- - - - -</td>
                    <td style="text-align: center">Entrada</td>
                    <td style="text-align: center">Sa�da</td>
                    <td style="text-align: center">Entrada</td>
                    <td style="text-align: center">Sa�da</td>
                    <td style="text-align: center">Entrada</td>
                    <td style="text-align: center">Sa�da</td>
                    <td>- - - - -</td>
                </tr>
                <?php
                for ($i = 26; $i <= 31; $i++) {

                    echo('<div id="controlePonto">');
                    echo "<tr>.<td>$i</td>";
                    for ($j = 0; $j <= 5; $j++) {
                        echo "<td><input type='text' class='form-control hora' id='horaPonto".$j."' placeholder='Insira o Hor�rio'  /></td>";
                    }
                    echo "<td><input type='text' class='form-control date horaDiaTotal' placeholder='Total Di�rio do dia $i'  /></td>";
                    echo "</tr>";
                    echo ('</div>');
                }

                for ($i = 1; $i <= 25; $i++) {

                    echo "<tr><td>$i</td>";
                    for ($j = 0; $j <= 5; $j++) {
                        echo "<td><input type='text' class='form-control' placeholder='Insira o Hor�rio'  /></td>";

                    }
                    echo "<td><input type='text' class='form-control' placeholder='Total Di�rio do dia $i'  /></td>";
                    echo "</tr>";
                }

                ?>

                <tr>
                    <td colspan="4" style="text-align: center"><label>Total de Horas</label></td>
                    <td colspan="4" style="text-align: center"><label id="totalHoras">--- Horas</label></td>
                </tr>

                </tbody>

            </table>
        </form>
    </div>
</div>