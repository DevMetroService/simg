<div class="page-header">
	<h1>Gerador de Relat�rios</h1>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
		
			<div class="panel-heading">Op��es de relat�rios</div>
			
			<div class="panel-body">
				
				<form role="form" class="form-inline">
					<div class="row">
						<div class="col-lg-12">
						
							<div class="form-group">
								<label>Nome:</label><input type="text" name="nomeRelatorioFuncionario" class="form-control">
							</div>
														
							<div class="form-group">
								<label>Idade:</label><input type="text" name="idadeRelatorioFuncionario" class="form-control">
							</div>
							
						</div>					
					</div>
					
					<div class="row">
						<div class="col-lg-7"> 
							<label>Data de Admiss�o:</label> <input type="text" name="admissaoRelatorioFuncionarioIni" class="data form-control">
							<label>At�</label> <input type="text" name="admissaoRelatorioFuncionarioFin" class="data form-control">
						</div>											
					</div>
					
					<div class="row">
						<div class="col-lg-7">						
							<label>Data de Demiss�o:</label> <input type="text" name="demissaoRelatorioFuncionarioIni" class="data form-control">
							<label>At�</label> <input type="text" name="demissaoRelatorioFuncionarioFin" class="data form-control">						
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-7"> 
							<label>Escolaridade:</label> <input type="text" name="escolaridadeRelatorioFuncionarioIni" class="form-control">
							<label> At� </label> <input type="text" name="escolaridadeRelatorioFuncionarioFin" class="form-control">
						</div>
					</div>
					<div class="row" style="border-bottom : 1px solid grey; padding-top: 1em;"></div>
					<div class="row" style="padding-top: 1em">
						<div class="col-md-12">
							<div class="form-group"> 
								<label>Estado</label> 
								<select class="form-control">
									<option>CE</option>
									<option>RJ</option>
								</select> 
							</div>
						
							<div class="form-group">
								<label>Cidade</label> 
								<select class="form-control">
									<option>Fortaleza</option>
									<option>Rio de Janeiro</option>
								</select>
							</div>
							<div class="form-group">
								<label>Bairro</label> 
								<select class="form-control">
									<option>Cidade dos Funcionarios</option>
									<option>Copacabana</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="row" style="border-bottom : 1px solid grey; padding-top: 1em;"></div>
					<div class="row" style="padding-top: 1em">
						<div class="col-md-12">
							<div class="form-group"> 
								<label>Cargo:</label> <input type="text" name="cargoRelatorioFuncionario" class="form-control"> 
							</div>
						
							<div class="form-group"> 
								<label>Centro de Custo:</label> <input type="text" name="centroCustoRelatorioFuncionario" class="form-control"> 
							</div>
							
							<div class="form-group"> 
								<label>Categoria:</label> 
									<select class="form-control">
										<option>Mensalista</option>
										<option>Bolsista</option>
										<option selected="selected">Ambos</option>
									</select>							
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-8"> 
							<label>Sal�rio:</label> <input type="text" name="salarioRelatorioFuncionarioIni" class="form-control real">
							<label>At�</label> <input type="text" name="salarioRelatorioFuncionarioFin" class="form-control real">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-10"> 
							<label>Carga Horaria:</label> <input type="text" name="cargaHorariaRelatorioFuncionarioIni" class="form-control">
							<label>At�</label> <input type="text" name="escolaridadeRelatorioFuncionarioFin" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-10"> 
							<label>Escolaridade:</label> <input type="text" name="atestadoSaudeOperacionalRelatorioFuncionarioIni" class="data form-control">
							<label>At�</label> <input type="text" name="atestadoSaudeOperacionalFuncionarioFin" class="data form-control">
						</div>
					</div>
					
					
					<div class="row" style="border-bottom : 1px solid grey; padding-top: 1em;"></div>
					
					<div class"row">
						<div class="col-md-12">
							<label>Campos adicionais:<small>(Relat�rio � gerado por padr�o com as colunas Matr�cula, Nome, Centro de Custo)</small></label>
						</div>
					</div>
					<div class="row" style="padding-top: 1em">
						<div class="checkbox">
							<div class="col-md-12">
								<label class="checkbox-inline">
									<input type="checkbox" name="campoEndereco">Endere�o
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoIdade">Idade
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoSalario">Setor
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoEscolaridade">Escolaridade
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoCategoria">Data de Admiss�o
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoCargaHoraria">Lotado
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoCargo">Cargo
								</label>
							</div>	
								
							<div class="col-md-12">
								<label class="checkbox-inline">
									<input type="checkbox" name="campoDataDemissao">Data de Demiss�o
								</label>
								<label class="checkbox-inline">
									<input type="checkbox" name="campoDataAdmissao">Data de Admiss�o
								</label>
							</div>
						</div>
						
					</div>
					
					<div class="row" style="padding-top: 1em;"></div>
					<div class="row" style="padding-top: 1em">
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary btn-lg">Gerar Relat�rio</button>
						</div>
					</div>
				</form>
				
			</div>
			
		</div>
	</div>
</div>
