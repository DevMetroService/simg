<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 03/05/2016
 * Time: 17:18
 */

$dadosFunc = $_SESSION['refillSimples'];

$prefix = substr($dadosFunc['matricula'], 0, 3);

switch ($prefix){
    case '700':
        $codTipo = "5";
        $tipo = "PJ";
        break;
    case '800':
        $codTipo = "3";
        $tipo = "MPE";
        break;
    case '900':
        $codTipo = "4";
        $tipo = "CRJ";
        break;
}

unset($_SESSION['refillSimples']);
?>
<div class="page-header">
    <h2>Cadastro de Pessoa Jur�dia ou Consorciada</h2>
</div>

<div class="row">
    <form id="formFuncionario" method="post" action="<?php if($dadosFunc) echo(HOME_URI). "/cadastro/alterarCadastroExterno"; else echo(HOME_URI). "/cadastro/salvarCadastroExterno";?>" class="form-group">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><label>Cadastro de Pessoa Jur�dica ou Consorciada</label></div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Tipo</label>
                                    <?php
                                    if($dadosFunc){
                                        echo "<input disabled value='{$tipo}' class='form-control' /><input type='hidden' name='tipoPj' value='{$codTipo}' class='form-control' />";
                                    }else {
                                        ?>

                                        <select name="tipoPj" required class="form-control">
                                            <option value="" selected disabled>Tipo</option>
                                            <?php
                                            $tipoFuncionario = $this->medoo->select("tipo_funcionario", "*", ["ORDER" => "cod_tipo_funcionario"]);
                                            foreach($tipoFuncionario as $dados=>$value){
                                                if(in_array($value['cod_tipo_funcionario'], array(3, 4, 5))) {
                                                    if ($codTipo == $value['cod_tipo_funcionario']) {
                                                        echo("<option value='{$value['cod_tipo_funcionario']}' selected>{$value['descricao']}</option>");
                                                    } else {
                                                        echo("<option value='{$value['cod_tipo_funcionario']}'>{$value['descricao']}</option>");
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>

                                    <?php } ?>
                                </div>

                                <div class="col-md-2">
                                    <label>Matricula</label>
                                    <a data-toggle="modal" data-target="#modalHelpMatriculaPj"
                                       class="btn-circle hidden-print"><i
                                            style="font-size: 20px"
                                            class="fa fa-question-circle fa-fw"></i></a>
                                    <?php
                                    if($dadosFunc){
                                        echo "<input name='matriculaPj' class='form-control number' required value='{$dadosFunc['matricula']}'/>";
                                        echo "<input type='hidden' name='matriculaAntiga' required value='{$dadosFunc['matricula']}'/>";
                                        echo "<input type='hidden' name='codFuncionario' required value='{$dadosFunc['cod_funcionario']}'/>";
                                    }else{
                                        echo "<input name='matriculaPj' required class='form-control number'/>";
                                    }
                                    ?>
                                </div>

                                <div class="col-md-4">
                                    <label>Nome Completo</label>
                                    <input type="text" name="nomePj" required value="<?php if($dadosFunc){ echo $dadosFunc['nome_funcionario']; } ?>" class="form-control">
                                </div>

                                <div class="col-md-4">
                                    <label>Fun��o</label>
                                    <select name="funcaoPj" required class="form-control">
                                        <option disabled selected>Selecione a Fun��o</option>
                                        <?php
                                        $cargo_funcionario = $this->medoo->select("cargos_funcionarios", "*", ["ORDER" => "descricao"]);

                                        foreach($cargo_funcionario as $dados => $value){
                                            if($dadosFunc['cod_cargo'] == $value['cod_cargos'])
                                                echo("<option value='{$value['cod_cargos']}' selected>{$value['descricao']}</option>");
                                            else
                                                echo("<option value='{$value['cod_cargos']}' >{$value['descricao']}</option>");
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>CPF/CNPJ</label>
                                    <input type="text" name="cpfCnpj" required value="<?php if($dadosFunc){ echo $dadosFunc['cnpj_cpf']; } ?>" class="form-control cpfpj">
                                </div>

                                <div class="col-md-4">
                                    <label>Centro de Resultado</label>
                                    <select class="form-control" required name="centroResultadoPj">
                                        <option disabled selected >Selecione a Ger�ncia</option>
                                        <?php
                                        $centroCusto = $this->medoo->select("centro_resultado", "*", ["ORDER" => "descricao"]);

                                        foreach($centroCusto as $dados => $value){
                                            if($dadosFunc['cod_centro_resultado'] == $value['cod_centro_resultado'])
                                                echo("<option value='{$value['cod_centro_resultado']}' selected>{$value['descricao']}</option>");
                                            else
                                                echo("<option value='{$value['cod_centro_resultado']}' >{$value['descricao']}</option>");
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <label>Lota��o</label>
                                    <select class="form-control" required name="unidadePj">
                                        <option disabled selected> Selecione a Lota��o</option>
                                        <?php
                                        $unidade = $this->medoo->select("unidade", "*", ["ORDER" => "nome_unidade"]);

                                        foreach($unidade as $dados => $value){
                                            if($dadosFunc['unidade'] == $value['cod_unidade'])
                                                echo("<option value='{$value['cod_unidade']}' selected>{$value['nome_unidade']}</option>");
                                            else
                                                echo("<option value='{$value['cod_unidade']}' >{$value['nome_unidade']}</option>");
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>E-mail Corporativo ou Principal</label>
                                    <input type="email" name="emailCorpPj" value="<?php if($dadosFunc){ echo $dadosFunc['email_corporativo']; } ?>" class="form-control" required>
                                </div>
                                <div class="col-md-3">
                                    <label>E-mail Pessoal</label>
                                    <input type="email" name="emailPesPj" value="<?php if($dadosFunc){ echo $dadosFunc['email_pessoal']; } ?>" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Telefone Corporativo ou Principal</label>
                                    <input type="text" name="telefoneCorpPj" value="<?php if($dadosFunc){ echo $dadosFunc['celular_corporativo']; } ?>" class="form-control telefone" required>
                                </div>
                                <div class="col-md-3">
                                    <label>Telefone Pessoal</label>
                                    <input type="text" name="telefonePesPj" value="<?php if($dadosFunc){ echo $dadosFunc['celular_pessoal']; } ?>" class="form-control telefone">
                                </div>
                            </div>

                            <div class="row hidden-print" style="padding-top: 2em">
                                <div class="col-md-3">
                                    <button class="btn btn-success btn-lg btn-block btnSalvarContinuar" type="button">
                                        <i class="fa fa-edit fa-fw"></i>
                                        <?php
                                        if($dadosFunc)
                                            echo (' Atualizar');
                                        else
                                            echo (' Cadastrar');
                                        ?>
                                    </button>
                                </div>

                                <div class="col-md-3">
                                    <button class="btn btn-primary btn-lg btn-block" onclick="window.print()"><i class="fa fa-print"></i> Imprimir</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>