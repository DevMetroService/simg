<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/02/2019
 * Time: 10:43
 */
$this->dashboard->modalUserForm($this->dadosUsuario);

?>


<div class="page-header">
    <h1>Dashboard</h1>
</div>

<!--Quadros INFORMATIVOS-->
<div class="row scrollAuto">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-wrench fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge" id="qtdManut"><?php echo COUNT($veiculosManut);?></div>
                        <div>Manuten��o</div>
                    </div>
                </div>
            </div>
            <a href="#manutencao" data-toggle="modal" data-target="#veiculoManutencao">
                <div class="panel-footer">
                    <span class="pull-left">Visualizar Ve�culos em Manuten��o</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-cog fa-spin fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge" id="qtdOperacao"><?php echo COUNT($veiculosOperacao);?></div>
                        <div>Opera��o</div>
                    </div>
                </div>
            </div>
            <a href="#operacao"  data-toggle="modal" data-target="#veiculoOperacao">
                <div class="panel-footer">
                    <span class="pull-left">Visualizar Ve�culos em Opera��o</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-clipboard fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">1</div>
                        <div>PMP</div>
                    </div>
                </div>
            </div>
            <a href="#preventiva">
                <div class="panel-footer">
                    <span class="pull-left">Visualizar Ve�culos Dispon�veis p/ Preventiva</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

<!--Grafico e tabela de Falha/Confiabilidade-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h4>Falhas/M�s</h4>
                    </div>
                    <div class="col-md-offset-2 col-md-2">
                        <select id="grafFrotaFalha" class="form-control">
                            <?php
                            $frotas = $this->medoo->query("Select distinct (frota),
                                CASE 
                                    WHEN frota='s' THEN 'Sobral'
                                    WHEN frota='c' THEN 'Cariri'
                                    WHEN frota='T1' THEN 'TUE'
                                    WHEN frota='F1' THEN 'FORTALEZA 1'
                                    WHEN frota='F2' THEN 'FORTALEZA 2'
                                END
                            FROM veiculo WHERE frota is not null")->fetchAll(PDO::FETCH_ASSOC);

                            foreach($frotas as $dados=>$value){
                                echo "<option value='{$value['frota']}' ";
                                echo $value['frota'] == 'T1' ? 'selected' : '';
                                echo " >{$value['case']}</option>";

                            }

                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="grafMesFalha" class="form-control">
                            <?php
                            $date = date('m-Y');
                            echo("<option value='{$date}'>{$date}</option>");

                            for($i = 1; $i <= 6; $i++){
                                $timeMonth = strtotime("-{$i} Month", time());
                                $date = date('m-Y', $timeMonth);

                                echo("<option value='{$date}'>{$date}</option>");
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body" style="overflow-y: auto">
                <div class="row">
                    <div class="col-md-4">
                        <div id="confiabilidade" style="overflow-y: scroll; padding: 15px">
                            <table id="tbConfiabilidade" class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>Dia</th>
                                <th>A</th>
                                <th>B</th>
                                <th>C</th>
                                <th>TOTAL</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div id="graphFalha"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--APP p/ LINHA e GERAL-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h4>APP</h4>
                    </div>
                    <div class="col-md-offset-4 col-md-2">
                        <select id="grafApp" class="form-control">
                            <?php
                            $date = date('m-Y');
                            echo("<option value='{$date}'>{$date}</option>");

                            for($i = 1; $i <= 6; $i++){
                                $timeMonth = strtotime("-{$i} Month", time());
                                $date = date('m-Y', $timeMonth);

                                echo("<option value='{$date}'>{$date}</option>");
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#Appgeral" aria-controls="home" role="tab" data-toggle="tab">GERAL</a></li>
                            <li role="presentation"><a href="#AppSul" aria-controls="profile" role="tab" data-toggle="tab">L. Sul</a></li>
                            <li role="presentation"><a href="#AppOeste" aria-controls="profile" role="tab" data-toggle="tab">L. Oeste</a></li>
                            <li role="presentation"><a href="#AppCariri" aria-controls="profile" role="tab" data-toggle="tab">L. Cariri</a></li>
                            <li role="presentation"><a href="#AppSobral" aria-controls="profile" role="tab" data-toggle="tab">L. Sobral</a></li>
                            <li role="presentation"><a href="#AppParangabaMucuripe" aria-controls="profile" role="tab" data-toggle="tab">L. Parangaba - Mucuripe</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="Appgeral">
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="appTUE"></div>
                                </div>
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="appVLT"></div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="AppSul">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>Ficha</th>
                                            <th>Denomica��o</th>
                                            <th>Periodicidade</th>
                                            <th>PDF</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $fichas = $this->medoo->select('ficha_material_rodante', [
                                            '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                        ],[
                                            'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'servico_material_rodante.nome_servico_material_rodante'
                                        ],[
                                            'AND'=>[
                                                'servico_material_rodante.ativo' => 's',
                                                'cod_grupo' => '23'
                                            ], 'ORDER' => 'numero_ficha'
                                        ]);

                                        foreach($fichas as $dados=>$value){
                                            echo ("<tr>
                                        <td>{$value['numero_ficha']}</td>
                                        <td>{$value['denominacao']}</td>
                                        <td>{$value['nome_servico_material_rodante']}</td>
                                        <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/TUE/FICHA/" . strtoupper($value['numero_ficha']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                    </tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="AppOeste">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>Ficha</th>
                                            <th>Denomica��o</th>
                                            <th>Periodicidade</th>
                                            <th>PDF</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $fichas = $this->medoo->select('ficha_material_rodante', [
                                            '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                        ],[
                                            'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'servico_material_rodante.nome_servico_material_rodante'
                                        ],[
                                            'AND'=>[
                                                'servico_material_rodante.ativo' => 's',
                                                'cod_grupo' => '23'
                                            ], 'ORDER' => 'numero_ficha'
                                        ]);

                                        foreach($fichas as $dados=>$value){
                                            echo ("<tr>
                                        <td>{$value['numero_ficha']}</td>
                                        <td>{$value['denominacao']}</td>
                                        <td>{$value['nome_servico_material_rodante']}</td>
                                        <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/TUE/FICHA/" . strtoupper($value['numero_ficha']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                    </tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="AppCariri">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>Ficha</th>
                                            <th>Denomica��o</th>
                                            <th>Periodicidade</th>
                                            <th>PDF</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $fichas = $this->medoo->select('ficha_material_rodante', [
                                            '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                        ],[
                                            'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'servico_material_rodante.nome_servico_material_rodante'
                                        ],[
                                            'AND'=>[
                                                'servico_material_rodante.ativo' => 's',
                                                'cod_grupo' => '23'
                                            ], 'ORDER' => 'numero_ficha'
                                        ]);

                                        foreach($fichas as $dados=>$value){
                                            echo ("<tr>
                                        <td>{$value['numero_ficha']}</td>
                                        <td>{$value['denominacao']}</td>
                                        <td>{$value['nome_servico_material_rodante']}</td>
                                        <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/TUE/FICHA/" . strtoupper($value['numero_ficha']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                    </tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="AppSobral">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>Ficha</th>
                                            <th>Denomica��o</th>
                                            <th>Periodicidade</th>
                                            <th>PDF</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $fichas = $this->medoo->select('ficha_material_rodante', [
                                            '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                        ],[
                                            'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'servico_material_rodante.nome_servico_material_rodante'
                                        ],[
                                            'AND'=>[
                                                'servico_material_rodante.ativo' => 's',
                                                'cod_grupo' => '23'
                                            ], 'ORDER' => 'numero_ficha'
                                        ]);

                                        foreach($fichas as $dados=>$value){
                                            echo ("<tr>
                                        <td>{$value['numero_ficha']}</td>
                                        <td>{$value['denominacao']}</td>
                                        <td>{$value['nome_servico_material_rodante']}</td>
                                        <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/TUE/FICHA/" . strtoupper($value['numero_ficha']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                    </tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="AppParangabaMucuripe">
                                <div class="col-md-12" style="margin-top: 15px;">
                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                        <tr role="row">
                                            <th>Ficha</th>
                                            <th>Denomica��o</th>
                                            <th>Periodicidade</th>
                                            <th>PDF</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $fichas = $this->medoo->select('ficha_material_rodante', [
                                            '[><]servico_material_rodante' => 'cod_servico_material_rodante',
                                        ],[
                                            'ficha_material_rodante.denominacao', 'ficha_material_rodante.numero_ficha', 'servico_material_rodante.nome_servico_material_rodante'
                                        ],[
                                            'AND'=>[
                                                'servico_material_rodante.ativo' => 's',
                                                'cod_grupo' => '23'
                                            ], 'ORDER' => 'numero_ficha'
                                        ]);

                                        foreach($fichas as $dados=>$value){
                                            echo ("<tr>
                                        <td>{$value['numero_ficha']}</td>
                                        <td>{$value['denominacao']}</td>
                                        <td>{$value['nome_servico_material_rodante']}</td>
                                        <td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:materialRodante/TUE/FICHA/" . strtoupper($value['numero_ficha']) ."'><button type='button' class='text-center btn btn-default'><i class='fa fa-file-pdf-o fa-fw'></i> PDF</button> </a></td>
                                    </tr>");
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!--MODAL Ve�culos em Manuten��o-->
<div class="modal fade" id="veiculoManutencao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close fa-fw"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Lista de ve�culos em manuten��o</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer tbManut">
                            <thead>
                            <tr role="row">
                                <th>Veiculo</th>
                                <th>Data de Entrada</th>
                                <th>Local</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($veiculosManut as $dados=>$value){
                                echo ("<tr><td>{$value['nome_veiculo']}</td><td>{$value['data_alteracao']}</td><td>CMVF</td></tr>");
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!--MODAL Ve�culos Dispon�veis para Opera��o-->
<div class="modal fade" id="veiculoOperacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close fa-fw"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Lista de ve�culos em manuten��o</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer tbManut">
                            <thead>
                            <tr role="row">
                                <th>Veiculo</th>
                                <th>Data de Entrada</th>
                                <th>Local</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($veiculosOperacao as $dados=>$value){
                                echo ("<tr><td>{$value['nome_veiculo']}</td><td>{$value['data_alteracao']}</td><td>CMVF</td></tr>");
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

