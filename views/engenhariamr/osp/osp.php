<?php

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$acao = true;
$pag = "dadosGerais";
$tipoOS = "osp";

$sql = 'SELECT  *, osp.complemento as complent
            FROM osp
            JOIN osp_servico serv USING (cod_osp)
            LEFT JOIN material_rodante_osp USING(cod_osp)
            WHERE osp.cod_osp = ' . $_SESSION['refillOs']['codigoOs'];

$os = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$os = $os[0];

$os['cod_os'] = $os['cod_osp'];

$encerramento = $this->medoo->select("osp_encerramento", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$encerramento = $encerramento[0];

$tempo = $this->medoo->select("status_osp", "data_status",
    [
        "AND" => [
            "cod_osp"       => $os['cod_osp'],
            "cod_status"    => 10
        ]
    ]);
$tempo = $tempo[0];

$ss = $this->medoo->select("v_ssp", "*",["cod_ssp" => (int) $os['cod_ssp']]);
$ss = $ss[0];

$ss['cod_ss'] = $os['cod_ssm'];

$responsavel = $this->medoo->select("usuario", "usuario" ,["cod_usuario" => $os['usuario_responsavel']]);
$responsavel = $responsavel[0];

$selectDescStatus = $this->medoo->select("status_osp", ["[><]status" => "cod_status"], "*", ["cod_ospstatus" => $os['cod_ospstatus'] ]);
$selectDescStatus = $selectDescStatus[0];

//Recebe o status atual para futuras avalias
$_SESSION['dadosCheck']['statusOs'] = $selectDescStatus['cod_status'];
if($_SESSION['dadosCheck']['codigoOs'] != $_SESSION['refillOs']['codigoOs']){
    $_SESSION['dadosCheck']['codigoOs'] = $_SESSION['refillOs']['codigoOs'];

    $_SESSION['dadosCheck']['maquinaCheck'] = false;
    $_SESSION['dadosCheck']['materialCheck'] = false;
}

//Verifica se a OS est� em execu��o. Quando estiver encerrada n�o h� a necessidade de travar.
if($selectDescStatus['cod_status'] == 10){
    $_SESSION['dadosCheck']['maquina'] = $this->medoo->select("osp_maquina", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
    $_SESSION['dadosCheck']['material'] = $this->medoo->select("osp_material", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
}


$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

$_SESSION['bloqueio'] = $this->bloquearEdicao($selectDescStatus['data_status'],$selectDescStatus['nome_status']);

require_once(ABSPATH . "/views/_includes/formularios/os/dadosGerais.php");