<div class="page-header">
    <h1>Ordem de Servi�o de Manuten��o</h1>
</div>

<?php
if(!empty($_SESSION['refillOsm'])){
    $osm = $this->medoo->select("osm_falha",["[><]osm_servico" => "cod_osm"], "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
    $osm = $osm[0];

    $encerramento = $this->medoo->select("osm_encerramento", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
    $encerramento = $encerramento[0];

    $tempo = $this->medoo->select("status_osm", "data_status",
        [
            "AND" => [
                "cod_osm"       => $osm['cod_osm'],
                "cod_status"    => 10
            ]
        ]);
    $tempo = $tempo[0];

    $ssm = $this->medoo->select("v_ssm", "*",["cod_ssm" => (int) $osm['cod_ssm']]);
    $ssm = $ssm[0];

    $saf = $this->medoo->select("v_saf", "*",["cod_saf" => (int) $ssm['cod_saf']]);
    $saf = $saf[0];

}


?>
<div class="row">
    <form class="form-group" method="post" id="osmDadosGerais">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Ordem de Servi�o da Manunten��o - Falha</label></div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label for="equipe">Equipe</label>
                                                <?php
                                                if(!empty($osm)){
                                                    echo ('<input type="text" disabled class="form-control" value="'.$ssm['sigla_equipe'].'">');
                                                    echo ('<input type="hidden" name="equipeOsm" class="form-control" value="'.$ssm['sigla_equipe'].'"/>');
                                                }else{
                                                    echo ('<input type="text" class="form-control" value="" disabled>');
                                                }
                                                ?>
                                            </div>

                                            <div class="col-md-5">
                                                <label for="equipe">Grupo / Sistema Afetado (Falha) </label>
                                                <?php
                                                if(!empty($osm)){
                                                    echo('<input type="text" disabled id="equipe" class="form-control" value="'.$ssm['grupo_sistema']
                                                        .$ssm['sigla_sistema']." / ".$ssm['nome_grupo']." - ".$ssm['nome_sistema'].'"/>');
                                                }else{
                                                    echo('<input type="text" disabled id="equipe" class="form-control" value=""/>');
                                                }
                                                ?>
                                            </div>

                                            <div class="col-md-1">
                                                <label for="nivelSsm">Nivel</label>
                                                <?php
                                                if(!empty($osm)){
                                                    echo('<input type="text" id="nivelSsm" disabled class="form-control" value="'.$ssm['nivel'].'">');
                                                }else{
                                                    echo('<input type="text" id="nivelSsm" disabled class="form-control">');
                                                }
                                                ?>
                                            </div>

                                            <div class="col-md-3">
                                                <label for="dataHora">Data / Hora</label>
                                                <input type="text" disabled id="dataHora" value="<?php echo $this->parse_timestamp($tempo); ?>" class="form-control">
                                            </div>

                                            <div class="col-md-2">
                                                <label for="numeroOsm">N�</label>
                                                <?php
                                                if(!empty($osm)){
                                                    echo('<input type="text" id="numeroOsm" disabled class="form-control" value="'.$osm['cod_osm'].'">');
                                                    echo('<input type="hidden" name="codigoOsm" class="form-control" value="'.$osm['cod_osm'].'">');
                                                }else{
                                                    echo('<input type="text" id="numeroOsm" disabled class="form-control" >');
                                                }
                                                ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><label>Identifica��o</label></div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <label for="codGrupo">Cod.</label>
                                                                <input id="codGrupo" disabled name="codGrupoOsmAtuado" class="form-control"/>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="grupoSistemaAtuado">Grupo de Sistema Atuado </label>
                                                                <select id="grupoSistemaAtuado" name="grupoOsmAtuado" class="form-control">
                                                                    <?php
                                                                    $grupoSistema = $this->medoo->select('grupo', ['nome_grupo','cod_grupo', 'sigla']);
                                                                    $grupoSistema = array_combine(range(1, count($grupoSistema)), $grupoSistema);

                                                                    echo('<option disabled selected>Selecione o Grupo do Sistema</option>');

                                                                    foreach($grupoSistema as $dadosGrupo => $value) {
                                                                        if (!empty($osm)) {
                                                                            if($osm['grupo_atuado']== $value['cod_grupo']){
                                                                                echo('<option value="' . $value['sigla'] . '" selected>' . $value['nome_grupo'] . '</option>');
                                                                            } else {
                                                                                echo('<option value="' . $value['sigla'] . '">' . $value['nome_grupo'] . '</option>');
                                                                            }
                                                                        } else {
                                                                            echo('<option value="' . $value['sigla'] . '">' . $value['nome_grupo'] . '</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <label for="grupo">Cod.</label>
                                                                <input id="grupo" disabled name="codSistemaOsmAtuado" class="form-control"/>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="sistema">Sistema Atuado </label>
                                                                <select id="sistema" name="sistemaOsmAtuado" class="form-control">
                                                                    <?php
                                                                    $sistema = $this->medoo->select('sistema', ['cod_sistema','nome_sistema','sigla'], ['ORDER' => 'nome_sistema']);
                                                                    $sistema = array_combine(range(1, count($sistema)), $sistema);

                                                                    echo('<option disabled selected>Selecione o Sistema</option>');

                                                                    foreach($sistema as $dados => $value){
                                                                        if(!empty($osm)){
                                                                            if($osm['sistema_atuado']==$value["cod_sistema"]){
                                                                                echo ('<option value="'.$value["sigla"].'" selected>'.$value["nome_sistema"].'</option>');
                                                                            }else{
                                                                                echo ('<option value="'.$value["sigla"].'">'.$value["nome_sistema"].'</option>');
                                                                            }
                                                                        }else{
                                                                            echo ('<option value="'.$value["sigla"].'">'.$value["nome_sistema"].'</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label for="linha">Linha</label>
                                                                <select id="linha" name="linhaOsmAtuado" class="form-control">
                                                                    <?php
                                                                    $linha= $this->medoo->select('linha', ['nome_linha','cod_linha'], ["ORDER" => "cod_linha"]);
                                                                    $linha= array_combine(range(1, count($linha)), $linha);

                                                                    foreach($linha as $dados => $value){
                                                                        if(!empty ($osm) ){
                                                                            if($osm['cod_linha_atuado'] == $value['cod_linha']){
                                                                                echo('<option value="'.$value['cod_linha']. '" selected>'. $value['nome_linha'] . '</option>');
                                                                            }else{
                                                                                echo('<option value="'.$value['cod_linha']. '">'. $value['nome_linha'] . '</option>');
                                                                            }
                                                                        }else{
                                                                            echo('<option value="'.$value['cod_linha']. '">'. $value['nome_linha'] . '</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="trecho">Sigla/Trecho</label>
                                                                <select id="trecho" name="trechoOsmAtuado" class="form-control">
                                                                    <?php
                                                                    if(!empty($osm)){

                                                                        $trecho = $this->medoo->select("trecho", ["nome_trecho", "cod_trecho", "descricao_trecho"],["cod_linha" => (int)$osm['cod_linha_atuado']]);
                                                                        $trecho = array_combine(range(1, count($trecho)), $trecho);

                                                                        foreach($trecho as $dados => $value){
                                                                            if($osm['trecho_atuado'] == $value['cod_trecho'] ){
                                                                                echo('<option value="'.$value['cod_trecho']. '" selected>'. $value['nome_trecho']." - ".$value['descricao_trecho'] . '</option>');
                                                                            }else{
                                                                                echo('<option value="'.$value['cod_trecho']. '">'. $value['nome_trecho']." - ".$value['descricao_trecho'] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="pontoNotavel">Ponto Not�vel</label>
                                                                <select id="pontoNotavel" name="pontoNotavelOsmAtuado" class="form-control">
                                                                    <?php
                                                                    if(!empty($osm)){
                                                                        $pn = $this->medoo->select("ponto_notavel", ["nome_ponto","cod_ponto_notavel"], ["cod_trecho" => (int) $osm['trecho_atuado']]);
                                                                        $pn= array_combine(range(1, count($pn)), $pn);

                                                                        foreach($pn as $dados => $value){
                                                                            if($value['cod_ponto_notavel'] == $osm['cod_ponto_notavel_atuado']){
                                                                                echo('<option value="'.$value['cod_ponto_notavel']. '" selected>'. $value['nome_ponto'] . '</option>');
                                                                            }else{
                                                                                echo('<option value="'.$value['cod_ponto_notavel']. '">'. $value['nome_ponto'] . '</option>');
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="complemento">Complemento</label>
                                                                <?php
                                                                if(!empty($osm)){
                                                                    echo('<input id="complementoLocal" type="text" class="form-control" name="complementoLocalOsmAtuado" value="'.$osm['complemento'].'">');
                                                                }else{
                                                                    echo('<input id="complementoLocal" type="text" class="form-control" name="complementoLocalOsmAtuado">');
                                                                }
                                                                ?>
                                                            </div>

                                                        </div>

                                                        <div class="row" style="display: none">
                                                            <div class="col-md-2">
                                                                <label for="composicao">Composi��o</label>
                                                                <select id="composicao" name="composicao" class="form-control">
                                                                    <option>null</option>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <label for="carroComando">Carro Comando</label>
                                                                <input type="text" name="carroComando" id="carroComando"
                                                                       class="form-control"/>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <label for="carroAvariado">Carro Avariado</label>
                                                                <input type="text" name="carroAvariado" id="carroAvariado"
                                                                       class="form-control"/>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="odomContador">Od�m/Cont</label>
                                                                <input type="text" name="odomContador" id="odomContador"
                                                                       class="form-control"/>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label for="prefixo">Prefixo</label>
                                                                <select id="prefixo" name="prefixo" class="form-control">
                                                                    <option>null</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label for="viaAtuada">Via</label>
                                                                <select id="viaAtuada" class="form-control" name="viaOsmAtuada">
                                                                <?php
                                                                if(!empty($osm)){
                                                                    $via = $this->medoo->select("via", ["nome_via","cod_via"], ["cod_linha" => (int)$osm['cod_linha_atuado']]);

                                                                    foreach($via as $dados => $value){
                                                                        if($osm['cod_via'] == $value['cod_via']){
                                                                            echo('<option value="'.$value['cod_via'].'" selected>'.$value['nome_via'].'</option>');
                                                                        }else{
                                                                            echo('<option value="'.$value['cod_via'].'">'.$value['nome_via'].'</option>');
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="kmInicial">Km. Inicial</label>
                                                                <?php
                                                                if(!empty($osm['km_inicial'])){
                                                                    echo('<input type="text" name="kmInicialOsm" id="kmInicial" class="form-control" value="'.$osm['km_inicial'].'">');
                                                                }else{
                                                                    echo('<input type="text" name="kmInicialOsm" id="kmInicial" class="form-control" value="">');
                                                                }
                                                                ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="kmFinal">Km. Final</label>
                                                                <?php
                                                                if(!empty($osm['km_final'])){
                                                                    echo('<input type="text" name="kmFinalOsm" id="kmFinal" class="form-control" value="'.$osm['km_final'].'">');
                                                                }else{
                                                                    echo('<input type="text" name="kmFinalOsm" id="kmFinal" class="form-control" value="">');
                                                                }
                                                                ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="posicao">Posi��o</label>
                                                                <?php
                                                                if(!empty($osm['posicao_atuado'])){
                                                                    echo('<input type="text" name="posicaoAtuado" id="posicao" class="form-control" value="'.$osm['posicao_atuado'].'">');
                                                                }else{
                                                                    echo('<input type="text" name="posicaoAtuado" id="posicao" class="form-control" value="">');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">Servi�o a ser Executado</div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="codigoServico">C�digo</label>
                                                                <input type="text" value="" class="form-control"
                                                                       id="codigoServico" disabled name="codigoServico"/>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="descricaoServico">Descri��o servi�o</label>
                                                                <select id="descricaoServico" name="servicoExecutadoOsm" class="form-control">
                                                                    <?php
                                                                    $servicos = $this->medoo->select("servico", ["nome_servico","cod_servico"],["ORDER" => "cod_servico"]);
                                                                    $servicos = array_combine(range(1, count($servicos)), $servicos);

                                                                    echo ('<option disabled selected>Selecione o Servi�o</option>');

                                                                    foreach ($servicos as $dados => $value){
                                                                        if(!empty($osm)){
                                                                            if($osm['cod_servico']==$value['cod_servico']){
                                                                                echo('<option selected value="'.$value['cod_servico'].'">'.$value['nome_servico']." - ".$value['cod_servico'].'</option>');
                                                                            }else{
                                                                                echo('<option value="'.$value['cod_servico'].'">'.$value['nome_servico']." - ".$value['cod_servico'].'</option>');
                                                                            }
                                                                        }else{
                                                                            echo('<option value="'.$value['cod_servico'].'">'.$value['nome_servico']." - ".$value['cod_servico'].'</option>');
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <label for="complementoServico">Complemento</label>
                                                                <?php
                                                                if(!empty($osm['complemento'])){
                                                                    echo('<input type="text" value="'.$osm['complemento'].'" class="form-control" id="complementoServico" name="complementoServico"/>');
                                                                }else{
                                                                    echo('<input type="text" value="" class="form-control" id="complementoServico" name="complementoServico"/>');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="efetivoServico">Efetivo</label>
                                                                <?php
                                                                if(!empty($osm)){
                                                                    echo('<input type="text" value="'.$osm['qtd_pessoas'].'" class="form-control" id="efetivoServico" name="efetivoServico"/>');
                                                                }else{
                                                                    echo('<input type="text" value="" class="form-control" id="efetivoServico" name="efetivoServico"/>');
                                                                }
                                                                ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="tempoServico">Tempo</label>
                                                                <?php
                                                                if(!empty($osm['tempo'])){
                                                                    echo('<input type="text" value="'.$osm['tempo'].'" class="form-control" id="tempoServico" name="tempoServico"/>');
                                                                }else{
                                                                    echo('<input type="text" value="" class="form-control" id="tempoServico" name="tempoServico"/>');
                                                                }
                                                                ?>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <label for="unidadeTempoServico">Unidade</label>
                                                                <select id="unidadeTempoServico" name="unidadeTempoServico" class="form-control">
                                                                    <?php
                                                                    $unidadeTempo = $this->medoo->select('unidade_tempo',['cod_uni_tempo','nome_un_tempo']);

                                                                    foreach($unidadeTempo as $dados => $value){
                                                                        if(!empty($osm)){
                                                                            if($osm['unidadeTempoServico'] == $value['nome_un_tempo']){
                                                                                echo('<option value="'.$value['cod_uni_tempo'].'" selected>'.$value['nome_un_tempo'].'</option>' );
                                                                            }else{
                                                                                echo('<option value="'.$value['cod_uni_tempo'].'">'.$value['nome_un_tempo'].'</option>' );
                                                                            }
                                                                        }else{
                                                                            echo('<option value="'.$value['cod_uni_tempo'].'">'.$value['nome_un_tempo'].'</option>' );
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label for="totalServico">Total Servi�o</label>
                                                                <?php
                                                                if(!empty($osm['total_servico'])){
                                                                    echo('<input type="text" value="'.$osm['total_servico'].'" class="form-control" id="totalServico" name="totalServico"/>');
                                                                }else{
                                                                    echo('<input type="text" value="" class="form-control" id="totalServico" name="totalServico"/>');
                                                                }
                                                                ?>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <label for="unidadeTotalServico">Unidade</label>
                                                                <select id="unidadeTotalServico" name="unidadeTotalServico" class="form-control">
                                                                    <?php
                                                                    $unidadeTempo = $this->medoo->select('unidade_medida',['cod_uni_medida','nome_uni_medida']);

                                                                    foreach($unidadeTempo as $dados => $value){
                                                                        if(!empty($osm)){
                                                                            if($osm['unidadeTotalServico'] == $value['cod_uni_medida']){
                                                                                echo('<option value="'.$value['cod_uni_medida'].'" selected>'.$value['nome_uni_medida'].'</option>' );
                                                                            }else{
                                                                                echo('<option value="'.$value['cod_uni_medida'].'">'.$value['nome_uni_medida'].'</option>' );
                                                                            }
                                                                        }else{
                                                                            echo('<option value="'.$value['cod_uni_medida'].'">'.$value['nome_uni_medida'].'</option>' );
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">Preenchimento</div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="responsavelSAF">Respons�vel</label>
                                                                <input class="form-control" type="text" id="responsavelSAF" disabled
                                                                       value="<?php echo $this->dadosUsuario['usuario']; ?>"/>
                                                                <input class="form-control" type="hidden"
                                                                       value="<?php echo $this->dadosUsuario['usuario']; ?>"
                                                                       name="responsavelPreenchimento"
                                                                    />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">Situa��o da OSM</div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="situacao">Situa��o</label>
                                                                <?php
                                                                $selectDescStatus = $this->medoo->select("status_osm", ["[><]status" => "cod_status"], "nome_status", ["cod_osm" => $_SESSION['refillOsm']['codigoOsm'] ]);
                                                                $selectDescStatus = $selectDescStatus[0];
                                                                ?>
                                                                <input type="text" disabled class="form-control" value="<?php echo($selectDescStatus); ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">Condi��o da Falha</div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label for="condicao">Condi��o</label>
                                                                <?php
                                                                if($encerramento['liberacao'] == "s"){
                                                                    echo('<input class="form-control" type="text" id="situacao" disabled value="Liberado"/>');
                                                                }else{
                                                                    echo('<input class="form-control" type="text" id="situacao" disabled value="N�o Liberado"/>');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <?php
                            $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                            $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                            $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                            ?>
                            <div class="col-lg-offset-5 col-lg-7">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Complementos</div>
                                    <div class="panel-body">
                                        <div class="btn-group btn-group-justified" role="group">
                                            <div class="btn-group">
                                                <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                                    <i class="fa fa-home fa-2x"></i>
                                                </button>
                                            </div>

                                            <div class="btn-group" >
                                                <a href="<?php echo HOME_URI; ?>/moduloPmp/manobrasEletricas">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                                        <i class="fa fa-bolt fa-2x"></i>
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="<?php echo HOME_URI; ?>/moduloPmp">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                                    <i class="fa fa-users fa-2x"></i> </button>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="<?php echo HOME_URI; ?>/moduloPmp/tempoTotal">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                                    <i class="fa fa-clock-o fa-2x"></i> </button>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                                    <i class="fa fa-crosshairs fa-2x"></i> </button>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="<?php echo HOME_URI; ?>/moduloPmp/maquinaUtilizada">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                                        <i class="fa fa-truck fa-2x"></i>
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                                <a href="<?php echo HOME_URI; ?>/moduloPmp/materialUtilizado/">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                                        <i class="fa fa-wrench fa-2x"></i>
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="btn-group">
                                               <a href="<?php echo HOME_URI; ?>/moduloPmp/encerramento">
                                                    <button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                                    <i class="fa fa-power-off fa-2x"></i> </button>
                                               </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>