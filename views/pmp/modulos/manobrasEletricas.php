<div class="page-header">
    <h2>Manobras El�tricas</h2>
</div>

<div class="row">
    <form class="form-group" method="post">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><label>Ordem de Servi�o da Manunten��o - Falha</label></div>
                    <div class="panel-body">


            <div class="panel panel-default">
                <div class="panel-heading">Lista de Manobras</div>
                <div class="panel-body">
                    <div id="listaManobras">

                        <?php
                        $manobrasEletricas = $this->medoo->select("osm_manobra_eletrica", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']] );

                        $contadorManobra = 0;

                        foreach($manobrasEletricas as $dados=>$value){
                            $selectSolicitanteDesl = $this->medoo->select("funcionario", "nome_funcionario",["cod_funcionario" => (int) $value['solicitado_por_desl']]);
                            $selectSolicitanteDesl = $selectSolicitanteDesl[0];
                            $selectSolicitanteLig = $this->medoo->select("funcionario", "nome_funcionario",["cod_funcionario" => (int) $value['solicitado_por_lig']]);
                            $selectSolicitanteLig = $selectSolicitanteLig[0];


                            $contadorManobra = $contadorManobra +1;
                            echo('<div class="row">');
                            echo('<div class="col-md-2"><label>Local</label><input type="text" class="form-control" disabled value="'.$value['local'].'"></div>');
                            echo('<div class="col-md-1"><label>Chave/Disj.</label><input type="text" class="form-control" disabled value="'.$value['chave'].'"></div>');
                            echo('<div class="col-md-2"><label>Data/Hora Deslig.</label><input type="text" class="form-control" disabled value="'.$value['data_desl'].' '.$value['hora_desl'].'"></div>');
                            echo('<div class="col-md-1"><label>P.M. Deslig.</label><input type="text" class="form-control" disabled value="'.$value['pedido_manobra'].'"></div>');
                            echo('<input type="hidden" name="pm'.$contadorManobra.'" class="form-control" value="'.$value['pedido_manobra'].'">');
                            echo('<div class="col-md-3"><label>Solicitado Por</label><input type="text" class="form-control" disabled value="'.$value['solicitado_por_desl'].' - '.$selectSolicitanteDesl.'"></div>');
                            echo('<div class="col-md-3"><label>Atendido Por</label><input type="text" class="form-control" disabled value="'.$value['atendido_por_desl'].'"></div>');
                            echo('</div>');
                            echo('<div class="row">');
                            echo('<div class="col-md-2"><label>Data/Hora Deslig.</label><input type="text" class="form-control" disabled value="'.$value['data_lig'].' '.$value['hora_lig'].'"></div>');
                            echo('<div class="col-md-2"><label>P.M. Ligam.</label><input type="text" class="form-control" disabled value="'.$value['pedido_manobra'].'"></div>');
                            echo('<div class="col-md-3"><label>Solicitado Por</label><input type="text" class="form-control" disabled value="'.$value['solicitado_por_lig'].' - '.$selectSolicitanteLig.'"></div>');
                            echo('<div class="col-md-3"><label>Atendido Por</label><input type="text" class="form-control" disabled value="'.$value['atendido_por_lig'].'"></div>');
                            echo('<div class="col-md-2"><label>Excluir</label><input type="checkbox" class="checkbox" name="retirarManobra'.$contadorManobra.'"></div>');
                            echo('</div>');
                        }

                        ?>
                        <input type="hidden" value="<?php echo($contadorManobra); ?>" name="contadorManobra">

                    </div>
                </div>
            </div>

            <div class="row">

                <?php
                $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                ?>
                <div class="col-lg-offset-5 col-lg-7">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Complementos</div>
                        <div class="panel-body">
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/dashboardPmp/observarOsm">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                            <i class="fa fa-home fa-2x"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="btn-group" >
                                    <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                        <i class="fa fa-bolt fa-2x"></i>
                                    </button>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/moduloPmp">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                            <i class="fa fa-users fa-2x"></i> </button>
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/moduloPmp/tempoTotal">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                            <i class="fa fa-clock-o fa-2x"></i> </button>
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                            <i class="fa fa-crosshairs fa-2x"></i> </button>
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/moduloPmp/maquinaUtilizada">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                            <i class="fa fa-truck fa-2x"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/moduloPmp/materialUtilizado/">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                            <i class="fa fa-wrench fa-2x"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo HOME_URI; ?>/moduloPmp/encerramento">
                                        <button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                            <i class="fa fa-power-off fa-2x"></i> </button>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>