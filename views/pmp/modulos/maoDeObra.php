<div class="page-header">
    <h2>M�o de Obra</h2>
</div>

<div class="row">
    <form method="POST" class="form-group">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Ordem de Servi�o da Manuten��o - M�o de Obra Utilizada</div>

                <div class="panel-body">



                    <div class="panel panel-default">
                        <div class="panel-heading">M�o de Obra Utilizada</div>
                        <div class="panel-body">


                                <div id="maoObraOsm">

                                    <?php
                                    $maoObraFuncionario = $this->medoo->select("osm_mao_de_obra", "*",["cod_osm" => (int) $_SESSION['refillOsm']['codigoOsm']]);
                                    $contador = 0;
                                    foreach ($maoObraFuncionario as $dados=>$value) {
                                        $contador += 1;
                                        echo('<div class="row">');
                                        echo('<div class="col-md-2"> <label>Matr�cula</label> <input type="text" class="form-control" disabled value="'.$value['cod_funcionario'].'"></div>');
                                        echo('<input type="hidden" class="form-control" value="'.$value['cod_funcionario'].'" name="matriculaFuncionarioMaoObraOsm'.$contador.'">');
                                        $funcionario = $this->medoo->select("funcionario", "nome_funcionario", ["cod_funcionario" => (int) $value['cod_funcionario']]);
                                        echo('<div class="col-md-4"><label>Nome</label> <input type="text" class="form-control" disabled value="'.$funcionario[0].'"></div>');
                                        echo('<div class="col-md-2"><label>In�cio</label> <input type="text" class="form-control" disabled value="'.$value['data_inicio'].'"></div>');
                                        echo('<div class="col-md-2"><label>T�rmino</label> <input type="text" class="form-control" disabled value="'.$value['data_termino'].'"></div>');
                                        echo('<div class="col-md-1"><label>Excluir</label> <input type="checkbox" class="checkbox" name="retirarFuncionario'.$contador.'"></div>');
                                        /*echo('<div class="col-md-1"><label>Editar</label><div class="btn-group-lg"><div class="btn-group">');
                                        echo('<button class="btn btn-circle btn-default" type="button" data-action="'.$funcionario[0].'" data-toggle="modal" data-target="#editarMaoObraModal">'.
                                            '<i class="fa fa-edit fa-fw"></i>'.
                                            '</button></div></div></div>');*/
                                        echo('</div>');
                                    }

                                    echo('<input type="hidden" value="'.$contador.'" name="contadorMaoObra">');

                                    ?>

                                    <div class="row" style="border-bottom: 1px solid #000000; padding-top: 1em"></div>
                                </div>
                        </div>
                    </div>

    </form>

                    <div class="row">

                        <?php
                        $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                        $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        ?>
                        <div class="col-lg-offset-5 col-lg-7">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Complementos</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/dashboardPmp/observarOsm">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                                    <i class="fa fa-home fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group" >
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/manobrasEletricas">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                                    <i class="fa fa-bolt fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                                <i class="fa fa-users fa-2x"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/tempoTotal">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                                    <i class="fa fa-clock-o fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                                    <i class="fa fa-crosshairs fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/maquinaUtilizada">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                                    <i class="fa fa-truck fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/materialUtilizado/">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                                    <i class="fa fa-wrench fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/encerramento">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                                    <i class="fa fa-power-off fa-2x"></i> </button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>