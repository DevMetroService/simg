<div class="page-header">
	<h1>Ordem de Servi�o de Manuten��o</h1>
</div>
<?php
$selectTempo = $this->medoo->select('osm_tempo','*',["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
$selectTempo = $selectTempo[0];

?>

<div class="row">

	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Tempos Totais</div>
				
			<div class="panel-body">
				<form class="form-group">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">Recebimento pelo T�cnico</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Data/Hora</label>
                                            <?php
                                            if(!empty($selectTempo)){
                                                echo('<input disabled="disabled" value="'.$this->parse_timestamp($selectTempo['data_recebimento']).'" class="form-control timeStamp">');
                                            }else{
                                                $selectStatus = $this->medoo->select('status_osm','data_status',[
                                                    'AND' =>[
                                                        'cod_status'    => 10,
                                                        'cod_osm'       => (int) $_SESSION['refillOsm']['codigoOsm']
                                                    ]
                                                ]);
                                                $selectStatus = $selectStatus[0];

                                                echo('<input disabled="disabled" value="'.$this->parse_timestamp($selectStatus).'" class="form-control timeStamp">');
                                                echo('<input  type="hidden" name="dataRecebimento" value="'.$this->parse_timestamp($selectStatus).'" class="form-control timeStamp">');

                                            }
                                            ?>

                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select name="atrasoRecebimento" class="form-control">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_recebimento']);
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-offset-5 col-md-2">
                                            <label>C�digo Osm</label>
                                            <input type="text" disabled class="form-control" value="<?php echo($_SESSION['refillOsm']['codigoOsm']);?>">
                                            <input type="hidden" name="codigoOsm" class="form-control" value="<?php echo($_SESSION['refillOsm']['codigoOsm']);?>">
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">Prepara��o e Acesso</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>In�cio</label>
                                            <input class="form-control timeStamp" required name="preparacaoInicio" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_preparacao']) );?>">
                                        </div>

                                        <div class="col-md-2">
                                            <label>Pronto para Sair</label>
                                            <input class="form-control timeStamp" required name="acessoProntoSair" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_ace_pronto']) );?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Sa�da Autorizada</label>
                                            <input class="form-control timeStamp" required name="acessoSaidaAutorizada" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_ace_saida_autorizada']) );?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Sa�da</label>
                                            <input class="form-control timeStamp" required name="acessoSaida" value="<?php  if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_ace_saida']));?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Chegada</label>
                                            <input class="form-control timeStamp" name="acessoChegada" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_ace_chegada']));?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoIniPreparacao">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_preparacao']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoAcessoProntoSair">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_ace_pronto']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoAcessoSaida">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_ace_saida']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoAcessoChegada">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_ace_chegada']);
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
					</div>

					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Execu��o</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>In�cio</label>
                                            <input class="form-control timeStamp" required name="execucaoInicio" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_exec_inicio']) );?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoExecInicio">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_exec_inicio']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-2">
                                            <label>T�rmino</label>
                                            <input class="form-control timeStamp" required name="execucaoTermino" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_exec_termino']) );?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoExecTermino">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_exec_termino']);
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
					</div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Regresso</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Pronto para Sair</label>
                                            <input class="form-control timeStamp" required name="regressoProntoSair" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_reg_pronto']) );?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Sa�da Autorizada</label>
                                            <input class="form-control timeStamp" required name="regressoSaidaAutorizada" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_reg_saida_autorizada'] ));?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Chegada</label>
                                            <input class="form-control timeStamp" required name="regressoChegada" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_reg_chegada'] ));?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Desmobiliza��o</label>
                                            <input class="form-control timeStamp" required name="regressoDesmobilizacao" value="<?php if(!empty($selectTempo)) echo($this->parse_timestamp($selectTempo['data_desmobilizacao']));?>">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoRegressoProntoSair">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_reg_pronto']);
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-md-offset-2">
                                            <label>Atraso</label>
                                            <select class="form-control" name="atrasoRegressoChegada">
                                                <?php
                                                $this->imprimirOption($selectTempo['atraso_reg_chegada']);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="row">
						<div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Observa��o</div>

                                <div class="panel-body">
                                    <label>Observa��o</label>
                                    <textarea rows="3" cols="110" class="form-control" name="descricaoTempo"><?php echo($selectTempo['descricao']);?></textarea>
                                </div>
                            </div>
						</div>
					</div>

                    <div class="row">
                        <?php
                        $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                        $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        ?>
                        <div class="col-lg-offset-5 col-lg-7">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Complementos</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/dashboardPmp/observarOsm">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                                    <i class="fa fa-home fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group" >
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/manobrasEletricas">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                                    <i class="fa fa-bolt fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                                    <i class="fa fa-users fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                                <i class="fa fa-clock-o fa-2x"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                                    <i class="fa fa-crosshairs fa-2x"></i> </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/maquinaUtilizada">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                                    <i class="fa fa-truck fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/materialUtilizado/">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                                    <i class="fa fa-wrench fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/encerramento">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                                    <i class="fa fa-power-off fa-2x"></i> </button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</form>
			</div>

		</div>
	</div>
</div>

<?php
?>