<div class="page-header">
	<h1>Ordem de Servi�o de Manuten��o - Falha</h1>
</div>
<?php
$osm = $this->medoo->select("osm_falha", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
$osm = $osm[0];

$ssm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$osm['cod_ssm']]);
$ssm = $ssm[0];

$encerramento = $this->medoo->select("osm_encerramento",["[>]pendencia" => "cod_pendencia"], "*", ["cod_osm" => $osm['cod_osm']]);
$encerramento = $encerramento[0];

$unEquipe = $this->medoo->select("un_equipe",[
    "[><]equipe" => "cod_equipe",
    "[><]unidade" => "cod_unidade"
    ], "*" ,[
    'cod_un_equipe' => $encerramento['cod_un_equipe']
    ]
);
//echo(var_dump($unEquipe));
$unEquipe = $unEquipe[0];

$tempo = $this->medoo->select("status_osm", "data_status",
    [
        "AND" => [
            "cod_osm"       => $osm['cod_osm'],
            "cod_status"    => 10
        ]
    ]);
$tempo = $tempo[0];

//Informa��es relacionadas ao m�dulo de Registro de Execu��o
$unidadeEquipe = $this->medoo->select("osm_registro",[
    "[><]un_equipe" => "cod_un_equipe",
    "[><]unidade"   => "cod_unidade",
    "[><]equipe"    => "cod_equipe"
],[
    "equipe.sigla",
    "unidade.nome_unidade"
],[
    "osm_registro.cod_osm" => $osm['cod_osm']
]);
$unidadeEquipe = $unidadeEquipe[0];


?>
<div class="row">
	<form class="form-group" method="post">
		<div class="panel panel-primary">
			<div class="panel-heading"><label>Encerramento</label></div>
			
			<div class="panel-body">
				<div class="row">	
					<div class="col-md-12">
					
						<div class="panel panel-default">
							<div class="panel-heading">
								<label>Relat�rios</label>
							</div>
							<div class="panel-body">
								<div class="row">
                                    <div class="col-md-1">
                                        <label for="equipe">Equipe</label>
                                        <?php
                                        if(!empty($osm)){
                                            echo ('<input type="text" disabled class="form-control" value="'.$ssm['sigla_equipe'].'">');
                                        }else{
                                            echo ('<input type="text" class="form-control" value="" disabled>');
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="equipe">Grupo / Sistema Afetado (Falha) </label>
                                        <?php
                                        if(!empty($osm)){
                                            echo('<input type="text" disabled id="equipe" class="form-control" value="'.$ssm['grupo_sistema']
                                                .$ssm['sigla_sistema']." / ".$ssm['nome_grupo']." - ".$ssm['nome_sistema'].'"/>');
                                        }else{
                                            echo('<input type="text" disabled id="equipe" class="form-control" value=""/>');
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-3">
                                        <label for="dataHora">Data / Hora</label>
                                        <input type="text" disabled id="dataHora" value="<?php echo $this->parse_timestamp($tempo); ?>" class="form-control">
                                    </div>

                                    <div class="col-md-2">
                                        <label for="numeroOsm">N�</label>
                                        <?php
                                        if(!empty($osm)){
                                            echo('<input type="text" id="numeroOsm" disabled class="form-control" value="'.$osm['cod_osm'].'">');
                                            echo('<input type="hidden" name="codigoOsm" class="form-control" value="'.$osm['cod_osm'].'">');
                                        }else{
                                            echo('<input type="text" id="numeroOsm" disabled class="form-control" >');
                                        }
                                        ?>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading"><label>Fechamento</label></div>
							
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<label>Equipe / Local</label>
										<input type="text" disabled class="form-control" value="<?php echo($unidadeEquipe['sigla']." - ".$unidadeEquipe['nome_unidade']);?>">
									</div>
									<div class="col-md-2">
										<label>Data / Hora</label>
										<input name="dataHoraFechamento" required="required" type="text" class="form-control timeStamp" value="<?php if(!empty($encerramento)){ echo($this->parse_timestamp($encerramento['data_encerramento']) );} ?>">
									</div>
									<div class="col-md-4">
										<label>Descri��o tipo do fechamento</label>
										<select name="descricaoTipoFechamento" class="form-control">
                                            <?php
                                            $selectTipoFechamento = $this->medoo->select("tipo_fechamento","*");

                                            foreach ($selectTipoFechamento as $dados => $value){
                                                if(!empty($encerramento)){
                                                    if($encerramento['cod_tipo_fechamento'] == $value['cod_tipo_fechamento']){
                                                        echo('<option value="'.$value['cod_tipo_fechamento'].'" selected>'.$value['nome_fechamento'].'</option>');
                                                    }else{
                                                        echo('<option value="'.$value['cod_tipo_fechamento'].'">'.$value['nome_fechamento'].'</option>');
                                                    }
                                                }else{
                                                    echo('<option value="'.$value['cod_tipo_fechamento'].'">'.$value['nome_fechamento'].'</option>');
                                                }
                                            }
                                            ?>
										</select>
									</div>
								</div>
								
								<div class="row" id="comPendencia" style="display:none">
									<div class="col-md-1">
										<label>N�</label>
										<input name="numeroTipoPendenciaFechamento" disabled type="text" class="form-control">
									</div>
									<div class="col-md-5">
										<label>Tipo Pend�ncia</label>
										<select name="tipoPendenciaFechamento" class="form-control">
                                            <?php
                                            $selectTipoPendencia = $this->medoo->select("tipo_pendencia", "*");

                                            echo("<option disabled selected>Selecione o tipo de pend�ncia</option>");

                                            foreach($selectTipoPendencia as $dados => $value){
                                                if(!empty($encerramento)){
                                                    if($encerramento['cod_tipo_pendencia'] == $value['cod_tipo_pendencia'] ){
                                                        echo('<option value="'.$value['cod_tipo_pendencia'].'" selected>'.$value['nome_pendencia'].'</option>');
                                                        $tipoPendencia = $value['cod_tipo_pendencia'];
                                                    }else{
                                                        echo('<option value="'.$value['cod_tipo_pendencia'].'">'.$value['nome_pendencia'].'</option>');
                                                    }
                                                }else{
                                                    echo('<option value="'.$value['cod_tipo_pendencia'].'">'.$value['nome_pendencia'].'</option>');
                                                }
                                            }

                                            ?>
										</select>
									</div>
									<div class="col-md-1">
										<label>N�</label>
										<input name="numeroPendenciaFechamento" disabled type="text" class="form-control">
									</div>
									<div class="col-md-5">
										<label>Pend�ncia</label>
										<select name="pendenciaFechamento" class="form-control">
                                            <?php
                                            if(!empty($encerramento)){

                                                $selectPendencia = $this->medoo->select("pendencia", "*", ["cod_tipo_pendencia" => $encerramento['cod_tipo_pendencia']]);

                                                echo("<option disabled selected>Selecione a pend�ncia</option>");

                                                foreach($selectPendencia as $dados => $value){
                                                    if($encerramento['cod_pendencia'] == $value['cod_pendencia'] ){
                                                        echo('<option value="'.$value['cod_pendencia'].'" selected>'.$value['nome_pendencia'].'</option>');
                                                    }else{
                                                        echo('<option value="'.$value['cod_pendencia'].'">'.$value['nome_pendencia'].'</option>');
                                                    }
                                                }
                                            }
                                            ?>
										</select>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-2">
										<label>Libera��o</label>
										<select name="liberacaoTrafego" class="form-control">
                                            <?php
                                            if(!empty($encerramento)){
                                                if($encerramento['liberacao']=="n"){
                                                    echo('<option value="n" selected>N�o</option>');
                                                    echo('<option value="s">Sim</option>');
                                                }else{
                                                    echo('<option value="n">N�o</option>');
                                                    echo('<option value="s" selected>Sim</option>');
                                                }
                                            }else{
                                                echo('<option value="n">N�o</option>');
                                                echo('<option value="s">Sim</option>');
                                            }
                                            ?>
                                        </select>
									</div>
									<div class="col-md-2">
										<label>Transfer�ncia</label>
										<select name="transferenciaAtividadeOsm" class="form-control">
                                            <?php
                                            if(!empty($encerramento)){
                                                if($encerramento['transferida']=="n"){
                                                    echo('<option value="n" selected>N�o</option>');
                                                    echo('<option value="s">Sim</option>');
                                                }else{
                                                    echo('<option value="n">N�o</option>');
                                                    echo('<option value="s" selected>Sim</option>');
                                                }
                                            }else{
                                                echo('<option value="n">N�o</option>');
                                                echo('<option value="s">Sim</option>');
                                            }
                                            ?>
                                        </select>
									</div>
									<div class="col-md-2">
										<label>Matr�cula</label>
										<input name="matriculaResponsavelInformacoesOsm" required="required" type="text" class="form-control" value="<?php if(!empty($encerramento)){echo($encerramento['cod_funcionario']); } ?>">
									</div>
									<div class="col-md-6">
										<label>Respons�vel pelas informa��es</label>
										<input name="responsavelInformacaoOsm" type="text" disabled="disabled" class="form-control">
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				
				<div class="row" id="dadosTransferencia" style="display: none">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading"><label>Continuidade</label></div>
						
							<div class="panel-body">
								<div class="row">
									<div class="col-md-1">
										<label>N�</label>
										<input name="numeroEquipeContinuidade" disabled type="text" class="form-control"/>
									</div>
									<div class="col-md-5">
										<label>Equipe</label>
										<select name="equipeContinuidade" class="form-control" >
                                        <?php
                                        if(!empty($encerramento['cod_un_equipe'])){
                                            $equipe = $this->medoo->select('un_equipe', ['[><]equipe' => 'cod_equipe'], '*', ['cod_unidade' => $unEquipe['cod_unidade']]);

                                            echo ('<option disabled selected>Selecione a Equipe</option>');

                                            foreach($equipe as $dados => $value){
                                                if($unEquipe['cod_equipe'] == $value['cod_equipe']){
                                                    echo ('<option selected value="'.$value['cod_equipe'].'">'.$value['nome_equipe'].'</option>');
                                                }else{
                                                    echo ('<option value="'.$value['cod_equipe'].'">'.$value['nome_equipe'].'</option>');
                                                }
                                            }
                                        }
                                        ?>
										</select>
									</div>
									<div class="col-md-1">
										<label>N�</label>
										<input name="numeroUnidadeContinuidade" type="text" disabled class="form-control" />
									</div>
									<div class="col-md-5">
										<label>Local</label>
                                        <select name="unidadeContinuidade" class="form-control">
                                        <?php
                                        $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"], ["ORDER" => "cod_unidade"]);
                                        $unidade = array_combine(range(1,count($unidade)), $unidade);

                                        echo ('<option disabled selected>Selecione o Local</option>');

                                        foreach($unidade as $dados =>$value){
                                            if(!empty($encerramento['cod_un_equipe'])){
                                                if($unEquipe['cod_unidade'] == $value['cod_unidade'] ){
                                                    echo('<option value="'.$value['cod_unidade'].'" selected>'.$value['nome_unidade'].'</option>');
                                                }else{
                                                    echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                                                }
                                            }else{
                                                echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                                            }
                                        }
                                        ?>
                                        </select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<label>Recomenda��es</label>
										<textarea name="recomendacoesContinuidade" class="form-control"><?php if(!empty($encerramento)) echo($encerramento['recomendacao']);?></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

                <div class="row">


                    <?php
                    $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                    $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                    $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                    ?>
                    <div class="col-lg-offset-5 col-lg-7">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Complementos</div>
                            <div class="panel-body">
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group">
                                        <a href="<?php echo HOME_URI; ?>/dashboardPmp/observarOsm">
                                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                                <i class="fa fa-home fa-2x"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="btn-group" >
                                        <a href="<?php echo HOME_URI; ?>/moduloPmp/manobrasEletricas">
                                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                                <i class="fa fa-bolt fa-2x"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="<?php echo HOME_URI; ?>/moduloPmp">
                                            <button type="button" class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                                <i class="fa fa-users fa-2x"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                       <a href="<?php echo HOME_URI; ?>/moduloPmp/tempoTotal">
                                           <button type="button" class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                               <i class="fa fa-clock-o fa-2x"></i>
                                           </button>
                                       </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                                <i class="fa fa-crosshairs fa-2x"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="<?php echo HOME_URI; ?>/moduloPmp/maquinaUtilizada">
                                            <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                                <i class="fa fa-truck fa-2x"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="<?php echo HOME_URI; ?>/moduloPmp/materialUtilizado/">
                                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                                <i class="fa fa-wrench fa-2x"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                            <i class="fa fa-power-off fa-2x"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
			</div>
		</div>
		
	</form>
</div>