<div class="page-header">
    <h2>M�quinas Utilizadas</h2>
</div>

<div class="row">
    <form class="form-group">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Ordem de Servi�o e Manuten��o - M�quinas e Equipamentos Utilizados</div>

                <div class="panel-body">



                    <div class="panel panel-default">
                        <div class="panel-heading">M�quinas e Equipamentos Cadastrados</div>
                        <div class="panel-body">
                            <div id="maquinaUtilizada">
                                <?php

                                $maquinaUtilizada = $this->medoo->select("osm_maquina", "*", [ "cod_osm" => $_SESSION['refillOsm']['codigoOsm']]);
                                $contador = 0;

                                foreach($maquinaUtilizada as $dados=>$value){
                                    $contador = $contador +1;
                                    echo('<div class="row">');
                                    echo('<div class="col-md-1"><label>C�digo</label><input type="text" disabled value="'.$value['cod_material'].'" class="form-control"></div>');
                                    echo('<input type="hidden" name="codigoMaquina'.$contador.'" value="'.$value['cod_material'].'">');
                                    $selectMaquina = $this->medoo->select("v_material", "*", ["cod_material" => (int) $value['cod_material'] ]);
                                    $selectMaquina = $selectMaquina[0];
                                    echo('<div class="col-md-4"><label>Nome</label><input type="text" disabled class="form-control" value="'.$selectMaquina['nome_material'].' '.$selectMaquina['nome_marca'].'"></div>');
                                    echo('<div class="col-md-3"><label>N� S�rie</label><input type="text" disabled class="form-control" value="'.$value['num_serie'].'"></div>');
                                    echo('<input type="hidden" name="numeroSerieMaquina'.$contador.'" value="'.$value['num_serie'].'">');
                                    echo('<div class="col-md-1"><label>Qtd.</label><input type="text" name="qtdMaquina'.$contador.'" class="form-control" value="'.$value['qtd'].'"></div>');
                                    echo('<div class="col-md-2"><label>Total de Horas</label><input type="text" name="totalHorasMaquina'.$contador.'" class="form-control hora" value="'.$value['total_hrs'].'"></div>');
                                    echo('<div class="col-md-1"><label>Excluir</label> <input type="checkbox" class="checkbox" name="retirarMaquina'.$contador.'"></div>');
                                    echo("</div>");
                                    echo('<div class="row">');
                                    echo('<div class="col-md-12"><label>Descri��o</label><input class="form-control" type="text" name="descricaoMaquina'.$contador.'" value="'.$value['descricao'].'"></div>');
                                    echo("</div>");

                                }

                                echo('<input type="hidden" value="'.$contador.'" name="contadorMaquina">');

                                ?>

                                <div class="row" style="border-bottom: 1px solid #000000; padding-top: 1em"></div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <?php
                        $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                        $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        ?>
                        <div class="col-lg-offset-5 col-lg-7">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Complementos</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/dashboardPmp/observarOsm">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                                    <i class="fa fa-home fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group" >
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/manobrasEletricas">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                                    <i class="fa fa-bolt fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                                    <i class="fa fa-users fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/tempoTotal">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                                    <i class="fa fa-clock-o fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                                    <i class="fa fa-crosshairs fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                                <i class="fa fa-truck fa-2x"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/materialUtilizado/">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                                    <i class="fa fa-wrench fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/encerramento">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                                    <i class="fa fa-power-off fa-2x"></i> </button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>