<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28/10/2015
 * Time: 11:55
 */
?>

<div class="page-header">
    <h2>Registro de Execu��o</h2>
</div>

<?php
$registro = $this->medoo->select("osm_registro","*",["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
$registro = $registro[0];

$unEquipe = $this->medoo->select("un_equipe", ['[><]equipe' => 'cod_equipe'],"*", ["cod_un_equipe" => (int)$registro['cod_un_equipe']]);
$unEquipe = $unEquipe[0];

?>

<div class="row">
<form method="post" class="form-group" action="<?php echo(HOME_URI)?>/modulosupervisao/salvarRegistroExecucao">
<div class="col-md-12">
<div class="panel panel-primary">
<div class="panel-heading"><label>Execu��o</label></div>

<div class="panel-body">
<div class="panel panel-default">
<div class="panel-body">
<div class="row">
    <div class="col-md-1">
        <label>N�</label>
        <input name="codUnidadeExecucao" disabled type="text" class="form-control">
    </div>
    <div class="col-md-4">
        <label>Local</label>
        <select name="unidadeExecucao" class="form-control" required>
            <?php
            $unidade = $this->medoo->select("unidade", ["cod_unidade", "nome_unidade"], ["ORDER" => "cod_unidade"]);
            $unidade = array_combine(range(1,count($unidade)), $unidade);

            echo ('<option disabled selected>Selecione o Local</option>');

            foreach($unidade as $dados =>$value){
                if(!empty($registro)){
                    if($unEquipe['cod_unidade'] == $value['cod_unidade'] ){
                        echo('<option value="'.$value['cod_unidade'].'" selected>'.$value['nome_unidade'].'</option>');
                    }else{
                        echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                    }
                }else{
                    echo('<option value="'.$value['cod_unidade'].'">'.$value['nome_unidade'].'</option>');
                }
            }
            ?>
        </select>
    </div>

    <div class="col-md-1">
        <label>N�</label>
        <input name="codEquipeExecucao" disabled type="text"
               value="<?php echo (!empty($registro) )? $unEquipe['sigla']:$ss['sigla']; ?>" class="form-control">
    </div>

    <div class="col-md-4">
        <label>Equipe</label>
        <select name="equipeExecucao" class="form-control" required>
            <?php
            $equipe = $this->medoo->select("equipe", "*",["ORDER" => "cod_equipe"]);
            $equipe = array_combine(range(1,count($equipe)), $equipe);

            echo ('<option disabled selected>Selecione a Equipe</option>');

            foreach($equipe as $dados => $value){
                if(!empty($registro)){
                    if($unEquipe['cod_equipe'] == $value['cod_equipe']){
                        echo ('<option selected value="'.$value['cod_equipe'].'">'.$value['nome_equipe'].'</option>');
                    }else{
                        echo ('<option value="'.$value['cod_equipe'].'">'.$value['nome_equipe'].'</option>');
                    }
                }else{
                    echo ('<option value="'.$value['cod_equipe'].'">'.$value['nome_equipe'].'</option>');
                }
            }
            ?>
        </select>
    </div>
    <div class="col-md-2">
        <label>Cod. OSM</label>
        <input type="text" disabled class="form-control" value="<?php echo($_SESSION['refillOsm']['codigoOsm']);?>">
        <input type="hidden" name="codOsm" class="form-control" value="<?php echo($_SESSION['refillOsm']['codigoOsm']);?>">
    </div>
</div>

<div class="row" style="padding-top: 2em;">
    <div class="col-md-1">
        <label>Cod.</label>
        <input name="codCausa" class="form-control" type="text">
    </div>
    <div class="col-md-5">
        <label>Descri��o da Causa</label>
        <select name="causaAvaria" class="form-control">
            <?php
            $selectCausa = $this->medoo->select("causa", ["cod_causa", "nome_causa"], ["ORDER" => "cod_causa"]);

            foreach($selectCausa as $dados => $value){
                if(!empty($registro)){
                    if($registro['cod_causa'] == $value['cod_causa']){
                        echo('<option value="'.$value['cod_causa'].'" selected>'.$value['nome_causa'].'</option>');
                    }else{
                        echo('<option value="'.$value['cod_causa'].'">'.$value['nome_causa'].'</option>');
                    }
                }else{
                    echo('<option value="'.$value['cod_causa'].'">'.$value['nome_causa'].'</option>');
                }
            }
            ?>
        </select>
    </div>
    <div class="col-md-1">
        <label>Cod.</label>
        <input name="codAtuacao" class="form-control" type="text">
    </div>
    <div class="col-md-5">
        <label>Descri��o da Atua��o</label>
        <select name="atuacaoExecucao" class="form-control">
            <?php
            $selectAtuacao = $this->medoo->select("atuacao", "*", ["ORDER" => "cod_atuacao"] );

            foreach ($selectAtuacao as $dados => $value){
                if(!empty($registro)){
                    if($registro['cod_atuacao'] == $value['cod_atuacao']){
                        echo('<option value="'.$value['cod_atuacao'].'" selected>'.$value['nome_atuacao'].'</option>');
                    }else{
                        echo('<option value="'.$value['cod_atuacao'].'">'.$value['nome_atuacao'].'</option>');
                    }
                }else{
                    echo('<option value="'.$value['cod_atuacao'].'">'.$value['nome_atuacao'].'</option>');
                }
            }
            ?>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label>Observa��o da Causa</label>
        <textarea name="obsCausa" class="form-control"><?php if(!empty($registro['desc_causa'])){echo($registro['desc_causa']);}?></textarea>
    </div>
    <div class="col-md-6">
        <label>Observa��o da Atua��o</label>
        <textarea name="obsAtuacao" class="form-control"><?php if(!empty($registro['desc_atuacao'])){echo($registro['desc_atuacao']);}?></textarea>
    </div>
</div>

<div class="row">
    <div class="col-md-1">
        <label>Cod.</label>
        <input name="codAgCausador" class="form-control" type="text">
    </div>
    <div class="col-md-4">
        <label>Ag. Causador</label>
        <select class="form-control" name="agCausador">
            <?php
            $agCausador = $this->medoo->select("agente_causador","*",["ORDER"=>"cod_ag_causador"]);

            foreach($agCausador as $dados => $value){
                if(!empty($registro)){
                    if($registro['cod_ag_causador'] == $value['cod_ag_causador']){
                        echo('<option value="'.$value['cod_ag_causador'].'" selected>'.$value['nome_agente'].'</option>');
                    }else{
                        echo('<option value="'.$value['cod_ag_causador'].'">'.$value['nome_agente'].'</option>');
                    }
                }else{
                    echo('<option value="'.$value['cod_ag_causador'].'">'.$value['nome_agente'].'</option>');
                }
            }
            ?>
        </select>
    </div>
    <div class="col-md-3">
        <label>Clima</label>
        <select name="climaExecucao" class="form-control">
            <?php
            if(!empty($registro)){
                switch($registro['clima']){
                    case "b":
                        echo('<option value="b" selected>Bom</option>');
                        echo('<option value="c">Chuvoso</option>');
                        echo('<option value="n">Nublado</option>');
                        echo('<option value="e">Neblina</option>');
                        break;

                    case "c":
                        echo('<option value="b">Bom</option>');
                        echo('<option value="c" selected>Chuvoso</option>');
                        echo('<option value="n">Nublado</option>');
                        echo('<option value="e">Neblina</option>');
                        break;

                    case "n":
                        echo('<option value="b">Bom</option>');
                        echo('<option value="c">Chuvoso</option>');
                        echo('<option value="n" selected>Nublado</option>');
                        echo('<option value="e">Neblina</option>');
                        break;

                    case "e":
                        echo('<option value="b">Bom</option>');
                        echo('<option value="c">Chuvoso</option>');
                        echo('<option value="n">Nublado</option>');
                        echo('<option value="e" selected>Neblina</option>');
                        break;
                }

            }else{
                echo('<option value="b">Bom</option>');
                echo('<option value="c">Chuvoso</option>');
                echo('<option value="n">Nublado</option>');
                echo('<option value="e">Neblina</option>');
            }

            ?>
        </select>
    </div>
    <div class="col-md-2">
        <label>Temperatura</label>
        <input type="text" name="temperaturaExecucao" value="<?php if(!empty($registro)){echo($registro['temperatura']);}?>" class="form-control">
    </div>
    <div class="col-md-1">
        <label>Km Inicial</label>
        <input type="text" name="kmIniExecucao" value="<?php if(!empty($registro)){echo($registro['km_inicial']);}?>" class="form-control">
    </div>
    <div class="col-md-1">
        <label>Km Final</label>
        <input type="text" name="kmFimExecucao" value="<?php if(!empty($registro)){echo($registro['km_final']);}?>" class="form-control">
    </div>
</div>

<div class="row">
    <div style="display: none">
        <div class="col-md-2">
            <label>N� Locomotiva</label>
        </div>
        <div class="col-md-2">
            <label>Prefixo</label>
        </div>
    </div>
    <div class="col-md-3">
        <label>Transporte Utilizado</label>
        <select name="transporteExecucao" class="form-control">
            <?php
            $selectVeiculo = $this->medoo->select("automovel", "*", ["ORDER" => "cod_automovel"]);

            foreach ($selectVeiculo as $dados => $value){
                if(!empty($registro)){
                    if($registro['cod_automovel'] == $value['cod_automovel']){
                        echo('<option value="'.$value['cod_automovel'].'" selected>'.$value['nome_automovel'].' - '.$value['numero_automovel'].'</option>');
                    }else{
                        echo('<option value="'.$value['cod_automovel'].'">'.$value['nome_automovel'].' - '.$value['numero_automovel'].'</option>');
                    }
                }else{
                    echo('<option value="'.$value['cod_automovel'].'">'.$value['nome_automovel'].' - '.$value['numero_automovel'].'</option>');
                }
            }
            ?>
        </select>
    </div>
    <div class="col-md-1">
        <label>Cautela</label>
        <?php
        if(!empty($registro)){
            if($registro['cautela'] == "s"){
                echo('<input type="checkbox" checked="checked" class="checkbox" name="cautelaExecucao">');
            }else{
                echo('<input type="checkbox" class="checkbox" name="cautelaExecucao">');
            }
        }else{
            echo('<input type="checkbox" class="checkbox" name="cautelaExecucao">');
        }
        ?>
    </div>
    <div id="cautelaRegistro" style="display: none">
        <div class="col-md-1">
            <label>Km Inicial</label>
            <?php
            if(!empty($registro)){
                echo('<input type="text" name="kmIniCautela" value="'.$registro['km_inicial_cautela'].'" class="form-control">');
            }else{
                echo('<input type="text" name="kmIniCautela" value="" class="form-control">');
            }
            ?>
        </div>
        <div class="col-md-1">
            <label>Km Final</label>
            <?php
            if(!empty($registro)){
                echo('<input type="text" name="kmFimCautela" value="'.$registro['km_final_cautela'].'" class="form-control">');
            }else{
                echo('<input type="text" name="kmFimCautela" class="form-control">');
            }
            ?>
        </div>
        <div class="col-md-2">
            <label>Restringir Velocidade</label>
            <?php
            if(!empty($registro)){
                echo('<input type="text" name="restringirVelocidadeExecucao" value="'.$registro['restricao_veloc'].'" class="form-control">');
            }else{
                echo('<input type="text" name="restringirVelocidadeExecucao" class="form-control">');
            }
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Dados Complementares</label>
        <textarea name="dadosComplementares" class="form-control"><?php if(!empty($registro)){echo($registro['dados_complementar']);}?></textarea>
    </div>
</div>
</div>
</div>

<div class="row">
    <?php
    $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
    $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
    $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
    ?>
    <div class="col-lg-offset-5 col-lg-7">
        <div class="panel panel-primary">
            <div class="panel-heading">Complementos</div>
            <div class="panel-body">
                <div class="btn-group btn-group-justified" role="group">
                    <div class="btn-group">
                        <a href="<?php echo HOME_URI; ?>/dashboardSupervisao/observarOsm">
                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                <i class="fa fa-home fa-2x"></i>
                            </button>
                        </a>
                    </div>
                    <div class="btn-group" >
                        <a href="<?php echo HOME_URI; ?>/modulosupervisao/manobrasEletricas">
                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                <i class="fa fa-bolt fa-2x"></i>
                            </button>
                        </a>
                    </div>
                    <div class="btn-group">
                        <?php
                        if(!empty($maoObra)){
                            echo('<a href="'.HOME_URI.'/modulosupervisao">');
                            echo('<button type="button" class="btn btn-success" aria-label="Left Align" title="M�o de Obra">');
                            echo('<i class="fa fa-users fa-2x"></i> </button>');
                            echo('</a>');
                        }else{
                            echo('<a href="'.HOME_URI.'/modulosupervisao">');
                            echo('<button type="button" class="btn btn-danger" aria-label="Left Align" title="M�o de Obra">');
                            echo('<i class="fa fa-users fa-2x"></i> </button>');
                            echo('</a>');
                        }
                        ?>
                    </div>
                    <div class="btn-group">
                        <?php
                        if(!empty($tempo)){
                            echo('<a href="'.HOME_URI.'/modulosupervisao/tempoTotal">');
                            echo('<button type="button" class="btn btn-success" aria-label="Left Align" title="Tempos Totais">');
                            echo('<i class="fa fa-clock-o fa-2x"></i> </button>');
                            echo('</a>');
                        }else{
                            echo('<a href="'.HOME_URI.'/modulosupervisao/tempoTotal">');
                            echo('<button type="button" class="btn btn-danger" aria-label="Left Align" title="Tempos Totais">');
                            echo('<i class="fa fa-clock-o fa-2x"></i> </button>');
                            echo('</a>');
                        }
                        ?>
                    </div>
                    <div class="btn-group">
                        <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                            <i class="fa fa-crosshairs fa-2x"></i>
                        </button>
                    </div>
                    <div class="btn-group">
                        <a href="<?php echo HOME_URI; ?>/modulosupervisao/maquinaUtilizada">
                            <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                <i class="fa fa-truck fa-2x"></i>
                            </button>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a href="<?php echo HOME_URI; ?>/modulosupervisao/materialUtilizado/">
                            <button type="button" class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                <i class="fa fa-wrench fa-2x"></i>
                            </button>
                        </a>
                    </div>
                    <div class="btn-group">
                        <?php

                        if(!empty($maoObra) && !empty($tempo) && !empty($regExecucao) ){
                            echo('<a href="'.HOME_URI.'/modulosupervisao/encerramento">');
                            echo('<button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">');
                            echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                            echo('</a>');
                        }else{
                            echo('<div rel="tooltip-wrapper" data-title="Encerramento. [Somente permitido ap�s preencher m�dulo de Tempo, M�o de Obra e Registro de Execu��o]">');
                            echo('<button type="button" disabled class="btn btn-default" aria-label="Left Align" >');
                            echo('<i class="fa fa-power-off fa-2x"></i> </button>');
                            echo('</div>');
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
</form>
</div>