<div class="page-header">
    <h2>Materiais Utilizados</h2>
</div>

<div class="row">
    <form class="form-group" method="post" action="<?php echo(HOME_URI);?>/modulosupervisao/salvarMaterialUtilizado">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Ordem de Servi�o e Manuten��o - Materiais Utilizados</div>

                <div class="panel-body">


                    <div class="panel panel-default">
                        <div class="panel-heading">Materiais Utilizados</div>

                        <div class="panel-body">
                            <div id="materialUtilizadoAdicionado">
                                <?php
                                $materialOsm = $this->medoo->select("osm_material", "*",["cod_osm" => (int) $_SESSION['refillOsm']['codigoOsm']]);
                                $contador = 0;
                                foreach ($materialOsm as $dados=>$value) {
                                    $contador += 1;
                                    echo('<div class="row">');
                                    echo('<div class="col-md-2"> <label>Cod.</label> <input type="text" class="form-control" disabled value="'.$value['cod_material'].'"></div>');
                                    echo('<input type="hidden" class="form-control" value="'.$value['cod_material'].'" name="codigoMaterial'.$contador.'">');
                                    $material = $this->medoo->select("v_material", "*", ["cod_material" => (int) $value['cod_material']]);
                                    $material = $material[0];
                                    echo('<div class="col-md-3"><label>Nome</label> <input type="text" class="form-control" disabled value="'.$material['nome_material']." ".$material['nome_marca'].'"></div>');
                                    echo('<div class="col-md-1"><label>Un.</label> <input type="text" class="form-control" disabled value="'.$material['sigla_uni_medida'].'"></div>');
                                    echo('<div class="col-md-1"><label>Qtd</label> <input type="text" class="form-control" value="'.$value['utilizado'].'" name="qtdUtilizado'.$contador.'"></div>');
                                    echo('<div class="col-md-2"><label>Estado</label> <input type="text" disabled class="form-control" value="'.$value['estado'].'" name="estadoMaterial'.$contador.'"></div>');
                                    echo('<div class="col-md-2"><label>Origem</label> <input type="text" disabled class="form-control" value="'.$value['origem'].'" name="origemMaterial'.$contador.'"></div>');
                                    echo('<div class="col-md-1"><label>Excluir</label> <input type="checkbox" class="checkbox" name="retirarMaterial'.$contador.'"></div>');
                                    echo('</div>');
                                }

                                echo('<input type="hidden" value="'.$contador.'" name="contadorMaterial">');

                                ?>

                                <div class="row" style="border-bottom: 1px solid #000000; padding-top: 1em"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <?php
                        $maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm'] ]);
                        $tempo = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        $regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOsm']['codigoOsm']]);
                        ?>
                        <div class="col-lg-offset-5 col-lg-7">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Complementos</div>
                                <div class="panel-body">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/dashboardPmp/observarOsm">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Dados Gerais">
                                                    <i class="fa fa-home fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group" >
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/manobrasEletricas">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Manobras El�tricas">
                                                    <i class="fa fa-bolt fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="M�o de Obra">
                                                    <i class="fa fa-users fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/tempoTotal">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Tempos Totais">
                                                    <i class="fa fa-clock-o fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/registroExecucao">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Registro de Execu��o">
                                                    <i class="fa fa-crosshairs fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/maquinaUtilizada">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="M�quinas e Equipamentos">
                                                    <i class="fa fa-truck fa-2x"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" disabled class="btn btn-default" aria-label="Left Align" title="Materiais Utilizados">
                                                <i class="fa fa-wrench fa-2x"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <a href="<?php echo HOME_URI; ?>/moduloPmp/encerramento">
                                                <button type="button" class="btn btn-default" aria-label="Left Align" title="Encerramento">
                                                    <i class="fa fa-power-off fa-2x"></i> </button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </form>
</div>