<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html;iso-8859-1">

<!-- BLoco de CSS -->
<link href='http://fonts.googleapis.com/css?family=Exo+2:700,300,400' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URI; ?>/views/_css/style.css" />
<link rel="icon" type="image/jpg" href="<?php echo HOME_URI ?>/views/_images/mini_metroservice_logo.png" />

<!-- T�tulo -->
<title><?php echo $this->titulo; ?></title>

</head>

<body>
<?php
    $mystring = $_SERVER['HTTP_USER_AGENT'];
    $findme   = 'Firefox';
    $pos = strpos($mystring, $findme);
?>

	<div id="login" class="form bradius">
		<!-- Login -->
		
		<!-- Logo -->
		<div class="logo">
			<a href="index.php" title="Metro Service"><img alt="Metro Service" src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo.png" width="250px" height="78px"/></a>
		</div>
		<!-- Fim logo -->

		<!-- Acomodar -->
		<div class="acomodar">
			<form method="post">
		
				<!-- Form -->
				<label for="usuario">Usuario:</label><input id="usuario" required class="txt bradius" type="text" name="dadosUsuario[usuario]" value="" <?php if($pos === false)echo "disabled" ?>/>
		
				<!-- Senha -->
				<label for="senha">Senha:</label><input id="senha" required class="txt bradius" type="password" name="dadosUsuario[senha]" value="" <?php if($pos === false)echo "disabled" ?>/>
				
				<!-- Bot�o Submit -->
				<input type="submit" class="sb bradius" value="Entrar" class="submit" <?php if($pos === false)echo "disabled" ?>/>
			
			</form>
			<!-- Fim Form -->
			
		</div>
		<!-- Fim Acomodar -->

	</div>
	<!-- Fim Login -->

	<!-- Div-Erro  -->
	<div id="erroLogin" class="form bradius">
			<?php
            echo $this->erroLogin;
            if($pos === false)
                echo "<h3>O SIMG ter� total funcionalidade sendo acessado pelo Firefox.
                        <br /><br />Por favor, acesse utilizando o Mozilla Firefox.</h3>";
            ?>
	</div>
	<!-- Fim - Div-Erro -->
	
</body>
</html>