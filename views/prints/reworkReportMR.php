<?php
require_once(ABSPATH . "/views/_includes/_header.php");

$title="";

if(count($_GET) > 1)
{
  $_GET = array_filter($_GET);
  $where = "";
  $whereData = "";

  unset($_GET['path']);

  if(!empty($_GET['dataPartir']) || !empty($_GET['dataAte']))
  {
    if(!empty($_GET['dataPartir']))
    {
      $whereData .= " AND data_abertura >= '{$_GET['dataPartir']}'";
      $dataPartir = $_GET['dataPartir'];
      unset($_GET['dataPartir']);
      $title .= "|| A PARTIR <strong>{$dataPartir}</strong> ||";
    }

    if(!empty($_GET['dataAte']))
    {
      $whereData .= "AND data_abertura <= '{$_GET['dataAte']}'";
      $dataAte = $_GET['dataAte'];
      unset($_GET['dataAte']);
      $title .= "|| AT� <strong>{$dataAte}</strong> ||";
    }
    
  }else{
    $whereData .= "AND vsaf.data_abertura>(NOW()-interval '30 days')";
    $title .= "|| �LTIMOS 30 DIAS ||";
  }

  foreach ($_GET as $dados=>$value)
  {
    $where .= " AND {$dados} = {$value}";
    switch($dados)
    {
        case "cod_grupo":
            $grupo = $this->medoo->select("grupo", ["nome_grupo"], ["cod_grupo"=> $value])[0];
            $grupo = strToUpper($grupo['nome_grupo']);
            $title .= "|| GRUPO <strong>{$grupo}</strong> ||";
            break;
        case "cod_veiculo":
            $grupo = $this->medoo->select("veiculo", ["nome_veiculo"], ["cod_veiculo"=> $value])[0];
            $grupo = strToUpper($grupo['nome_veiculo']);
            $title .= "|| VE�CULO <strong>{$grupo}</strong> ||";
            break;
        case "cod_linha":
            $grupo = $this->medoo->select("linha", ["nome_linha"], ["cod_linha"=> $value])[0];
            $grupo = strToUpper($grupo['nome_linha']);
            $title .= "|| LINHA <strong>{$grupo}</strong> ||";
            break;
        case "cod_carro":
            $grupo = $this->medoo->select("carro", ["nome_carro"], ["cod_carro"=> $value])[0];
            $grupo = strToUpper($grupo['nome_carro']);
            $title .= "|| CARRO <strong>{$grupo}</strong> ||";
            break;
    }
  }

  $sql = "SELECT
                count(*) as ctd, vsaf.cod_avaria, vsaf.cod_carro, vsaf.nome_avaria, vsaf.nome_carro, vsaf.nome_veiculo
            FROM
                v_saf vsaf
            WHERE
                vsaf.cod_carro IS NOT NULL
                {$whereData}
                {$where}
            GROUP BY
                vsaf.cod_avaria, vsaf.cod_carro, vsaf.nome_avaria, vsaf.nome_carro, vsaf.nome_veiculo
            HAVING count(*) > 1
            ORDER BY
                ctd DESC";
            
  $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

  if(empty($title))
    $title = " || SEM FILTRO";
}
?>

<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> IMPRIMIR RELAT�RIO
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="text-center" style="font-size: 15pt; font-weight: bold; padding-bottom: 10px">RELAT�RIO DE RETRABALHO</div>
<div class="text-center" style="font-size: 10pt; padding-bottom: 20px">
    <?= $title ?>
</div>

<div class="geralPdfRel">
    <div>
        <table class="table" style="text-align: center;">
        <thead>
            <tr>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">AVARIA</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">VE�CULO</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">CARRO</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">QTD SAF'S</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">SAF'S</td>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($result))
            {
              foreach($result as $dados=>$value)
              {
                $row ="";
                $sql = "SELECT
                            vsaf.cod_saf, vsaf.nome_status, vsaf.data_abertura
                        FROM
                            v_saf vsaf
                        WHERE
                            vsaf.cod_carro = {$value['cod_carro']}
                            AND vsaf.cod_avaria = {$value['cod_avaria']}
                            {$whereData}
                        ORDER BY vsaf.cod_saf";

                $saf = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                $ctdSaf = count($saf);
                foreach($saf as $valor)
                {
                    $dataAbertura = $this->parse_timestamp($valor['data_abertura']);
                    $row .= "<tr>
                    <td>
                      {$valor['cod_saf']}
                    </td>
                    <td>
                      {$valor['nome_status']}
                    </td>
                    <td>
                      {$dataAbertura}
                    </td>
                    </tr>";
                }
                $table = "
                <table class='table-bordered table-stripped' style='text-align: center; width: 100%'>
                  <thead>
                    <tr>
                      <th style='text-align: center;'>
                        SAF
                      </th>
                      <th style='text-align: center;'>
                        STATUS
                      </th>
                      <th style='text-align: center;'>
                        DATA ABERTURA
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {$row}
                  </tbody>
                </table>";
            
                 echo <<<HTML
                 <tr>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_avaria"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_veiculo"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_carro"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$ctdSaf}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$table}
                   </td>
                 </tr>
            HTML;
              }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<?php
require_once(ABSPATH . "/views/layout/bottonLayout.php");
?>
