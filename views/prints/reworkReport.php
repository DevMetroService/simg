<?php
require_once(ABSPATH . "/views/_includes/_header.php");

$title="";
$rows ="";

if(count($_GET) > 1)
{
  $_GET = array_filter($_GET);
  $where = "";
  $whereData = "";

  unset($_GET['path']);

  if(!empty($_GET['dataPartir']) || !empty($_GET['dataAte']))
  {
    if(!empty($_GET['dataPartir']))
    {
      $whereData .= " AND data_abertura >= '{$_GET['dataPartir']}'";
      $dataPartir = $_GET['dataPartir'];
      unset($_GET['dataPartir']);
      $title .= "|| A PARTIR <strong>{$dataPartir}</strong> ||";
    }

    if(!empty($_GET['dataAte']))
    {
      $whereData .= "AND data_abertura <= '{$_GET['dataAte']}'";
      $dataAte = $_GET['dataAte'];
      unset($_GET['dataAte']);
      $title .= "|| AT� <strong>{$dataAte}</strong> ||";
    }
    
  }else{
    $whereData .= "AND vsaf.data_abertura>(NOW()-interval '30 days')";
    $title .= "|| �LTIMOS 30 DIAS ||";
  }

  foreach ($_GET as $dados=>$value)
  {
    $where .= " AND {$dados} = {$value}";
    switch($dados)
    {
      case "cod_grupo":
        $grupo = $this->medoo->select("grupo", ["nome_grupo"], ["cod_grupo"=> $value])[0];
        $grupo = strToUpper($grupo['nome_grupo']);
        $title .= "|| GRUPO <strong>{$grupo}</strong> ||";
        break;
      case "cod_sistema":
        $grupo = $this->medoo->select("sistema", ["nome_sistema"], ["cod_sistema"=> $value])[0];
        $grupo = strToUpper($grupo['nome_sistema']);
        $title .= "|| SISTEMA <strong>{$grupo}</strong> ||";
        break;
      case "cod_linha":
        $grupo = $this->medoo->select("linha", ["nome_linha"], ["cod_linha"=> $value])[0];
        $grupo = strToUpper($grupo['nome_linha']);
        $title .= "|| LINHA <strong>{$grupo}</strong> ||";
        break;
      case "cod_trecho":
        $grupo = $this->medoo->select("trecho", ["descricao_trecho"], ["cod_trecho"=> $value])[0];
        $grupo = strToUpper($grupo['descricao_trecho']);
        $title .= "|| TRECHO <strong>{$grupo}</strong> ||";
        break;
    }
  }

  $sql = "SELECT
            count(*) as ctd, vsaf.cod_grupo, vsaf.nome_grupo, vsaf.cod_sistema, vsaf.nome_sistema, vsaf.cod_linha
            , vsaf.nome_linha, vsaf.cod_trecho, vsaf.nome_trecho, vsaf.cod_avaria, vsaf.nome_avaria
          FROM
            v_saf vsaf
          WHERE
            vsaf.cod_carro IS NULL
            {$where}
            {$whereData}
          GROUP BY
            vsaf.cod_grupo, vsaf.nome_grupo, vsaf.cod_sistema, vsaf.nome_sistema, vsaf.cod_linha, vsaf.nome_linha
            , vsaf.cod_trecho, vsaf.nome_trecho, vsaf.cod_avaria, vsaf.nome_avaria
          HAVING count(*) > 1
          ORDER BY
            ctd DESC, cod_grupo, cod_sistema, cod_linha, cod_trecho";
            
  $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

  if(empty($title))
    $title = " || SEM FILTRO";
}
?>

<div class="row hidden-print">
    <div class="col-md-offset-4 col-md-4 hidden-print">
        <button class="imprimir btn btn-primary btn-block hidden-print"><i class="fa fa-print fa-fw"></i> IMPRIMIR RELAT�RIO
        </button>
    </div>
</div>

<div class="cabecalhoRel">
    <img src="<?php echo HOME_URI; ?>/views/_images/metrofor.png"/>

    <label class="dirRel"><?php echo date('d \d\e M \d\e Y'); ?></label>
</div>

<div class="text-center" style="font-size: 15pt; font-weight: bold; padding-bottom: 10px">RELAT�RIO DE RETRABALHO</div>
<div class="text-center" style="font-size: 10pt; padding-bottom: 20px">
    <?= $title ?>
</div>

<div class="geralPdfRel">
    <div>
        <table class="table" style="text-align: center;">
        <thead>
            <tr>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">AVARIA</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">GRUPO DE SISTEMA</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">SISTEMA</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">LINHA</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">TRECHO</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">QTD SAF'S</td>
                <td style="background: #424242; color: #ffffff; font-weight: bold; text-align: center;">SAF'S</td>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($result))
            {
              foreach($result as $dados=>$value)
              {
                $sql = "SELECT
                           vsaf.cod_saf, vsaf.nome_status, vsaf.complemento_local, vsaf.data_abertura
                         FROM
                           v_saf vsaf
                         WHERE
                           vsaf.cod_carro IS NULL
                           {$whereData}
                           AND vsaf.cod_sistema = {$value['cod_sistema']}
                           AND vsaf.cod_trecho = {$value['cod_trecho']}
                           AND vsaf.cod_avaria = {$value['cod_avaria']}
                         ORDER BY vsaf.cod_saf";
                $saf = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                $ctdSaf = count($saf);
            
                foreach($saf as $valor)
                {
                    $dataAbertura = $this->parse_timestamp($valor['data_abertura']);
                    $row .= "<tr>
                    <td>
                      {$valor['cod_saf']}
                    </td>
                    <td>
                      {$valor['complemento_local']}
                    </td>
                    <td>
                      {$valor['nome_status']}
                    </td>
                    <td>
                      {$dataAbertura}
                    </td>
                    </tr>";
                }
            
                $table = "
                <table class='table-bordered table-stripped' style='text-align: center; width: 100%'>
                  <thead>
                    <tr>
                      <th style='text-align: center;'>
                        SAF
                      </th>
                      <th style='text-align: center;'>
                        COMPLEMENTO LOCAL
                      </th>
                      <th style='text-align: center;'>
                        STATUS
                      </th>
                      <th style='text-align: center;'>
                        DATA ABERTURA
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {$row}
                  </tbody>
                </table>";
            
                 echo <<<HTML
                 <tr>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_avaria"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_grupo"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_sistema"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_linha"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$value["nome_trecho"]}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$ctdSaf}
                   </td>
                   <td style='background: #ECEFF1; color: #424242; border: groove; text-align: center;'>
                    {$table}
                   </td>
                 </tr>
            HTML;
              }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<?php
require_once(ABSPATH . "/views/layout/bottonLayout.php");
?>
