<?php
if (!empty($this->codOs)) {
    $osm = $this->medoo->select("osm_falha",
        [
            "[>]grupo" => ["grupo_atuado" => "cod_grupo"],
            "[>]sistema" => ["sistema_atuado" => "cod_sistema"],
            "[>]subsistema" => ["subsistema_atuado" => "cod_subsistema"],
            "[>]local_grupo" => ["cod_local_grupo" => "cod_local_grupo"],
            "[>]sublocal_grupo" => ["cod_sublocal_grupo" => "cod_sublocal_grupo"],
            "[>]linha" => ["cod_linha_atuado" => "cod_linha"],
            "[>]trecho" => ["trecho_atuado" => "cod_trecho"],
            "[>]ponto_notavel" => ["cod_ponto_notavel_atuado" => "cod_ponto_notavel"],
            "[>]via" => "cod_via",
            "[><]osm_servico" => "cod_osm",
            "[><]servico" => "cod_servico",
            "[>]material_rodante_osm" => "cod_osm",
            "[>]veiculo" => "cod_veiculo",
            "[>]carro" => "cod_carro"
        ], "*", ["cod_osm" => (int)$this->codOs])[0];

    $usuarioCriador = $this->medoo->select("status_osm",["[><]usuario" => ["status_osm.usuario" => "cod_usuario"]], "*", ["cod_osm" =>(int)$this->codOs])[0]['usuario'];

    $usuarioResponsavel = $this->medoo->select("usuario", '*',['cod_usuario' => $osm['usuario_responsavel']])[0];

    $status = $this->medoo->select("status_osm", ["[><]status" => "cod_status"], "*", ["cod_ostatus" => (int)$osm['cod_ostatus']])[0];

    $tempo = $this->medoo->select('osm_tempo', '*', ["cod_osm" => (int)$this->codOs])[0];

    $maoObra = $this->medoo->select("osm_mao_de_obra", ["[><]funcionario" => "cod_funcionario"], "*", ["cod_osm" => (int)$this->codOs]);

    $registro = $this->medoo->select("osm_registro", [
        "[>]un_equipe" => "cod_un_equipe",
        "[>]unidade" => "cod_unidade",
        "[>]equipe" => "cod_equipe",
        "[>]causa" => "cod_causa",
        "[>]atuacao" => "cod_atuacao",
        "[>]agente_causador" => "cod_ag_causador"
    ], "*", ["cod_osm" => (int)$this->codOs])[0];

    $maquinas = $this->medoo->select("osm_maquina", ["[><]material" => "cod_material"],
        [
            "cod_material",
            "nome_material",
            "qtd",
            "material.descricao(descricao_material)",
            "total_hrs",
            "num_serie",
            "osm_maquina.descricao"
        ], ["cod_osm" => $this->codOs]);

    $materiais = $this->medoo->query("SELECT
                                        *
                                    FROM osm_material osm
                                    JOIN material USING (cod_material)
                                    JOIN unidade_medida um ON(osm.unidade = um.cod_uni_medida)
                                    JOIN osm_falha USING (cod_osm)
                                    WHERE cod_osm = {$this->codOs}
                                    ORDER BY nome_material")->fetchAll(PDO::FETCH_ASSOC);

    $encerramento = $this->medoo->select("osm_encerramento",
        [
            "[>]funcionario" => "cod_funcionario",
            "[>]tipo_fechamento" => "cod_tipo_fechamento",
            "[>]pendencia" => "cod_pendencia",
            "[>]tipo_pendencia" => "cod_tipo_pendencia",
            "[>]un_equipe" => "cod_un_equipe",
            "[>]unidade" => "cod_unidade",
            "[>]equipe" => "cod_equipe"
        ], "*", ["cod_osm" => (int)$this->codOs])[0];

    $ssm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$osm['cod_ssm']])[0];

    $saf = $this->medoo->select("v_saf", "*", ["cod_saf" => (int)$ssm['cod_saf']])[0];
}
?>
<div class="pagina">
    <div class="title">
        <img src="<?php echo HOME_URI ?>/views/_images/metroservice_logo.png"/>

        <h3>OSM - Ordem de Servi�o de Manuten��o</h3>
    </div>
    <hr/>

    <div class="title-module">Dados Gerais</div>
    <table>
        <tr>
            <th>OSM</th>
            <th>SSM</th>
            <th>Data/Hora Abertura</th>
            <th>N�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osm['cod_osm']; ?> </td>
            <td class="center"><?php echo $osm['cod_ssm']; ?> </td>
            <td class="center"><?php echo MainController::parse_timestamp_static($osm['data_abertura']); ?></td>
            <td class="center"><?php echo !empty($osm['nivel']) ? $osm['nivel'] : $ssm['nivel']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Equipe</th>
            <th colspan="2">Grupo/Sistema</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $ssm['sigla_equipe'] . " / " . $ssm['nome_equipe']; ?> </td>
            <td colspan="2" class="center"><?php
                echo($ssm['grupo_sistema'] . $ssm['sigla_sistema'] . " / " . $ssm['nome_grupo'] . " - " . $ssm['nome_sistema']); ?></td>
        </tr>
        <tr>
            <th colspan="4">Respons�vel pela cria��o</th>
        </tr>
        <tr>
            <td colspan="4" class="center"><?php echo $usuarioCriador ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Origem</div>
    <table>
        <tr>
            <th>SAF</th>
            <th>Data/Hora</th>
            <th>�rea</th>
        </tr>
        <tr>
            <td class="center"><?php echo $saf['cod_saf']; ?> </td>
            <td class="center"><?php echo MainController::parse_timestamp_static($saf['data_abertura']); ?> </td>
            <td class="center"><?php if (!empty($osm)) {
                    switch ($saf['tipo_orisaf']) {
                        case 1:
                            echo("MetroService");
                            break;
                        case 2:
                            echo("MetroFor");
                            break;
                        case 3:
                            echo("PJ - MetroService");
                            break;
                        case 4:
                            echo("Terceiros");
                    }
                } ?></td>
        </tr>
        <tr>
            <?php if (!empty($saf['matricula'])) {
                echo "<th>Matr�cula</th>
                      <th>Nome Completo</th>";
            } else {
                echo "<th colspan='2'>Nome Completo</th>";
            } ?>
            <th>Contato</th>
        </tr>
        <tr>
            <?php if (!empty($saf['matricula'])) {
                echo "<td class='center'>{$saf['matricula']}</td>
                      <td>{$saf['nome']}</td>";
            } else {
                echo "<td colspan='2'>{$saf['nome']}</td>";
            } ?>
            <td class="center"><?php echo $saf['contato']; ?></td>
        </tr>
    </table>

    <div class="title-table">&raquo; Identifica��o</div>
    <table>
        <tr>
            <th colspan="2">Grupo de Sistema Atuado</th>
            <th colspan="2">Sistema Atuado</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $osm['nome_grupo']; ?></td>
            <td colspan="2" class="center"><?php echo $osm['nome_sistema']; ?></td>
        </tr>

        <tr>
            <th colspan="4">SubSistema Atuado</th>
        </tr>
        <tr>
            <td colspan="4" class="center"><?php echo $osm['nome_subsistema']; ?></td>
        </tr>
        <tr>
            <th colspan="2">Local</th>
            <th colspan="2">SubLocal</th>
        </tr>
        <tr>
            <td colspan="2" class="center"><?php echo $osm['nome_local_grupo']; ?></td>
            <td colspan="2" class="center"><?php echo $osm['nome_sublocal_grupo']; ?></td>
        </tr>
        <tr>
            <th>Linha</th>
            <th colspan="2">Trecho</th>
            <th>Ponto Not�vel</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osm['nome_linha']; ?></td>
            <td colspan="2" class="center">
                <?php if (!empty($osm)) {
                    echo $osm['nome_trecho'] . ' - ' . $osm['descricao_trecho'];
                } ?>
            </td>
            <td class="center">
                <?php if (!empty($osm)) {
                    echo $osm['nome_ponto'];
                } ?>
            </td>
        </tr>
        <tr>
            <th colspan="4">Complemento</th>
        </tr>
        <tr>
            <td colspan="4"><?php echo $osm['complemento_local']; ?></td>
        </tr>
        <tr>
            <th>Via</th>
            <th>Km Inicial</th>
            <th>Km Final</th>
            <th>Posi��o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osm['nome_via']; ?></td>
            <td class="center"><?php echo $osm['km_inicial'] ?></td>
            <td class="center"><?php echo $osm['km_final']; ?></td>
            <td class="center"><?php echo $osm['posicao_atuado']; ?></td>
        </tr>
    </table>

    <?php if (!empty($osm['nome_veiculo'])) { ?>
        <div class="title-table">&raquo; Material Rodante</div>
        <table>
            <tr>
                <th>Ve�culo</th>
                <th>Carro</th>
                <th>Odometro</th>
            </tr>
            <tr>
                <td>
                    <?php echo $osm['nome_veiculo']; ?>
                </td>
                <td>
                    <?php echo $osm['nome_carro']; ?>
                </td>
                <td>
                    <?php echo $osm['odometro']; ?>
                </td>
            </tr>
        </table>
    <?php } ?>

    <div class="title-table">&raquo; Servi�o a Ser Executado</div>
    <table>
        <tr>
            <th colspan="3">Descri��o do Servi�o</th>
        </tr>
        <tr>
            <td colspan="3"><?php echo $osm['nome_servico']; ?></td>
        </tr>
        <tr>
            <th colspan="3">Complemento</th>
        </tr>
        <tr>
            <td colspan="3"><?php echo $osm['complemento']; ?></td>
        </tr>
        <tr>
            <th>Efetivo</th>
            <th>Tempo</th>
            <th>Total Servi�o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $osm['qtd_pessoas']; ?></td>
            <td class="center"><?php echo $osm['tempo']; ?></td>
            <td class="center"><?php echo $osm['total_servico']; ?></td>
        </tr>
    </table>

    <?php if (!empty($status['descricao'])) { ?>
        <div class="title-table">&raquo; Situa��o da Solicita��o</div>
        <table>
            <tr>
                <th>Status</th>
                <th>Descri��o</th>
            </tr>
            <tr>
                <td><?php echo $status['nome_status']; ?></td>
                <td><?php echo $status['descricao']; ?></td>
            </tr>
        </table>
    <?php } ?>

    <table class="footer">
        <tr>
            <th>Respons�vel</th>
            <th>Situa��o</th>
            <th>Condi��o</th>
        </tr>
        <tr>
            <td class="center"><?php echo $usuarioResponsavel['usuario']; ?></td>
            <td class="center"><?php echo $status['nome_status']; ?></td>
            <td class="center"><?php echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado'; ?></td>
        </tr>
    </table>

    <?php
    if (!empty($tempo)) {
        ?>
        <div class="title-module">Tempos Totais</div>

        <div class="title-table">&raquo; Prepara�ao e Acesso</div>
        <table>
            <tr>
                <th>In�cio</th>
                <th>Pronto para Sair</th>
                <th>Sa�da Autorizada</th>
                <th>Sa�da</th>
                <th>Chegada</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_preparacao']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_ace_pronto']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_ace_saida_autorizada']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_ace_saida']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_ace_chegada']);
                    }
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Execu��o</div>
        <table>
            <tr>
                <th>In�cio</th>
                <th>T�rmino</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_exec_inicio']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_exec_termino']);
                    }
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Regresso</div>
        <table>
            <tr>
                <th>Pronto para Sair</th>
                <th>Sa�da Autorizada</th>
                <th>Chegada</th>
                <th>Desmobiliza��o</th>
            </tr>
            <tr>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_reg_pronto']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_reg_saida_autorizada']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_reg_chegada']);
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if (!empty($tempo)) {
                        echo MainController::parse_timestamp_static($tempo['data_desmobilizacao']);
                    }
                    ?>
                </td>
            </tr>
        </table>

        <div class="title-table">&raquo; Observa��o</div>
        <table>
            <tr>
                <td><?php echo $tempo['descricao']; ?></td>
            </tr>
        </table>
        <?php
    }

    if (!empty($maoObra)) {
        echo '<div class="title-module">M�o de Obra</div>
                    <table>
                    <tr>
                        <th>Matr�cula</th>
                        <th>Nome</th>
                        <th>In�cio</th>
                        <th>T�rmino</th>
                    </tr>';

        foreach ($maoObra as $dados => $value) {
            ?>
            <tr>
                <td class="center">
                    <?php
                    echo $value['matricula'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $value['nome_funcionario'];
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $value['data_inicio'];
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $value['data_termino'];
                    ?>
                </td>
            </tr>
            <?php
        }
        echo '</table>';
    }

    if (!empty($registro)) {
        ?>
        <div class="title-module">Registro de Execu��o</div>

        <table>
            <tr>
                <th colspan="2">Local</th>
                <th colspan="2">Equipe</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo $registro['nome_unidade']; ?></td>
                <td colspan="2" class="center"><?php echo $registro['sigla'] . ' - ' . $registro['nome_equipe']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Causa</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['nome_causa']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o da Causa</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['desc_causa']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Atua��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['nome_atuacao']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o da Atua��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['desc_atuacao']; ?></td>
            </tr>
            <tr>
                <th colspan="2">Ag.Causador</th>
                <th>Clima</th>
                <th>Temperatura</th>
            </tr>
            <tr>
                <td colspan="2" class="center"><?php echo $registro['nome_agente']; ?></td>
                <td class="center"><?php
                    switch ($registro['clima']) {
                        case "b":
                            echo "Bom";
                            break;
                        case "c":
                            echo "Chuvoso";
                            break;
                        case "n":
                            echo "Nublado";
                            break;
                        case "e":
                            echo "Neblina";
                            break;
                    }
                    ?></td>
                <td class="center"><?php echo $registro['temperatura']; ?></td>
            </tr>
            <tr>
                <th>Cautela</th>
                <th>Km Inicial</th>
                <th>Km Final</th>
                <th>Restringir Velocidade</th>
            </tr>
            <tr>
                <td class="center"><?php echo $registro['cautela']; ?></td>
                <td class="center"><?php echo $registro['km_inicial_cautela']; ?></td>
                <td class="center"><?php echo $registro['km_final_cautela']; ?></td>
                <td class="center"><?php echo $registro['restricao_veloc']; ?></td>
            </tr>
            <tr>
                <th>Hor�metro Tra��o MA</th>
                <th>Hor�metro Gerador MA</th>
                <th>Hor�metro Tra��o MB</th>
                <th>Hor�metro Gerador MB</th>
            </tr>
            <tr>
                <td class="center"><?php echo $osm['horimetro_tracao_ma']; ?></td>
                <td class="center"><?php echo $osm['horimetro_gerador_ma']; ?></td>
                <td class="center"><?php echo $osm['horimetro_tracao_mb']; ?></td>
                <td class="center"><?php echo $osm['horimetro_gerador_mb']; ?></td>
            </tr>
            <tr>
                <th colspan="4">Observa��o</th>
            </tr>
            <tr>
                <td colspan="4"><?php echo $registro['dados_complementar']; ?></td>
            </tr>
        </table>
        <?php
    }

    if (!empty($maquinas)) {
        echo '<div class="title-module">M�quinas e Equipamentos Utilizados</div>';

        foreach ($maquinas as $dados => $value) {
            ?>
            <table>
                <tr>
                    <th>C�digo</th>
                    <th colspan="2">Nome</th>
                    <th>Quantidade</th>
                </tr>
                <tr>
                    <td class="center"><?php echo $value['cod_material']; ?></td>
                    <td colspan="2"><?php echo $value['nome_material']; ?></td>
                    <td class="center"><?php echo $value['qtd']; ?></td>
                </tr>
                <tr>
                    <th colspan="4">Complemento</th>
                </tr>
                <tr>
                    <td colspan="4"><?php echo $value['descricao_material']; ?></td>
                </tr>
                <tr>
                    <th colspan="2">Total de Hora</th>
                    <th colspan="2">N�mero de S�rie</th>
                </tr>
                <tr>
                    <td colspan="2" class="center"><?php echo $value['total_hrs']; ?></td>
                    <td colspan="2" class="center"><?php echo $value['num_serie']; ?></td>
                </tr>
                <tr>
                    <th colspan="4">Descri��o</th>
                </tr>
                <tr class="tr-destaque">
                    <td colspan="4"><?php echo $value['descricao']; ?></td>
                </tr>
            </table>
            <?php
        }
    }

    if (!empty($materiais)) {
        echo ' <div class="title-module">Materiais Utilizados</div><table>';

        foreach ($materiais as $dados => $value) {
            ?>
            <tr>
                <th>C�digo</th>
                <th>Nome</th>
                <th>Quantidade</th>
            </tr>
            <tr>
                <td class="center"><?php echo "{$value['cod_material']}"; ?></td>
                <td><?php echo "{$value['nome_material']} - {$value['descricao']}";?></td>
                <td class="center"><?php echo $value['utilizado']; ?></td>
            </tr>
            <tr>
                <th>Estado</th>
                <th>Origem</th>
                <th>Unidade de Medida</th>
            </tr>
            <tr class="tr-destaque">
                <td class="center">
                    <?php
                    switch($value['estado']){
                        case "n":
                            echo 'Novo';
                            break;
                        case "u":
                            echo 'Usado';
                            break;
                        case "r":
                            echo 'Reparado';
                            break;
                        default:
                            echo 'ERROR, contatar TI.';
                            break;
                    }
                    ?></td>
                <td class="center"><?php
                    switch ($value['origem']) {
                        case 's':
                            echo "MetroService";
                            break;
                        case 'f':
                            echo "MetroFor";
                            break;
                        default:
                            echo "Outros";
                            break;
                    }
                    ?></td>
                <td class="center"><?php
                    echo $value['sigla_uni_medida'] . ' - ' . $value['nome_uni_medida'];
                    ?></td>
            </tr>
            <?php
        }
        echo '</table>';
    }

    if (!empty($encerramento)) {
        ?>
        <div class="title-module">Encerramento</div>

        <table>
            <tr>
                <th colspan="2">Data / Hora de Recebimento de Informa��es</th>
                <th>Matricula</th>
                <th>Respons�vel pelas Informa��es</th>
            </tr>
            <tr>
                <td colspan="2" class="center">
                    <?php
                    echo MainController::parse_timestamp_static($encerramento['data_encerramento']);
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo $encerramento['matricula'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $encerramento['nome_funcionario'];
                    ?>
                </td>
            </tr>
            <tr>
                <th>Libera��o</th>
                <th>Transfer�ncia</th>
                <th colspan="2">Tipo de Encerramento</th>
            </tr>
            <tr>
                <td class="center">
                    <?php
                    echo ($encerramento['liberacao'] == "s") ? 'Liberado' : 'N�o Liberado';
                    ?>
                </td>
                <td class="center">
                    <?php
                    echo ($encerramento['transferida'] == "s") ? 'Transferido' : 'N�o Transferido';
                    ?>
                </td>
                <td colspan="2" class="center">
                    <?php
                    echo $encerramento['nome_fechamento'];
                    ?>
                </td>
            </tr>
            <?php
            if ($encerramento['cod_tipo_fechamento'] == 3) {
                ?>
                <tr>
                    <th colspan="4">Motivo da N�o Execu��o</th>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php
                        echo $encerramento['descricao'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            if (!empty($encerramento['cod_pendencia'])) {
                ?>
                <tr>
                    <th colspan="3">Tipo de Pend�ncia</th>
                    <th>Pend�ncia</th>
                </tr>
                <tr>
                    <td colspan="3">
                        <?php
                        echo $encerramento['nome_tipo_pendencia'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $encerramento['nome_pendencia'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            if ($encerramento['transferida'] == 's') {
                ?>
                <tr>
                    <th colspan="3">Local Transfer�ncia</th>
                    <th>Equipe Transferida</th>
                </tr>
                <tr>
                    <td colspan="3">
                        <?php
                        echo $encerramento['nome_unidade'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $encerramento['nome_equipe'];
                        ?>
                    </td>
                </tr>
                <tr>
                    <th colspan="4">
                        Observa��o Geral
                    </th>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php
                        echo $encerramento['descricao'];
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
    ?>
</div>
