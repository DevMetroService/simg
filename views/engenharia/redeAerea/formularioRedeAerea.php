<?php

require_once(ABSPATH . '/functions/modulosPMP.php');

if ($_SESSION['refillPmp']) {
    $refill = $_SESSION['refillPmp'];
}

$sistemaRa = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 20]);
if (!$sistemaRa)
    $sistemaRa = null;

$subsistemaRa = null;
if (!empty($refill['sistema']))
    $subsistemaRa = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['cod_sistema' => $refill['sistema']]);

$servicoPmpRa = null;
if (!empty($refill['subSistema']))
    $servicoPmpRa = $this->medoo->select('servico_pmp_sub_sistema',
        [
            '[><]servico_pmp' => 'cod_servico_pmp',
            '[><]sub_sistema' => 'cod_sub_sistema'
        ],
        ['nome_servico_pmp', 'cod_servico_pmp'], ["AND" => ['sub_sistema.cod_subsistema' => $refill['subSistema'], 'cod_grupo' => 20]]);

$periodicidadeRa = null;
if (!empty($refill['servicoPmp']))
    $periodicidadeRa = $this->medoo->select('servico_pmp_periodicidade',
        [
            '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
            '[><]tipo_periodicidade' => 'cod_tipo_periodicidade'
        ],
        ['nome_periodicidade', 'cod_tipo_periodicidade'], ["servico_pmp_sub_sistema.cod_servico_pmp" => $refill['servicoPmp']]);

$pmpRedeAerea = $this->medoo->select("pmp",
    [
        '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
        '[><]pmp_rede_aerea' => 'cod_pmp'
    ], '*', ['ativo' => ['A','E']]);

if (!empty($pmpRedeAerea)) {

    $servicoPmpSbS;
    $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                  FROM servico_pmp_sub_sistema sp
                                  inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                  inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                  inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                  inner join sistema s on ss.cod_sistema = s.cod_sistema
                                  WHERE cod_grupo = 20")->fetchAll();

    if ($sql)
        foreach ($sql as $dados)
            $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


    $periodicidade;
    $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
    if ($selectPeriodicidade)
        foreach ($selectPeriodicidade as $dados)
            $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


    $procedimento;
    $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 20]);
    if ($selectProcedimento)
        foreach ($selectProcedimento as $dados)
            $procedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];

    $local;
    $selectLocal = $this->medoo->select("local", ["cod_local", "nome_local"]);
    if ($selectLocal)
        foreach ($selectLocal as $dados)
            $local[$dados['cod_local']] = $dados['nome_local'];

    $poste;
    $selectPoste = $this->medoo->select("poste", ["cod_poste", "nome_poste"]);
    if ($selectPoste)
        foreach ($selectPoste as $dados)
            $poste[$dados['cod_poste']] = $dados['nome_poste'];

}


$btnsAcao = "";
$btnsAcao .= btnSalvarPmp();
//$btnsAcao .= btnImprimirPmp();

require_once(ABSPATH . "/views/_includes/formularios/pmp/ra/redeAerea.php");

unset ($_SESSION['refillPmp']);