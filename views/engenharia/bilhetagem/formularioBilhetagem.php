<?php

require_once(ABSPATH . '/functions/modulosPMP.php');

if ($_SESSION['refillPmp']) {
    $refill = $_SESSION['refillPmp'];
}

$sistemaBl = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 28]);
if (!$sistemaBl)
    $sistemaBl = null;

$subsistemaBl = null;
if (!empty($refill['sistema']))
    $subsistemaBl = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['cod_sistema' => $refill['sistema']]);

$servicoPmpBl = null;
if (!empty($refill['subSistema']))
    $servicoPmpBl = $this->medoo->select('servico_pmp_sub_sistema',
        [
            '[><]servico_pmp' => 'cod_servico_pmp',
            '[><]sub_sistema' => 'cod_sub_sistema'
        ],
        ['nome_servico_pmp', 'cod_servico_pmp'], ["AND" => ['sub_sistema.cod_subsistema' => $refill['subSistema'], 'cod_grupo' => 28]]);

$periodicidadeBl = null;
if (!empty($refill['servicoPmp']))
    $periodicidadeBl = $this->medoo->select('servico_pmp_periodicidade',
        [
            '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
            '[><]tipo_periodicidade' => 'cod_tipo_periodicidade'
        ],
        ['nome_periodicidade', 'cod_tipo_periodicidade'], ["servico_pmp_sub_sistema.cod_servico_pmp" => $refill['servicoPmp']]);


$pmpBilhetagem = $this->medoo->select("pmp",
    [
        '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
        '[><]pmp_bilhetagem' => 'cod_pmp'
    ], '*', ['ativo' => ['A','E']]);

if (!empty($pmpBilhetagem)) {

    $estacao;
    $selectEstacao = $this->medoo->select("estacao", ["cod_estacao", "nome_estacao"]);
    if ($selectEstacao)
        foreach ($selectEstacao as $dados)
            $estacao[$dados['cod_estacao']] = $dados['nome_estacao'];

    $servicoPmpSbS;
    $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                  FROM servico_pmp_sub_sistema sp
                                  inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                  inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                  inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                  inner join sistema s on ss.cod_sistema = s.cod_sistema
                                  WHERE cod_grupo = 28")->fetchAll();

    if ($sql)
        foreach ($sql as $dados)
            $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


    $periodicidade;
    $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
    if ($selectPeriodicidade)
        foreach ($selectPeriodicidade as $dados)
            $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


    $procedimento;
    $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 28]);
    if ($selectProcedimento)
        foreach ($selectProcedimento as $dados => $value)
            $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

}


$btnsAcao = "";
$btnsAcao .= btnSalvarPmp();

require_once(ABSPATH . "/views/_includes/formularios/pmp/bl/bilhetagem.php");

unset ($_SESSION['refillPmp']);