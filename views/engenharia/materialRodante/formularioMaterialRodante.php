<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 01/12/2017
 * Time: 10:26
 */

require_once(ABSPATH . '/functions/modulosPMP.php');

if ($_SESSION['refillPmp']) {
    $refill = $_SESSION['refillPmp'];
}

$sistemaVlt = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 22]);
if (!$sistemaVlt)
    $sistemaVlt = null;

$subsistemaVlt = null;
if (!empty($refill['sistema']))
    $subsistemaVlt = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['cod_sistema' => $refill['sistema']]);

$servicoPmpVlt = null;
if (!empty($refill['subSistema']))
    $servicoPmpVlt = $this->medoo->select('servico_pmp_sub_sistema',
        [
            '[><]servico_pmp' => 'cod_servico_pmp',
            '[><]sub_sistema' => 'cod_sub_sistema'
        ],
        ['nome_servico_pmp', 'cod_servico_pmp'], ["AND" => ['sub_sistema.cod_subsistema' => $refill['subSistema'], 'cod_grupo' => 22]]);

$periodicidadeVlt = null;
if (!empty($refill['servicoPmp']))
    $periodicidadeVlt = $this->medoo->select('servico_pmp_periodicidade',
        [
            '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
            '[><]tipo_periodicidade' => 'cod_tipo_periodicidade'
        ],
        ['nome_periodicidade', 'cod_tipo_periodicidade'], ["servico_pmp_sub_sistema.cod_servico_pmp" => $refill['servicoPmp']]);


$pmpMaterialRodanteVlt = $this->medoo->select("pmp",
    [
        '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
        '[><]pmp_vlt' => 'cod_pmp'
    ], '*', ['ativo' => ['A','E']]);

if (!empty($pmpMaterialRodanteVlt)) {

    $veiculo;
    $selectVeiculo = $this->medoo->select("veiculo", ["cod_veiculo", "nome_veiculo"]);
    if ($selectVeiculo)
        foreach ($selectVeiculo as $dados)
            $veiculo[$dados['cod_veiculo']] = $dados['nome_veiculo'];

    $servicoPmpSbS;
    $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                  FROM servico_pmp_sub_sistema sp
                                  inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                  inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                  inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                  inner join sistema s on ss.cod_sistema = s.cod_sistema
                                  WHERE cod_grupo = 22")->fetchAll();

    if ($sql)
        foreach ($sql as $dados)
            $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


    $periodicidade;
    $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
    if ($selectPeriodicidade)
        foreach ($selectPeriodicidade as $dados)
            $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


    $procedimento;
    $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 22]);
    if ($selectProcedimento)
        foreach ($selectProcedimento as $dados => $value)
            $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

}

//Pesquisa de Ve�culo VLT
$pmp_veiculo_oeste = $this->medoo->select('veiculo', '*', ["AND" =>['cod_linha' => 1, 'cod_grupo' => 22 ]]);
$pmp_veiculo_cariri = $this->medoo->select('veiculo', '*', ["AND" =>['cod_linha' => 2, 'cod_grupo' => 22 ]]);
$pmp_veiculo_sobral = $this->medoo->select('veiculo', '*', ["AND" =>['cod_linha' => 7, 'cod_grupo' => 22 ]]);
$pmp_veiculo_parangaba_mucuripe = $this->medoo->select('veiculo', '*', ["AND" =>['cod_linha' => 6, 'cod_grupo' => 22 ]]);


$btnsAcao = "";
$btnsAcao .= btnSalvarPmp();
//$btnsAcao .= btnImprimirPmp();

require_once(ABSPATH . "/views/_includes/formularios/pmp/mr/mrVlt.php");

unset ($_SESSION['refillPmp']);