<div class="page-header">
    <h1>Engenharia - Programa��o</h1>
</div>
<div class="row">
    
    <!-- FullCalendar -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div id="calendarioProgramacaoCCM"></div>

            <div id="modalCalendarioProgramacaoCCM" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">x</span>
                                <span class="sr-only">close</span>
                            </button>

                            <h4 id="modalTitleCalendarioProgramacaoCCM" class="modal-title"></h4>
                        </div>

                        <div id="modalBodyCalendarioProgramacaoCCM" class="modal-body"></div>

                        <div class="modal-footer">
                            <button class="btn btn-primary btnAbrirSspCalendario">Abrir Ssp</button>
                            <button class="btn btn-success btnAbrirOspCalendario">Abrir Osp's</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 2em">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSspAberta">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSspAberta" aria-expanded="false"
                               aria-controls="collapseOspExecucao">
                                <button class="btn btn-default"><label>SSP</label></button>
                            </a>
                        </div>
                    </div>
                </div>

                <div id="tabelaSspAberta" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="tabelaSspAberta">
                    <div class="panel-body">
                        <table id="indicadorSsp" class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                                <tr role="row">
                                    <th>N�</th>
                                    <th>Servi�o</th>
                                    <th>Data Programada</th>
                                    <th>Qtd de dias</th>
                                    <th>Status</th>
                                    <th>A��o</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $ssp = $this->medoo->select('v_ssp', '*', ['AND' =>
                                        [
                                            "nome_status[!]" => ["Autorizada", "Cancelada", "Encerrada"]
                                        ]
                                    ]);
                                    if(!empty($ssp)){
                                        foreach($ssp as $dados){
                                            echo('<tr>');
                                            echo('<td>'.$dados['cod_ssp'].'</td>');
                                            echo('<td><span class="primary-info" rel="tooltip-wrapper" data-placement="right" data-title="'.$dados['complemento'].'">'.$dados['nome_servico'].'</span></td>');
                                            echo('<td>'.$this->parse_timestamp($dados['data_programada']).'</td>');
                                            echo('<td>'.$dados['dias_servico'].'</td>');
                                            echo('<td>'.$dados['nome_status'].'</td>');
                                            echo('<td>');
                                            echo('<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-eye fa-fw"></i></button> ');
                                            echo('<button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                            echo('</td>');
                                            echo('</tr>');
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="OSP">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>OSP</label></div>
                <div class="panel-body">
                    <table id="indicadorOspExecucao" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr role="row">
                                <th>N� Osp</th>
                                <th>N� Ssp</th>
                                <th>Data de Abertura</th>
                                <th>Equipe</th>
                                <th>Local</th>
                                <th>A��o</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                $sql = "SELECT * FROM osp "
                                    . "JOIN status_osp ostatus USING (cod_ospstatus) "
                                    . "JOIN v_ssp ssp ON ssp.cod_ssp = osp.cod_ssp "
                                    . "WHERE ostatus.cod_status = 10";

                                $osp = $this->medoo->query($sql);

                                if(!empty($osp)){
                                    foreach($osp as $dados){
                                        echo('<tr>');
                                        echo('<td>'.$dados['cod_osp'].'</td>');
                                        echo('<td>'.$dados['cod_ssp'].'</td>');
                                        echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                        echo('<td><span class="primary-info" data-placement="right" rel="tooltip-wrapper" data-title="'.$dados['nome_equipe'].' - '.$dados['nome_unidade'].'">'.$dados['sigla_equipe'].' - '.$dados['sigla_unidade'].'</span></td>');
                                        echo('<td><span class="primary-info">'.$dados['nome_linha'].'</span><br /><span class="sub-info">'.$dados['descricao_trecho'].'</span></td>');
                                        echo('<td>');
                                        echo('<button class="btn btn-default btn-circle btnEditarOsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-eye fa-fw"></i></button> ');
                                        echo('<button class="btn btn-default btn-circle btnImprimirOsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>