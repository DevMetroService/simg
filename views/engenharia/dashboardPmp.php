<div class="page-header">
    <h1>Engenharia - Gerador PMP</h1>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-university fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeEd);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Edifica��es Ativas e Abertas</div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa icon-iconRa fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeRa);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Rede A�rea Ativas e Abertas</div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-bolt fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeSb);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Subesta��o Ativas e Abertas</div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-road fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeVp);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Via Permanente Ativas e Abertas</div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-phone fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeTl);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Telecom Ativas e Abertas</div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-ticket fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeBl);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Bilhetagem Ativas e Abertas</div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-pagelines fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <div class="huge" id="quantSafCcm">
                            <?php
                            echo($this->quantidadeJd);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div>Jardins e �reas Verdes</div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel ">
            <a href="<?php echo HOME_URI; ?>/dashboardGeral/csv/PmpCompleto"><button class="btn btn-default btn-lg"><i class="fa fa-file"></i> Pmp Completo</button></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPED">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#tabelaPMPED" aria-expanded="false" aria-controls="collapsePMPED">
                            <button class="btn btn-default"><label>PMP - Edifica��es</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPED" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPED">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPEdificacao">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Edifica��es - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPEdificacaoQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPED" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th>N�</th>
                                <th>Servi�o</th>
                                <th>Quinzena Inicial</th>
                                <th>Periodicidade</th>
                                <th>A��es</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpEdificacao = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 21]]);

                        if($pmpEdificacao) {
                            foreach ($pmpEdificacao as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Edifica��o' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPRA">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#tabelaPMPRA" aria-expanded="false" aria-controls="collapsePMPRA">
                            <button class="btn btn-default"><label>PMP - Rede A�rea</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPRA" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPRA">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPRedeAerea">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Rede A�rea - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPRedeAereaQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPRA"
                           class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th>N�</th>
                                <th>Servi�o</th>
                                <th>Quinzena Inicial</th>
                                <th>Periodicidade</th>
                                <th>A��es</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpRedeAerea = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 20]]);

                        if($pmpRedeAerea) {
                            foreach ($pmpRedeAerea as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Rede Aerea' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPSB">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse"
                           href="#tabelaPMPSB" aria-expanded="false"
                           aria-controls="collapsePMPSB">
                            <button class="btn btn-default"><label>PMP - Subesta��o</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPSB" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPSB">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPSubestacao">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Subesta��o - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPSubestacaoQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPSB"
                           class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th>N�</th>
                                <th>Servi�o</th>
                                <th>Quinzena Inicial</th>
                                <th>Periodicidade</th>
                                <th>A��es</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpSubestacao = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 25]]);

                        if($pmpSubestacao) {
                            foreach ($pmpSubestacao as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Subesta��o' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPVP">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse"
                           href="#tabelaPMPVP" aria-expanded="false"
                           aria-controls="collapsePMPVP">
                            <button class="btn btn-default"><label>PMP - Via Permanente</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPVP" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPVP">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPViaPermanente">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Via Permanente - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPViaPermanenteQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPVP"
                           class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th>N�</th>
                                <th>Servi�o</th>
                                <th>Quinzena Inicial</th>
                                <th>Periodicidade</th>
                                <th>A��es</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpViaPermanente = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 24]]);

                        if($pmpViaPermanente) {
                            foreach ($pmpViaPermanente as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Via Permanente' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPTL">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#tabelaPMPTL" aria-expanded="false" aria-controls="collapsePMPTL">
                            <button class="btn btn-default"><label>PMP - Telecom</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPTL" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPTL">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPTelecom">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Telecom - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPTelecomQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPTL" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr>
                            <th>N�</th>
                            <th>Servi�o</th>
                            <th>Quinzena Inicial</th>
                            <th>Periodicidade</th>
                            <th>A��es</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpTelecom = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 27]]);

                        if($pmpTelecom) {
                            foreach ($pmpTelecom as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Telecom' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPBL">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#tabelaPMPBL" aria-expanded="false" aria-controls="collapsePMPBL">
                            <button class="btn btn-default"><label>PMP - Bilhetagem</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPBL" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPBL">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPBilhetagem">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Bilhetagem - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPBilhetagemQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPBL" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr>
                            <th>N�</th>
                            <th>Servi�o</th>
                            <th>Quinzena Inicial</th>
                            <th>Periodicidade</th>
                            <th>A��es</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpBilhetagem = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 28]]);

                        if($pmpBilhetagem) {
                            foreach ($pmpBilhetagem as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Bilhetagem' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php /*
<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPMR">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse"
                           href="#tabelaPMPMR" aria-expanded="false"
                           aria-controls="collapsePMPMR">
                            <button class="btn btn-default"><label>PMP - Material Rodante VLT</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPMR" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPMR">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPVlt">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Material Rodante Vlt - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPVltQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPMR"
                           class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr>
                            <th>N�</th>
                            <th>Servi�o</th>
                            <th>Quinzena Inicial</th>
                            <th>Periodicidade</th>
                            <th>A��es</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpMrVLT = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 22]]);

                        if($pmpMrVLT) {
                            foreach ($pmpMrVLT as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Material Rodante' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
*/ ?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingPMPJD">
                <div class="row">
                    <div class="col-md-2">
                        <a class="collapsed" role="button" data-toggle="collapse" href="#tabelaPMPJD" aria-expanded="false" aria-controls="collapsePMPJD">
                            <button class="btn btn-default"><label>PMP - Jardins e �reas Verdes</label></button>
                        </a>
                    </div>
                </div>
            </div>
            <div id="tabelaPMPJD" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="tabelaPMPJD">
                <div class="panel-heading">
                    <button class="btn btn-default btnImprimirPMPJardins">
                        <i class="fa fa-print fa-fw fa-1x"></i> Imprimir PMP Jardins e �reas Verdes - Provis�rio
                    </button>
                    <button class="btn btn-success btnImprimirPMPJardinsQzn">
                        <i class="fa fa-eye fa-fw fa-1x"></i> Visualiza��o por Quinzena
                    </button>
                </div>
                <div class="panel-body">
                    <table id="indicadorPMPJD" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr>
                            <th>N�</th>
                            <th>Servi�o</th>
                            <th>Quinzena Inicial</th>
                            <th>Periodicidade</th>
                            <th>A��es</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pmpJardins = $this->medoo->select("pmp",
                            [
                                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                                '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
                                '[><]servico_pmp' => 'cod_servico_pmp',
                                '[><]tipo_periodicidade' => "cod_tipo_periodicidade"
                            ], [
                                'cod_pmp',
                                'nome_servico_pmp',
                                'quinzena',
                                'nome_periodicidade'
                            ], ['AND' => ['ativo[!]' => 'D', 'pmp.cod_grupo' => 31]]);

                        if($pmpJardins) {
                            foreach ($pmpJardins as $dados) {
                                echo('<tr>');
                                echo("<td>{$dados['cod_pmp']}<input type='hidden' value='Jardins e �reas Verdes' /></td>");
                                echo("<td>{$dados['nome_servico_pmp']}</td>");
                                echo("<td>{$dados['quinzena']}</td>");
                                echo("<td>{$dados['nome_periodicidade']}</td>");
                                echo ("<td><button type='button' class='abrirModal btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirPmp'><i class='fa fa-eye'></i></button></td>");
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->dashboard->modalExibirPmp() ?>