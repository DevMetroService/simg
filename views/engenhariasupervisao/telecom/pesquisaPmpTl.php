<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 25/10/2016
 * Time: 09:32
 */
require_once(ABSPATH . '/functions/modulosPMP.php');

$refill = $_SESSION['dadosPesquisaPmpTl'];

$local;
$selectLocal = $this->medoo->select("local", ["cod_local", "nome_local"]);
if ($selectLocal)
    foreach ($selectLocal as $dados)
        $local[$dados['cod_local']] = $dados['nome_local'];

$sistema;
$selectSistema = $this->medoo->select('grupo_sistema',
    ["[><]sistema" => "cod_sistema"],
    ['nome_sistema', 'cod_sistema'],
    ['ORDER' => 'nome_sistema', "cod_grupo" => 27]);
if ($selectSistema)
    foreach ($selectSistema as $dados)
        $sistema[$dados['cod_sistema']] = $dados['nome_sistema'];

$subsistema;
$selectSubsistema = $this->medoo->select("subsistema", ["cod_subsistema", "nome_subsistema"]);
if ($selectSubsistema)
    foreach ($selectSubsistema as $dados)
        $subsistema[$dados['cod_subsistema']] = $dados['nome_subsistema'];

$servico;
$selectServico = $this->medoo->select("servico_pmp", ["cod_servico_pmp", "nome_servico_pmp"], ['cod_grupo' => 27]);
if ($selectServico)
    foreach ($selectServico as $dados)
        $servico[$dados['cod_servico_pmp']] = $dados['nome_servico_pmp'];

$procedimento;
$selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 27]);
if ($selectProcedimento)
    foreach ($selectProcedimento as $dados => $value)
        $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];


if (!empty($refill['sistema']))
    $subsistemaTl = $this->medoo->select('sub_sistema',
        ["[><]subsistema" => "cod_subsistema"],
        ['nome_subsistema', 'cod_subsistema'],
        ['cod_sistema' => $refill['sistema']]);

if (!empty($refill['subSistema']))
    $servicoPmpTl = $this->medoo->select('servico_pmp',
        ['[><]sub_sistema' => 'cod_sub_sistema'],
        ['nome_servico_pmp', 'cod_servico_pmp'],
        ["AND" => ['sub_sistema.cod_subsistema' => $refill['subSistema'], 'cod_grupo' => 27]]);

$selectPerio = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
if($selectPerio)
    foreach ($selectPerio as $dados => $value)
        $periodicidade[$value['cod_tipo_periodicidade']] = $value['nome_periodicidade'];

require_once(ABSPATH . "/views/_includes/formularios/pmp/tl/pesquisaPmp.php");

$this->dashboard->modalExibirPmp();