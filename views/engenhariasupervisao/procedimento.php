<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 06/10/2016
 * Time: 09:44
 */

if ($_SESSION['refillPmp']) {
    $refill = $_SESSION['refillPmp'];
}

$tipoGrupo = $_SESSION['tipoGrupo'];
$codGrupo;

switch ($tipoGrupo){
    case 'E':
        $nomeSistemaFormulario = 'Edifica��o';
        $codGrupo = 21;
        break;
    case 'R':
        $nomeSistemaFormulario = 'Rede A�rea';
        $codGrupo = 20;
        break;
    case 'V':
        $nomeSistemaFormulario = 'Via Permanente';
        $codGrupo = 24;
        break;
    case 'S':
        $nomeSistemaFormulario = 'Subesta��o';
        $codGrupo = 25;
        break;
    case 'T':
        $nomeSistemaFormulario = 'Telecom';
        $codGrupo = 27;
        break;
    case 'B':
        $nomeSistemaFormulario = 'Bilhetagem';
        $codGrupo = 28;
        break;
    case 'Tv':
        $nomeSistemaFormulario = 'Transportes Verticais';
        $codGrupo = 30;
        break;
    case 'J':
        $nomeSistemaFormulario = 'Jardins e �reas Verdes';
        $codGrupo = 31;
        break;
    case 'M':
    default :
        $nomeSistemaFormulario = 'Material Rodante VLT';
        $codGrupo = 22;
        break;
}

$sistemaProc = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => $codGrupo]);
if(!$sistemaProc)
    $sistemaProc = null;

$subSistemaProc = null;
if(!empty($refill['sistema']))
    $subSistemaProc = $this->medoo->select('sub_sistema', ["[><]subsistema" => "cod_subsistema"], ['nome_subsistema', 'cod_subsistema'], ['cod_sistema' => $refill['sistema']]);

$servicoPmpProc = null;
if(!empty($refill['subSistema'])) {
    $servicoPmpProc = $this->medoo->query("SELECT sp.nome_servico_pmp, sp.cod_servico_pmp
                                              FROM servico_pmp_sub_sistema sss 
                                              JOIN servico_pmp sp ON sss.cod_servico_pmp = sp.cod_servico_pmp
                                              JOIN sub_sistema ss ON sss.cod_sub_sistema = ss.cod_sub_sistema
                                              WHERE ss.cod_sistema = {$refill['sistema']} AND ss.cod_subsistema = {$refill['subSistema']} AND cod_grupo = '{$codGrupo}'")->fetchAll();

}


$selectSistema = null;
$sistema = $this->medoo->select("sistema", ['nome_sistema', 'cod_sistema'], ["ORDER" => "nome_sistema"]);
if($sistema)
    foreach($sistema as $dados)
        $selectSistema[$dados['cod_sistema']] = $dados['nome_sistema'];


$selectSubSistema = null;
$subSistema = $this->medoo->select("subsistema", ['nome_subsistema', 'cod_subsistema'], ["ORDER" => "nome_subsistema"]);
if($subSistema)
    foreach($subSistema as $dados)
        $selectSubSistema[$dados['cod_subsistema']] = $dados['nome_subsistema'];


$selectPeriodicidade = null;
$periodicidade = $this->medoo->select("tipo_periodicidade", ['cod_tipo_periodicidade', 'nome_periodicidade'], ["ORDER" => "cod_tipo_periodicidade"]);
if($periodicidade)
    foreach($periodicidade as $dados)
        $selectPeriodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


$selectProcedimento = null;
$procedimento = $this->medoo->select("procedimento", ['cod_procedimento', 'nome_procedimento'], ["ORDER" => "cod_procedimento", "cod_grupo" => $codGrupo]);
if($procedimento)
    foreach($procedimento as $dados)
        $selectProcedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];


$servicoPmpPeriodicidade = $this->medoo->query("SELECT
                                  cod_servico_pmp_periodicidade, s.cod_sistema, su.cod_subsistema, 
                                  sp.nome_servico_pmp, cod_tipo_periodicidade, cod_procedimento
                                  
                                  FROM servico_pmp_periodicidade
                                  JOIN servico_pmp_sub_sistema sss USING (cod_servico_pmp_sub_sistema)
                                  JOIN servico_pmp sp ON sss.cod_servico_pmp = sp.cod_servico_pmp
                                  JOIN sub_sistema ss ON sss.cod_sub_sistema = ss.cod_sub_sistema
                                  JOIN subsistema su ON ss.cod_subsistema = su.cod_subsistema
                                  JOIN sistema s ON ss.cod_sistema = s.cod_sistema
                                  WHERE cod_grupo = '{$codGrupo}'")->fetchAll();


require_once(ABSPATH . "/views/_includes/formularios/pmp/cadastroProcedimento.php");

unset ($_SESSION['refillPmp']);