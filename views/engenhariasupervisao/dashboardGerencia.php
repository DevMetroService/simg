<div class="page-header">
    <h1>Engenharia - Supervis�o</h1>
</div>

<div class="row">
    
    <div class="row" class="scrollAuto">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-file-text-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                <?php
                                echo($this->quantidadeSAF);
                                ?>
                            </div>
                            <div>SAF(s) Aberta(s)</div>
                        </div>
                    </div>
                </div>
                <a href="#SAF">
                    <div class="panel-footer">
                        <span class="pull-left">Ver S.A.F's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="<?php
                            if ($this->quantidadeSSM == 0) {
                                echo 'fa fa-cog fa-5x';
                            } else {
                                echo 'fa fa-cog fa-spin fa-5x';
                            }
                            ?>"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php
                                echo $this->quantidadeSSM
                                ?></div>
                            <div>SSM(s) Abertas</div>
                        </div>
                    </div>
                </div>
                <a href="#ssmAberta">
                    <div class="panel-footer">
                        <span class="pull-left">Ver SSM's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="<?php
                            if ($this->quantidadeOSM == 0) {
                                echo 'fa fa-cog fa-5x';
                            } else {
                                echo 'fa fa-cog fa-spin fa-5x';
                            }
                            ?>"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php
                                echo $this->quantidadeOSM
                                ?></div>
                            <div>OSM em Execu��o</div>
                        </div>
                    </div>
                </div>
                <a href="#OSM">
                    <div class="panel-footer">
                        <span class="pull-left">Ver OSM's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <?php
    $this->dashboard->auditoriaSaf($this->dadosUsuario['cod_usuario']);

    $safDev = $this->medoo->select("v_saf", "*", [
        "AND" => [
            "nome_status" => "Devolvida",
            "usuario" => $this->dadosUsuario['usuario']
        ]
    ]);
    $quantDevolvida = count($safDev);
    if($quantDevolvida > 0){ ?>
        <div class="row" id="safDevolvida">
            <div class="col-md-12">
                <div class='panel-danger panel'>
                    <div class="panel-heading" role="tab" id="headingSafDevolvida">
                        <div class="row">
                            <div class="col-xs-3">
                                <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorSafDevolvida" aria-expanded="false" aria-controls="collapseSafDevolvida">
                                    <button class="btn btn-default"><label>SAF's Devolvidas</label></button>
                                </a>
                            </div>
                            <?php
                            if ($quantDevolvida > 0)
                                echo"<div class='col-xs-9'><h4>".$quantDevolvida." Saf(s) Devolvida(s)</h4></div>";
                            ?>
                        </div>
                    </div>
                    <div id="divIndicadorSafDevolvida" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingSafDevolvida">
                        <div class="panel-body"  id="tabelaSafDevolvida">
                            <table id="indicadorSafDevolvida" class="table table-striped table-bordered no-footer">
                                <thead id="headIndicadorSaf">
                                <tr role="row">
                                    <th>N�</th>
                                    <th>Local</th>
                                    <th>Data de Abertura</th>
                                    <th>Motivo</th>
                                    <th>N�vel</th>
                                    <th>A��o</th>
                                </tr>
                                </thead>
                                <tbody id="bodyIndicadorSafDevolvida">
                                <?php

                                if (!empty($safDev)) {
                                    foreach ($safDev as $dados) {
                                        echo('<tr>');
                                        echo('<td>' . $dados['cod_saf'] . '</td>');
                                        echo('<td>' . $dados['nome_linha'] . ' ' . $dados['nome_trecho'] . '</td>');
                                        echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                        echo('<td>' . $dados['descricao_status'] . '</td>');
                                        echo('<td>' . $dados['nivel'] . '</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/saf/returned/{$dados['cod_saf']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="row" id="SAF">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SAF</label></div>
                <div class="panel-body" id="divIndicadorSaf">
                    <table id="indicadorSaf" class="table table-striped table-bordered no-footer">
                        <thead id="headIndicadorSaf">
                        <tr role="row">
                            <th>N�</th>
                            <th>Local</th>
                            <th>Data de Abertura</th>
                            <th>Status</th>
                            <th>N�vel</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody id="bodyIndicadorSaf">
                        <?php
                        $saf = $this->medoo->select("v_saf", "*",[
                            "nome_status" => "Aberta"
                        ]);

                        if(!empty($saf)){
                            foreach($saf as $dados){
                                echo('<tr>');
                                echo('<td>'.$dados['cod_saf'].'</td>');
                                echo('<td>'.$dados['nome_linha'] .' '. $dados['nome_trecho'].'</td>');
                                echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                echo('<td>'.$dados['nome_status'].'</td>');
                                echo('<td>'.$dados['nivel'].'</td>');
                                echo('<td>');
                                echo('<button class="btn btn-default btn-circle btnVisualizarSaf" type="button" title="Ver Dados Gerais"><i class="fa fa-eye fa-fw"></i></button> ');
                                echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="ssmAberta">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SSM Abertas</label></div>
                <div class="panel-body">
                    <table id="indicadorSsmAberta" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr role="row">
                            <th>N�</th>
                            <th>N� SAF</th>
                            <th>Data de Abertura</th>
                            <th>Status</th>
                            <th>Nivel</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $saf = $this->medoo->select("v_ssm", "*",["cod_status" => 9]);

                        if(!empty($saf)){
                            foreach($saf as $dados){
                                echo('<tr>');
                                echo('<td>'.$dados['cod_ssm'].'</td>');
                                echo('<td>'.$dados['cod_saf'].'</td>');
                                echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                echo('<td>'.$dados['nome_status'].'</td>');
                                echo('<td>'.$dados['nivel'].'</td>');
                                echo('<td>');
                                echo('<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Ver Dados Gerais"><i class="fa fa-eye fa-fw"></i></button> ');
                                echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SSM Devolvidas</label></div>
                <div class="panel-body">
                    <table id="indicadorSsmDevolvida" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr role="row">
                            <th>N�</th>
                            <th>N� SAF</th>
                            <th>Data de Abertura</th>
                            <th>Status</th>
                            <th>Motivo</th>
                            <th>Nivel</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $saf = $this->medoo->select("v_ssm", "*",["cod_status" => 6]);

                        if(!empty($saf)){
                            foreach($saf as $dados){
                                echo('<tr>');
                                echo('<td>'.$dados['cod_ssm'].'</td>');
                                echo('<td>'.$dados['cod_saf'].'</td>');
                                echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                echo('<td>'.$dados['nome_status'].'</td>');
                                echo('<td>'.$dados['descricao_status'].'</td>');
                                echo('<td>'.$dados['nivel'].'</td>');
                                echo('<td>');
                                echo('<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Ver Dados Gerais"><i class="fa fa-eye fa-fw"></i></button> ');
                                echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SSM Encaminhadas e Transferidas</label></div>
                <div class="panel-body">
                    <table id="indicadorSsmEncaminhada" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr role="row">
                            <th>N�</th>
                            <th>N� Saf</th>
                            <th>Encaminhamento</th>
                            <th>Status</th>
                            <th>Nivel</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $saf = $this->medoo->select("v_ssm", "*",[
                            "cod_status" => [12,8]
                        ]);

                        if(!empty($saf)){
                            foreach($saf as $dados){
                                echo('<tr>');
                                echo('<td>'.$dados['cod_ssm'].'</td>');
                                echo('<td>'.$dados['cod_saf'].'</td>');
                                echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                echo('<td>'.$dados['nome_status'].'</td>');
                                echo('<td>'.$dados['nivel'].'</td>');
                                echo('<td>');
                                echo('<button class="btn btn-default btn-circle btnEditarSsm" type="button" title="Ver Dados Gerais"><i class="fa fa-eye fa-fw"></i></button> ');
                                echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="OSM">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>OSM</label></div>
                <div class="panel-body">
                    <table id="indicadorOsmExecucao" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                        <tr role="row">
                            <th>N�</th>
                            <th>N� SSM</th>
                            <th>Data de Abertura</th>
                            <th>Unidade</th>
                            <th>Equipe</th>
                            <th>Nivel</th>
                            <th>A��o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $osm = $this->medoo->select("osm_falha",["[><]status_osm" => "cod_ostatus"], "*",["cod_status" => 10]);

                        if(!empty($osm)){
                            foreach($osm as $dados){

                                $ssm = $this->medoo->select("v_ssm", "*",["cod_ssm" => (int)$dados['cod_ssm']]);
                                $ssm = $ssm[0];

                                echo('<tr>');
                                echo('<td>'.$dados['cod_osm'].'</td>');
                                echo('<td>'.$dados['cod_ssm'].'</td>');
                                echo('<td>'.$this->parse_timestamp($dados['data_status']).'</td>');
                                echo('<td>'.$ssm['nome_unidade'].'</td>');
                                echo('<td>'.$ssm['nome_equipe'].'</td>');
                                echo('<td>'.$ssm['nivel'].'</td>');
                                echo('<td>');
                                echo('<button class="btn btn-default btn-circle btnEditarOsm" type="button" title="Ver Dados Gerais"><i class="fa fa-eye fa-fw"></i></button> ');
                                echo('</tr>');
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

