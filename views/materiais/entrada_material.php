<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">

            <div class="panel-heading">Entrada de Material</div>

            <div class="panel-body">

                <form id="formPrincipal" class="form-group" method="post" role="form"
                      action="<?php echo(HOME_URI); ?>cadastroMateriais/entradaMaterial">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Adicionar Materiais/Pedidos de Compra</div>
                                <div class="panel-body">

                                    <row>
                                        <div id="teste"></div>
                                    </row>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label>Pesquisar Pedido</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control number"
                                                       placeholder="C�DIGO DO PEDIDO"
                                                       name="codPedidoComprasMaterial">
                                                  <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i
                                                            class="fa fa-search"
                                                            style="padding-top: 20%;padding-bottom: 20%;"></i></button>
                                                  </span>
                                            </div>
                                        </div>
                                    </div><!-- /row -->

                                    <br/>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <label>Material</label>
                                            <?php
                                            $consultaMaterial = $this->medoo->select("v_material", "nome_material");
                                            if (!empty($consultaMaterial)) {
                                                $consultaMaterial = array_combine(range(1, count($consultaMaterial)), $consultaMaterial);
                                            }

                                            echo('<select name="descMaterial" class="form-control"
                                                required data-validation-required-message=" Campo precisa ser preenchido"
                                                data-bv-row=".col-lg-4"
                                                data-fv-notempty="true"
                                                data-fv-notempty-message="Selecione a unidade de medida.">');
                                            echo('<option selected="selected" value="" disabled="disabled">Escolha o Material</option>');

                                            foreach ($consultaMaterial as $key => $value) {
                                                echo "<option value=\"$key\" >$value</option>";
                                            }
                                            echo('</select>');

                                            ?>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Fornecedor</label>
                                            <select name="nomeFornecedor" class="form-control" required>
                                                <option value="" selected disabled>Escolha o Fornecedor</option>
                                                <?php
                                                $fornecedor = $this->medoo->select("fornecedor", ["cod_fornecedor", "nome_fantasia"]);

                                                foreach ($fornecedor as $key => $value) {
                                                    echo "<option value='{$value['cod_fornecedor']}'>{$value['nome_fantasia']}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Quantidade</label>
                                            <input type="text" class="form-control number" name="qtdSolicitada"
                                                   required>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Unidade</label>
                                            <select class="form-control" name="unidade" id="unidade" required>
                                                <option value="" selected disabled>Unid.</option>
                                                <?php
                                                $unidade = $this->medoo->select("unidade_medida", ["sigla_uni_medida", "cod_uni_medida", "nome_uni_medida"]);

                                                foreach ($unidade as $key => $value) {
                                                    echo "<option value='{$value['cod_uni_medida']}' >{$value['sigla_uni_medida']} - {$value['nome_uni_medida']}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div><!-- /row -->

                                    <br/>

                                    <!-- btn AddPedido-->
                                    <div class="row">
                                        <div class="col-md-2">
                                            <button id="btnAddMaterial" type="button" class="btn btn-default btn-lg"><i
                                                    class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                </div><!-- /panel-body -->
                            </div><!-- /panel-default -->
                        </div> <!-- /col-md-12 -->
                    </div><!-- /row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lista de Materiais para Entrada</div>

                                <div class="panel-body">
                                    <table id="tabelaListaDeMateriais"
                                           class="table table-bordered table-striped table-responsive">
                                        <thead>
                                        <tr>
                                            <th>n�</th>
                                            <th>Nome Material</th>
                                            <th>Fornecedor</th>
                                            <th>Quantidade</th>
                                            <th>Unidade</th>
                                            <th>A��es</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 btn-group" style="float: right; ">
                            <div style="float: right">
                                <button id="btnCadastrarMateriais" type="submit" class="btn btn-success btn-lg"
                                        style="margin-right: 1em;">Cadastrar Materiais
                                </button>
                                <button id="zerar" class="btn btn-default btn-lg" type="reset">Zerar Dados</button>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>