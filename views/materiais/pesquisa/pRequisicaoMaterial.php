<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/12/2016
 * Time: 15:25
 */
?>

<div class="page-header">
    <h2>Pesquisa Requisi��es de Materiais</h2>
</div>

<!-- Conteudo -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">

            <!-- Panel Head -->
            <div class="panel-heading"></div>
            <!-- / Panel Head -->


            <!-- Panel Body -->
            <div class="panel-body">

                <!-- Area dos Filtros de Pesquisa-->
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading">Filtros de Pesquisa</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>C�d. da Requisi��o</label>
                                    <input type="text" class="form-control number">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>�rea Solicitante</label>
                                    <select class="form-control" name="areaSolicitante" id="areaSolicitante">
                                        <option value="_area">_area</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Status da Solicita��o</label>
                                    <select class="form-control" name="statusSolicitacao" id="statusSolicitacao">
                                        <option value="_status">_status</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Nome do Solicitante</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Nome do Aprovador</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>Data / Apartir</label>
                                    <input type="text" class="form-control data">
                                </div>
                                <div class="col-md-3">
                                    <label>Data / At�</label>
                                    <input type="text" class="form-control data">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <button id="btnPesquisar" type="button" class="btn btn-success btn-lg btnPesquisar" style="margin-right: 15px">Pesquisar</button>
                                    <button id="btnResetarPesquisa" type="button" class="btn btn-default btn-lg btnResetarPesquisa" style="margin-right: 15px">Resetar Filtro</button>
                                </div>
                            </div>
                        </div>

                        <br>

                    </div>
                </div>
                <!-- / Area dos Filtros de Pesquisa-->

                <!-- Area dos Resultados da Pesquisa-->
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading">Resultados da Pesquisa</div>
                        
                        <div class="panel-body">
                            <div class="row">
                                <table id="tabela" class="table table-bordered table-striped table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>C�d. Req</th>
                                            <th>Nome Solicitante</th>
                                            <th>Qtd. Materiais</th>
                                            <th>Data Solicita��o</th>
                                            <th>Status Requisi��o</th>
                                            <th>A��es</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>0001</td>
                                            <td>Abner Israel</td>
                                            <td>70</td>
                                            <td>28-12-2017</td>
                                            <td>Em espera</td>
                                            <td>
                                                <button class="btnToolTip btn btn-circle btn-success" title="Clique para Aprovar a Requisi��o" data-toggle="tooltip" data-placement="left"><i class="fa fa-check"></i></button>
                                                <button class="btnToolTip btn btn-circle btn-primary" title="Clique para Visualizar a Requisi��o" data-target="#modalVisualizar" data-toggle="modal" data-placement="left"><i class="fa fa-eye"></i></button>
                                                <button class="btnToolTip btn btn-circle btn-default" title="Clique para Imprimir a Requisi��o" data-toggle="tooltip" data-placement="left"><i class="fa fa-file-o"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / Area dos Resultados da Pesquisa-->

            </div>
            <!-- / Panel Body -->

        </div>
    </div>
</div>
<!-- / Conteudo -->



<!-- Area dos Modals -->

<!-- Modal-Visualizar -->
<div class="modal fade" id="modalVisualizar" tabindex="-1" role="dialog">

    <div class="modal-dialog modal-lg" role="document">

        <!-- Modal-Content -->
        <div class="modal-content">
            <!-- modal-header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3 class="modal-title">Detalhes da Requisi��o</h3>
            </div>
            <!-- / modal-header -->

            <!-- modal-body -->
            <div id="modal_body" class="modal-body"
                 style="position: relative; height: 60%;overflow-y: scroll;">

                <!-- Area Dados da Requisicao  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Dados da Requisi��o</div>

                            <div class="panel-body">

                                <form id="dadosDaRequisicao">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>C�d. da Requisi��o</label>
                                            <input type="text" class="form-control" name="modal_codReq" disabled>
                                        </div>
                                        <div class="col-md-4">
                                            <label>�rea Solicitante</label>
                                            <input type="text" class="form-control" name="modal_areaSolicitante" disabled>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Status</label>
                                            <input type="text" class="form-control" name="modal_statusReq" disabled>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Dados do Requisitante</div>

                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Requisitante</label>
                                                            <input class="form-control" value=""
                                                                   placeholder="Nome do Requisitante" type="text"
                                                                   required
                                                                   name="funcionarioRequisitante"
                                                                   id="funcionarioRequisitante" disabled>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Data e Hora da Requisi��o</label>
                                                            <input class="form-control timeStamp" value="" required
                                                                   type="text" name="dataRequisicao"
                                                                   id="dataRequisicao" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Dados da Aprova��o</div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Aprovado por:</label>
                                                            <input class="form-control" value="" required
                                                                   placeholder="Nome do Aprovador" type="text"
                                                                   name="aprovadorRequisicao" id="aprovadorRequisicao" disabled>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Data e Hora da Aprova��o</label>
                                                            <input class="form-control timeStamp" value="" required
                                                                   type="text" name="dataAprovacao" id="dataAprovacao" disabled>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Observa��es:</label>
                                        <textarea class="form-control" rows="5" id="observacoes"
                                                  name="txtObservacoes" disabled></textarea>
                                        </div>
                                    </div>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- / Area Dados da Requisicao  -->

                <!-- Tabela com Lista de Materiais -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"> Lista de Materiais Requisitados</div>

                            <div class="panel-body">
                                <div class="row">
                                    <table id="tabela_modal_visualizar" class="table table-bordered table-striped table-hover dataTable no-footer">
                                        <thead>
                                        <tr>
                                            <th>N</th>
                                            <th>Cod. Material / Desc. Material</th>
                                            <th>OS</th>
                                            <th>Qtd. / Unid. Solicitada</th>
                                            <th>Qtd. Entregue</th>
                                            <th>Data/Hora Entrega</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">1</td>
                                            <td>11-TOUCA </td>
                                            <td>0001</td>
                                            <td>70-pk</td>
                                            <td>70</td>
                                            <td>29-12-2017 10:00</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / Tabela com Lista de Materiais -->

            </div>
            <!-- / modal-body -->

            <!-- modal-footer -->
            <div class="modal-footer">
                <div class="btn-group">
                    <button id="btn-modalAprovar" class="btnToolTip btn btn-success" title="Clique para Aprovar esta Requisi��o" data-toggle="tooltip" data-placement="top" style="margin-right: 15px"><i class="fa fa-check fa-fw fa-3x" title="Salvar Altera��es"></i></button>
                    <button id="btn-modalFecharJanela"type="button" class="btnToolTip btn btn-default" data-dismiss="modal" title="Fechar Janela" data-toggle="tooltip" data-placement="top"><i class="fa fa-close fa-fw fa-3x"></i></button>
                </div>
            </div>
            <!-- / modal-footer -->

        </div>
        <!-- / Modal-Content -->

    </div>

</div>
<!-- / Modal-Visualizar -->

<!-- / Area dos Modals-->