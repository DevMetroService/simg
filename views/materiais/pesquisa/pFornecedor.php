<?php
/**
 * Created by PhpStorm.
 * User: Rycker
 * Date: 07/06/2016
 * Time: 10:29
 */
?>

<!--  -->
<input id="msgUpdateFornecedor"
       type="hidden"
       value="<?php echo $_SESSION['statusUpdate']; unset($_SESSION['statusUpdate']); ?>"
>
<!-- / -->

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Pesquisa Fornecedor</div>

            <div class="panel-body">


                <!-- AREA DO FORMULARIO DE PESQUISA FORNECEDOR -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Filtro</div>
                            <div class="panel-body">
                                <form  action="<?php echo HOME_URI; ?>/dashboardMateriais/executarPesquisaFornecedor"
                                       class="form-group" method="post" id="formPesquisaFornecedor">

                                    <!-- Filtros do Formulario de Pesquisa -->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>CNPJ</label>
                                            <input id="cnpjFormatado" type="text" class="form-control cnpj" value="">
                                            <input name="cnpj" type="hidden" value="">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Avalia��o</label>
                                            <input id="avaliacao" name="avaliacao" class="form-control resetForm" type="text">
                                        </div>
<!--                                        <div class="col-md-3">-->
<!--                                            <label>Ramo da Atividade</label>-->
<!--                                            <select id="ramoAtividade" class="form-control resetForm">-->
<!--                                                <option value="" selected>:D</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
                                        <div class="col-md-4">
                                            <label>UF</label>
                                            <select id="uf" name="uf" class="form-control resetForm">
                                                <option value="" selected></option>
                                                <?php
                                                $material = $this->medoo->select('endereco_fornecedor', '*');

                                                $listaUF = [];
                                                foreach ($material as $dados => $value) {
                                                    $aux = $value['uf'];
                                                    $resp = array_search($aux, $listaUF); //retorna false se nao existir, ou o index no Array se existir

                                                    if ($resp === false && strlen($aux) != 0) {
                                                        array_push($listaUF, $aux);
                                                        echo("<option value='{$value['uf']}'>{$value['uf']}</option>");
                                                    }

                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- / Filtros do Formulario de Pesquisa -->

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / AREA DO FORMULARIO DE PESQUISA FORNECEDOR -->

                <!-- BTN�s Pesquisar e Resetar-->
                <div class="row">
                    <div class="col-md-12">
                        <button id="btnPesquisar" type="button" class="btn btn-success btn-lg btnPesquisar">Pesquisar</button>
<!--                        <button id="btnResetarPesquisa" type="button" class="btn btn-default btn-lg btnResetarPesquisa">Resetar Filtro</button>-->
                    </div>
                </div>
                <!-- / BTN�s -->

                </br>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Tabela de Fornecedores</div>
                            <div class="panel-body">
                                <table id="tabelaFornecedor" class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>UF</th>
                                        <th>Fornecedor</th>
                                        <th>Raz�o Social</th>
                                        <th>CNPJ</th>
                                        <th>Avalia��o</th>
                                        <th>Descri��o</th>
                                        <th>A��es</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    if(!empty($selectFornecedor)) {

                                        foreach($selectFornecedor as $dados => $value){

                                            echo("<tr>");
                                            echo("<td>{$value['uf']}</td>");
                                            echo("<td>{$value['nome_fantasia']}</td>");
                                            echo("<td>{$value['razao_social']}</td>");
                                            echo("<td>{$value['cnpjcpf']}</td>");
                                            echo("<td>{$value['avaliacao']}</td>");
                                            echo("<td>{$value['descricao']}</td>");
                                            echo("<td>
                                                        <button class='btnToolTip btn btn-circle btn-primary btnEditarMaterial' 
                                                        title='Clique para Editar o Fornecedor' 
                                                        data-toggle='modal'
                                                        data-placement='left'><i class='fa fa-pencil'></i></button>
                                                </td>");
                                            echo("</tr>");
                                        }

                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>





<!-- JANELA MODAL - EDITAR FORNECEDOR -->

<div class="modal fade modalEditar" id="modal-editarFornecedor" tabindex="-1" role="dialog">
    <form id="formEditarFornecedor" action="<?php echo HOME_URI; ?>/dashboardMateriais/editarFornecedor" method="post">

        <div class="modal-dialog modal-lg" role="document">

            <!-- Modal-Content -->
            <div class="modal-content">
                <!-- modal-header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h3 class="modal-title">Editar Dados do Fornecedor</h3>
                </div>
                <!-- / modal-header -->

                <!-- modal-body -->
                <div id="modal_body" class="modal-body"
                     style="position: relative; height: 60%;overflow-y: scroll;">

                    <!-- Dados do Fornecedor -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Dados Cadastrais do Fornecedor</div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input name="hidden_codFornecedor"
                                                   type="hidden"
                                                   value="<?php echo $_SESSION['statusUpdate']; unset($_SESSION['statusUpdate']); ?>"
                                            >


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Nome Fantasia</label>
                                                    <input type="text" class="form-control"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['nome_fantasia'];?>"
                                                           name="nomeFornecedor"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="razaoSocial">Raz�o Social</label>
                                                    <input type="text" class="form-control"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['razao_social'];?>"
                                                           name="razaoSocial"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="cnpjFornecedor">CNPJ</label>
                                                    <input type="text" class="form-control cnpj"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['cnpjcpf'];?>"
                                                           name="cnpjFornecedor"
                                                           placeholder="N�O H� DADOS">
                                                    <input type="hidden" name="hiddenCnpj" value="">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Optante pelo Simples:</label>
                                                    <select class="form-control" name="simplesNacionalFornecedor" required>
                                                        <option value="" selected="selected" disabled="disabled">Selecione uma op��o</option>
                                                        <option value="n" <?php if($dadosFornecedor['optante_simples'] == "n") echo "selected"?> >N�o</option>
                                                        <option value="s" <?php if($dadosFornecedor['optante_simples'] == "s") echo "selected"?> >Sim</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Inscri��o Municipal</label>
                                                    <input type="text" class="form-control number" name="inscricaoMunicipalFornecedor"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['insc_municipal'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Inscri��o Estadual</label>
                                                    <input type="text" class="form-control number" name="inscricaoEstadualFornecedor"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['insc_estadual'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>

                                            </div>

                                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                                            </div>

                                            <div class="row" style="margin-top: 2em">
                                                <div class="col-md-2">
                                                    <label for="cepFornecedorMaterial">CEP</label>
                                                    <input class="form-control cep" name="cepFornecedor" type="text"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['cep'];?>"
                                                           placeholder="N�O H� DADOS">
                                                    <input type="hidden" name="hiddenCep" value="">
                                                </div>

                                                <div class="col-md-10">
                                                    <label for="logradouroEnderecoFornecedor">Endere�o</label>
                                                    <input type="text" id="logradouro" name="logradouroEnderecoFornecedor"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['logradouro'];?>"
                                                           class="form-control"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="numeroEnderecoFornecedor">N�mero</label>
                                                    <input type="text" name="numeroEnderecoFornecedor" id="numero" class="form-control number"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['numero_endereco'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="complementoEnderecoFornecedor">Complemento</label>
                                                    <input type="text" name="complementoEnderecoFornecedor" class="form-control"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['complemento'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>

                                                <div class="col-md-2">
                                                    <label for="bairroEnderecoFornecedor">Bairro</label>
                                                    <input type="text" id="bairro" name="bairroEnderecoFornecedor" class="form-control"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['bairro'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="municipioEnderecoFornecedor">Munic�pio</label>
                                                    <input type="text" id="cidade" name="municipioEnderecoFornecedor"
                                                           class="form-control"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['municipio'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="ufEnderecoFornecedor">UF</label>
                                                    <input type="text" class="form-control" name="ufEnderecoFornecedor" id="uf"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['UF'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                                            </div>

                                            <div class="row" style="margin-top: 2em">
                                                <div class="col-md-3">
                                                    <label for="nomeContatoFornecedor">Nome do Contato</label>
                                                    <input class="form-control" name="nomeContatoFornecedor" type="text"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['nome_contato'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="telefoneContatoFornecedor">Telefone</label>
                                                    <input class="form-control telefone" name="telefoneContatoFornecedor" type="text"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['fone_fornecedor'];?>"
                                                           placeholder="N�O H� DADOS">
                                                    <input type="hidden" name="hiddenTelefone" value="">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="emailContatoFornecedor">E-mail</label>
                                                    <input class="form-control" name="emailContatoFornecedor" type="text"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['email_fornecedor'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="siteFornecedor">Site</label>
                                                    <input class="form-control" name="siteFornecedor" type="text"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['site'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                                            </div>

                                            <div class="row" style="margin-top: 2em">
                                                <div class="col-md-3">
                                                    <label for="agenciaBancoFornecedor">Ag�ncia</label>
                                                    <input type="text" class="form-control number" name="agenciaBancoFornecedor"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['agencia'];?>"
                                                           placeholder="N�O H� DADOS ">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="ccBancoFornecedor">Conta Corrente</label>
                                                    <input type="text" class="form-control number" name="ccBancoFornecedor"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['conta_corrente'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="nomeBancoFornecedor">Banco</label>
                                                    <input type="text" class="form-control" name="nomeBancoFornecedor"
                                                        <?php if($dadosFornecedor)echo $dadosFornecedor['banco'];?>
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="numeroBancoFornecedor">N�mero do Banco</label>
                                                    <input type="text" class="form-control number" name="numeroBancoFornecedor"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['num_transferencia'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 2em; border-top: 1px solid black">
                                            </div>

                                            <div class="row" style="margin-top: 2em">
                                                <div class="col-md-4">
                                                    <label>Avali��o</label>
                                                    <input type="text" name="avalicaoFornecedor" class="form-control number"
                                                           value="<?php if($dadosFornecedor)echo $dadosFornecedor['avaliacao'];?>"
                                                           placeholder="N�O H� DADOS">
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 2em">
                                                <div class="col-md-12">
                                                    <label>Informa��es Adicionais</label>
                                                    <textarea rows="2" class="form-control" name="descricaoFornecedor" placeholder="N�O H� DADOS"><</textarea>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- / Dados do Fornecedor -->

                </div>
                <!-- / modal-body -->

                <!-- modal-footer -->
                <div class="modal-footer">
<!---->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-12">-->
<!--                                                <textarea class="form-control text-danger" id="modal_debug"></textarea>-->
<!--                                                <br>-->
<!--                                            </div>-->
<!--                                        </div>-->

                    <div class="btn-group" >
                        <button id="btn-modalSalvar" class="btnToolTip btn btn-success" title="Salvar Altera��es" data-toggle="tooltip" data-placement="top" style="margin-right: 15px"><i class="fa fa-save fa-fw fa-3x" title="Salvar Altera��es"></i></button>
                        <button id="btn-modalFecharJanela" type="button" class="btnToolTip btn btn-default" data-dismiss="modal" title="Cancelar Altera��es" data-toggle="tooltip" data-placement="top"><i class="fa fa-close fa-fw fa-3x"></i></button>
                    </div>

                </div>
                <!-- / modal-footer -->

            </div>
            <!-- / Modal-Content -->

        </div>

    </form>
</div>

<!-- / MODAL -->

