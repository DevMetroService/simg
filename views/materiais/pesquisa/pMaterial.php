<?php
/**
 * Created by PhpStorm.
 * User: Rycker
 * Date: 07/06/2016
 * Time: 10:29
 */
//echo var_dump($_POST);
?>

<!-- CONTEUDO -->

<div class="page-header">
    <h1>Pesquisa de Materiais</h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Pesquisa de Materiais</div>
            <div class="panel-body">

                <!-- FORM DE PESQUISA -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Filtro</div>
                            <div class="panel-body">

                                <!--
                                     Recebe msg com status do update
                                     ao editar um material, para ser exibida no alerta.
                                -->
                                <input id="msgUpdate" type="hidden"
                                       value="<?php echo $_SESSION['statusUpdate'];
                                                    unset($_SESSION['statusUpdate']); ?>"
                                >

                                <form
                                    action="<?php echo HOME_URI; ?>/dashboardMateriais/executarPesquisaMaterial"
                                    class="form-group" method="post" id="formPesquisaMaterial">

                                    <!-- Filtros de Pesquisa-->
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label>Categoria</label>
                                            <select id="categoria" name="categoria" class="form-control">
                                                <option value=""></option>
                                                <?php
                                                $selectCategoria = $this->medoo->select("categoria_material", "*");
                                                foreach ($selectCategoria as $dados => $value) {
                                                    if (!empty($dadosRefill) && $dadosRefill['categoria'] == $value['cod_categoria'])
                                                        echo("<option value='{$value['cod_categoria']}' selected>{$value['nome_categoria']}</option>");
                                                    else
                                                        echo("<option value='{$value['cod_categoria']}'>{$value['nome_categoria']}</option>");
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-9">
                                            <label>Material</label>
                                            <input type="hidden" name="pesquisaMaterial"/>
                                            <input id="pesquisaMaterialInput" name="pesquisaMaterialInput"
                                                   list="pesquisaMaterialDataList"
                                                   class="form-control"
                                                   placeholder="Clique duas vezes ou Digite o nome do material"
                                                   autocomplete="off"
                                                   value=""/>

                                            <datalist id="pesquisaMaterialDataList">
                                                <?php
                                                if (!empty($dadosRefill['categoria'])) {
                                                    $material = $this->medoo->select("material", "*");

                                                    foreach ($material as $dados => $value) {
                                                        $nome = strtoupper($value['nome_material']);
                                                        if (!empty($dadosRefill) && $dadosRefill['pesquisaMaterial'] == $value['cod_material'])
                                                            echo("<option value='{$value['cod_material']}' selected>{$nome}</option>");
                                                        else
                                                            echo("<option value='{$value['cod_material']}'>{$nome}</option>");
                                                    }

                                                }
                                                ?>
                                            </datalist>
                                        </div>

                                    </div>
                                    <!-- /Filtros de Pesquisa-->

                                    </br>

                                    <!-- BTNs  do Filtro de Pesquisa-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="btnPesquisar" type="button"
                                                    class="btn btn-success btn-lg btnPesquisar">Pesquisar
                                            </button>
                                        </div>
                                        <!-- / BTNs -->

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- / FORM DE PESQUISA -->


                <!-- TABELA -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Resultado da Pesquisa</div>
                            <div class="panel-body">
                                <table id="tabelaMateriais"
                                       class="table table-bordered table-striped table-hover dataTable no-footer">
                                    <thead>
                                    <tr>
                                        <th>C�digo</th>
                                        <th>Material</th>
                                        <th>Unidade</th>
                                        <th>Categoria</th>
                                        <th>A��o</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if ($selectMaterial) {
                                        foreach ($selectMaterial as $dados => $value) {
                                            echo('<tr>');
                                            echo("<td>{$value['cod_material']}</td>");
                                            echo("<td>{$nome}</td>");
                                            echo("<td>{$value['sigla_uni_medida']}</td>");
                                            echo("<td>{$value['nome_categoria']}</td>");
                                            echo("<td>
                                                <button type='button' class='btn btn-primary btn-circle btnEditarMaterial' data-target='#editarMaterial' data-toggle='modal'><i class='fa fa-file fa-fw'></i></button>
                                            </td>");
                                            echo('</tr>');
                                        }
                                    }

                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / TABELA -->


            </div>
        </div>
    </div>
</div>

<!-- / CONTEUDO -->


<!-- JANELA MODAL - EDITAR MATERIAL -->

<div class="modal fade " id="modal_editarMaterial" tabindex="-1" role="dialog">
    <form id="formEditarMaterial" action="<?php echo HOME_URI; ?>/dashboardMateriais/editarMaterial" method="post">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h3 class="modal-title">Editar o de Material</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Cod. Material</label>
                            <input class="form-control" name="codMaterial" readonly>
                        </div>
                        <div class="col-md-offset-6 col-md-3">
                            <label>Estoque Atual</label>
                            <input class="form-control" name="estoqueAtual" value="" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome</label>
                            <input class="form-control" name="nomeMaterial" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Unidade de Medida</label>
                            <select class="form-control" name="unidadeMedida" required>
                                <?php
                                $selectUnidade = $this->medoo->select("unidade_medida", "*");
                                foreach ($selectUnidade as $dados => $value) {
                                    echo("<option value='{$value['cod_uni_medida']}'>{$value['nome_uni_medida']}</option>");
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Quantidade M?nima</label>
                            <input class="form-control float" name="qtdMinima" required/>
                        </div>
                        <div class="col-md-4">
                            <label>Categoria</label>
                            <select class="form-control" name="categoria" required>
                                <?php
                                $selectCategoria = $this->medoo->select("categoria_material", "*");
                                foreach ($selectCategoria as $dados => $value) {
                                    echo("<option value='{$value['cod_categoria']}'>{$value['nome_categoria']}</option>");
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Marca</label>
                            <select class="form-control" name="marcaMaterial">
                                <?php
                                $selectCategoria = $this->medoo->select("marca_material", "*");
                                foreach ($selectCategoria as $dados => $value) {
                                    echo("<option value='{$value['cod_marca']}'>{$value['nome_marca']}</option>");
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label>Descri??o</label>
                            <input class="form-control" name="descricao" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <!--                    <div class="row">-->
                    <!--                        <div class="col-md-12">-->
                    <!--                            <textarea class="form-control text-danger" id="modal_debug"></textarea>-->
                    <!--                            <br>-->
                    <!--                        </div>-->
                    <!--                    </div>-->

                    <div class="btn-group">
                        <button id="btn-modalSalvar" class="btn btn-success" style="ma"><i
                                class="fa fa-save fa-fw fa-3x" title="Salvar Altera??es"></i></button>
                        <button id="btn-modalCancelar" type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="fa fa-close fa-fw fa-3x" title="Fechar Janela"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- / MODAL -->

