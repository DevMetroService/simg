<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/12/2016
 * Time: 15:25
 */
?>
<div class="page-header">
    <h2>Requisi��o de Materiais</h2>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Formul�rio de Requisi��o</div>

            <div class="panel-body">

                <!-- Panel Dados da Requisicao  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Dados da Requisi��o</div>

                            <div class="panel-body">

                                <form id="dadosDaRequisicao">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>�rea Solicitante</label>
                                            <select class="form-control" name="areaSolicitante" id="areaSolicitante"
                                                    required>
                                                <option value="default">_default</option>
                                            </select>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Dados do Requisitante</div>

                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Requisitante</label>
                                                            <input class="form-control" value=""
                                                                   placeholder="Nome do Requisitante" type="text"
                                                                   required
                                                                   name="funcionarioRequisitante"
                                                                   id="funcionarioRequisitante">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Data e Hora da Requisi��o</label>
                                                            <input class="form-control timeStamp" value="" required
                                                                   type="text" name="dataRequisicao"
                                                                   id="dataRequisicao">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Dados da Aprova��o</div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Aprovado por:</label>
                                                            <input class="form-control" value="" required
                                                                   placeholder="Nome do Aprovador" type="text"
                                                                   name="aprovadorRequisicao" id="aprovadorRequisicao">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Data e Hora da Aprova��o</label>
                                                            <input class="form-control timeStamp" value="" required
                                                                   type="text" name="dataAprovacao" id="dataAprovacao">
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Observa��es:</label>
                                        <textarea class="form-control" rows="5" id="observacoes"
                                                  name="txtObservacoes"></textarea>
                                        </div>
                                    </div>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <!-- Fim Panel Dados da Requisicao  -->


                <br/>

                <!-- Panel Lista de Materiais Requisitados  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel panel-heading">Requisitar Materiais</div>


                            <div class="panel-body">

                                <!-- Panel Adicionar Materiais  -->
                                <div class="row" style="margin-top: -15px">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <form id="formAddMaterial">
                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label>C�digo:</label>
                                                                <input id="codigo" class="form-control formField number"
                                                                       value=""
                                                                       placeholder="" type="text" name="codigo"
                                                                       disabled>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <label>Desc. do Material:</label>
                                                                <select name="descMaterial" class="form-control"
                                                                        required>
                                                                    <option value="" selected></option>
                                                                    <?php
                                                                    $material = $this->medoo->select('v_material', '*');

                                                                    foreach ($material as $dados => $value) {
                                                                        echo("<option value='{$value['cod_material']}'>{$value['nome_material']}</option>");
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>N� OS:</label>
                                                                <input class="form-control formField number" value=""
                                                                       placeholder=""
                                                                       type="text" name="os">
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label>Qtd. Solicitada:</label>
                                                                <input class="form-control number" value=""
                                                                       placeholder="" type="text" name="qtdSolicitada"
                                                                       required>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <label>Unidade</label>
                                                                <select class="form-control" name="unidade" id="unidade" required>
                                                                    <option value="" selected disabled></option>
                                                                    <?php
                                                                    $unidade = $this->medoo->select("unidade_medida", ["sigla_uni_medida", "cod_uni_medida", "nome_uni_medida"]);

                                                                    foreach ($unidade as $key => $value) {
                                                                        echo "<option value='{$value['sigla_uni_medida']}' >{$value['sigla_uni_medida']} - {$value['nome_uni_medida']}</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Qtd. Entregue:</label>
                                                                <input class="form-control number" value=""
                                                                       placeholder=""
                                                                       type="text" name="qtdEntregue">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Data e Hora da Entrega</label>
                                                                <input class="form-control timeStamp" value=""
                                                                       type="text" name="dataHoraEntrega">
                                                            </div>
                                                        </div>

                                                        <br/>
                                                    </div>
                                                </form>

                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <!-- Fim Panel Adicionar Materiais  -->

                                <div class="row">
                                    <div class="col-md-2" style="margin-bottom: 1em;">
                                        <div class="btn-group-lg">
                                            <button type="button" id="btnAddMaterial" name="btnAddMaterial" title=""
                                                    class="btn btn-lg btn-default">
                                                <i class="fa fa-plus fa-fw"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <div class="row">
                                    <form id="formListaDeMateriais" method="post" action="">
                                        <table id="tableListaMateriais"
                                               class="table table-striped table-bordered table-hover dataTable no-footer"
                                               role="grid" aria-describedby="">
                                            <thead>
                                            <tr>
                                                <th class="col-sm-0">N</th>
                                                <th class="col-sm-4">Material</th>
                                                <th class="col-sm-1">OS</th>
                                                <th class="col-sm-3">Qtd. / Unid. Solicitada</th>
                                                <th class="col-sm-2">Qtd. Entregue</th>
                                                <th class="col-sm-3">Data/Hora Entrega</th>
                                                <th class="col-sm-1">A��es</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            </tbody>


                                        </table>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>


                </div>


                <!-- Panel Lista de Materiais Requisitados  -->
                <div class="row">
                    <div class="col-md-offset-8 col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">A��es</div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group  btn-group-justified " role="group">

                                            <div class="btn-group">
                                                <button type="button" aria-label="right align"
                                                        id="btnFinalizarRequisicao" class="btn btn-default btn-lg"
                                                        title="Salvar">
                                                    <i class="fa fa-floppy-o fa-2x"></i>
                                                </button>
                                            </div>

                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-lg" id="btnPrint"
                                                        title="Imprimir" aria-label="right align">
                                                    <i class="fa fa-print fa-2x"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- Fim do body Principal-->

        </div>
    </div>
</div>

<div id="pagePrint">


</div>