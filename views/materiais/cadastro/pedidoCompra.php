<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/12/2016
 * Time: 15:25
 */
?>
<div class="page-header">
    <h2>Pedido de Compras</h2>
</div>

<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Pedido de Compras</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">Dados do Pedido</div>
                            <div class="panel-body">
                                <form id="dadosDaRequisicao">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Cod. do Pedido</label>
                                            <input type="text" class="form-control" name="codPedidoCompra"
                                                   id="codPedidoCompra" readonly>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Data do Pedido</label>
                                            <input type="text" class="form-control timeStamp" name="dataPedido"
                                                   id="dataPedido" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Fornecedor</label>
                                            <select name="nomeFornecedor" class="form-control" required>
                                                <option value="" selected disabled>SELECIONE O FORNECEDOR</option>
                                                <?php
                                                $fornecedor = $this->medoo->select("fornecedor", ["cod_fornecedor", "nome_fantasia"]);

                                                foreach ($fornecedor as $key => $value) {
                                                    echo "<option value='{$value['cod_fornecedor']}'>{$value['nome_fantasia']}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Cod. Fornecedor</label>
                                            <input type="text" class="form-control" name="codFornecedor"
                                                   id="codFornecedor" readonly>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Responsavel para Contato</label>
                                            <input type="text" name="contatoFornecedor"
                                                   id="contatoFornecedor" class="form-control" readonly>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Departamento</label>
                                            <input type="text" class="form-control" name="departContatoFornecedor"
                                                   id="departContatoFornecedor" readonly>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Fone</label>
                                            <input type="text" class="form-control telefone"
                                                   name="telefoneContatoFornecedor"
                                                   id="telefoneContatoFornecedor" readonly>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="emailContatoFornecedor">E-mail</label>
                                            <input class="form-control" name="emailContatoFornecedor"
                                                   id="emailContatoFornecedor" type="text" readonly>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Endere�o do Fornecedor</label>
                                            <input type="text" class="form-control" name="enderecoFornecedor"
                                                   id="enderecoFornecedor" readonly>
                                        </div>
                                    </div>
                                </form>


                            </div>
                        </div>

                        <!-- Form Pedidos de Compra -->

                        <div class="panel panel-default">
                            <div class="panel-heading">Pedidos de Compra</div>
                            <div class="panel-body">

                                <form id="addPedidoCompra">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Cod. Material</label>
                                            <input type="text" class="form-control number" name="codMaterial"
                                                   id="codMaterial" readonly>
                                        </div>
                                        <div class="col-md-10">
                                            <label>Descri��o do Material</label>
                                            <select name="descMaterial" class="form-control" required>
                                                <option value="" selected disabled>SELECIONE O MATERIAL</option>
                                                <?php
                                                $material = $this->medoo->select('v_material', '*');

                                                foreach ($material as $dados => $value) {
                                                    echo("<option value='{$value['cod_material']}'>{$value['nome_material']}</option>");
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Quantidade</label>
                                            <input type="text" class="form-control number" name="qtdSolicitada"
                                                   id="qtdSolicitada" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Unidade</label>
                                            <select class="form-control" name="unidade" id="unidade" required>
                                                <option value="" selected disabled></option>
                                                <?php
                                                $unidade = $this->medoo->select("unidade_medida", ["sigla_uni_medida", "cod_uni_medida", "nome_uni_medida"]);

                                                foreach ($unidade as $key => $value) {
                                                    echo "<option value='{$value['sigla_uni_medida']}' >{$value['sigla_uni_medida']} - {$value['nome_uni_medida']}</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Valor Unit�rio</label>
                                            <input type="text" class="form-control real" name="valorUnitarioMaterial"
                                                   id="valorUnitarioMaterial" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Prazo de Entrega</label>
                                            <input type="text" class="form-control" name="prazoEntregaMaterial"
                                                   id="prazoEntregaMaterial" required>
                                        </div>
                                    </div>

                                    <!-- btn AddPedido-->
                                    <div class="row">
                                        <div class="col-md-2" style="padding: 1em;">
                                            <button id="btnAddPedido" type="button" class="btn btn-default btn-lg"><i
                                                    class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </form>

                                <br/>

                                <!-- Tabela de Compra -->
                                <div class="row">
                                    <table id="tableListaCompras"
                                           class="table table-striped table-bordered table-hover dataTable no-footer"
                                           role="grid" aria-describedby="">
                                        <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Descri��o do Material</th>
                                            <th>Qtd.</th>
                                            <th>Unid.</th>
                                            <th>Valor Unit�rio</th>
                                            <th>Valor Total</th>
                                            <th>Prazo de Entrega</th>
                                            <th>A��o</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>


                        <!-- Botoes de A��o-->
                        <div class="row">
                            <div class="col-md-offset-8 col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">A��es</div>

                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="btn-group  btn-group-justified " role="group">

                                                    <div class="btn-group">
                                                        <button type="button" aria-label="right align"
                                                                id="btnAcaoFinalizar" class="btn btn-default btn-lg"
                                                                title="Salvar">
                                                            <i class="fa fa-floppy-o fa-2x"></i>
                                                        </button>
                                                    </div>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default btn-lg"
                                                                id="btnPrint" title="Imprimir" aria-label="right align">
                                                            <i class="fa fa-print fa-2x"></i>
                                                        </button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="marcaModal" tabindex="-1" role="form" aria-labelledby="marcaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="marcaModalLabel">Cadastrar Marca</h4>
            </div>

            <form id="formularioModal_1" method="post">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome da marca:</label>
                            <input type="text" name="nomeMarca" class="form-control"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Selecione a unidade de medida.">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Marca</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Categoria -->
<div class="modal fade" id="categoriaModal" tabindex="-1" role="form" aria-labelledby="categoriaModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="categoriaModalLabel">Cadastrar Marca</h4>
            </div>

            <form id="formularioModal_2" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nome da Categoria:</label>
                            <input type="text" name="nomeCategoria" class="form-control"
                                   required data-validation-required-message=" Campo precisa ser preenchido"
                                   data-bv-row=".col-lg-4"
                                   data-fv-notempty="true"
                                   data-fv-notempty-message="Selecione a unidade de medida.">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Descri��o:</label>
                            <textarea class="form-control" name="descricaoCategoria"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Cadastrar Material</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>