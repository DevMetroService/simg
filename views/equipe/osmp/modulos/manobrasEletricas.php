<?php
$tituloOs = "Ordem de Servi�o de Manuten��o Programada";
$actionForm = "moduloOsmp";

$acao = true;
$pag = "manobrasEletricas";
$tipoOS = "osmp/" . $_SESSION['refillOs']['form'];

$manobrasEletricas = $this->medoo->select("osmp_manobra_eletrica", "*", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);

$maoObra = $this->medoo->select("osmp_mao_de_obra", "cod_osmp_mao_de_obra", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$tempoTotal = $this->medoo->select("osmp_tempo", "cod_osmp_tempo", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osmp_registro", "cod_osmp_registro", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);


if ($tipoOS == "osmp/vp" || $tipoOS == "osmp/Vp") {
    $dados = $this->medoo->select("osmp_vp", ["km_inicial", "km_final"], ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
    $dados = $dados[0];
}

require_once(ABSPATH . "/views/_includes/formularios/os/manobrasEletricas.php");