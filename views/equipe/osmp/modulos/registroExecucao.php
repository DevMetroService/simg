<?php

$tituloOs = "Ordem de Servi�o de Manuten��o Programada";
$actionForm = "moduloOsmp";

$acao = true;
$pag = "registroExecucao";
$tipoOS = "osmp/" . $_SESSION['refillOs']['form'];

$codSsmp = $this->medoo->select("osmp", "cod_ssmp", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$codSsmp = $codSsmp[0];

$ss = $this->medoo->select("ssmp", [
    '[><]un_equipe' => 'cod_un_equipe',
    '[><]equipe' => 'cod_equipe'
], [
    "cod_unidade","sigla(sigla_equipe)","cod_equipe"

], ["cod_ssmp" => (int)$codSsmp]);
$ss = $ss[0];

$registro = $this->medoo->select("osmp_registro", "*", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$registro = $registro[0];

$unEquipe = $this->medoo->select("un_equipe", ['[><]equipe' => 'cod_equipe'], "*", ["cod_un_equipe" => (int)$registro['cod_un_equipe']]);
$unEquipe = $unEquipe[0];

$maoObra     = $this->medoo->select("osmp_mao_de_obra", "cod_osmp_mao_de_obra", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$tempoTotal  = $this->medoo->select("osmp_tempo", "cod_osmp_tempo", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$regExecucao = $registro['cod_osmp_registro'];

if ($tipoOS == "osmp/vp" || $tipoOS == "osmp/Vp") {
    $dados = $this->medoo->select("osmp_vp", ["km_inicial", "km_final"], ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
    $dados = $dados[0];
}

require_once(ABSPATH . "/views/_includes/formularios/os/registroExecucao.php");