<?php
$tituloOs = "Ordem de Servi�o de Manuten��o Programada";
$actionForm = "moduloOsmp";

$acao = true;
$pag = "tempoTotal";
$tipoOS = "osmp/" . $_SESSION['refillOs']['form'];

$selectTempo = $this->medoo->select('osmp_tempo', '*', ['cod_osmp' => $_SESSION['refillOs']['codigoOs']]);
$selectTempo = $selectTempo[0];

if(empty($selectTempo)) {
    $selectAbertura = $this->medoo->select('osmp','data_abertura',[
        'cod_osmp'       => (int) $_SESSION['refillOs']['codigoOs']
    ]);
    $selectAbertura = $selectAbertura[0];
}

$maoObra     = $this->medoo->select("osmp_mao_de_obra", "cod_osmp_mao_de_obra", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
$tempoTotal  = $selectTempo["cod_osmp_tempo"];
$regExecucao = $this->medoo->select("osmp_registro", "cod_osmp_registro", ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);

if ($tipoOS == "osmp/vp" || $tipoOS == "osmp/Vp") {
    $dados = $this->medoo->select("osmp_vp", ["km_inicial", "km_final"], ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
    $dados = $dados[0];
}

require_once(ABSPATH . "/views/_includes/formularios/os/temposTotais.php");
