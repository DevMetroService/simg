<?php

$tituloOs = "Ordem de Servi�o de Manuten��o Programada";
$actionForm = "moduloOsmp";

$acao = true;
$pag = "materialUtilizado";
$tipoOS = "osmp/" . $_SESSION['refillOs']['form'];

$sql = "SELECT  m.cod_material,
                mat.nome_material,
                u.sigla_uni_medida,
                m.utilizado,
                m.estado,
                m.origem,
                m.descricao_origem,
                u.cod_uni_medida
        FROM osmp_material m
          JOIN material mat USING (cod_material)
          JOIN unidade_medida u ON u.cod_uni_medida = m.unidade
          WHERE m.cod_osmp = ". $_SESSION['refillOs']['codigoOs'];

$maoObra = $this->medoo->select("osmp_mao_de_obra", "*", ["cod_osmp" =>  $_SESSION['refillOs']['codigoOs']]);
$tempoTotal = $this->medoo->select("osmp_tempo", "*", ["cod_osmp" =>  $_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osmp_registro", "*", ["cod_osmp" =>  $_SESSION['refillOs']['codigoOs']]);

if ($tipoOS == "osmp/vp" || $tipoOS == "osmp/Vp") {
    $dados = $this->medoo->select("osmp_vp", ["km_inicial", "km_final"], ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
    $dados = $dados[0];
}

require_once(ABSPATH . "/views/_includes/formularios/os/materiaisUtilizados.php");
