<?php
$tituloOs = "Ordem de Servi�o de Manuten��o Programada";
$actionForm = "moduloOsmp";

$acao = true;
$pag = "encerramento";
$tipoOS = "osmp/" . $_SESSION['refillOs']['form'];

switch ($_SESSION['refillOs']['form']){
    case "ed":
        $nomeGrupo = "Edifica��es";
        $nomeTable = "edificacao";
        $nomeTableSsmp = "ed";
        break;
    case "vp":
        $nomeGrupo = "Via Permanente";
        $nomeTable = "via_permanente";
        $nomeTableSsmp = "vp";
        break;
    case "su":
        $nomeGrupo = "Subesta��o";
        $nomeTable = "subestacao";
        $nomeTableSsmp = "su";
        break;
    case "ra":
        $nomeGrupo = "Rede Aerea";
        $nomeTable = "rede_aerea";
        $nomeTableSsmp = "ra";
        break;
    case "vlt":
        $nomeGrupo = "Material Rodante VLT";
        $nomeTable = "vlt";
        $nomeTableSsmp = "vlt";
        break;
    case "tue":
        $nomeGrupo = "Material Rodante TUE";
        $nomeTable = "tue";
        $nomeTableSsmp = "tue";
        break;
}

$os = $this->medoo->select("osmp", "*", ["cod_osmp" => (int)$_SESSION['refillOs']['codigoOs']]);
$os = $os[0];

$os['cod_os'] = $os['cod_osmp'];

$ss = $this->medoo->select("ssmp",
    [
        '[><]ssmp_'.$nomeTableSsmp => 'cod_ssmp',
        '[><]pmp_'.$nomeTable => 'cod_pmp_'.$nomeTable,
        '[><]pmp' => 'cod_pmp',
        '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
        '[><]servico_pmp_sub_sistema' => 'cod_servico_pmp_sub_sistema',
        '[><]sub_sistema' => 'cod_sub_sistema',
        '[><]sistema' => 'cod_sistema'
    ],
    "*", ["cod_ssmp" => (int)$os['cod_ssmp']]);
$ss = $ss[0];

$ss['cod_ss'] = $ss['cod_ssmp'];

$encerramento = $this->medoo->select("osmp_encerramento",
[
    "[>]pendencia" => "cod_pendencia", 
    "[><]funcionario" => "cod_funcionario", 
    "[><]funcionario_empresa" => "cod_funcionario_empresa"
], "*", 
[
    "cod_osmp" => $os['cod_osmp']
])[0];

$unEquipe = $this->medoo->select("un_equipe", [
    "[><]equipe" => "cod_equipe",
    "[><]unidade" => "cod_unidade"
], "*", [
        'cod_un_equipe' => $encerramento['cod_un_equipe']
    ]
);
$unEquipe = $unEquipe[0]; // para Transferencia

//Informa��es relacionadas ao m�dulo de Registro de Execu��o
$unidadeEquipe = $this->medoo->select("osmp_registro", [
    "[><]un_equipe" => "cod_un_equipe",
    "[><]unidade" => "cod_unidade",
    "[><]equipe" => "cod_equipe"
], [
    "equipe.sigla",
    "unidade.nome_unidade"
], [
    "osmp_registro.cod_osmp" => $os['cod_osmp']
]);
$unidadeEquipe = $unidadeEquipe[0]; // vem do Registro de Execu��o

$maoObra = $this->medoo->select("osmp_mao_de_obra", "*", ["cod_osmp" =>  $_SESSION['refillOs']['codigoOs']]);
$tempoTotal = $this->medoo->select("osmp_tempo", "*", ["cod_osmp" =>  $_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osmp_registro", "*", ["cod_osmp" =>  $_SESSION['refillOs']['codigoOs']]);

if ($tipoOS == "osmp/vp" || $tipoOS == "osmp/Vp") {
    $dados = $this->medoo->select("osmp_vp", ["km_inicial", "km_final"], ["cod_osmp" => $_SESSION['refillOs']['codigoOs']]);
    $dados = $dados[0];
}

require_once(ABSPATH . "/views/_includes/formularios/os/encerramento.php");