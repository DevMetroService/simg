<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 29/03/2016
 * Time: 16:45
 */
?>

<div class="page-header">
    <h2>Solicita��o de Acesso - SA</h2>
</div>

<div class="row">
    <form class="form-group" method="post" action="">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Dados da Solicita��o de Acesso</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Tipo de Acesso</label>
                                            <select name="tipoAcesso" class="form-control" required>
                                                <option value="">Selecione</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Linha de Acesso</label>
                                            <select name="linhaAcesso" class="form-control" required>
                                                <option value="">Selecione</option>
                                                <?php
                                                $selectLinha = $this->medoo->select("linha", "*");

                                                foreach ($selectLinha as $dados=>$value){
                                                    echo("<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>");
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Requisitos de acesso</label>
                                            <input class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Instala��o Operacional</label>
                                            <select name="instalacaoOperacional" class="form-control" required>
                                                <option value="">Selecione</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Esta��o Inicial</label>
                                            <select name="estacaoInicial" class="form-control" required>
                                                <option value="">Selecione</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Esta��o Final</label>
                                            <select name="estacaoFinal" class="form-control" required>
                                                <option value="">Selecione</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Vias ou Margens</label>
                                            <select name="viasMargens" class="form-control" required>
                                                <option value="">Selecione</option>
                                                <option value="1">Via 01</option>
                                                <option value="2">Via 02</option>
                                                <option value="3">Vias 01 e 02</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="descricaoServicoAcesso">Descri��o do Servi�o/Atividades</label>
                                            <textarea id="descricaoServicoAcesso" name="descricaoServicoAcesso" class="form-control"
                                                      spellcheck="true"></textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>