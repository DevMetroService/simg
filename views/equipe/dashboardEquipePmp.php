<?php
$this->dashboard->cabecalho('Preventiva');

$unidadesUserSql = Sql::getUnidadesUserSql($_SESSION['equipeUsuario']);
$qznNextMes = $this->getQznNextMes();
//$qznNextMes['ano'] = 2022;
//$qznNextMes['primQ'] = 1;
//$qznNextMes['segQ'] = 2;
//$qznNextMes['nome'] = 'Janeiro';

$tagEd = false;
$tagVp = false;
$tagSu = false;
$tagRa = false;
$tagTl = false;
$tagBl = false;
$tagMRT = false;
$tagMRV = false;

foreach ($_SESSION['equipeUsuario'] as $dados) {
    switch ($dados['sigla']) {
        case 'EE': // EDIFICAÇÕES
        case 'OR': // GESIV METRO SERVICE OBRAS
        case 'EL': // GESIV METRO SERVICE ELETRICA
            if (!$tagEd) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Edificação", 2);

                $this->dashboard->painelCronograma('Ed', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Ed', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Ed', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Ed', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Ed', $unidadesUserSql, true);
            }
            $tagEd = true;
            break;
        case 'VI': // GESIV METRO SERVICE VIA PERMANENTE
            if (!$tagVp) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Via Permanente", 2);

                $this->dashboard->painelCronograma('Vp', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Vp', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Vp', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Vp', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Vp', $unidadesUserSql, true);
            }
            $tagVp = true;
            break;
        case 'EN': // GESIV METRO SERVICE ENERGIA(SUBESTAÇÃO)
            if (!$tagSu) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Subestação", 2);

                $this->dashboard->painelCronograma('Su', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Su', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Su', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Su', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Su', $unidadesUserSql, true);
            }
            $tagSu = true;
            break;
        case 'RA': // GESIV METRO SERVICE REDE AEREA
            if (!$tagRa) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Rede Aerea", 2);

                $this->dashboard->painelCronograma('Ra', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Ra', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Ra', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Ra', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Ra', $unidadesUserSql, true);
            }
            $tagRa = true;
            break;
        case 'TC': // GESIV METRO SERVICE TELECOM
            if (!$tagTl) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Telecom", 2);

                $this->dashboard->painelCronograma('Tl', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Tl', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Tl', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Tl', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Tl', $unidadesUserSql, true);
            }
            $tagTl = true;
            break;
        case 'BI': // GESIV METRO SERVICE BILHETAGEM
            if (!$tagBl) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Bilhetagem", 2);

                $this->dashboard->painelCronograma('Bl', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Bl', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Bl', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Bl', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Bl', $unidadesUserSql, true);
            }
            $tagBl = true;
            break;
        case 'MRT': // GEMOF MATERIAL RODANTE - TUE
            if (!$tagMRT) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - TUE", 2);

                $this->dashboard->painelCronogramaTue(true);

                $this->dashboard->painelSsmpAbertas('Tue', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Tue', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Tue', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Tue', $unidadesUserSql, true);
            }
            $tagMRT = true;
            break;
        case 'MRV': // GEMOF MATERIAL RODANTE - VLT
            if (!$tagMRV) {
                $this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - VLT", 2);

                $this->dashboard->painelCronograma('Vlt', false, true, $qznNextMes, $unidadesUserSql, true, true, 'default');

                $this->dashboard->painelSsmpAbertas('Vlt', $unidadesUserSql, true);
                $this->dashboard->painelSsmpProgramadas('Vlt', $unidadesUserSql, true);
                $this->dashboard->painelSsmpPendentes('Vlt', $unidadesUserSql, true);
                $this->dashboard->painelOsmp('Vlt', $unidadesUserSql, true);
            }
            $tagMRV = true;
            break;
    }
}
$this->dashboard->modalExibirCronograma();
