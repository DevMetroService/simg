<?php
$this->dashboard->modalUserForm($this->dadosUsuario);
?>
<div class="page-header">
    <h1>Controle da Manuten��o</h1>
</div>

<?php
$safDev = $this->medoo->select("v_saf", "*", [
    "AND" => [
        "nome_status"   => "Devolvida",
        "usuario"       => $this->dadosUsuario['usuario']
    ]
]);
$quantDevolvida = count($safDev);

if($quantDevolvida > 0){ ?>
    <div class="row" id="safDevolvida">
        <div class="col-md-12">
            <div class='panel-danger panel'>
                <div class="panel-heading" role="tab" id="headingSafDevolvida">
                    <div class="row">
                        <div class="col-xs-3">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorSafDevolvida" aria-expanded="false" aria-controls="collapseSafDevolvida">
                                <button class="btn btn-default"><label>SAF's Devolvidas</label></button>
                            </a>
                        </div>
                        <?php
                        if ($quantDevolvida > 0)
                            echo"<div class='col-xs-9'><h4>".$quantDevolvida." Saf(s) Devolvida(s)</h4></div>";
                        ?>
                    </div>
                </div>
                <div id="divIndicadorSafDevolvida" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingSafDevolvida">
                    <div class="panel-body"  id="tabelaSafDevolvida">
                        <table id="indicadorSafDevolvida" class="table table-striped table-bordered no-footer">
                            <thead id="headIndicadorSaf">
                            <tr role="row">
                                <th>N�</th>
                                <th>Local</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Data de Abertura</th>
                                <th>Motivo</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody id="bodyIndicadorSafDevolvida">
                            <?php

                            if (!empty($safDev)) {
                                foreach ($safDev as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_saf'] . '</td>');
                                    echo('<td>' . $dados['nome_linha'] . ' ' . $dados['nome_trecho'] . '</td>');

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                    echo('<td>' . $dados['descricao_status'] . '</td>');
                                    echo('<td>' . $dados['nivel'] . '</td>');
                                    echo('<td>');
                                    echo('<button class="btn btn-primary btn-circle btnEditarSaf" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                                    echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</td>');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

<?php
foreach ($this->equipeUsuario as $dados => $value) {
    $whereUnEquipe[] = "cod_un_equipe = {$value}";
}

$hasEquipe = false;

if(!empty($whereUnEquipe)){
    $hasEquipe = true;
}


if ($hasEquipe) {
?>
    <div class="page-header">
        <h2><span style="font-size: 35px">&raquo;</span> Controle de Corretivas</h2>
    </div>
<?php
    $whereTxtUnEquipe = ' WHERE (' . implode(' OR ', $whereUnEquipe);
    $sql = "SELECT * FROM v_ssm JOIN saf USING(cod_saf)";
    $sql = $sql . $whereTxtUnEquipe;

    $sql = $sql . ') AND cod_status = 12'; // Encaminhado

    $ssm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    $dataMaisAntiga = $ssm[0]['data_status'];
    $codSsmAntiga = $ssm[0]['cod_ssm'];

    foreach ($ssm as $dados) {
        if ($dados['nivel'] == "A") {
            $this->ctdA += 1;
        } else {
            if ($dados['nivel'] == "B") {
                $this->ctdB += 1;
            } else {
                $this->ctdC += 1;
            }
        }

        if ($dados['data_status'] < $dataMaisAntiga) {
            $dataMaisAntiga = $dados['data_status'];
            $codSsmAntiga = $dados['cod_ssm'];
        }
    }
    ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSsmRecebida">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSsmRecebida" aria-expanded="true"
                               aria-controls="collapseSsmRecebida">
                                <button class="btn btn-default"><label>SSM Recebida</label></button>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <label>A - <span id="ssmRContadorNivelA"><?php echo $this->ctdA ?></span> </label>
                        </div>
                        <div class="col-md-2">
                            <label>B - <span id="ssmRContadorNivelB"><?php echo $this->ctdB ?></span> </label>
                        </div>
                        <div class="col-md-2">
                            <label>C - <span id="ssmRContadorNivelC"><?php echo $this->ctdC ?></span> </label>
                        </div>
                        <div class="col-md-4">
                            <label>Data mais antiga
                                - <span
                                        id="ssmRContadorData"><?php if (count($ssm)) echo $this->parse_timestamp($dataMaisAntiga) . '</span> - <span  id="ssmRContadorCod">' . $codSsmAntiga ?></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div id="tabelaSsmRecebida" class="panel-collapse collapse in" role="tabpanel"
                     aria-labelledby="headingSsmRecebida">
                    <div class="panel-body">
                        <table id="indicadorSsmRecebida"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Local</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Sistema</th>
                                <th>Avaria</th>
                                <th>Data de Abertura</th>
                                <th>Nivel</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssm)) {
                                foreach ($ssm as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_ssm'] . '</td>');
                                    echo('<td>' . $dados['cod_saf'] . '</td>');
                                    echo("<td><span class='primary-info'>{$dados['nome_linha']}</span><br /><span class='sub-info'>{$dados['descricao_trecho']}</span></td>");

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo("<td> {$dados['nome_sistema']} </td>");
                                    echo("<td> <span class='primary-info'>{$dados['nome_avaria']}</span><br /><span class='sub-info'>{$dados['complemento_falha']}</span> </td>");
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                    echo('<td>' . $dados['nivel'] . '</td>');
                                    echo('<td>');
                                    echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                    echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $this->ctdA = 0;
    $this->ctdB = 0;
    $this->ctdC = 0;
    $where = "";

    $sql = "SELECT * FROM v_ssm";
    $sql = $sql . $whereTxtUnEquipe;

    $sql = $sql . ') AND cod_status = 36'; // Reprovada

    $ssm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    $dataMaisAntiga = $ssm[0]['data_status'];
    $codSsmAntiga = $ssm[0]['cod_ssm'];

    foreach ($ssm as $dados) {
        if ($dados['nivel'] == "A") {
            $this->ctdA += 1;
        } else {
            if ($dados['nivel'] == "B") {
                $this->ctdB += 1;
            } else {
                $this->ctdC += 1;
            }
        }

        if ($dados['data_status'] < $dataMaisAntiga) {
            $dataMaisAntiga = $dados['data_status'];
            $codSsmAntiga = $dados['cod_ssm'];
        }
    }
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-danger">
                <div class="panel-heading" role="tab" id="headingSsmReprovada">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSsmReprovada" aria-expanded="false"
                               aria-controls="collapseSsmReprovada">
                                <button class="btn btn-default"><label>SSM's Reprovadas</label></button>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <label>A - <span id="ssmPContadorNivelA"><?php echo $this->ctdA ?></span> </label>
                        </div>
                        <div class="col-md-2">
                            <label>B - <span id="ssmPContadorNivelC"><?php echo $this->ctdB ?></span> </label>
                        </div>
                        <div class="col-md-2">
                            <label>C - <span id="ssmPContadorNivelC"><?php echo $this->ctdC ?></span> </label>
                        </div>
                        <div class="col-md-4">
                            <label>Data mais antiga
                                - <span
                                        id="ssmPContadorData"><?php if (count($ssm)) echo $this->parse_timestamp($dataMaisAntiga) . '</span> - <span  id="ssmPContadorCod">' . $codSsmAntiga ?></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="tabelaSsmReprovada" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="headingSsmReprovada">

                    <div class="panel-body">
                        <table id="indicadorSsmReprovada"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Local</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Servi�o</th>
                                <th>Grupo de Sistema</th>
                                <th>Motivo</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssm)) {
                                foreach ($ssm as $dados) {
                                    echo("<tr>");
                                    echo("<td>{$dados["cod_ssm"]}</td>");
                                    echo("<td>{$dados["cod_saf"]}</td>");
                                    echo("<td><span class='primary-info'>{$dados["nome_linha"]}</span><br /><span class='sub-info'>{$dados["descricao_trecho"]}</span></td>");

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");
                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento_servico']}'>{$dados['nome_servico']}</span></td>");
                                    echo("<td>{$dados["nome_grupo"]}</td>");
                                    echo("<td>{$dados["descricao_status"]}</td>");
                                    echo("<td>");
                                    echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                    echo("<button class='btn btn-default btn-circle btnImprimirSsm' type='button' title='Imprimir'><i class='fa fa-print fa-fw'></i></button>");
                                    echo("</tr>");
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $this->ctdA = 0;
    $this->ctdB = 0;
    $this->ctdC = 0;
    $where = "";

    $sql = "SELECT * FROM v_ssm";

    $sql = $sql . $whereTxtUnEquipe;

    $sql = $sql . ') AND cod_status = 15'; // Pendente

    $ssm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    $dataMaisAntiga = $ssm[0]['data_status'];
    $codSsmAntiga = $ssm[0]['cod_ssm'];

    foreach ($ssm as $dados) {
        if ($dados['nivel'] == "A") {
            $this->ctdA += 1;
        } else {
            if ($dados['nivel'] == "B") {
                $this->ctdB += 1;
            } else {
                $this->ctdC += 1;
            }
        }

        if ($dados['data_status'] < $dataMaisAntiga) {
            $dataMaisAntiga = $dados['data_status'];
            $codSsmAntiga = $dados['cod_ssm'];
        }
    }
    ?>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSsmPendente">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSsmPendente" aria-expanded="false"
                               aria-controls="collapseSsmPendente">
                                <button class="btn btn-default"><label>SSM Pendente</label></button>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <label>A - <span id="ssmPContadorNivelA"><?php echo $this->ctdA ?></span> </label>
                        </div>
                        <div class="col-md-2">
                            <label>B - <span id="ssmPContadorNivelC"><?php echo $this->ctdB ?></span> </label>
                        </div>
                        <div class="col-md-2">
                            <label>C - <span id="ssmPContadorNivelC"><?php echo $this->ctdC ?></span> </label>
                        </div>
                        <div class="col-md-4">
                            <label>Data mais antiga
                                - <span
                                        id="ssmPContadorData"><?php if (count($ssm)) echo $this->parse_timestamp($dataMaisAntiga) . '</span> - <span  id="ssmPContadorCod">' . $codSsmAntiga ?></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="tabelaSsmPendente" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="headingSsmPendente">

                    <div class="panel-body">
                        <table id="indicadorSsmPendente"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Local</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Servi�o</th>
                                <th>Data de Abertura</th>
                                <th>Nivel</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssm)) {
                                foreach ($ssm as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_ssm'] . '</td>');
                                    echo('<td>' . $dados['cod_saf'] . '</td>');
                                    echo("<td><span class='primary-info'>{$dados['nome_linha']}</span><br /><span class='sub-info'>{$dados['descricao_trecho']}</span></td>");

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento_servico']}'>{$dados['nome_servico']}</span></td>");
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                    echo('<td>' . $dados['nivel'] . '</td>');
                                    echo('<td>');
                                    echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                    echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php

    if ($this->dadosUsuario['nivel'] != '5.2'){

    $where = "";

    $sql = "SELECT * FROM v_ssm";

    $sql = $sql . $whereTxtUnEquipe;

    $sql = $sql . ') AND cod_status = 42'; // Pendente Aprovado Compra Material

    $ssm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    ?>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSsmPendente">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSsmPendenteCompraAprovada" aria-expanded="false"
                               aria-controls="collapseSsmPendenteCompraAprovada">
                                <button class="btn btn-default"><label>SSM Pendente (Compra de Material Aprovada)</label></button>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="tabelaSsmPendenteCompraAprovada" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="headingSsmPendente">

                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer tableData">
                            <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Local</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Servi�o</th>
                                <th>Data de Abertura</th>
                                <th>Nivel</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssm)) {
                                foreach ($ssm as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_ssm'] . '</td>');
                                    echo('<td>' . $dados['cod_saf'] . '</td>');
                                    echo("<td><span class='primary-info'>{$dados['nome_linha']}</span><br /><span class='sub-info'>{$dados['descricao_trecho']}</span></td>");

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento_servico']}'>{$dados['nome_servico']}</span></td>");
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                    echo('<td>' . $dados['nivel'] . '</td>');
                                    echo('<td>');
                                    echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                    echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $where = "";

    $sql = "SELECT * FROM v_ssm";

    $sql = $sql . $whereTxtUnEquipe;

    $sql = $sql . ') AND cod_status = 43'; // Pendente (Compra Negada)

    $ssm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    ?>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSsmPendente">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSsmPendenteCompraReprovada" aria-expanded="false"
                               aria-controls="collapseSsmPendenteCompraReprovada">
                                <button class="btn btn-default"><label>SSM Pendente (Compra de Material N�o Aprovada)</label></button>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="tabelaSsmPendenteCompraReprovada" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="headingSsmPendente">

                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer tableData">
                            <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Local</th>
                                <th>Servi�o</th>
                                <th>Data de Abertura</th>
                                <th>Justificativa</th>
                                <th>Nivel</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssm)) {
                                foreach ($ssm as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_ssm'] . '</td>');
                                    echo('<td>' . $dados['cod_saf'] . '</td>');
                                    echo("<td><span class='primary-info'>{$dados['nome_linha']}</span><br /><span class='sub-info'>{$dados['descricao_trecho']}</span></td>");
                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento_servico']}'>{$dados['nome_servico']}</span></td>");
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                    echo("<td>{$dados['descricao_status']}</td>");
                                    echo('<td>' . $dados['nivel'] . '</td>');
                                    echo('<td>');
                                    echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                    echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    }
    $this->ctdA = 0;
    $this->ctdB = 0;
    $this->ctdC = 0;

    $where = [];

    $sql = "SELECT vo.nome_servico as servico_os, vo.complemento as complemento_servico_os, vo.nivel as nivel_osm, *, vo.data_abertura as data_abertura_osm

              FROM v_osm vo
              JOIN v_ssm vs USING(cod_ssm)";

    foreach ($this->equipeUsuario as $dados => $value) {
        $where[] = "vs.cod_un_equipe = {$value}"; // Por motivo da Transferencia
    }

    if (count($where)) {
        $sql = $sql . ' WHERE (' . implode(' OR ', $where);
    }

    $sql = $sql . ') AND vo.cod_status = 10'; // Execu��o

    $osm = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    ?>
    <div class="row">

        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOsmExecucao">
                    <a class="collapsed" role="button" data-toggle="collapse"
                       href="#tabelaOsmExecucao" aria-expanded="false"
                       aria-controls="collapseOsmExecucao">

                        <div class="row">
                            <div class="col-md-2">
                                <button class="btn btn-default"><label>Osm em execu��o</label></button>
                            </div>
                            <div class="col-md-offset-6 col-md-2">
                                <?php
                                $this->btnDownloadFormOsm('formOsm.xlsx');
                                ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="tabelaOsmExecucao" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="headingOsmExecucao">
                    <div class="panel-body">
                        <table id="indicadorOsmExecucao"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N� Osm</th>
                                <th>N� Ssm</th>
                                <th>Data de Abertura</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Equipe</th>
                                <th>Servi�o</th>
                                <th>Local</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($osm)) {
                                foreach ($osm as $dados) {

                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_osm'] . '</td>');
                                    echo('<td>' . $dados['cod_ssm'] . '</td>');
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura_osm']) . '</td>');

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['nome_equipe']} - {$dados['nome_unidade']}'>{$dados['sigla_equipe']} - {$dados['sigla_unidade']}</span></td>");
                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento_servico_os']}'>{$dados['servico_os']}</span></td>");
                                    echo("<td><span class='primary-info'>{$dados['nome_linha']}</span><br /><span class='sub-info'>{$dados['descricao_trecho']}</span></td>");
                                    echo('<td>');
                                    echo $dados['nivel_osm'] != '' ? $dados['nivel_osm'] : $dados['nivel'];
                                    echo('</td>');
                                    echo('<td>');
                                    echo("<a href='{$this->home_uri}/OSM/dadosGerais/{$dados['cod_osm']}'>
                                                    <button class='btn btn-default btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button>
                                                </a>");
                                    echo('<button class="btn btn-default btn-circle btnImprimirOsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</td>');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-header">
        <h2><span style="font-size: 35px">&raquo;</span> Controle de Programa��es</h2>
    </div>

    <?php
    $sql = "SELECT * FROM v_ssp ";

    $sql = $sql . $whereTxtUnEquipe;
    $sql = $sql . ") AND nome_status = 'Programado'";

    $ssp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $time = date('Y-m-d H:i:s', time());
    foreach ($ssp as $dados) {
        if ($time < $dados['data_programada'] && $dataMaisAntiga < $dados['data_programada']) {
            $dataMaisAntiga = $dados['data_programada'];
            $codSspAntiga = $dados['cod_ssp'];
        }
    }
    ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSspAberta">
                    <div class="row">
                        <div class="col-md-3">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSspAberta" aria-expanded="false"
                               aria-controls="collapseOspExecucao">
                                <button class="btn btn-default"><label>SSP</label></button>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <label>Pr�xima SSP
                                - <?php echo $this->parse_timestamp($dataMaisAntiga) . " - " . $codSspAntiga ?> </label>
                        </div>
                    </div>
                </div>
                <div id="tabelaSspAberta" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="tabelaSspAberta">
                    <div class="panel-body">
                        <table id="indicadorSspExecucao"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N�</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Servi�o</th>
                                <th>Data Programada</th>
                                <th>Qtd de dias</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssp)) {
                                foreach ($ssp as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_ssp'] . '</td>');

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento']}'>{$dados['nome_servico']}</span></td>");
                                    echo('<td>' . $this->parse_timestamp($dados['data_programada']) . '</td>');
                                    echo('<td>' . $dados['dias_servico'] . '</td>');
                                    echo('<td>');
                                    echo('<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                                    echo('<button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $sql = "SELECT * FROM v_ssp ";

    $sql = $sql . $whereTxtUnEquipe;
    $sql = $sql . ") AND nome_status = 'Pendente'";

    $ssp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    $time = date('Y-m-d H:i:s', time());
    foreach ($ssp as $dados) {
        if ($time < $dados['data_programada'] && $dataMaisAntiga < $dados['data_programada']) {
            $dataMaisAntiga = $dados['data_programada'];
            $codSspAntiga = $dados['cod_ssp'];
        }
    }
    ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSspPendente">
                    <div class="row">
                        <div class="col-md-2">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaSspPendente" aria-expanded="false"
                               aria-controls="collapseSspPendente">
                                <button class="btn btn-default"><label>SSP Pendente</label></button>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="tabelaSspPendente" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="tabelaSspPendente">
                    <div class="panel-body">
                        <table id="indicadorSspPendente"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N�</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Servi�o</th>
                                <th>Data Programada</th>
                                <th>Qtd de dias</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($ssp)) {
                                foreach ($ssp as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_ssp'] . '</td>');

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento']}'>{$dados['nome_servico']}</span></td>");
                                    echo('<td>' . $this->parse_timestamp($dados['data_programada']) . '</td>');
                                    echo('<td>' . $dados['dias_servico'] . '</td>');
                                    echo('<td>');
                                    echo('<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                                    echo('<button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $sql = "SELECT s.nome_servico as servico_os, oss.complemento as complemento_servico_os, * FROM osp
            JOIN osp_servico oss USING(cod_osp)
            JOIN servico s ON (oss.cod_servico = s.cod_servico)
            JOIN status_osp ostatus USING (cod_ospstatus)
            JOIN v_ssp ssp ON ssp.cod_ssp = osp.cod_ssp ";

    $sql = $sql . $whereTxtUnEquipe;

    $sql = $sql . ") AND ostatus.cod_status = 10"; // Execu��o

    $osp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOspExecucao">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="collapsed" role="button" data-toggle="collapse"
                               href="#tabelaOspExecucao" aria-expanded="false"
                               aria-controls="collapseOspExecucao">

                                <div class="row">
                                    <div class="col-md-2">
                                        <button class="btn btn-default"><label>Osp em execu��o</label></button>
                                    </div>
                                    <div class="col-md-offset-6 col-md-2">
                                        <?php
                                        $this->btnDownloadFormOsm('formOsp.xlsx');
                                        ?>
                                    </div>
                                </div>

                            </a>
                        </div>
                    </div>
                </div>
                <div id="tabelaOspExecucao" class="panel-collapse collapse out" role="tabpanel"
                     aria-labelledby="tabelaOspExecucao">
                    <div class="panel-body">
                        <table id="indicadorOspExecucao"
                               class="table table-striped table-bordered table-hover dataTable no-footer">
                            <thead>
                            <tr role="row">
                                <th>N� Osp</th>
                                <th>N� Ssp</th>
                                <th>Data de Abertura</th>
                                <?php
                                if ($this->dadosUsuario['nivel'] == '5.2')
                                    echo "<th>Ve�culo</th>";
                                ?>
                                <th>Equipe</th>
                                <th>Servi�o</th>
                                <th>Local</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($osp)) {
                                foreach ($osp as $dados) {
                                    echo('<tr>');
                                    echo('<td>' . $dados['cod_osp'] . '</td>');
                                    echo('<td>' . $dados['cod_ssp'] . '</td>');
                                    echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');

                                    if ($this->dadosUsuario['nivel'] == '5.2')
                                        echo("<td> {$dados['nome_veiculo']} </td>");

                                    echo('<td><span class="primary-info" data-placement="right" rel="tooltip-wrapper" data-title="' . $dados['nome_equipe'] . ' - ' . $dados['nome_unidade'] . '">' . $dados['sigla_equipe'] . ' - ' . $dados['sigla_unidade'] . '</span></td>');
                                    echo("<td><span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$dados['complemento_servico_os']}'>{$dados['servico_os']}</span></td>");
                                    echo('<td><span class="primary-info">' . $dados['nome_linha'] . '</span><br /><span class="sub-info">' . $dados['descricao_trecho'] . '</span></td>');
                                    echo('<td>');
                                    echo('<button class="btn btn-default btn-circle btnEditarOsp" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                                    echo('<button class="btn btn-default btn-circle btnImprimirOsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                    echo('</tr>');
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
}
else{
?>
    <div class="row text-center">
        <p class="alert-danger  ">
            USU�RIO N�O PERTENCE A NENHUMA EQUIPE DE MANUTEN��O.</br>
            ENTRAR EM CONTATO COM A �REA RESPOS�VEL PARA SOLU��O.
        </p>
    </div>
<?php
}
?>
