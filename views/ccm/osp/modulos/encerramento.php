<?php

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$os = $this->medoo->select("osp", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$os = $os[0];

$os['cod_os'] = $os['cod_osp'];

$ss = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$os['cod_ssp']]);
$ss = $ss[0];

$ss['cod_ss'] = $ss['cod_ssp'];

$qtdDias = $this->medoo->select('osp', 'cod_ssp',["cod_ssp"=>$os['cod_ssp']]);
$qtdDias = count($qtdDias);

$qtdDias = $ss['dias_servico'] - $qtdDias;

$encerramento = $this->medoo->select("osp_encerramento",[
    "[>]pendencia" => "cod_pendencia", 
    "[><]funcionario" => "cod_funcionario",
    "[><]funcionario_empresa" => "cod_funcionario_empresa"
], "*", ["cod_osp" => $os['cod_osp']])[0];

$unEquipe = $this->medoo->select("un_equipe",[
    "[><]equipe" => "cod_equipe",
    "[><]unidade" => "cod_unidade"
], "*" ,[
        'cod_un_equipe' => $encerramento['cod_un_equipe']
    ]
);
$unEquipe = $unEquipe[0]; // para Transferencia

//Informa��es relacionadas ao m�dulo de Registro de Execu��o
$unidadeEquipe = $this->medoo->select("osp_registro",[
    "[><]un_equipe" => "cod_un_equipe",
    "[><]unidade"   => "cod_unidade",
    "[><]equipe"    => "cod_equipe"
],[
    "equipe.sigla",
    "unidade.nome_unidade"
],[
    "osp_registro.cod_osp" => $os['cod_osp']
]);
$unidadeEquipe = $unidadeEquipe[0]; // vem do Registro de Execu��o

$acao = true;
$pag = "encerramento";
$tipoOS = "osp";

$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/encerramento.php");