<?php

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$acao = true;
$pag = "tempoTotal";
$tipoOS = "osp";

$selectTempo = $this->medoo->select('osp_tempo','*',["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$selectTempo = $selectTempo[0];

if(empty($selectTempo)) {
    $selectAbertura = $this->medoo->select('osp','data_abertura',[
        'cod_osp'       => (int) $_SESSION['refillOs']['codigoOs']
    ]);
    $selectAbertura = $selectAbertura[0];
}

$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/temposTotais.php");
