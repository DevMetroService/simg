<?php
$this->dashboard->cabecalho('Preventiva');

// -- Controle de PMP - Edificação
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Edificação", 2);

$this->dashboard->painelSsmpProgramadas('Ed', '', true);
$this->dashboard->painelSsmpPendentes('Ed', '', true);
$this->dashboard->painelOsmp('Ed', '', true);

// -- Controle de PMP - Via Permanente
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Via Permanente", 2);

$this->dashboard->painelSsmpProgramadas('Vp', '', true);
$this->dashboard->painelSsmpPendentes('Vp', '', true);
$this->dashboard->painelOsmp('Vp', '', true);

// -- Controle de PMP - Subestação
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Subestação", 2);

$this->dashboard->painelSsmpProgramadas('Su', '', true);
$this->dashboard->painelSsmpPendentes('Su', '', true);
$this->dashboard->painelOsmp('Su', '', true);

// -- Controle de PMP - Rede Aerea
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Rede Aerea", 2);

$this->dashboard->painelSsmpProgramadas('Ra', '', true);
$this->dashboard->painelSsmpPendentes('Ra', '', true);
$this->dashboard->painelOsmp('Ra', '', true);

// -- Controle de PMP - Telecom
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Telecom", 2);

$this->dashboard->painelSsmpProgramadas('Tl', '', true);
$this->dashboard->painelSsmpPendentes('Tl', '', true);
$this->dashboard->painelOsmp('Tl', '', true);

// -- Controle de PMP - Bilhetagem
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Bilhetagem", 2);

$this->dashboard->painelSsmpProgramadas('Bl', '', true);
$this->dashboard->painelSsmpPendentes('Bl', '', true);
$this->dashboard->painelOsmp('Bl', '', true);

/*/
// -- Controle de PMP - VLT
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Material Rodante VLT", 2);

$this->dashboard->painelSsmpProgramadas('Vlt', '', true);
$this->dashboard->painelSsmpPendentes('Vlt', '', true);
$this->dashboard->painelOsmp('Vlt', '', true);


// -- Controle de PMP - TUE
$this->dashboard->cabecalho("<span style='font-size: 35px'>&raquo;</span> Controle de PMP - Material Rodante TUE", 2);

$this->dashboard->painelSsmpProgramadas('Tue', '', true);
$this->dashboard->painelSsmpPendentes('Tue', '', true);
$this->dashboard->painelOsmp('Tue', '', true);
/*/

$this->dashboard->modalExibirCronograma();