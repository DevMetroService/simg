<?php
$tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
$actionForm = "moduloOsm";

$acao = true;
$pag = "tempoTotal";
$tipoOS = "osm";

$selectTempo = $this->medoo->select('osm_tempo','*',["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$selectTempo = $selectTempo[0];

if(empty($selectTempo)) {
    $selectAbertura = $this->medoo->select('osm_falha','data_abertura',[
        'cod_osm'       => (int) $_SESSION['refillOs']['codigoOs']
    ]);
    $selectAbertura = $selectAbertura[0];
}

$maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/temposTotais.php");
