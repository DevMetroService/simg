<?php

$tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
$actionForm = "moduloOsm";

$acao = true;
$pag = "registroExecucao";
$tipoOS = "osm";

$sistemaFixos = [20,21,24,25,27,28];

$os = $this->medoo->select("osm_falha", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$os = $os[0];

$statusOs = $this->medoo->select("status_osm", "*", ["cod_ostatus" => $os['cod_ostatus']])[0];

$os['cod_os'] = $os['cod_osm'];

//Recebe todas as OS's
$firstRE = $this->medoo->select('osm_falha', '*', ['cod_ssm' => $os['cod_ssm'], "ORDER" => 'cod_osm']);
$blocked = false; //vari�vel auxiliar para bloqueio de edi��o.
if(count($firstRE) > 1){
    // Verifica se pertence aos sistemas fixos e bloqueia edi��o.
    if(in_array($os['grupo_atuado'], $sistemaFixos) && $statusOs['cod_status'] == 10)
        $blocked = true;

    $firstRE = $firstRE[0];
    $firstRE = $this->medoo->select('osm_registro', '*', ['cod_osm' => $firstRE['cod_osm'] ]);
    $firstRE = $firstRE[0];
}else{
    $firstRE = false;
}

$ss = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$os['cod_ssm']]);
$ss = $ss[0];

$ss['cod_ss'] = $ss['cod_ssm'];

if($os['grupo_atuado'] == 22){
    $materialRodante = $this->medoo->select("material_rodante_osm", '*', ["cod_osm" => $os['cod_os']])[0];
}

$registro = $this->medoo->select("osm_registro","*",["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$registro = $registro[0];

$unEquipe = $this->medoo->select("un_equipe", ['[><]equipe' => 'cod_equipe'],"*", ["cod_un_equipe" => (int)$registro['cod_un_equipe']]);
$unEquipe = $unEquipe[0];

$maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/registroExecucao.php");