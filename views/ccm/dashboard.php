<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">CCM - Centro de Controle da Manuten��o</h1>
    </div>
    <?php
    if(!empty($_SESSION['erroCadastroModel'])){
        echo ("<div class='alert-danger'>{$_SESSION['erroCadastroModel']}</div>");
        unset($_SESSION['erroCadastroModel']);
    }
    $this->dashboard->modalUserForm($this->dadosUsuario);

    ?>

    <div class="row scrollAuto">
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-file-text-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="quantSafCcm">
                                <?php
                                echo($this->quantidadeSAF);
                                ?>
                            </div>
                            <div>SAF(s) Aberta(s)</div>
                        </div>
                    </div>
                </div>
                <a href="#SAF">
                    <div class="panel-footer">
                        <span class="pull-left">Ver SAF's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="<?php
                            if ($this->quantidadeSSM == 0) {
                                echo 'fa fa-cog fa-5x';
                            } else {
                                echo 'fa fa-cog fa-spin fa-5x';
                            }
                            ?>"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="quantSsmCcm"><?php
                                echo $this->quantidadeSSM
                                ?></div>
                            <div>SSM(s) Abertas</div>
                        </div>
                    </div>
                </div>
                <a href="#ssmAberta">
                    <div class="panel-footer">
                        <span class="pull-left">Ver SSM's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="<?php
                            if ($this->quantidadeOSM == 0) {
                                echo 'fa fa-cog fa-5x';
                            } else {
                                echo 'fa fa-cog fa-spin fa-5x';
                            }
                            ?>"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php
                                echo $this->quantidadeOSM
                                ?></div>
                            <div>OSM(s) em Execu��o</div>
                        </div>
                    </div>
                </div>
                <a href="#OSM">
                    <div class="panel-footer">
                        <span class="pull-left">Ver OSM's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="<?php
                            if ($this->quantidadeOSMfechamento == 0) {
                                echo 'fa fa-cog fa-5x';
                            } else {
                                echo 'fa fa-cog fa-spin fa-5x';
                            }
                            ?>"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php
                                echo $this->quantidadeOSMfechamento
                                ?></div>
                            <div>OSM(s) sem fechamento</div>
                        </div>
                    </div>
                </div>
                <a href="#OSM">
                    <div class="panel-footer">
                        <span class="pull-left">Ver OSM's</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <?php
    $this->dashboard->auditoriaSaf($this->dadosUsuario['cod_usuario']);
    $safDev = $this->medoo->select("v_saf", "*", [
        "AND" => [
            "nome_status" => "Devolvida",
            "usuario" => $this->dadosUsuario['usuario']
        ]
    ]);
    $quantDevolvida = count($safDev);
    if($quantDevolvida > 0){ ?>
        <div class="row" id="safDevolvida">
            <div class="col-md-12">
                <div class='panel-danger panel'>
                    <div class="panel-heading" role="tab" id="headingSafDevolvida">
                        <div class="row">
                            <div class="col-xs-3">
                                <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorSafDevolvida" aria-expanded="false" aria-controls="collapseSafDevolvida">
                                    <button class="btn btn-default"><label>SAF's Devolvidas</label></button>
                                </a>
                            </div>
                            <?php
                            if ($quantDevolvida > 0)
                                echo"<div class='col-xs-9'><h4>".$quantDevolvida." Saf(s) Devolvida(s) ao Usu�rio</h4></div>";
                            ?>
                        </div>
                    </div>
                    <div id="divIndicadorSafDevolvida" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingSafDevolvida">
                        <div class="panel-body"  id="tabelaSafDevolvida">
                            <table id="indicadorSafDevolvida" class="table table-striped table-bordered no-footer">
                                <thead id="headIndicadorSaf">
                                <tr role="row">
                                    <th>N�</th>
                                    <th>Local</th>
                                    <th>Data de Abertura</th>
                                    <th>Motivo</th>
                                    <th>N�vel</th>
                                    <th>A��o</th>
                                </tr>
                                </thead>
                                <tbody id="bodyIndicadorSafDevolvida">
                                <?php

                                if (!empty($safDev)) {
                                    foreach ($safDev as $dados) {
                                        echo('<tr>');
                                        echo('<td>' . $dados['cod_saf'] . '</td>');
                                        echo('<td>' . $dados['nome_linha'] . ' ' . $dados['nome_trecho'] . '</td>');
                                        echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                                        echo('<td>' . $dados['descricao_status'] . '</td>');
                                        echo('<td>' . $dados['nivel'] . '</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/saf/returned/{$dados['cod_saf']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>


    <?php
    $safDev = $this->medoo->select("v_saf", "*", [
        "AND" => [
            "nome_status" => "Devolvida"
        ]
    ]);

    $dias = "+5 days";

    $result = "";

    foreach($safDev as $dados =>$value){

        $dataLimite = date('d-m-Y', strtotime("+5 days", strtotime($value['data_status'])));
        $dataAtual = date('d-m-Y');

        if (strtotime($dataAtual) > strtotime($dataLimite)) {
            $result .= "
                    <tr>
                        <td>{$value['cod_saf']}</td>
                        <td>{$value['nome_linha']} {$value['nome_trecho']} </td>
                        <td>{$this->parse_timestamp($value['data_abertura'])}</td>
                        <td>{$value['descricao_status']}</td>
                        <td>{$value['nivel']}</td>
                        <td>
                        <a href='{$this->home_uri}/saf/edit/{$dados['cod_saf']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>
                            <button class='btn btn-default btn-circle btnImprimirSaf' type='button' title='Imprimir'><i class='fa fa-print fa-fw'></i></button>
                        </td>
                    </tr>
                ";
        }
    }

    $quantDevolvida = count($safDev);
    if($quantDevolvida > 0){ ?>
        <div class="row" id="safDevolvida">
            <div class="col-md-12">
                <div class='panel-danger panel'>
                    <div class="panel-heading" role="tab" id="headingSafDevolvida">
                        <div class="row">
                            <div class="col-xs-3">
                                <a class="collapsed" role="button" data-toggle="collapse" href="#divIndicadorSafDevolvida" aria-expanded="false" aria-controls="collapseSafDevolvida">
                                    <button class="btn btn-default"><label>SAF's Devolvidas</label></button>
                                </a>
                            </div>
                            <?php
                            if ($quantDevolvida > 0)
                                echo"<div class='col-xs-9'><h4>".$quantDevolvida." Saf(s) Devolvida(s)</h4></div>";
                            ?>
                        </div>
                    </div>
                    <div id="divIndicadorSafDevolvida" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingSafDevolvida">
                        <div class="panel-body"  id="tabelaSafDevolvida">
                            <table id="indicadorSafDevolvida" class="table table-striped table-bordered no-footer">
                                <thead id="headIndicadorSaf">
                                <tr role="row">
                                    <th>N�</th>
                                    <th>Local</th>
                                    <th>Data de Abertura</th>
                                    <th>Motivo</th>
                                    <th>N�vel</th>
                                    <th>A��o</th>
                                </tr>
                                </thead>
                                <tbody id="bodyIndicadorSafDevolvida">
                                <?php
                                echo $result;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>


    <div class="row" id="SAF">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SAF Aberta</label></div>

                <div class="panel-body" id="divIndicadorSaf">
                    <table id="indicadorSaf" class="table table-striped table-bordered no-footer">
                        <thead id="headIndicadorSaf">
                            <tr role="row">
                                <th>N�</th>
                                <th>Local</th>
                                <th>Data de Abertura</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                        </thead>

                        <tbody id="bodyIndicadorSaf" class="bodySaf">
                            <?php
                                $saf = $this->medoo->select("v_saf", "*", ["nome_status" => "Aberta"]);

                                if(!empty($saf)){
                                    foreach($saf as $dados){
                                        echo('<tr>');
                                        echo('<td>'.$dados['cod_saf'].'</td>');
                                        echo('<td><span class="primary-info">'.$dados['nome_linha'] .'</span><br /><span class="sub-info">'. $dados['descricao_trecho'].'</span></td>');
                                        echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                        echo('<td>'.$dados['nivel'].'</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/saf/edit/{$dados['cod_saf']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="ssmAberta">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SSM Abertas</label></div>

                <div class="panel-body">
                    <table id="indicadorSsmAberta" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Data de Abertura</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                $ssm = $this->medoo->select("v_ssm", "*", ["cod_status" => 9]); // Aberta

                                if(!empty($ssm)){
                                    foreach($ssm as $dados){
                                        echo('<tr>');
                                        echo('<td>'.$dados['cod_ssm'].'</td>');
                                        echo('<td>'.$dados['cod_saf'].'</td>');
                                        echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                        echo('<td>'.$dados['nivel'].'</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SSM Devolvidas e Transferidas</label></div>

                <div class="panel-body">
                    <table id="indicadorSsmDevolvida" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Data de Abertura</th>
                                <th>Status</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                $ssm = $this->medoo->select("v_ssm", "*", ["cod_status" => [6,8] ]); // Devolvida ou Transferida

                                if(!empty($ssm)){
                                    foreach($ssm as $dados){
                                        echo('<tr>');
                                        echo('<td>'.$dados['cod_ssm'].'</td>');
                                        echo('<td>'.$dados['cod_saf'].'</td>');
                                        echo('<td>'.$this->parse_timestamp($dados['data_abertura']).'</td>');
                                        echo('<td><span class="primary-info">'.$dados['nome_status'].'</span><br /><span class="sub-info">'.$dados['descricao_status'].'</span></td>');
                                        echo('<td>'.$dados['nivel'].'</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><label>SSM Encaminhadas</label></div>

                <div class="panel-body">
                    <table id="indicadorSsmEncaminhada" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr role="row">
                                <th>N� Ssm</th>
                                <th>N� Saf</th>
                                <th>Encaminhamento</th>
                                <th>Servi�o</th>
                                <th>Equipe</th>
                                <th>N�vel</th>
                                <th>A��o</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                $ssm = $this->medoo->select("v_ssm", "*",["cod_status" => 12]); // Encaminhado

                                if(!empty($ssm)){
                                    foreach($ssm as $dados){
                                        echo('<tr>');
                                        echo('<td>'.$dados['cod_ssm'].'</td>');
                                        echo('<td>'.$dados['cod_saf'].'</td>');
                                        echo('<td>'.$this->parse_timestamp($dados['data_status']).'</td>');
                                        echo('<td><span class="primary-info">'.$dados['nome_servico'].'</span><br /><span class="sub-info">'.$dados['complemento_servico'].'</span></td>');
                                        echo('<td><span class="primary-info" data-placement="right" rel="tooltip-wrapper" data-title="'.$dados['nome_equipe'].' - '.$dados['nome_unidade'].'">'.$dados['sigla_equipe'].' - '.$dados['sigla_unidade'].'</span></td>');
                                        echo('<td>'.$dados['nivel'].'</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/ssm/edit/{$dados['cod_ssm']}'><button class='btn btn-primary btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button></a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="OSM">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-2">
                            <label>OSM em Execu��o</label>
                        </div>
                        <div class="col-md-offset-6 col-md-2">
                            <?php
                            $this->btnDownloadFormOsm('formOsm.xlsx');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="indicadorOsmExecucao" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr role="row">
                                <th>N� Osm</th>
                                <th>N� Ssm</th>
                                <th>Data de Abertura</th>
                                <th>Equipe</th>
                                <th>Local</th>
                                <th>Nivel</th>
                                <th>A��o</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                $sql = "SELECT osm.data_abertura AS data_abertura_osm,* "
                                          . "FROM osm_falha osm "
                                          . "JOIN status_osm ostatus USING (cod_ostatus)"
                                          . "JOIN v_ssm ssm USING (cod_ssm)"
                                          . "WHERE ostatus.cod_status = 10";

                                $osm = $this->medoo->query($sql);

                                if(!empty($osm)){
                                    foreach($osm as $dados){
                                        echo('<tr>');
                                        echo('<td>'.$dados['cod_osm'].'</td>');
                                        echo('<td>'.$dados['cod_ssm'].'</td>');
                                        echo('<td>'.$this->parse_timestamp($dados['data_abertura_osm']).'</td>');
                                        echo('<td><span class="primary-info" data-placement="right" rel="tooltip-wrapper" data-title="'.$dados['nome_equipe'].' - '.$dados['nome_unidade'].'">'.$dados['sigla_equipe'].' - '.$dados['sigla_unidade'].'</span></td>');
                                        echo('<td><span class="primary-info">'.$dados['nome_linha'] .'</span><br /><span class="sub-info">'. $dados['descricao_trecho'].'</span></td>');
                                        echo('<td>'.$dados['nivel'].'</td>');
                                        echo('<td>');
                                        echo("<a href='{$this->home_uri}/OSM/dadosGerais/{$dados['cod_osm']}'>
                                                    <button class='btn btn-default btn-circle' type='button' title='Ver Dados Gerais/Editar'><i class='fa fa-file-o fa-fw'></i></button>
                                                </a>");
                                        echo('<button class="btn btn-default btn-circle btnImprimirOsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                                        echo('</td>');
                                        echo('</tr>');
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
