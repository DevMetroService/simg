<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 18/05/2017
 * Time: 11:14
 */
?>

<div class="modal fade ExibirCronograma" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="overflow:auto;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3 class="modal-title">Item do Cronograma</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>Cod. Cronograma</label>
                        <input class="form-control" readonly name="cronogramaModal"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo</label>
                        <input class="form-control" readonly name="grupoModal"/>
                    </div>
                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input class="form-control" readonly name="sistemaModal"/>
                    </div>
                    <div class="col-md-4">
                        <label>SubSistema</label>
                        <input class="form-control" readonly name="subsistemaModal"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label>Servi�o</label>
                        <input class="form-control" readonly name="servicoModal"/>
                    </div>
                    <div class="col-md-2">
                        <label>Procedimento</label>
                        <input class="form-control" readonly name="procedimentoModal"/>
                    </div>
                    <div class="col-md-2">
                        <label>Periodicidade</label>
                        <input class="form-control" readonly name="periodicidadeModal"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Linha</label>
                        <input class="form-control" readonly name="linhaModal"/>
                    </div>
                    <div class="col-md-8">
                        <label>Local</label>
                        <textarea class="form-control" readonly name="localModal"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <label>Quinzena Prevista</label>
                        <input class="form-control" readonly name="quinzenaModal"/>
                    </div>
                    <div class="col-md-3">
                        <label>Homem - Hora</label>
                        <input class="form-control" readonly name="hhModal"/>
                    </div>
                    <div class="col-md-3">
                        <label>Situa��o</label>
                        <input class="form-control" readonly name="statusModal"/>
                    </div>
                </div>
                <div class="dadosSsmp" style="margin-top: 2em"></div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="tableRelModal table table-bordered table-responsive">
                            <thead>
                            <th>Cod. Osmp</th>
                            <th>Cod. Ssmp</th>
                            <th>Data de Abertura</th>
                            <th>Status</th>
                            <th>A��es</th>
                            </thead>

                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-target=".listaCronogramas"
                        data-toggle="modal">Retornar
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade listaCronogramas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="overflow: auto;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><label>Lista de itens do Cronograma</label>
                    <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="fa fa-close"></i></button>
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="indicadorListaCronograma_modal"
                               class="table table-striped table-responsive table-bordered no-footer">
                            <thead>
                            <tr>
                                <th>Cod. Cronograma</th>
                                <th>Quinzena de execu��o</th>
                                <th>Sistema</th>
                                <th>Sub-Sistema</th>
                                <th>Servi�o</th>
                                <th>Status</th>
                                <th>A��o</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade osDetalhes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="overflow: auto;">
    <div class=" modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <h3 class="modal-title"><label>Detalhes da Ordem de Servi�o</label>
            <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="fa fa-close"></i></button>
        </h3>
    </div>
    <div class="modal-body">
        <div class="row panel">
            <div class="col-md-4">
                <label>C�digo OSMP</label>
                <input readonly value="" class="form-control" name="cod_osmp_modal">
            </div>
            <div class="col-md-4">
                <label>Grupo de Sistema</label>
                <input readonly value="" class="form-control" name="grupo_sistema_modal">
            </div>
        </div>
        <!-- Tempos Totais -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading"><label>Tempos Totais</label></div>

                    <div class="panel-body">
                        <label>Prepara��o e Acesso</label>
                        <div class="row">
                            <div class="col-md-2">
                                <label>In�cio</label>
                                <input class="form-control" readonly name="preparacaoInicio_modal" value="">
                            </div>
                            <div class="col-md-2">
                                <label>Pronto para Sair</label>
                                <input class="form-control" readonly name="acessoProntoSair_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Sa�da Autorizada</label>
                                <input class="form-control" readonly name="acessoSaidaAutorizada_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Sa�da</label>
                                <input class="form-control" readonly name="acessoSaida_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Chegada</label>
                                <input class="form-control" readonly name="acessoChegada_modal"/>
                            </div>
                        </div>
                        <label>Execu��o do Servi�o</label>
                        <div class="row">
                            <div class="col-md-2">
                                <label>In�cio</label>
                                <input class="form-control" readonly name="execucaoInicio_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>T�rmino</label>
                                <input class="form-control" readonly name="execucaoTermino_modal"/>
                            </div>
                        </div>
                        <label>Regresso</label>
                        <div class="row">
                            <div class="col-md-2">
                                <label>Pronto para Sair</label>
                                <input class="form-control" readonly name="regressoProntoSair_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Sa�da Autorizada</label>
                                <input class="form-control" readonly name="regressoSaidaAutorizada_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Chegada</label>
                                <input class="form-control" readonly name="regressoChegada_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Desmobiliza��o</label>
                                <input class="form-control" readonly name="regressoDesmobilizacao_modal"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- M�o de Obra -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>M�o de Obra</label></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="indicadorMaoObra_modal"
                                       class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>Funcionario</th>
                                        <th>Inicio</th>
                                        <th>T�rmino</th>
                                        <th>Homem Hora</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Registro de Execu��o -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Registro de Execu��o</label></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Causa</label>
                                <textarea class="form-control" readonly name="causa_modal"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>Atua��o</label>
                                <textarea class="form-control" readonly name="atuacao_modal"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>Agente Causador</label>
                                <textarea class="form-control" readonly name="agenteCausador_modal"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Observa��es</label>
                                <textarea class="form-control" readonly
                                          name="observacoesRegistroExecucao-modal"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- M�quinas e Equipamentos -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>M�quinas e Equipamentos</label></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="indicadorMaoObra_modal"
                                       class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>Funcionario</th>
                                        <th>Inicio</th>
                                        <th>T�rmino</th>
                                        <th>Homem Hora</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Material -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><label>Material Utilizado</label></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="indicadorMaoObra_modal"
                                       class="table table-striped table-bordered no-footer">
                                    <thead>
                                    <tr>
                                        <th>Funcionario</th>
                                        <th>Inicio</th>
                                        <th>T�rmino</th>
                                        <th>Homem Hora</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Encerramento -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading"><label>Encerramento</label></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4" id="tipoFechamento_modal">
                                <label>Tipo de Fechamento</label>
                                <input class="form-control" readonly name="tipoFechamento_modal" value="">
                            </div>
                            <div class="col-md-4" id="tipoPendencia_modal">
                                <label>Tipo de Pend�ncia</label>
                                <input class="form-control" readonly name="tipoPendencia_modal"/>
                            </div>
                            <div class="col-md-4" id="pendencia_modal">
                                <label>Pend�ncia</label>
                                <input class="form-control" readonly name="pendencia_modal"/>
                            </div>
                            <div class="col-md-8" id="naoExecutado_modal">
                                <label>Motivo da N�o Execu��o</label>
                                <input class="form-control" readonly name="motivoNaoExecucao_modal"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label>Liberado</label>
                                <input class="form-control" readonly name="liberado_modal"/>
                            </div>
                            <div class="col-md-2">
                                <label>Tranferido</label>
                                <input class="form-control" readonly name="transferido_modal"/>
                            </div>
                            <div class="col-md-4">
                                <label>Data do Recebimento das Informa��es</label>
                                <input class="form-control" readonly name="dataRecebimentoInformacao_modal"/>
                            </div>
                            <div class="col-md-4">
                                <label>Respons�vel pelas Informa��es</label>
                                <input class="form-control" readonly name="responsavelInformacao_modal"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Dados da Transfer�ncia</label>
                                <input class="form-control" readonly name="dadosTransferencia_modal"/>
                            </div>
                            <div class="col-md-8">
                                <label>Recomenda��es</label>
                                <input class="form-control" readonly name="recomendacaoTranferencia_modal"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" data-target=".ExibirCronograma"
                    data-toggle="modal">Retornar
            </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        </div>
    </div>
</div>
</div>
</div>

<div class="page-header">
    <h1>M�dulo de Ordem de Servi�o</h1>
</div>

<!--<button class='btn btn-default btnDadosFuncionario' data-toggle='modal' data-target='.osDetalhes'><i class='fa fa-eye fa-fw'></i></button>-->
<?php

$procedimentos = $this->medoo->select("servico_pmp_periodicidade",
    [
        "[><]procedimento" => "cod_procedimento",
        "[><]servico_pmp_sub_sistema" => "cod_servico_pmp_sub_sistema",
        "[><]grupo" => "cod_grupo",
        "[><]servico_pmp" => "cod_servico_pmp",
        "[><]tipo_periodicidade" => "cod_tipo_periodicidade",
        "[><]sub_sistema" => "cod_sub_sistema",
        "[><]sistema" => "cod_sistema"
    ],
    ["cod_servico_pmp_periodicidade", "nome_servico_pmp", "nome_sistema", "nome_periodicidade", "nome_procedimento", "nome_grupo"],
    [
        "ORDER" => "nome_servico_pmp"
    ]);
$this->printModuloNavigator();
//var_dump($procedimentos);
$this->dashboard->graficoAcompanhamentoAnoAtual();

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-10">
                        <h4>Procedimentos Padronizados</h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="indicadorProcedimentoPadronizado"
                               class="table table-striped table-bordered no-footer">
                            <thead>
                            <tr>
                                <th>C�digo</th>
                                <th>Servi�o</th>
                                <th>Grupo de Sistema</th>
                                <th>Sistema</th>
                                <th>Periodicidade</th>
                                <th>Procedimento</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($procedimentos as $dados => $value) {
                                echo("<tr>");
                                echo("<td>{$value['cod_servico_pmp_periodicidade']}</td>");
                                echo("<td>{$value['nome_servico_pmp']}</td>");
                                echo("<td>{$value['nome_grupo']}</td>");
                                echo("<td>{$value['nome_sistema']}</td>");
                                echo("<td>{$value['nome_periodicidade']}</td>");
                                echo("<td><a href='' class='exibirPdfLink' value='dashboardDiretoria/exibirPdfProcedimento:{$value['nome_procedimento']}'>{$value['nome_procedimento']}</a></td>");
                                echo("</tr>");
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
