<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/05/2017
 * Time: 10:35
 */
?>

<div class="page-header">
    <h1>M�dulo de Engenharia</h1>
</div>

<?php
$this->printModuloNavigator();
?>

<div class="row">
    <div class="col-md-12">
        <div class="relCronograma"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Fluxograma de Estrutura de OS's</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carousel-fluxograma" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-fluxograma" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-fluxograma" data-slide-to="1"></li>
                                <li data-target="#carousel-fluxograma" data-slide-to="2"></li>
<!--                                <li data-target="#carousel-fluxograma" data-slide-to="3"></li>-->
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="<?php echo HOME_URI; ?>/views/_images/fluxogramas/FluxFalha.png" alt="" width="800px" height="1000px">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="<?php echo HOME_URI; ?>/views/_images/fluxogramas/FluxSspCorretiva.png" alt="" width="800px" height="1000px">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
<!--                                <div class="item">-->
<!--                                    <img src="--><?php //echo HOME_URI; ?><!--/views/_images/fluxogramas/FluxSspProgramada.png" alt="" width="800px" height="1000px">-->
<!--                                    <div class="carousel-caption">-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="item">
                                    <img src="<?php echo HOME_URI; ?>/views/_images/fluxogramas/FluxoPmp.png" alt="" width="800px" height="1000px">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-fluxograma" role="button" data-slide="prev">
                                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-fluxograma" role="button" data-slide="next">
                                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Relat�rios</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/APP" class="list-group-item">
                                <h4 class="list-group-item-heading">APP</h4>
                                <p class="list-group-item-text">Aproveitamento de Programa��es Preventivas</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/Corretivas60Dias" class="list-group-item">
                                <h4 class="list-group-item-heading">60 Dias</h4>
                                <p class="list-group-item-text">Relat�rio de Indicador de Atividades superiores a 60 dias.</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/PreventivaCorretiva" class="list-group-item">
                                <h4 class="list-group-item-heading">Preventiva / Corretiva</h4>
                                <p class="list-group-item-text">Gr�fico Comparativo Entre Corretivas e Preventivas</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/AcompanhamentoAnual" class="list-group-item">
                                <h4 class="list-group-item-heading">Acompanhamento Anual</h4>
                                <p class="list-group-item-text">Gr�fico de Preventivas Por Ano</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>/dashboardGeral/relatoriosDiversos/CronogramaSimples" class="list-group-item">
                                <h4 class="list-group-item-heading">Cronograma Simples</h4>
                                <p class="list-group-item-text">Gerador do Cronograma Simples do PMP</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item"  data-toggle='modal' data-target='#imprimirPMPEdModalCronograma'>
                                <h4 class="list-group-item-heading">Cronograma Completo Edifica��o</h4>
                                <p class="list-group-item-text">Gerador do Cronograma Completo do PMP de Edifica��o.</p>
                            </a>
                            <?php
                            $this->modalCronograma("Ed", "E", "Edifica��o", 21);
                            ?>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item"  data-toggle='modal' data-target='#imprimirPMPVpModalCronograma'>
                                <h4 class="list-group-item-heading">Cronograma Completo Via Permanente</h4>
                                <p class="list-group-item-text">Gerador do Cronograma Completo do PMP de Via Permanente.</p>
                            </a>
                            <?php
                            $this->modalCronograma("Vp", "V", "Via Permanente", 24);
                            ?>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item"  data-toggle='modal' data-target='#imprimirPMPSuModalCronograma'>
                                <h4 class="list-group-item-heading">Cronograma Completo Subesta��o</h4>
                                <p class="list-group-item-text">Gerador do Cronograma Completo do PMP de Subesta��o.</p>
                            </a>
                            <?php
                            $this->modalCronograma("Su", "S", "Subesta��o", 25);
                            ?>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item"  data-toggle='modal' data-target='#imprimirPMPRaModalCronograma'>
                                <h4 class="list-group-item-heading">Cronograma Completo Rede A�rea</h4>
                                <p class="list-group-item-text">Gerador do Cronograma Completo do PMP de Rede A�rea.</p>
                            </a>
                            <?php
                            $this->modalCronograma("Ra", "R", "Rede A�rea", 20);
                            ?>
                        </div>
<!--                        <div class="list-group">-->
                        <!--                            <a href="-->
                        <?php //echo HOME_URI; ?><!--dashboardGeral/relatoriosDiversos/LevantamentoOcorrencias" class="list-group-item">-->
                        <!--                                <h4 class="list-group-item-heading">Levantamento de Ocorr�ncias</h4>-->
                        <!--                                <p class="list-group-item-text">Gerador de Relat�rio de Ocorr�ncias Executadas e Canceladas por Linha e Grupo de Sistema</p>-->
                        <!--                            </a>-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>