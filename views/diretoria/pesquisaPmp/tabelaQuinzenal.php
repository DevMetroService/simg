<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 20/04/2016
 * Time: 17:01
 */

$dadosTabela = $_SESSION['tabela'];
unset($_SESSION['tabela']);
$coluna = "Servi�o de Manuten��o";
if($dadosTabela['servicoPmp']) {
    $titulo[] = 1;
    $where[] = "sp.cod_servico_prev = {$dadosTabela['servicoPmp']}";
    $coluna = "Esta��o";
}
if($dadosTabela['localPmp']) {
    $titulo[] = 2;
    $where[] = "est.cod_estacao = {$dadosTabela['localPmp']}";
    if($coluna == "Esta��o")
        $coluna = "Periodicidade";
}
if($dadosTabela['linhaPmp']) {
    $titulo[] = 3;
    $where[] = "est.cod_linha = {$dadosTabela['linhaPmp']}";
}
if($dadosTabela['periodicidadePmp']){
    $titulo[] = 4;
    $where[] = "pmp.quant_intervalos= {$dadosTabela['periodicidadePmp']}";
}

$sql = "SELECT * FROM pmp_edificacao pmp
            JOIN estacao est USING(cod_estacao)
            JOIN servico_preventivo sp USING(cod_servico_prev)
            JOIN tipo_periodicidade tp USING(cod_tipo_periodicidade)
            JOIN linha USING (cod_linha)";

if (count($where)) {
    $sql = $sql . ' WHERE ' . implode(' AND ', $where);
}
$resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$header;
foreach ($titulo as $dados){
    switch ($dados){
        case 1:
            $header[] = $resultado[0]['nome_servico_prev'];
            break;
        case 2:
            $header[] = "Esta��o " . $resultado[0]['nome_estacao'];
            break;
        case 3:
            $header[] = $resultado[0]['nome_linha'];
            break;
        case 4:
            $header[] = $this->intervalo[ $resultado[0]['quant_intervalos'] ] ;
            break;
    }
}

if(count($header)){
    $titulo = implode(" - ", $header);
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3>PMP - Edifica��es - <?php echo $titulo?></h3></div>
            <div class="panel-body">
                <table id="indicadorTabela" class="table table-striped static table-bordered no-footer">
                    <thead>
                    <tr>
                        <td><label>N�</label></td>
                        <td><label><?php echo $coluna ?></label></td>
                        <?php
                        for($i=1; $i <25; $i++){
                            echo("<td>{$i}</td>");
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $linha ="";
                    foreach($resultado as $dados=>$value){

                        echo ("<tr>");
                        echo("<td>{$value['cod_pmp_edificacao']}</td>");
                        echo ("<td>");
                        //Preenche a primeira coluna
                        if($coluna == "Servi�o de Manuten��o")
                            echo($value['nome_servico_prev']);
                        else
                            if($coluna=='Esta��o')
                                echo($value['nome_linha'].' - '. $value['nome_estacao']);
                            else
                                echo($value['nome_periodicidade']);

                        echo ("</td>");

                        //Preenche as demais colunas
                        for($i=1; $i <25; $i++){
                            if($value['qzn'] == $i)
                                echo("<td><label><button class='btn-success btn btn-large' data-toggle='modal' data-action='{$value['cod_pmp_edificacao']}' data-target='#tabelaPmpModal'></button></label></td>");
                            else
                                echo("<td><label></label></td>");
                        }

                        echo ("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--MODAL-->
<!--MODAL-->
<!--MODAL-->
<div class="row">
    <div class="col-md-12">
        <div class="modal fade" id="tabelaPmpModal" tabindex="-1" role="form" aria-labelledby="tabelaPmpLabel" data-backdrop="static" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title" id="tabelaPmpLabel">Programa��o</h4>
                    </div>

                    <form action="" method="post" class="form-group">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>N� PMP</label>
                                    <input type="text" name="nPmp" readonly class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>S.A.</label>
                                    <input type="text" name="nSa" readonly class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Status</label>
                                    <input type="text" name="statusPmp" readonly class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Periodicidade</label>
                                    <input type="text" name="periodicidadePmp" readonly class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Data de Inicio</label>
                                    <input type="text" name="dataInicio" readonly class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Qtd. de Dias</label>
                                    <input type="text" name="qtdDias" readonly class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label>Qzn. de Execu��o</label>
                                    <input type="text" name="qznExecucao" readonly class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Procedimentos</label>
                                    <input type="text" name="procedimento" readonly class="form-control procedimento">
                                </div>
                            </div>
                            <div class="row" style="padding: 10px">
                                <div class="col-md-12">
                                    <table id="indicadorTabelaPmp" class="table table-striped static table-bordered no-footer">
                                        <thead>
                                        <tr>
                                            <td>N�mero</td>
                                            <td>Data de Execu��o</td>
                                            <td>A��o</td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

