<?php

require_once(ABSPATH . '/functions/functionsRelatorio.php');

$sql = "SELECT os.cod_osm, data_abertura, cod_ssm, nome_grupo, nome_linha, nome_servico FROM osm_falha os
	JOIN grupo ON(grupo.cod_grupo = os.grupo_atuado)
	JOIN linha ON(linha.cod_linha = os.cod_linha_atuado)
	JOIN osm_servico USING(cod_osm)
	JOIN servico USING(cod_servico)
	JOIN status_osm USING(cod_ostatus)
	WHERE cod_status = 10";

$safAbertas = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$validacao = false;
if(in_array($this->dadosUsuario['cod_usuario'], $this->dashboard->usuarioValidacao)){
    $validacao = true;
}

$sql = "SELECT * FROM v_ssm WHERE cod_status = 35";

$ssmAguardando['edificacao'] = $this->medoo->query($sql . " AND cod_grupo = 21")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['viaPermanente'] = $this->medoo->query($sql . " AND cod_grupo = 24")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['subestacao'] = $this->medoo->query($sql . " AND cod_grupo = 25")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['energia'] = $this->medoo->query($sql . " AND cod_grupo = 29")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['redeAerea'] = $this->medoo->query($sql . " AND cod_grupo = 20")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['bilhetagem'] = $this->medoo->query($sql . " AND cod_grupo = 28")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['telecom'] = $this->medoo->query($sql . " AND cod_grupo = 27")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['tue'] = $this->medoo->query($sql . " AND cod_grupo = 23")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['vlt'] = $this->medoo->query($sql . " AND cod_grupo = 22")->fetchAll(PDO::FETCH_ASSOC);

$somaValidacao = 0;
foreach($ssmAguardando as $dados){
    $somaValidacao += count($dados);
}
$btnAcoes = '<button class="btn btn-default btn-circle btnEditarOsm" type="button" title="Ver Dados Gerais / Editar"><i class="fa fa-file-o fa-fw"></i></button>
             <button class="btn btn-default btn-circle btnImprimirOsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
             <button class="btn btn-success btn-circle btnAbrirPesquisaSsm" type="button" title="Abrir Pesquisa SSM">SSM</button> ';

?>

    <div class="col-lg-12">
        <h1 class="page-header">Diretoria</h1>
    </div>

    <!-- Falhas Abertas -->
   <div class="modal fade safAberta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"><label>Falhas abertas</label>
                        <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="fa fa-close"></i></button>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tableSafAbertas" class="table table-bordered table-striped text-center">
                                <thead>
                                <tr>
                                    <th>OSM</th>
                                    <th>Data Abertura</th>
                                    <th>Grupo</th>
                                    <th>Linha</th>
                                    <th>Servi�o</th>
                                    <th>A��es</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($safAbertas as $dados) {
                                    echo("<tr>");
                                    echo("<td>{$dados['cod_osm']}</td>");
                                    echo("<td>{$this->parse_timestamp($dados['data_abertura'])}</td>");
                                    echo("<td>{$dados['nome_grupo']}</td>");
                                    echo("<td>{$dados['nome_linha']}</td>");
                                    echo("<td>{$dados['nome_servico']}</td>");
                                    echo("<td>{$btnAcoes}</td>");
                                    echo("</tr>");
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 60 Dias -->
    <!--<button class="btn btn-primary" data-toggle="modal" data-target=".saf60">Large modal</button>-->
    <div class="modal fade saf60" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"><label>Falhas com mais de 60 Dias</label>
                        <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="fa fa-close"></i></button>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tableSaf60" class="table table-bordered table-striped text-center">
                                <thead>
                                <tr>
                                    <th>N� SAF</th>
                                    <th>Grupo</th>
                                    <th>Linha</th>
                                    <th>Avaria</th>
                                    <th>Data Abertura</th>
                                    <th>Dias Aberto</th>
                                    <th>A��es</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
																$grupoWhere = implode(",", $this->sistemas_fixos);
																
																$sql60dias = "SELECT date_part('day', SUM(COALESCE(data_termino_status, CURRENT_TIMESTAMP) - ss.data_status)) as diasEmAberto, 
																cod_saf, nome_grupo, nome_linha || ' - ' ||  descricao_trecho as local, nome_avaria, nivel, data_abertura
																FROM status_saf ss JOIN v_saf USING(cod_saf) 
																WHERE ss.cod_status NOT IN(2,14,27) AND cod_grupo IN ($grupoWhere) AND v_saf.cod_status NOT IN (2,14,27)
																GROUP BY cod_saf, nome_grupo, nome_linha, descricao_trecho, nome_avaria, nivel, data_abertura
																ORDER BY cod_saf ASC";
																
																
                                $sql = "SELECT *
																	FROM (
																		{$sql60dias}
																	)as saf WHERE diasEmAberto > 60";
																	
                                $dias = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                foreach ($dias as $dados) {
                                    echo("<tr>");
                                    echo("<td>{$dados['cod_saf']}</td>");
                                    echo("<td>{$dados['nome_grupo']}</td>");
                                    echo("<td>{$dados['local']}</td>");
                                    echo("<td>{$dados['nome_avaria']}</td>");
                                    echo("<td>{$this->parse_timestamp($dados['data_abertura'])}</td>");
                                    echo("<td>{$dados['diasemaberto']} dias</td>");
                                    echo("<td>
																		<button class='btn btn-default btn-circle btnEditarSaf' type='button' title='Ver Dados Gerais / Editar'><i class='fa fa-file-o fa-fw'></i></button>
												             <button class='btn btn-default btn-circle btnImprimirSaf' type='button' title='Imprimir'><i class='fa fa-print fa-fw'></i></button>
																		 <a href='{$this->home_uri}/dashboardGeral/pesquisaSafParaSsm/{$dados['cod_saf']}'>
												             	<button class='btn btn-success btn-circle' type='button' title='Abrir Pesquisa SSM'>SSM</button> 
																		 </a>
																		</td>");
                                    echo("</tr>");
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Servi�os Aguardando Valida��o -->
    <!--<button class="btn btn-primary" data-toggle="modal" data-target=".saf60">Large modal</button>-->
    <div class="modal fade aguardandoValidacao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"><label>Servi�os Aguardando Valida��o</label>
                        <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="fa fa-close"></i></button>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Servi�os</div>
                                <div class="panel-body">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#ed" aria-controls="ed" role="tab" data-toggle="tab">Edifica��es</a></li>
                                            <li role="presentation"><a href="#su" aria-controls="su" role="tab" data-toggle="tab">Subesta��o</a></li>
                                            <li role="presentation"><a href="#vp" aria-controls="vp" role="tab" data-toggle="tab">Via Permanente</a></li>
                                            <li role="presentation"><a href="#ra" aria-controls="ra" role="tab" data-toggle="tab">Rede A�rea</a></li>
                                            <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">Energia</a></li>
                                            <li role="presentation"><a href="#bi" aria-controls="bi" role="tab" data-toggle="tab">Bilhetagem</a></li>
                                            <li role="presentation"><a href="#te" aria-controls="te" role="tab" data-toggle="tab">Telecom</a></li>
                                            <li role="presentation"><a href="#tue" aria-controls="tue" role="tab" data-toggle="tab">TUE</a></li>
                                            <li role="presentation"><a href="#vlt" aria-controls="vlt" role="tab" data-toggle="tab">VLT</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="ed">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['edificacao'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="su">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['subestacao'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="en">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['energia'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="vp">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['viaPermanente'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="vlt">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['vlt'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="te">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['telecom'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="bi">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['bilhetagem'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="ra">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['redeAerea'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="tue">
                                                <div class="col-md-12" style="margin-top: 15px;">
                                                    <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                                        <thead>
                                                        <tr role="row">
                                                            <th>SSM</th>
                                                            <th>SAF</th>
                                                            <th>Grupo de Sistemas</th>
                                                            <th>Local</th>
                                                            <th>Data de Abertura</th>
                                                            <th>Avaria</th>
                                                            <th>Servi�o</th>
                                                            <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $home_uri = HOME_URI;
                                                        foreach ($ssmAguardando['tue'] as $dados => $value) {
                                                            $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                            echo "<tr>
                                                            <td>{$value['cod_ssm']}</td>
                                                            <td>{$value['cod_saf']}</td>
                                                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                            <td>{$dataAbertura}</td>
                                                            <td>{$value['nome_avaria']}</td>";
                                                            if (!empty($value['nome_veiculo'])) {
                                                                echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                            } else {
                                                                echo "<td>{$value['nome_servico']}</td>";
                                                            }

                                                            if($validacao){
                                                                echo "<td>
                                                                    <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                        <i class='fa fa-eye fa-fw'></i>
                                                                    </button>
                                                                    </a>
                                                                </td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>


<?php
$this->printModuloNavigator();
?>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-cog fa-spin fa-5x fa-fw"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="quantSafCcm">
                                <?php
                                echo count($safAbertas);
                                ?>
                            </div>
                            <div>OSM em execu��o</div>
                        </div>
                    </div>
                </div>
                <a data-toggle="modal" data-target=".safAberta">
                    <div class="panel-footer">
                        <span class="pull-left">Ver Ordens de Servi�o da Manuten��o</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-cog fa-spin fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="quantSsmCcm">
                                <?php
                                $sql = "SELECT count(cod_saf)
																		FROM (
																			{$sql60dias}
																		)as saf WHERE diasEmAberto > 60";
                                $totalsaf = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
                                echo $totalsaf[0]['count'];
                                ?>
                            </div>
                            <div>Falhas 60 dias</div>
                        </div>
                    </div>
                </div>
                <a data-toggle="modal" data-target=".saf60">
                    <div class="panel-footer">
                        <span class="pull-left">Ver Falhas com mais de 60 dias</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-cog fa-spin fa-5x fa-fw"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="quantSafCcm">
                                <?php
                                echo $somaValidacao;
                                ?>
                            </div>
                            <div>Servi�os Aguardando Valida��o</div>
                        </div>
                    </div>
                </div>
                <a data-toggle="modal" data-target=".aguardandoValidacao">
                    <div class="panel-footer">
                        <span class="pull-left">Ver Servi�os Aguardando Valida��o</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>APP - Aproveitamento de Programa��es Preventivas <?php echo date('Y') ?></h4>
                            <input type="hidden" name="title" value="<?php echo date('Y') ?>">
                        </div>
                        <div class="col-md-2" style="float: right">
                            <select name="mesApp" class="form-control">
                                <?php
                                foreach (MainController::$monthComplete as $key => $value) {
                                    echo (date('n') == $key) ? "<option value='$key' selected>$value</option>" : "<option value='$key'>$value</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div id="boxEd"></div>
                        </div>
                        <div class="col-md-3">
                            <div id="boxVp"></div>
                        </div>
                        <div class="col-md-3">
                            <div id="boxRa"></div>
                        </div>
                        <div class="col-md-3">
                            <div id="boxSb"></div>
                        </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-3">
                            <div id="boxBi"></div>
                        </div>
                        <div class="col-md-3">
                            <div id="boxTe"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->dashboard->graficoAcompanhamentoAnoAtual(); ?>