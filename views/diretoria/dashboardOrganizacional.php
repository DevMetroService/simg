<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/05/2017
 * Time: 15:48
 */
?>

<div class="page-header">
    <h1>M�dulo Organizacional</h1>
</div>
<?php
$this->printModuloNavigator();
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Organograma</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carousel-fluxograma" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-fluxograma" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-fluxograma" data-slide-to="1"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="<?php echo HOME_URI; ?>/views/_images/organograma/organogramaMF.png" alt="" width="800px" height="1000px">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="<?php echo HOME_URI; ?>/views/_images/organograma/organogramaMS.png" alt="" width="800px" height="1000px">
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-fluxograma" role="button" data-slide="prev">
                                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-fluxograma" role="button" data-slide="next">
                                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



