<?php

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$acao = true;
$pag = "materialUtilizado";
$tipoOS = "osp";

$sql = "SELECT m.cod_material, m.estado, m.origem, m.utilizado, mat.nome_material, u.sigla_uni_medida, u.cod_uni_medida "
    . "FROM osp_material m "
    . "JOIN material mat USING (cod_material) "
    . "JOIN unidade_medida u ON u.cod_uni_medida = m.unidade "
    . "WHERE m.cod_osp = " . $_SESSION['refillOs']['codigoOs'];

$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/materiaisUtilizados.php");
