<?php

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$acao = true;
$pag = "registroExecucao";
$tipoOS = "osp";

$os = $this->medoo->select("osp", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$os = $os[0];

$os['cod_os'] = $os['cod_osp'];

$ss = $this->medoo->select("v_ssp", "*", ["cod_ssp" => (int)$os['cod_ssp']]);
$ss = $ss[0];

$ss['cod_ss'] = $ss['cod_ssp'];

$registro = $this->medoo->select("osp_registro","*",["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$registro = $registro[0];

$unEquipe = $this->medoo->select("un_equipe", ['[><]equipe' => 'cod_equipe'],"*", ["cod_un_equipe" => (int)$registro['cod_un_equipe']]);
$unEquipe = $unEquipe[0];

$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/registroExecucao.php");
