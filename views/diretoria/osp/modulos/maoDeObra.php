<?php

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$acao = true;
$pag = "maoObra";
$tipoOS = "osp";

$maoObraFuncionario = $this->medoo->select("osp_mao_de_obra", ["cod_ospmao(cod_osmao)","matricula","data_inicio","data_termino"],["cod_osp" => (int) $_SESSION['refillOs']['codigoOs']]);

$funcionario;
$selectFuncionario = $this->medoo->select("funcionario", ["matricula", "nome_funcionario"]);
if ($selectFuncionario)
    foreach ($selectFuncionario as $dados)
        $funcionario[$dados['matricula']] = $dados['nome_funcionario'];

$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/maoObra.php");
