<?php
require_once ( ABSPATH . '/functions/btnForm.php');

$tituloOs = "Ordem de Servi�o Preventivo";
$actionForm = "moduloOsp";

$acao = true;
$pag = "dadosGerais";
$tipoOS = "osp";

$sql = 'SELECT  *, osp.complemento as complent
            FROM osp
            JOIN osp_servico serv USING (cod_osp)
            WHERE osp.cod_osp = ' . $_SESSION['refillOs']['codigoOs'];

$os = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
$os = $os[0];

$os['cod_os'] = $os['cod_osp'];

$responsavel = $this->medoo->select("usuario", "usuario" ,["cod_usuario" => $os['usuario_responsavel']]);
$responsavel = $responsavel[0];

$encerramento = $this->medoo->select("osp_encerramento", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$encerramento = $encerramento[0];

$tempo = $os['data_abertura'];

$ss = $this->medoo->select("v_ssp", "*",["cod_ssp" => (int) $os['cod_ssp']]);
$ss = $ss[0];

$ss['cod_ss'] = $os['cod_ssp'];

$selectDescStatus = $this->medoo->select("status_osp", ["[><]status" => "cod_status"], "*", ["cod_ospstatus" => $os['cod_ospstatus'] ]);
$selectDescStatus = $selectDescStatus[0];

$_SESSION['bloqueio'] = $this->bloquearEdicao($selectDescStatus['data_status'],$selectDescStatus['nome_status']);


$maoObra = $this->medoo->select("osp_mao_de_obra", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osp_tempo", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osp_registro", "*", ["cod_osp" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/dadosGerais.php");

modalDevolverCancelar($os, "Osp");