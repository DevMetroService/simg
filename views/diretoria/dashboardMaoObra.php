<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 29/05/2017
 * Time: 15:54
 */
?>

<div class="page-header">
    <h1>M�dulo de m�o de obra</h1>
</div>

<?php
$this->printModuloNavigator();
?>

<div class="panel panel-primary">
    <div class="panel-heading"><label>Funcionarios Cadastrados</label></div>

    <div class="panel-body">
        <?php
        $func = $this->medoo->select("funcionario",
            [
                "[>]dados_empresa_funcionario" => "cod_funcionario",
                "[>]centro_resultado(cr)"      => "cod_centro_resultado",
                "[>]cargos_funcionarios(cg)"   => "cod_cargos"
            ], [
                "matricula",
                "nome_funcionario",
                "cg.descricao(cargo)",
                "cr.descricao(centro_resultado)"
            ]);
        ?>

        <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
            <strong>Total de Itens da tabela: <?php echo !empty($func) ? count($func): ""; ?><br/><br/></strong>
            <thead>
            <tr>
                <th>Matricula</th>
                <th>Nome Funcionario</th>
                <th>Cargo</th>
                <th>Centro de Resultado</th>
                <th>A��o</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($func)){
                foreach($func as $dados){
                    echo('<tr>');
                    echo('<td>'.$dados['matricula'].'</td>');
                    echo('<td>'.$dados['nome_funcionario'].'</td>');
                    echo('<td>'.$dados['cargo'].'</td>');
                    echo('<td>'.$dados['centro_resultado'].'</td>');
                    echo('<td>');
                    echo('<button class="btn btn-primary btn-circle btnDadosFuncionario" type="button" data-target=".exibirDadosFuncionario" data-toggle="modal" title="Ver Dados Gerais/Editar"><i class="fa fa-eye fa-fw"></i></button> ');
                    echo('</td>');
                    echo('</tr>');
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Relat�rios</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>/dashboardGeral/relatorio/hh" class="list-group-item">
                                <h4 class="list-group-item-heading">Homem Hora</h4>
                                <p class="list-group-item-text">Relat�rio de Homem Hora.</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="list-group">
                            <a href="" value='dashboardDiretoria/exibirPdfTreinamento:treinamentoFuncionario' class="list-group-item exibirPdfLink">
                                <h4 class="list-group-item-heading">Treinamento X Funcion�rio</h4>
                                <p class="list-group-item-text">Tabela de treinamentos dos funcion�rios por cargos.</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade exibirDadosFuncionario" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3 class="modal-title">Dados Funcionario</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>Matricula</label>
                        <input class="form-control" readonly id="matriculaModal" />
                    </div>
                    <div class="col-md-10">
                        <label>Nome Funcionario</label>
                        <input class="form-control" readonly id="nomeModal" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Escolaridade</label>
                        <input class="form-control" value="" id="escolaridade" readonly>
                    </div>
                    <div class="col-md-6">
                        <label>Curso T�cnico</label>
                        <input class="form-control" value="" id="cursoTecnico" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>CPF</label>
                        <input class="form-control cpf" readonly id="cpfModal" />
                    </div>
                    <div class="col-md-3">
                        <label>Fun��o</label>
                        <input class="form-control" data-toggle="tooltip" data-placement="bottom" readonly id="funcaoModal" />
                    </div>
                    <div class="col-md-3">
                        <label>Lota��o</label>
                        <input class="form-control" data-toggle="tooltip" data-placement="bottom" readonly id="lotacaoModal" />
                    </div>
                    <div class="col-md-3">
                        <label>Centro Resultado</label>
                        <input class="form-control" data-toggle="tooltip" data-placement="bottom" readonly id="centroResultadoModal" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

