<?php

$tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
$actionForm = "moduloOsm";

$acao = true;
$pag = "materialUtilizado";
$tipoOS = "osm";

$sql = "SELECT m.cod_material, m.estado, m.origem, m.utilizado, mat.nome_material, u.sigla_uni_medida, u.cod_uni_medida "
    . "FROM osm_material m "
    . "JOIN material mat USING (cod_material) "
    . "JOIN unidade_medida u ON u.cod_uni_medida = m.unidade "
    . "WHERE m.cod_osm = " . $_SESSION['refillOs']['codigoOs'];

$maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/materiaisUtilizados.php");
