<?php

$tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
$actionForm = "moduloOsm";

$os = $this->medoo->select("osm_falha", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$os = $os[0];

$os['cod_os'] = $os['cod_osm'];

$ss = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$os['cod_ssm']]);
$ss = $ss[0];

$ss['cod_ss'] = $ss['cod_ssm'];

$encerramento = $this->medoo->select("osm_encerramento",["[>]pendencia" => "cod_pendencia"], "*", ["cod_osm" => $os['cod_osm']]);
$encerramento = $encerramento[0];

$unEquipe = $this->medoo->select("un_equipe",[
    "[><]equipe" => "cod_equipe",
    "[><]unidade" => "cod_unidade"
], "*" ,[
        'cod_un_equipe' => $encerramento['cod_un_equipe']
    ]
);
$unEquipe = $unEquipe[0]; // para Transferencia

//Informa��es relacionadas ao m�dulo de Registro de Execu��o
$unidadeEquipe = $this->medoo->select("osm_registro",[
    "[><]un_equipe" => "cod_un_equipe",
    "[><]unidade"   => "cod_unidade",
    "[><]equipe"    => "cod_equipe"
],[
    "equipe.sigla",
    "unidade.nome_unidade"
],[
    "osm_registro.cod_osm" => $os['cod_osm']
]);
$unidadeEquipe = $unidadeEquipe[0]; // vem do Registro de Execu��o

$acao = true;
$pag = "encerramento";
$tipoOS = "osm";

$maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/encerramento.php");