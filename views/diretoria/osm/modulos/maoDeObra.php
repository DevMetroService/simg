<?php
$tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
$actionForm = "moduloOsm";

$acao = true;
$pag = "maoObra";
$tipoOS = "osm";

$disableMaoObra="";

$maoObraFuncionario = $this->medoo->select("osm_mao_de_obra", "*",["cod_osm" => (int) $_SESSION['refillOs']['codigoOs']]);

$funcionario;
$selectFuncionario = $this->medoo->select("funcionario", ["matricula", "nome_funcionario"]);
if ($selectFuncionario)
    foreach ($selectFuncionario as $dados)
        $funcionario[$dados['matricula']] = $dados['nome_funcionario'];

$maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
if(empty($tempoTotal)){
    $disableMaoObra = "disabled";
}
$regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/maoObra.php");