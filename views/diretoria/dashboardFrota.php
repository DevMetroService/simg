<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 14/08/2017
 * Time: 09:13
 */
?>

<div class="page-header">
    <h1>M�dulo de Engenharia</h1>
</div>

<?php
$this->printModuloNavigator();

$mes = date('n');
$ano = date('Y');

$sql = "SELECT * FROM calc_disponibilidade WHERE cod_linha = 1 AND mes = {$mes} AND ano = {$ano} ORDER BY dia";
$disponibilidadeOeste = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM calc_disponibilidade WHERE cod_linha = 2 AND mes = {$mes} AND ano = {$ano} ORDER BY dia";
$disponibilidadeCariri = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM calc_disponibilidade WHERE cod_linha = 5 AND mes = {$mes} AND ano = {$ano} ORDER BY dia";
$disponibilidadeSul = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM calc_disponibilidade WHERE cod_linha = 6 AND mes = {$mes} AND ano = {$ano} ORDER BY dia";
$disponibilidadePM = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

$sql = "SELECT * FROM calc_disponibilidade WHERE cod_linha = 7 AND mes = {$mes} AND ano = {$ano} ORDER BY dia";
$disponibilidadeSobral = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);




?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Dispon�bilidade de Circula��o em <?php echo MainController::$monthComplete[$mes] . " de " . $ano?></div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs -->
                    <ul id='dispTab' class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#sul" aria-controls="sul" role="tab" data-toggle="tab">Linha SUL</a></li>
                        <li role="presentation"><a href="#oeste" aria-controls="oeste" role="tab" data-toggle="tab">Linha OESTE</a></li>
                        <li role="presentation"><a href="#pm" aria-controls="pm" role="tab" data-toggle="tab">Linha PARANGABA-MUCURIPE</a></li>
                        <li role="presentation"><a href="#sobral" aria-controls="sobral" role="tab" data-toggle="tab">Linha SOBRAL</a></li>
                        <li role="presentation"><a href="#cariri" aria-controls="cariri" role="tab" data-toggle="tab">Linha CARIRI</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="sul">
                            <div class="row">
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="confSul" style="overflow-y: scroll; padding: 15px">
                                        <table id="tableRelSul" class="table table-bordered table-responsive" style="font-size: small">
                                            <thead>
                                            <tr>
                                                <th>Dia</th>
                                                <th>Dados de Disponibilidade</th>
                                                <th>Disp. Di�ria</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(!empty($disponibilidadeSul))
                                            {
                                                foreach($disponibilidadeSul as $dados=>$value){
                                                    echo ("<tr>");
                                                    echo("<td>{$value['dia']}</td>");
                                                    echo("<td>{$value['info']}</td>");
                                                    echo("<td>{$value['resultado']}</td>");
                                                    echo ("</tr>");
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="graficoSul"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <table id="mediaMensal" class="table table-bordered table-responsive" style="font-size: small; text-align: center">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Disponibilidade Mensal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td id="resultMensalSul" style="font-size: large"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="oeste">
                            <div class="row">
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="confOeste" style="overflow-y: scroll; padding: 15px">
                                        <table id="tableRelOeste" class="table table-bordered table-responsive" style="font-size: small">
                                            <thead>
                                            <tr>
                                                <th>Dia</th>
                                                <th>Dados de Disponibilidade</th>
                                                <th>Disp. Di�ria</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(!empty($disponibilidadeOeste))
                                            {
                                                foreach($disponibilidadeOeste as $dados=>$value){
                                                    echo ("<tr>");
                                                    echo("<td>{$value['dia']}</td>");
                                                    echo("<td>{$value['info']}</td>");
                                                    echo("<td>{$value['resultado']}</td>");
                                                    echo ("</tr>");
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="graficoOeste"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <table id="mediaMensal" class="table table-bordered table-responsive" style="font-size: small; text-align: center">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Disponibilidade Mensal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td id="resultMensalOeste" style="font-size: large"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="pm">
                            <div class="row">
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="confPM" style="overflow-y: scroll; padding: 15px">
                                        <table id="tableRelPM" class="table table-bordered table-responsive" style="font-size: small">
                                            <thead>
                                            <tr>
                                                <th>Dia</th>
                                                <th>Dados de Disponibilidade</th>
                                                <th>Disp. Di�ria</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(!empty($disponibilidadePM))
                                            {
                                                foreach($disponibilidadePM as $dados=>$value){
                                                    echo ("<tr>");
                                                    echo("<td>{$value['dia']}</td>");
                                                    echo("<td>{$value['info']}</td>");
                                                    echo("<td>{$value['resultado']}</td>");
                                                    echo ("</tr>");
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="graficoPM"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <table id="mediaMensal" class="table table-bordered table-responsive" style="font-size: small; text-align: center">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Disponibilidade Mensal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td id="resultMensalPM" style="font-size: large"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="sobral">
                            <div class="row">
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="confSobral" style="overflow-y: scroll; padding: 15px">
                                        <table id="tableRelSobral" class="table table-bordered table-responsive" style="font-size: small">
                                            <thead>
                                            <tr>
                                                <th>Dia</th>
                                                <th>Dados de Disponibilidade</th>
                                                <th>Disp. Di�ria</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(!empty($disponibilidadeSobral))
                                            {
                                                foreach($disponibilidadeSobral as $dados=>$value){
                                                    echo ("<tr>");
                                                    echo("<td>{$value['dia']}</td>");
                                                    echo("<td>{$value['info']}</td>");
                                                    echo("<td>{$value['resultado']}</td>");
                                                    echo ("</tr>");
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="graficoSobral"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <table id="mediaMensal" class="table table-bordered table-responsive" style="font-size: small; text-align: center">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Disponibilidade Mensal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td id="resultMensalSobral" style="font-size: large"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="cariri">
                            <div class="row">
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <div id="confCariri" style="overflow-y: scroll; padding: 15px">
                                        <table id="tableRelCariri" class="table table-bordered table-responsive" style="font-size: small">
                                            <thead>
                                            <tr>
                                                <th>Dia</th>
                                                <th>Dados de Disponibilidade</th>
                                                <th>Disp. Di�ria</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if(!empty($disponibilidadeCariri))
                                            {
                                                foreach($disponibilidadeCariri as $dados=>$value){
                                                    echo ("<tr>");
                                                    echo("<td>{$value['dia']}</td>");
                                                    echo("<td>{$value['info']}</td>");
                                                    echo("<td>{$value['resultado']}</td>");
                                                    echo ("</tr>");
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="graficoCariri"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <table id="mediaMensal" class="table table-bordered table-responsive" style="font-size: small; text-align: center">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Disponibilidade Mensal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td id="resultMensalCariri" style="font-size: large"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>SAF's Material Rodante Abertas</label></div>
            <div class="panel-body">
                <table id="indicadorSaf" class="table table-striped table-bordered no-footer">
                    <thead id="headIndicadorSaf">
                    <tr role="row">
                        <th>N�</th>
                        <th>Local</th>
                        <th>Grupo de Sistema</th>
                        <th>Avaria</th>
                        <th>Data de Abertura</th>
                        <th>N�vel</th>
                        <th>A��o</th>
                    </tr>
                    </thead>

                    <tbody id="bodyIndicadorSaf" class="bodySaf">
                    <?php
                    $saf = $this->medoo->select("v_saf", "*", ["AND" =>
                        [
                            "cod_status[!]" => [2, 14, 27],
                            "cod_grupo" => [22, 23, 26]
                        ]
                    ]);

                    if (!empty($saf)) {
                        foreach ($saf as $dados) {
                            echo('<tr>');
                            echo('<td>' . $dados['cod_saf'] . '</td>');
                            echo('<td><span class="primary-info">' . $dados['nome_linha'] . '</span><br /><span class="sub-info">' . $dados['descricao_trecho'] . '</span></td>');
                            echo('<td>' . $dados['nome_grupo'] . '</td>');
                            echo('<td>' . $dados['nome_avaria'] . '</td>');
                            echo('<td>' . $this->parse_timestamp($dados['data_abertura']) . '</td>');
                            echo('<td>' . $dados['nivel'] . '</td>');
                            echo('<td>');
                            echo('<button class="btn btn-default btn-circle btnEditarSaf" type="button" title="Ver Dados Gerais/Editar"><i class="fa fa-file-o fa-fw"></i></button> ');
                            echo('<button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button> ');
                            echo('</td>');
                            echo('</tr>');
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Estado da Frota</div>
            <div class="panel-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tue" aria-controls="tue" role="tab" data-toggle="tab">TUE</a></li>
                        <li role="presentation"><a href="#vlt" aria-controls="vlt" role="tab" data-toggle="tab">VLT</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tue">
                            <div class="col-md-12" style="margin-top: 15px;">
                                <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                    <thead>
                                    <tr role="row">
                                        <th>VEICULO</th>
                                        <th>LOCALIZA��O</th>
                                        <th>ESTADO</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $tblLocDisp = $this->medoo->select("veiculo",["[><]linha" => "cod_linha"], "*", ["cod_grupo" => 23 ]);

                                    if (!empty($tblLocDisp)) {
                                        foreach ($tblLocDisp as $dados) {

                                            switch($dados['disponibilidade']){
                                                case 'i':
                                                    $disp="Inoperante";
                                                    break;
                                                case 's':
                                                    $disp="Dispon�vel";
                                                    break;
                                                case 'n':
                                                    $disp="Imobilizado";
                                                    break;
                                                default:
                                                    $disp="Sem informa��o";
                                            }

                                            echo('<tr>');
                                            echo("<td>{$dados['nome_veiculo']}</td>");
                                            echo("<td>{$dados['nome_linha']}</td>");
                                            echo("<td>{$disp}</td>");
                                            echo('</tr>');
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="vlt">
                            <div class="col-md-12" style="margin-top: 15px;">
                                <table class="tableIS table table-striped table-bordered table-hover dataTable no-footer">
                                    <thead>
                                    <tr role="row">
                                        <th>VEICULO</th>
                                        <th>LOCALIZA��O</th>
                                        <th>ESTADO</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $tblLocDisp = $this->medoo->select("veiculo",["[><]linha" => "cod_linha"], "*", ["cod_grupo" => 22 ]);

                                    if (!empty($tblLocDisp)) {
                                        foreach ($tblLocDisp as $dados) {

                                            switch($dados['disponibilidade']){
                                                case 'i':
                                                    $disp="Inoperante";
                                                    break;
                                                case 's':
                                                    $disp="Dispon�vel";
                                                    break;
                                                case 'n':
                                                    $disp="Imobilizado";
                                                    break;
                                                default:
                                                    $disp="Sem informa��o";
                                            }

                                            echo('<tr>');
                                            echo("<td>{$dados['nome_veiculo']}</td>");
                                            echo("<td>{$dados['nome_linha']}</td>");
                                            echo("<td>{$disp}</td>");
                                            echo('</tr>');
                                        }
                                    }
                                    ?>
                                    </tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Relat�rios</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="list-group">
                            <a href="<?php echo HOME_URI; ?>dashboardGeral/relatoriosDiversos/MKBF"
                               class="list-group-item">
                                <h4 class="list-group-item-heading">MKBF</h4>
                                <p class="list-group-item-text">M�dia de Km entre Falhas</p>
                            </a>
                            <a href="<?php echo HOME_URI; ?>dashboardGeral/relatoriosDiversos/MKBFFrota"
                               class="list-group-item">
                                <h4 class="list-group-item-heading">MKBF - Frota</h4>
                                <p class="list-group-item-text">M�dia de Km da Frota</p>
                            </a>
                            <a href="<?php echo HOME_URI; ?>dashboardGeral/relatoriosDiversos/DisponibilidadeMrMensal"
                               class="list-group-item">
                                <h4 class="list-group-item-heading">Disponibilidade de Circula��o Mensal</h4>
                                <p class="list-group-item-text">Relat�rio em gr�fico de linha sobre a % de circula��o dos trens entre as pretendidas e a realizadas.</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

