<div class="page-header">
    <h1>M�dulo de Almoxarifado</h1>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 09/08/2017
 * Time: 11:44
 */
$this->printModuloNavigator();
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tabela de Materiais, M�quinas e Equipamentos</div>
            <div class="panel-body">
                <table id="indicadorTabelaMaterial" class="table table-striped table-bordered no-footer">
                    <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Material</th>
                        <th>Descri��o</th>
                        <th>Categoria</th>
                        <th>Unidade</th>
                        <th>Estoque M�nimo</th>
                        <th>Estoque Atual</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $material = $this->medoo->select("v_material", "*",[
                        "cod_categoria" => [10,32] , "ORDER" => "nome_material"]);


                    foreach($material as $dados => $value){

//                        $minimo = (int)$value['quant_min'];
                        $minimo = rand(1,5);
                        $quantidade = (int)$value['quantidade'];

                        echo("<tr>");
                        echo("<td>{$value['cod_material']}</td>");
                        echo("<td>{$value['nome_material']}</td>");
                        echo("<td>{$value['descricao_material']}</td>");
                        echo("<td>{$value['nome_categoria']}</td>");
                        echo("<td>{$value['sigla_uni_medida']}</td>");
                        echo("<td>{$minimo}</td>");
                        if(!empty($value['quantidade']))
                            echo("<td>{$quantidade}</td>");
                        else
                            echo("<td>0</td>");
                        echo("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tabela de Fornecedores</div>
            <div class="panel-body">
                <table id="indicadorTabelaFornecedor" class="table table-striped table-bordered no-footer">
                    <thead>
                    <tr>
                        <th>Fornecedor</th>
                        <th>Raz�o Social</th>
                        <th>CNPJ</th>
                        <th>Avalia��o</th>
                        <th>Descri��o</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $fornecedor = $this->medoo->select("fornecedor", "*");


                    foreach($fornecedor as $dados => $value){

                        echo("<tr>");
                        echo("<td>{$value['nome_fantasia']}</td>");
                        echo("<td>{$value['razao_social']}</td>");
                        echo("<td>{$value['cnpjcpf']}</td>");
                        echo("<td>{$value['avaliacao']}</td>");
                        echo("<td>{$value['descricao']}</td>");
                        echo("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Relat�rios</label></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="list-group">
                            <a id="materialUtilizadoRelatorio" href="<?php echo HOME_URI; ?>/views\_images\almoxarifado\AlmoxarifadoMetrofor.xlsx" class="list-group-item">
                                <h4 class="list-group-item-heading">Lista de Materiais MetroFor</h4>
                                <p class="list-group-item-text">Link de download da listagem dos materiais do MetroFor em Excel.</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a id="materialUtilizadoRelatorio" href="<?php echo HOME_URI; ?>/dashboardGeral/relatorio/mu" class="list-group-item">
                                <h4 class="list-group-item-heading">Relat�rio de Materiais Utilizados</h4>
                                <p class="list-group-item-text">Lista de quantidade de materiais utilizados nas ordem de servi�o.</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a id="maquinaUtilizadoRelatorio" href="<?php echo HOME_URI; ?>/dashboardGeral/relatorio/meu" class="list-group-item">
                                <h4 class="list-group-item-heading">M�quinas e Equipamentos Utilizados</h4>
                                <p class="list-group-item-text">Lista de quantidade de m�quinas e equipamentos utilizados nas ordem de servi�o.</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
