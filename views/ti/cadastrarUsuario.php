<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 15/04/2016
 * Time: 11:07
 */

require_once ( ABSPATH . '/functions/btnForm.php');

if($_SESSION['refillUser']){
    $dadosUser = $this->medoo->select("usuario","*",["cod_usuario" => (int)$_SESSION['refillUser']['codigoUsuario'] ] );
    $dadosUser = $dadosUser[0];

    $equipeUser = $this->medoo->select("equipe","*",["cod_usuario" => $dadosUser['cod_usuario']]);
    $equipeUser = $equipeUser[0];

}

$btnsAcao .= btnCriarUser($dadosUser);
$btnsAcao .= btnDesativarUsuario($dadosUser);

require_once(ABSPATH . "/views/_includes/formularios/administrador/user.php");

unset ($_SESSION['refillUser']);
