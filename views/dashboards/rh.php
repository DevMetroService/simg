<?php

require_once(ABSPATH . "/views/layout/topLayout.php");

$this->dashboard->modalUserForm($this->dadosUsuario);

$active = "active";
$tabs = "";
$tabsContent = "";

foreach($tipo_funcionario as $dados => $value)
{
    $id = str_replace(' ', '', $value['descricao']);
    $tabs .=  <<<HTML
        <li role="presentation" class="{$active}"><a href="#{$id}" aria-controls="{$id}" role="tab" data-toggle="tab">{$value['descricao']}</a></li>
    HTML;

    $listFuncionario = $modelFuncionario->getFuncionarioByTipoEmpresa($value['cod_tipo_funcionario']);
    
    $tableRow = "";

    foreach($listFuncionario as $data=>$val)
    {   
        $tableRow .= <<<HTML
            <tr>
                <td>{$val['matricula']}</td>
                <td>{$val['nome_funcionario']}</td>
                <td>{$val['cargo']}</td>
                <td>{$val['centro_resultado']}</td>
                <td>
                    <a href='{$this->home_uri}/funcionario/edit/{$val['cod_funcionario']}'>
                        <button class='btn btn-primary btn-circle' type='button' title='Editar'><i class='fa fa-file-o fa-fw'></i></button>
                    </a> 
                </td>
            </tr>
        HTML;
    }

    $tabsContent .= <<<HTML
        <div role="tabpanel" class="tab-pane {$active}" id="{$id}">                                  
            <table class="table table-striped table-bordered table-hover dataTable no-footer indicadorFuncionario">
                <thead>
                    <tr>
                        <th>Matricula</th>
                        <th>Nome Funcion�rio</th>
                        <th>Cargo</th>
                        <th>Centro de Resultado</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>
                    {$tableRow}
                </tbody>
            </table>
        </div>
    HTML;

    $active = "";
}

echo <<<HTML

<div class="">
    <h1 class="page-header">DASHBOARD</h1>
</div>

<div class="row" >
    <div class="col-md-12">
        <div class='panel panel-default'>
            <div class='panel-heading'><label>FUNCION�RIOS CADASTRADOS</label></div>
            <div class='panel-body'>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    {$tabs}
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    {$tabsContent}
                </div>
            </div>
        </div>        
    </div>
</div>
HTML;


require_once(ABSPATH . "/views/layout/bottonLayout.php");


?>