<?php

require_once(ABSPATH . "/views/layout/topLayout.php");

$this->dashboard->modalUserForm($this->dadosUsuario);

foreach($listaAtivos as $data=>$val)
{   
    $tableRowActive .= <<<HTML
        <tr>
            <td>{$val['nome_material']} - {$val['descricao_material']}</td>
            <td>{$val['nome_uni_medida']}</td>
            <td>{$val['valor_unitario']}</td>
            <td>{$val['nome_categoria']}</td>
            <td>{$val['nome_tipo_material']}</td>
            <td>
                <a href="{$this->home_uri}/material/edit/{$val['cod_material']}" >
                  <button type="button" class="btn btn-default btn-circle" title="Editar">
                    <i class="fa fa-file-o fa-fw"></i>
                  </button>
                </a>
            </td>
        </tr>
    HTML;
}
foreach($listaInativos as $data=>$val)
{   
    $tableRowDisable .= <<<HTML
        <tr>
            <td>{$val['nome_material']} - {$val['descricao_material']}</td>
            <td>{$val['nome_uni_medida']}</td>
            <td>{$val['valor_unitario']}</td>
            <td>{$val['nome_categoria']}</td>
            <td>{$val['nome_tipo_material']}</td>
            <td>
                <a href="{$this->home_uri}/material/edit/{$val['cod_material']}" >
                  <button type="button" class="btn btn-default btn-circle" title="Editar">
                    <i class="fa fa-file-o fa-fw"></i>
                  </button>
                </a>
            </td>
        </tr>
    HTML;
}

echo <<<HTML

<div class="page-header">
    <h1>Sistema Gerenciador de Materiais</h1>
</div>

<div class="row" >
    <div class="col-md-12">
        <div class='panel panel-default'>
            <div class='panel-heading'><label>LISTA DE MATERIAIS</label></div>
            <div class='panel-body'>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#ativos" data-test ="tbAtivos" aria-controls="ativos" role="tab" data-toggle="tab">ATIVADOS</a></li>
                  <li role="presentation" class=""><a href="#inativos" data-test ="tbInativos" aria-controls="inativos" role="tab" data-toggle="tab">DESATIVADOS</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="ativos"> 
                    <table class="table table-striped table-bordered table-hover no-footer tbMaterial" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Material</th>
                            <th>Unidade de Medida</th>
                            <th>Valor Unit�rio</th>
                            <th>Categoria</th>
                            <th>Tipo de Material</th>
                            <th>A��es</th>
                          </tr>
                        </thead>
                        <tbody>
                            {$tableRowActive}
                        </tbody>
                    </table>
                  </div>    
                  <div role="tabpanel" class="tab-pane" id="inativos">                                  
                    <table class="table table-striped table-bordered table-hover no-footer tbMaterial" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Material</th>
                            <th>Unidade de Medida</th>
                            <th>Valor Unit�rio</th>
                            <th>Categoria</th>
                            <th>Tipo de Material</th>
                            <th>A��es</th>
                          </tr>
                        </thead>
                        <tbody>
                            {$tableRowDisable}
                        </tbody>
                    </table>
                  </div>  
                </div>
            </div>
        </div>        
    </div>
</div>
HTML;


require_once(ABSPATH . "/views/layout/bottonLayout.php");
?>