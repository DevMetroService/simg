<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 17/09/2020
 * Time: 13:16
 */

$this->dashboard->modalUserForm($this->dadosUsuario);

$arr['ssmAguardandoAprovacao'] = $this->medoo->select("v_ssm", "*", ['cod_status' => 41]);

$arr['ssmAlteracaoNivel'] = $this->medoo->select("v_solicitacao_alter_nivel", "*", ['cod_status' => 44]);

$ctdSsmPendenteMaterial = !empty($arr['ssmAguardandoAprovacao']) ? count($arr['ssmAguardandoAprovacao']) : 0;
$ctdSsmAguardandoMaterial = !empty($ssmAguardandoMaterial) ? count($ssmAguardandoMaterial) : 0;
$ctdSsmAlteracaoNivel = !empty($arr['ssmAlteracaoNivel']) ? count($arr['ssmAlteracaoNivel']) : 0;

$validacao = false;
if(in_array($this->dadosUsuario['cod_usuario'], $this->dashboard->usuarioValidacao)){
    $validacao = true;
}

$sql = "SELECT * FROM v_ssm WHERE cod_status = 35";

$ssmAguardando['edificacao'] = $this->medoo->query($sql . " AND cod_grupo = 21")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['viaPermanente'] = $this->medoo->query($sql . " AND cod_grupo = 24")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['subestacao'] = $this->medoo->query($sql . " AND cod_grupo = 25")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['energia'] = $this->medoo->query($sql . " AND cod_grupo = 29")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['redeAerea'] = $this->medoo->query($sql . " AND cod_grupo = 20")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['bilhetagem'] = $this->medoo->query($sql . " AND cod_grupo = 28")->fetchAll(PDO::FETCH_ASSOC);
$ssmAguardando['telecom'] = $this->medoo->query($sql . " AND cod_grupo = 27")->fetchAll(PDO::FETCH_ASSOC);

$somaValidacao = 0;
foreach($ssmAguardando as $dados){
    $somaValidacao += count($dados);
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">GESIV</h1>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center" style="font-size: 150%">
                                <i class="fa fa-clock-o"></i> Materiais Pendentes
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-footer" style="padding: 0px !important;">
                    <div class="row">
                        <div class="col-xs-6" style="padding-right: 0px !important;" data-toggle="modal" data-target="#materialAguardandoLiberacao">
                            <button class="btn btn-lg btn-default btn-block" type="button" style="padding: 10px 10px !important;">
                            <div class="text-center" style="white-space: normal !important;">Aguardando Libera��o</div>
                                <div class="text-center" style="font-size: 150%"><strong id="ctdAguardandoCompra"><?= $ctdSsmPendenteMaterial ?></strong></div>
                            </button>
                        </div>

                        <div class="col-xs-6" style="padding-left: 0px !important;" data-toggle="modal" data-target="#ssmAguardandoMaterial">
                            <button class="btn btn-lg btn-default btn-block" style="padding: 10px 10px !important;">
                                <div class="text-center" style="white-space: normal !important;">Aguardando Material</div>
                                <div class="text-center" style="font-size: 150%"><strong id="ctdAguardandoMaterial"><?= $ctdSsmAguardandoMaterial ?></strong></div>
                            </button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center" style="font-size: 150%">
                                <i class="fa fa-check"></i> Aguardando Aprova��o de N�vel
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="padding: 0px !important;">
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-lg btn-default btn-block" type="button" style="padding: 10px 10px !important;" data-toggle="modal" data-target="#ssmAlteracaoNivel">
                            <div class="text-center">SSM</div>
                            <div class="text-center" style="font-size: 150%"><strong id="ctdAlterNivel"><?= $ctdSsmAlteracaoNivel ?></strong></div>
                            </button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center" style="font-size: 150%">
                                <i class="fa fa-list-alt"></i> Aguardando Valida��o
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="padding: 0px !important;">
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-lg btn-default btn-block" style="padding: 10px 10px !important;" data-toggle="modal" data-target=".aguardandoValidacao">
                                <div class="text-center">SSM</div>
                                <div class="text-center" style="font-size: 150%"><strong><?= $somaValidacao ?></strong></div>
                            </button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->

<!--//Pendencia MATERIAL N�O CONTRATUAL-->
<div class="modal fade" id="materialAguardandoLiberacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Aguardando Aprova��o</h4>
            </div>
            <div class="modal-body">
                <table id="tbMaterialWaitAprove" class="table table-bordered table-responsive table-stripped">
                    <thead>
                    <tr>
                        <th>SSM</th>
                        <th>Data de Solicita��o</th>
                        <th>Quantidade de Material</th>
                        <th>Valor Total</th>
                        <th>A��es</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if(is_array($arr) && !empty($arr['ssmAguardandoAprovacao']))
                    {
                      foreach ($arr['ssmAguardandoAprovacao'] as $dados => $value)
                      {

                        $materiais = $this->medoo->query
                        ("
                            SELECT max(os.cod_osm), ss.cod_ssm, ss.data_status, valor_unitario, nome_material, quantidade, material.descricao
                            FROM osm_material_solicitado
                            JOIN osm_falha os USING(cod_osm)
                            JOIN material USING(cod_material)
                            JOIN v_ssm ss USING(cod_ssm)
                            WHERE cod_ssm = {$value['cod_ssm']}
                            GROUP BY os.cod_osm, ss.cod_ssm, ss.data_status, valor_unitario, nome_material, quantidade, material.descricao
                        ")->fetchAll(PDO::FETCH_ASSOC);

                        $qtdMateriais = !empty($materiais) ?  count($materiais): 0;

                        $contentPopover = "";

                        $valorTotal = 0;

                        foreach($materiais as $line => $name)
                        {
                          $nomeMaterial = htmlspecialchars("{$name['nome_material']} - {$name['descricao']}");
                            $contentPopover .= "<tr>
                            <td>
                              $nomeMaterial
                            </td>
                            <td>
                              {$name['quantidade']}
                            </td>
                            <td>
                              {$name['valor_unitario']}
                            </td>
                            </tr>";
                            $valorTotal += (float)$name['quantidade'] * (float)$name['valor_unitario'];
                        }

                        $table = "
                        <table class='table-bordered table-stripped'>
                          <thead class='popover-table'>
                            <tr>
                              <th>
                                Material
                              </th>
                              <th>
                              Qtd
                              </th>
                              <th>
                                Valor Unit�rio
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {$contentPopover}
                          </tbody>
                        </table>";

                        $valorTotal = number_format($valorTotal, 2, ',', '.');

                        echo <<<HTML

                              <tr>
                                  <td>{$value['cod_ssm']}</td>
                                  <td>{$this->parse_timestamp($value['data_status'])}</td>
                                  <td class='text-center'>
                                      <button tabindex='0' class='btn btn-block btn-default clickPop' role='button'
                                      data-toggle='popover'
                                      data-placement='top'
                                      data-trigger='focus'
                                      data-html='true'
                                      title='Materiais'
                                      data-content="{$table}">
                                          $qtdMateriais
                                      </button>
                                  </td>
                                  <td>R$ {$valorTotal}</td>
                                  <td>
                                    <button id="btnAprovaCompra" data-time="{$this->parse_timestamp($value['data_status'])}" data-ssm ="{$value['cod_ssm']}" type="button" class="btn btn-circle btn-success">
                                        <i class='fa fa-thumbs-up'></i>
                                    </button>
                                    <button type="button" class="btn btn-circle btn-danger repBtnCompra" data-ssm ="{$value['cod_ssm']}" data-toggle="modal" data-target="#justificativaNegada" data-dismiss="modal">
                                      <i class='fa fa-thumbs-down'></i>
                                    </button>
                                  </td>
                              </tr>
  HTML;
                      }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!--//Justificativa Compra Negada-->
<div class="modal fade" id="justificativaNegada" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" data-toggle="modal" data-target="#materialAguardandoLiberacao" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Justificativa</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <label>Justificativa da a��o</label>
                    <textarea id="taJustificativaCompraNegada" name="descricao" class="form-control"></textarea>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnCompraNegada" type="button" class="btn btn-danger" data-ssm data-toggle="modal" data-target="#materialAguardandoLiberacao" data-dismiss="modal">
                  Salvar
                </button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#materialAguardandoLiberacao" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!--Pend�ncia AGUARDANDO MATERIAL-->
<div class="modal fade" id="ssmAguardandoMaterial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Aguardando Material</h4>
            </div>
            <div class="modal-body">
                <table id="tbWaitMaterial" class="table table-bordered table-responsive table-stripped">
                    <thead>
                    <tr>
                        <th>SSM</th>
                        <th>Data de Aprova��o</th>
                        <th>Materiais</th>
                        <th>Dias em Aguardo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($ssmAguardandoMaterial as $dados => $value)
                    {

                        $materiais = $this->medoo->query
                        ("
                            SELECT max(cod_osm), valor_unitario, nome_material, quantidade, material.descricao
                            FROM osm_material_solicitado
                            JOIN osm_falha USING(cod_osm)
                            JOIN material USING(cod_material)
                            WHERE cod_ssm = {$value['cod_ssm']}
                            GROUP BY cod_osm, valor_unitario, nome_material, quantidade, material.descricao
                        ")->fetchAll(PDO::FETCH_ASSOC);


                        $qtdMateriais = !empty($materiais) ?  count($materiais): 0;
                        $contentPopover = "";

                        foreach($materiais as $line => $name)
                        {
                            $contentPopover .= "-> ". htmlspecialchars("{$name['nome_material']}-{$name['descricao']}</br>");
                        }

                        echo
                          <<<HTML
                            <tr>
                                <td>{$value['cod_ssm']}</td>
                                <td>{$this->parse_timestamp($value['data_status'])}</td>
                                <td class='text-center'>
                                    <button tabindex='0' class='btn btn-block btn-default clickPop' role='button'
                                    data-toggle='popover'
                                    data-placement='top'
                                    data-trigger='focus'
                                    data-html="true"
                                    title='Materiais'
                                    data-content="{$contentPopover}">
                                        {$qtdMateriais}
                                    </button>
                                </td>
                                <td>{$value['dias_aberto']}</td>
                            </tr>
HTML;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Altera��o de N�vel -->
<div class="modal fade" id="ssmAlteracaoNivel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Altera��o de N�vel</h4>
            </div>
            <div class="modal-body">
                <table id="tbAlterLevelSsm" class="table table-bordered table-responsive table-stripped">

                <thead>
                    <tr>
                        <th>SSM</th>
                        <th>Data de Solicita��o</th>
                        <th>Altera��es de N�vel</th>
                        <th>Solicitante</th>
                        <th>Motivo</th>
                        <th>A��o</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      if(!empty($arr['ssmAlteracaoNivel']))
                      {

                        foreach($arr['ssmAlteracaoNivel'] as $dados=>$value)
                        {
                            echo <<<HTML
                            <tr>
                              <td>
                                {$value['cod_ssm']}
                              </td>
                              <td>
                                {$value['data_status']}
                              </td>
                              <td>
                                DE:<strong>{$value['nivel_antigo']}</strong></br>
                                PARA:<strong>{$value['nivel_novo']}</strong>
                              </td>
                              <td>
                                {$value['usuario']}
                              </td>
                              <td>
                                {$value['observacao']}
                              </td>
                              <td>
                                  <button data-value="{$value['cod_reg_alter_nivel_ssm']}" type="button" class="btnAproveChangeLevel btn btn-success btn-circle"><i class="fa fa-thumbs-up fa-fw"></i></button>
                                  <button data-value="{$value['cod_reg_alter_nivel_ssm']}" type="button" class="btnReproveChangeLevel btn btn-danger btn-circle"><i class="fa fa-thumbs-down fa-fw"></i></button>
                              </td>
                            </tr>
  HTML;
                        }
                      }
                      ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Servi�os Aguardando Valida��o -->
<div class='modal fade' id='modal-validador' tabindex='-1' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close -align-right' data-dismiss='modal' aria-label='Close'>
                    <i aria-hidden='true' class='fa fa-close'></i>
                </button>
                <h4 class='modal-title' id='tituloValidacao'>Dados do Servi�o(SSM) N� <span id='numServicoValidacao'></span> </h4>
            </div>
            <div class='modal-body'>
                <form id='formModalSsm' class='form-group' method='post' action='". HOME_URI ."cadastroGeral/aprovarSsm'>
                    <div class='col-md-12'>
                        <div class='panel panel-primary'>
                            <div class='panel-heading' style='background-color: #337ab7 !important;'>
                                <label>Resumo das atua��es</label>
                            </div>
                            <div class='panel-body'>
                                <div id='infosModal'></div>
                            </div>
                        </div>
                        <div class='panel panel-primary'>
                            <div class='panel-heading' style='background-color: #337ab7 !important;'>
                                <label>Observa��es</label>
                            </div>
                            <div class='panel-body'>
                                <label id='observacoes'>Registre as observa��es</label>
                                <div>
                                    <textarea id='observacaoModal' class='form-control' type='text' value='observacao' placeholder='Observa��es'></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-success' id='btnFinalizarValidacao'>
                    <i class='fa fa-check'></i><span id='spanValidar'>Validar</span>
                </button>
                <button type='button' class='btn btn-danger' id='btnCancelaValidacao' data-dismiss="modal">
                    <i class='fa fa-close'></i> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade aguardandoValidacao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><label>Servi�os Aguardando Valida��o</label>
                    <button type="button" class="close -align-right" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="fa fa-close"></i></button>
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Servi�os</div>
                            <div class="panel-body">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#ed" aria-controls="ed" role="tab" data-toggle="tab">Edifica��es</a></li>
                                        <li role="presentation"><a href="#su" aria-controls="su" role="tab" data-toggle="tab">Subesta��o</a></li>
                                        <li role="presentation"><a href="#vp" aria-controls="vp" role="tab" data-toggle="tab">Via Permanente</a></li>
                                        <li role="presentation"><a href="#ra" aria-controls="ra" role="tab" data-toggle="tab">Rede A�rea</a></li>
                                        <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">Energia</a></li>
                                        <li role="presentation"><a href="#bi" aria-controls="bi" role="tab" data-toggle="tab">Bilhetagem</a></li>
                                        <li role="presentation"><a href="#te" aria-controls="te" role="tab" data-toggle="tab">Telecom</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="ed">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoEd" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['edificacao'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="su">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoSu" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['subestacao'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="en">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoEn" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['energia'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="vp">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoVp" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['viaPermanente'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="te">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoTl" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['telecom'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="bi">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoBi" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['bilhetagem'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="ra">
                                            <div class="col-md-12" style="margin-top: 15px;">
                                                <table id="tbValidacaoRa" class="table table-striped table-bordered table-hover dataTable no-footer">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>SSM</th>
                                                        <th>SAF</th>
                                                        <th>Grupo de Sistemas</th>
                                                        <th>Local</th>
                                                        <th>Data de Abertura</th>
                                                        <th>Avaria</th>
                                                        <th>Servi�o</th>
                                                        <?php if($validacao) {?><th>A��o</th><?php } ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $home_uri = HOME_URI;
                                                    foreach ($ssmAguardando['redeAerea'] as $dados => $value) {
                                                        $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                                                        echo "<tr>
                                                        <td>{$value['cod_ssm']}</td>
                                                        <td>{$value['cod_saf']}</td>
                                                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                                                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                                                        <td>{$dataAbertura}</td>
                                                        <td>{$value['nome_avaria']}</td>";
                                                        if (!empty($value['nome_veiculo'])) {
                                                            echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                                                        } else {
                                                            echo "<td>{$value['nome_servico']}</td>";
                                                        }

                                                        if($validacao){
                                                            echo "<td>
                                                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                                                    <i class='fa fa-eye fa-fw'></i>
                                                                </button>
                                                                </a>
                                                            <br>

                                                            <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' 
                                                            data-status='16' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-thumbs-up'></i>
                                                            </button>

                                                            <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' 
                                                            data-status='36' data-ssm='{$value['cod_ssm']}' data-dismiss='modal'>
                                                                <i class='fa fa-close'></i>
                                                            </button>
                                                            </td>";
                                                        }
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>