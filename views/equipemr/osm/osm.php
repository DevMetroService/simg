<?php
require_once ( ABSPATH . '/functions/btnForm.php');

$tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
$actionForm = "moduloOsm";

$acao = true;
$pag = "dadosGerais";
$tipoOS = "osm";

$os = $this->medoo->select("osm_falha",["[>]osm_servico" => "cod_osm","[>]material_rodante_osm" => "cod_osm"], "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$os = $os[0];

$os['cod_os'] = $os['cod_osm'];

$responsavel = $this->medoo->select("usuario", "usuario" ,["cod_usuario" => $os['usuario_responsavel']]);
$responsavel = $responsavel[0];

$encerramento = $this->medoo->select("osm_encerramento", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$encerramento = $encerramento[0];

$tempo = $os['data_abertura'];

$ss = $this->medoo->select("v_ssm", "*",["cod_ssm" => (int) $os['cod_ssm']]);
$ss = $ss[0];

$ss['cod_ss'] = $os['cod_ssm'];

$saf = $this->medoo->select("v_saf", "*",["cod_saf" => (int) $ss['cod_saf']]);
$saf = $saf[0];

$selectDescStatus = $this->medoo->select("status_osm", ["[><]status" => "cod_status"], "*", ["cod_ostatus" => $os['cod_ostatus'] ]);
$selectDescStatus = $selectDescStatus[0];

//Recebe o status atual para futuras avalias
$_SESSION['dadosCheck']['statusOs'] = $selectDescStatus['cod_status'];
if($_SESSION['dadosCheck']['codigoOs'] != $_SESSION['refillOs']['codigoOs']){
    $_SESSION['dadosCheck']['codigoOs'] = $_SESSION['refillOs']['codigoOs'];

    $_SESSION['dadosCheck']['maquinaCheck'] = false;
    $_SESSION['dadosCheck']['materialCheck'] = false;
}

//Verifica se a OS est� em execu��o. Quando estiver encerrada n�o h� a necessidade de travar.
if($selectDescStatus['cod_status'] == 10){
    $_SESSION['dadosCheck']['maquina'] = $this->medoo->select("osm_maquina", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
    $_SESSION['dadosCheck']['material'] = $this->medoo->select("osm_material", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
}


$_SESSION['bloqueio'] = $this->bloquearEdicao($selectDescStatus['data_status'],$selectDescStatus['nome_status']);

$maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs'] ]);
$tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);
$regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$_SESSION['refillOs']['codigoOs']]);

require_once(ABSPATH . "/views/_includes/formularios/os/dadosGerais.php");

modalDevolverCancelar($os, "Osm");