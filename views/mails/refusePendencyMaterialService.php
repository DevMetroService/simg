<?php
$justificativa = strtoupper($dados['descricao_status']);
$subject = "Nova Pend�ncia de Materiais";
// $message = file_get_contents(ABSPATH."/aux.html");
$message = <<<HTML
<h3>Atendimento de Solicita��o Realizada</h3>

<p>SSM de n�mero <strong>{$dados['cod_ssm']}</strong> se encontra com o status de <strong>{$dados['nome_status']}</strong>
entrando na condi��o de <strong>{$dados['condicao']}</strong> a respeito da solicita��o realizada atrav�s da OSM <strong>{$dados['cod_osm']}</strong>.</p>

<ul>
Motivo da <strong>N�o Aprova��o:</strong>
<li>{$justificativa}</li>
</ul>

<p>
  A SSM foi retornada a equipe com o tempo de atendimento sendo contabilizado novamente.
</p>
HTML;
 ?>
