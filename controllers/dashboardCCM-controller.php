<?php

class DashBoardCCMController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "2";

    public function index()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "CCM";

        $this->canalNotificacao ="ccm";

        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "dashboardCCM.js";
        $this->paginaDashboard = "corretiva";

        $contador = $this->medoo->select("v_saf", "*",["nome_status" => "Aberta"]);
        $contador = count($contador);
        $this->quantidadeSAF = $contador;

        $contador = $this->medoo->select("v_ssm", "*",["cod_status" => (int)9]); // Aberta
        $contador = count($contador);
        $this->quantidadeSSM = $contador;

        $arryOSM = $this->medoo->select("osm_falha",["[><]status_osm" =>"cod_ostatus"], "*",["cod_status" => 10]); // Encerrada
        $quantOSM = count($arryOSM);
        $this->quantidadeOSM = $quantOSM;

        $quantOSMfechamento = $this->contarOSMTempoLimite($arryOSM);
        $this->quantidadeOSMfechamento = $quantOSMfechamento;

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH 	. "/views/ccm/dashboard.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function corretiva(){
        $this->index();
    }

    public function programacao(){
        $this->controleAcesso();

        $this->canalNotificacao ="ccm";

        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "dashboardPreventiva.js";
        $this->paginaDashboard = "programacao";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/dashboardPreventiva.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function dashboardPreventiva(){
        $this->programacao();
    }

    public function preventiva(){
        $this->controleAcesso();

        $this->canalNotificacao ="ccm";

        $this->titulo = "Centro de Controle da Manutenção - Metro Service";
        $this->script = "dashboardCcmPmp.js";
        $this->paginaDashboard = "preventiva";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/dashboardPmp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function dashboardPmp(){
        $this->preventiva();
    }
}
