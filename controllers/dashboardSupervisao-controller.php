<?php

/**
 * Created by PhpStorm.
 * User: Valmar
 * Date: 06/03/2015
 * Time: 08:58
 */
class DashboardSupervisaoController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "2.1";

    public function index()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Supervisao";

        $this->pagina = "dashboard";

        $this->titulo = "Sistema de Manutenção - Metro Service / METROFOR";
        $this->script = "dashboardSupervisao.js";
        $this->paginaDashboard = "corretiva";

        $contador = $this->medoo->select("v_saf", "*",["nome_status" => "Aberta"]);
        $contador = count($contador);
        $this->quantidadeSAF = $contador;

        $contador = $this->medoo->select("v_ssm", "*",["cod_status" => (int)9]);
        $contador = count($contador);
        $this->quantidadeSSM = $contador;

        $contador = $this->medoo->select("osm_falha",["[><]status_osm" =>"cod_ostatus"], "*",["cod_status" => 10]);
        $contador = count($contador);
        $this->quantidadeOSM = $contador;

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH . "/views/supervisao/dashboard.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function corretiva(){
        $this->index();
    }
    
    public function programacao()
    {
        $this->controleAcesso();

        $this->pagina = "dashboard";

        $this->titulo = "Sistema de Manutenção - Metro Service / METROFOR";
        $this->script = "dashboardSupervisao.js";
        $this->paginaDashboard = "programacao";
        
        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH . "/views/supervisao/dashboardPreventiva.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function dashboardPreventiva(){
        $this->programacao();
    }

    public function preventiva(){
        $this->controleAcesso();

        $this->canalNotificacao ="ccm";

        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "dashboardCcmPmp.js";
        $this->paginaDashboard = "preventiva";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/supervisao/dashboardPmp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function dashboardPmp(){
        $this->preventiva();
    }

    public function executarPesquisaSsp()
    {
        $_SESSION['dadosPesquisaSsm'] = $_POST;

        $executar = $this->carregaModelo("ccm/ssm/pesquisaSsm-model");

        header("Location:".HOME_URI."dashboardSupervisao/pesquisaSsm");

        echo(var_dump($_SESSION['dadosPesquisaSsm']));
    }

    public function exibirPdf(){
        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $_SESSION['nomeArquivoPdf'] = $_POST;
        }

        require_once(ABSPATH . "/views/supervisao/exibirPdf.php");
    }

    public function pmp(){
        $_SESSION['filtroPmpTabela'] = $_POST;

        $this->controleAcesso();

        $this->titulo = "Ordem de Serviço de Manuntenção / SSM- Metro Service";
        $this->script = "detalhamentoPmp.js";

        require_once ( ABSPATH . "/views/_includes/_header.php");
        require_once ( ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH . "/views/_includes/_body.php");
        require_once ( ABSPATH . "/views/supervisao/pmp.php");
        require_once ( ABSPATH . "/views/_includes/_footer.php");
    }

    public function filtroPmp(){
        $_SESSION['filtroDetalhePmpTabela'] = array("filtro" => utf8_decode( $_POST['filtro']), "where" => $_POST['where']) ;
    }

    public function execucaoPmp(){
        $selectOsp = $this->medoo->query("SELECT cod_ssp, data_programada, dias_servico FROM ssp WHERE cod_ssp ={$_POST['codigoSsp']}")->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($selectOsp);
    }

}
