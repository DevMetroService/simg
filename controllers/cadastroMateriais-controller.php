<?php

class CadastroMateriaisController extends MainController
{
    public $loginNecessario = true;


    public $nivelNecessario = "4";

    public function index()
    {

        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/cadastro/material.php");                 # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");                           # Carrega o footer
    }

    public function adicionarMaterial(){
        $_SESSION['dadosMaterial'] = $_POST;

        $this->controleAcesso();

        $model = $this->carregaModelo('Materiais/cadastroMateriais-model');

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/dashboard.php");                         # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");

        unset($_POST);
    }

    public function adicionarFornecedor(){
        $_SESSION['dadosFornecedor'] = $_POST;

        $this->controleAcesso();

        //echo (var_dump($_SESSION['dadosFornecedor']));
        $model = $this->carregaModelo('Materiais/cadastroFornecedor-model');

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/dashboard.php");                         # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function entradaMaterial(){
        $_SESSION['entradaMaterial'] = $_POST;

        $this->controleAcesso();

        $model = $this->carregaModelo('Materiais/entradaEstoque-model');

        /*$parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/dashboard.php");                         # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");*/
    }

    public function cadastroMarcaModal()
    {
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_SESSION['cadastroMarca'] = $_POST;
        }

        $model = $this->carregaModelo('Materiais/cadastroMarca-model');

        echo('Cadastro realizado com sucesso.');

    }

    public function cadastroCategoriaModal()
    {
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_SESSION['cadastroCategoria'] = $_POST;
        }

        $model = $this->carregaModelo('Materiais/cadastroCategoria-model');

        echo('sucesso');

    }

    public function requisicaoMateriais(){
        $this->controleAcesso();

        $this->script = "scriptRequisicaoMateriais.js";

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/cadastro/requisicaoMaterial.php");                 # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pedidoCompras(){
        $this->controleAcesso();

        $this->script = "scriptPedidoCompra.js";

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/cadastro/pedidoCompra.php");                 # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cotacaoMateriais(){
        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->navegador = "sidebarMateriais.php";                                        # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";                                            # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina

        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");

        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body

        require_once(ABSPATH . "/views/materiais/cadastro/material.php");                 # Carrega o respectivo conte�do

        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function alterarMaterial(){

        $this->controleAcesso();

        $this->titulo = "Almoxarifado - Metro Service / METROFOR";
        $this->script = "scriptPesquisaMaterial.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        $_SESSION['dadosMaterial'] = $_POST;

//        echo var_dump($_POST);
//
        $model = $this->carregaModelo('Materiais/alterarMaterial-model');

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Entrada Material";					    		#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/pesquisa/pMaterial.php");   		# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }

    public function vardumppost(){
       return var_dump($_POST);
    }
}

