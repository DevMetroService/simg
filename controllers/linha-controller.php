<?php

class LinhaController extends MainController
{
    protected $linhaModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->linhaModel = $this->carregaModelo('linha-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptLinha.js";

        $linha = $this->linhaModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/linha/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $this->linhaModel->create();

        header("Location:{$this->home_uri}/linha/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codLinha = $parametros[0];

        $this->linhaModel->update($codLinha);

        header("Location:{$this->home_uri}/linha/create");
    }

}
 ?>
