<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 17/09/2020
 * Time: 11:54
 */


class DashboardController extends MainController
{
    public $loginNecessario = true;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->nivelNecessario = $_SESSION['nivelNecessario'];
        $this->controleAcesso();

        $this->titulo = "SIMG";

        $this->script = "/dashboards/{$_SESSION['direcionamento']}.js";

        $_SESSION['bloqueio'] = false;
    }

    public function gesiv()
    {
        $listaMaterialSolicitado = $this->carregaModelo('osmMaterialSolicitado-model');

        $ssmAguardandoMaterial = $this->medoo->query
        ("
            SELECT ssm.cod_ssm, ss.data_status, date_part('day', (CURRENT_TIMESTAMP - ss.data_status)) as dias_aberto
            FROM ssm
            JOIN status_ssm ss USING(cod_mstatus)
            WHERE cod_status = 42
        ")->fetchAll(PDO::FETCH_ASSOC);

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/dashboards/gesiv.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function material()
    {
        $materialModel = $this->carregaModelo('material-model');

        $listaAtivos = $materialModel->getAll(['ativo' => 's', 'OR' => ['cod_grupo[!]' => [22,23], 'cod_grupo' => null]]);
        $listaInativos = $materialModel->getAll(['ativo' => 'n', 'OR' => ['cod_grupo[!]' => [22,23], 'cod_grupo' => null]]);

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/dashboards/material.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function Rh()
    {
        $_SESSION['navegador'] = "sidebarRH.php";

        $modelFuncionario = $this->carregaModelo('funcionario-model');
        
        $tipo_funcionario = $this->medoo->select("tipo_funcionario", "*");

        require_once(ABSPATH . "/views/dashboards/rh.php");
    }

    public function astig()
    {
        require_once(ABSPATH . "/views/dashboards/astig.php");
    }

}
