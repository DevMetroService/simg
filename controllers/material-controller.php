<?php

class MaterialController extends MainController
{
    public $nivelNecessario = [ "4", "6.1" ];

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);
        
        $this->controleAcesso();
    }

    public function create()
    {
      $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

      $this->titulo = "SIMG";                                            # T�tulo da P�gina

      require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina
      require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
          . "navegador.php");
      require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body
      require_once(ABSPATH . "/views/forms/material/create.php");                 # Carrega o respectivo conte�do
      require_once(ABSPATH . "/views/_includes/_footer.php");                           # Carrega o footer
    }

    public function store()
    {
        $materialModel = $this->carregaModelo('material-model');

        $materialModel->create();

        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");

    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codMaterial = $parametros[0];

        $materialModel = $this->carregaModelo('material-model');

        $materialModel->update($codMaterial);

        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
    }

    public function edit()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $parametros = $parametros[0];

        $material = $this->carregaModelo('material-model')->get($parametros);

        $this->titulo = "SIMG";                                                   # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body
        require_once(ABSPATH . "/views/forms/material/edit.php");                 # Carrega o respectivo conte�do
        require_once(ABSPATH . "/views/_includes/_footer.php");                           # Carrega o footer
    }

}
 ?>
