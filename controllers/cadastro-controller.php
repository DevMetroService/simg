<?php

/**
 * Controller de para cadastro de funcionarios  S.I.M.G
 *
 * @package SimgMVC
 * @since 0.1
 *
 * Este c�digo � confidencial.
 * A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 *
 */
class CadastroController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "1";
    public $dadosPessoais;
    public $documentacao;
    public $filiacao;
    public $empresa;
    public $erroFoto;
    public $tipoFoto;
    public $getLastID;

    public $quantidadeMS;
    public $quantidadeMPE;
    public $quantidadeCRJ;
    public $quantidadePJ;


    public function index()
    {
        $this->controleAcesso();
        
        $this->titulo = "SIMG - Cadastro Funcion�rio";
        
        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/dadosPessoaisFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    // Cadastro Funcionario Simples

    public function cadastroSimplesFuncionarioRefill()
    {
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $_SESSION['refillSimples'] = $this->medoo->select('v_funcionario', '*', ['matricula' => $_POST['matricula']]);
            $_SESSION['refillSimples'] = $_SESSION['refillSimples'][0];
        }

    }

    public function cadastroSimplesFuncionario()
    {
        $this->controleAcesso();

        $this->script = "scriptFuncionario.js";
        $this->titulo = "SIMG - Cadastro Funcion�rio";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/cadastro-simples.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cadastroSimples(){
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $_SESSION['dadosNovoFuncionario'] = $_POST;
        }

        $this->carregaModelo('RH/cadastroSimples-model');
        header("Location:".HOME_URI."/dashboardRh");
    }

    public function cadastroCargo(){
        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $recebe_model = $this->carregaModelo('campos/Cargo-model');
            if(empty($_POST["cod_cargos"])) {
                $recebe_model = $recebe_model->insert();
                header("Location:" . HOME_URI . "/dashboardRh/cargos/success/1");
                return;
            }
            else {
                $recebe_model = $recebe_model->update();
                header("Location:" . HOME_URI . "/dashboardRh/cargos/success/2");
                return;
            }
        }
        header("Location:".HOME_URI."/dashboardRh/cargos/danger/3");
    }

    public function alterarCadastroSimples(){
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $_SESSION['dadosFuncionario'] = $_POST;
        }

        $this->carregaModelo('RH/alterarCadastroSimples-model');
        header("Location:".HOME_URI."/dashboardRh");
    }

    // Cadastro Externo Simples

    public function cadastroExternoRefill()
    {
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $prefix = substr($_POST['matricula'], 0, 3);

            switch($prefix){
                case '900':
                    $tabela = "funcionario_crj";
                    $nome = "nome_funcionario_crj";
                    $cod = "cod_funcionario_crj";
                    break;

                case '800':
                    $tabela = "funcionario_mpe";
                    $nome = "nome_funcionario_mpe";
                    $cod = "cod_funcionario_mpe";
                    break;

                default:
                    $tabela = "funcionario_pj";
                    $nome = "nome_funcionario_pj";
                    $cod = "cod_funcionario_pj";
                    break;
            }

            $sql = "SELECT {$nome} AS nome_funcionario, {$cod} AS cod_funcionario, * FROM {$tabela} WHERE matricula = {$_POST['matricula']}";

            $_SESSION['refillSimples'] = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $_SESSION['refillSimples'] = $_SESSION['refillSimples'][0];
        }

    }

    public function cadastroExterno(){
        $this->controleAcesso();

        $this->script = "scriptFuncionarioExterno.js";
        $this->titulo = "SIMG - Cadastrar Pj E Consorciados";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/rh/cadastro-externo/externo.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function salvarCadastroExterno(){
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $_SESSION['dadosPj'] = $_POST;
        }

        $this->carregaModelo('RH/cadastroPj-model');
        header("Location:".HOME_URI."/dashboardRh");
    }

    public function alterarCadastroExterno(){
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
            $_SESSION['dadosPj'] = $_POST;
        }

        $this->carregaModelo('RH/alterarCadastroPj-model');
        header("Location:".HOME_URI."/dashboardRh");
    }

    // Tabelas

    public function visualizarTabelas()
    {
        $this->controleAcesso();

        $ms = $this->medoo->select("funcionario", "cod_funcionario", ["matricula[<]" => 6999]);
        if(!empty(count($ms)))
            $this->quantidadeMS = count($ms);

        $mpe = $this->medoo->select("funcionario_mpe", "cod_funcionario_mpe");
        if(!empty(count($mpe)))
            $this->quantidadeMS = count($mpe);

        $crj = $this->medoo->select("funcionario_crj", "cod_funcionario_crj");
        if(!empty(count($crj)))
            $this->quantidadeMS = count($crj);

        $pj = $this->medoo->select("funcionario_pj", "cod_funcionario_pj");
        if(!empty(count($pj)))
            $this->quantidadeMS = count($pj);

        $this->titulo = "SIMG";
        $this->script = "dashboardRh.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/rh/visualizarTabelas.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function documentacaoFuncionario()
    {

        $_SESSION['dadosPessoais'] = $_POST;

        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                            #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "documentacaoFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer

    }

    public function filiacaoFuncionario()
    {

        $_SESSION['documentacaoFuncionario'] = $_POST;

        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                               #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "filiacaoFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer

    }

    public function empresaFuncionario()
    {
        $_SESSION['filiacaoFuncionario'] = $_POST;
        $this->controleAcesso();


        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                             #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                               #

        require_once(ABSPATH . "/views/_includes/_header.php");                         # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                          # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                           # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "empresaFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                         # Carrega o footer

    }

    public function confirmaCadastro()
    {
        $_SESSION['empresaFuncionario'] = $_POST;

        $this->controleAcesso();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------	TRATAMENTO FOTO 	------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $funcionarioID = $this->medoo->select("funcionario", "cod_funcionario", ["ORDER" => "cod_funcionario cresc"]);

        $contador = count($funcionarioID);

        $lastIdFunc = (int)$funcionarioID[$contador-1];

        $lastIdFunc += 1 ;


        $tipoFoto = $_FILES['fotoFuncionario']['type'];

        if ($tipoFoto === 'image/gif' || $tipoFoto === 'image/png' || $tipoFoto === 'image/jpg' || $tipoFoto === 'image/jpeg' || $tipoFoto === 'image/bmp') {
            $nomeFoto = addslashes($_FILES['fotoFuncionario']['name']);
            $pegaExtensao = preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $nomeFoto, $ext);
            $nomeFotoHash = $lastIdFunc . "." . $ext[1];
            $tempImagem = $_FILES['fotoFuncionario']['tmp_name'];
            $tamanhoImagem = $_FILES['fotoFuncionario']['size'];

            {
                if(file_exists ( UP_ABSPATH . "/rh/" . $nomeFotoHash )){
                    unlink(UP_ABSPATH . "/rh/" . $nomeFotoHash);
                }
                if ($tamanhoImagem > ((1024 * 1024) * 3)) {
                    echo("<script>$.alert('ERRO: Arquivo Maior que 3 MB!')</script>");
                    $this->vaiParaPagina('empresaFuncionarioRefill');
                    return;
                } else {
                    $caminhoFoto = UP_ABSPATH . "/rh/" . $nomeFotoHash;
                    move_uploaded_file($tempImagem, $caminhoFoto);
                    $salvarFotoBanco = $this->medoo->insert('fotostemp',
                        [
                            "foto" => $caminhoFoto,
                            "nome" => $nomeFotoHash
                        ]);

                }

            }
        } else {
            echo("<script>$.alert('ERRO: Imagem submetida n�o � gif, png, bmp, jpg ou jpeg!')</script>");
            $this->vaiParaPagina('empresaFuncionarioRefill');
            return;
        }

        $usuario = $_SESSION['empresaFuncionario']['usuarioFuncionario'];
        $usuarios = $this->medoo->count("usuario", ["usuario" => strtolower($usuario)]);

        if ($usuarios >= 1) {
            echo("<script>$.alert('ERRO: Usu�rio J� existe')</script>");
            $this->vaiParaPagina('empresaFuncionarioRefill');
            return;
        }


        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                              #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "confirmaCadastro.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer


    }

    public function inserirFuncionario()
    {

        $this->controleAcesso();

        $modelo = $this->carregaModelo('RH/cadastroFuncionario-model');

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";   								        	# Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";											# T�tulo da P�gina

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/rh/dashboard.php");				        	# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer


    }

    # ------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------  FUNCTIONS PARA O REFILL  -------------------------------------- #
    # ------------------------------------------------------------------------------------------------------- #

    public function indexRefill()
    {
        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                               #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "cadastro-simples.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer

    }

    public function documentacaoFuncionarioRefill()
    {

        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                               #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio / Documenta��o";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "documentacaoFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer

    }

    public function filiacaoFuncionarioRefill()
    {

        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                            #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "filiacaoFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer
    }

    public function empresaFuncionarioRefill()
    {

        $this->controleAcesso();


        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                                 #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                                #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "empresaFuncionario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer

    }

    public function confirmaCadastroRefill()
    {


        $this->controleAcesso();

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                              #
        $this->titulo = "S.I.M.G - Cadastro Funcion�rio";                            #

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                        # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "confirmaCadastro.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");                        # Carrega o footer


    }

    public function imprimirCadastro()
    {
        $this->controleAcesso();


        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->titulo = "S.I.M.G - Impress�o Do Cadastro";                                # T�tulo da P�gina

        require_once(ABSPATH . "/views/_includes/_header.php");                        # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/_body.php");                        # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario"
            . "/imprimirCadastro.php");                                                # Carrega o respectivo conte�do
        require_once(ABSPATH . "/views/_includes/_footer.php");

    }

    public function demissao()
    {

        $this->controleAcesso();


        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();                #

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";                                               #
        $this->titulo = "S.I.M.G - Desligamento de Funcion�rio";                        #

        require_once(ABSPATH . "/views/_includes/_header.php");                         # Carrega o header da p�gina
        require_once(ABSPATH . "/views/_includes/navegadores/"                          # Carrega o respectivo navegador
            . "navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");                           # Carrega o Body
        require_once(ABSPATH . "/views/rh/cadastro-funcionario/"                        # Carrega o respectivo conte�do
            . "demissao.php");

        require_once(ABSPATH . "/views/_includes/_footer.php");                         # Carrega o footer

    }

}

