<?php

class PontoNotavelController extends MainController
{
    protected $pnModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->pnModel = $this->carregaModelo('pontoNotavel-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptPontoNotavel.js";

        $pontoNotavel = $this->pnModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/pontoNotavel/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $this->pnModel->create();

        header("Location:{$this->home_uri}/pontoNotavel/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codPN = $parametros[0];

        $this->pnModel->update($codPN);

        header("Location:{$this->home_uri}/pontoNotavel/create");
    }

}
 ?>
