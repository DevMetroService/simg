<?php

class ServicoPmpController extends MainController
{
    protected $servicoModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->servicoModel = $this->carregaModelo('servicoPmp-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptServicoPmp.js";

        $servico_pmp = $this->servicoModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/servicoPmp/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $subSistema = $this->medoo->select("sub_sistema", "*", [
            "AND" => [
                "cod_sistema" => $_POST['cod_sistema'], 
                "cod_subsistema" => $_POST['cod_subsistema']
                ]
            ]
        )[0];

        $_POST['cod_sub_sistema'] = $subSistema['cod_sub_sistema'];

        $this->servicoModel->create();

        header("Location:{$this->home_uri}/servicoPmp/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codServico = $parametros[0];

        $this->servicoModel->update($codServico);

        header("Location:{$this->home_uri}/amv/create");
    }

}
 ?>
