<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 22/02/2019
 * Time: 10:11
 */

class DashBoardEngenhariaMrController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = ["7.2", "5.1"];

    public function index()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia Material Rodante";
        $this->script = "dashboardEngenhariaMr.js";
        $this->paginaDashboard = "cronograma";

        $_SESSION['direcionamento'] = "EngenhariaMr";

        $veiculosManut = $this->medoo->query("SELECT distinct on(cod_veiculo) *
            FROM disponibilidade
            JOIN veiculo USING(cod_veiculo)
            WHERE disponibilidade IN ('i', 'n') AND cod_grupo IN (22,23)
            ORDER BY cod_veiculo, data_alteracao DESC")->fetchAll(PDO::FETCH_ASSOC);

        $veiculosOperacao = $this->medoo->query("SELECT distinct on(cod_veiculo) *
            FROM disponibilidade
            JOIN veiculo USING(cod_veiculo)
            WHERE disponibilidade IN ('s') AND cod_grupo IN (22,23)
            ORDER BY cod_veiculo, data_alteracao DESC")->fetchAll(PDO::FETCH_ASSOC);

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariamr/dashboardEngenharia.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function corretiva()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia MR";
        $this->script = "dashboardEngenhariaCorretiva.js";
        $this->paginaDashboard = "corretiva";


        $contador = $this->medoo->select("v_saf", "*",["nome_status" => "Aberta"]);
        $contador = count($contador);
        $this->quantidadeSAF = $contador;

        $contador = $this->medoo->select("v_ssm", "*",["cod_status" => (int)9]);
        $contador = count($contador);
        $this->quantidadeSSM = $contador;

        $contador = $this->medoo->select("osm_falha",["[><]status_osm" =>"cod_ostatus"], "*",["cod_status" => 10]);
        $contador = count($contador);
        $this->quantidadeOSM = $contador;


        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariamr/dashboardCorretiva.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function programacao()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia MR";
        $this->script = "dashboardEngenhariaProgramacao.js";
        $this->paginaDashboard = "programacao";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariamr/dashboardProgramacao.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function equipe()
    {
      $_SESSION['equipeUsuario'] = $this->medoo->select('eq_us_unidade', ["[><]un_equipe" => "cod_un_equipe", "[><]equipe" => "cod_equipe", "[><]unidade" => "cod_unidade"], ['sigla', 'nome_equipe', 'sigla_unidade', 'cod_unidade', 'nome_unidade'], ['cod_usuario' => (int)$this->dadosUsuario['cod_usuario']]);

//      $nivelUsuario = $this->dadosUsuario['nivel'];

      $this->equipeUsuario = $this->medoo->select('eq_us_unidade', ["[><]un_equipe" => "cod_un_equipe"], 'cod_un_equipe', ['cod_usuario' => (int)$this->dadosUsuario['cod_usuario']]);
      $this->equipeUsuario;

      $this->controleAcesso();

      $_SESSION['direcionamento'] = "EngenhariaMr";
      $this->paginaDashboard = "correProg";

      $this->titulo = "Sistema de Manutenção - Metro Service / METROFOR";
      $this->script = "dashboardEquipe.js";

      require_once(ABSPATH . "/views/_includes/_header.php");
      require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
      require_once(ABSPATH . "/views/_includes/_body.php");
      require_once(ABSPATH . "/views/equipemr/dashboardEquipeMr.php");
      require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cadastroMateriais()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia MR - Materiais";
        $this->script = "scriptCadastroMateriaisMr.js";
        $this->paginaDashboard = "programacao";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/cadastroMateriaisMr.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function adicionarMaterial()
    {
        $_SESSION['dadosMaterial'] = $_POST;

        $this->controleAcesso();

        $model = $this->carregaModelo('Materiais/cadastroMateriais-model');

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        unset($_SESSION['dadosMaterial']);

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . '/cadastroMateriais');
    }

}
