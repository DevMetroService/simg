<?php

class DashBoardEngenhariaController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "7";

    public function index()
    {

        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia";
        $this->script = "dashboardEngenhariaCronograma.js";
        $this->paginaDashboard = "cronograma";

        $_SESSION['direcionamento'] = "Engenharia";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        
        $this->dashboard->modalUserForm($this->dadosUsuario);

        $this->dashboard->cabecalho('Engenharia - Cronograma ' . date('Y'));
        $this->dashboard->graficoAcompanhamentoAnoAtual();
        $this->analiseServico();
        $this->dashboard->painelCronograma('Ed', true);
        $this->dashboard->painelCronograma('Vp', true);
        $this->dashboard->painelCronograma('Su', true);
        $this->dashboard->painelCronograma('Ra', true);
        $this->dashboard->painelCronograma('Tl', true);
        $this->dashboard->painelCronograma('Bl', true);

        $this->dashboard->modalExibirCronograma();

        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cronograma(){
        $this->index();
    }

    public function pmp()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia";
        $this->script = "dashboardEngenhariaPmp.js";
        $this->paginaDashboard = "pmp";

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 21]]);
        $contador = count($contador);
        $this->quantidadeEd = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 20]]);
        $contador = count($contador);
        $this->quantidadeRa = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 25]]);
        $contador = count($contador);
        $this->quantidadeSb = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 24]]);
        $contador = count($contador);
        $this->quantidadeVp = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 27]]);
        $contador = count($contador);
        $this->quantidadeTl = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 28]]);
        $contador = count($contador);
        $this->quantidadeBl = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 31]]);
        $contador = count($contador);
        $this->quantidadeJd = $contador;


        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/dashboardPmp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function corretiva()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia";
        $this->script = "dashboardEngenhariaCorretiva.js";
        $this->paginaDashboard = "corretiva";


        $contador = $this->medoo->select("v_saf", "*",["nome_status" => "Aberta"]);
        $contador = count($contador);
        $this->quantidadeSAF = $contador;

        $contador = $this->medoo->select("v_ssm", "*",["cod_status" => (int)9]);
        $contador = count($contador);
        $this->quantidadeSSM = $contador;

        $contador = $this->medoo->select("osm_falha",["[><]status_osm" =>"cod_ostatus"], "*",["cod_status" => 10]);
        $contador = count($contador);
        $this->quantidadeOSM = $contador;


        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/dashboardCorretiva.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function programacao()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia";
        $this->script = "dashboardEngenhariaProgramacao.js";
        $this->paginaDashboard = "programacao";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/dashboardProgramacao.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function preventiva()
    {
        $this->controleAcesso();

        $this->titulo = "SIMG - Engenharia";
        $this->script = "dashboardEngenhariaPreventiva.js";
        $this->paginaDashboard = "preventiva";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/dashboardPreventiva.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function dashboardPmp(){
        $this->preventiva();
    }

    public function analiseServico()
    {
        $ssms = $this->medoo->select('v_ssm', '*', ['cod_status'=> 39]);

        $contador = count($ssms);
        if ($contador > 0) {
            $this->htmlTabelaInicio("AnaliseSsm", "SSM's Aguardando An�lise", $contador);

            echo "<thead>
                <tr>
                    <th>SSM</th>
                    <th>SAF</th>
                    <th>Grupo de Sistemas</th>
                    <th>Local</th>
                    <th>Data de Abertura</th>
                    <th>Avaria</th>
                    <th>Servi�o</th>
                    <th>A��o</th>
                </tr>
            </thead>
            <tbody>";
            $home_uri = HOME_URI;

            foreach ($ssms as $dados => $value) {
                $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                echo "<tr>
                        <td>{$value['cod_ssm']}</td>
                        <td>{$value['cod_saf']}</td>
                        <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                        <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                        <td>{$dataAbertura}</td>
                        <td>{$value['nome_avaria']}</td>";
//                    if(!empty($value['nome_veiculo'])){
//                        echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
//                    }else{
                    echo "<td>{$value['nome_servico']}</td>";
//                    }

                echo "<td>
                            <a href='{$home_uri}/dashboardEngenharia/analisarSsm/{$value['cod_ssm']}'>
                                <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                    <i class='fa fa-eye fa-fw'></i>
                                </button>
                            </a>
                            <br>
                        </td>
                    </tr>";
            }

            $this->htmlTabelaFim();

        }
    }

    public function analisarSsm()
    {

            $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
            $codSsm = $parametros[0];

            $query = "SELECT ss.cod_saf, ss.data_abertura, ss.nome_equipe, ss.cod_ssm, ss.nome_servico, ss.nome_grupo,
                    ss.nome_sistema, ss.nome_subsistema, ss.nome_linha, ss.nome_trecho, ss.nome_ponto, ss.nome_avaria,
                    s.complemento_falha, ss.complemento_local, ss.complemento_servico, ss.descricao_trecho, ss.cod_status,
                    ss.nome_veiculo, ss.nome_carro, s.nivel as nivel_saf
                  FROM v_ssm ss
                  JOIN v_saf s USING(cod_saf)
                  WHERE cod_ssm = {$codSsm}";

            $valores = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $valores = $valores[0];

            if($valores['cod_status'] != 39){
                header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
            }

            $query = "SELECT cod_osm, nome_atuacao, desc_atuacao, cod_ag_causador, nome_agente
                    FROM osm_registro
                    JOIN atuacao USING(cod_atuacao)
                    JOIN agente_causador USING(cod_ag_causador)
                    WHERE cod_osm IN (SELECT cod_osm FROM osm_falha WHERE cod_ssm = {$codSsm}) ORDER BY cod_osm asc";

            $valores['registro_os'] = $this->medoo->query($query)->fetchAll();

            $cod_ag_causador = $valores['registro_os'][0]['cod_ag_causador'];
            $origem = array('s' => 'MetroService', 'f' => 'MetroFor', "t" => "TSM", 'o' => 'Outros');

            $this->titulo = "Valida��o de Servi�o";
            $this->script = "scriptAnalisarSsm.js";

            require_once(ABSPATH . "/views/_includes/_header.php");
            require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
            require_once(ABSPATH . "/views/_includes/_body.php");
            require_once(ABSPATH . "/views/_includes/formularios/ssm/analisarSsm.php");
            require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function ssmAnalisada(){
        $ssm = $this->medoo->select ("ssm", "*", ["cod_ssm" => $_POST['cod_ssm']]);

        $time = date('d-m-Y H:i:s', time());
        //Alterar status da SSM
        $this->alterarStatusSsm($_POST['cod_ssm'], 35, $time, $this->dadosUsuario); //Status da SSM fica em Aguardando Valida��o
        //Alterar o status da SAF caso seja a �ltima
        $this->encerrarSaf($ssm[0]['cod_saf'], $time, $this->dadosUsuario, 37); //Status da SAF fica Aguardando Valida��o SE esta for a �ltima SSM.

        $osms = $this->medoo->select ('osm_falha', '*', ["cod_ssm" => $_POST['cod_ssm']]);

        foreach($osms as $dados=>$value)
        {
            $this->medoo->update ('osm_registro',
                [
                    'cod_ag_causador' => $_POST['agenteCausador']
                ],
                [
                    'cod_osm' => $value['cod_osm']
                ]
            );
        }

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);

    }

    private function alterarStatusSsm($codSsm, $status, $time, $dadosUsuario, $descricao = null){
        $ssmPendente = $this->medoo->insert("status_ssm", [
            "cod_ssm"     => (int)$codSsm,
            "cod_status"  => (int)$status,
            "descricao"   => $descricao,
            "data_status" => $time,
            "usuario"     => (int)  $dadosUsuario['cod_usuario']
        ]);

        $updateSsm = $this->medoo->update("ssm", [
            "cod_mstatus" => (int)$ssmPendente
        ] ,[
            "cod_ssm"     => (int)$codSsm
        ]);
    }

    private function encerrarSaf($codSaf, $time, $dadosUsuario, $status = 14, $descricao = ""){
        //Por meio da OS n�o ser� possivel encerrar a SAF, deixando-a para analise
        if( $this->verificarSsm($codSaf)){
            $encerrarStatusSaf = $this->medoo->insert("status_saf",[
                "cod_status"        => (int) $status, //Encerrada 14
                "cod_saf"           => (int) $codSaf,
                "descricao"         => $descricao,
                "data_status"       => $time,
                "usuario"           => (int)  $dadosUsuario['cod_usuario']
            ]);

            $encerrarSaf = $this->medoo->update("saf",[
                "cod_ssaf"  => (int) $encerrarStatusSaf
            ],[
                "cod_saf"   => (int) $codSaf
            ]);
        }
    }

    private function verificarSsm($codSaf){
        $ssms = $this->medoo->select("v_ssm","cod_status",["cod_saf" => $codSaf]);

        if(count($ssms) == 1)
        {
            return true;
        }

        $status = [6,7,16, 26, 35]; // DEVOLVIDA, CANCELADA, ENCERRADA, PROGRAMADA, AGUARDANDO VALIDA��O

        foreach($ssms as $dados => $value){
            //Se for diferente de DEVOLVIDA, CANCELADA, PROGRAMADA ou ENCERRADA,
            //N�O altera a SAF
            if(!in_array($value, $status)){
                return false;
            }
        }
        return true;
    }

    private function htmlTabelaInicio($table, $titulo, $contador, $cor = 'default', $subTitulo = ''){
        $corBtn = "default";
        if($cor == "red") $corBtn = $cor;

        echo <<<HTML
<div class='row'>
    <div class='col-sm-12'>
        <div class='panel panel-{$cor}'>
            <div class='panel-heading' role='tab' id='heading'>
                <div class='row'>
                    <div class='col-md-4'>
                        <a class='collapsed' role='button' data-toggle='collapse' href='#tabela{$table}' aria-expanded='false'>
                            <button class='btn btn-{$corBtn}'><label>{$titulo}</label></button>
                        </a>
                    </div>
                    {$subTitulo}
                </div>
            </div>
            <div id='tabela{$table}' class='panel-collapse collapse out' role='tabpanel'>
                <div class='panel-body'>
                    <strong>Total de Itens da tabela: {$contador}</strong>
                    <table id='indicador{$table}' class='table table-bordered table-striped'>
HTML;
    }

    private function htmlTabelaFim(){
        echo <<<HTML
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
    }
}
