<?php

class AvariaController extends MainController
{
    private $avariaModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);
        $this->titulo = "SIMG";                                                   # T�tulo da P�gina

        $this->avariaModel = $this->carregaModelo('campos/avaria-model');
    }

    public function create()
    {
      $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();      
    }

    public function store()
    {

        $this->avariaModel->create();

        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");

    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codAvaria = $parametros[0];

        $this->avariaModel->update($codAvaria);

        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
    }

    public function edit()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $parametros = $parametros[0];

    }

    public function getAvariaById()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $parametros = $parametros[0];
        
        // echo json_encode($this->avariaModel->getAvaria($parametros));
        $arr = $this->avariaModel->getAvaria($parametros);
        $arr['nome_avaria'] = utf8_decode($arr['nome_avaria']);
        echo json_encode($arr);
    }

}
 ?>
