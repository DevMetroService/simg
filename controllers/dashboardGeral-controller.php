<?php

/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 04/04/2016
 * Time: 09:35
 */
class DashBoardGeralController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "0";

    public function controle()
    {
        switch (strtolower($_SESSION['direcionamento'])) {
            case "rh":
                $this->nivelNecessario = "1";
                break;

            case "ccm":
                $this->nivelNecessario = "2";
                break;

            case "supervisao":
                $this->nivelNecessario = "2.1";
                break;

            case "usuario":
                $this->nivelNecessario = "2.3";
                break;

            case "ti":
                $this->nivelNecessario = "3";
                break;

            case "materiais":
                $this->nivelNecessario = "4";
                break;

            case "equipe":
                $this->nivelNecessario = ["5", "5.2"];
                break;

            case "equipemr":
                $this->nivelNecessario = "5.1";
                break;

            case "diretoria":
                $this->nivelNecessario = "6";
                break;

            case "gesiv":
                $this->nivelNecessario = "6.1";
                break;

            case "astig":
                $this->nivelNecessario = "6.2";
                break;

            case "engenharia":
                $this->nivelNecessario = "7";
                break;

            case "engenhariasupervisao":
                $this->nivelNecessario = "7.1";
                break;

            case "engenhariamr":
                $this->nivelNecessario = "7.2";
                break;
        }

        $this->controleAcesso();
    }

    //------------------------SAF------------------------//
    public function saf()
    {
        $this->controle();

        $this->titulo = "Solicita��o de Abertura de Falha / SAF - Metro Service";
        $this->script = "scriptSaf.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");

        if (empty($_SESSION['refillSaf']['codigoSaf']))
            $_SESSION['refillSaf']['codigoSaf'] = null;

        $arrayResult = $this->form->saf($this->dadosUsuario, $_SESSION['refillSaf']['codigoSaf']);
        $saf = $arrayResult['dados'];
        $btnsAcao = $arrayResult['botoes'];
        $historico = $arrayResult['historico'];

        require_once(ABSPATH . "/views/_includes/formularios/saf/saf.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

        unset ($_SESSION['refillSaf']);
    }

    public function printSaf()
    {
        $this->titulo = "SAF";

        if(!empty($_SESSION['refillSaf']['codigoForm']))
            $_SESSION['refillSaf']['codigoSaf'] = $_SESSION['refillSaf']['codigoForm'];

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/formularios/saf/printSaf.php");
    }

    public function viewSaf()
    {
        $this->titulo = "SAF";

        $arrayResult = $this->form->viewSafButton($this->dadosUsuario, $_SESSION['refillSaf']['codigoSaf']);
        $button = $arrayResult['botoes'];

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/formularios/saf/printSaf.php");
    }

    public function pesquisaSaf($dadosRefill = null, $returnPesquisa = null)
    {
        $this->controle();

        $this->titulo = "Pesquisa de SAF - Metro Service";
        $this->script = "scriptPesquisaSaf.js";

        $btnAcoes = $this->form->botoesPesquisaSaf();

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/saf/pesquisaSaf.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

        //Modal para exibi��o de SAF do resultado da pesquisa
        //$this->dashboard->modalExibirSaf();
    }

    public function executarPesquisaSaf()
    {
        $_POST['whereSql'] = "cod_saf, nome_linha, descricao_trecho, data_abertura, nome_veiculo, nome_avaria, nome_status, nivel";
        $pesquisaSafModel = $this->carregaModelo("ccm/saf/pesquisaSaf-model");
        $returnPesquisa = $pesquisaSafModel->getDados();

        $this->pesquisaSaf($_POST, $returnPesquisa);
    }

    public function resetarPesquisaSaf()
    {
        $this->pesquisaSaf();
    }

    public function csvPesquisaSafCompleta()
    {
        $_POST['whereSql'] = "*";
        $pesquisaSafModel = $this->carregaModelo("ccm/saf/pesquisaSaf-model");
        $returnPesquisa = $pesquisaSafModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/saf/csvPesquisaSafCompleta.php");
    }

    public function csvKmRodadosPorVeiculo()
    {
        $pesquisaSafModel = $this->carregaModelo("mr-model");

        // TUE
        if ( (int)$_POST['grupo'] == 23 ) {
            $returnPesquisa = $pesquisaSafModel->csvKmRodadosPorVeiculoTUE();
        }

        // VLT
        if ( (int)$_POST['grupo'] == 22 ) {
            $returnPesquisa = $pesquisaSafModel->csvKmRodadosPorVeiculoVLT();
        }

        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/csvKmRodadosPorVeiculo.php");
    }

    public function pesquisaSafParaSsm()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codigoSaf = $parametros[0];

        unset($_POST);
        $_POST['codigoSaf'] = $codigoSaf;

        $this->executarPesquisaSsm();
    }

    //------------------------SSM
    public function ssm()
    {
        if (empty($_SESSION['refillSsm']['codigoSsm']))
            header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);

        $this->controle();

        $this->titulo = "Solicita��o de Servi�o da Manunten��o / SSM - Metro Service";
        $this->script = "scriptSsm.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");

        $arrayResult = $this->form->ssm($this->dadosUsuario, $_SESSION['refillSsm']['codigoSsm']);
        $ssm = $arrayResult['dadosSsm'];
        $saf = $arrayResult['dadosSaf'];
        $btnsAcao = $arrayResult['botoes'];
        $historico = $arrayResult['historico'];
        $dataEncaminhamento = $arrayResult['dataEncaminhamento'];

        require_once(ABSPATH . "/views/_includes/formularios/ssm/ssm.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function aprovarSsm()
    {
        $this->controle();

        if(in_array($this->dadosUsuario['cod_usuario'], $this->dashboard->usuarioValidacao)) {

            $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
            $codSsm = $parametros[0];

            $query = "SELECT ss.cod_saf, ss.data_abertura, ss.nome_equipe, ss.cod_ssm, ss.nome_servico, ss.nome_grupo,
                    ss.nome_sistema, ss.nome_subsistema, ss.nome_linha, ss.nome_trecho, ss.nome_ponto, ss.nome_avaria,
                    s.complemento_falha, ss.complemento_local, ss.complemento_servico, ss.descricao_trecho, ss.cod_status,
                    ss.nome_veiculo, ss.nome_carro
                  FROM v_ssm ss
                  JOIN saf s USING(cod_saf)
                  WHERE cod_ssm = {$codSsm}";

            $valores = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $valores = $valores[0];

            if($valores['cod_status'] != 35){
                header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
            }

            $query = "SELECT cod_osm, nome_atuacao, desc_atuacao
                    FROM osm_registro
                    JOIN atuacao USING(cod_atuacao)
                    WHERE cod_osm IN (SELECT cod_osm FROM osm_falha WHERE cod_ssm = {$codSsm}) ORDER BY cod_osm asc";

            $valores['registro_os'] = $this->medoo->query($query)->fetchAll();

            $this->controle();

            $this->titulo = "Valida��o de Servi�o";
            $this->script = "scriptAprovarSsm.js";

            require_once(ABSPATH . "/views/_includes/_header.php");
            require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
            require_once(ABSPATH . "/views/_includes/_body.php");
            require_once(ABSPATH . "/views/_includes/formularios/ssm/aprovarSsm.php");
            require_once(ABSPATH . "/views/_includes/_footer.php");
        }else{
            header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
        }

    }

    public function printSsm()
    {
        $this->titulo = "SSM";

        if (!empty($_SESSION['refillSsm']['codigoForm']))
            $_SESSION['refillSsm']['codigoSsm'] = $_SESSION['refillSsm']['codigoForm'];

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/formularios/ssm/printSsm.php");
    }

    public function viewSsm()
    {
        $this->titulo = "SSM";

        $arrayResult = $this->form->viewSsmButton($this->dadosUsuario, $_SESSION['refillSsm']['codigoSsm']);
        $button = $arrayResult['botoes'];

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/formularios/ssm/printSsm.php");
    }

    public function pesquisaSsm($dadosRefill = null, $returnPesquisa = null)
    {
        $this->controle();

        $this->titulo = "Pesquisa de SSM - Metro Service";
        $this->script = "scriptPesquisaSsm.js";

        $btnAcoes = $this->form->botoesPesquisaSsm();

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/ssm/pesquisaSsm.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaSsm()
    {
        $_POST['whereSql'] = "cod_ssm, cod_saf, data_abertura, nome_subsistema, nome_linha, descricao_trecho, nome_status, nome_veiculo, nome_servico, complemento_servico, nivel";
        $pesquisaSsmModel = $this->carregaModelo("ccm/ssm/pesquisaSsm-model");
        $returnPesquisa = $pesquisaSsmModel->getDados();

        $this->pesquisaSsm($_POST, $returnPesquisa);
    }

    public function resetarPesquisaSsm()
    {
        $this->pesquisaSsm();
    }

    public function csvPesquisaSsmCompleta()
    {
        $_POST['whereSql'] = "*";
        $pesquisaSsmModel = $this->carregaModelo("ccm/ssm/pesquisaSsm-model");
        $returnPesquisa = $pesquisaSsmModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/ssm/csvPesquisaSsmCompleta.php");
    }

    public function pesquisaSsmParaSaf()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codigoSaf = $parametros[0];

        unset($_POST);
        $_POST['codigoSaf'] = $codigoSaf;

        $this->executarPesquisaSaf();
    }

    public function pesquisaSsmParaOsm()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codigoSsm = $parametros[0];

        unset($_POST);
        $_POST['codigoSsm'] = $codigoSsm;

        $this->executarPesquisaOsm();
    }

    //------------------------OSM
    public function osm()
    {
        $this->controle();

        $this->titulo = "Ordem de Servi�o de Manunten��o / OSM- Metro Service";
        $this->script = "scriptOsDadosGerais.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/osm.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function printOsm()
    {
        $this->titulo = "OSM";

        if(!empty($_SESSION['refillOs']['codigoForm']))
        $_SESSION['refillOs']['codigoOs'] = $_SESSION['refillOs']['codigoForm'];
        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/formularios/osm/printOsm.php");
    }

    public function pesquisaOsm($dadosRefill = null, $returnPesquisa = null)
    {
        $this->controle();

        $this->titulo = "Pesquisa de OSM - Metro Service";
        $this->script = "scriptPesquisaOsm.js";

        $btnAcoes = $this->form->botoesPesquisaOsm();

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/osm/pesquisaOsm.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaOsm()
    {
        $pesquisaOsmModel = $this->carregaModelo("ccm/osm/pesquisaOsm-model");
        $returnPesquisa = $pesquisaOsmModel->getDados();

        $this->pesquisaOsm($_POST, $returnPesquisa);
    }

    public function resetarPesquisaOsm()
    {
        $this->pesquisaOsm();
    }

    public function csvPesquisaOsmCompleta()
    {
        $pesquisaOsmModel = $this->carregaModelo("ccm/osm/pesquisaOsm-model");
        $returnPesquisa = $pesquisaOsmModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/osm/txtPesquisaOsmCompleta.php");
    }

    public function csvPesquisaOsmMaoObra()
    {
        $pesquisaOsmModel = $this->carregaModelo("ccm/osm/pesquisaOsm-model");
        $returnPesquisa = $pesquisaOsmModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/osm/txtPesquisaOsmMaoObra.php");
    }

    public function csvPesquisaOsmMateriais()
    {
        $pesquisaOsmModel = $this->carregaModelo("ccm/osm/pesquisaOsm-model");
        $returnPesquisa = $pesquisaOsmModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/osm/txtPesquisaOsmMateriais.php");
    }

    public function pesquisaOsmParaSsm()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codigoSsm = $parametros[0];

        unset($_POST);
        $_POST['numeroSsm'] = $codigoSsm;

        $this->executarPesquisaSsm();
    }

    //-------------------------SSP
    public function ssp()
    {
        $this->controle();

        $this->titulo = "Solicita��o de Servi�o Preventivo / SSP - Metro Service";
        $this->script = "scriptSsp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");

        $arrayResult = $this->form->ssp($this->dadosUsuario, $_SESSION['refillSsp']['codigoSsp']);
        $Ssp = $arrayResult['dados'];
        $btnsAcao = $arrayResult['botoes'];

        require_once(ABSPATH . "/views/_includes/formularios/ssp/ssp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

        unset ($_SESSION['refillSsp']);
    }

    public function printSsp()
    {
        $this->titulo = "SSP";

            if(!empty($_SESSION['refillSsp']['codigoForm']))
            $_SESSION['refillSsp']['codigoSsp'] = $_SESSION['refillSsp']['codigoForm'];
            require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
            require_once(ABSPATH . "/views/_includes/formularios/ssp/printSsp.php");
    }

    public function pesquisaSsp($dadosRefill = null, $returnPesquisa = null)
    {
        $this->controle();

        $this->titulo = "Pesquisa de SSP - Metro Service";
        $this->script = "scriptPesquisaSsp.js";

        $btnAcoes = $this->form->botoesPesquisaSsp();

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/ssp/pesquisaSsp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaSsp()
    {
        $_POST['whereSql'] = "cod_ssp, nome_linha, descricao_trecho, data_programada, nome_tipo_intervencao, nome_servico, nome_status";
        $pesquisaSspModel = $this->carregaModelo("ccm/ssp/pesquisaSsp-model");
        $returnPesquisa = $pesquisaSspModel->getDados();

        $this->pesquisaSsp($_POST, $returnPesquisa);
    }

    public function resetarPesquisaSsp()
    {
        $this->pesquisaSsp();
    }

    public function csvPesquisaSspCompleta()
    {
        $_POST['whereSql'] = "*";
        $pesquisaSspModel = $this->carregaModelo("ccm/ssp/pesquisaSsp-model");
        $returnPesquisa = $pesquisaSspModel->getDados();
        // var_dump($returnPesquisa);
        require_once(ABSPATH . "/views/_includes/formularios/ssp/csvPesquisaSspCompleta.php");
    }

    public function pesquisaSspParaOsp()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codigoSsp = $parametros[0];

        unset($_POST);
        $_POST['codigoSsp'] = $codigoSsp;

        $this->executarPesquisaOsp();
    }

    //--------------------------OSP
    public function osp()
    {
        $this->controle();

        $this->titulo = "Ordem de Servi�o Preventivo / OSP - Metro Service";
        $this->script = "scriptOsDadosGerais.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/osp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function printOsp()
    {
        $this->titulo = "OSP";

        if(!empty($_SESSION['refillOs']['codigoForm']))
            $_SESSION['refillOs']['codigoOs'] = $_SESSION['refillOs']['codigoForm'];
            require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
            require_once(ABSPATH . "/views/_includes/formularios/osp/printOsp.php");

    }

    public function pesquisaOsp($dadosRefill = null, $returnPesquisa = null)
    {
        $this->controle();

        $this->titulo = "Pesquisa de OSP - Metro Service";
        $this->script = "scriptPesquisaOsp.js";

        $btnAcoes = $this->form->botoesPesquisaOsp();

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/osp/pesquisaOsp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaOsp()
    {
        $pesquisaOspModel = $this->carregaModelo("ccm/osp/pesquisaOsp-model");
        $returnPesquisa = $pesquisaOspModel->getDados();

        $this->pesquisaOsp($_POST, $returnPesquisa);
    }

    public function resetarPesquisaOsp()
    {
        $this->pesquisaOsp();
    }

    public function csvPesquisaOspCompleta()
    {
        $pesquisaOspModel = $this->carregaModelo("ccm/osp/pesquisaOsp-model");
        $returnPesquisa = $pesquisaOspModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/osp/csvPesquisaOspCompleta.php");
    }

    public function csvPesquisaOspMaoObra()
    {
        $pesquisaOspModel = $this->carregaModelo("ccm/osp/pesquisaOsp-model");
        $returnPesquisa = $pesquisaOspModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/osp/csvPesquisaOspMaoObra.php");
    }

    public function csvPesquisaOspMateriais()
    {
        $pesquisaOspModel = $this->carregaModelo("ccm/osp/pesquisaOsp-model");
        $returnPesquisa = $pesquisaOspModel->getDados();

        require_once(ABSPATH . "/views/_includes/formularios/osp/csvPesquisaOspMateriais.php");
    }

    public function pesquisaOspParaSsp()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codigoSsp = $parametros[0];

        unset($_POST);
        $_POST['numeroSsp'] = $codigoSsp;

        $this->executarPesquisaSsp();
    }

    //===========================SSMP==============================//
    public function ssmp()
    {
        if (empty($_SESSION['refillSsmp']['codigoSsmp']))
            header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/dashboardPmp");

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $parametros = $parametros[0];

        $this->controle();

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "scriptSsmp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");

        $arrayResult = $this->form->ssmp($this->dadosUsuario, $parametros, $_SESSION['refillSsmp']['codigoSsmp']);
        $dados = $arrayResult['dados'];
        $btnsAcao = $arrayResult['botoes'];
        $tipo  = $arrayResult['tipo'];
        $nomeTipo = $arrayResult['nomeTipo'];

        require_once(ABSPATH . "/views/_includes/formularios/ssmp/ssmp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

        unset ($_SESSION['refillSsmp']);
    }

    public function printSsmp()
    {
        if (empty($_SESSION['refillSsmp']['codigoSsmp']))
            if(!empty($_SESSION['refillSsmp']['codigoForm']))
                $_SESSION['refillSsmp']['codigoSsmp'] = $_SESSION['refillSsmp']['codigoForm'];
            else
                header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/dashboardPmp");

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $parametros = $parametros[0];

        $this->controle();

        $this->titulo = "SSMP";

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/formularios/ssmp/printSsmp{$parametros}.php");
    }

    public function pesquisaSsmp()
    {
        $this->controle();

        $this->titulo = "Pesquisa de SSMP - Metro Service";
        $this->script = "scriptPesquisaSsmp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/ssmp/pesquisaSSMP.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaSsmp()
    {
        $_SESSION['dadosPesquisaSsmp'] = $_POST;

        $this->carregaModelo("engenharia/ssmp/pesquisaSsmp-model");

        header("Location:" . HOME_URI . "/dashboardGeral/pesquisaSsmp");
    }

    public function resetarPesquisaSsmp()
    {
        unset($_SESSION['dadosPesquisaSsmp']);

        header("Location:" . HOME_URI . "/dashboardGeral/pesquisaSsmp");
    }

    public function csvPesquisaSsmpCompleta()
    {
        $_SESSION['dadosPesquisaSsmp'] = $_POST;

        $this->carregaModelo("engenharia/ssmp/pesquisaSsmp-model");

        require_once(ABSPATH . "/views/_includes/formularios/ssmp/csvPesquisaSsmpCompleta.php");
    }

    //------------------------OSMP------------------------//
    public function osmp()
    {
        if (empty($_SESSION['refillOs']['codigoOs']))
            header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/dashboardPmp");

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $parametros = $parametros[0];

        $this->controle();

        $this->titulo = "Ordem de Servi�o de Manuten��o Programada";
        $this->script = "scriptOsmp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");


        $arrayResult = $this->form->osmp($this->dadosUsuario, $parametros, $_SESSION['refillOs']['codigoOs']);
        $tituloOs = $arrayResult["tituloOs"];
        $actionForm = $arrayResult["actionForm"];
        $acao = $arrayResult["acao"];
        $pag = $arrayResult["pag"];
        $tipoOS = $arrayResult["tipoOS"];
        $dados = $arrayResult["dados"];
        $maoObra = $arrayResult["maoObra"];
        $tempoTotal = $arrayResult["tempoTotal"];
        $regExecucao = $arrayResult["regExecucao"];
        $selectDescStatus = $arrayResult["selectDescStatus"];
        $modal = $arrayResult["modal"];
        $boolean = true;

        require_once(ABSPATH . "/views/_includes/formularios/osmp/dadosGerais.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function printOsmp()
    {
            $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
            $parametros = $parametros[0];

            $_SESSION['refillOs']['form'] = $parametros;

            $this->controle();

            $this->titulo = "OSMP";

            require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
            require_once(ABSPATH . "/views/_includes/formularios/osmp/printOsmp.php");
    }

    public function pesquisaOsmp()
    {
        $this->controle();

        $this->titulo = "Pesquisa de OSMP - Metro Service";
        $this->script = "scriptPesquisaOsmp.js";

        $dadosRefill = $_SESSION['dadosPesquisaOsmp'];
        $btnAcoes = $this->form->botoesPesquisaOsmp();

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/osmp/pesquisaOsmp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

//        $_SESSION['impressaoPesquisaOsmp'] = $_SESSION['resultadoPesquisaOsmp'];
        unset($_SESSION['resultadoPesquisaOsmp']);
    }

    public function executarPesquisaOsmp()
    {
        $_SESSION['dadosPesquisaOsmp'] = $_POST;

        $this->carregaModelo("engenharia/osmp/pesquisaOsmp-model");

        header("Location:" . HOME_URI . "/dashboardGeral/pesquisaOsmp");
    }

    public function resetarPesquisaOsmp()
    {
        unset($_SESSION['dadosPesquisaOsmp']);

        header("Location:" . HOME_URI . "/dashboardGeral/pesquisaOsmp");
    }

    public function csvPesquisaOsmpCompleta()
    {
        $_SESSION['dadosPesquisaOsmp'] = $_POST;

        $this->carregaModelo("engenharia/osmp/pesquisaOsmp-model");

        require_once(ABSPATH . "/views/_includes/formularios/osmp/csvPesquisaOsmpCompleta.php");
    }

    public function csvPesquisaOsmpMaoDeObra()
    {
        $_SESSION['dadosPesquisaOsmp'] = $_POST;

        $this->carregaModelo("engenharia/osmp/pesquisaOsmp-model");

        require_once(ABSPATH . "/views/_includes/formularios/osmp/csvPesquisaOsmpMaoObra.php");
    }

    public function csvPesquisaOsmpMateriais()
    {
        $_SESSION['dadosPesquisaOsmp'] = $_POST;

        $this->carregaModelo("engenharia/osmp/pesquisaOsmp-model");

        require_once(ABSPATH . "/views/_includes/formularios/osmp/csvPesquisaOsmpMateriais.php");
    }

    //===========================Material Rodante==============================//
    public function configComposicao()
    {
        $this->controle();

        $this->titulo = "Configura��o de Composi��o";
        $this->script = "scriptConfigComposicao.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/configComposicao.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function registroOdometroTue()
    {
        $this->controle();

        $this->titulo = "OdometroTue";
        $this->script = "scriptRegistroOdometro.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/registroOdometroTue.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function registroOdometroVlt()
    {
        $this->controle();

        $this->titulo = "OdometroVlt";
        $this->script = "scriptRegistroOdometro.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/registroOdometroVlt.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cadastroPMPMaterialRodante(){
        $this->controle();

        $month = $this::$monthComplete;

        $this->titulo = "Cadastro PMP MR";
        $this->script = "scriptCadastroPMPMaterialRodante.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/cadastroPMPMaterialRodante.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cadastroUsuario(){
        $this->controle();

        $month = $this::$monthComplete;

        $this->titulo = "Cadastro PMP MR";
        $this->script = "scriptCadastroUsuario.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/administrador/cadastroUsuario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function registroDisponibilidade(){
        $this->controle();


        if($_POST){
            $this->medoo->update("veiculo",[
                "disponibilidade" => $_POST["disponibilidade"],

            ],[
                "nome_veiculo" => $_POST["nomeVeiculo"],
            ]);


            $_POST = null;
        }

        $sql = "SELECT DISTINCT ON(frota) frota,
                CASE
                WHEN frota = 'c' THEN 'Cariri'
                WHEN frota = 'F1' THEN 'Fortaleza 1'
                WHEN frota = 'F2' THEN 'Fortaleza 2'
                WHEN frota = 's' THEN 'Sobral'
                WHEN frota = 'T1' THEN 'TUE'
                END
                FROM veiculo
                WHERE frota IS NOT NULL";
        $frotas = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        $this->titulo = "Registro Disponibilidade";
        $this->script = "scriptRegistroDisponibilidade.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/registroDisponibilidade.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function registroFrota(){
        $this->controle();

        if($_POST){
            $this->medoo->update('veiculo', [
                'frota' => $_POST['frota']
            ],[
                'nome_veiculo' => $_POST['nomeVeiculo']
            ]);
        }


        $dadosVeiculos = $this->medoo->select('veiculo', ["[><]linha"=>"cod_linha", "[><]grupo" =>"cod_grupo"],
            '*', ['cod_grupo'=>[22, 23], "ORDER" =>"nome_veiculo"]);

        $this->titulo = "Registro Frota";
        $this->script = "scriptRegistroFrota.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/registroFrota.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function registroProcedimento(){
        $this->controle();

        $this->titulo = "Registro Procedimento";
        $this->script = "scriptRegistroProcedimento.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/registroProcedimento.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function registroFicha(){
        $this->controle();

        $this->titulo = "Registro Ficha";
        $this->script = "scriptRegistroFicha.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/registroFicha.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function consultaISMRTUE()
    {
        $this->controle();

        $this->titulo = "Instru��o de Servi�o TUE";
        $this->script = "scriptISMR.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/instrucaoServicoTUE.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function consultaISMRVLT()
    {
        $this->controle();

        $this->titulo = "Instru��o de Servico VLT";
        $this->script = "scriptISMR.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/instrucaoServicoVLT.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function consultaOcorrencia()
    {

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $ano = $parametros[0];
        $sistemaSul = $parametros[1];
        $sistemaOeste = $parametros[2];
        $sistemaCariri = $parametros[3];
        $sistemaSobral = $parametros[4];
        $sistemaPM = $parametros[5];

        $this->controle();

        $this->titulo = "Ocorr�ncias MR";
        $this->script = "scriptTblOcorrencia.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/ocorrenciaFrotaVeiculo.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function consultaOcorrenciaSistema()
    {

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $ano = $parametros[0];

        $this->controle();

        $this->titulo = "Ocorr�ncias MR";
        $this->script = "scriptTblOcorrenciaSistema.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/materialRodante/ocorrenciaFrotaSistema.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    //===========================Relatorio==============================//
    public function relatorio()
    {

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $relatorio = $parametros[0];

        $this->controle();

        $this->titulo = "Relat�rio";
        $this->script = "relatorios/scriptRelatorioForm.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/relatorios/form.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function relatoriosDiversos()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $relatorio = $parametros[0];

        $this->controle();

        $this->titulo = "Relat�rio";
        $this->script = "relatorios/script{$relatorio}.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/relatorios/rel{$relatorio}.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function gerarRelatorio()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $relatorio = $parametros[0];

        $this->controle();

        $this->titulo = "Relat�rio";
        $this->script = "relatorios/script{$relatorio}.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/relatorios/rel{$relatorio}.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function printRelatorio()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $relatorio = $parametros[0];

        $this->controle();

        $this->titulo = "Relat�rio";
        $this->script = "relatorios/printRelatorio/scriptPrint{$relatorio}.js";

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/_includes/printer/rel{$relatorio}.php");
        require_once(ABSPATH . "/views/_includes/_footerImpressao.php");
    }

//    public function gerarRelatorio()
//    {
//        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
//        $relatorio = $parametros[0];
//
//        $this->controle();
//
//        $this->titulo = "Relat�rio";
//        $this->script = "relatorios/script{$relatorio}.js";
//
////        require_once(ABSPATH . "/views/_includes/_header.php");
//        require_once(ABSPATH . "/views/_includes/relatorios/rel{$relatorio}.php");
////        require_once(ABSPATH . "/views/_includes/_footer.php");
//    }

    //===============Pesquisa Cronograma===============//
    public function pesquisaCronograma()
    {
        $this->controle();

        $this->titulo = "Pesquisa Cronograma";
        $this->script = "scriptPesquisaCronograma.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenharia/cronograma/pesquisaCronograma.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaCronograma()
    {
        $_SESSION['dadosPesquisaCronograma'] = $_POST;

        $this->carregaModelo("engenharia/cronograma/pesquisaCronograma-model");

        header("Location:" . HOME_URI . "/dashboardGeral/pesquisaCronograma");
    }

    public function resetarPesquisaCronograma()
    {
        unset($_SESSION['dadosPesquisaCronograma']);

        header("Location:" . HOME_URI . "/dashboardGeral/pesquisaCronograma");
    }

    //===============Pesquisa Din?mica===============//
    public function pesquisaDinamica()
    {
        $this->controle();

        $this->titulo = "Pesquisa Din�mica";
        $this->script = "scriptPesquisaDinamica.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/pesquisa/pesquisaDinamica.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function levantamentoOcorrencias()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $relatorio = $parametros[0];

        $this->controle();

        $this->titulo = "Relat�rio";
        $this->script = "relatorios/script{$relatorio}.js";

//        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/relatorios/rel{$relatorio}.php");
//        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function controleEstoque()
    {
//        $this->download("estoqueMetroService", ABSPATH."/views/_images/","controleEstoqueMS");
        header("Location:" . HOME_URI . "/views/_images/estoqueMetroService.xlsx");
        echo("<script>
            window.close();
        </script>");
    }

    public function csv()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        require_once(ABSPATH . "/views/_includes/csv/csv" . $parametros[0] . ".php");
    }
}
