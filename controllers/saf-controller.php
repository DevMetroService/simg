<?php

class SAFController extends MainController
{
    private $cod_saf;
    private $statusBlock;
    public $safModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->statusBlock = [1,2,14,27,37];

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $this->cod_saf = $parametros[0];

        $this->safModel = $this->carregaModelo('saf-model');

        $this->titulo = "SAF";

        $this->controle();
    }

    public function create()
    {
        $this->script = "scriptSaf.js";

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once( ABSPATH  . "/views/forms/saf/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $_POST['cod_saf'] = $this->safModel->create();

        if(!in_array($_POST['cod_grupo'], $this->sistemas_fixos) )
        {
            $mrModel = $this->carregaModelo('materialRodanteSaf-model');
            $mrModel->create();
        }

        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function edit()
    {
        $this->script = "scriptSaf.js";

        $saf = $this->medoo->select("v_saf", "*", ["cod_saf" => $this->cod_saf])[0];
        if(in_array($saf['cod_status'], $this->statusBlock) || $this->dadosUsuario['nivel'] != 2)
        {
            $_SESSION['bloqueio'] = true;
            $btnBlock = true;
        }

        $showMR ='none';
        $showBlTl ='none';

        if(in_array($saf['cod_grupo'], [28,27]))
            $showBlTl = 'block';

        if(in_array($saf['cod_grupo'], [22,23]))
            $showMR = 'block';

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once( ABSPATH  . "/views/forms/saf/edit.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function update()
    {
        $codigoStatus = $this->medoo->select('saf', ['[><]status_saf' => 'cod_ssaf'], 'cod_status', ['saf.cod_saf' => (int)$_POST['cod_saf']])[0];
        if ($codigoStatus == 4) {
            $this->safModel->update($this->cod_saf);

            if(!in_array($_POST['cod_grupo'], $this->sistemas_fixos) )
            {
                $mrModel = $this->carregaModelo('materialRodanteSaf-model');
                $mrModel->update();
            }
        }

        $_SESSION['dadosSsm'] = $_POST;

        $ssm = $this->carregaModelo("ssm-model");
        $ssm->gerar();

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);

        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function giveback()
    {
        $this->safModel->alterarStatusSaf($this->cod_saf, 3, date('d-m-Y H:i:s', time()), $this->dadosUsuario, $_POST['descricao'] );

        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function returned()
    {
        $this->script = "scriptSaf.js";

        $saf = $this->medoo->select("v_saf", "*", ["cod_saf" => $this->cod_saf])[0];

        if($this->dadosUsuario['cod_usuario'] != $saf['cod_usuario'])
        {
            $_SESSION['bloqueio'] = true;
            $btnBlock = true;
        }

        $showMR ='none';
        $showBlTl ='none';

        if(in_array($saf['cod_grupo'], [28,27]))
            $showBlTl = 'block';
        else if(in_array($saf['cod_grupo'], [22,23]))
            $showMR = 'block';

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once( ABSPATH  . "/views/forms/saf/returned.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function resend()
    {
        $this->safModel->alterarStatusSaf($this->cod_saf, 4, date('d-m-Y H:i:s', time()), $this->dadosUsuario);

        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function cancel()
    {
        $codSaf = $_POST['cod_saf'];
        $time = date('d-m-Y H:i:s', time());
        
        if($codSaf)
        {
            $ssmModel = $this->carregaModelo('ssm-model');  
            $osmModel = $this->carregaModelo('osm-model');

            foreach($ssmModel->findBySaf($codSaf) as $dados => $valor){
                foreach($osmModel->findBySsm($valor['cod_ssm']) as $data => $value ){
                    $osmModel->alterarStatusOsm($value["cod_osm"], 38, $time, $this->dadosUsuario);
                }
                $ssmModel->alterarStatusSsm($valor['cod_ssm'], 7, $time, $this->dadosUsuario); 
            }

            $this->safModel->alterarStatusSaf($_POST['cod_saf'], 2, $time, $this->dadosUsuario, $_POST['descricao'] );
        }
        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function cancelSaf()
    {
        $this->script = "../forms/saf/hardCancel.js";

        if($this->dadosUsuario['cod_usuario'] != 28)
        {
           $_SESSION['bloqueio'] = true;
        }

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once( ABSPATH  . "/views/forms/saf/cancel.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }
    
    public function cancelCustomSaf()
    {
    $arr = [39524, 39538, 40000, 40265, 40459, 40566, 40771, 40824, 40900, 40962, 40963, 40964, 41047, 41050, 
        41067, 41076, 41094, 41101, 41237, 41619, 41730, 41806, 41873, 41909, 42156, 42281, 
        42473, 42485, 42523, 42657, 42659, 42661, 42665, 42666, 42667, 42668, 42669, 42750, 
        42753, 42882, 42921, 43171, 43220, 43342, 43343, 43494, 43752, 43827, 43839, 43932, 43960, 45096];
        foreach($arr as $dados) {
            $this->safModel->alterarStatusSaf($dados, 2, date('d-m-Y H:i:s', time()), $this->dadosUsuario );
        }
        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
    }

}
