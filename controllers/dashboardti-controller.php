<?php
/**
 * Created by PhpStorm.
 * User: Valmar
 * Date: 11/02/2015
 * Time: 15:35
 */

class DashBoardTiController extends MainController {

    public $loginNecessario = true;
    public $nivelNecessario = "3";

    public function index()
    {
        $this->controleAcesso();


        $_SESSION['direcionamento'] = "TI";
        $_SESSION['navegador'] = "sidebarTI.php";
        $this->script = "dashboardTi.js";

        $this->titulo = "S.I.M.G - Dashboard";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH 	. "/views/ti/dashboard.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function cadastrarUsuario(){

        $this->titulo = "Pesquisa de SSP - Metro Service";
        $this->script = "scriptTi.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ti/cadastrarUsuario.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }



}