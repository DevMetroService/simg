<?php

class EstacaoController extends MainController
{
    protected $estacaoModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->estacaoModel = $this->carregaModelo('estacao-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptEstacao.js";

        $estacao = $this->estacaoModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/estacao/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $this->estacaoModel->create();

        header("Location:{$this->home_uri}/estacao/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codEstacao = $parametros[0];

        $this->estacaoModel->update($codEstacao);

        header("Location:{$this->home_uri}/estacao/create");
    }

}
 ?>
