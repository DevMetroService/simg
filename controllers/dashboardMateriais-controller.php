<?php

class DashboardMateriaisController extends MainController{

/**
* Vari�vel para controlar se a p�gina precisa de login ou n�o.
*
* @var loginNecessario
* @access public
*/
    public $loginNecessario = true;


    public $nivelNecessario = "4";

    public function index() {

        $this->script="dashboardMateriais.js";

        $this->controleAcesso();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $_SESSION['direcionamento'] = "Materiais";
        $_SESSION['navegador'] = "sidebarMateriais.php";

        //$this->navegador = "sidebarMateriais.php";										# Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Dashboard";											# T�tulo da P�gina

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
        ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/materiais/dashboard.php");					# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }

    public function cadastroMaterial() {

        $this->controleAcesso();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";											#
        $this->titulo 		= "S.I.M.G - Cadastro Funcion�rio";							#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/cadastro/"						    # Carrega o respectivo conte�do
                . "material.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");					    # Carrega o footer

    }

    public function cadastroFornecedor() {

        $this->controleAcesso();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";											#
        $this->titulo 		= "S.I.M.G - Cadastro Funcion�rio";
        $this->script="scriptFornecedor.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/cadastro/"		    				# Carrega o respectivo conte�do
            ."fornecedor.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer

    }

    public function cadastroCategoria() {

        $_SESSION['documentacaoFuncionario'] = $_POST;

        $this->controleAcesso();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Cadastro Categoria";							#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/cadastro/"		    				# Carrega o respectivo conte�do
            ."categoria.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer

    }

    public function entradaMaterial() {

        $_SESSION['documentacaoFuncionario'] = $_POST;

        $this->controleAcesso();
        $this->script="scriptEntradaMaterial.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Entrada Material";					    		#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/entrada_material.php");				# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer

    }

    public function cadastroMarcaModal()
    {
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
           $_SESSION['cadastroMarca'] = $_POST;
        }

        $model = $this->carregaModelo('Materiais/cadastroMarca-model');

        echo ('Cadastro realizado com sucesso.');

    }

    public function cadastroCategoriaModal()
    {
        ini_set('display_errors', true);
        error_reporting( ERROR_REPORT_LEVEL );

        if( $_SERVER['REQUEST_METHOD']=='POST')
        {
           $_SESSION['cadastroCategoria'] = $_POST;
        }

        $model = $this->carregaModelo('Materiais/cadastroCategoria-model');

        echo ('sucesso');

    }
    
    public function pesquisaMaterial($dadosRefill = null, $selectMaterial = null){

        $this->controleAcesso();

        $this->titulo = "Almoxarifado - Metro Service / METROFOR";
        $this->script = "scriptPesquisaMaterial.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Entrada Material";					    		#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/pesquisa/pMaterial.php");   		# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }

    public function executarPesquisaMaterial() {
        $_POST['whereSql'] = "cod_material, nome_material, sigla_uni_medida, nome_categoria";
        $pesquisaMaterial = $this->carregaModelo("Materiais/pesquisaMaterial-model");
        $returnPesquisa = $pesquisaMaterial->getDados();

        $this->pesquisaMaterial($_POST, $returnPesquisa);
    }

    public function editarMaterial() {

        if($_POST != null) {
            $_SESSION['dadosMaterial'] =  $_POST;

            $this->carregaModelo("Materiais/alterarMaterial-model");

            $this->pesquisaMaterial();

        }else {
            $this->pesquisaMaterial();
        }
    }

    public function pesquisaFornecedor($dadosRefill = null, $selectFornecedor = null){

        $this->controleAcesso();

        $this->titulo = "Almoxarifado - Metro Service / METROFOR";
        $this->script = "scriptPesquisaFornecedor.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Entrada Material";					    		#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/pesquisa/pFornecedor.php");   		# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }
    
    public function executarPesquisaFornecedor() {
        $_POST['whereSql'] = "nome_fantasia, razao_social, cnpjcpf, avaliacao, descricao, uf";

        $pesquisaFornecedor = $this->carregaModelo("Materiais/pesquisaFornecedor-model");
        $returnPesquisa = $pesquisaFornecedor->getDados();

        $this->pesquisaFornecedor($_POST, $returnPesquisa);
    }

    public function editarFornecedor() {

        if($_POST != null) {

            $_SESSION['dadosFornecedor'] =  $_POST;

            $this->carregaModelo("Materiais/alterarFornecedor-model");

            $this->pesquisaFornecedor();

        }else {
            $this->pesquisaFornecedor();
        }
    }
    
    
    public function pesquisaRequisicaoMateriais(){

        $this->controleAcesso();

        $this->titulo = "Almoxarifado - Metro Service / METROFOR";
        $this->script = "scriptPesquisaRequisicaoMateriais.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Entrada Material";					    		#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/pesquisa/pRequisicaoMaterial.php");   		# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }
    
    
    public function pesquisaPedidoCompra(){

        $this->controleAcesso();

        $this->titulo = "Almoxarifado - Metro Service / METROFOR";
        $this->script = "scriptPesquisaPedidoCompra.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();				#

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador 	= "sidebarMateriais.php";									#
        $this->titulo 		= "S.I.M.G - Entrada Material";					    		#

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
        require_once ( ABSPATH 	. "/views/materiais/pesquisa/pPedidoCompra.php");   		# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }
    
}


?>