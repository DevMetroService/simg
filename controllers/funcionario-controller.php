<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 27/08/2020
 * Time: 15:55
 */

class FuncionarioController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "1";

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);
        $_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";
    }

    public function create()
    {
        $dadosFormulario = $this->dependenciasFormulario();

        $this->controleAcesso();

        $this->script = '../forms/funcionario.js';

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->titulo = "SIMG - Cadastro Funcionario";									# T�tulo da P�gina

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/forms/funcionario/create.php");				# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
    }

    public function update()
    {
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
        $codFuncionario = $parametros[0];

        $_POST['cod_funcionario'] = $codFuncionario;
        
        $funcionarioModel = $this->carregaModelo('funcionario-model');
        $funcionarioEmpresaModel = $this->carregaModelo('funcionarioEmpresa-model');
        $dadosEmpresaModel = $this->carregaModelo('dadosEmpresa-model');
        $dadosContatoModel = $this->carregaModelo('contatoFuncionario-model');
        $dadosEscolaridadeModel = $this->carregaModelo('escolaridadeFuncionario-model');

        $hasDados = $dadosEmpresaModel->getByEmployer($codFuncionario);
        if(!empty($hasDados)) $dadosEmpresaModel->update($codFuncionario); else $dadosEmpresaModel->create();

        $hasContato = $dadosContatoModel->getByEmployer($codFuncionario);
        if(!empty($hasContato)) $dadosContatoModel->update($codFuncionario); else $dadosContatoModel->create();

        $_POST['ef_andamento'] = !empty($_POST['ef_andamento']) ? 's' : 'n';
        $_POST['ef_curso_tecnico'] = !empty($_POST['ef_curso_tecnico']) ? 's' : 'n';
        $hasEscolaridade = $dadosEscolaridadeModel->getByEmployer($codFuncionario);
        if(!empty($hasEscolaridade))$dadosEscolaridadeModel->update($codFuncionario); else $dadosEscolaridadeModel->create();

        $empresa_atual = $funcionarioEmpresaModel->getActualEmployer($codFuncionario);
        if( ($empresa_atual['cod_tipo_funcionario'] == $_POST['cod_tipo_funcionario']) && ($empresa_atual['cod_cargos'] == $_POST['cod_cargos']))
        {
            $funcionarioEmpresaModel->update($empresa_atual['cod_funcionario_empresa']);
        }else{
            $_POST['cod_funcionario_empresa'] = $funcionarioEmpresaModel->create();
        }

        $_POST['ativo'] = !empty($_POST['ativo'])? 's': 'n';
        $funcionarioModel->update($codFuncionario);

        header("Location:" . HOME_URI . "/dashboard/" . $_SESSION['direcionamento']);
    }
    public function store()
    {
        $funcionarioModel = $this->carregaModelo('funcionario-model');
        $funcionarioEmpresaModel = $this->carregaModelo('funcionarioEmpresa-model');
        $dadosEmpresaModel = $this->carregaModelo('dadosEmpresa-model');
        $dadosContatoModel = $this->carregaModelo('contatoFuncionario-model');
        $dadosEscolaridadeModel = $this->carregaModelo('escolaridadeFuncionario-model');

        $_POST['cod_funcionario'] = $funcionarioModel->create();

        $_POST['cod_funcionario_empresa'] = $funcionarioEmpresaModel->create();
        $funcionarioModel->update($_POST['cod_funcionario']);

        $dadosEmpresaModel->create();
        $dadosContatoModel->create();

        $_POST['ef_andamento'] = !empty($_POST['ef_andamento']) ? 's' : 'n';
        $_POST['ef_curso_tecnico'] = !empty($_POST['ef_curso_tecnico']) ? 's' : 'n';
        $dadosEscolaridadeModel->create();

        header("Location:" . HOME_URI . "/dashboard/" . $_SESSION['direcionamento']);
    }

    public function show()
    {
        $this->controleAcesso();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        //TODO::P�gina de visualiza��o
    }

    public function edit()
    {
        $this->controleAcesso();
        $this->script = '../forms/funcionario.js';

        $dadosFormularios = $this->dependenciasFormulario();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();


        $funcionario = $this->carregaModelo('funcionario-model');

        $dadosFuncionario = $funcionario->getFuncionario($parametros[0]);
        $dadosFuncEmpresa = $funcionario->getEmpresa($parametros[0]);
        $dadosEmpresa = $funcionario->getDadosEmpresa($parametros[0]);
        $dadosContato = $funcionario->getContato($parametros[0]);
        $dadosEscolaridade = $funcionario->getEscolaridade($parametros[0]);

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        $this->titulo = "SIMG - Cadastro Funcionario";									# T�tulo da P�gina

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/forms/funcionario/edit.php");				# Carrega o respectivo conte�do

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    function dependenciasFormulario()
    {
        $dadosFormularios['tipo_funcionario'] = $this->medoo->select("tipo_funcionario" , ["cod_tipo_funcionario" , "descricao"]);
        $dadosFormularios['cargos']= $this->medoo->select("cargos_funcionarios", "*", ["ORDER" => "descricao"]);
        $dadosFormularios['escolaridade'] = $this->medoo->select("tipo_escolaridade", "*", ["ORDER" => "cod_tipo_escolaridade"]);
        $dadosFormularios['centro_custo'] = $this->medoo->select("centro_resultado", "*", ["ORDER" => "descricao"]);
        $dadosFormularios['unidade'] = $this->medoo->select("unidade", "*", ["ORDER" => "nome_unidade"]);

        return $dadosFormularios;
    }

    function hasSameCPF()
    {
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
        $cpf = $parametros[0];
        $cpf = preg_replace('/[^0-9]/', '', $cpf);
        
        $result = $this->medoo->query("
            SELECT cpf_cnpj, nome_funcionario
            FROM funcionario 
            WHERE regexp_replace(cpf_cnpj,'[^a-zA-Z0-9]','', 'g') = '{$cpf}'
        ")->fetchAll(PDO::FETCH_ASSOC)[0];
        
        if(!empty($result))
            echo json_encode($result);
    }

}
