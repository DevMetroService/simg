<?php

class UsuarioController extends MainController
{

  public function __construct($parametros = array())
  {
      parent::__construct($parametros);
      $this->controle();

      $this->titulo = "Usu�rios";
  }

  public function create()
  {
    $this->script = "scriptCadastroUsuario.js";

    $validaterCheck = "none";
    if($this->dadosUsuario['nivel'] == '6.2')
      $validaterCheck = "block";

    require_once(ABSPATH . "/views/layout/topLayout.php");
    require_once(ABSPATH . "/views/forms/usuario/usuario.php");
    require_once(ABSPATH . "/views/layout/bottonLayout.php");
  }

  public function store()
  {
    $usuarioModel = $this->carregaModelo('usuario-model');
    $usuarioEquipeModel = $this->carregaModelo('eq_us_unidade-model');

    $_POST['validater'] = !empty($_POST['validater']) ? 's' : 'n';
    $_POST['permissao'] = !empty($_POST['permissao']) ? 's' : 'n';
    $_POST['senha'] = $this->phpass->HashPassword("admin");

    $userId = $usuarioModel->create();

    if (!empty( $_POST['equipes'] )){

      $_POST['cod_usuario'] = $userId;

      foreach($_POST['equipes'] as $key => $value){
        $_POST['cod_un_equipe'] = $value;
        
        $usuarioEquipeModel->create();
      }
    }

    header("Location:{$this->home_uri}/usuario/create");
  }

  public function update()
  {
    $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
    $_POST['cod_usuario'] = $parametros[0];

    $_POST['validater'] = !empty($_POST['validater']) ? 's' : 'n';
    $_POST['permissao'] = !empty($_POST['permissao']) ? 's' : 'n';

    $usuarioModel = $this->carregaModelo('usuario-model');
    $usuarioEquipeModel = $this->carregaModelo('eq_us_unidade-model');

    $usuarioModel->update();
    $usuarioEquipeModel->delete($_POST['cod_usuario']);

    if (!empty( $_POST['equipes'] )){

      foreach($_POST['equipes'] as $key => $value){
        $_POST['cod_un_equipe'] = $value;
        
        $usuarioEquipeModel->create();
      }
    }

    

    header("Location:{$this->home_uri}/usuario/create");
  }

  public function resetarSenha()
  {
    $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
    $cod_usuario = $parametros[0];

    $novaSenha = $this->phpass->HashPassword("admin");

    $usuarioModel = $this->carregaModelo('usuario-model');
    if($usuarioModel->resetSenha($cod_usuario, $novaSenha)[1] == null )
    {
      echo "Senha resetada com sucesso.";
    }else
    {
      echo "Houve um problema ao resetar sua senha.";
    }
  }


  private function hasEmail($id)
  {
    $email = $this->medoo->select("usuario", 'email', ['cod_usuario' => $id])[0];
    
    if (empty($email))
      return false;

    return true;
  }

  private function hasNomeCompleto($id)
  {
    $nome = $this->medoo->select("usuario", 'nome_completo', ['cod_usuario' => $id])[0];
    
    if (empty($nome))
      return false;

    return true;
  }

  public function hasCompleteRegister()
  {
    $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
    $this->codUser = $parametros[0];

    if($this->hasEmail($this->codUser) && $this->hasNomeCompleto($this->codUser))
    {
      echo "true";
      return;
    }

    echo "false";
  }
}
?>