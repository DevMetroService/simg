<?php

class CategoriaController extends MainController
{
  public $nivelNecessario = "4";

  public function __construct($parametros = array())
  {
      parent::__construct($parametros);

      $this->controleAcesso();
  }

  public function create()
  {
    $this->titulo = "SIMG";                                            # T�tulo da P�gina

    require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina
    require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
        . "navegador.php");
    require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body
    require_once(ABSPATH . "/views/forms/categoria/create.php");                 # Carrega o respectivo conte�do
    require_once(ABSPATH . "/views/_includes/_footer.php");                           # Carrega o footer
  }

  public function store()
  {
    $categoriaModel = $this->carregaModelo('categoria-model');

    $categoriaModel->create();

    header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");

  }

  public function edit()
  {
    $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
    $categoria = $parametros[0];

    $this->titulo = "SIMG";

    $dadosCategoria = $this->medoo->select("categoria_material", "*", ["cod_categoria" => $categoria])[0];

    require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina
    require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
        . "navegador.php");
    require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body
    require_once(ABSPATH . "/views/forms/categoria/edit.php");                 # Carrega o respectivo conte�do
    require_once(ABSPATH . "/views/_includes/_footer.php");
  }

  public function update()
  {
    $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
    $categoria = $parametros[0];

    $categoriaModel = $this->carregaModelo('categoria-model');

    $categoriaModel->update($categoria);

    header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
  }
}

 ?>
