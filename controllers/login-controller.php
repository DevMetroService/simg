<?php

/**
 * Controller de login para a aplica��o S.I.M.G
 *
 * @package SimgMVC
 * @since 0.1
 *
 * Este c?digo ? confidencial.
 * A c?pia parcial ou integral de qualquer parte do texto abaixo poder? implicar em encargos judiciais.
 *
 */
class LoginController extends MainController
{

    public function index()
    {                                                                                # Carrega a p?gina "/views/login/index.php"

        $this->titulo = 'Sistema Integrado de Gest�o e da Manunten��o - Metro Service';

        switch ($this->nivelUsuario) {
            case "1":
                //Exemplo para utilizar em todos os n�veis futuramente
                $hasWellcome = $this->vaiParaPagina('dashboard/Rh');
                $_SESSION['direcionamento'] = "Rh";
                $_SESSION['nivelNecessario'] = "1";
                break;

            case "2":
                $hasWellcome = $this->vaiParaPagina('dashboardCCM');
                break;

            case "2.1":
                $hasWellcome = $this->vaiParaPagina('dashboardSupervisao');
                break;

            case "2.3":
                $hasWellcome = $this->vaiParaPagina('dashboardUsuario');
                break;

            case "3":
                $hasWellcome = $this->vaiParaPagina('dashboardTi');
                break;

            case "4":
                //Exemplo para utilizar em todos os n�veis futuramente
                $hasWellcome = $this->vaiParaPagina('dashboard/material');
                $_SESSION['direcionamento'] = "material";
                $_SESSION['nivelNecessario'] = "4";
                break;

            case "5":
                $hasWellcome = $this->vaiParaPagina('dashboardEquipe');
                break;

            case "5.1":
                $hasWellcome = $this->vaiParaPagina('dashboardequipeMr');
                break;

            case "5.2":
                $hasWellcome = $this->vaiParaPagina('dashboardEquipe');
                break;

            case "6":
                $hasWellcome = $this->vaiParaPagina('dashboardDiretoria');
                break;

            case "6.1":
                //Exemplo para utilizar em todos os n�veis futuramente
                $hasWellcome = $this->vaiParaPagina('dashboard/gesiv');
                $_SESSION['direcionamento'] = "gesiv";
                $_SESSION['nivelNecessario'] = "6.1";
                break;

            case "6.2":
                $hasWellcome = $this->vaiParaPagina('dashboard/astig');
                $_SESSION['direcionamento'] = "astig";
                $_SESSION['nivelNecessario'] = "6.2";
                break;

            case "7":
                $hasWellcome = $this->vaiParaPagina('dashboardEngenharia');
                break;

            case "7.1":
                $hasWellcome = $this->vaiParaPagina('dashboardEngenhariaSupervisao');
                break;

            case "7.2":
                $hasWellcome = $this->vaiParaPagina('dashboardEngenhariaMr');
                break;
        }

        if (!empty($hasWellcome)) {
            if ($hasWellcome == "WellCome") {
                require ABSPATH . '/includes/wellcome.php';
            } else {
                echo("<script type='text/javascript'>ir();</script>");
            }
        } else {
            require ABSPATH . '/views/login/login-view.php';
        }

        /**
         * Carrega os arquivos do view *
         */

    }
}
