<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 08/07/2015
 * Time: 08:32
 */

class ModuloOsmController extends MainController
{

    public function salvarDadosGerais(){
        $_SESSION['dadosGeraisOsm'] = $_POST;

        $this->carregaModelo("ccm/osm/salvarDadosGeraisOsm-model");
        header("Location:".HOME_URI."dashboardGeral/osm");
    }

    public function encerrarDadosGerais()
    {
        $_SESSION['dadosGeraisOsm'] = $_POST;

        $this->carregaModelo("ccm/osm/encerrarDadosGeraisOsm-model");
        header("Location:".HOME_URI."dashboard".$_SESSION['direcionamento']);
    }

    // ------- maoObra
    public function maoObra(){
        $this->pagina = "Mão de Obra";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaoObra.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/maoDeObra.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaoObra(){
        $_SESSION['dadosMaoObra'] = $_POST;

        $this->carregaModelo("ccm/osm/modulos/maoObraOsm-model");
        header("Location:".HOME_URI."/moduloOsm/maoObra");
    }

    // ------- materialUtilizado
    public function materialUtilizado(){
        $this->pagina = "Material Utilizado";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaterialUtilizado.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/materiaisUtilizados.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaterialUtilizado(){
        $_SESSION['dadosMateriais'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['materialCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['materialCheck'] = false;
        }

        $this->carregaModelo('ccm/osm/modulos/materialUtilizado-model');
        header("Location:".HOME_URI."/moduloOsm/materialUtilizado");
    }

    // ------- maquinaUtilizada
    public function maquinaUtilizada(){
        $this->pagina = "Maquinas e Equiepamentos Utilizados";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaquinaUtilizada.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/maquinasUtilizadas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaquinaUtilizada(){
        $_SESSION['dadosMaquina'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['maquinaCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['maquinaCheck'] = false;
        }

        $this->carregaModelo('ccm/osm/modulos/maquinaUtilizada-model');
        header("Location:".HOME_URI."/moduloOsm/maquinaUtilizada");
    }

    // ------- registroExecucao
    public function registroExecucao(){
        $this->pagina = "Registro de Execução";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsRegistroExecucao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/registroExecucao.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarRegistroExecucao(){
        $_SESSION['registroExecucao'] = $_POST;

        $this->carregaModelo('ccm/osm/modulos/registroExecucao-model');
        header("Location:".HOME_URI."/moduloOsm/registroExecucao");
    }

    // ------- manobrasEletricas
    public function manobrasEletricas(){
        $this->pagina = "Manobras Elétricas";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsManobrasEletricas.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/manobrasEletricas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarManobrasEletricas(){
        $_SESSION['dadosManobra'] = $_POST;

        $this->carregaModelo('ccm/osm/modulos/manobraEletrica-model');
        header("Location:".HOME_URI."/moduloOsm/manobrasEletricas");
    }

    // ------- tempoTotal
    public function tempoTotal(){
        $this->pagina = "Tempos Totais";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsTemposTotais.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/temposTotais.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarTemposTotais(){
        $_SESSION['dadosTemposTotais'] = $_POST;

        $this->carregaModelo('ccm/osm/modulos/temposTotais-model');
        header("Location:".HOME_URI."/moduloOsm/tempoTotal");
    }

    // ------- encerramento
    public function encerramento(){
        $this->pagina ="Encerramento";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsmEncerramento.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osm/modulos/encerramento.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function hasManobraEntrada($codSsm){
        $allOs = $this->medoo->select('v_osm','*', ['cod_ssm' => $codSsm]);

        foreach($allOs as $dados){
            if($dados['nome_servico'] == 'MANOBRA DE ENTRADA'){
                return true;
            }
        }
        return false;
    }

    public function salvarEncerramento(){
        $_SESSION['dadosEncerramento'] = $_POST;

        unset($_SESSION['dadosMaquinaOs']);
        unset($_SESSION['dadosMaterialOs']);

        $this->carregaModelo('ccm/osm/modulos/encerramento-model');

        header("Location:".HOME_URI."/dashboard".$_SESSION['direcionamento']);
    }

    function imprimirOption($tempo){
        switch($tempo){
            case 5:
                echo('<option value="">__________</option>');
                echo('<option value="5" selected>5 Minutos</option>');
                echo('<option value="15" >15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 15:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15" selected>15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 30:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30" selected>30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 60:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60" selected>1 Hora</option>');
                break;

            default:
                echo('<option value="" selected>__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;
        }

    }

}