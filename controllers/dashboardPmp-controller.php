<?php

/**
 * Created by PhpStorm.
 * User: Ricardo
 * Date: 14/10/2015
 * Time: 08:58
 */
class DashBoardPmpController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "0";

    public function __construct()
    {
        parent::__construct();
        $this->paginaDashboard = "pmp";
    }

    public function controle()
    {
        switch (strtolower($_SESSION['direcionamento'])) {
            case "RH":
                $this->nivelNecessario = "1";
                break;

            case "ccm":
                $this->nivelNecessario = "2";
                break;

            case "supervisao":
                $this->nivelNecessario = "2.1";
                break;

            case "usuario":
                $this->nivelNecessario = "2.3";
                break;

            case "ti":
                $this->nivelNecessario = "3";
                break;

            case "materiais":
                $this->nivelNecessario = "4";
                break;

            case "equipe":
                $this->nivelNecessario = "5";
                break;

            case "diretoria":
                $this->nivelNecessario = "6";
                break;

            case "engenharia":
                $this->nivelNecessario = "7";
                break;

            case "engenhariasupervisao":
                $this->nivelNecessario = "7.1";
                break;
        }

        $this->controleAcesso();
    }

    //===================================Procedimento===================================//
    public function procedimento()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['tipoGrupo'] = $parametros[0];

        $this->controle();
        $this->script = "procedimentoPmp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/procedimento.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    //==================================Pmp Edifica��o==================================//
    public function formularioEdificacao()
    {
        $this->controle();
        $this->script = "scriptPmpEd.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/edificacao/formularioEdificacao.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfEd()
    {
        //require_once(ABSPATH . "/views/_includes/_header.php");
        //require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ed/pdfEdificacao.php");
        //require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfEdQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

//        require_once(ABSPATH . "/views/_includes/_header.php");
//        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ed/pdfEdificacaoQzn.php");
//        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfEdCrono()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ed/pdfEdCrono.php");
    }

    public function pdfEdCronoSimples()
    {
//        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
//        $_SESSION['mes'] = $parametros[0];
//        $_SESSION['linha'] = $parametros[1];
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ed/pdfEdCronoSimples.php");
    }

    public function pesquisaEdificacao()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpEd.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/edificacao/pesquisaPmpEd.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpEd()
    {
        $_SESSION['dadosPesquisaPmpEd'] = $_POST;

        $this->carregaModelo("engenharia/pmpEdificacao/pesquisaPmpEd-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaEdificacao");
    }

    public function resetarPesquisaPmpEd()
    {
        unset($_SESSION['dadosPesquisaPmpEd']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaEdificacao");
    }

    public function depreciarPmpEd()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpEdificacao/pesquisaPmpEd-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaEdificacao");
    }

    public function gerarPmpAnualEd()
    {
        $this->carregaModelo("engenharia/pmpEdificacao/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    //==================================Pmp Rede A�rea==================================//
    public function formularioRedeAerea()
    {
        $this->controle();
        $this->script = "scriptPmpRa.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/redeAerea/formularioRedeAerea.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfRa()
    {
        //require_once(ABSPATH . "/views/_includes/_header.php");
        //require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ra/pdfRedeAerea.php");
        //require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfRaQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

//        require_once(ABSPATH . "/views/_includes/_header.php");
//        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ra/pdfRedeAereaQzn.php");
//        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfRaCrono()
    {
//        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
//        $_SESSION['mes'] = $parametros[0];
//        $_SESSION['linha'] = $parametros[1];
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ra/pdfRaCrono.php");
    }

    public function pdfRaCronoSimples()
    {
//        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
//        $_SESSION['mes'] = $parametros[0];
//        $_SESSION['linha'] = $parametros[1];
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/ra/pdfRaCronoSimples.php");
    }

    public function pesquisaRedeAerea()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpRa.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/redeAerea/pesquisaPmpRa.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpRa()
    {
        $_SESSION['dadosPesquisaPmpRa'] = $_POST;

        $this->carregaModelo("engenharia/pmpRedeAerea/pesquisaPmpRa-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaRedeAerea");
    }

    public function resetarPesquisaPmpRa()
    {
        unset($_SESSION['dadosPesquisaPmpRa']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaRedeAerea");
    }

    public function depreciarPmpRa()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpRedeAerea/pesquisaPmpRa-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaRedeAerea");
    }

    public function gerarPmpAnualRa()
    {
        $this->carregaModelo("engenharia/pmpRedeAerea/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    //==================================Pmp Subesta��o====================================//
    public function formularioSubestacao()
    {
        $this->controle();
        $this->script = "scriptPmpSb.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/subestacao/formularioSubestacao.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfSb()
    {
        //require_once(ABSPATH . "/views/_includes/_header.php");
        //require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/sb/pdfSubestacao.php");
        //require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfSbQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

//        require_once(ABSPATH . "/views/_includes/_header.php");
//        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/sb/pdfSubestacaoQzn.php");
//        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfSuCrono()
    {
//        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
//        $_SESSION['mes'] = $parametros[0];
//        $_SESSION['linha'] = $parametros[1];
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/sb/pdfSuCrono.php");
    }

    public function pdfSuCronoSimples()
    {
//        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
//        $_SESSION['mes'] = $parametros[0];
//        $_SESSION['linha'] = $parametros[1];
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/sb/pdfSuCronoSimples.php");
    }

    public function pesquisaSubestacao()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpSb.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/subestacao/pesquisaPmpSb.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpSb()
    {
        $_SESSION['dadosPesquisaPmpSb'] = $_POST;

        $this->carregaModelo("engenharia/pmpSubestacao/pesquisaPmpSb-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaSubestacao");
    }

    public function resetarPesquisaPmpSb()
    {
        unset($_SESSION['dadosPesquisaPmpSb']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaSubestacao");
    }

    public function depreciarPmpSb()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpSubestacao/pesquisaPmpSb-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaSubestacao");
    }

    public function gerarPmpAnualSb()
    {
        $this->carregaModelo("engenharia/pmpSubestacao/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    //================================Pmp Via Permanente===================================//
    public function formularioViaPermanente()
    {
        $this->controle();
        $this->script = "scriptPmpVp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/viaPermanente/formularioViaPermanente.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfVp()
    {
        //require_once(ABSPATH . "/views/_includes/_header.php");
        //require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/pmp/vp/pdfViaPermanente.php");
        //require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfVpQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

        require_once(ABSPATH . "/views/_includes/formularios/pmp/vp/pdfViaPermanenteQzn.php");
    }

    public function pdfVpCrono()
    {
        $_SESSION['modal'] = $_POST;

        require_once(ABSPATH . "/views/_includes/formularios/pmp/vp/pdfVpCrono.php");
    }

    public function pdfVpCronoSimples()
    {
        $_SESSION['modal'] = $_POST;

        require_once(ABSPATH . "/views/_includes/formularios/pmp/vp/pdfVpCronoSimples.php");
    }

    public function pesquisaViaPermanente()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpVp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/viaPermanente/pesquisaPmpVp.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpVp()
    {
        $_SESSION['dadosPesquisaPmpVp'] = $_POST;

        $this->carregaModelo("engenharia/pmpViaPermanente/pesquisaPmpVp-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaViaPermanente");
    }

    public function resetarPesquisaPmpVp()
    {
        unset($_SESSION['dadosPesquisaPmpVp']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaViaPermanente");
    }

    public function depreciarPmpVp()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpViaPermanente/pesquisaPmpVp-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaViaPermanente");
    }

    public function gerarPmpAnualVp()
    {
        $this->carregaModelo("engenharia/pmpViaPermanente/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    //==================================Pmp Telecom==================================//
    public function formularioTelecom()
    {
        $this->controle();
        $this->script = "scriptPmpTl.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/telecom/formularioTelecom.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfTl()
    {
        require_once(ABSPATH . "/views/_includes/formularios/pmp/tl/pdfTelecom.php");
    }

    public function pdfTlQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

        require_once(ABSPATH . "/views/_includes/formularios/pmp/tl/pdfTelecomQzn.php");
    }

    public function pdfTlCrono()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/tl/pdfTlCrono.php");
    }

    public function pdfTlCronoSimples()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/tl/pdfTlCronoSimples.php");
    }

    public function pesquisaTelecom()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpTl.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/telecom/pesquisaPmpTl.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpTl()
    {
        $_SESSION['dadosPesquisaPmpTl'] = $_POST;

        $this->carregaModelo("engenharia/pmpTelecom/pesquisaPmpTl-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaTelecom");
    }

    public function resetarPesquisaPmpTl()
    {
        unset($_SESSION['dadosPesquisaPmpTl']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaTelecom");
    }

    public function depreciarPmpTl()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpTelecom/pesquisaPmpTl-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaTelecom");
    }

    public function gerarPmpAnualTl()
    {
        $this->carregaModelo("engenharia/pmpTelecom/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    //==================================Pmp Bilhetagem==================================//
    public function formularioBilhetagem()
    {
        $this->controle();
        $this->script = "scriptPmpBl.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/bilhetagem/formularioBilhetagem.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfBl()
    {
        require_once(ABSPATH . "/views/_includes/formularios/pmp/bl/pdfBilhetagem.php");
    }

    public function pdfBlQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

        require_once(ABSPATH . "/views/_includes/formularios/pmp/bl/pdfBilhetagemQzn.php");
    }

    public function pdfBlCrono()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/bl/pdfBlCrono.php");
    }

    public function pdfBlCronoSimples()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/bl/pdfBlCronoSimples.php");
    }

    public function pesquisaBilhetagem()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpBl.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/bilhetagem/pesquisaPmpBl.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpBl()
    {
        $_SESSION['dadosPesquisaPmpBl'] = $_POST;

        $this->carregaModelo("engenharia/pmpBilhetagem/pesquisaPmpBl-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaBilhetagem");
    }

    public function resetarPesquisaPmpBl()
    {
        unset($_SESSION['dadosPesquisaPmpBl']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaBilhetagem");
    }

    public function depreciarPmpBl()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpBilhetagem/pesquisaPmpBl-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaBilhetagem");
    }

    public function gerarPmpAnualBl()
    {
        $this->carregaModelo("engenharia/pmpBilhetagem/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    //=================================Material Rodante======================================//
    public function formularioMaterialRodante()
    {
        $this->controle();
        $this->script = "scriptPmpMr.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/materialRodante/formularioMaterialRodante.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfVlt()
    {
        require_once(ABSPATH . "/views/_includes/formularios/pmp/mr/pdfMrVlt.php");
    }

    public function pdfVltQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];

        require_once(ABSPATH . "/views/_includes/formularios/pmp/mr/pdfMrVltQzn.php");
    }

    public function pdfVltCrono()
    {
        $_SESSION['modal'] = $_POST;

        require_once(ABSPATH . "/views/_includes/formularios/pmp/mr/pdfVltCrono.php");
    }

    public function pdfVltCronoSimples()
    {
        $_SESSION['modal'] = $_POST;

        require_once(ABSPATH . "/views/_includes/formularios/pmp/mr/pdfVltCronoSimples.php");
    }

    public function pesquisaMrVlt()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpVp.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/materialRodante/pesquisaPmpVlt.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpVlt()
    {
        $_SESSION['dadosPesquisaPmpVp'] = $_POST;

        $this->carregaModelo("engenharia/pmpViaPermanente/pesquisaPmpVp-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaViaPermanente");
    }

    public function resetarPesquisaPmpVlt()
    {
        unset($_SESSION['dadosPesquisaPmpVp']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaMrVlt");
    }

    public function depreciarPmpVlt()
    {
        //$this->carregaModelo("engenharia/pmpViaPermanente/depreciarPmpVp-model");

        //$this->carregaModelo("engenharia/pmpViaPermanente/pesquisaPmpVp-model");

        ///header("Location:" . HOME_URI . "/dashboardPmp/pesquisaViaPermanente");
    }

    public function pdfMr()
    {
        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function pdfMrQzn()
    {
        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function pesquisaMaterialRodante()
    {
        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);

    }

    public function executarPesquisaPmpMr()
    {
        $_SESSION['dadosPesquisaPmpMr'] = $_POST;

        $this->carregaModelo("engenharia/pmpMaterialRodante/pesquisaPmpMr-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaMaterialRodante");
    }

    public function resetarPesquisaPmpMr()
    {
        unset($_SESSION['dadosPesquisaPmpMr']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaMaterialRodante");
    }

    public function depreciarPmpMr()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpMaterialRodante/pesquisaPmpMr-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaMaterialRodante");
    }

    //==================================Pmp Jardins e �reas Verdes==================================//
    public function formularioJardins()
    {
        $this->controle();
        $this->script = "scriptPmpJd.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/jardins/formularioJardins.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function pdfJd()
    {
        require_once(ABSPATH . "/views/_includes/formularios/pmp/jd/pdfJardins.php");
    }

    public function pdfJdQzn()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['qzn'] = $parametros[0];
        
        require_once(ABSPATH . "/views/_includes/formularios/pmp/jd/pdfJardinsQzn.php");
    }

    public function pdfJdCrono()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/jd/pdfJdCrono.php");
    }

    public function pdfJdCronoSimples()
    {
        $_SESSION['modal'] = $_POST;
        require_once(ABSPATH . "/views/_includes/formularios/pmp/jd/pdfJdCronoSimples.php");
    }

    public function pesquisaJardins()
    {
        $this->controle();
        $this->script = "scriptPesquisaPmpJd.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/".strtolower($_SESSION['direcionamento'])."/jardins/pesquisaPmpJd.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaPmpJd()
    {
        $_SESSION['dadosPesquisaPmpJd'] = $_POST;

        $this->carregaModelo("engenharia/pmpJardins/pesquisaPmpJd-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaJardins");
    }

    public function resetarPesquisaPmpJd()
    {
        unset($_SESSION['dadosPesquisaPmpJd']);

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaJardins");
    }

    public function depreciarPmpJd()
    {
        $_SESSION['dadosPmp'] = $_SESSION['refillPmp'];

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->depreciarAtivarDeletar();

        $this->carregaModelo("engenharia/pmpJardins/pesquisaPmpJd-model");

        header("Location:" . HOME_URI . "/dashboardPmp/pesquisaJardins");
    }

    public function gerarPmpAnualJd()
    {
        $this->carregaModelo("engenharia/pmpJardins/gerarPmpAnual-model");

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }
}
