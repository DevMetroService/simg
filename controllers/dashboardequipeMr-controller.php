<?php

class DashboardEquipeMrController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = ["5.1"];

    public $ctdA = 0;
    public $ctdB = 0;
    public $ctdC = 0;

    public $equipeUsuario;

    public function index()
    {
        $_SESSION['equipeUsuario'] = $this->medoo->select('eq_us_unidade', ["[><]un_equipe" => "cod_un_equipe", "[><]equipe" => "cod_equipe", "[><]unidade" => "cod_unidade"], ['sigla', 'nome_equipe', 'sigla_unidade', 'cod_unidade', 'nome_unidade'], ['cod_usuario' => (int)$this->dadosUsuario['cod_usuario']]);

        $this->equipeUsuario = $this->medoo->select('eq_us_unidade', ["[><]un_equipe" => "cod_un_equipe"], 'cod_un_equipe', ['cod_usuario' => (int)$this->dadosUsuario['cod_usuario']]);
        $this->equipeUsuario;

        $this->controleAcesso();

        $_SESSION['direcionamento'] = "equipeMr";
        $this->paginaDashboard = "correProg";

        $this->titulo = "Sistema de Manutenção - Metro Service / METROFOR";
        $this->script = "dashboardEquipe.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/equipemr/dashboardEquipeMr.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function correProg(){
        $this->index();
    }

}
