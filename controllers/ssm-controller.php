<?php

class SSMController extends MainController
{
    private $codSsm;
    private $ssmModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $this->codSsm = $parametros[0];

        $this->ssm = $this->carregaModelo("ssm-model");

        $this->titulo = "SSM";

        $this->controle();
    }

    public function edit()
    {
        $this->script = "scriptSsm.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");

        $arrayResult = $this->form->ssm($this->dadosUsuario, $this->codSsm);
        $ssm = $arrayResult['dadosSsm'];
        $saf = $arrayResult['dadosSaf'];
        $btnsAcao = $arrayResult['botoes'];
        $historico = $arrayResult['historico'];
        $dataEncaminhamento = $arrayResult['dataEncaminhamento'];

        require_once(ABSPATH . "/views/_includes/formularios/ssm/ssm.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function update()
    {

    }

    public function store()
    {
      $_SESSION['dadosSsm'] = $_POST;
      $_SESSION['dadosSaf'] = $_POST;

      $saf = $this->carregaModelo("saf-model");

      $saf->alterar();   
      $this->ssm->gerar();
      
      header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

//=========CONTROLE DE PEND�NCIA DE MATERIAL N�O CONTRATUAL
    public function aprovarCompraMaterial()
    {
      $ssm = $this->carregaModelo('ssm-model');

      $ssm->alterarStatus($this->codSsm, 42, true, 41);

      //Envio de e-mail de notifica��o
      $dadosSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => $this->codSsm])[0];
      $os = $this->medoo->select('osm_falha', 'cod_osm', ['cod_ssm' => $this->codSsm, "ORDER" => ["cod_osm" => "DESC"] ])[0];
      $codUsuarios = $this->medoo->select("eq_us_unidade", "cod_usuario", ["cod_un_equipe" => $dadosSsm['cod_un_equipe']]);
      $emails = $this->medoo->select('usuario', 'email', ['cod_usuario' => $codUsuarios ]);
      Mailer::sendEmail($emails, 'acceptPendencyMaterialService', 
      [
        'cod_ssm' => $this->codSsm,
        'nome_status' => "Aguardando Material" ,
        'condicao' => "Compra Aprovada",
        'cod_osm' => $os
      ]);
      
      if($_SESSION['direcionamento'] == 'gesiv')
        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
      else
        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function reprovarCompraMaterial()
    {

      $extraMails = ["fernando.donadelli@metroservice.eng.br",
      "karolina.carvalho@metroservice.eng.br",
      "everton.rocha@metroservice.eng.br",
      "rebeca.cabral@metroservice.eng.br"];

      $ssm = $this->carregaModelo('ssm-model');

      $ssm->alterarStatus($this->codSsm, 43, true, 41, $_POST['descricao']);

      //Envio de e-mail de notifica��o
      $dadosSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => $this->codSsm])[0];
      $os = $this->medoo->select('osm_falha', 'cod_osm', ['cod_ssm' => $this->codSsm, "ORDER" => ["cod_osm" => "DESC"] ])[0];
      $codUsuarios = $this->medoo->select("eq_us_unidade", "cod_usuario", ["cod_un_equipe" => $dadosSsm['cod_un_equipe']]);
      $emails = $this->medoo->select('usuario', 'email', ['cod_usuario' => $codUsuarios ]);

      $emails = array_merge($emails , $extraMails);
      
      Mailer::sendEmail($emails, 'refusePendencyMaterialService', 
      [
        'cod_ssm' => $this->codSsm,
        'nome_status' => 'Pend�ncia (Compra n�o Aprovada)',
        'condicao' => "Compra N�o Aprovada",
        'descricao_status' => $dadosSsm['descricao_status'],
        'cod_osm' => $os
      ]);

      if($this->nivelUsuario == 6.1 ||  $this->nivelUsuario == 4)
        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
      else
        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }
//==================

//=========CONTROLE DE SOLICITA��O DE ALTERA��O DE N�VEL
    public function solicitarAlterNivel()
    {
      $alterNivel = $this->carregaModelo('SSM/alterNivel-model');

      $alterNivel->create();

      if($this->nivelUsuario == 6.1 ||  $this->nivelUsuario == 4)
        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
      else
        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");

    }

    public function aprovarAlterNivel()
    {
      $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
      $codReg = $parametros[0];

      $alterNivel = $this->carregaModelo('SSM/alterNivel-model');

      $alterNivel->aprove(45, $codReg);

      header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
    }

    public function reprovarAlterNivel()
    {
      $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
      $codReg = $parametros[0];

      $alterNivel = $this->carregaModelo('SSM/alterNivel-model');

      $alterNivel->refuse( 46, $codReg);

      header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
    }
//===================

//=========CONSUMO DE API
    public function hasSolicitation()
    {
      $alterNivel = $this->carregaModelo('SSM/alterNivel-model');

      $result = $alterNivel->getBySsm($this->codSsm, 44);

      if(!empty($result))
        echo true;
      else
        echo 0;

    }
    
    public function aproveCompraMaterial()
    {
      $ssm = $this->carregaModelo('ssm-model');

      $ssm->alterarStatus($this->codSsm, 42, true, 41);
    }

    public function sendEmailAprove()
    {
      //Envio de e-mail de notifica��o
      $dadosSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => $this->codSsm])[0];
      $os = $this->medoo->select('osm_falha', 'cod_osm', ['cod_ssm' => $this->codSsm, "ORDER" => ["cod_osm" => "DESC"] ])[0];
      $codUsuarios = $this->medoo->select("eq_us_unidade", "cod_usuario", ["cod_un_equipe" => $dadosSsm['cod_un_equipe']]);
      $codUsuarios = implode(',',$codUsuarios);
      $sql = "SELECT distinct on (email) email FROM usuario WHERE cod_usuario IN ({$codUsuarios})";
      $emails = $this->medoo->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0);

      Mailer::sendEmail($emails, 'acceptPendencyMaterialService', 
      [
        'cod_ssm' => $this->codSsm,
        'nome_status' => "Aguardando Material" ,
        'condicao' => "Compra Aprovada",
        'cod_osm' => $os
      ]);
    }

    public function sendEmailDesaprove()
    {
      $extraMails = ["fernando.donadelli@metroservice.eng.br",
      "karolina.carvalho@metroservice.eng.br",
      "everton.rocha@metroservice.eng.br",
      "rebeca.cabral@metroservice.eng.br"];

      //Envio de e-mail de notifica��o
      $dadosSsm = $this->medoo->select("v_ssm", "*", ["cod_ssm" => $this->codSsm])[0];
      $os = $this->medoo->select('osm_falha', 'cod_osm', ['cod_ssm' => $this->codSsm, "ORDER" => ["cod_osm" => "DESC"] ])[0];
      $codUsuarios = $this->medoo->select("eq_us_unidade", "cod_usuario", ["cod_un_equipe" => $dadosSsm['cod_un_equipe']]);
      $codUsuarios = implode(',',$codUsuarios);
      $sql = "SELECT distinct on (email) email FROM usuario WHERE cod_usuario IN ({$codUsuarios})";
      $emails = $this->medoo->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0);
      $emails = array_merge($emails , $extraMails);

      Mailer::sendEmail($emails, 'refusePendencyMaterialService', 
      [
        'cod_ssm' => $this->codSsm,
        'nome_status' => "Aguardando Material" ,
        'condicao' => "Compra Aprovada",
        'descricao_status' => $dadosSsm['descricao_status'],
        'cod_osm' => $os
      ]);
    }

    public function getMaterialSolicitacao()
    {
      $materiais = $this->medoo->query
      ("
          SELECT max(cod_osm), valor_unitario, nome_material, quantidade, material.descricao
          FROM osm_material_solicitado
          JOIN osm_falha USING(cod_osm)
          JOIN material USING(cod_material)
          WHERE cod_ssm = {$this->codSsm}
          GROUP BY cod_osm, valor_unitario, nome_material, quantidade, material.descricao
      ")->fetchAll(PDO::FETCH_ASSOC);

      $qtdMateriais = !empty($materiais) ?  count($materiais): 0;
      $contentPopover = "";

      foreach($materiais as $line => $name)
      {
          $contentPopover .= "-> ". htmlspecialchars("{$name['nome_material']}-{$name['descricao']}</br>");
      }

      //Prepara o bot�o para exibi��o
      $button = "<button tabindex='0' class='btn btn-block btn-default clickPop text-center' role='button'
      data-toggle='popover'
      data-placement='top'
      data-trigger='focus'
      data-html='true'
      title='Materiais'
      data-content=\"{$contentPopover}\">
          {$qtdMateriais}
      </button>";

      echo json_encode(['button' => utf8_encode($button), 'days'=> 0]);

      // //Calcula diferen�a de datas
      // $data_inicio = new DateTime($_GET['time']);
      // $data_fim = new DateTime(date('d-m-Y H:i:s', time()));
      // // Resgata diferen�a entre as datas
      // $dateInterval = $data_inicio->diff($data_fim);

      //Returna informa��o
      // echo json_encode(['button' => utf8_encode($button), 'days'=> $dateInterval->days]);
    }

    public function repCompraMaterial()
    {
      $ssm = $this->carregaModelo('ssm-model');

      $ssm->alterarStatus($this->codSsm, 43, true, 41, $_GET['justificativa']);
    }

    public function aproveValidation()
    {
      $ssm = $this->carregaModelo('ssm-model');
      
      $justify = $_GET['justify'];
      $cod_status = $_GET['cod_status'];

      $ssm->aprovarReproveWithId($this->codSsm, $cod_status, $justify);
    }
//===================
}
 ?>
