<?php
/**
 * Created by PhpStorm.
 * User: iramar.junior
 * Date: 21/12/2016
 * Time: 17:10
 */

class ModuloOsmpController extends MainController
{
    public function salvarDadosGerais(){
        $_SESSION['dadosGeraisOsmp'] = $_POST;

        $this->carregaModelo("engenharia/osmp/salvarDadosGeraisOsmp-model");
        header("Location:".HOME_URI."/dashboardGeral/osmp/{$_POST['form']}");
    }

    // ------- maoObra
    public function maoObra(){
        $this->pagina = "Mão de Obra";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaoObra.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/maoDeObra.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaoObra(){
        $_SESSION['dadosMaoObra'] = $_POST;

        $this->carregaModelo("engenharia/osmp/modulos/maoObra-model");
        header("Location:".HOME_URI."/moduloOsmp/maoObra");
    }

    // ------- materialUtilizado
    public function materialUtilizado(){
        $this->pagina = "Material Utilizado";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaterialUtilizado.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/materiaisUtilizados.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaterialUtilizado(){
        $_SESSION['dadosMateriais'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['materialCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['materialCheck'] = false;
        }

        $this->carregaModelo('engenharia/osmp/modulos/materialUtilizado-model');
        header("Location:".HOME_URI."/moduloOsmp/materialUtilizado");
    }

    // ------- maquinaUtilizada
    public function maquinaUtilizada(){
        $this->pagina = "Maquinas e Equiepamentos Utilizados";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaquinaUtilizada.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/maquinasUtilizadas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaquinaUtilizada(){
        $_SESSION['dadosMaquina'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['maquinaCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['maquinaCheck'] = false;
        }

        $this->carregaModelo('engenharia/osmp/modulos/maquinaUtilizada-model');
        header("Location:".HOME_URI."/moduloOsmp/maquinaUtilizada");
    }

    // ------- registroExecucao
    public function registroExecucao(){
        $this->pagina = "Registro de Execução";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsRegistroExecucao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/registroExecucao.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarRegistroExecucao(){
        $_SESSION['registroExecucao'] = $_POST;

        $this->carregaModelo('engenharia/osmp/modulos/registroExecucao-model');
        header("Location:".HOME_URI."/moduloOsmp/registroExecucao");
    }

    // ------- manobrasEletricas
    public function manobrasEletricas(){
        $this->pagina = "Manobras Elétricas";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsManobrasEletricas.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/manobrasEletricas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarManobrasEletricas(){
        $_SESSION['dadosManobra'] = $_POST;

        $this->carregaModelo('engenharia/osmp/modulos/manobraEletrica-model');
        header("Location:".HOME_URI."/moduloOsmp/manobrasEletricas");
    }

    // ------- tempoTotal
    public function tempoTotal(){
        $this->pagina = "Tempos Totais";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsTemposTotais.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/temposTotais.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarTemposTotais(){
        $_SESSION['dadosTemposTotais'] = $_POST;

        $this->carregaModelo('engenharia/osmp/modulos/temposTotais-model');
        header("Location:".HOME_URI."/moduloOsmp/tempoTotal");
    }

    // ------- encerramento
    public function hasManobraEntrada($codSsm){
        return false;
    }


    public function encerramento(){
        $this->pagina ="Encerramento";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOspEncerramento.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/equipe/osmp/modulos/encerramento.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarEncerramento(){
        $_SESSION['dadosEncerramento'] = $_POST;

        $this->carregaModelo('engenharia/osmp/modulos/encerramento-model');
        header("Location:".HOME_URI."/dashboard".$_SESSION['direcionamento']."/dashboardPmp");
    }

    function imprimirOption($tempo){
        switch($tempo){
            case 5:
                echo('<option value="">__________</option>');
                echo('<option value="5" selected>5 Minutos</option>');
                echo('<option value="15" >15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 15:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15" selected>15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 30:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30" selected>30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 60:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60" selected>1 Hora</option>');
                break;

            default:
                echo('<option value="" selected>__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;
        }
    }
}
