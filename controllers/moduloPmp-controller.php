<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 17/12/2015
 * Time: 14:19
 */

class ModuloPmpController extends MainController
{
    /**
     *
     */

    //Cadastro de SAF's
    public function index()
    {

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina = "M�o de Obra";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos"                                  # Carrega o respectivo conte�do
            ."/maoDeObra.php");

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");

    }

    public function materialUtilizado(){
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina = "Material Utilizado";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos"                                  # Carrega o respectivo conte�do
            ."/materiaisUtilizados.php");

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function maquinaUtilizada(){
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina = "Maquinas e Equiepamentos Utilizados";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos"                                  # Carrega o respectivo conte�do
            ."/maquinasUtilizadas.php");

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function registroExecucao(){
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina = "Registro de Execu��o";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos"                                  # Carrega o respectivo conte�do
            ."/registroExecucao.php");

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function manobrasEletricas(){
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina = "Manobras El�tricas";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos"                                  # Carrega o respectivo conte�do
            ."/manobrasEletricas.php");

        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function tempoTotal()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina = "Tempos Totais";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";


        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos/"                                 # Carrega o respectivo conte�do
            ."temposTotais.php");


        require_once ( ABSPATH 	. "/views/_includes/_footer.php");

    }

    public function encerramento()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();


        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #


        $this->pagina ="Encerramento";
        $this->titulo = "Centro de Controle da Manunten��o - Metro Service";
        $this->script = "osmDadosGeraisSupervisao.js";


        require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina

        require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
            ."navegador.php" );

        require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body

        require_once ( ABSPATH 	. "/views/pmp/modulos/"                                 # Carrega o respectivo conte�do
            ."encerramento.php");


        require_once ( ABSPATH 	. "/views/_includes/_footer.php");

    }

    function imprimirOption($tempo){
        switch($tempo){
            case 5:
                echo('<option value="">__________</option>');
                echo('<option value="5" selected>5 Minutos</option>');
                echo('<option value="15" >15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 15:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15" selected>15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 30:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30" selected>30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 60:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60" selected>1 Hora</option>');
                break;

            default:
                echo('<option value="" selected>__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;
        }

    }

}