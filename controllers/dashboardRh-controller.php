<?php

/**
 * Controller cujo recepciona os usu�rios de perfil RH da apliaca��o S.I.M.G
 *
 * @package SimgMVC
 * @since 0.1
 *
 * Este c�digo � confidencial.
 * A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 *
 */
class DashBoardRhController extends MainController {
	
	/**
	 * Vari�vel para controlar se a p�gina precisa de login ou n�o.
	 * 
	 * @var loginNecessario
	 * @access public
	 */
	public $loginNecessario = true;
	public $nivelNecessario = "1";
	
	public function index() {
		$this->controleAcesso();

		$_SESSION['direcionamento'] = "Rh";
        $_SESSION['navegador'] = "sidebarRH.php";

		$this->titulo = "SIMG";
		$this->script = "dashboardRh.js";

		require_once ( ABSPATH	. "/views/_includes/_header.php");
		require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php" );
		require_once ( ABSPATH 	. "/views/_includes/_body.php" );
		require_once ( ABSPATH 	. "/views/rh/dashboard.php");
		require_once ( ABSPATH 	. "/views/_includes/_footer.php");
	}
	
	public function consulta () {
        $this->controleAcesso();
		
		
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
		
		# ------------------------------------------------------------------------------------------------------- #
		# ------------------------------------------- 	VIEWS 	------------------------------------------------- #
		# ------------------------------------------------------------------------------------------------------- #
		
		//$this->navegador = "sidebarRH.php";												# Navegador necess�rio para a p�gina
		$this->titulo = "S.I.M.G - Consulta";											# T�tulo da P�gina
		
		require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
		require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
								."navegador.php" );
		require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
		require_once ( ABSPATH 	. "/views/rh/pesquisa.php");							# Carrega o respectivo conte�do
		require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
	}

	public function controlePonto()
	{
        $this->controleAcesso();
		$this->titulo = "SIMG - Ponto de Funcion�rio";
		
		require_once(ABSPATH . "/views/_includes/_header.php");
		require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
		require_once(ABSPATH . "/views/_includes/_body.php");
		require_once(ABSPATH . "/views/rh/controlePonto.php");
		require_once(ABSPATH . "/views/_includes/_footer.php");
	}

	public function relatorio () {
        $this->controleAcesso();
	
		$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
	
		# ------------------------------------------------------------------------------------------------------- #
		# ------------------------------------------- 	VIEWS 	------------------------------------------------- #
		# ------------------------------------------------------------------------------------------------------- #
	
		//$this->navegador = "sidebarRH.php";												# Navegador necess�rio para a p�gina
		$this->titulo = "S.I.M.G - Relat�rio";											# T�tulo da P�gina
	
		require_once ( ABSPATH	. "/views/_includes/_header.php");						# Carrega o header da p�gina
		require_once ( ABSPATH 	. "/views/_includes/navegadores/"						# Carrega o respectivo navegador
		."navegador.php" );
		require_once ( ABSPATH 	. "/views/_includes/_body.php" );						# Carrega o Body
		require_once ( ABSPATH 	. "/views/rh/relatorio.php");							# Carrega o respectivo conte�do
		require_once ( ABSPATH 	. "/views/_includes/_footer.php");						# Carrega o footer
	}


    public function cargos () {
        $this->controleAcesso();
        $this->script = "campos/scriptCargo.js";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        if(!empty($parametros))
            switch($parametros[1]){
                case 1:
                    $message = "Cargo inserido com sucesso.";
                    break;
                case 2:
                    $message = "Cargo atualizado com sucesso.";
                    break;
                default:
                    $message = "Ocorreu um erro. Informar ao setor de desenvolvimento";
            }

        # ------------------------------------------------------------------------------------------------------- #
        # ------------------------------------------- 	VIEWS 	------------------------------------------------- #
        # ------------------------------------------------------------------------------------------------------- #

        //$this->navegador = "sidebarRH.php";										        		    # Navegador necess�rio para a p�gina
        $this->titulo = "S.I.M.G - Cargos";											                    # T�tulo da P�gina

        require_once ( ABSPATH	. "/views/_includes/_header.php");				            		    # Carrega o header da p�gina
        require_once ( ABSPATH 	. "/views/_includes/navegadores/"				            	    	# Carrega o respectivo navegador
            ."navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );			                			# Carrega o Body
        require_once ( ABSPATH 	. "/views/_includes/formularios/campos/cargosFuncionarios.php");		# Carrega o respectivo conte�do
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");					                	# Carrega o footer
    }
}