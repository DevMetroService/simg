<?php

class dashboardEngenhariaSupervisaoController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "7.1";

    public function index()
    {
        $this->controleAcesso();

        $this->titulo = "Supervisão - Engenharia";
        $this->script = "dashboardEngenhariaSupervisao.js";
        $this->paginaDashboard = "cronograma";

        $_SESSION['direcionamento'] = "EngenhariaSupervisao";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        
        $this->dashboard->modalUserForm($this->dadosUsuario);

        $this->dashboard->cabecalho('Engenharia Supervisão - Cronograma ' . date('Y'));
        $this->dashboard->graficoAcompanhamentoAnoAtual();
        $this->dashboard->painelCronograma('Ed', true);
        $this->dashboard->painelCronograma('Vp', true);
        $this->dashboard->painelCronograma('Su', true);
        $this->dashboard->painelCronograma('Ra', true);
        $this->dashboard->painelCronograma('Tl', true);
        $this->dashboard->painelCronograma('Bl', true);

        $this->dashboard->modalExibirCronograma();

        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function cronograma(){
        $this->index();
    }

    public function pmp()
    {
        $this->controleAcesso();

        $this->titulo = "Supervisão - Engenharia";
        $this->script = "dashboardEngSupPmp.js";
        $this->paginaDashboard = "pmp";

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 21]]);
        $contador = count($contador);
        $this->quantidadeEd = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 20]]);
        $contador = count($contador);
        $this->quantidadeRa = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 25]]);
        $contador = count($contador);
        $this->quantidadeSb = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 24]]);
        $contador = count($contador);
        $this->quantidadeVp = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 27]]);
        $contador = count($contador);
        $this->quantidadeTl = $contador;

        $contador = $this->medoo->select("pmp", "*",['AND' => ['ativo[!]' => 'D', 'cod_grupo' => 28]]);
        $contador = count($contador);
        $this->quantidadeBl = $contador;


        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariasupervisao/dashboardPMP.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function corretiva()
    {
        $this->controleAcesso();

        $this->titulo = "Supervisão - Engenharia";
        $this->script = "dashboardSupervisao.js";
        $this->paginaDashboard = "corretiva";


        $contador = $this->medoo->select("v_saf", "*",["nome_status" => "Aberta"]);
        $contador = count($contador);
        $this->quantidadeSAF = $contador;

        $contador = $this->medoo->select("v_ssm", "*",["cod_status" => (int)9]);
        $contador = count($contador);
        $this->quantidadeSSM = $contador;

        $contador = $this->medoo->select("osm_falha",["[><]status_osm" =>"cod_ostatus"], "*",["cod_status" => 10]);
        $contador = count($contador);
        $this->quantidadeOSM = $contador;


        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariasupervisao/dashboardGerencia.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function programacao()
    {
        $this->controleAcesso();

        $this->titulo = "Supervisão - Engenharia";
        $this->script = "dashboardSupervisao.js";
        $this->paginaDashboard = "programacao";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariasupervisao/dashboardGerenciaPreventiva.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function preventiva()
    {
        $this->controleAcesso();

        $this->titulo = "Supervisão - Engenharia";
        $this->script = "dashboardEngenhariaPreventiva.js";
        $this->paginaDashboard = "preventiva";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/engenhariasupervisao/dashboardPreventiva.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function dashboardPmp(){
        $this->preventiva();
    }
}
