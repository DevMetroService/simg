<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 08/07/2015
 * Time: 08:32
 */

class ModuloOspController extends MainController
{

    public function salvarDadosGerais(){
        $_SESSION['dadosGeraisOsp'] = $_POST;

        $this->carregaModelo("ccm/osp/salvarDadosGeraisOsp-model");
        header("Location:".HOME_URI."/dashboardGeral/osp");
    }

    public function encerrarDadosGerais()
    {
        $_SESSION['dadosGeraisOsp'] = $_POST;

        $this->carregaModelo("ccm/osp/encerrarDadosGeraisOsp-model");
        header("Location:".HOME_URI."/dashboard".$_SESSION['direcionamento']);
    }

    // ------- maoObra
    public function maoObra()
    {
        $this->pagina = "Mãoo de Obra";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaoObra.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/maoDeObra.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaoObra(){
        $_SESSION['dadosMaoObra'] = $_POST;

        $this->carregaModelo("ccm/osp/modulos/maoObraOsp-model");
        header("Location:".HOME_URI."/moduloOsp/maoObra");
    }

    // ------- materialUtilizado
    public function materialUtilizado(){
        $this->pagina = "Material Utilizado";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaterialUtilizado.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/materiaisUtilizados.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaterialUtilizado(){
        $_SESSION['dadosMateriais'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['materialCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['materialCheck'] = false;
        }

        $this->carregaModelo('ccm/osp/modulos/materialUtilizadoOsp-model');
        header("Location:".HOME_URI."/moduloOsp/materialUtilizado");
    }

    // ------- maquinaUtilizada
    public function maquinaUtilizada(){
        $this->pagina = "Maquinas e Equiepamentos Utilizados";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsMaquinaUtilizada.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/maquinasUtilizadas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaquinaUtilizada(){
        $_SESSION['dadosMaquina'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['maquinaCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['maquinaCheck'] = false;
        }

        $this->carregaModelo('ccm/osp/modulos/maquinaUtilizadaOsp-model');
        header("Location:".HOME_URI."/moduloOsp/maquinaUtilizada");
    }

    // ------- registroExecucao
    public function registroExecucao(){
        $this->pagina = "Registro de Execução";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsRegistroExecucao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/registroExecucao.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarRegistroExecucao(){
        $_SESSION['registroExecucao'] = $_POST;

        $this->carregaModelo('ccm/osp/modulos/registroExecucaoOsp-model');
        header("Location:".HOME_URI."/moduloOsp/registroExecucao");
    }

    // ------- manobrasEletricas
    public function manobrasEletricas(){
        $this->pagina = "Manobras Elétricas";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsManobrasEletricas.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/manobrasEletricas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarManobrasEletricas(){
        $_SESSION['dadosManobra'] = $_POST;

        $this->carregaModelo('ccm/osp/modulos/manobraEletricaOsp-model');
        header("Location:".HOME_URI."/moduloOsp/manobrasEletricas");
    }

    // ------- tempoTotal
    public function tempoTotal()
    {
        $this->pagina = "Tempos Totais";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOsTemposTotais.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/temposTotais.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarTemposTotais(){
        $_SESSION['dadosTemposTotais'] = $_POST;

        $this->carregaModelo('ccm/osp/modulos/temposTotaisOsp-model');
        header("Location:".HOME_URI."/moduloOsp/tempoTotal");
    }

    // ------- encerramento
    public function hasManobraEntrada($codSsm){
        $allOs = $this->medoo->select('v_osp','*', ['cod_ssm' => $codSsm]);

        foreach($allOs as $dados){
            if($dados['nome_servico'] == 'MANOBRA DE ENTRADA'){
                return true;
            }
        }
        return false;
    }

    public function encerramento()
    {
        $this->pagina = "Encerramento";
        $this->titulo = "Centro de Controle da Manuntenção - Metro Service";
        $this->script = "scriptOspEncerramento.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/ccm/osp/modulos/encerramento.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarEncerramento(){
        $_SESSION['dadosEncerramento'] = $_POST;

        $this->carregaModelo('ccm/osp/modulos/encerramentoOsp-model');

        if($_SESSION['direcionamento'] == "ccm")
            $dash = $_SESSION['direcionamento']."/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:{$this->home_uri}/dashboard".$dash);
    }

    function imprimirOption($tempo){
        switch($tempo){
            case 5:
                echo('<option value="">__________</option>');
                echo('<option value="5" selected>5 Minutos</option>');
                echo('<option value="15" >15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 15:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15" selected>15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 30:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30" selected>30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 60:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60" selected>1 Hora</option>');
                break;

            default:
                echo('<option value="" selected>__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;
        }

    }

}
