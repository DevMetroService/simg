<?php

class NotificacaoController extends MainController{

    public function notificar(){
        if(!empty($_SESSION['notificacao'])){

            foreach($_SESSION['notificacao'] as $key => $value){
                if(gettype($value) == "string"){
                    $_SESSION['notificacao'][$key] = utf8_encode($value);
                }
            }

            //---- SAF , SSM Equipe , OSM , OSP
            if($_SESSION['notificacao']['nomeLinha'])
                $_SESSION['notificacao']['local'] = $this->primary_sub_info($_SESSION['notificacao']['nomeLinha'],$_SESSION['notificacao']['nomeTrecho']);

            //---- OSM -- Com pendencia
            if($_SESSION['notificacao']['ssmPendenteNomeLinha'])
                $_SESSION['notificacao']['ssmPendenteLocal'] = $this->primary_sub_info($_SESSION['notificacao']['ssmPendenteNomeLinha'],$_SESSION['notificacao']['ssmPendenteNomeTrecho']);

            //---- OSM -- Transferida
            if($_SESSION['notificacao']['transferenciaStatus'])
                $_SESSION['notificacao']['transferenciaStatus'] = $this->primary_sub_info($_SESSION['notificacao']['transferenciaStatus'],$_SESSION['notificacao']['transferenciaMotivo']);

            //---- OSP -- Com pendencia
            if($_SESSION['notificacao']['sspPendenteServico'])
                $_SESSION['notificacao']['sspPendenteServico'] = $this->tooltipWrapper($_SESSION['notificacao']['sspPendenteComplementoServico'],$_SESSION['notificacao']['sspPendenteServico']);


            //---- SSM , OSM , OSP
            if($_SESSION['notificacao']['nomeEquipe'])
                $_SESSION['notificacao']['equipe'] = $this->tooltipWrapper(
                    $_SESSION['notificacao']['nomeEquipe']  . " - " . $_SESSION['notificacao']['nomeUnidade'],
                    $_SESSION['notificacao']['siglaEquipe'] . " - " . $_SESSION['notificacao']['siglaUnidade']
                );

            //----- SSM
            if($_SESSION['notificacao']['tabela'] == "Ssm"){
                if($_SESSION['notificacao']['status'])
                    $_SESSION['notificacao']['statusMotivo'] = $this->primary_sub_info($_SESSION['notificacao']['status'],$_SESSION['notificacao']['motivo']);

                if($_SESSION['notificacao']['servico'])
                    $_SESSION['notificacao']['servico'] = $this->primary_sub_info($_SESSION['notificacao']['servico'],$_SESSION['notificacao']['complementoServico']);
            }

            //---- SSP
            if($_SESSION['notificacao']['tabela'] == "Ssp"){
                if($_SESSION['notificacao']['servico'])
                    $_SESSION['notificacao']['servico'] = $this->tooltipWrapper($_SESSION['notificacao']['complementoServico'],$_SESSION['notificacao']['servico']);
            }
            
            $_SESSION['notificacao']['statusEmit'] = true;

            $this->encaminharChannel('Ccm');

            if($_SESSION['notificacao']['subChannelEquipe']) {
                $this->encaminharChannel('Equipe');
            }else{
                if($this->dadosUsuario['nivel'] != "2"){
                    $_SESSION['notificacao']['usuarioCriador'] = true;
                }
            }

            $dados = $_SESSION['notificacao'];

            unset($_SESSION['notificacao']);

        }else{
            $dados = array("statusEmit" => false);
        }

        echo json_encode($dados);
    }

    public function encaminharChannel($channel){
        if($_SESSION['notificacao']['mensag'])
            $_SESSION['notificacao']['channel'][] = "notification{$channel}";

        $_SESSION['notificacao']['channel'][] = "function{$channel}";
    }

    public function primary_sub_info($primary, $sub){
        return "<span class='primary-info'>{$primary}</span><br /><span class='sub-info'>{$sub}</span>";
    }

    public function tooltipWrapper($title, $primary){
        return "<span class='primary-info' data-placement='right' rel='tooltip-wrapper' data-title='{$title}'>{$primary}</span>";
    }

    public function notificarNoRealTime(){
        if(!empty($_SESSION['notificacao'])){
            echo json_encode($_SESSION['notificacao']['mensag']);
            unset($_SESSION['notificacao']);
        }
    }

    public function feedBackBD(){ // tipo // titulo // mensagem
        if(!empty($_SESSION['feedBackBD'])){
            $dados = $_SESSION['feedBackBD'];

            $dados['titulo'] = utf8_encode($dados['titulo']);
            $dados['mensagem'] = utf8_encode($dados['mensagem']);
            $dados['feedBack'] = true;

            unset($_SESSION['feedBackBD']);
        }else{
            $dados = array("feedBack" => false);
        }
        echo json_encode($dados);
    }
}