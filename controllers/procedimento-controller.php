<?php

class ProcedimentoController extends MainController
{
    protected $procedimentoModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->procedimentoModel = $this->carregaModelo('procedimento-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptProcedimento.js";

        $procedimento = $this->procedimentoModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/procedimento/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $this->procedimentoModel->create();

        header("Location:{$this->home_uri}/procedimento/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codPN = $parametros[0];

        $this->procedimentoModel->update($codPN);

        header("Location:{$this->home_uri}/procedimento/create");
    }
}
 ?>
