<?php

/**
 * Created by PhpStorm.
 * User: Valmar
 * Date: 06/03/2015
 * Time: 08:58
 */
class DashboardUsuarioController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "2.3";

    public function index()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Usuario";
        $_SESSION['navegador'] = "sidebarUsuario.php";

        $this->canalNotificacao ="user";

        $this->titulo = "SIMG";
        $this->script = "dashboardUsuario.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php" );
        require_once ( ABSPATH 	. "/views/_includes/_body.php" );
        require_once ( ABSPATH  . "/views/usuario/dashboardUsuario.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }


}