<?php

class PMPController extends MainController{

    public $loginNecessario = true;
    public $nivelNecessario = ["7", "7.1"];
    public $gerarPmpAnual = false;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);
        $this->controle();

        if($this->dadosUsuario['nivel'] == '7.1')
            $this->gerarPmpAnual = true;
    }

    public function gerarPmpAnual(){

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $grupo[] = $parametros[0];
        $ano = $parametros[1];
        $ativo = $parametros[2];
        $novo = $parametros[3];

        $tipoPmp = [];
        if($ativo != "false")
        {
            $tipoPmp[] = 'A';
        }
        if($novo != "false")
        {
            $tipoPmp[] = 'E';
        }

        $pmp = $this->carregaModelo("pmp-model");    
        $pmp->gerarPmpAnual($grupo, $ano, $tipoPmp);

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    public function edificacao()
    {
        $this->script = "scriptPmpEd.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        //Preenche Sistemas de Edificações
        $sistemaEd = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 21]);
        if (!$sistemaEd)
            $sistemaEd = null;

        $pmpEdificacao = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_edificacao' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpEdificacao)) {

            $local;
            $selectLocal = $this->medoo->select("local", ['[><]linha' => 'cod_linha'], ["cod_local", "nome_local", "nome_linha"]);
            if ($selectLocal)
                foreach ($selectLocal as $dados) {
                    $local[$dados['cod_local']]['nome_local'] = $dados['nome_local'];
                    $local[$dados['cod_local']]['nome_linha'] = $dados['nome_linha'];
                }

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 21")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 21]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados => $value)
                    $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

        }
        
        require_once(ABSPATH . "/views/forms/pmp/edificacao/create.php");
    }

    public function viaPermanente()
    {
        $this->script = "scriptPmpVp.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        $sistemaVp = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 24]);
        if (!$sistemaVp)
            $sistemaVp = null;

        $pmpViaPermanente = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_via_permanente' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpViaPermanente)) {
            $estacao;
            $selectEstacao = $this->medoo->select("estacao", ['[><]linha' => 'cod_linha'], "*");
            if ($selectEstacao)
                foreach ($selectEstacao as $dados)
                    $estacao[$dados['cod_estacao']] = $dados;

            $amv = $this->medoo->select("amv", "*");
                if ($amv)
                    foreach ($amv as $dados)
                        $amv[$dados['cod_amv']] = $dados['nome_amv'];

            $via;
            $selectVia = $this->medoo->select("via", "*");
            if ($selectVia)
                foreach ($selectVia as $dados)
                    $via[$dados['cod_via']] = $dados['nome_via'];

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 24")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados)
                    $procedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];

        }

        require_once(ABSPATH . "/views/forms/pmp/viaPermanente/create.php");
    }

    public function bilhetagem()
    {
        $this->script = "scriptPmpBl.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        $sistemaBl = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 28]);
        if (!$sistemaBl)
            $sistemaBl = null;

        $pmpBilhetagem = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_bilhetagem' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpBilhetagem)) {

            $estacao;
        $selectEstacao = $this->medoo->select("estacao", ["[><]linha" => "cod_linha"], ["cod_estacao", "nome_estacao", "nome_linha"]);
            if ($selectEstacao)
                foreach ($selectEstacao as $dados){
                    $estacao[$dados['cod_estacao']]["linha"] = $dados['nome_linha'];
                    $estacao[$dados['cod_estacao']]["estacao"] = $dados['nome_estacao'];
                }

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 28")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 28]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados => $value)
                    $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

        }

        require_once(ABSPATH . "/views/forms/pmp/bilhetagem/create.php");
    }

    public function telecom()
    {
        $this->script = "scriptPmpTl.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        $sistemaTl = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 27]);
        if (!$sistemaTl)
            $sistemaTl = null;

        $pmpTelecom = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_telecom' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpTelecom)) {

            $local;
            $selectLocal = $this->medoo->select("local", ['[><]linha' => 'cod_linha'], ["cod_local", "nome_local", "nome_linha"]);
            if ($selectLocal)
                foreach ($selectLocal as $dados) {
                    $local[$dados['cod_local']]['nome_local'] = $dados['nome_local'];
                    $local[$dados['cod_local']]['nome_linha'] = $dados['nome_linha'];
                }

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 27")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 27]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados => $value)
                    $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

        }

        require_once(ABSPATH . "/views/forms/pmp/telecom/create.php");
    }

    public function redeAerea()
    {
        $this->script = "scriptPmpRa.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        $sistemaRa = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 20]);
        if (!$sistemaRa)
            $sistemaRa = null;

        $pmpRedeAerea = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_rede_aerea' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpRedeAerea)) {

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 20")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 20]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados)
                    $procedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];

            $local;
            $selectLocal = $this->medoo->select("local", ["cod_local", "nome_local"]);
            if ($selectLocal)
                foreach ($selectLocal as $dados)
                    $local[$dados['cod_local']] = $dados['nome_local'];

            $poste;
            $selectPoste = $this->medoo->select("poste", ["cod_poste", "nome_poste"]);
            if ($selectPoste)
                foreach ($selectPoste as $dados)
                    $poste[$dados['cod_poste']] = $dados['nome_poste'];

        }

        require_once(ABSPATH . "/views/forms/pmp/redeAerea/create.php");
    }

    public function subestacao()
    {
        $this->script = "scriptPmpSb.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        $sistemaSb = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 25]);
        if (!$sistemaSb)
            $sistemaSb = null;

        $pmpSubestacao = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_subestacao' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpSubestacao)) {

            $local;
            $selectLocal = $this->medoo->select("local", ['[><]linha' => 'cod_linha'], ["cod_local", "nome_local", "nome_linha"]);
            if ($selectLocal)
                foreach ($selectLocal as $dados) {
                    $local[$dados['cod_local']]['nome_local'] = $dados['nome_local'];
                    $local[$dados['cod_local']]['nome_linha'] = $dados['nome_linha'];
                }

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 25")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 25]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados)
                    $procedimento[$dados['cod_procedimento']] = $dados['nome_procedimento'];

        }

        require_once(ABSPATH . "/views/forms/pmp/subestacao/create.php");
    }

    public function jardins()
    {
        $this->script = "scriptPmpJd.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        $sistemaJd = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 31]);
        if (!$sistemaJd)
            $sistemaJd = null;

        $pmpJardins = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_jardins' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpJardins)) {

            $estacao;
            $selectEstacao = $this->medoo->select("estacao", ["[><]linha" => "cod_linha"], ["cod_estacao", "nome_estacao", "nome_linha"]);
            if ($selectEstacao)
                foreach ($selectEstacao as $dados){
                    $estacao[$dados['cod_estacao']]["linha"] = $dados['nome_linha'];
                    $estacao[$dados['cod_estacao']]["estacao"] = $dados['nome_estacao'];
                }

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 31")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 31]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados => $value)
                    $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

        }
        
        require_once(ABSPATH . "/views/forms/pmp/jardins/create.php");
    }

    public function transportes()
    {
        $this->script = "scriptPmpTv.js";

        require_once(ABSPATH . '/functions/modulosPMP.php');

        //Preenche Sistemas de Edificações
        $sistemaTv = $this->medoo->select('grupo_sistema', ["[><]sistema" => "cod_sistema"], ['nome_sistema', 'cod_sistema'], ['ORDER' => 'nome_sistema', "cod_grupo" => 30]);
        if (!$sistemaTv)
            $sistemaTv = null;

        $pmpTransportesVerticais = $this->medoo->select("pmp",
            [
                '[><]servico_pmp_periodicidade' => 'cod_servico_pmp_periodicidade',
                '[><]pmp_transportes_verticais' => 'cod_pmp'
            ], '*', ['ativo' => ['A','E']]);

        if (!empty($pmpTransportesVerticais)) {

            $local;
            $selectLocal = $this->medoo->select("local", ['[><]linha' => 'cod_linha'], ["cod_local", "nome_local", "nome_linha"]);
            if ($selectLocal)
                foreach ($selectLocal as $dados) {
                    $local[$dados['cod_local']]['nome_local'] = $dados['nome_local'];
                    $local[$dados['cod_local']]['nome_linha'] = $dados['nome_linha'];
                }

            $servicoPmpSbS;
            $sql = $this->medoo->query("SELECT sp.cod_servico_pmp_sub_sistema, s.nome_sistema, su.nome_subsistema, sr.nome_servico_pmp
                                        FROM servico_pmp_sub_sistema sp
                                        inner join servico_pmp sr on sp.cod_servico_pmp = sr.cod_servico_pmp
                                        inner join sub_sistema ss on sp.cod_sub_sistema = ss.cod_sub_sistema
                                        inner join subsistema su on ss.cod_subsistema = su.cod_subsistema
                                        inner join sistema s on ss.cod_sistema = s.cod_sistema
                                        WHERE cod_grupo = 30")->fetchAll();

            if ($sql)
                foreach ($sql as $dados)
                    $servicoPmpSbS[$dados['cod_servico_pmp_sub_sistema']] = $dados;


            $periodicidade;
            $selectPeriodicidade = $this->medoo->select("tipo_periodicidade", ["cod_tipo_periodicidade", "nome_periodicidade"]);
            if ($selectPeriodicidade)
                foreach ($selectPeriodicidade as $dados)
                    $periodicidade[$dados['cod_tipo_periodicidade']] = $dados['nome_periodicidade'];


            $procedimento;
            $selectProcedimento = $this->medoo->select("procedimento", ["cod_procedimento", "nome_procedimento"], ['cod_grupo' => 30]);
            if ($selectProcedimento)
                foreach ($selectProcedimento as $dados => $value)
                    $procedimento[$value['cod_procedimento']] = $value['nome_procedimento'];

        }
        
        require_once(ABSPATH . "/views/forms/pmp/transportesVerticais/create.php");
    }
/*
    public function deleteCronograma()
    {
        $cronogramaPMP = $this->medoo->query("SELECT cod_cronograma_pmp FROM cronograma_pmp
                                            JOIN pmp USING(cod_pmp)
                                            JOIN pmp_via_permanente USING(cod_pmp)
                                            LEFT JOIN ssmp USING(cod_cronograma_pmp)
                                            JOIN cronograma c USING(cod_cronograma)
                                            WHERE c.ano = 2021
                                            AND cod_ssmp IS NULL
                                            AND c.mes IN (1,2,3)
                                            ORDER BY cod_cronograma_pmp")->fetchAll(PDO::FETCH_COLUMN);


        $statusCronogramPmp = $this->medoo->query("SELECT cod_status_cronograma_pmp FROM status_cronograma_pmp
        WHERE cod_cronograma_pmp IN
        (
            SELECT cod_cronograma_pmp FROM cronograma_pmp
            JOIN pmp USING(cod_pmp)
            JOIN pmp_via_permanente USING(cod_pmp)
            LEFT JOIN ssmp USING(cod_cronograma_pmp)
            JOIN cronograma c USING(cod_cronograma)
            WHERE c.ano = 2021
            AND cod_ssmp IS NULL
            AND c.mes IN (1,2,3)
            ORDER BY cod_cronograma_pmp
        )")->fetchAll(PDO::FETCH_COLUMN);

        foreach($statusCronogramPmp as $dados=>$codStatusCronogramaPmp)
        {
            $this->medoo->update("status_cronograma_pmp",["cod_cronograma_pmp" => null],["cod_status_cronograma_pmp" => $codStatusCronogramaPmp]);

            $this->medoo->delete("status_cronograma_pmp",["cod_status_cronograma_pmp" => $codStatusCronogramaPmp]);
        }

        $this->medoo->delete("cronograma_pmp", ["cod_cronograma_pmp"=> $cronogramaPMP]);

        for($i=4; $i<=12; $i++)
        {
            $pmpMes = $this->medoo->query("SELECT count(cod_pmp), cod_pmp, cod_tipo_periodicidade, nome_periodicidade, c.quinzena FROM cronograma_PMP
            JOIN cronograma c USING(cod_cronograma)
            JOIN pmp USING(cod_pmp)
            JOIN pmp_via_permanente USING(cod_pmp)
            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
            JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
            where c.ano = 2021
            AND c.mes = {$i}
            group by cod_pmp, nome_periodicidade, cod_tipo_periodicidade, c.quinzena
            having count(cod_pmp)>1
            ORDER BY cod_pmp")->fetchAll(PDO::FETCH_ASSOC);


            foreach($pmpMes as $dados=>$pmpTriplicado)
            {
                $qzn = $i*2-1;

                for($q=$qzn; $q <= $i*2; $q++)
                {
                    $cronogramaPMP = $this->medoo->query("SELECT * FROM cronograma_PMP
                        JOIN cronograma c USING(cod_cronograma)
                        JOIN pmp USING(cod_pmp)
                        JOIN pmp_via_permanente USING(cod_pmp)
                        JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                        JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                        where cod_pmp= {$pmpTriplicado['cod_pmp']}
                        AND c.ano = 2021
                        AND c.mes = {$i}
                        AND c.quinzena = {$q}")->fetchAll(PDO::FETCH_ASSOC);

                    
                    $codCrono = (int)$cronogramaPMP[0]['cod_cronograma_pmp'];
                    $sql = "SELECT cod_status_cronograma_pmp FROM status_cronograma_pmp
                    WHERE cod_cronograma_pmp = {$codCrono}";

                    $statusCronogramPmp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_COLUMN);
                    foreach($statusCronogramPmp as $dados=>$codStatusCronogramaPmp)
                    {
                        $this->medoo->update("status_cronograma_pmp",["cod_cronograma_pmp" => null],["cod_status_cronograma_pmp" => $codStatusCronogramaPmp]);
            
                        $this->medoo->delete("status_cronograma_pmp",["cod_status_cronograma_pmp" => $codStatusCronogramaPmp]);
                    }
                    $this->medoo->delete("cronograma_pmp", ["cod_cronograma_pmp"=> $cronogramaPMP[0]['cod_cronograma_pmp']]);

                    $codCrono = (int)$cronogramaPMP[1]['cod_cronograma_pmp'];
                    $sql = "SELECT cod_status_cronograma_pmp FROM status_cronograma_pmp
                    WHERE cod_cronograma_pmp = {$codCrono}";

                    $statusCronogramPmp = $this->medoo->query($sql)->fetchAll(PDO::FETCH_COLUMN);
                    foreach($statusCronogramPmp as $dados=>$codStatusCronogramaPmp)
                    {
                        $this->medoo->update("status_cronograma_pmp",["cod_cronograma_pmp" => null],["cod_status_cronograma_pmp" => $codStatusCronogramaPmp]);
            
                        $this->medoo->delete("status_cronograma_pmp",["cod_status_cronograma_pmp" => $codStatusCronogramaPmp]);
                    }
                    $this->medoo->delete("cronograma_pmp", ["cod_cronograma_pmp"=> $cronogramaPMP[1]['cod_cronograma_pmp']]);
                }
            }
        }
    }

    public function deactivatePmp()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $grupo = $parametros[0];   

        $this->medoo->update("pmp", ["ativo" => "D"], ["cod_grupo" => $grupo]);

        echo("PMP do grupo desativado");
    }

    public function deleteCronogramaByGroupAndYear()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $grupo = $parametros[0];
        $ano = $parametros[1];

        $cod_cronograma_pmps = $this->medoo->query(
            "SELECT cod_cronograma_pmp
            FROM cronograma_pmp
            JOIN pmp USING(cod_pmp)
            JOIN cronograma USING(cod_cronograma)
            WHERE cod_grupo = {$grupo} AND ano = {$ano}"
        )->fetchAll(PDO::FETCH_COLUMN);
        $cod_cronograma_pmps = implode(',', $cod_cronograma_pmps);

        $cod_status_cronograma_pmp = $this->medoo->query(
            "SELECT cod_status_cronograma_pmp 
            FROM status_cronograma_pmp 
            WHERE cod_cronograma_pmp IN ({$cod_cronograma_pmps})"
            )->fetchAll(PDO::FETCH_COLUMN);
        $cod_status_cronograma_pmp = implode(',', $cod_status_cronograma_pmp);

        $this->medoo->query(
            "UPDATE status_cronograma_pmp SET cod_cronograma_pmp = null 
            WHERE cod_status_cronograma_pmp IN ({$cod_status_cronograma_pmp})"
            )->execute();

        $this->medoo->query(
            "UPDATE cronograma_pmp SET cod_status_cronograma_pmp = null 
            WHERE cod_cronograma_pmp IN ({$cod_cronograma_pmps})"
            )->execute();

        $this->medoo->query(
            "DELETE FROM status_cronograma_pmp WHERE cod_status_cronograma_pmp IN({$cod_status_cronograma_pmp})"
        )->execute();

        $cod_ssmps = $this->medoo->query(
            "SELECT cod_ssmp FROM ssmp WHERE cod_cronograma_pmp IN ({$cod_cronograma_pmps})"
            )->fetchAll(PDO::FETCH_COLUMN);
        $cod_ssmps = implode(',', $cod_ssmps);

        if(!empty($cod_ssmps))
        {
            $cod_status_ssmp = $this->medoo->query(
                "SELECT cod_status_ssmp FROM status_ssmp WHERE cod_ssmp IN ({$cod_ssmps})"
                )->fetchAll(PDO::FETCH_COLUMN);
            $cod_status_ssmp = implode(',', $cod_status_ssmp);
    
            $this->medoo->query(
                "UPDATE status_ssmp SET cod_ssmp = null 
                WHERE cod_status_ssmp IN ({$cod_status_ssmp})"
                )->execute();
    
            $this->medoo->query(
                "UPDATE ssmp SET cod_status_ssmp = null 
                WHERE cod_ssmp IN ({$cod_ssmps})"
                )->execute();
    
            $this->medoo->query(
                "DELETE FROM status_ssmp WHERE cod_status_ssmp IN({$cod_status_ssmp})"
            )->execute();

            $cod_osmps = $this->medoo->query(
                "SELECT cod_osmp FROM osmp WHERE cod_ssmp IN ({$cod_ssmps})"
                )->fetchAll(PDO::FETCH_COLUMN);
            $cod_osmps = implode(',', $cod_osmps);

            if(!empty($cod_osmps)){
                $cod_status_osmp = $this->medoo->query(
                    "SELECT cod_status_osmp FROM status_osmp WHERE cod_osmp IN ({$cod_osmps})"
                    )->fetchAll(PDO::FETCH_COLUMN);
                $cod_status_osmp = implode(',', $cod_status_osmp);
        
                $this->medoo->query(
                    "UPDATE status_osmp SET cod_osmp = null 
                    WHERE cod_status_osmp IN ({$cod_status_osmp})"
                    )->execute();
        
                $this->medoo->query(
                    "UPDATE osmp SET cod_status_osmp = null 
                    WHERE cod_osmp IN ({$cod_osmps})"
                    )->execute();
        
                $this->medoo->query(
                    "DELETE FROM status_osmp WHERE cod_status_osmp IN({$cod_status_osmp})"
                )->execute();

                $this->medoo->query(
                    "DELETE FROM osmp_registro WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_encerramento WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_manobra_eletrica WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_mao_de_obra WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_maquina WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_material WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_tempo WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
    
                $this->medoo->query(
                    "DELETE FROM osmp_ed WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_vp WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_bi WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_ra WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_su WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
                $this->medoo->query(
                    "DELETE FROM osmp_te WHERE cod_osmp IN({$cod_osmps})"
                )->execute();

                $this->medoo->query(
                    "DELETE FROM osmp WHERE cod_osmp IN({$cod_osmps})"
                )->execute();
            }

            $this->medoo->query(
                "DELETE FROM ssmp WHERE cod_ssmp IN({$cod_ssmps})"
            )->execute();
            
        }

        $this->medoo->query(
            "DELETE FROM cronograma_pmp WHERE cod_cronograma_pmp IN({$cod_cronograma_pmps})"
        )->execute();

        echo("Cronograma Excluido");
    }
*/
}