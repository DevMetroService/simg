<?php

class LocalController extends MainController
{
    protected $localModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->localModel = $this->carregaModelo('local-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptLocal.js";

        $local = $this->localModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/local/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $this->localModel->create();

        header("Location:{$this->home_uri}/local/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codLocal = $parametros[0];

        $this->localModel->update($codLocal);

        header("Location:{$this->home_uri}/local/create");
    }

}
 ?>
