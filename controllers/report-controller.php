<?php

class ReportController extends MainController
{

  public function __construct($parametros = array())
  {
      parent::__construct($parametros);
      $this->controle();
      unset($_SESSION['bloqueio']);
  }

  public function solicitacaoAlterNivelSSM()
  {
    $this->script = '../report/solicitacaoAlterNivelSSM.js';

    $alterNivel = $this->carregaModelo('SSM/alterNivel-model');
    $rows ="";

    if(count($_GET) > 1)
    {
      $resultado = $alterNivel->getAllByFilter($_GET);

      if(!empty($resultado));
        foreach($resultado as $dados=>$value)
        {

          $rows .= "
          <tr>
            <td>
              {$value['cod_ssm']}
            </td>
            <td>
              {$value['data_solicitacao']}
            </td>
            <td>
              {$value['nivel_antigo']}
            </td>
            <td>
              {$value['nivel_novo']}
            </td>
            <td>
              {$value['nome_status']}
            </td>
            <td>
              <button type='button' class='btn btn-circle btn-default btnEditarSsm'><i class='fa fa-eye fa-fw'></i></button>
            </td>
          </tr>";
        }
    }

    require_once(ABSPATH . "/views/report/solicitacaoAlterNivelSSM.php");
  }

  public function indicadorRetrabalho()
  {
    $this->script = '../report/indicadorRetrabalho.js';
    $title="";
    $rows ="";

    if(count($_GET) > 1)
    {
      $_GET = array_filter($_GET);
      $where = "";
      $whereData = "";

      unset($_GET['path']);

      if(!empty($_GET['dataPartir']) || !empty($_GET['dataAte']))
      {
        if(!empty($_GET['dataPartir']))
        {
          $whereData .= " AND data_abertura >= '{$_GET['dataPartir']}'";
          $dataPartir = $_GET['dataPartir'];
          unset($_GET['dataPartir']);
          $title .= "|| A PARTIR <strong>{$dataPartir}</strong> ||";
        }

        if(!empty($_GET['dataAte']))
        {
          $whereData .= "AND data_abertura <= '{$_GET['dataAte']}'";
          $dataAte = $_GET['dataAte'];
          unset($_GET['dataAte']);
          $title .= "|| AT� <strong>{$dataAte}</strong> ||";
        }
        
      }else{
        $whereData .= "AND vsaf.data_abertura>(NOW()-interval '30 days')";
        $title .= "|| �LTIMOS 30 DIAS ||";
      }

      foreach ($_GET as $dados=>$value)
      {
        $where .= " AND {$dados} = {$value}";
        switch($dados)
        {
          case "cod_grupo":
            $grupo = $this->medoo->select("grupo", ["nome_grupo"], ["cod_grupo"=> $value])[0];
            $grupo = strToUpper($grupo['nome_grupo']);
            $title .= "|| GRUPO <strong>{$grupo}</strong> ||";
            break;
          case "cod_sistema":
            $grupo = $this->medoo->select("sistema", ["nome_sistema"], ["cod_sistema"=> $value])[0];
            $grupo = strToUpper($grupo['nome_sistema']);
            $title .= "|| SISTEMA <strong>{$grupo}</strong> ||";
            break;
          case "cod_linha":
            $grupo = $this->medoo->select("linha", ["nome_linha"], ["cod_linha"=> $value])[0];
            $grupo = strToUpper($grupo['nome_linha']);
            $title .= "|| LINHA <strong>{$grupo}</strong> ||";
            break;
          case "cod_trecho":
            $grupo = $this->medoo->select("trecho", ["descricao_trecho"], ["cod_trecho"=> $value])[0];
            $grupo = strToUpper($grupo['descricao_trecho']);
            $title .= "|| TRECHO <strong>{$grupo}</strong> ||";
            break;
        }
      }

      $sql = "SELECT
                count(*) as ctd, vsaf.cod_grupo, vsaf.nome_grupo, vsaf.cod_sistema, vsaf.nome_sistema, vsaf.cod_linha
                , vsaf.nome_linha, vsaf.cod_trecho, vsaf.nome_trecho, vsaf.cod_avaria, vsaf.nome_avaria
              FROM
                v_saf vsaf
              WHERE
                vsaf.cod_carro IS NULL
                {$where}
                {$whereData}
              GROUP BY
                vsaf.cod_grupo, vsaf.nome_grupo, vsaf.cod_sistema, vsaf.nome_sistema, vsaf.cod_linha, vsaf.nome_linha
                , vsaf.cod_trecho, vsaf.nome_trecho, vsaf.cod_avaria, vsaf.nome_avaria
              HAVING count(*) > 1
              ORDER BY
                ctd DESC, cod_grupo, cod_sistema, cod_linha, cod_trecho";
                
      $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
      if(empty($title))
        $title = " || SEM FILTRO";
    }

    require_once(ABSPATH . "/views/report/indicadorRetrabalho.php");
  }

  public function indicadorRetrabalhoMR()
  {
    $this->script = '../report/indicadorRetrabalhoMR.js';
    $title = "";
    $rows ="";
    if(count($_GET) > 1)
    {
      $_GET = array_filter($_GET);
      $where = "";
      $whereData = "";

      unset($_GET['path']);

      if(!empty($_GET['dataPartir']) || !empty($_GET['dataAte']))
      {
        if(!empty($_GET['dataPartir']))
        {
          $whereData .= " AND data_abertura >= '{$_GET['dataPartir']}'";
          $dataPartir = $_GET['dataPartir'];
          $title .= "|| A PARTIR <strong>{$dataPartir}</strong>||";
          unset($_GET['dataPartir']);
        }

        if(!empty($_GET['dataAte']))
        {
          $whereData .= "AND data_abertura <= '{$_GET['dataAte']}'";
          $dataAte = $_GET['dataAte'];
          $title .= "|| AT� <strong>{$dataAte}</strong> ||";
          unset($_GET['dataAte']);
        }
        
      }else{
        $where .= "AND vsaf.data_abertura>(NOW()-interval '30 days')";
        $title .= "|| �LTIMOS 30 DIAS ||";
      }

      foreach ($_GET as $dados=>$value)
      {
        $where .= " AND {$dados} = {$value}";

        switch($dados)
        {
          case "cod_grupo":
            $grupo = $this->medoo->select("grupo", ["nome_grupo"], ["cod_grupo"=> $value])[0];
            $grupo = strToUpper($grupo['nome_grupo']);
            $title .= "|| GRUPO <strong>{$grupo}</strong> ||";
            break;
          case "cod_veiculo":
            $grupo = $this->medoo->select("veiculo", ["nome_veiculo"], ["cod_veiculo"=> $value])[0];
            $grupo = strToUpper($grupo['nome_veiculo']);
            $title .= "|| VE�CULO <strong>{$grupo}</strong> ||";
            break;
          case "cod_linha":
            $grupo = $this->medoo->select("linha", ["nome_linha"], ["cod_linha"=> $value])[0];
            $grupo = strToUpper($grupo['nome_linha']);
            $title .= "|| LINHA <strong>{$grupo}</strong> ||";
            break;
          case "cod_carro":
            $grupo = $this->medoo->select("carro", ["nome_carro"], ["cod_carro"=> $value])[0];
            $grupo = strToUpper($grupo['nome_carro']);
            $title .= "|| CARRO <strong>{$grupo}</strong> ||";
            break;
        }
      }
      $sql = "SELECT
                count(*) as ctd, vsaf.cod_avaria, vsaf.cod_carro, vsaf.nome_avaria, vsaf.nome_carro, vsaf.nome_veiculo
              FROM
                v_saf vsaf
              WHERE
                vsaf.cod_carro IS NOT NULL
                {$whereData}
                {$where}
              GROUP BY
                vsaf.cod_avaria, vsaf.cod_carro, vsaf.nome_avaria, vsaf.nome_carro, vsaf.nome_veiculo
              HAVING count(*) > 1
              ORDER BY
                ctd DESC";
      $result = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

      if(empty($title))
        $title = " || SEM FILTRO";
    }
    
    require_once(ABSPATH . "/views/report/indicadorRetrabalhoMR.php");
  }

  public function printReport()
  {
    $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
    $printName = $parametros[0];

    $this->script = '../report/printDefault.js';

    // $_GET = $_GET['get'];

    require_once(ABSPATH . "/views/prints/{$printName}.php");
  }

  public function getOptionSelectByVariable($variable, $indexHtml, $indexValue, $valueSelected=null, $subIndex = null)
  {
    $options = "";

    foreach($variable as $dados=>$value)
    {
      if($subIndex != null)
        $labelOption = "{$value[$indexHtml]} - {$value[$subIndex]}";
      else
        $labelOption = $value[$indexHtml];


      if($value[$indexValue] == $valueSelected){
        $options .= "<option value='$value[$indexValue]' selected>$labelOption</option>";
      }
      else{
        $options .= "<option value='$value[$indexValue]'>$labelOption</option>";
      }
    }

    return $options;
  }

}

 ?>
