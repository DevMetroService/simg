<?php

class AmvController extends MainController
{
    protected $amvModel;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);

        $this->amvModel = $this->carregaModelo('amv-model');
        
        $this->controle();
    }

    public function create()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        $this->script = "scriptAmv.js";

        $amv = $this->amvModel->all();

        require_once(ABSPATH . "/views/layout/topLayout.php");
        require_once(ABSPATH  . "/views/forms/amv/create.php");
        require_once(ABSPATH . "/views/layout/bottonLayout.php");
    }

    public function store()
    {
        $amvModel = $this->carregaModelo('amv-model');

        $amvModel->create();

        header("Location:{$this->home_uri}/amv/create");
    }

    public function update()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codAmv = $parametros[0];

        $this->amvModel->update($codAmv);

        header("Location:{$this->home_uri}/amv/create");
    }

}
 ?>
