<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 21/09/2020
 * Time: 11:25
 */


class OSMController extends MainController{

    //T�tulo da p�gina
    public $tituloOs = "Ordem de Servi�o de Manuten��o - Falha";
    //Formul�rio de link em m�dulos de OS
    public $actionForm = "OSM";
    //tipo de OS //TODO::Ajustar para actionForm e retirar vari�vel.
    public $tipoOS = "osm";

    public $codOs;
    public $os;
    public $statusOs;

    public $maoObra;
    public $tempoTotal;
    public $regExecucao;

    public function __construct($parametros = array())
    {
        parent::__construct($parametros);
        $this->controle();

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
        $this->codOs = $parametros[0];

        $this->os = $this->medoo->select("osm_falha",["[>]osm_servico" => "cod_osm","[>]material_rodante_osm" => "cod_osm"], "*", ["cod_osm" => $this->codOs])[0];
        $this->statusOs = $this->medoo->select("status_osm", ["[><]status" => "cod_status"], "*", ["cod_ostatus" => $this->os['cod_ostatus']])[0];

        $_SESSION['bloqueio'] = $this->bloquearEdicaoOsm($this->statusOs['data_status'],$this->statusOs['nome_status'], $this->statusOs['cod_status']);

        $this->maoObra = $this->medoo->select("osm_mao_de_obra", "*", ["cod_osm" => (int)$this->codOs]);
        $this->tempoTotal = $this->medoo->select("osm_tempo", "*", ["cod_osm" => (int)$this->codOs]);
        $this->regExecucao = $this->medoo->select("osm_registro", "*", ["cod_osm" => (int)$this->codOs]);

        $this->titulo = "SIMG - OSM";
    }

    public function gerarOsm()
    {
        if($_POST['statusAntigo'] == 'Encaminhado')
        {
            $_SESSION['dadosSsm'] = $_POST;
            $ssmModel = $this->carregaModelo("ssm-model");            
            $ssmModel->update();
        }

        $osmModel = $this->carregaModelo("osm-model");
        $osmModel->create();

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

//============== DADOS GERAIS
    public function dadosGerais()
    {
        $acao = true;
        $pag = "dadosGerais";

        $this->script = "scriptOsDadosGerais.js";

        require_once ( ABSPATH . '/functions/btnForm.php');

        $responsavel = $this->medoo->select("usuario", "usuario" ,["cod_usuario" => $this->os['usuario_responsavel']])[0];
        $encerramento = $this->medoo->select("osm_encerramento", "*", ["cod_osm" => (int)$this->codOs])[0];
        $tempo = $this->os['data_abertura'];

        $ss = $this->medoo->select("v_ssm", "*",["cod_ssm" => (int) $this->os['cod_ssm']])[0];
        $ss['cod_ss'] = $this->os['cod_ssm'];

        $saf = $this->medoo->select("v_saf", "*",["cod_saf" => (int) $ss['cod_saf']])[0];

        //Processo de valida��o de Maquina, Equipamento e Materiais utilizados.
        //Recebe o status atual para futuras avalias
        $_SESSION['dadosCheck']['statusOs'] = $this->statusOs['cod_status'];

        if($_SESSION['dadosCheck']['codigoOs'] != $this->codOs){
            $_SESSION['dadosCheck']['codigoOs'] = $this->codOs;

            $_SESSION['dadosCheck']['maquinaCheck'] = false;
            $_SESSION['dadosCheck']['materialCheck'] = false;
        }

        //Verifica se a OS est� em execu��o. Quando estiver encerrada n�o h� a necessidade de travar.
        if($this->statusOs['cod_status'] == 10){
            $_SESSION['dadosCheck']['maquina'] = $this->medoo->select("osm_maquina", "*", ["cod_osm" => (int)$this->codOs]);
            $_SESSION['dadosCheck']['material'] = $this->medoo->select("osm_material", "*", ["cod_osm" => (int)$this->codOs]);
        }

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/forms/os/modulos/dadosGerais.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

        modalDevolverCancelar($this->os, "Osm");
    }

    public function salvarDadosGerais(){
        $_SESSION['dadosGeraisOsm'] = $_POST;

        $this->carregaModelo("ccm/osm/salvarDadosGeraisOsm-model");
        header("Location:{$this->home_uri}/OSM/dadosGerais/{$this->codOs}");
    }

    public function encerrarDadosGerais()
    {
        $_SESSION['dadosGeraisOsm'] = $_POST;

        $this->carregaModelo("ccm/osm/encerrarDadosGeraisOsm-model");
        header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
    }

    public function cancelarOsm()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/cancelarOs-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }
//=====
    public function imprimirOsm()
    {
        $this->titulo = "OSM";

        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        //recebe o c�digo por par�metro
        $this->codOs = $parametros[0];

        require_once(ABSPATH . "/views/_includes/_headerImpressao.php");
        require_once(ABSPATH . "/views/prints/osm.php");
    }

    public function pesquisaOsm($dadosRefill = null, $returnPesquisa = null)
    {
        $this->controle();
        $this->script = "scriptPesquisaOsm.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/osm/pesquisaOsm.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function executarPesquisaOsm()
    {
        $pesquisaOsmModel = $this->carregaModelo("ccm/osm/pesquisaOsm-model");
        $returnPesquisa = $pesquisaOsmModel->getDados();

        $this->pesquisaOsm($_POST, $returnPesquisa);
    }

    public function resetarPesquisaOsm()
    {
        $this->pesquisaOsm();
    }

    // ------- maoObra
    public function maoObra(){
        $this->pagina = "M�o de Obra";
        $this->script = "scriptOsMaoObra.js";

        $acao = true;
        $pag = "maoObra";

        $disableMaoObra="";

        $funcionarioModel = $this->carregaModelo('funcionario-model');

        $maoObraFuncionario = $funcionarioModel->getFuncionarioByOsm($this->codOs);

        if(empty($this->tempoTotal)){
            $disableMaoObra = "disabled";
        }

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/maoObra.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaoObra(){
        $_SESSION['dadosMaoObra'] = $_POST;

        $this->carregaModelo("ccm/osm/modulos/maoObraOsm-model");
        header("Location:{$this->home_uri}/OSM/maoObra/{$this->codOs}");
    }

    // ------- materialUtilizado
    public function materialUtilizado(){
        $acao = true;
        $pag = "materialUtilizado";

        $sql = "SELECT m.cod_material, m.estado, m.origem, m.utilizado, m.descricao_origem, mat.nome_material, u.sigla_uni_medida, u.cod_uni_medida "
            . "FROM osm_material m "
            . "JOIN material mat USING (cod_material) "
            . "JOIN unidade_medida u ON u.cod_uni_medida = m.unidade "
            . "WHERE m.cod_osm = {$this->codOs}";

        //Recebe o grupo atuado
        $mr = $this->medoo->select("v_osm", "grupo_atuado", ['cod_osm' => $this->codOs]);
        $mr = $mr[0];

        $check = false;
        if(in_array($mr,$this->sistemas_fixos))
        {
          $check = false;
          $selectEquipamento = $this->medoo->select("v_material", ['nome_material','cod_material', 'descricao_material'],
              ["ORDER" => "nome_material", "AND" => ["cod_categoria" => 32, "ativo" => 's', "OR" => ["cod_grupo[!]" => [22,23], "cod_grupo" => null]]]);
        }else{
              $check = true;
              $selectEquipamento = $this->medoo->select("v_material", ['nome_material','cod_material', 'descricao_material'],
                  ["ORDER" => "nome_material", "AND" => ["cod_categoria" => 32, "cod_grupo" => $mr , "ativo" => 's']]);
        }


        $this->pagina = "Material Utilizado";
        $this->script = "scriptOsMaterialUtilizado.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/materiaisUtilizados.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaterialUtilizado(){
        $_SESSION['dadosMateriais'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['materialCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['materialCheck'] = false;
        }

        $this->carregaModelo('ccm/osm/modulos/materialUtilizado-model');
        header("Location:{$this->home_uri}/OSM/materialUtilizado/{$this->codOs}");
    }

    // ------- maquinaUtilizada
    public function maquinaUtilizada(){
        $acao = true;
        $pag = "maquinaUtilizada";

        $maquinaUtilizada = $this->medoo->select("osm_maquina", "*", [ "cod_osm" => $this->codOs]);

        $this->pagina = "Maquinas e Equiepamentos Utilizados";
        $this->script = "scriptOsMaquinaUtilizada.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/maquinasUtilizadas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarMaquinaUtilizada(){
        $_SESSION['dadosMaquina'] = $_POST;

        if($_POST['checkMaquina']){
            $_SESSION['dadosCheck']['maquinaCheck'] = true;
        }else{
            $_SESSION['dadosCheck']['maquinaCheck'] = false;
        }

        $this->carregaModelo('ccm/osm/modulos/maquinaUtilizada-model');
        header("Location:{$this->home_uri}/OSM/maquinaUtilizada/{$this->codOs}");
    }

    // ------- registroExecucao
    public function registroExecucao(){
        $acao = true;
        $pag = "registroExecucao";

        //Recebe todas as OS's
        $firstRE = $this->medoo->select('osm_falha', '*', ['cod_ssm' => $this->os['cod_ssm'], "ORDER" => 'cod_osm']);
        $blocked = false; //vari�vel auxiliar para bloqueio de edi��o.
        if(count($firstRE) > 1){
            // Verifica se pertence aos sistemas fixos e bloqueia edi��o.
            if(in_array($this->os['grupo_atuado'], $this->sistemas_fixos) && $this->statusOs['cod_status'] == 10 && $firstRE[0]['cod_osm'] != $this->codOs)
                $blocked = true;

            $firstRE = $firstRE[0];
            $firstRE = $this->medoo->select('osm_registro', '*', ['cod_osm' => $firstRE['cod_osm'] ]);
            $firstRE = $firstRE[0];
        }else{
            $firstRE = false;
        }

        $ss = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$this->os['cod_ssm']])[0];
        $ss['cod_ss'] = $ss['cod_ssm'];

        if($this->os['grupo_atuado'] == 22){
            $materialRodante = $this->medoo->select("material_rodante_osm", '*', ["cod_osm" => $this->codOs])[0];
        }

        $registro = $this->regExecucao[0];
        $unEquipe = $this->medoo->select("un_equipe", ['[><]equipe' => 'cod_equipe'],"*", ["cod_un_equipe" => (int)$registro['cod_un_equipe']])[0];

        $this->pagina = "Registro de Execu��o";
        $this->script = "scriptOsRegistroExecucao.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/registroExecucao.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarRegistroExecucao(){
        $_SESSION['registroExecucao'] = $_POST;
        $this->carregaModelo('ccm/osm/modulos/registroExecucao-model');
        header("Location:{$this->home_uri}/OSM/registroExecucao/{$this->codOs}");
    }

    // ------- manobrasEletricas
    public function manobrasEletricas(){
        $acao = true;
        $pag = "manobrasEletricas";

        $manobrasEletricas = $this->medoo->select("osm_manobra_eletrica", "*", ["cod_osm" => (int)$this->codOs] );

        $this->pagina = "Manobras El�tricas";
        $this->script = "scriptOsManobrasEletricas.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/manobrasEletricas.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarManobrasEletricas(){
        $_SESSION['dadosManobra'] = $_POST;

        $this->carregaModelo('ccm/osm/modulos/manobraEletrica-model');
        header("Location:{$this->home_uri}/OSM/manobrasEletricas/{$this->codOs}");
    }
    // ------- tempoTotal
    public function tempoTotal(){
        $acao = true;
        $pag = "tempoTotal";

        $selectTempo = $this->tempoTotal[0];

        if(empty($selectTempo)) {
            $selectAbertura = $this->medoo->select('osm_falha','data_abertura',[
                'cod_osm'       => (int) $this->codOs
            ])[0];
        }

        $this->pagina = "Tempos Totais";
        $this->script = "scriptOsTemposTotais.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/temposTotais.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function salvarTemposTotais(){
        $this->controle();

        $_SESSION['dadosTemposTotais'] = $_POST;

        $this->carregaModelo('ccm/osm/modulos/temposTotais-model');
        header("Location:{$this->home_uri}/OSM/tempoTotal/{$this->codOs}");
    }

    // ------- encerramento
    public function encerramento(){
        $acao = true;
        $pag = "encerramento";

        $os = $this->medoo->query("
            SELECT cod_osm as cod_os, *
            FROM osm_falha os
            JOIN grupo gr ON(gr.cod_grupo = os.grupo_atuado)
            WHERE cod_osm = {$this->codOs}")->fetchAll(PDO::FETCH_ASSOC)[0];

        $ss = $this->medoo->select("v_ssm", "*", ["cod_ssm" => (int)$os['cod_ssm']])[0];
        $ss['cod_ss'] = $ss['cod_ssm'];

        $encerramento = $this->medoo->select("osm_encerramento",["[>]pendencia" => "cod_pendencia", "[><]funcionario" => "cod_funcionario"], "*", ["cod_osm" => $os['cod_osm']])[0];

        $empresaFuncionario = $this->medoo->select("funcionario_empresa", "*", ["cod_funcionario_empresa" => $encerramento['cod_funcionario_empresa']])[0];

        $unEquipe = $this->medoo->select("un_equipe",[
            "[><]equipe" => "cod_equipe",
            "[><]unidade" => "cod_unidade"
        ], "*" ,[
                'cod_un_equipe' => $encerramento['cod_un_equipe']
            ]
        )[0];

        //Lista de materiais solicitados
        $listaMaterialSolicitado = $this->carregaModelo("osmMaterialSolicitado-model")->listByOsm($this->codOs);
        //Informa��es relacionadas ao m�dulo de Registro de Execu��o
        $unidadeEquipe = $this->medoo->select("osm_registro",[
            "[><]un_equipe" => "cod_un_equipe",
            "[><]unidade"   => "cod_unidade",
            "[><]equipe"    => "cod_equipe"
        ],[
            "equipe.sigla",
            "unidade.nome_unidade"
        ],[
            "osm_registro.cod_osm" => $os['cod_osm']
        ])[0];

        $materialNContratual = $this->medoo->query("SELECT * FROM v_material WHERE
        cod_tipo_material = 2
        AND cod_categoria = 32
        AND ativo = 's'
        AND (cod_grupo IS NULL OR cod_grupo NOT IN (22,23))")->fetchAll(PDO::FETCH_ASSOC);            

        $this->pagina ="Encerramento";
        $this->script = "scriptOsmEncerramento.js";

        require_once ( ABSPATH	. "/views/_includes/_header.php");
        require_once ( ABSPATH 	. "/views/_includes/navegadores/navegador.php");
        require_once ( ABSPATH 	. "/views/_includes/_body.php");
        require_once ( ABSPATH  . "/views/forms/os/modulos/encerramento.php");
        require_once ( ABSPATH 	. "/views/_includes/_footer.php");
    }

    public function hasManobraEntrada($codSsm){
        $allOs = $this->medoo->select('v_osm','*', ['cod_ssm' => $codSsm]);

        foreach($allOs as $dados){
            if($dados['nome_servico'] == 'MANOBRA DE ENTRADA'){
                return true;
            }
        }
        return false;
    }

    public function salvarEncerramento(){
        //verifica se h� informa��es na tabela desta OSM
        $selectEncerramento = $this->medoo->select("osm_encerramento", "*", ["cod_osm" => $this->dadosEncerramento['codigoOs']]);

        unset($_SESSION['dadosMaquinaOs']);
        unset($_SESSION['dadosMaterialOs']);

        if($_POST['descricaoTipoFechamento'] != 4)
        {
          $materialSolicitado = $this->carregaModelo('osmMaterialSolicitado-model');
          $materialSolicitado->deleteAllByOsm($_POST['codigoOs']);
        }

        $encerramentoModel = $this->carregaModelo('ccm/osm/modulos/encerramento-model');

        if(empty($selectEncerramento))
          $encerramentoModel->store();
        else{
          $selectEncerramento->update();
        }
        // var_dump($_SESSION['notificacao']);
        header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function hasNonContratualMaterial()
    {
      $material = $this->medoo->select('osm_material',["material"=>"cod_material"], '*', ["cod_osm" => $this->codOs]);

      if(!empty($material))
      {
          foreach($material as $dados=>$value)
          {
            if($value['cod_tipo_material'] != 1 && !empty($value['cod_tipo_material'])) echo true;
            return;
          }
      }

      echo false;
    }

    public function registrarMaterialNaoContratual()
    {
      $materialSolicitado = $this->carregaModelo('osmMaterialSolicitado-model');

      echo $materialSolicitado->create();
    }

    public function removerMaterialNaoContratual()
    {
      $materialSolicitado = $this->carregaModelo('osmMaterialSolicitado-model');

      echo $materialSolicitado->delete($_POST['cod_osm_material_solicitado']);
    }

    function imprimirOption($tempo){
        switch($tempo){
            case 5:
                echo('<option value="">__________</option>');
                echo('<option value="5" selected>5 Minutos</option>');
                echo('<option value="15" >15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 15:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15" selected>15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 30:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30" selected>30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;

            case 60:
                echo('<option value="">__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60" selected>1 Hora</option>');
                break;

            default:
                echo('<option value="" selected>__________</option>');
                echo('<option value="5">5 Minutos</option>');
                echo('<option value="15">15 Minutos</option>');
                echo('<option value="30">30 Minutos</option>');
                echo('<option value="60">1 Hora</option>');
                break;
        }

    }

}
?>
