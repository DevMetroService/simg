<?php

/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 04/04/2016
 * Time: 09:35
 */
class CadastroCamposController extends MainController
{
    public $loginNecessario = true;
    public $nivelNecessario = "0";

    public function controle()
    {
        switch (strtolower($_SESSION['direcionamento'])) {

            case "equipemr":
                $this->nivelNecessario = "5.1";
                break;

            case "engenharia":
                $this->nivelNecessario = "7";
                break;

            case "engenhariasupervisao":
                $this->nivelNecessario = "7.1";
                break;

            case "engenhariamr":
                $this->nivelNecessario = "7.2";
                break;
        }

        $this->controleAcesso();
    }

    //------------------------AVARIA------------------------//
    public function avaria()
    {
        $this->controle();

        $this->titulo = "Gerenciador de Avarias";
        $this->script = "scriptAddAvaria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/campos/avaria.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");

    }


    public function gravarAvaria () {
        $camposModel = $this->carregaModelo("crudCampos-model");

        if(empty($_POST['cod_avaria'])){
            $camposModel->createAvaria();
        }else{
            $camposModel->updateAvaria();
        }

        header("Location:" . HOME_URI . "/cadastroCampos/avaria");
    }

    public function localGrupo(){
        $this->controle();

        $this->titulo = "Gerenciador de Avarias";
        $this->script = "campos/scriptLocalGrupo.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/_includes/formularios/campos/localGrupo.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function gravarLocalGrupo(){
        $camposModel = $this->carregaModelo("crudCampos-model");

        if(empty($_POST['cod_local_grupo'])){
            $camposModel->createLocalGrupo();
        }else{
            $camposModel->updateLocalGrupo();
        }
        header("Location:{$this->home_uri}/cadastroCampos/localGrupo");
    }



}