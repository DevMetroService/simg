<?php

class CadastroGeralController extends MainController
{
    // Cadastro de SAF's

    public function gerarSaf()
    {
        $_SESSION['dadosSaf'] = $_POST;

        $saf = $this->carregaModelo("saf-model");

        $saf->cadastro();

        //TODO::retirar quando todos os dashboard se adequarem ao padrão.
        $nivel = ['astig', 'gesiv'];
        if(in_array($_SESSION['direcionamento'], $nivel))
            header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
        else
            header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function alterarSaf()
    {
        $_SESSION['dadosSaf'] = $_POST;

        $saf = $this->carregaModelo("saf-model");

        $saf->alterar();

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function refillSaf()
    {
        $_SESSION['refillSaf'] = $_POST;
    }

    public function imprimirSaf()
    {
        $_SESSION['refillSaf'] = $_POST;
    }

    public function aprovarSaf()
    {
        $_SESSION['dadosSaf'] = $_POST;

        $saf = $this->carregaModelo("saf-model");
        $saf->aprovar();

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function negarSaf()
    {
        $_SESSION['dadosSaf'] = $_POST;

        $saf = $this->carregaModelo("saf-model");
        $saf->negar();

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function devolverCancelarSaf()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/saf/devolverCancelarSaf-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    // Cadastro de SSM's

    public function gerarSsm()
    {
        $_SESSION['dadosSaf'] = $_POST;
        $_SESSION['dadosSsm'] = $_POST;

        if(!empty($_POST['altSafGerSsm'])) {
            $codigoStatus = $this->medoo->select('saf', ['[><]status_saf' => 'cod_ssaf'], 'cod_status', ['saf.cod_saf' => (int)$_POST['codigoSaf']]);
            if ($codigoStatus[0] != 1) { // Se não for Autorizada
                $saf = $this->carregaModelo("saf-model");

                $saf->alterar();
            }
        }

        $ssm = $this->carregaModelo("ssm-model");

        $ssm->gerar();

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function alterarSsm()
    {
        $_SESSION['alterarSsm'] = $_POST;

        $this->carregaModelo("ccm/ssm/alterarSsm-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function aprovarSsm()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codSsm = $parametros[0];

        $_SESSION['dadosSsm'] = $_POST;
        $_SESSION['dadosSsm']['cod_ssm'] = $codSsm;
        $_SESSION['dadosSsm']['cod_saf'] = $this->medoo->select('ssm', 'cod_saf', ['cod_ssm'=> $codSsm])[0];

        $ssm = $this->carregaModelo("ssm-model");

        $ssm->aprovar();

        if($this->nivelUsuario == 6.1 ||  $this->nivelUsuario == 4)
            header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
        else
            header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function desaprovarSsm()
    {

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $codSsm = $parametros[0];

        $_SESSION['dadosSsm'] = $_POST;
        $_SESSION['dadosSsm']['cod_ssm'] = $codSsm;
        $_SESSION['dadosSsm']['cod_saf'] = $this->medoo->select('ssm', 'cod_saf', ['cod_ssm'=>$codSsm])[0];

        var_dump($_SESSION['dadosSsm']);
        $ssm = $this->carregaModelo("ssm-model");

        $ssm->desaprovar();

        if($this->nivelUsuario == 6.1 ||  $this->nivelUsuario == 4)
            header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
        else
            header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function refillSsm()
    {
        $_SESSION['refillSsm'] = $_POST;
    }

    public function imprimirSsm()
    {
        $_SESSION['refillSsm'] = $_POST;
    }

    public function devolverCancelarSsm()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/ssm/devolverCancelarSsm-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function encaminharSsm()
    {
        $_SESSION['dadosSsm'] = $_POST;

        $ssm = $this->carregaModelo("ssm-model");

        $ssm->encaminhar();        

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function verificarSsm()
    {
        $selectOsm = $this->medoo->select("osm_falha",
            [
                "[><]status_osm" => "cod_ostatus",
                "[><]status" => "cod_status"
            ],
            ["osm_falha.cod_osm", "nome_status"], ["cod_ssm" => (int)$_GET['codigoSsm']]);

        $permissao = true;
        if ($selectOsm) {
            $permissao = false;
            foreach ($selectOsm as $dados) {
                $osm['codigo'] = $dados['cod_osm'];
                $osm['status'] = utf8_encode($dados['nome_status']);

                $arr['ordens'][] = $osm;
            }
        }
        $arr['permissao'] = $permissao;

        echo json_encode($arr);
    }

    // Cadastro de OSM's

    public function gerarOsm()
    {
        $_SESSION['gerarOsm'] = $_POST;

        $this->carregaModelo("ccm/gerarOsm-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function refillOsm()
    {
        $_SESSION['refillOs'] = $_POST;
    }

    public function imprimirOsm()
    {
        $_SESSION['refillOs'] = $_POST;
    }

    public function cancelarOsm()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/cancelarOs-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    // Cadastro de SSP's

    public function programarSsp()
    {
        $_SESSION['dadoSsp'] = $_POST;

        $this->carregaModelo("ccm/ssp/programarSspPreventiva-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function gerarSsp()
    {
        $_SESSION['dadoSsp'] = $_POST;

        if($this->validarDadosSsp($_SESSION['dadoSsp'])){
            $this->carregaModelo("ccm/ssp/cadastroSsp-model");
        }else{
            $_SESSION['erroCadastroModel'] = "Ocorreu um erro ao cadastrar seus dados no Banco de Dados. Entre em contato com a T.I.";
        }

        if ($_SESSION['direcionamento'] == "ccm")
            $dash = "ccm/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:" . HOME_URI . "/dashboard" . $dash);
    }

    public function gerarSspPreventiva()
    {
        $_SESSION['codPMP'] = $_POST['codigoPMP'];

        $this->carregaModelo("ccm/ssp/cadastroSspPreventiva-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento']);
    }

    public function alterarSsp()
    {
        $_SESSION['dadoSsp'] = $_POST;

        $this->carregaModelo("ccm/ssp/alterarSsp-model");

        if ($_SESSION['direcionamento'] == "ccm")
            $dash = "ccm/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:" . HOME_URI . "/dashboard" . $dash);
    }

    public function refillSsp()
    {
        $_SESSION['refillSsp'] = $_POST;
    }

    public function imprimirSsp()
    {
        $_SESSION['refillSsp'] = $_POST;
    }

    public function cancelarSsp()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/ssp/cancelarSsp-model");

        if ($_SESSION['direcionamento'] == "ccm")
            $dash = "ccm/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:" . HOME_URI . "/dashboard" . $dash);
    }

    public function verificarSsp()
    {
        $selectOsp = $this->medoo->select("osp",
            [
                "[><]status_osp" => "cod_ospstatus",
                "[><]status" => "cod_status"
            ],
            ["osp.cod_osp", "nome_status"], ["cod_ssp" => (int)$_GET['codigoSsp']]);

        $permissao = true;
        if ($selectOsp) {
            $permissao = false;
            foreach ($selectOsp as $dados) {
                $osp['codigo'] = $dados['cod_osp'];
                $osp['status'] = utf8_encode($dados['nome_status']);

                $arr['ordens'][] = $osp;
            }
        }
        $arr['permissao'] = $permissao;

        echo json_encode($arr);
    }

    // Cadastro de OSP's

    public function gerarOsp()
    {
        $_SESSION['gerarOsp'] = $_POST;

        $this->carregaModelo("ccm/gerarOsp-model");

        if ($_SESSION['direcionamento'] == "ccm")
            $dash = "ccm/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:" . HOME_URI . "/dashboard" . $dash);
    }

    public function refillOsp()
    {
        $_SESSION['refillOs'] = $_POST;
    }

    public function imprimirOsp()
    {
        $_SESSION['refillOs'] = $_POST;
    }

    public function cancelarOsp()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/cancelarOs-model");

        if ($_SESSION['direcionamento'] == "ccm")
            $dash = "ccm/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:" . HOME_URI . "/dashboard" . $dash);
    }

    // Cadastro Pmp
    public function refillPmp()
    {
        $_SESSION['refillPmp'] = $_POST;
    }

    // Cadastro Itens Pmp
    public function salvarListaPmp()
    {
        $_SESSION['dadosPmp'] = $_POST;

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->insertListaPmp();
    }

    public function gerarPmpAnual(){
        $_SESSION['dadosPmp'] = $_POST;

        $pmp = $this->carregaModelo("pmp-model");

        // Insira o array de grupos que deseja gerar o PMP
        // Insira o ano que o pmp deve gerar
        // Insira o array se é Ativos ou Editáveis
//        $pmp->gerarPmpAnual(['27', '28'], 2018, ['A', 'E']);

        $pmp->gerarPmpAnual();
//
//        $pmp->gerarPmpAnual(); // Gera Todos os Grupos do proximo ano Ativos e Editaveis

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    public function gerarPmpAnualMr(){
        $_SESSION['dadosPmp'] = $_POST;

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->gerarPmpAnual(['22'], 2018);

        header("Location:" . HOME_URI . "/dashboard{$_SESSION['direcionamento']}/pmp");
    }

    // Cadastro Procedimento
    public function cadastrarProcedimento()
    {
        $_SESSION['dadosPmp'] = $_POST;

        $pmp = $this->carregaModelo("pmp-model");
        $pmp->cadastroProcedimento();
    }

    // Cadastro SSMP
    public function gerarSsmp() {
        $_SESSION['codCronograma'] = $_POST['codCronograma'];

        $this->carregaModelo("engenharia/ssmp/{$_POST['form']}/gerarSsmp-model");
    }

    public function gerarSsmpTue() {
        $_SESSION['dadosTue'] = $_POST;

        $this->carregaModelo("engenharia/ssmp/tue/gerarSsmp-model");
    }

    public function programarSsmp(){
        $_SESSION['dadosSsmp'] = $_POST;

        $this->carregaModelo("engenharia/ssmp/{$_POST['form']}/programarSsmp-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/dashboardPmp");
    }

    public function gerarReprogramada() {
        $_SESSION['codCronograma'] = $_POST['codCronograma'];

        $this->carregaModelo("engenharia/ssmp/{$_POST['form']}/gerarSsmpReprogramada-model");
    }

    public function salvarSsmp(){
        $_SESSION['dadosSsmp'] = $_POST;

        $this->carregaModelo("engenharia/ssmp/salvarSsmp-model");

        header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/preventiva");
    }

    public function refillSsmp() {
        $_SESSION['refillSsmp'] = $_POST;
    }

    public function imprimirSsmp()
    {
        $_SESSION['refillSsmp'] = $_POST;
    }

    // Cadastro OSMP
    public function gerarOsmp(){
        $_SESSION['dadosSsmp'] = $_POST;

        if($this->validacaoHorario($_POST['dataHoraProgramada'], 'ssmp', $_POST['codigoSsmp'])){
            $_SESSION['alert_message'] = "Um <strong>Erro</strong> ocorreu. Não foi possível gerar OSMP. ERRO: 'Hora inválida'";
            header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/dashboardPmp");
        }else{
            $this->carregaModelo("engenharia/ssmp/{$_POST['form']}/gerarOsmp-model");
            header("Location:" . HOME_URI . "/dashboard" . $_SESSION['direcionamento'] . "/dashboardPmp");
        }
    }

    public function refillOsmp()
    {
        $_SESSION['refillOs'] = $_POST;
    }

    public function imprimirOsmp()
    {
        $_SESSION['refillOs'] = $_POST;
    }

    public function cancelarOsmp()
    {
        $_SESSION['dados'] = $_POST;

        $this->carregaModelo("ccm/cancelarOs-model");

        if ($_SESSION['direcionamento'] == "ccm")
            $dash = "ccm/dashboardPreventiva";
        else
            $dash = $_SESSION['direcionamento'];

        header("Location:" . HOME_URI . "/dashboard" . $dash);
    }

    // Material Rodante
    public function cadastrarVeiculo(){
        $_SESSION['dadosMr'] = $_POST;

        $veiculo = $this->carregaModelo("mr-model");

        $veiculo->cadastroVeiculo();

        header("Location:" . HOME_URI . "/dashboardGeral/configComposicao");
    }

    public function cadastrarCarro(){
        $_SESSION['dadosMr'] = $_POST;

        $carro = $this->carregaModelo("mr-model");

        $carro->cadastroCarro();

        header("Location:" . HOME_URI . "/dashboardGeral/configComposicao");
    }

    public function cadastrarComposicao(){
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $_SESSION['dadosMr'] = $parametros[0];

        $carro = $this->carregaModelo("mr-model");

        $carro->cadastroComposicao();

        header("Location:" . HOME_URI . "/dashboardGeral/configComposicao");
    }

    public function ConfigComposicao(){
        $_SESSION['dadosMr'] = $_POST;

        $carro = $this->carregaModelo("mr-model");

        $carro->configComposicao();

        header("Location:" . HOME_URI . "/dashboardGeral/configComposicao");
    }

    public function registrarOdometroTue(){
        $_SESSION['dadosMr'] = $_POST;

        $carro = $this->carregaModelo("mr-model");

        $carro->odometroTUE();

        header("Location:" . HOME_URI . "/dashboardGeral/registroOdometroTue");
    }

    public function registrarOdometroVlt(){
        $_SESSION['dadosMr'] = $_POST;

        $carro = $this->carregaModelo("mr-model");

        $carro->odometroVLT();

        header("Location:" . HOME_URI . "/dashboardGeral/registroOdometroVlt");
    }

    public function registrarProcedimento(){
        $_SESSION['dadosMr'] = $_POST;

        $mrModel = $this->carregaModelo("mr-model");

        $mrModel->procedimento();

        header("Location:" . HOME_URI . "/dashboardGeral/registroProcedimento");
    }

    public function registrarFicha(){
        $_SESSION['dadosMr'] = $_POST;

        $modelMR = $this->carregaModelo("mr-model");

        $modelMR->ficha();

        header("Location:" . HOME_URI . "/dashboardGeral/registroFicha");
    }

    public function registrarDisponibilidadeMr(){
        if($_POST)
        {
            $_SESSION['dadosMr'] = $_POST;

            $modelMR = $this->carregaModelo("mr-model");

            $modelMR->calculoDisponibilidade();
            echo json_encode(array("msg" => "Registro realizado com sucesso.") );
        }else{
            echo json_encode(array("msg" => "Ocorreu um erro. As informações não foram enviadas ao registro.
            Entre em contato com a TI.") );
        }

    }

    public function disponibilidadeTue(){
        $_SESSION['dadosMr'] = $_POST;

        $carro = $this->carregaModelo("mr-model");

        $carro->disponibilidadeTUE();

        header("Location:" . HOME_URI . "/dashboardGeral/disponibilidade/TUE");
    }

    public function disponibilidadeVlt(){
        $_SESSION['dadosMr'] = $_POST;

        $carro = $this->carregaModelo("mr-model");

        $carro->disponibilidadeVLT();

        header("Location:" . HOME_URI . "/dashboardGeral/disponibilidade/VLT");
    }

    public function cadastrarPmpMaterialRodante () {
        $_SESSION['dadosMr'] = $_POST;

        $pmp = $this->carregaModelo("mr-model");

        if(!empty($_POST["cod_pmp_mr"])) {
            $pmp->atualizarItemPmp();
        }
        else { // SE NAO houver cod_pmp_mr, um novo registro sera CRIADO
            $pmp->cadastroItemPmp();
        }

        //
        header("Location:" . HOME_URI . "/dashboardGeral\cadastroPMPMaterialRodante");

    }

    // Diversos
    public function cadastrarUsuario()
    {
        $_SESSION['dadosUser'] = $_POST;
        $_SESSION['dadosUser']['senha'] = $this->phpass->HashPassword("admin");

        $this->carregaModelo("administrador/cadastrarUsuario-model");
        
        $nivel = ['astig', 'gesiv'];
        if(in_array($_SESSION['direcionamento'], $nivel))
            header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
        else
            header("Location:{$this->home_uri}/dashboard{$_SESSION['direcionamento']}");
    }

    public function mostrarFuncionario()
    {
        $option = "";

        $selectFuncionario = $this->medoo->select("funcionario", ["cod_funcionario", "nome_funcionario", "matricula"], ["ORDER" => "matricula"]);

        foreach ($selectFuncionario as $dados => $value) {
            echo("<option value='" . $value['cod_funcionario'] . "'> " . $value['nome_funcionario'] . " - " . $value['matricula'] . "</option>");
        }

        return $option;
    }

    public function alterarSenhaUsuario()
    {
        $retorno = $this->alterarSenha($_POST['nomeUsuario'], $_POST['senhaAntiga'], $_POST['novaSenha'], $_POST['confirmarSenha']);

        if ($retorno) {
            echo true;
        } else {
            echo false;
        }
    }

    public function getDateServer(){
        $arr = array(
            "tempo" => date('d-m-Y H:i:s', time())
        );

        echo json_encode($arr);

//        echo date('Y-m-d h:i:s');
    }

    public function exibirPdf(){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_SESSION['nomeArquivoPdf'] = $_POST;
        }
    }



}
