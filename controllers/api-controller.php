<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 16/03/2016
 * Time: 08:47
 */

class ApiController extends MainController
{
    private $bdname = DB_NAME;
    private $host = HOSTNAME;
    private $user = DB_USER;
    private $password = DB_SENHA;
    private $port = PORTNUMBER;

    public function retorna($dados, $parametro, $db)
    {
        switch ($parametro) {
            case "ssmPendente":
                return $this->getSsmParaSsp($dados, $db);
                break;
            case "sistema":
                return $this->getSistemaPorGrupo($dados, $db);
                break;
            case "getGrupoServico":
                return $this->getGrupoServico($dados, $db);
                break;
            case "getGrupoServicoMr":
                return $this->getGrupoServicoMr($dados, $db);
                break;
            case "subsistema":
                return $this->getSubPorSistema($dados, $db);
                break;
            case "equipe":
                return $this->getEquipePorLocal($dados, $db);
                break;
            case "ssmProgramada":
                return $this->getSspStatusProgramada($dados, $db);
                break;
            case "ssmServico":
                return $this->getSspStatusServico($dados, $db);
                break;
            case "matricula":
                return $this->getFuncPorMat($dados, $db);
                break;
            case "linhaVia":
                return $this->getViaPorLinha($dados, $db);
                break;
            case "linhaEstacao":
                return $this->getEstacaoPorLinha($dados, $db);
                break;
            case "linhaTrecho":
                return $this->getTrechoPorLinha($dados, $db);
                break;
            case "linhaPn":
                return $this->getPontoPorLinha($dados, $db);
                break;
            case "trechoPn":
                return $this->getPontoPorTrecho($dados, $db);
                break;
            case "subLocal":
                return $this->getSubLocal($dados, $db);
                break;
            case "pmp":
                return $this->getDadosPmpEdificacao($dados, $db);
                break;
            case "getSafByCod":
                return $this->getSafByCod($dados, $db);
                break;
            case "getDadoSparkline":
                return $this->getCountSparkline($db);
                break;
            case "getDadoLineChart":
                return $this->getCountLineChartMonth($db);
                break;
            case "getDadoLineChartSaf":
                return $this->getCountLineChartSaf($db);
                break;
            case "getDadoLineChartSsm":
                return $this->getCountLineChartSsm($db);
                break;
            case "getDadoLineChartOsm":
                return $this->getCountLineChartOsm($db);
                break;
            case "getDadosPizza":
                return $this->getDataPizzaSistema($dados, $db);
                break;
            case "getServicoPreventivo":
                return $this->getServicoPreventivo($dados, $db);
                break;
            case "codMaterial":
                return $this->getMaterialPorCodigo($dados, $db);
                break;
            case "unidEquipe":
                return $this->getEquipePorUnidade($dados, $db);
                break;
            case "tipoPendencia":
                return $this->getPendenciaPorTipo($dados, $db);
                break;
            case "getSspCalendario":
                return $this->getSspPorAnoCalendario($dados, $db);
                break;
            case "getSemelhanteSaf":
                return $this->getSemelhanteSaf($dados, $db);
                break;
            case "validatePost":
                return $this->validade_post($dados);
                break;
            case "getTipoManutencaoProcedimento":
                return $this->getTipoManutencaoProcedimento($dados, $db);
                break;
            case "getServicoPmpSubsistema":
                return $this->getServicoPmpSubsistema($dados, $db);
                break;
            case "getServicoPeriodicidade":
                return $this->getServicoPeriodicidade($dados, $db);
                break;
            case "getPeriodicidadeProcedimento":
                return $this->getPeriodicidadeProcedimento($dados, $db);
                break;
            case "getNumberEngSupervisao":
                return $this->getNumberEngSupervisao($dados, $db);
                break;
            case "getNumberTotalSaf":
                return $this->getNumberTotalSaf($db);
                break;
            case "getNumberTotalSsm":
                return $this->getNumberTotalSsm($db);
                break;
            case "getNumberTotalOsm":
                return $this->getNumberTotalOsm($db);
                break;
            case "getNumberTotalSsp":
                return $this->getNumberTotalSsp($db);
                break;
            case "getNumberTotalOsp":
                return $this->getNumberTotalOsp($db);
                break;
            case "getTotalDadosSistemas":
                return $this->getTotalDadosSistemas($dados, $db);
                break;
            case "getTotalSsmpEd":
                return $this->getTotalSsmpEd($db);
                break;
            case "getTotalSsmpRa":
                return $this->getTotalSsmpRa($db);
                break;
            case "getTotalSsmpSb":
                return $this->getTotalSsmpSb($db);
                break;
            case "getTotalSsmpVp":
                return $this->getTotalSsmpVp($db);
                break;
            case "getLinhaLocal":
                return $this->getLinhaLocal($dados, $db);
                break;
            case "getEstacaoAmv":
                return $this->getEstacaoAmv($dados, $db);
                break;
            case "valDuplicidadeOsm":
                return $this->valDuplicidadeOsm($dados, $db);
                break;
            case "valDuplicidadeOsp":
                return $this->valDuplicidadeOsp($dados, $db);
                break;
            case "getOsmpCronograma":
                return $this->getOsmpCronograma($dados, $db);
                break;
            case "getOsmSaf":
                return $this->getOsmSaf($dados, $db);
                break;
            case "getPmpCronograma":
                return $this->getPmpCronograma($dados, $db);
                break;
            case "getValueGrafCorretivo":
                return json_encode($this->getValueGrafCorretivo($db));
                break;
            case "getValueGrafPreventivo":
                return json_encode($this->getValueGrafPreventivo($db));
                break;
            case "getCarroTue" :
                return $this->getCarroTue($dados, $db);
                break;
            case "getValueAnualOcorrencias":
                return $this->getValueAnualOcorrencias($db);
                break;
            case "getValueApp":
                return $this->getValueApp($dados);
                break;
            case "getValueApc":
                return $this->getValueApc($dados);
                break;
            case "localPoste":
                return $this->localPoste($dados, $db);
            case "homemHora":
                return $this->homemHora($dados, $db);
                break;
            case "getListaCronograma_grafico":
                return $this->getListaCronograma_grafico($dados, $db);
                break;
            case "getComposicaoPorTipo":
                return $this->getComposicaoPorTipo($dados, $db);
                break;
            case "getAvariaGrupo":
                return $this->getAvariaGrupo($dados, $db);
                break;
            case "getVeiculo":
                return $this->getVeiculo($dados, $db);
                break;
            case "getVeiculoComp":
                return $this->getVeiculoComp($dados, $db);
                break;
            case "getCarro":
                return $this->getCarro($dados, $db);
                break;
            case "getCarroMr":
                return $this->getCarroMr($dados, $db);
                break;
            case "getVerificarCarro":
                return $this->getVerificarCarro($dados, $db);
                break;
            case "getLinhaPrefixo":
                return $this->getLinhaPrefixo($dados, $db);
                break;
            case "getFalhaSistema":
                return $this->getFalhaSistema($db);
                break;
            case "getFalhaTrem":
                return $this->getFalhaTrem($db);
                break;
            case "getFalhaMes":
                return $this->getFalhaMes($db);
                break;
            case "getTML":
                return $this->getTML($db);
                break;
            case "getNaoConstatada":
                return $this->getNaoConstatada($db);
                break;
            case "getMTTR":
                return $this->getMTTR($db);
                break;
            case "getMTBF":
                return $this->getMTBF($db);
                break;
            case "getCategoriasMaterial":
                return $this->getCategoriasMaterial($dados, $db);
                break;
            case "getTodosDadosMaterial":
                return $this->getTodosDadosMaterial($dados, $db);
                break;
            case "getTodosDadosFornecedor":
                return $this->getTodosDadosFornecedor($dados, $db);
                break;
            case "getAvaria":
                return $this->getAvaria($dados, $db);
                break;
            case "getLocalGrupo":
                return $this->getLocalGrupo($dados, $db);
                break;
            case "getHistoricoOdometro":
                return $this->getHistoricoOdometro($dados, $db);
                break;
            case "getRetornoOcorrenciaPorFrequencia":
                return $this->getRetornoOcorrenciaPorFrequencia($dados, $db);
                break;
            case "getServicoMr":
                return $this->getServicoMr($dados, $db);
                break;
            case "getFrota":
                return $this->getFrota($dados, $db);
                break;
            case "apiDisponibilidadeMR":
                return $this->apiDisponibilidadeMR($dados, $db);
                break;
            case "getQtdFalhaDia":
                return $this->getQtdFalhaDia($dados, $db);
                break;
            case "getQtdFalhaDiaPorNivel":
                return $this->getQtdFalhaDiaPorNivel($dados, $db);
                break;
            case "getVeiculoFrota":
                return $this->getVeiculoFrota($dados, $db);
            case "getAppMr":
                return $this->getAppMr($dados, $db);
                break;
            case "getFuncionariosByTipo":
                return $this->getFuncionariosByTipo($dados, $db);
                break;
            case "getListaOsmPorSsm":
                return $this->getListaOsmPorSsm($dados, $db);
                break;
            case "postValidaDevolveSsm":
                return $this->postValidaDevolveSsm($db);
                break;
            default:
                return ('Deu erro no switch');
                break;
        }
    }

    public function getEstacaoAmv($estacao, $db)
    {
        $sql = $db->prepare("SELECT * FROM amv WHERE cod_estacao = {$estacao}");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_amv'] => $dados['nome_amv']
                );
            }
        }
        return json_encode($arr);
    }

    public function getOsmpCronograma($dados, $db)
    {
        $sql = '';
        switch ($dados[1]) {
            case "Ed":
                $arr['form'] = 'ed';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, nome_status,
                            nome_linha, nome_local, homem_hora, c.quinzena

                                FROM cronograma_pmp ed

                                JOIN cronograma c USING (cod_cronograma)
                                JOIN pmp USING (cod_pmp)
                                JOIN pmp_edificacao USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING(cod_linha)

                                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                                JOIN status USING (cod_status)

                                WHERE ed.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "SELECT
                          ssmp.cod_ssmp,
                          data_programada AS inicio,
                          nome_status,
                          data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino
                          FROM cronograma_pmp
                          LEFT JOIN ssmp USING (cod_cronograma_pmp)
                          JOIN status_ssmp USING (cod_status_ssmp)
                          JOIN status USING (cod_status)
                          WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
            case "Re":
                $arr['form'] = 'ra';
                $sql = "SELECT
                          nome_sistema, nome_subsistema, nome_servico_pmp,
                          nome_procedimento, nome_periodicidade, nome_status,
                          nome_linha,
                          nome_local AS nome_local,
                          homem_hora, c.quinzena

                        FROM cronograma_pmp ra

                          JOIN cronograma c USING (cod_cronograma)
                          JOIN pmp USING (cod_pmp)
                          JOIN pmp_rede_aerea USING (cod_pmp)

                          JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                          JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                          JOIN servico_pmp USING (cod_servico_pmp)

                          JOIN sub_sistema USING (cod_sub_sistema)
                          JOIN sistema USING (cod_sistema)
                          JOIN subsistema USING (cod_subsistema)

                          JOIN procedimento USING (cod_procedimento)
                          JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                          JOIN local USING(cod_local)
                          JOIN linha USING(cod_linha)

                          JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                          JOIN status USING (cod_status)

                        WHERE ra.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "
                            SELECT ssmp.cod_ssmp,
                            data_programada AS inicio,
                            nome_status,
                            data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino
                            FROM cronograma_pmp
                            LEFT JOIN ssmp USING (cod_cronograma_pmp)
                            JOIN status_ssmp USING (cod_status_ssmp)
                            JOIN status USING (cod_status)
                            WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
            case "Vi":
                $arr['form'] = 'vp';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, nome_status,
                            nome_linha,
                            'Est. Inicial - ' || l.nome_estacao || '\nEst. Final - ' || f.nome_estacao || '\nKm Inicial - ' || p.km_inicial || ' --- Km Final - ' || p.km_final AS nome_local,
                            homem_hora, c.quinzena

                                FROM cronograma_pmp vp

                                JOIN cronograma c USING (cod_cronograma)
                                JOIN pmp USING (cod_pmp)
                                JOIN pmp_via_permanente p USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN estacao l ON l.cod_estacao = cod_estacao_inicial
                                JOIN linha USING(cod_linha)
                                JOIN estacao f ON f.cod_estacao = cod_estacao_final

                                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                                JOIN status USING (cod_status)

                                WHERE vp.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "SELECT
                            ssmp.cod_ssmp,
                            data_programada AS inicio,
                            nome_status,
                            data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino
                            FROM cronograma_pmp
                            LEFT JOIN ssmp USING (cod_cronograma_pmp)
                            JOIN status_ssmp USING (cod_status_ssmp)
                            JOIN status USING (cod_status)
                            WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
            case "Su":
                $arr['form'] = 'su';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, nome_status,
                            nome_linha, nome_local, homem_hora, c.quinzena

                                FROM cronograma_pmp su

                                JOIN cronograma c USING (cod_cronograma)
                                JOIN pmp USING (cod_pmp)
                                JOIN pmp_subestacao USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING(cod_linha)

                                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                                JOIN status USING (cod_status)

                                WHERE su.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "SELECT    ssmp.cod_ssmp, data_programada AS inicio, nome_status,
                                    data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino

                                    FROM cronograma_pmp
                                    LEFT JOIN ssmp USING (cod_cronograma_pmp)

                                    JOIN status_ssmp USING (cod_status_ssmp)
                                    JOIN status USING (cod_status)
                                    WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
            case "Ma":
                $arr['form'] = 'vlt';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, nome_status,
                            nome_linha, nome_veiculo AS nome_local, homem_hora, c.quinzena

                                FROM cronograma_pmp vlt

                                JOIN cronograma c USING (cod_cronograma)
                                JOIN pmp USING (cod_pmp)
                                JOIN pmp_vlt USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN veiculo USING(cod_veiculo)
                                JOIN linha USING(cod_linha)

                                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                                JOIN status USING (cod_status)

                                WHERE vlt.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "SELECT    ssmp.cod_ssmp, data_programada AS inicio, nome_status,
                                    data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino

                                    FROM cronograma_pmp
                                    LEFT JOIN ssmp USING (cod_cronograma_pmp)

                                    JOIN status_ssmp USING (cod_status_ssmp)
                                    JOIN status USING (cod_status)
                                    WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
            case "Te":
                $arr['form'] = 'tl';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, nome_status,
                            nome_linha, nome_local, homem_hora, c.quinzena

                                FROM cronograma_pmp tl

                                JOIN cronograma c USING (cod_cronograma)
                                JOIN pmp USING (cod_pmp)
                                JOIN pmp_telecom USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING(cod_linha)

                                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                                JOIN status USING (cod_status)

                                WHERE tl.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "SELECT    ssmp.cod_ssmp, data_programada AS inicio, nome_status,
                                    data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino

                                    FROM cronograma_pmp
                                    LEFT JOIN ssmp USING (cod_cronograma_pmp)

                                    JOIN status_ssmp USING (cod_status_ssmp)
                                    JOIN status USING (cod_status)
                                    WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
            case "Bi":
                $arr['form'] = 'bl';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, nome_status,
                            nome_linha, nome_estacao AS nome_local, homem_hora, c.quinzena

                                FROM cronograma_pmp bl

                                JOIN cronograma c USING (cod_cronograma)
                                JOIN pmp USING (cod_pmp)
                                JOIN pmp_bilhetagem USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN estacao USING(cod_estacao)
                                JOIN linha USING(cod_linha)

                                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                                JOIN status USING (cod_status)

                                WHERE bl.cod_cronograma_pmp = {$dados[0]}";

                $sqlSs = "SELECT    ssmp.cod_ssmp, data_programada AS inicio, nome_status,
                                    data_programada + ((dias_servico-1) || ' days') :: INTERVAL AS termino

                                    FROM cronograma_pmp
                                    LEFT JOIN ssmp USING (cod_cronograma_pmp)

                                    JOIN status_ssmp USING (cod_status_ssmp)
                                    JOIN status USING (cod_status)
                                    WHERE cod_cronograma_pmp = {$dados[0]}";
                break;
        }
        $sqlBd = $db->prepare($sql);
        $sqlBd->execute();

        $resultado = $sqlBd->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['sistema'] = $resultado['nome_sistema'];
        $arr['subsistema'] = $resultado['nome_subsistema'];
        $arr['servico'] = $resultado['nome_servico_pmp'];
        $arr['procedimento'] = $resultado['nome_procedimento'];
        $arr['periodicidade'] = $resultado['nome_periodicidade'];
        $arr['linha'] = $resultado['nome_linha'];
        $arr['local'] = $resultado['nome_local'];
        $arr['hh'] = $resultado['homem_hora'];
        $arr['quinzena'] = $resultado['quinzena'];
        $arr['status'] = $resultado['nome_status'];


        $sqlBdSs = $db->prepare($sqlSs);
        $sqlBdSs->execute();

        $resultadoSs = $sqlBdSs->fetchAll(PDO::FETCH_ASSOC);

        $ssms = [];

        $arr['ssmps'] = Array();
        if (!empty($resultadoSs)) {
            foreach ($resultadoSs as $dados => $value) {
                $arr['ssmps'][] = array(
                    'ssmp' => $value['cod_ssmp'],
                    'inicio' => $this->parse_timestamp_date($value['inicio']),
                    'termino' => $this->parse_timestamp_date($value['termino']),
//                    'inicio' => $this->date("d/m/Y", $value['inicio']),
//                    'termino' => $this->date("d/m/Y", $value['termino']),
                    'status' => $value['nome_status']
                );

                $ssms[] = $value['cod_ssmp'];
            }
        }

        $ssmsWhere = implode(',', $ssms);

        $sqlOs = "SELECT o.cod_osmp, cod_ssmp, data_abertura, nome_status
                    FROM osmp o
                        JOIN status_osmp USING(cod_status_osmp)
                        JOIN status USING (cod_status)

                        WHERE cod_ssmp IN({$ssmsWhere})

                        ORDER BY data_abertura";

        $sqlBdOs = $db->prepare($sqlOs);
        $sqlBdOs->execute();

        $resultadoOs = $sqlBdOs->fetchAll(PDO::FETCH_ASSOC);

        $arr['osmps'] = Array();
        if (!empty($resultadoOs)) {
            foreach ($resultadoOs as $dados => $value) {
                $arr['osmps'][] = array(
                    'osmp' => $value['cod_osmp'],
                    'ssmp' => $value['cod_ssmp'],
                    'data' => $this->parse_timestamp($value['data_abertura']),
                    'status' => $value['nome_status']
                );
            }
        }

        return json_encode($arr);
    }

    public function getListaOsmPorSsm($dados, $db)
    {
        $sql = "SELECT  osm_registro.cod_osm, atuacao.nome_atuacao, osm_registro.desc_atuacao 
                        from osm_registro 
                        INNER JOIN osm_falha ON osm_falha.cod_osm = osm_registro.cod_osm
                        INNER JOIN atuacao ON atuacao.cod_atuacao = osm_registro.cod_atuacao
                        WHERE osm_falha.cod_ssm = {$dados}
                        ORDER BY osm_registro.cod_osm";

        $sqlBd = $db->prepare($sql);
        $sqlBd->execute();

        $resultado = $sqlBd->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados => $value) {
                $arr[] = array(
                    'osm' => $value['cod_osm'],
                    'atuacao' => $value['nome_atuacao'],
                    'descricao' => $value['desc_atuacao']
                );
            }
        }

        return json_encode($arr);
    }

    public function postValidaDevolveSsm($db)
    {
        $time = date('d-m-Y H:i:s', time());
        $fuso = "-03";
        $data = $time.$fuso;

        $usuario = $this->dadosUsuario['cod_usuario'];
        $post = $_POST;
        foreach ($post as $key => $value) {
            $arr = $value;
        }

        $cod_ssm = intval($arr[0]);
        $cod_status = intval($arr[1]);

        $this->alterarStatusSsm($cod_ssm, $cod_status, $time, $this->dadosUsuario, $arr[2]);

        
        //Verificar todas as SSM's para ver se ir� ou n�o alterar o status da SAF.
        $codSaf = $this->medoo->select("v_ssm","cod_saf",["cod_ssm" => $cod_ssm]);//Recebe o c�digo da saf;
        $codSaf = $codSaf[0];

        if($cod_status == 36)
            $this->encerrarSaf($codSaf, $time, $this->dadosUsuario, 1, $arr[2]);
        else
            $this->encerrarSaf($codSaf, $time, $this->dadosUsuario, 14, $arr[2]);
            

    }

    private function alterarStatusSsm($codSsm, $status, $time, $dadosUsuario, $descricao = null){
        //FIM DE FLUXO
        $statusSsm=[7,16,26,28];

        if(in_array($status, $statusSsm))
          $ssmPendente = $this->medoo->insert("status_ssm", [
              "cod_ssm"     => (int)$codSsm,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "data_termino_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);
        else
          //insere novo status sem data t�rmino
          $ssmPendente = $this->medoo->insert("status_ssm", [
              "cod_ssm"     => (int)$codSsm,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);

        //recebe status atual
        $cod_mstatus = $this->medoo->select("ssm", "cod_mstatus", ["cod_ssm" => $codSsm]);

        //Atualiza data de t�rmino do status atual
        $updateLastStatus = $this->medoo->update("status_ssm",[
          "data_termino_status" => $time
        ], [
          "cod_mstatus" => (int)$cod_mstatus[0]
        ]);

        //Atualiza ssm para NOVO status
        $updateSsm = $this->medoo->update("ssm", [
            "cod_mstatus" => (int)$ssmPendente
        ] ,[
            "cod_ssm"     => (int)$codSsm
        ]);
    }
    private function encerrarSaf($codSaf, $time, $dadosUsuario, $status = 14, $descricao = ""){
        //Por meio da OS n�o ser� possivel encerrar a SAF, deixando-a para analise
        if( $this->verificarSsm($codSaf)){
            $this->alterarStatusSaf($codSaf, $status, $time, $dadosUsuario, $descricao);
        }
    }
    private function alterarStatusSaf($codSaf, $status, $time, $dadosUsuario, $descricao = null){
        //FIM DE FLUXO
        $statusSaf=[2,14,27];

        if(in_array($status, $statusSaf))
          $newStatusSaf = $this->medoo->insert("status_saf", [
              "cod_saf"     => (int)$codSaf,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "data_termino_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);
        else
          //insere novo status sem data t�rmino
          $newStatusSaf = $this->medoo->insert("status_saf", [
              "cod_saf"     => (int)$codSaf,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);

        //recebe status atual
        $cod_ssaf = $this->medoo->select("saf", "cod_ssaf", ["cod_saf" => $codSaf]);

        //Atualiza data de t�rmino do status atual
        $updateLastStatus = $this->medoo->update("status_saf",[
          "data_termino_status" => $time
        ], [
          "cod_ssaf"            => (int)$cod_ssaf[0]
        ]);

        //Atualiza ssm para NOVO status
        $updateSaf = $this->medoo->update("saf", [
            "cod_ssaf"    => (int)$newStatusSaf
        ] ,[
            "cod_saf"     => (int)$codSaf
        ]);
    }
    private function verificarSsm($codSaf){
        $ssms = $this->medoo->select("v_ssm","cod_status",["cod_saf" => $codSaf]);

        if(count($ssms)== 1)
            return true;


        $status = [6,7,16, 26, 35]; // DEVOLVIDA, CANCELADA, ENCERRADA, PROGRAMADA, AGUARDANDO VALIDA��O

        foreach($ssms as $dados => $value){
            //Se for diferente de DEVOLVIDA, CANCELADA, PROGRAMADA ou ENCERRADA,
            //N�O altera a SAF
            if(!in_array($value, $status)){
                return false;
            }
        }
        return true;
    }

    public function getOsmSaf($dados, $db)
    {
        $sql = "SELECT  nome, data_abertura, nome_grupo, nome_sistema, nome_subsistema,
                        nome_avaria, nivel, complemento_falha,
                        nome_linha, descricao_trecho, nome_ponto, complemento_local, nome_status, descricao_status

                        FROM v_saf
                        WHERE cod_saf = {$dados}";

        $sqlSs = "SELECT cod_ssm, nome_servico, nome_status, complemento_servico, nome_equipe, nome_unidade
                        FROM v_ssm
                        WHERE cod_saf = {$dados}";

        $sqlBd = $db->prepare($sql);
        $sqlBd->execute();

        $resultado = $sqlBd->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $dataAbertura = MainController::parse_timestamp_static($resultado['data_abertura']);

        $arr['solicitante'] = $resultado['nome'];
        $arr['dataAbertura'] = $dataAbertura;
        $arr['grupo'] = $resultado['nome_grupo'];
        $arr['sistema'] = $resultado['nome_sistema'];
        $arr['subsistema'] = $resultado['nome_subsistema'];
        $arr['avaria'] = $resultado['nome_avaria'];
        $arr['nivel'] = $resultado['nivel'];
        $arr['complementoAvaria'] = $resultado['complemento_falha'];
        $arr['linha'] = $resultado['nome_linha'];
        $arr['trecho'] = $resultado['descricao_trecho'];
        $arr['pontoNotavel'] = $resultado['nome_ponto'];
        $arr['complementoLocal'] = $resultado['complemento_local'];
        $arr['statusSaf'] = $resultado['nome_status'];
        $arr['descricaoStatus'] = $resultado['descricao_status'];

        $sqlBdSs = $db->prepare($sqlSs);
        $sqlBdSs->execute();

        $resultadoSs = $sqlBdSs->fetchAll(PDO::FETCH_ASSOC);

        $ssms = [];

        $arr['ssms'] = Array();
        if (!empty($resultadoSs)) {
            foreach ($resultadoSs as $dados => $value) {
                if (!$value['nome_servico']) $value['nome_servico'] = '';
                if (!$value['complemento_servico']) $value['complemento_servico'] = '';
                if (!$value['nome_equipe']) $value['nome_equipe'] = '';
                if (!$value['nome_unidade']) $value['nome_unidade'] = '';

                $arr['ssms'][] = array(
                    'ssm' => $value['cod_ssm'],
                    'status' => $value['nome_status'],
                    'servico' => $value['nome_servico'],
                    'complementoServico' => $value['complemento_servico'],
                    'equipe' => $value['nome_equipe'],
                    'unidade' => $value['nome_unidade']
                );

                $ssms[] = $value['cod_ssm'];
            }
        }

        $ssmsWhere = implode(',', $ssms);

        $sqlOs = "SELECT o.cod_osm, cod_ssm, data_abertura, nome_status
                    FROM osm_falha o
                    JOIN status_osm USING(cod_ostatus)
                    JOIN status USING (cod_status)

                    WHERE cod_ssm IN({$ssmsWhere})

                    ORDER BY data_abertura";

        $sqlBdOs = $db->prepare($sqlOs);
        $sqlBdOs->execute();

        $resultadoOs = $sqlBdOs->fetchAll(PDO::FETCH_ASSOC);

        $arr['osms'] = Array();
        if (!empty($resultadoOs)) {
            foreach ($resultadoOs as $dados => $value) {
                $arr['osms'][] = array(
                    'osm' => $value['cod_osm'],
                    'ssm' => $value['cod_ssm'],
                    'data' => $this->parse_timestamp($value['data_abertura']),
                    'status' => $value['nome_status']
                );
            }
        }

        return json_encode($arr);
    }

    public function getPmpCronograma($dados, $db)
    {
        $sql = '';
        switch ($dados[1]) {
            case "Ed":
                $arr['form'] = 'ed';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, nome_local, homem_hora, quinzena

                                FROM pmp_edificacao
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING(cod_linha)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_edificacao
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Re":
                $arr['form'] = 'ra';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, concat('Local: ', nome_local, '\nPoste: ', nome_poste) AS nome_local,
                            homem_hora, quinzena

                                FROM pmp_rede_aerea
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING (cod_linha)
                                LEFT JOIN poste USING(cod_poste)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_rede_aerea
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Vi":
                $arr['form'] = 'vp';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha,
                            concat('Est. Inicial - ', l.nome_estacao, '\nEst. Final - ', f.nome_estacao, '\nKm Inicial - ', km_inicial, ' --- Km Final - ', km_final) AS nome_local,
                            homem_hora, quinzena

                                FROM pmp_via_permanente
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN estacao l ON l.cod_estacao = cod_estacao_inicial
                                JOIN linha USING(cod_linha)
                                JOIN estacao f ON f.cod_estacao = cod_estacao_final

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_via_permanente
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Su":
                $arr['form'] = 'su';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, nome_local, homem_hora, quinzena

                                FROM pmp_subestacao
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING(cod_linha)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_subestacao
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Ma":
                $arr['form'] = 'vlt';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, concat('Veiculo - ',nome_veiculo) AS nome_local, homem_hora, quinzena

                                FROM pmp_vlt
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN veiculo USING(cod_veiculo)
                                JOIN linha USING(cod_linha)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_vlt
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Te":
                $arr['form'] = 'tl';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, nome_local, homem_hora, quinzena

                                FROM pmp_telecom
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN local USING(cod_local)
                                JOIN linha USING(cod_linha)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_telecom
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Bi":
                $arr['form'] = 'bl';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, nome_estacao AS nome_local, homem_hora, quinzena

                                FROM pmp_bilhetagem
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN estacao USING(cod_estacao)
                                JOIN linha USING(cod_linha)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_bilhetagem
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
            case "Ja":
                $arr['form'] = 'jd';
                $sql = "SELECT
                            nome_sistema, nome_subsistema, nome_servico_pmp,
                            nome_procedimento, nome_periodicidade, ativo,
                            nome_linha, nome_estacao AS nome_local, homem_hora, quinzena

                                FROM pmp_jardins
                                JOIN pmp USING (cod_pmp)

                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN servico_pmp USING (cod_servico_pmp)

                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)

                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)

                                JOIN estacao USING(cod_estacao)
                                JOIN linha USING(cod_linha)

                                WHERE cod_pmp = {$dados[0]}";

                $sqlCrono = "SELECT c.cod_cronograma_pmp AS cod_cronograma, cronograma.quinzena, mes, ano, nome_status FROM pmp_jardins
                                    JOIN cronograma_pmp c USING(cod_pmp)
                                    JOIN cronograma USING(cod_cronograma)

                                    JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                    JOIN status USING (cod_status)

                                    WHERE cod_pmp = {$dados[0]}";
                break;
        }
        $sqlBd = $db->prepare($sql);
        $sqlBd->execute();

        $resultado = $sqlBd->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['sistema'] = $resultado['nome_sistema'];
        $arr['subsistema'] = $resultado['nome_subsistema'];
        $arr['servico'] = $resultado['nome_servico_pmp'];
        $arr['procedimento'] = $resultado['nome_procedimento'];
        $arr['periodicidade'] = $resultado['nome_periodicidade'];
        $arr['linha'] = $resultado['nome_linha'];
        $arr['local'] = $resultado['nome_local'];
        $arr['hh'] = $resultado['homem_hora'];
        $arr['quinzena'] = $resultado['quinzena'];

        switch ($resultado['ativo']) {
            case "A":
                $arr['ativo'] = "Ativo";
                break;
            case "E":
                $arr['ativo'] = "Aberto";
                break;
            case "D":
                $arr['ativo'] = "Depreciado";
                break;
        }

        $sqlBdCrono = $db->prepare($sqlCrono);
        $sqlBdCrono->execute();

        $resultadoCrono = $sqlBdCrono->fetchAll(PDO::FETCH_ASSOC);

        $arr['cronogramas'] = Array();
        if (!empty($resultadoCrono)) {
            foreach ($resultadoCrono as $dados => $value) {
                $arr['cronogramas'][] = array(
                    'cronograma' => $value['cod_cronograma'],
                    'quinzena' => $value['quinzena'],
                    'mes' => $value['mes'],
                    'ano' => $value['ano'],
                    'status' => $value['nome_status']
                );
            }
        }

        return json_encode($arr);
    }

    public function getSistemaPorGrupo($grupo, $db)
    {
        if ($grupo == "*") {
            $sql = $db->prepare("SELECT * FROM grupo_sistema JOIN sistema USING(cod_sistema) JOIN grupo USING(cod_grupo) ORDER BY nome_sistema asc");
        } else {
            $sql = $db->prepare("SELECT * FROM grupo_sistema JOIN sistema USING(cod_sistema) JOIN grupo USING(cod_grupo) WHERE cod_grupo={$grupo} ORDER BY nome_sistema asc");
        }
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);


        //Iniciado Array com o nome do sistema pois, por AJAX, a ordena��o � feita pelo primeiro campo.
        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['nome_sistema'] => $dados['cod_sistema']
                );
            }
        }

        return json_encode($arr);
    }

    public function getGrupoServico($grupo, $db)
    {

        $sql = $db->prepare("SELECT * FROM servico_pmp WHERE cod_grupo='{$grupo}'");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_servico_pmp'] => $dados['nome_servico_pmp']
                );
            }
        }
        return json_encode($arr);
    }
    public function getGrupoServicoMr($grupo, $db)
    {

        $sql = $db->prepare("SELECT * FROM servico_material_rodante WHERE cod_grupo='{$grupo}'");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_servico_material_rodante'] => $dados['nome_servico_material_rodante']
                );
            }
        }
        return json_encode($arr);
    }

    public function getSubPorSistema($sistema, $db)
    {
        if ($sistema == "*") {
            $sql = $db->prepare("SELECT * FROM sub_sistema JOIN sistema USING(cod_sistema) JOIN subsistema USING(cod_subsistema) ORDER BY nome_subsistema");
        } else {
            $sql = $db->prepare("SELECT * FROM sub_sistema JOIN sistema USING(cod_sistema) JOIN subsistema USING(cod_subsistema) WHERE cod_sistema={$sistema} ORDER BY nome_subsistema");
        }
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['nome_subsistema'] => $dados['cod_subsistema']
                );
            }
        }
        return json_encode($arr);
    }

    public function getEquipePorLocal($local, $db)
    {
        if ($local == "*") {
            $sql = $db->prepare("SELECT * FROM un_equipe JOIN equipe USING(cod_equipe)");
        } else {
            $sql = $db->prepare("SELECT * FROM un_equipe JOIN equipe USING(cod_equipe) WHERE cod_unidade={$local}");
        }
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['sigla'] => $dados['nome_equipe']
                );
            }
        }
        return json_encode($arr);
    }

    public function getLinhaLocal($linha, $db)
    {
        if ($linha['1']) {
            switch ($linha['1']) {
                case 21: // Edifica��o
                    $tipoGrupo = 'E';
                    break;
                case 24: // Via Permanente
                    $tipoGrupo = 'V';
                    break;
                case 20: // Rede Aerea
                    $tipoGrupo = 'R';
                    break;
                case 25: // Subesta��o
                    $tipoGrupo = 'S';
                    break;
                case 26: // Bilhetagem
                    $tipoGrupo = 'B';
                    break;
                case 27: // Telecom
                    $tipoGrupo = 'T';
                    break;
            }
            $sql = $db->prepare("SELECT cod_local, nome_local FROM local WHERE cod_linha = {$linha['0']} AND grupo = '{$tipoGrupo}'");
        } else {
            $sql = $db->prepare("SELECT cod_local, nome_local FROM local WHERE cod_linha = {$linha}");
        }
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_local'] => $dados['nome_local']
                );
            }
        }

        return json_encode($arr);
    }

    public function getSsmParaSsp($ssm, $db)
    {
        $sql = $db->prepare("SELECT * FROM v_ssm WHERE cod_ssm={$ssm}");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr = Array();
        if (!empty($resultado) && $resultado['nome_status'] == "Pendente") {
            $arr['validade'] = true;
        } else {
            $arr['validade'] = false;
        }

        return json_encode($arr);
    }

    public function getSspStatusProgramada($ssp, $db)
    {
        $sql = $db->prepare("SELECT * FROM v_ssp WHERE cod_ssp={$ssp}");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr = Array();
        if (!empty($resultado) && $resultado['tipo_orissp'] == "3") {
            $arr['validade'] = true;
        } else {
            $arr['validade'] = false;
        }

        return json_encode($arr);
    }

    public function getSspStatusServico($ssp, $db)
    {
        $sql = $db->prepare("SELECT * FROM v_ssp WHERE cod_ssp={$ssp}");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr = Array();
        if (!empty($resultado) && $resultado['tipo_orissp'] == "4") {
            $arr['validade'] = true;
        } else {
            $arr['validade'] = false;
        }

        return json_encode($arr);
    }

    public function getViaPorLinha($linha, $db)
    {
        if ($linha == "*") {
            $sql = $db->prepare("SELECT cod_via, nome_via FROM via ORDER BY nome_via ");
        } else {
            $sql = $db->prepare("SELECT cod_via, nome_via FROM via WHERE cod_linha = {$linha} ORDER BY nome_via ");
        }
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_via'] => $dados['nome_via']
                );
            }
        }
        return json_encode($arr);
    }

    public function getEstacaoPorLinha($linha, $db)
    {
        if ($linha)
            $sql = $db->prepare("SELECT cod_estacao, nome_estacao FROM estacao WHERE cod_linha = {$linha} ORDER BY nome_estacao ");
        else
            $sql = $db->prepare("SELECT cod_estacao, nome_estacao FROM estacao ORDER BY nome_estacao ");

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_estacao'] => $dados['nome_estacao']
                );
            }
        }
        return json_encode($arr);
    }

    public function getTrechoPorLinha($linha, $db)
    {
        if ($linha == "*") {
            $sql = $db->prepare("SELECT cod_trecho, nome_trecho, descricao_trecho FROM trecho ORDER BY nome_trecho ");
        } else {
            $sql = $db->prepare("SELECT cod_trecho, nome_trecho, descricao_trecho FROM trecho WHERE cod_linha = {$linha} ORDER BY nome_trecho ");
        };

        $sql->execute();

        //$resultado = $sql->fetch(PDO::FETCH_ASSOC);
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {

                $siglaTrecho = $dados['nome_trecho'] . " - " . $dados['descricao_trecho'];
                $arr += array(
                    $dados['cod_trecho'] => $siglaTrecho
                );
            }
        }

        return json_encode($arr);
    }

    public function getPontoPorLinha($linha, $db)
    {
        if ($linha == "*") {
            $sql = $db->prepare("SELECT cod_ponto_notavel, nome_ponto FROM ponto_notavel ORDER BY nome_ponto");
        } else {
            $sql = $db->prepare("SELECT cod_ponto_notavel, nome_ponto FROM ponto_notavel JOIN trecho USING(cod_trecho) WHERE cod_linha = {$linha} ORDER BY nome_ponto");
        }

        $sql->execute();

        //$resultado = $sql->fetch(PDO::FETCH_ASSOC);
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_ponto_notavel'] => $dados['nome_ponto']
                );
            }
        }
        return json_encode($arr);
    }

    public function getPontoPorTrecho($trecho, $db)
    {
        if ($trecho == "*") {
            $sql = $db->prepare("SELECT cod_ponto_notavel, nome_ponto FROM ponto_notavel ORDER BY nome_ponto");
        } else {
            $sql = $db->prepare("SELECT cod_ponto_notavel, nome_ponto FROM ponto_notavel WHERE cod_trecho = {$trecho} ORDER BY nome_ponto");
        }

        $sql->execute();

        //$resultado = $sql->fetch(PDO::FETCH_ASSOC);
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_ponto_notavel'] => $dados['nome_ponto']
                );
            }
        }
        return json_encode($arr);
    }

    public function getSubLocal($local, $db){
        $sql = $db->prepare("SELECT * FROM local_sublocal_grupo JOIN sublocal_grupo USING(cod_sublocal_grupo) WHERE cod_local_grupo = {$local} ORDER BY nome_sublocal_grupo");

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_sublocal_grupo'] => $dados['nome_sublocal_grupo']
                );
            }
        }
        return json_encode($arr);
    }

    public function getFuncPorMat($matricula, $db)
    {
        if(!is_array($matricula)) {
            if (strlen($matricula))
                $prefix = substr($matricula, 0, 3);
            else
                $prefix = '';

            $arr = Array();
            $sql = $db->prepare("SELECT f.nome_funcionario, f.cpf_cnpj as cpf, cf.celular
                                FROM funcionario_empresa fe
                                JOIN funcionario f USING(cod_funcionario)
                                LEFT JOIN contato_funcionario cf ON(cf.cod_funcionario = f.cod_funcionario)
                                WHERE fe.matricula = '{$matricula}' AND f.ativo='s'");

            $sql->execute();
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);

            if (!empty($resultado)) {
                $arr['nome'] = $resultado['nome_funcionario'];
                $arr['cpf'] = $resultado['cpf'];
                $arr['celular'] = $resultado['celular'];
                $arr['statusPesquisa'] = true;

            } else {
                $arr['nome'] = 'Nao encontrado';
                $arr['statusPesquisa'] = false;
            }
        }else{
            $sql = $db->prepare("SELECT f.nome_funcionario, f.cpf_cnpj as cpf, cf.celular, fe.matricula
                                FROM funcionario_empresa fe
                                JOIN funcionario f USING(cod_funcionario)
                                LEFT JOIN contato_funcionario cf ON(cf.cod_funcionario = f.cod_funcionario)
                                WHERE fe.matricula = '{$matricula[1]}' AND fe.cod_tipo_funcionario = {$matricula[0]} AND f.ativo='s'");

            $sql->execute();
            $resultado = $sql->fetch(PDO::FETCH_ASSOC);

            if (!empty($resultado)) {
                $arr['nome'] = $resultado['nome_funcionario']. ' - '. $resultado['matricula'];
                $arr['cpf'] = $resultado['cpf'];
                $arr['celular'] = $resultado['celular'];
                $arr['statusPesquisa'] = true;

            } else {
                $arr['nome'] = 'Nao encontrado';
                $arr['statusPesquisa'] = "false";
            }
        }



        return json_encode($arr);
    }

    public function getDadosPmpEdificacao($numeroPmp, $db)
    {
        $sql = $db->prepare("SELECT * FROM pmp_edificacao
	                          JOIN servico_preventivo USING (cod_servico_prev)
	                          JOIN procedimento USING (cod_procedimento)
	                          WHERE cod_pmp_edificacao = {$numeroPmp}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $periodicidade = Array(
            '1' => 'Quinzenal',
            '2' => 'Mensal',
            '6' => 'Trimestral',
            '12' => 'Anual'
        );

        $arr['periodicidade'] = $periodicidade[$resultado['quant_intervalos']];
        $arr['qzn'] = $resultado['qzn'];
        $arr['procedimento'] = $resultado['nome_procedimento'];

        return json_encode($arr);
    }

    public function getCountSparkline($db)
    {
        $time = date('Y/m/d', time());
        //Recebe contador de todas as safs abertas do dia.

        $resultadoFinal = Array();

        //Recebe o valor total de SAFs abertas no dia.
        $countSafAberta = Array();
        for ($i = 0; $i < 10; $i++) {
            $end = date('Y/m/d', strtotime('-' . $i . ' days', strtotime($time)));
            $sql = $db->prepare("SELECT DISTINCT ON (cod_saf)*
								FROM status_saf
								JOIN status USING(cod_status)
								WHERE cod_status = 4 and data_status > '{$end} 00:00:00' and data_status < '{$end} 23:59:59'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado)
                $countSafAberta [] = count($resultado);
            else
                $countSafAberta [] = 0;
        }

        //Recebe o valor total de SAFs encerradas no dia.
        $countSafEncerrada = Array();
        for ($i = 0; $i < 10; $i++) {
            $end = date('Y/m/d', strtotime('-' . $i . ' days', strtotime($time)));
            $sql = $db->prepare("SELECT DISTINCT ON (cod_saf)*
								FROM status_saf
								JOIN status USING(cod_status)
								WHERE cod_status = 14 and data_status > '{$end} 00:00:00' and data_status < '{$end} 23:59:59'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado)
                $countSafEncerrada [] = count($resultado);
            else
                $countSafEncerrada [] = 0;
        }

        //Recebe o valor total de SSPs encerradas no dia.
        $countSspEncerrada = Array();
        for ($i = 0; $i < 10; $i++) {
            $end = date('Y/m/d', strtotime('-' . $i . ' days', strtotime($time)));
            $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
								FROM status_ssp
								JOIN status USING(cod_status)
								WHERE cod_status = 18 and data_status > '{$end} 00:00:00' and data_status < '{$end} 23:59:59'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado)
                $countSspEncerrada [] = count($resultado);
            else
                $countSspEncerrada [] = 0;
        }

        //Recebe o valor total de SSPs abertas no m�.
        $countPreventivaMes = Array();
        $year = date('Y', strtotime($time));
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
								FROM ssp
								JOIN status_ssp USING(cod_ssp)
								WHERE cod_status = 9 and data_programada > '{$year}-{$i}-01 00:00:00' and data_programada < '{$year}-{$nextMonth}-01 23:59:59'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado)
                $countPreventivaMes [] = count($resultado);
            else
                $countPreventivaMes [] = 0;
        }

        $resultadoFinal['contadorSafAberta'] = $countSafAberta;
        $resultadoFinal['contadorSafEncerrada'] = $countSafEncerrada;
        $resultadoFinal['contadorSspEncerrada'] = $countSspEncerrada;
        $resultadoFinal['contadorPreventivaMes'] = $countPreventivaMes;

        return json_encode($resultadoFinal);
    }

    public function getCountLineChartMonth($db)
    {

        $year = date('Y', time());
        $maxYaxis = 0;
        $countFalha = Array();
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_osm)*
								FROM status_osm
								JOIN status USING(cod_status)
								WHERE cod_status = 11 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['corretiva'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['corretiva'][] = $count;
                unset($count);
            }
        }
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_osp)*
								FROM status_osp
								JOIN status USING(cod_status)
								WHERE cod_status = 11 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['preventiva'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['preventiva'][] = $count;
                unset($count);
            }
        }

        $countFalha['maxYAxis'] = $maxYaxis;
        return json_encode($countFalha);
    }

    public function getCountLineChartSaf($db)
    {

        $year = date('Y', time());
        $maxYaxis = 0;
        $countFalha = Array();
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_saf)*
								FROM status_saf
								JOIN status USING(cod_status)
								WHERE cod_status = 4 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
//                $count[] = $this->month[$i];
//                $count[] = count($resultado);
                $countFalha['aberta'][] = count($resultado);
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['aberta'][] = $count;
                unset($count);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_saf)*
								FROM status_saf
								JOIN status USING(cod_status)
								WHERE cod_status = 14 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['encerrada'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['encerrada'][] = $count;
                unset($count);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_saf)*
								FROM status_saf
								JOIN status USING(cod_status)
								WHERE cod_status = 2 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['cancelada'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['cancelada'][] = $count;
                unset($count);
            }
        }

        $countFalha['maxYAxis'] = $maxYaxis;
        return json_encode($countFalha);
    }

    public function getCountLineChartSsm($db)
    {

        $year = date('Y', time());
        $maxYaxis = 0;
        $countFalha = Array();
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_ssm)*
								FROM status_ssm
								JOIN status USING(cod_status)
								WHERE cod_status = 16 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['encerrada'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['encerrada'][] = $count;
                unset($count);
            }
        }
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_ssm)*
								FROM status_ssm
								JOIN status USING(cod_status)
								WHERE cod_status = 7 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['cancelada'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['cancelada'][] = $count;
                unset($count);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_ssm)*
								FROM status_ssm
								JOIN status USING(cod_status)
								WHERE (cod_status = 9 or cod_status = 8) and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['aberta'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['aberta'][] = $count;
                unset($count);
            }
        }

        $countFalha['maxYAxis'] = $maxYaxis;
        return json_encode($countFalha);
    }

    public function getCountLineChartOsm($db)
    {
        $year = date('Y', time());
        $maxYaxis = 0;
        $countFalha = Array();
        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_osm)*
								FROM status_osm
								JOIN status USING(cod_status)
								WHERE cod_status = 11 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['encerrada'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['encerrada'][] = $count;
                unset($count);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_osm)*
								FROM status_osm
								JOIN status USING(cod_status)
								WHERE (cod_status = 23 or cod_status = 24 or cod_status = 25) and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['nexecutado'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['nexecutado'][] = $count;
                unset($count);
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $nextMonth = $i + 1;
            $sql = $db->prepare("SELECT DISTINCT ON (cod_osm)*
                                FROM status_osm
								JOIN status USING(cod_status)
								WHERE cod_status = 10 and data_status > '{$year}-{$i}-01 00:00:00' and data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            if ($resultado) {
                $count[] = $this->month[$i];
                $count[] = count($resultado);
                $countFalha['aberta'][] = $count;
                unset($count);

                if (count($resultado) > $maxYaxis)
                    $maxYaxis = count($resultado);

            } else {
                $count[] = $this->month[$i];
                $count[] = 0;
                $countFalha ['aberta'][] = $count;
                unset($count);
            }
        }

        $countFalha['maxYAxis'] = $maxYaxis;
        return json_encode($countFalha);
    }

    public function getDataPizzaSistema($equipe, $db)
    {
        $year = date('Y', time());
        $dados = Array();
        $nextYear = $year + 1;
        if ($equipe == "12") {
            $where = "WHERE (cod_equipe = 12 OR cod_equipe = 29 OR cod_equipe = 30)";
            $whereOsp = "WHERE (ssp.cod_equipe = 12 OR ssp.cod_equipe = 29 OR ssp.cod_equipe = 30)";
        } else {
            $where = "WHERE cod_equipe = {$equipe}";
            $whereOsp = " WHERE ssp.cod_equipe = {$equipe}";
        }

        //Recebe as programações executadas
        $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
                            FROM v_ssp
                            {$where} and data_programada > '{$year}-01-01 00:00:00'
                            and data_programada < '{$nextYear}-01-01 00:00:00' and nome_status='Encerrada'");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        if ($resultado) {
            $count = array(
                'label' => "Executadas",
                'data' => count($resultado)
            );
            $dados[] = $count;
            unset($count);
        } else {
            $count = array(
                'label' => "Executadas",
                'data' => 0
            );
            $dados[] = $count;
            unset($count);
        }

        //Recebe as programações pendentes
        $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
                            FROM v_ssp
                            {$where} and data_programada > '{$year}-01-01 00:00:00'
                            and data_programada < '{$nextYear}-01-01 00:00:00' and nome_status='Pendente'");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        if ($resultado) {
            $count = array(
                'label' => "Pendentes",
                'data' => count($resultado)
            );
            $dados[] = $count;
            unset($count);
        } else {
            $count = array(
                'label' => "Pendentes",
                'data' => 0
            );
            $dados[] = $count;
            unset($count);
        }

        //Recebe as programações executadas
        $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
                            FROM v_ssp
                            {$where} and data_programada > '{$year}-01-01 00:00:00'
                            and data_programada < '{$nextYear}-01-01 00:00:00' and nome_status='Programado'");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        if ($resultado) {
            $count = array(
                'label' => "Programadas",
                'data' => count($resultado)
            );
            $dados[] = $count;
            unset($count);
        } else {
            $count = array(
                'label' => "Programadas",
                'data' => 0
            );
            $dados[] = $count;
            unset($count);
        }

        //Recebe o total de serviços cancelados
        $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
                            FROM v_ssp
                            {$where} and data_programada > '{$year}-01-01 00:00:00'
                            and data_programada < '{$nextYear}-01-01 00:00:00' and nome_status='Cancelada'");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $count = array(
                'label' => utf8_encode("Cancelados"),
                'data' => count($resultado)
            );
            $dados[] = $count;
            unset($count);
        } else {
            $count = array(
                'label' => utf8_encode("Cancelados"),
                'data' => 0
            );
            $dados[] = $count;
            unset($count);
        }

        //Recebe o total de serviços cancelados
        $sql = $db->prepare("SELECT DISTINCT ON (cod_ssp)*
                            FROM v_ssp
                            {$where} and data_programada > '{$year}-01-01 00:00:00'
                            and data_programada < '{$nextYear}-01-01 00:00:00' and nome_status='Autorizada'");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $count = array(
                'label' => utf8_encode("Em execu��o"),
                'data' => count($resultado)
            );
            $dados[] = $count;
            unset($count);
        } else {
            $count = array(
                'label' => utf8_encode("Em execu��o"),
                'data' => 0
            );
            $dados[] = $count;
            unset($count);
        }

        return json_encode($dados);
    }

    public function getMaterialPorCodigo($material, $db)
    {
        if ($material == "*")
            $sql = $db->prepare("SELECT * FROM v_material");
        else
            $sql = $db->prepare("SELECT * FROM v_material WHERE cod_material={$material}");

        $sql->execute();

        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        //$resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($resultado)) {
            return json_encode($resultado);
        }

    }

    public function getEquipePorUnidade($unidade, $db)
    {
        if ($unidade == "*")
            $sql = $db->prepare("SELECT * FROM un_equipe JOIN equipe USING (cod_equipe)");
        else
            $sql = $db->prepare("SELECT * FROM un_equipe JOIN equipe USING (cod_equipe) WHERE cod_unidade = {$unidade}");

        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {

                $codigoEquipe = $dados['cod_equipe'];
                $nomeEquipe = $dados['nome_equipe'];
                $arr += array($codigoEquipe => $nomeEquipe);
            }
        } else {
            $arr[] = array("resultado" => "vazio");
        }
        return json_encode($arr);
        //echo (var_dump($arr));
    }

    public function getPendenciaPorTipo($tipoPendencia, $db)
    {
        if ($tipoPendencia == "*")
            $sql = $db->prepare("SELECT * FROM pendencia ORDER BY nome_pendencia ");
        else
            $sql = $db->prepare("SELECT * FROM pendencia WHERE cod_tipo_pendencia = {$tipoPendencia} ORDER BY nome_pendencia ");

        $sql->execute();

        //$resultado = $sql->fetch(PDO::FETCH_ASSOC);
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_pendencia'] => $dados['nome_pendencia']
                );
            }
        }


        return json_encode($arr);

    }

    public function getSspPorAnoCalendario($dados, $db)
    {
        if(is_array($dados)){
            if ($dados == "3-meses") {
                $ano = date('Y', time());
                $mes = date('m', time());
            } else {
                $mes = substr($dados, 0, 2);
                $ano = substr($dados, 3);
            }
        }else{
            $mes = Date('m');
            $ano = Date('Y');
        }

        if ($mes == 12) {
            $proxMes = 1;
            $proxAno = $ano + 1;

            $antMes = 11;
            $antAno = $ano;
        } else if ($mes == 1) {
            $proxMes = 2;
            $proxAno = $ano;

            $antMes = 12;
            $antAno = $ano - 1;
        } else {
            $proxMes = $mes + 1;
            $proxAno = $ano;

            $antMes = $mes - 1;
            $antAno = $ano;
        }

        try {
            if ($dados = "3-meses"){
                $sql = "SELECT * FROM v_ssp WHERE data_programada BETWEEN '".$antAno."-".$antMes."-01 00:00:00' AND '".$proxAno."-".$proxMes."-01 00:00:00'";
                $sql = $db->prepare($sql);

            }
            else{
                $sql = "SELECT * FROM v_ssp WHERE data_programada BETWEEN '".$ano."-".$mes."-01 00:00:00' AND '".$proxAno."-".$proxMes."-01 00:00:00'";
                $sql = $db->prepare($sql);

            }
            $sql->execute();

            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return $e;
        }
        $arr = Array();

        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                /** Obs.
                 *  utf8_decode() - Transforma toda a string na mesma codificação
                 *  para evitar conflito em  utf8_encode(), pois, a função é chamada pelo javascript que só funciona em utf-8
                 *  ( Informaçes de Banco já vem em utf-8, enquanto o sistema é ISO-8859-1 )
                 **/

                $title = utf8_decode($dados['cod_ssp'] . " - " . $dados['nome_servico']);
                $descr = '<div class="row">
								<div class="col-md-4">
									<label>Codigo Ssp</label>
									<input disabled type="text" name="codigoSspCallendar" value="' . $dados['cod_ssp'] . '" class="form-control">
								</div>
								<div class="col-md-8">
									<label>Status</label>
									<input disabled type="text" value="' . utf8_decode($dados['nome_status']) . '" class="form-control">
								</div>
							</div>
							<label>Tipo Intervenção</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_tipo_intervencao']) . '" class="form-control">
							<label>Local</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_trecho'] . ' - ' . $dados['descricao_trecho']) . '" class="form-control">
							<label>Equipe</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_equipe']) . '" class="form-control">
							<label>Serviço</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_servico']) . '" class="form-control">';
                $dataProgramada = $dados['data_programada'];
                $diasServico = $dados['dias_servico'];

                $timestamp = strtotime($dataProgramada);

                $start = date('Y/m/d', $timestamp);
                $end = date('Y/m/d', strtotime('+' . $diasServico . ' days', strtotime($dataProgramada)));

                if ($dados['nome_status'] == "Encerrada" || $dados['nome_status'] == "Cancelada") {
                    $backcolor = "gray";
                } else if ($dados['tipo_orissp'] == "2") {
                    $backcolor = "red";
                } else if ($dados['nome_status'] == "Pendente") {
                    $backcolor = "blue";
                } else if ($dados['nome_status'] == "Autorizada") {
                    $backcolor = "green";
                } else {
                    $backcolor = "default";
                }

                $data = array(
                    "allDay" => true,
                    "title" => utf8_encode($title),
                    "description" => utf8_encode($descr),
                    "start" => $start,
                    "end" => $end,
                    "backgroundColor" => $backcolor
                );
                $arr[] = $data;
            }
            return json_encode($arr);
        }
        return ('Error');
    }

    public function getServicoPreventivo($sistema, $db)
    {
        $flag = "";

        switch ($sistema) {
            case "53":
                $flag = 'E';
                break;

            case "51":
                $flag = 'V';
                break;
        }
        $sql = $db->prepare("SELECT * FROM servico_preventivo WHERE tipo_area = '{$flag}' ORDER BY nome_servico_prev ");
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if ($resultado) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_servico_prev'] => $dados['nome_servico_prev']
                );
            }
        }

        return json_encode($arr);
    }

    public function validade_post()
    {

        // Cria uma variável que terá os dados do erro
        $erro = false;

        // Verifica se o POST tem algum valor
        if (!isset($_POST) || empty($_POST)) {
            $erro = 'Nada foi postado.';
            return $erro;
        }
        $ctd = 0;
        $teste[] = array();
        // Cria as variáveis dinamicamente

        foreach ($_POST as $key => $value) {
            // Remove todas as tags HTML
            // Remove os espa�os em branco do valor
            $chave = trim(strip_tags($value));
            // Verifica se tem algum valor nulo

            $pos = (strpos($key, "codigo"));
            if (empty ($value) && $pos === false) {
                $ctd++;
            }

            if ($ctd > 0)
                $erro = "H� {$ctd} campos em branco. ";
        }

        /*
        // Verifica se $site realmente existe e se é uma URL.
        // Também verifica se não existe nenhum erro anterior
        if ( ( ! isset( $site ) || ! filter_var( $site, FILTER_VALIDATE_URL ) ) && !$erro ) {
            $erro[] = 'Envie um site válido.';
        }

        // Verifica se $email realmente existe e se é um email.
        // Também verifica se não existe nenhum erro anterior
        if ( ( ! isset( $email ) || ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) && !$erro ) {
            $erro[] = 'Envie um email válido.';
        }*/

        // Se existir algum erro, mostra o erro
        if ($erro) {
            return ($erro);
        } else {
            /* Se a variável erro continuar com valor falso
             Você pode fazer o que preferir aqui, por exemplo,
             enviar para a base de dados, ou enviar um email
             Tanto faz. Vou apenas exibir os dados na tela.*/
            return ('Validação de dados com sucesso. ');
        }
    }

    public function getSemelhanteSaf($dados, $db)
    {
        $linha = $dados[0];
        $trecho = $dados[1];
        $avaria = $dados[2];
        $grupo = $dados[3];
        $sistema = $dados[4];
        $subsistema = $dados[5];

        $codSaf = $dados[6];

        $where = [];

        if ($linha) $where[] = "cod_linha = {$linha}";
        if ($trecho) $where[] = "cod_trecho = {$trecho}";
        if ($avaria) $where[] = "cod_avaria = {$avaria}";
        if ($grupo) $where[] = "cod_grupo = {$grupo}";
        if ($sistema) $where[] = "cod_sistema = {$sistema}";
        if ($subsistema) $where[] = "cod_subsistema = {$subsistema}";
        if ($codSaf) $where[] = "cod_saf != {$codSaf}";

        //$where[] = "cod_status <> 14"; //Encerrada

        if ($grupo == '22' || $grupo == '23' || $grupo == '26') {
            $where[] = "cod_status IN(1, 4)"; //Aberta ou Autorizada
        }else{
            $where[] = "data_abertura >= '" . date('Y-m-d H:i:s', strtotime("-3 days", strtotime(date('d-m-Y H:i:s')))) . "'";
        }

        $sql = "SELECT cod_saf FROM v_saf";

        if (count($where)) {
            $sql = $sql . ' WHERE ' . implode(' AND ', $where);
        }

        $sql = $db->prepare($sql);
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if (count($resultado) > 0) {
            $arr['saf'] = "";

            foreach($resultado as $dados) {
                $arr['saf'] .= $dados['cod_saf'] . " - ";
            }

            $arr['semelhante'] = false;
            $arr['quant'] = count($resultado);
        } else {
            $arr['semelhante'] = true;
            $arr['quant'] = count($resultado);
        }

        return json_encode($arr);
    }

    public function getSafByCod($dados, $db)
    {
        $sql = "SELECT * FROM v_saf WHERE cod_saf = {$dados}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode($resultado[0]);
    }

    public function getServicoPmpSubsistema($dados, $db)
    {

        $sql = $db->prepare("SELECT sp.nome_servico_pmp, sp.cod_servico_pmp
                                              FROM servico_pmp_sub_sistema sss
                                              inner join servico_pmp sp on sss.cod_servico_pmp = sp.cod_servico_pmp
                                              inner join sub_sistema ss on sss.cod_sub_sistema = ss.cod_sub_sistema
                                              WHERE ss.cod_subsistema = {$dados[1]} AND ss.cod_sistema = {$dados[0]}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_servico_pmp'] => $dados['nome_servico_pmp']
                );
            }
        }
        return json_encode($arr);

    }

    public function getServicoPeriodicidade($dados, $db)
    {

        $sql = $db->prepare("SELECT nome_periodicidade, cod_tipo_periodicidade
                                FROM servico_pmp_periodicidade
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                                JOIN servico_pmp_sub_sistema sss USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema su USING (cod_sub_sistema)
                                WHERE sss.cod_servico_pmp = {$dados[0]} AND su.cod_sistema = {$dados[1]} AND su.cod_subsistema = {$dados[2]}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_tipo_periodicidade'] => $dados['nome_periodicidade']
                );
            }
        }
        return json_encode($arr);

    }

    public function getPeriodicidadeProcedimento($dados, $db)
    {

        $sql = $db->prepare("SELECT nome_procedimento
                                FROM servico_pmp_periodicidade
                                JOIN procedimento USING (cod_procedimento)
                                JOIN servico_pmp_sub_sistema sss USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema su USING (cod_sub_sistema)
                                WHERE sss.cod_servico_pmp = {$dados[1]} AND su.cod_sistema = {$dados[2]} AND su.cod_subsistema = {$dados[3]} AND cod_tipo_periodicidade = {$dados[0]}");

        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr["procedimento"] = $resultado['nome_procedimento'];

        return json_encode($arr);
    }

    public function getNumberEngSupervisao($dados, $db)
    {
        $arrayGroup = [21=>'Ed', 24=>'Vp', 20=>'Ra', 25=>'Su', 22=>'Vlt', 27=>'Tl', 28=>'Bl'];
        $arrayStatusCronograma = [32=>'aberta', 34=>'execucao', 21=>'autorizada', 22=>'pendente', 19=>'programada', 18=>'executada', 17=>'cancelada', 33=>'reprogramada'];

        foreach ($arrayGroup as $grupo) {
            $arr['ativos' . $grupo] = 0;
            foreach ($arrayStatusCronograma as $status) {
                $arr[$status . $grupo] = 0;
            }
        }

        //Retorna qtd de PMP cadastradas ATIVAS
        $sql = $db->prepare("SELECT cod_grupo, COUNT(*) FROM pmp WHERE ativo IN('A','E') GROUP BY cod_grupo");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if($resultado){
            foreach ($resultado as $value) {
                $arr['ativos' . $arrayGroup[ $value['cod_grupo'] ]] = $value['count'];
            }
        }

        if($dados)
            $where = " WHERE mes = {$dados} AND ano = " . date('Y'); // Parâmetros para consulta pelo mês
        else
            $where = " WHERE ano = " . date('Y'); // Parâmetros para consulta pelo ano


        //Retorna qtd de cronogramas PMP por status
        $sql = $db->prepare("SELECT cod_grupo, cod_status, COUNT(*) FROM cronograma_pmp
                                JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                                JOIN pmp USING(cod_pmp)
                                JOIN cronograma USING (cod_cronograma) {$where} GROUP BY cod_status, cod_grupo");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if($resultado){
            foreach ($resultado as $value) {
                $arr[$arrayStatusCronograma[ $value['cod_status'] ] . $arrayGroup[ $value['cod_grupo'] ]] = $value['count'];
            }
        }

        return json_encode($arr);
    }

    public function getNumberTotalSsm($db)
    {

        $year = date('Y', time());

        for ($i = 1; $i <= 12; $i++) {

            $nextMonth = $i + 1;

            //Retorna quantidade de Ssms Autorizadas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 5
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmAutorizada'][] = $resultado[0]['count'];
            } else {
                $arr['ssmAutorizada'][] = 0;
            }

            //Retorna quantidade de Ssms Devolvidas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 6
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmDevolvida'][] = $resultado[0]['count'];
            } else {
                $arr['ssmDevolvida'][] = 0;
            }

            //Retorna quantidade de Ssms Canceladas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 7
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmCancelada'][] = $resultado[0]['count'];
            } else {
                $arr['ssmCancelada'][] = 0;
            }

            //Retorna quantidade de Ssms Transferidas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 8
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmTransferida'][] = $resultado[0]['count'];
            } else {
                $arr['ssmTransferida'][] = 0;
            }

            //Retorna quantidade de Ssms Abertas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 9
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmAberta'][] = $resultado[0]['count'];
            } else {
                $arr['ssmAberta'][] = 0;
            }

            //Retorna quantidade de Ssms Encaminhadas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 12
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmEncaminhada'][] = $resultado[0]['count'];
            } else {
                $arr['ssmEncaminhada'][] = 0;
            }

            //Retorna quantidade de Ssms Pendentes

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 15
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmPendente'][] = $resultado[0]['count'];
            } else {
                $arr['ssmPendente'][] = 0;
            }

            //Retorna quantidade de Ssms Encerradas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 16
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmEncerrada'][] = $resultado[0]['count'];
            } else {
                $arr['ssmEncerrada'][] = 0;
            }

            //Retorna quantidade de Ssms Programadas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 26
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmProgramada'][] = $resultado[0]['count'];
            } else {
                $arr['ssmProgramada'][] = 0;
            }

            //Retorna quantidade de Ssms Agendadas

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssm s
	                              JOIN status_ssm ss ON s.cod_ssm = ss.cod_ssm
	                              WHERE cod_status = 28
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ssmAgendada'][] = $resultado[0]['count'];
            } else {
                $arr['ssmAgendada'][] = 0;
            }
        }

        return json_encode($arr);
    }

    public function getNumberTotalOsm($db)
    {

        $year = date('Y', time());

        for ($i = 1; $i <= 12; $i++) {

            $nextMonth = $i + 1;

            //Retorna quantidade de Osms Execução

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM osm_falha o
                                  JOIN status_osm so ON o.cod_osm = so.cod_osm
                                  WHERE cod_status = 10
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['osmExecucao'][] = $resultado[0]['count'];
            } else {
                $arr['osmExecucao'][] = 0;
            }

            //Retorna quantidade de Osms Encerrada

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM osm_falha o
                                  JOIN status_osm so ON o.cod_osm = so.cod_osm
                                  WHERE cod_status = 11
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['osmEncerrada'][] = $resultado[0]['count'];
            } else {
                $arr['osmEncerrada'][] = 0;
            }

            //Retorna quantidade de Osms Duplicada

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM osm_falha o
                                  JOIN status_osm so ON o.cod_osm = so.cod_osm
                                  WHERE cod_status = 24
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['osmDuplicada'][] = $resultado[0]['count'];
            } else {
                $arr['osmDuplicada'][] = 0;
            }

            //Retorna quantidade de Osms Não configura falha

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM osm_falha o
                                  JOIN status_osm so ON o.cod_osm = so.cod_osm
                                  WHERE cod_status = 25
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['osmNCFalha'][] = $resultado[0]['count'];
            } else {
                $arr['osmNCFalha'][] = 0;
            }

            //Retorna quantidade de Osms Não executada

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM osm_falha o
                                  JOIN status_osm so ON o.cod_osm = so.cod_osm
                                  WHERE cod_status = 23
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['osmNExecutada'][] = $resultado[0]['count'];
            } else {
                $arr['osmNExecutada'][] = 0;
            }
        }

        return json_encode($arr);
    }

    public function getNumberTotalSsp($db)
    {

        $year = date('Y', time());

        for ($i = 1; $i <= 12; $i++) {

            $nextMonth = $i + 1;

            //Retorna quantidade de Origem Corretiva

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssp s
                                  JOIN status_ssp ss ON s.cod_ssp = ss.cod_ssp
                                  JOIN origem_ssp os ON ss.cod_ssp = os.cod_ssp
                                  WHERE tipo_orissp = 1
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['sspCorretiva'][] = $resultado[0]['count'];
            } else {
                $arr['sspCorretiva'][] = 0;
            }

            //Retorna quantidade de Origem Preventiva

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssp s
                                  JOIN status_ssp ss ON s.cod_ssp = ss.cod_ssp
                                  JOIN origem_ssp os ON ss.cod_ssp = os.cod_ssp
                                  WHERE tipo_orissp = 2
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['sspPreventiva'][] = $resultado[0]['count'];
            } else {
                $arr['sspPreventiva'][] = 0;
            }

            //Retorna quantidade de Origem Programada

            $sql = $db->prepare("SELECT COUNT(*)
                                  FROM ssp s
                                  JOIN status_ssp ss ON s.cod_ssp = ss.cod_ssp
                                  JOIN origem_ssp os ON ss.cod_ssp = os.cod_ssp
                                  WHERE tipo_orissp = 3
                                  AND data_status > '{$year}-{$i}-01 00:00:00'
                                  AND data_status < '{$year}-{$nextMonth}-01 00:00:00'");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['sspProgramacao'][] = $resultado[0]['count'];
            } else {
                $arr['sspProgramacao'][] = 0;
            }
        }

        return json_encode($arr);
    }

    public function getNumberTotalOsp($db)
    {

        $year = date('Y', time());

        for ($i = 1; $i <= 12; $i++) {

            $nextMonth = $i + 1;

            //Retorna quantidade de Origem Corretiva

            $sql = $db->prepare("SELECT COUNT(*)
                                    FROM osp o
                                    JOIN ssp s ON o.cod_ssp = s.cod_ssp
                                    JOIN status_ssp ss ON s.cod_pstatus = ss.cod_pstatus
                                    JOIN origem_ssp os ON ss.cod_ssp = os.cod_ssp
                                    WHERE tipo_orissp = 1
                                    AND data_status > '{$year}-{$i}-01 00:00:00'
                                    AND data_status < '{$year}-{$nextMonth}-01 00:00:00'\");
            ");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ospCorretiva'][] = $resultado[0]['count'];
            } else {
                $arr['ospCorretiva'][] = 0;
            }

            //Retorna quantidade de Origem Preventiva

            $sql = $db->prepare("SELECT COUNT(*)
                                    FROM osp o
                                    JOIN ssp s ON o.cod_ssp = s.cod_ssp
                                    JOIN status_ssp ss ON s.cod_pstatus = ss.cod_pstatus
                                    JOIN origem_ssp os ON ss.cod_ssp = os.cod_ssp
                                    WHERE tipo_orissp = 2
                                    AND data_status > '{$year}-{$i}-01 00:00:00'
                                    AND data_status < '{$year}-{$nextMonth}-01 00:00:00'\");
            ");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ospPreventiva'][] = $resultado[0]['count'];
            } else {
                $arr['ospPreventiva'][] = 0;
            }

            //Retorna quantidade de Origem Programada

            $sql = $db->prepare("SELECT COUNT(*)
                                    FROM osp o
                                    JOIN ssp s ON o.cod_ssp = s.cod_ssp
                                    JOIN status_ssp ss ON s.cod_pstatus = ss.cod_pstatus
                                    JOIN origem_ssp os ON ss.cod_ssp = os.cod_ssp
                                    WHERE tipo_orissp = 3
                                    AND data_status > '{$year}-{$i}-01 00:00:00'
                                    AND data_status < '{$year}-{$nextMonth}-01 00:00:00'\");
            ");
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            if ($resultado) {
                $arr['ospProgramacao'][] = $resultado[0]['count'];
            } else {
                $arr['ospProgramacao'][] = 0;
            }
        }

        return json_encode($arr);
    }

    private function getTotalEd($db)
    {

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 32");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAbertaEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAbertaEd'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 21");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAutorizadaEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAutorizadaEd'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 17");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdCanceladaEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdCanceladaEd'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 18");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdEncerradaEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdEncerradaEd'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 33");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdExecucaoEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdExecucaoEd'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 22");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdPendenteEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdPendenteEd'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ed
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 19");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdProgramadoEd'][] = $resultado[0]['count'];
        } else {
            $arr['qtdProgramadoEd'][] = 0;
        }

        return $arr;
    }

    private function getTotalRa($db)
    {
        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 32");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAbertaRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAbertaRa'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 21");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAutorizadaRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAutorizadaRa'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 17");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdCanceladaRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdCanceladaRa'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 18");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdEncerradaRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdEncerradaRa'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 33");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdExecucaoRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdExecucaoRa'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 22");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdPendenteRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdPendenteRa'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_ra
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 19");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdProgramadoRa'][] = $resultado[0]['count'];
        } else {
            $arr['qtdProgramadoRa'][] = 0;
        }

        return ($arr);
    }

    private function getTotalSb($db)
    {
        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 32");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAbertaSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAbertaSb'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 21");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAutorizadaSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAutorizadaSb'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 17");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdCanceladaSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdCanceladaSb'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 18");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdEncerradaSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdEncerradaSb'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 33");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdExecucaoSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdExecucaoSb'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 22");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdPendenteSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdPendenteSb'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_sb
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 19");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdProgramadoSb'][] = $resultado[0]['count'];
        } else {
            $arr['qtdProgramadoSb'][] = 0;
        }

        return ($arr);
    }

    private function getTotalVp($db)
    {
        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 32");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAbertaVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAbertaVp'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 21");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdAutorizadaVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdAutorizadaVp'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 17");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdCanceladaVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdCanceladaVp'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 18");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdEncerradaVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdEncerradaVp'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 33");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdExecucaoVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdExecucaoVp'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 22");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdPendenteVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdPendenteVp'][] = 0;
        }

        $sql = $db->prepare("SELECT count(ss.cod_ssmp)
                                FROM ssmp_vp
                                JOIN ssmp sp USING(cod_ssmp)
                                JOIN status_ssmp ss ON sp.cod_status_ssmp = ss.cod_status_ssmp
                                JOIN status s ON ss.cod_status = s.cod_status
                                WHERE ss.cod_status = 19");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            $arr['qtdProgramadoVp'][] = $resultado[0]['count'];
        } else {
            $arr['qtdProgramadoVp'][] = 0;
        }

        return ($arr);
    }

    public function getTotalDadosSistemas($dados, $db)
    {
        switch ($dados) {
            case "1":
                $arr['edificacao'] = $this->getTotalEd($db);
                break;
            case "2":
                $arr['redeAerea'] = $this->getTotalRa($db);
                break;
            case "3":
                $arr['subestacao'] = $this->getTotalSb($db);
                break;
            case "4":
                $arr['viaPermanente'] = $this->getTotalVp($db);
                break;
            default:
                $arr['viaPermanente'] = $this->getTotalVp($db);
                $arr['edificacao'] = $this->getTotalEd($db);
                $arr['redeAerea'] = $this->getTotalRa($db);
                $arr['subestacao'] = $this->getTotalSb($db);

                $soma = $arr['edificacao']['dadosAberta'];
                $soma += $arr['redeAerea']['dadosAberta'];
                $soma += $arr['subestacao']['dadosAberta'];
                $soma += $arr['viaPermanente']['dadosAberta'];
                $arr['somaTotalAberta'] = $soma;

                $soma = $arr['edificacao']['dadosAutorizada'];
                $soma += $arr['redeAerea']['dadosAutorizada'];
                $soma += $arr['subestacao']['dadosAutorizada'];
                $soma += $arr['viaPermanente']['dadosAutorizada'];
                $arr['somaTotalAutorizada'] = $soma;

                $soma = $arr['edificacao']['dadosCancelada'];
                $soma += $arr['redeAerea']['dadosCancelada'];
                $soma += $arr['subestacao']['dadosCancelada'];
                $soma += $arr['viaPermanente']['dadosCancelada'];
                $arr['somaTotalCancelada'] = $soma;

                $soma = $arr['edificacao']['dadosEncerrada'];
                $soma += $arr['redeAerea']['dadosEncerrada'];
                $soma += $arr['subestacao']['dadosEncerrada'];
                $soma += $arr['viaPermanente']['dadosEncerrada'];
                $arr['somaTotalEncerrada'] = $soma;

                $soma = $arr['edificacao']['dadosExecucao'];
                $soma += $arr['redeAerea']['dadosExecucao'];
                $soma += $arr['subestacao']['dadosExecucao'];
                $soma += $arr['viaPermanente']['dadosExecucao'];
                $arr['somaTotalExecucao'] = $soma;

                $soma = $arr['edificacao']['dadosPendente'];
                $soma += $arr['redeAerea']['dadosPendente'];
                $soma += $arr['subestacao']['dadosPendente'];
                $soma += $arr['viaPermanente']['dadosPendente'];
                $arr['somaTotalPendente'] = $soma;

                $soma = $arr['edificacao']['dadosProgramado'];
                $soma += $arr['redeAerea']['dadosProgramado'];
                $soma += $arr['subestacao']['dadosProgramado'];
                $soma += $arr['viaPermanente']['dadosProgramado'];
                $arr['somaTotalProgramado'] = $soma;

                break;
        }

        return json_encode($arr);
    }

    public function getTotalSsmpEd($db)
    {
        $sql = $db->prepare("SELECT 	s.cod_ssmp,
                                        e.nome_equipe,
                                        s.data_abertura,
                                        s.data_programada,
                                        l.nome_local,
                                        s.dias_servico,
                                        st.cod_status
                                    FROM ssmp_ed s
                                JOIN ssmp USING(cod_ssmp)
                                    JOIN local l ON s.cod_local = l.cod_local
                                    JOIN un_equipe ue ON cod_un_equipe = ue.cod_un_equipe
                                    JOIN equipe e ON ue.cod_equipe = e.cod_equipe
                                    JOIN status_ssmp ss ON cod_status_ssmp = ss.cod_status_ssmp
                                    JOIN status st ON ss.cod_status = st.cod_status");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            foreach ($resultado as $dados => $value) {
                switch ($value['cod_status']) {
                    case "32":
                        $arr['abertas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "21":
                        $arr['autorizadas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "17":
                        $arr['canceladas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "18":
                        $arr['encerradas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "33":
                        $arr['execucoes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "22":
                        $arr['pendentes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "19":
                        $arr['programados'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    default:
                        $arr[] = array("dados" => false);
                }
            }
        } else {
            $arr[] = array("dados" => false);
        }

        return json_encode($arr);
    }

    public function getTotalSsmpRa($db)
    {
        $sql = $db->prepare("SELECT 	s.cod_ssmp,
                                        e.nome_equipe,
                                        s.data_abertura,
                                        s.data_programada,
                                        l.nome_local,
                                        s.dias_servico,
                                        st.cod_status
                                    FROM ssmp_ra s
                                JOIN ssmp USING(cod_ssmp)
                                    JOIN local l ON s.cod_local = l.cod_local
                                    JOIN un_equipe ue ON cod_un_equipe = ue.cod_un_equipe
                                    JOIN equipe e ON ue.cod_equipe = e.cod_equipe
                                    JOIN status_ssmp ss ON cod_status_ssmp = ss.cod_status_ssmp
                                    JOIN status st ON ss.cod_status = st.cod_status");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            foreach ($resultado as $dados => $value) {
                switch ($value['cod_status']) {
                    case "32":
                        $arr['abertas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "21":
                        $arr['autorizadas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "17":
                        $arr['canceladas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "18":
                        $arr['encerradas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "33":
                        $arr['execucoes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "22":
                        $arr['pendentes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "19":
                        $arr['programados'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    default:
                        $arr[] = array("dados" => false);
                }
            }
        } else {
            $arr[] = array("dados" => false);
        }

        return json_encode($arr);
    }

    public function getTotalSsmpSb($db)
    {
        $sql = $db->prepare("SELECT 	s.cod_ssmp,
                                        e.nome_equipe,
                                        s.data_abertura,
                                        s.data_programada,
                                        l.nome_local,
                                        s.dias_servico,
                                        st.cod_status
                                    FROM ssmp_sb s
                                JOIN ssmp USING(cod_ssmp)
                                    JOIN local l ON s.cod_local = l.cod_local
                                    JOIN un_equipe ue ON cod_un_equipe = ue.cod_un_equipe
                                    JOIN equipe e ON ue.cod_equipe = e.cod_equipe
                                    JOIN status_ssmp ss ON cod_status_ssmp = ss.cod_status_ssmp
                                    JOIN status st ON ss.cod_status = st.cod_status");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            foreach ($resultado as $dados => $value) {
                switch ($value['cod_status']) {
                    case "32":
                        $arr['abertas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "21":
                        $arr['autorizadas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "17":
                        $arr['canceladas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "18":
                        $arr['encerradas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "33":
                        $arr['execucoes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "22":
                        $arr['pendentes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "19":
                        $arr['programados'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    default:
                        $arr[] = array("dados" => false);
                }
            }
        } else {
            $arr[] = array("dados" => false);
        }

        return json_encode($arr);
    }

    public function getTotalSsmpVp($db)
    {
        $sql = $db->prepare("SELECT 	s.cod_ssmp,
                                        e.nome_equipe,
                                        s.data_abertura,
                                        s.data_programada,
                                        l.nome_local,
                                        s.dias_servico,
                                        st.cod_status
                                    FROM ssmp_vp s
                                JOIN ssmp USING(cod_ssmp)
                                    JOIN local l ON s.cod_local = l.cod_local
                                    JOIN un_equipe ue ON cod_un_equipe = ue.cod_un_equipe
                                    JOIN equipe e ON ue.cod_equipe = e.cod_equipe
                                    JOIN status_ssmp ss ON cod_status_ssmp = ss.cod_status_ssmp
                                    JOIN status st ON ss.cod_status = st.cod_status");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if ($resultado) {
            foreach ($resultado as $dados => $value) {
                switch ($value['cod_status']) {
                    case "32":
                        $arr['abertas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "21":
                        $arr['autorizadas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "17":
                        $arr['canceladas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "18":
                        $arr['encerradas'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "33":
                        $arr['execucoes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "22":
                        $arr['pendentes'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    case "19":
                        $arr['programados'][] = array(
                            "cod_ssmp" => $value['cod_ssmp'],
                            "nome_equipe" => $value['nome_equipe'],
                            "data_abertura" => $value['data_abertura'],
                            "data_programada" => $value['data_programada'],
                            "nome_local" => $value['nome_local'],
                            "dias_servico" => $value['dias_servico']
                        );
                        break;
                    default:
                        $arr[] = array("dados" => false);
                }
            }
        } else {
            $arr[] = array("dados" => false);
        }

        return json_encode($arr);
    }

    public function getDadosTueMaterialRodante($db)
    {
        //Retorna as avarias destacadas para TUE em material rodante
        $sql = $db->prepare("SELECT * FROM avaria_tue");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $arr['avariaTue'] = $resultado[0];

        //Retorna os ve�culos TUE em material rodante
        $sql = $db->prepare("SELECT * FROM carro_tue");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $arr['veiculoTue'] = $resultado[0];

        //Retorna os carros dos veiculos TUE em material rodante
        $sql = $db->prepare("SELECT * FROM pmp_edificacao");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $arr['ativosEd'] = $resultado[0]['count'];


    }

    public function valDuplicidadeOsm($dados, $db)
    {
        $sql = $db->prepare("SELECT * from osm_falha WHERE cod_osm={$dados}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $arr['validation'] = 'false';
        } else {
            $arr['validation'] = 'true';
        }

        return json_encode($arr);
    }

    public function valDuplicidadeOsp($dados, $db)
    {
        $sql = $db->prepare("SELECT * FROM osp WHERE cod_osp={$dados}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        if (empty($resultado)) {
            $arr['validation'] = 'false';
        } else {
            $arr['validation'] = 'true';
        }

        return json_encode($arr);
    }

    public function getValueGrafCorretivo($db)
    {
        $arr = Array();

        $ag_causador = [];

        if ($_POST['grupo']) {
            $where[] = "vsaf.cod_grupo = {$_POST['grupo']}";
            $whereOs[] = "grupo_atuado = {$_POST['grupo']}";

            if($_POST['grupo'] == 22 || $_POST['grupo'] == 23 || $_POST['grupo'] == 26){
                $ag_causador = "16, 21, 24, 27, 28";
            }else{
                $ag_causador = "16,28";
            }
            $grupo = $this->medoo->select("grupo", "nome_grupo", ["cod_grupo" => (int)$_POST['grupo']]);
            $grupo = $grupo[0];
            $arr['tituloFiltro'][] = $grupo;
        }

        if ($_POST['mes'] == 12) {
            $nextMes = 1;
            $ano = $_POST['ano'] + 1;
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos(MainController::$monthComplete[$_POST['mes']]));
        } else {
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos(MainController::$monthComplete[$_POST['mes']]));
            $nextMes = $_POST['mes'] + 1;
            $ano = $_POST['ano'];
        }

        if ($_POST['linha']) {
            $where[] = "vsaf.cod_linha = {$_POST['linha']}";
            $whereOs[] = "cod_linha_atuado = {$_POST['linha']}";

            $linha = $this->medoo->select("linha", "nome_linha", ["cod_linha" => (int)$_POST['linha']]);
            $linha = $linha[0];
            $arr['tituloFiltro'][] = $linha;
        }

        if ($_POST['sistema']) {
            $where[] = "vsaf.cod_sistema = {$_POST['sistema']}";
            $whereOs[] = "sistema_atuado = {$_POST['sistema']}";

            $sistema = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => (int)$_POST['sistema']]);
            $sistema = $sistema[0];
            $arr['tituloFiltro'][] = $sistema;
        }
        if ($_POST['subSistema']) {
            $where[] = "vsaf.cod_subsistema = {$_POST['subSistema']}";
            $whereOs[] = "subsistema_atuado = {$_POST['subSistema']}";

            $subSistema = $this->medoo->select("subsistema", "nome_subsistema", ["cod_subsistema" => (int)$_POST['subSistema']]);
            $subSistema = $subSistema[0];
            $arr['tituloFiltro'][] = $subSistema;
        }


        if ($where) {
            $where = " AND " . implode(' AND ', $where);
            $whereOs = " AND " . implode(' AND ', $whereOs);
            $arr['tituloFiltro'] = strtoupper($this->retiraAcentos(implode(' | ', $arr['tituloFiltro'])));
        }


        if (strlen($_POST['mes']) == 1) {
            $tempoInicial = "01/0{$_POST['mes']}/{$_POST['ano']} 00:00:00";
            $tempoFinal = "01/0{$nextMes}/{$ano} 00:00:00";
        } else {
            $tempoInicial = "01/{$_POST['mes']}/{$_POST['ano']} 00:00:00";
            $tempoFinal = "01/{$nextMes}/{$ano} 00:00:00";
        }

        //Total Abertas
        $arr['totalAbertas'] = 0;

        $sql = "SELECT COUNT(vsaf.cod_saf) AS valor
		FROM
		(
			SELECT
				date_part('day', data_abertura) AS dia,
		   		date_part('month', data_abertura) AS mes,
		   		date_part('year', data_abertura) AS ano, *
		 	FROM v_saf
		) AS vsaf

		INNER JOIN
		(
			SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
			FROM saf sa
			INNER JOIN ssm s
			ON sa.cod_saf = s.cod_saf
			GROUP BY s.cod_saf
		) AS menor_ssm
		ON vsaf.cod_saf = menor_ssm.cod_saf

		INNER JOIN ssm
		ON menor_ssm.menor = ssm.cod_ssm

		INNER JOIN
		(
			SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
			FROM ssm sm
			INNER JOIN osm_falha o
			ON sm.cod_ssm = o.cod_ssm
			GROUP BY o.cod_ssm
		) AS menor_osm
		ON ssm.cod_ssm = menor_osm.cod_ssm

		INNER JOIN osm_falha osm
		ON menor_osm.menorosm = osm.cod_osm

		INNER JOIN osm_registro osmr
		ON osmr.cod_osm = osm.cod_osm

		WHERE osmr.cod_ag_causador NOT IN ({$ag_causador}) {$where} AND vsaf.data_abertura > '{$tempoInicial}' AND vsaf.data_abertura < '{$tempoFinal}'";
        $query = $db->prepare($sql);
        $query->execute();
        $resultado = $query->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalAbertas'] = $resultado['valor'];

        $arr['totalEncerrada'] = 0;

        $sql = "SELECT COUNT(vsaf.cod_saf) AS valor
		FROM
		(
			SELECT
				date_part('day', data_abertura) AS dia,
		   		date_part('month', data_abertura) AS mes,
		   		date_part('year', data_abertura) AS ano, *
		 	FROM v_saf
		) AS vsaf

		INNER JOIN
		(
			SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
			FROM saf sa
			INNER JOIN ssm s
			ON sa.cod_saf = s.cod_saf
			GROUP BY s.cod_saf
		) AS menor_ssm
		ON vsaf.cod_saf = menor_ssm.cod_saf

		INNER JOIN ssm
		ON menor_ssm.menor = ssm.cod_ssm

		INNER JOIN
		(
			SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
			FROM ssm sm
			INNER JOIN osm_falha o
			ON sm.cod_ssm = o.cod_ssm
			GROUP BY o.cod_ssm
		) AS menor_osm
		ON ssm.cod_ssm = menor_osm.cod_ssm

		INNER JOIN osm_falha osm
		ON menor_osm.menorosm = osm.cod_osm

		INNER JOIN osm_registro osmr
		ON osmr.cod_osm = osm.cod_osm

		WHERE vsaf.cod_status = 14 AND osmr.cod_ag_causador NOT IN ({$ag_causador}) {$where} AND vsaf.data_abertura > '{$tempoInicial}' AND vsaf.data_abertura < '{$tempoFinal}'";

        $query = $db->prepare($sql);
        $query->execute();
        $resultado = $query->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalEncerrada'] += $resultado['valor'];

        //Canceladas

        $arr['totalCancelada'] = 0;

        $sql = "SELECT COUNT(vsaf.cod_saf) AS valor
		FROM
		(
			SELECT
				date_part('day', data_abertura) AS dia,
		   		date_part('month', data_abertura) AS mes,
		   		date_part('year', data_abertura) AS ano, *
		 	FROM v_saf
		) AS vsaf

		INNER JOIN
		(
			SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
			FROM saf sa
			INNER JOIN ssm s
			ON sa.cod_saf = s.cod_saf
			GROUP BY s.cod_saf
		) AS menor_ssm
		ON vsaf.cod_saf = menor_ssm.cod_saf

		INNER JOIN ssm
		ON menor_ssm.menor = ssm.cod_ssm

		INNER JOIN
		(
			SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
			FROM ssm sm
			INNER JOIN osm_falha o
			ON sm.cod_ssm = o.cod_ssm
			GROUP BY o.cod_ssm
		) AS menor_osm
		ON ssm.cod_ssm = menor_osm.cod_ssm

		INNER JOIN osm_falha osm
		ON menor_osm.menorosm = osm.cod_osm

		INNER JOIN osm_registro osmr
		ON osmr.cod_osm = osm.cod_osm

		WHERE vsaf.cod_status = 2 AND osmr.cod_ag_causador NOT IN ({$ag_causador}) {$where} AND vsaf.data_abertura > '{$tempoInicial}' AND vsaf.data_abertura < '{$tempoFinal}'";

        $query = $db->prepare($sql);
        $query->execute();
        $resultado = $query->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalCancelada'] += $resultado['valor'];

        //Aguardando Validação

        $arr['totalValidacao'] = 0;

        $sql = "SELECT COUNT(vsaf.cod_saf) AS valor
		FROM
		(
			SELECT
				date_part('day', data_abertura) AS dia,
		   		date_part('month', data_abertura) AS mes,
		   		date_part('year', data_abertura) AS ano, *
		 	FROM v_saf
		) AS vsaf

		INNER JOIN
		(
			SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
			FROM saf sa
			INNER JOIN ssm s
			ON sa.cod_saf = s.cod_saf
			GROUP BY s.cod_saf
		) AS menor_ssm
		ON vsaf.cod_saf = menor_ssm.cod_saf

		INNER JOIN ssm
		ON menor_ssm.menor = ssm.cod_ssm

		INNER JOIN
		(
			SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
			FROM ssm sm
			INNER JOIN osm_falha o
			ON sm.cod_ssm = o.cod_ssm
			GROUP BY o.cod_ssm
		) AS menor_osm
		ON ssm.cod_ssm = menor_osm.cod_ssm

		INNER JOIN osm_falha osm
		ON menor_osm.menorosm = osm.cod_osm

		INNER JOIN osm_registro osmr
		ON osmr.cod_osm = osm.cod_osm

		WHERE vsaf.cod_status = 37 AND osmr.cod_ag_causador NOT IN ({$ag_causador}) {$where} AND vsaf.data_abertura > '{$tempoInicial}' AND vsaf.data_abertura < '{$tempoFinal}'";

        $query = $db->prepare($sql);
        $query->execute();
        $resultado = $query->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalValidacao'] += $resultado['valor'];


        $arr['totalAtendimento'] = $arr['totalAbertas'] - ($arr['totalCancelada'] + $arr['totalEncerrada']) - $arr['totalValidacao'];

        if ($arr['totalAtendimento'] < 0) {
            $arr['totalRetroativa'] = $arr['totalAtendimento'] * (-1);
            $arr['totalAtendimento'] = 0;
        }

        return ($arr);
    }

    public function getValueGrafPreventivo($db)
    {

        $arr = Array();

        //Verifica��o de virada de ano.
        if ($_POST['mes'] == 12) {
            $nextMes = 1;
            $ano = $_POST['ano'] + 1;
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos(MainController::$monthComplete[$_POST['mes']])) . " ";
        } else {
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos(��MainController::$monthComplete[$_POST['mes']])) . " ";
            $nextMes = $_POST['mes'] + 1;
            $ano = $_POST['ano'];
        }

        //Verificando a necessidade de se adicionar 0 em m�s e adi��o do hor�rio.
        if (strlen($_POST['mes']) == 1) {
            $tempoInicial = "01/0{$_POST['mes']}/{$_POST['ano']} 00:00:00";
            $tempoFinal = "01/0{$nextMes}/{$ano} 00:00:00";
        } else {
            $tempoInicial = "01/{$_POST['mes']}/{$_POST['ano']} 00:00:00";
            $tempoFinal = "01/{$nextMes}/{$ano} 00:00:00";
        }

        if ($_POST['linha']) {
            $where[] = "cod_linha = {$_POST['linha']}";
            $wherePmp[] = "cod_linha = {$_POST['linha']}";
            $whereOs[] = "cod_linha_atuado = {$_POST['linha']}";

            $linha = $this->medoo->select("linha", "nome_linha", ["cod_linha" => (int)$_POST['linha']]);
            $linha = $linha[0];
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos($linha));
        }

        if ($_POST['sistema']) {
            $where[] = "cod_sistema = {$_POST['sistema']}";
            $wherePmp[] = "cod_sistema = {$_POST['sistema']}";
            $whereOs[] = "sistema_atuado = {$_POST['sistema']}";

            $sistema = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => (int)$_POST['sistema']]);
            $sistema = $sistema[0];
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos($sistema));
        }
        if ($_POST['subSistema']) {
            $where[] = "cod_subsistema = {$_POST['subSistema']}";
            $wherePmp[] = "cod_subsistema = {$_POST['subSistema']}";
            $whereOs[] = "subsistema_atuado = {$_POST['subSistema']}";

            $subSistema = $this->medoo->select("subsistema", "nome_subsistema", ["cod_subsistema" => (int)$_POST['subSistema']]);
            $subSistema = $subSistema[0];
            $arr['tituloFiltro'][] = strtoupper($this->retiraAcentos($subSistema));
        }

        if ($_POST['grupo']) {
            $where[] = "cod_grupo = {$_POST['grupo']}";
            $whereOs[] = "grupo_atuado = {$_POST['grupo']}";
        }

        if ($wherePmp) {
            $where = " AND " . implode(' AND ', $where);
            $wherePmp = " AND " . implode(' AND ', $wherePmp);
            $whereOs = " AND " . implode(' AND ', $whereOs);
            $arr['tituloFiltro'] = strtoupper($this->retiraAcentos(implode(' | ', $arr['tituloFiltro'])));
        } else {
            if ($where) {
                $where = " AND " . implode(' AND ', $where);
                $whereOs = " AND " . implode(' AND ', $whereOs);
            }
        }

        switch ($_POST['grupo']) {
            case 21:
                $selectPmpTotal = "SELECT (
                                      SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_ed USING (cod_ssmp)
                                      JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON(pe.cod_local = local.cod_local)

                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp}
                                  ) + (
                                      SELECT count(*) FROM ssmp_ed
                                      JOIN ssmp USING (cod_ssmp)
                                      JOIN status_ssmp USING (cod_status_ssmp)
                                      JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON(pe.cod_local = local.cod_local)

                                      WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)
                                  ) AS preventivas_abertas";

                $selectPmpEncerradas = "SELECT (
                                            SELECT count(*) FROM osmp
                                            JOIN status_osmp USING (cod_status_osmp)
                                            JOIN ssmp_ed USING (cod_ssmp)
                                            JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                            JOIN pmp USING (cod_pmp)

                                            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                            JOIN sub_sistema USING(cod_sub_sistema)
                                            JOIN local ON(pe.cod_local = local.cod_local)

                                            WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11
                                        ) AS preventivas_encerradas";

                $selectPmpCancelada = "SELECT (
                                          SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_ed USING (cod_ssmp)
                                          JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                          JOIN pmp USING (cod_pmp)

                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)

                                          WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)
                                      ) AS preventivas_canceladas";

                $arr['tituloFiltroGrupo'] = "GRUPO EDIFICACOES";
                break;
            case 24:
                $selectPmpTotal = "SELECT (
                                      SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_vp USING (cod_ssmp)
                                      JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)

                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp}
                                  ) + (
                                      SELECT count(*) FROM ssmp_vp
                                      JOIN ssmp USING (cod_ssmp)
                                      JOIN status_ssmp_vp USING (cod_status_ssmp_vp)
                                      JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)

                                      WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' AND cod_status IN(19) {$wherePmp}
                                  ) AS preventivas_abertas";

                $selectPmpEncerradas = "SELECT (
                                            SELECT count(*) FROM osmp
                                            JOIN status_osmp USING (cod_status_osmp)
                                            JOIN ssmp_vp USING (cod_ssmp)
                                            JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                            JOIN pmp USING (cod_pmp)

                                            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                            JOIN sub_sistema USING(cod_sub_sistema)
                                            JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)

                                            WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11
                                        ) AS preventivas_encerradas";

                $selectPmpCancelada = "SELECT (
                                          SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_vp USING (cod_ssmp)
                                          JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                          JOIN pmp USING (cod_pmp)

                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)

                                          WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)
                                      ) AS preventivas_canceladas";
                $arr['tituloFiltroGrupo'] = "GRUPO VIA PERMANENTE";
                break;
            case 25:
                $selectPmpTotal = "SELECT (
                                      SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_su USING (cod_ssmp)
                                      JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON(pe.cod_local = local.cod_local)
                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp}
                                  ) + (
                                      SELECT count(*) FROM ssmp_su
                                      JOIN ssmp USING (cod_ssmp)
                                      JOIN status_ssmp_su USING (cod_status_ssmp_su)
                                      JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON(pe.cod_local = local.cod_local)
                                      WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)
                                  ) AS preventivas_abertas";

                $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_su USING (cod_ssmp)
                                          JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                      JOIN pmp USING (cod_pmp)
                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)
                                         WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11 ) AS preventivas_encerradas";
                $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_su USING (cod_ssmp)
                                          JOIN pmp_subestacao pe USING (cod_pmp_subestacao)
                                      JOIN pmp USING (cod_pmp)
                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)
                                       WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)) AS preventivas_canceladas";
                $arr['tituloFiltroGrupo'] = "GRUPO SUBESTACAO";
                break;
            case 20:
                $selectPmpTotal = "SELECT (SELECT count(*)
                                FROM osmp
                                  JOIN status_osmp USING (cod_status_osmp)
                                  JOIN ssmp_ra USING (cod_ssmp)
                                  JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                      JOIN pmp USING (cod_pmp)
                                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                  JOIN sub_sistema USING (cod_sub_sistema)
                                  JOIN local ON (pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp})+
                                  (SELECT count(*)
                                FROM ssmp_ra
                                  JOIN ssmp USING (cod_ssmp)
                                  JOIN status_ssmp USING (cod_status_ssmp)
                                  JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                      JOIN pmp USING (cod_pmp)
                                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                  JOIN sub_sistema USING (cod_sub_sistema)
                                  JOIN local ON (pe.cod_local = local.cod_local)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)) AS preventivas_abertas";
                $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_ra USING (cod_ssmp)
                                          JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                      JOIN pmp USING (cod_pmp)
                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)
                                        WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11 ) AS preventivas_encerradas";
                $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_ra USING (cod_ssmp)
                                      JOIN pmp_rede_aerea pe USING (cod_pmp_rede_aerea)
                                      JOIN pmp USING (cod_pmp)
                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON (pe.cod_local = local.cod_local)
                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23) ) AS preventivas_canceladas";
                $arr['tituloFiltroGrupo'] = "GRUPO REDE AEREA";
                break;
            case 28:
                $selectPmpTotal = "SELECT (
                                      SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_bi USING (cod_ssmp)
                                      JOIN pmp_bilhetagem pe USING (cod_pmp_bilhetagem)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN estacao est ON(pe.cod_estacao = est.cod_estacao)

                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp}
                                  ) + (
                                      SELECT count(*) FROM ssmp_bi
                                      JOIN ssmp USING (cod_ssmp)
                                      JOIN status_ssmp USING (cod_status_ssmp)
                                      JOIN pmp_bilhetagem pe USING (cod_pmp_bilhetagem)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN estacao est ON(pe.cod_estacao = est.cod_estacao)

                                      WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' AND cod_status IN(19) {$wherePmp}
                                  ) AS preventivas_abertas";

                $selectPmpEncerradas = "SELECT (
                                            SELECT count(*) FROM osmp
                                            JOIN status_osmp USING (cod_status_osmp)
                                            JOIN ssmp_bi USING (cod_ssmp)
                                            JOIN pmp_bilhetagem pe USING (cod_pmp_bilhetagem)
                                            JOIN pmp USING (cod_pmp)

                                            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                            JOIN sub_sistema USING(cod_sub_sistema)
                                            JOIN estacao ON(pe.cod_estacao = estacao.cod_estacao)

                                            WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11
                                        ) AS preventivas_encerradas";

                $selectPmpCancelada = "SELECT (
                                          SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_vp USING (cod_ssmp)
                                          JOIN pmp_bilhetagem pe USING (cod_pmp_bilhetagem)
                                          JOIN pmp USING (cod_pmp)

                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN estacao ON(pe.cod_estacao = estacao.cod_estacao)

                                          WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)
                                      ) AS preventivas_canceladas";
                $arr['tituloFiltroGrupo'] = "GRUPO BILHETAGEM";
                break;
            case 27:
                $selectPmpTotal = "SELECT (
                                      SELECT count(*) FROM osmp
                                      JOIN status_osmp USING (cod_status_osmp)
                                      JOIN ssmp_te USING (cod_ssmp)
                                      JOIN pmp_telecom pe USING (cod_pmp_telecom)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON(pe.cod_local = local.cod_local)

                                      WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp}
                                  ) + (
                                      SELECT count(*) FROM ssmp_te
                                      JOIN ssmp USING (cod_ssmp)
                                      JOIN status_ssmp USING (cod_status_ssmp)
                                      JOIN pmp_telecom pe USING (cod_pmp_telecom)
                                      JOIN pmp USING (cod_pmp)

                                      JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                      JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                      JOIN sub_sistema USING(cod_sub_sistema)
                                      JOIN local ON(pe.cod_local = local.cod_local)

                                      WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)
                                  ) AS preventivas_abertas";

                $selectPmpEncerradas = "SELECT (
                                            SELECT count(*) FROM osmp
                                            JOIN status_osmp USING (cod_status_osmp)
                                            JOIN ssmp_te USING (cod_ssmp)
                                            JOIN pmp_telecom pe USING (cod_pmp_telecom)
                                            JOIN pmp USING (cod_pmp)

                                            JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                            JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                            JOIN sub_sistema USING(cod_sub_sistema)
                                            JOIN local ON(pe.cod_local = local.cod_local)

                                            WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11
                                        ) AS preventivas_encerradas";

                $selectPmpCancelada = "SELECT (
                                          SELECT count(*) FROM osmp
                                          JOIN status_osmp USING (cod_status_osmp)
                                          JOIN ssmp_te USING (cod_ssmp)
                                          JOIN pmp_telecom pe USING (cod_pmp_telecom)
                                          JOIN pmp USING (cod_pmp)

                                          JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                          JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                          JOIN sub_sistema USING(cod_sub_sistema)
                                          JOIN local ON(pe.cod_local = local.cod_local)

                                          WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)
                                      ) AS preventivas_canceladas";

                $arr['tituloFiltroGrupo'] = "GRUPO TELECOM";
                break;
            default:
                $selectPmpTotal = "SELECT (SELECT count(*) FROM osmp
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_ed USING (cod_ssmp)
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp})+
                                  (SELECT count(*) FROM osmp
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}'  {$wherePmp} )+
                                 (SELECT count(*) FROM ssmp_vp
                                JOIN ssmp USING (cod_ssmp)
                                 JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19))+
                                  (SELECT count(*) FROM ssmp_ed
                                JOIN ssmp USING (cod_ssmp)
                                  JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE ssmp.data_programada >= '{$tempoInicial}' AND ssmp.data_programada < '{$tempoFinal}' {$wherePmp} AND cod_status IN(19)) AS preventivas_abertas";
                $selectPmpEncerradas = "SELECT (SELECT count(*) FROM osmp
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11)+
                                 (SELECT count(*) FROM osmp
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status = 11) AS preventivas_encerradas";
                $selectPmpCancelada = "SELECT (SELECT count(*) FROM osmp
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_ed USING (cod_ssmp)
                                JOIN pmp_edificacao pe USING (cod_pmp_edificacao)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN local ON(pe.cod_local = local.cod_local)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23) )+
                                 (SELECT count(*) FROM osmp
                                JOIN status_osmp USING (cod_status_osmp)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN pmp_via_permanente pe USING (cod_pmp_via_permanente)
                                      JOIN pmp USING (cod_pmp)
                                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING(cod_sub_sistema)
                                JOIN estacao ON(pe.cod_estacao_inicial = estacao.cod_estacao)
                                WHERE osmp.data_abertura >= '{$tempoInicial}' AND osmp.data_abertura < '{$tempoFinal}' {$wherePmp} AND cod_status IN (24,25,23)) AS preventivas_canceladas";
                $arr['tituloFiltroGrupo'] = "GRUPO GERAL";
                break;
        }

        //########################### Total Abertas
        //########################### Total Abertas
        //########################### Total Abertas

        $arr['totalAbertas'] = 0;

        $sql = $db->prepare($selectPmpTotal);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalAbertas'] = $resultado['preventivas_abertas'];

        //############################# Total Executadas
        //############################# Total Executadas
        //############################# Total Executadas

        $arr['totalEncerrada'] = 0;

        $sql = $db->prepare($selectPmpEncerradas);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalEncerrada'] = $resultado['preventivas_encerradas'];

        // ###########################    Canceladas
        // ###########################    Canceladas
        // ###########################    Canceladas

        $arr['totalCancelada'] = 0;

        $sql = $db->prepare($selectPmpCancelada);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['totalCancelada'] = $resultado['preventivas_canceladas'];

        $arr['totalRetroativa'] = 0;
        $arr['totalAtendimento'] = $arr['totalAbertas'] - ($arr['totalCancelada'] + $arr['totalEncerrada']);
        if ($arr['totalAtendimento'] < 0) {
            $arr['totalRetroativa'] = $arr['totalAtendimento'] * (-1);
            $arr['totalAtendimento'] = 0;
        }

        return ($arr);
    }

    public function getValueAnualOcorrencias($db)
    {
        $abertas = 0;
        $encerradas = 0;

        $arr = [];
        $where = [];

        $arr['tituloFiltro'][] = $_POST['ano'];

        $where[] = " vsaf.mes = {$_POST['mes']}";
        $where[] = " vsaf.ano = {$_POST['ano']}";
        $grupoCronograma="";

        if ($_POST['grupo']) {
            $grupo = $this->medoo->select("grupo", "nome_grupo", ["cod_grupo" => (int)$_POST['grupo']]);
            $grupo = $grupo[0];
            $arr['tituloFiltro'][] = $grupo;
            $where[] = " vsaf.cod_grupo = {$_POST['grupo']}";
            $grupoCronograma .= " AND cod_grupo = {$_POST['grupo']}";
        }

        if ($_POST['linha']) {
            $linha = $this->medoo->select("linha", "nome_linha", ["cod_linha" => (int)$_POST['linha']]);
            $linha = $linha[0];
            $arr['tituloFiltro'][] = $linha;
            $where[] = " vsaf.cod_linha = {$_POST['linha']}";
            $grupoCronograma .= " AND cod_linha = {$_POST['linha']}";
        }

        if ($_POST['sistema']) {
            $sistema = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => (int)$_POST['sistema']]);
            $sistema = $sistema[0];
            $arr['tituloFiltro'][] = $sistema;
            $where[] = " vsaf.cod_sistema = {$_POST['sistema']}";
            $grupoCronograma .= " AND cod_sistema = {$_POST['sistema']}";
        }

        if ($_POST['subSistema']) {
            $subSistema = $this->medoo->select("subsistema", "nome_subsistema", ["cod_subsistema" => (int)$_POST['subSistema']]);
            $subSistema = $subSistema[0];
            $arr['tituloFiltro'][] = $subSistema;
            $where[] = " vsaf.cod_subsistema = {$_POST['subSistema']}";
            $grupoCronograma .= " AND cod_subsistema = {$_POST['subSistema']}";
        }

        $joins = '';
        switch($_POST['grupo']){
            case '20': //Rede Aérea
                $joins = "JOIN pmp_rede_aerea USING(cod_pmp) JOIN local USING(cod_local)";
                break;

            case '21': //Edificações
                $joins = "JOIN pmp_edificacao USING(cod_pmp) JOIN local USING(cod_local)";
                break;

            case '24': //Via Permanente
                $joins = "JOIN pmp_via_permanente USING(cod_pmp) JOIN estacao ON(cod_estacao_inicial = cod_estacao)";
                break;

            case '25': //Subestação
                $joins = "JOIN pmp_subestacao USING(cod_pmp) JOIN local USING(cod_local)";
                break;

            case '27': //TElecom
                $joins = "JOIN pmp_telecom USING (cod_pmp) JOIN local USING(cod_local)";
                break;

            case '28': // Bilhetagem
                $joins = "JOIN pmp_bilhetagem USING(cod_pmp) JOIN estacao USING(cod_estacao)";
                break;

        }

        $where = " AND ". implode(' AND ', $where);

        $arr['tituloFiltro'] = strtoupper($this->retiraAcentos(implode(' | ', $arr['tituloFiltro'])));

        if ($_POST['anual'] != "false") {
            for ($i = 1; $i <= 12; $i++) {
                $_POST['mes'] = $i;//$key;
                $arrCorretiva = $this->getValueGrafCorretivo($db);
                $arrPreventiva = $this->getValueGrafPreventivo($db);

                $abertas += $arrCorretiva['totalAbertas'];
                $abertas += $arrPreventiva['totalAbertas'];

                $encerradas += $arrCorretiva['totalCancelada'];
                $encerradas += $arrCorretiva['totalEncerrada'];

                $encerradas += $arrPreventiva['totalCancelada'];
                $encerradas += $arrPreventiva['totalEncerrada'];

                $arr['abertas'][] = $abertas;
                $arr['encerradas'][] = $encerradas;

                $abertas = 0;
                $encerradas = 0;
            }
        } else {

            $sql ="
            SELECT
            COUNT(vsaf) as corretivas,
            (
                SELECT count(*)

                FROM cronograma_pmp

                JOIN cronograma
                USING(cod_cronograma)

                JOIN pmp
                USING(cod_pmp)

                {$joins}

                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
				JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
				JOIN sub_sistema USING(cod_sub_sistema)

                WHERE mes = {$_POST['mes']} AND ano = {$_POST['ano']} {$grupoCronograma}
            ) as preventivas
            FROM
               (
			    SELECT
		   		  date_part('month', data_abertura) AS mes,
		   		  date_part('year', data_abertura) AS ano, *
		 	    FROM v_saf
		        ) AS vsaf

            INNER JOIN
            (
                SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
                FROM saf sa
                INNER JOIN ssm s
                ON sa.cod_saf = s.cod_saf
                GROUP BY s.cod_saf
            ) AS menor_ssm
            ON vsaf.cod_saf = menor_ssm.cod_saf

            INNER JOIN ssm
            ON menor_ssm.menor = ssm.cod_ssm

            INNER JOIN
            (
                SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
                FROM ssm sm
                INNER JOIN osm_falha o
                ON sm.cod_ssm = o.cod_ssm
                GROUP BY o.cod_ssm
            ) AS menor_osm
            ON ssm.cod_ssm = menor_osm.cod_ssm

            INNER JOIN osm_falha osm
            ON menor_osm.menorosm = osm.cod_osm

            INNER JOIN osm_registro osmr
            ON osmr.cod_osm = osm.cod_osm

            WHERE cod_ag_causador NOT IN(16,17,22, 28) $where

            ";
            $arr['sql'] = $sql;
            $sql = $db->prepare($sql);
            $sql->execute();
            $arr['resultado'] = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

        return json_encode($arr);
    }

    public function getValueApp($mes)
    {
        $sqlEd = "SELECT COUNT(*)
            FROM cronograma c
              JOIN cronograma_pmp c_ed USING (cod_cronograma)
              JOIN pmp USING (cod_pmp)
              JOIN pmp_edificacao USING (cod_pmp)
              JOIN local l USING (cod_local)
              JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
              JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
              JOIN sub_sistema USING (cod_sub_sistema)
              JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
              JOIN status USING (cod_status)
              JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
              JOIN servico_pmp USING (cod_servico_pmp)";
        $sqlRa = "SELECT COUNT(*)
            FROM cronograma c
              JOIN cronograma_pmp c_ra USING (cod_cronograma)
              JOIN pmp USING (cod_pmp)
              JOIN pmp_rede_aerea USING (cod_pmp)
              JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
              JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
              JOIN sub_sistema USING (cod_sub_sistema)
              JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
              JOIN status USING (cod_status)
              JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
              JOIN servico_pmp USING (cod_servico_pmp)";
        $sqlVp = "SELECT COUNT(*)
            FROM cronograma c
              JOIN cronograma_pmp c_vp USING (cod_cronograma)
              JOIN pmp USING (cod_pmp)
              JOIN pmp_via_permanente USING (cod_pmp)
              JOIN estacao l ON l.cod_estacao = cod_estacao_inicial
              JOIN estacao f ON f.cod_estacao = cod_estacao_final
              JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
              JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
              JOIN sub_sistema USING (cod_sub_sistema)
              JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
              JOIN status USING (cod_status)
              JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
              JOIN servico_pmp USING (cod_servico_pmp)";
        $sqlSb = "SELECT COUNT(*)
            FROM cronograma c
              JOIN cronograma_pmp c_su USING (cod_cronograma)
              JOIN pmp USING (cod_pmp)
              JOIN pmp_subestacao USING (cod_pmp)
              JOIN local l USING (cod_local)
              JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
              JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
              JOIN sub_sistema USING (cod_sub_sistema)
              JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
              JOIN status USING (cod_status)
              JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
              JOIN servico_pmp USING (cod_servico_pmp)";
        $sqlBi ="SELECT
                  COUNT(*)
                FROM cronograma c
                JOIN cronograma_pmp c_ed USING (cod_cronograma)
                JOIN pmp USING (cod_pmp)
                JOIN pmp_bilhetagem USING (cod_pmp)
                JOIN estacao l USING (cod_estacao)
                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                JOIN sub_sistema USING (cod_sub_sistema)
                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                JOIN status USING (cod_status)
                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                JOIN servico_pmp USING (cod_servico_pmp)";
        $sqlTe ="SELECT
                COUNT(*)
                FROM cronograma c
                JOIN cronograma_pmp c_ed USING (cod_cronograma)
                JOIN pmp USING (cod_pmp)
                JOIN pmp_telecom USING (cod_pmp)
                JOIN local l USING (cod_local)
                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                JOIN sub_sistema USING (cod_sub_sistema)
                JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                JOIN status USING (cod_status)
                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                JOIN servico_pmp USING (cod_servico_pmp)";

        $ano = date('Y');
        $nomeMes = MainController::$monthComplete[$mes];

        $returnSqlEd = $this->medoo->query($sqlEd . "WHERE ano = " . $ano . " AND mes = " . $mes)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlEdEncerradas = $this->medoo->query($sqlEd . "WHERE ano = " . $ano . " AND mes = " . $mes . " AND cod_status = " . 18)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlRa = $this->medoo->query($sqlRa . "WHERE ano = " . $ano . " AND mes = " . $mes)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlRaEncerradas = $this->medoo->query($sqlRa . "WHERE ano = " . $ano . " AND mes = " . $mes . " AND cod_status = " . 18)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlVp = $this->medoo->query($sqlVp . "WHERE ano = " . $ano . " AND mes = " . $mes)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlVpEncerradas = $this->medoo->query($sqlVp . "WHERE ano = " . $ano . " AND mes = " . $mes . " AND cod_status = " . 18)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlSb = $this->medoo->query($sqlSb . "WHERE ano = " . $ano . " AND mes = " . $mes)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlSbEncerradas = $this->medoo->query($sqlSb . "WHERE ano = " . $ano . " AND mes = " . $mes . " AND cod_status = " . 18)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlBi = $this->medoo->query($sqlBi . "WHERE ano = " . $ano . " AND mes = " . $mes)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlBiEncerradas = $this->medoo->query($sqlBi . "WHERE ano = " . $ano . " AND mes = " . $mes . " AND cod_status = " . 18)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlTe = $this->medoo->query($sqlTe . "WHERE ano = " . $ano . " AND mes = " . $mes)->fetchAll(PDO::FETCH_ASSOC);
        $returnSqlTeEncerradas = $this->medoo->query($sqlTe . "WHERE ano = " . $ano . " AND mes = " . $mes . " AND cod_status = " . 18)->fetchAll(PDO::FETCH_ASSOC);

        $arr['totalEd'] = $returnSqlEd[0]['count'];
        $arr['totalRa'] = $returnSqlRa[0]['count'];
        $arr['totalVp'] = $returnSqlVp[0]['count'];
        $arr['totalSb'] = $returnSqlSb[0]['count'];
        $arr['totalBi'] = $returnSqlBi[0]['count'];
        $arr['totalTe'] = $returnSqlTe[0]['count'];
        $arr['encerradasEd'] = $returnSqlEdEncerradas[0]['count'];
        $arr['encerradasRa'] = $returnSqlRaEncerradas[0]['count'];
        $arr['encerradasVp'] = $returnSqlVpEncerradas[0]['count'];
        $arr['encerradasSb'] = $returnSqlSbEncerradas[0]['count'];
        $arr['encerradasBi'] = $returnSqlBiEncerradas[0]['count'];
        $arr['encerradasTe'] = $returnSqlTeEncerradas[0]['count'];

        $arr['nomeMes'] = $this->retiraAcentos($nomeMes);

        return json_encode($arr);
    }

    public function getValueApc($dados)
    {
        return json_encode($dados);
    }

    public function localPoste($dados, $db)
    {
        $sql = $db->prepare("SELECT * FROM poste WHERE cod_local={$dados}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr['opcao'] = "";

        foreach ($resultado as $dados => $value) {
            $arr['opcao'] .= "<option value='{$value['cod_poste']}'>{$value['nome_poste']}</option>";
        }

        return json_encode($arr);
    }

    public function homemHora($dados, $db)
    {
        $sql = $db->prepare("SELECT * FROM v_funcionario f WHERE cod_funcionario={$dados}");
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $sql = $db->prepare("SELECT * FROM funcionario f JOIN funcionario_empresa USING(cod_funcionario_empresa) WHERE f.cod_funcionario={$dados}");
        $sql->execute();
        $dadosEmpresa = $sql->fetchAll(PDO::FETCH_ASSOC);
        $dadosEmpresa = $dadosEmpresa[0];

        $arr = [
            "matricula" => $dadosEmpresa['matricula'],
            "nome" => $resultado['nome_funcionario'],
            "escolaridade" => $resultado['escolaridade'],
            "cursoTecnico" => $resultado['ef_nome_curso_tecnico'],
            "funcao" => $resultado['cargo'],
            'cpf' => $resultado['cpf'],
            'centroResultado' => $resultado['centro_resultado'],
            'unidade' => $resultado['nome_unidade']
        ];


        return json_encode($arr);
    }

    public function getComposicaoPorTipo($dados, $db)
    {
        $arr = [];
        $sql = "SELECT * FROM composicao WHERE cod_grupo = {$dados}";
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $dados => $value) {
            $arr += [$value['cod_composicao'] => $value['numero_composicao']];
        }

        return json_encode($arr);
    }

    public function getAvariaGrupo($dados, $db)
    {
        $arr = [];

        $sql = "SELECT * FROM avaria WHERE cod_grupo = {$dados} OR cod_grupo IS NULL ORDER BY nome_avaria";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $dados => $value) {
            $arr += [$value['nome_avaria'] => $value['cod_avaria']];
        }

        return json_encode($arr);
    }

    public function getVeiculo($dados, $db)
    {
        $arr = [];

        if ($dados[0] == "*") {
            $sql = "SELECT * FROM veiculo ORDER BY nome_veiculo ASC ";
        } else {
            if($dados[0] != "" && $dados[1] != ""){
                if($dados[1] == 6 || $dados[1] == 1)
                    $sql = "SELECT * FROM veiculo WHERE cod_grupo = {$dados[0]} AND cod_linha IN (6,1) ORDER BY nome_veiculo ASC ";
                else
                    $sql = "SELECT * FROM veiculo WHERE cod_grupo = {$dados[0]} AND cod_linha = {$dados[1]} ORDER BY nome_veiculo ASC ";
            }
            else{
                $sql = "SELECT * FROM veiculo WHERE cod_grupo = {$dados[0]} ORDER BY nome_veiculo ASC ";
            }
        }

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $value) {
            $arr += [$value['nome_veiculo'] => $value['cod_veiculo']];
        }

        return json_encode($arr);
    }

    public function getVeiculoFrota($frota, $db)
    {
        $arr = [];

        $sql = "SELECT * FROM veiculo WHERE frota = '{$frota}' ORDER BY nome_veiculo ASC";


        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $value) {
            $arr += [$value['nome_veiculo'] => $value['cod_veiculo']];
        }

        return json_encode($arr);
    }

    public function getVeiculoComp($dados, $db)
    {
        $arr = [];

        $sql = "SELECT * FROM veiculo WHERE cod_composicao = {$dados}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $value) {
            array_push($arr, $value['cod_veiculo']);
        }

        return json_encode($arr);
    }

    public function getCarro($dados, $db)
    {
        $arr = [];

        $sql = "SELECT * FROM carro WHERE cod_veiculo = {$dados}";
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $value) {
            $txt = $value['sigla_carro'] . ' - ' . $value['nome_carro'];
            $arr += [$value['cod_carro'] => $txt];
        }

        return json_encode($arr);
    }

    public function getCarroMr($dados, $db)
    {
        $arr = [];

        if ($dados[0] == 'veiculo') {
            if ($dados[1] == "*") {
                $where = "";
            } else {
                $where = "AND cod_veiculo = {$dados[1]}";
            }
        } else if ($dados[0] == 'multi') {
            $where = "AND cod_grupo = {$dados[1][0]}";

            $sql = "SELECT * FROM carro WHERE cod_veiculo = {$dados[1][1]}";
            $sql = $db->prepare($sql);
            $sql->execute();
            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            $aux = [];
            foreach ($resultado as $key => $value) {
                $txt = $value['sigla_carro'] . ' - ' . $value['nome_carro'];
                $aux += [$value['cod_carro'] => $txt];
            }

            $arr['veiculo'] = $aux;
        } else {
            if ($dados[1] == "*") {
                $where = "";
            } else {
                $where = "AND cod_grupo = {$dados[1]}";
            }
        }

        $sqlMotor = "SELECT * FROM carro WHERE cod_tipo_carro = 1 {$where}";
        $sqlMotor = $db->prepare($sqlMotor);
        $sqlMotor->execute();
        $resultadoMotor = $sqlMotor->fetchAll(PDO::FETCH_ASSOC);
        $auxMotor = [];
        foreach ($resultadoMotor as $key => $value) {
            $txt = $value['sigla_carro'] . ' - ' . $value['nome_carro'];
            $auxMotor += [$value['cod_carro'] => [$txt, $value['odometro'], $value['sigla_carro']]];
        }

        $arr['motor'] = $auxMotor;

        $sqlSem = "SELECT * FROM carro WHERE cod_tipo_carro = 2 {$where}";
        $sqlSem = $db->prepare($sqlSem);
        $sqlSem->execute();
        $resultadoSem = $sqlSem->fetchAll(PDO::FETCH_ASSOC);
        $auxSem = [];
        foreach ($resultadoSem as $key => $value) {
            $txt = $value['sigla_carro'] . ' - ' . $value['nome_carro'];
            $auxSem += [$value['cod_carro'] => [$txt, $value['odometro'], $value['sigla_carro']]];
        }
        $arr['sem'] = $auxSem;


        return json_encode($arr);
    }

    public function getVerificarCarro($carro, $db)
    {
        $arr = [];

        $sql = "SELECT * FROM carro LEFT JOIN veiculo USING(cod_veiculo) WHERE cod_carro = {$carro}";
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['cod'] = $resultado['cod_veiculo'];
        $arr['nome'] = $resultado['nome_veiculo'];

        return json_encode($arr);
    }

    public function getListaCronograma_grafico($dados, $db)
    {
        $sql = "SELECT cred.cod_cronograma_pmp AS cod_cronograma_pmp, cr.quinzena AS quinzena_exec, nome_servico_pmp, nome_status, nome_sistema, nome_subsistema
                FROM cronograma_pmp cred
                JOIN cronograma cr USING(cod_cronograma)
                JOIN pmp USING(cod_pmp)
                JOIN servico_pmp_periodicidade USING(cod_servico_pmp_periodicidade)
                JOIN servico_pmp_sub_sistema USING(cod_servico_pmp_sub_sistema)
                JOIN sub_sistema USING(cod_sub_sistema)
                JOIN sistema USING(cod_sistema)
	            JOIN subsistema USING(cod_subsistema)
                JOIN servico_pmp USING(cod_servico_pmp)
                JOIN status_cronograma_pmp USING(cod_status_cronograma_pmp)
                JOIN status USING(cod_status)";
        switch ($dados[0]) {
            case '0':
                $where = " WHERE mes={$dados[2]}";
                break;
            case '1':
                $where = " WHERE mes={$dados[2]} AND cod_status IN (19, 32, 34, 21, 22)";
                break;
            case '2':
                $where = " WHERE mes={$dados[2]} AND cod_status IN (18)";
                break;
            case '3':
                $where = " WHERE mes={$dados[2]} AND cod_status IN (17)";
                break;
        }
        $sql = $db->prepare($sql . $where);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return json_encode($resultado);
    }

    public function getLinhaPrefixo($dados, $db)
    {
        $arr = [];

        $sql = "SELECT * FROM prefixo WHERE cod_linha = {$dados} ORDER BY nome_prefixo";
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $value) {
            $arr += [$value['cod_prefixo'] => $value['nome_prefixo']];
        }

        return json_encode($arr);
    }

    public function getFalhaSistema($db)
    {
        $arr = [];

        $grupo = $this->medoo->select("grupo", "nome_grupo", ["cod_grupo" => (int)$_POST['grupo']]);
        $grupo = $grupo[0];
        $arr['subtitulo'][] = utf8_encode($grupo);

        $where = [];

        if ($_POST['dataPartir']) {
            $arr['subtitulo'][] = $_POST['dataPartir'];
            $where[] = "data_abertura >= '{$_POST['dataPartir']} 00:00:00'";
        }

        if ($_POST['dataRetroceder']) {
            $arr['subtitulo'][] = "At� " . $_POST['dataRetroceder'];
            $where[] = "data_abertura <= '{$_POST['dataRetroceder']} 23:59:59' ";
        }

        if ($_POST['nivel']) {
            $arr['subtitulo'][] = "Nivel " . $_POST['nivel'];
            $where[] = "nivel = '{$_POST['nivel']}' ";
        }

        if ($where) {
            $where = " AND " . implode(' AND ', $where);
            $arr['subtitulo'] = implode(" - ", $arr['subtitulo']);
        } else {
            $where = " ";
        }

        $sql = "SELECT nome_sistema, count(*) FROM v_saf WHERE cod_grupo = {$_POST['grupo']} {$where} GROUP BY nome_sistema ORDER BY COUNT  DESC ";
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $dados => $value) {
            $arr['valorX'][] = [$value['nome_sistema'], (int)$value['count']];
        }

        return json_encode($arr);
    }

    public function getFalhaTrem($db)
    {
        $arr = [];
        $where = [];

        if ($_POST['linha']) {
            $sistema = $this->medoo->select("linha", "nome_linha", ["cod_linha" => (int)$_POST['linha']]);
            $sistema = $sistema[0];
            $arr['subtitulo'][] = $sistema;
            $where[] = "cod_linha = {$_POST['linha']}";
        }

        if ($_POST['grupo']) {
            $grupo = $this->medoo->select("grupo", "nome_grupo", ["cod_grupo" => (int)$_POST['grupo']]);
            $grupo = $grupo[0];
            $arr['subtitulo'][] = utf8_encode($grupo);
            $where[] = "cod_grupo = {$_POST['grupo']}";
        }

        if ($_POST['sistema']) {
            $sistema = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => (int)$_POST['sistema']]);
            $sistema = $sistema[0];
            $arr['subtitulo'][] = $sistema;
            $where[] = "cod_sistema = {$_POST['sistema']}";
        }

        if ($_POST['nivel']) {
            $arr['subtitulo'][] = "Nivel " . $_POST['nivel'];
            $where[] = "nivel = '{$_POST['nivel']}'' ";
        }

        if ($_POST['dataPartir']) {
            $arr['subtitulo'][] = $_POST['dataPartir'];
            $where[] = "data_abertura >= '{$_POST['dataPartir']} 00:00:00'";
        }

        if ($_POST['dataRetroceder']) {
            $arr['subtitulo'][] = "At� " . $_POST['dataRetroceder'];
            $where[] = "data_abertura <= '{$_POST['dataRetroceder']} 23:59:59' ";
        }

        if ($where) {
            $where = implode(' AND ', $where);
            $arr['subtitulo'] = implode(" - ", $arr['subtitulo']);
        } else {
            $where = " ";
        }

        $sql = "SELECT nome_veiculo, count(*) FROM v_saf WHERE {$where} GROUP BY nome_veiculo ORDER BY COUNT  DESC ";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $dados => $value) {
            $arr['valorX'][] = [$value['nome_veiculo'], (int)$value['count']];
        }

        $arr['sql'] = $sql;

        return json_encode($arr);
    }

    public function getFalhaMes($db)
    {
        $arr = [];
        $where = [];

        if ($_POST['grupo']) {
            $where[] = "v_saf.cod_grupo = {$_POST['grupo']}";
            $grupo = $this->medoo->select("grupo", "nome_grupo", ["cod_grupo" => (int)$_POST['grupo']]);
            $grupo = $grupo[0];
            $arr['subtitulo'][] = utf8_encode($grupo);
        }

        if ($_POST['sistema']) {
            $where[] = "v_saf.cod_sistema = {$_POST['sistema']}";
            $sistema = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => (int)$_POST['sistema']]);
            $sistema = $sistema[0];
            $arr['subtitulo'][] = $sistema;
        }

        if ($_POST['subsistema']) {
            $where[] = "v_saf.cod_subsistema = {$_POST['subsistema']}";
            $subSistema = $this->medoo->select("subsistema", "nome_subsistema", ["cod_subsistema" => $_POST['subsistema']]);
            $subSistema = $subSistema[0];
            $arr['subtitulo'][] = $subSistema;
        }

        if ($_POST['linha']) {
            $where[] = "v_saf.cod_linha = {$_POST['linha']}";
            $linha = $this->medoo->select("linha", "nome_linha", ["cod_linha" => (int)$_POST['linha']]);
            $linha = $linha[0];
            $arr['subtitulo'][] = $linha;
        }

        if ($_POST['trecho']) {
            $where[] = "v_saf.cod_trecho = {$_POST['trecho']}";
            $trecho = $this->medoo->select("trecho", "nome_trecho", ["cod_trecho" => (int)$_POST['trecho']]);
            $trecho = $trecho[0];
            $arr['subtitulo'][] = $trecho;
        }

        if ($_POST['nivel']) {
            $where[] = "v_saf.nivel = '{$_POST['nivel']}' ";
            $arr['subtitulo'][] = "Nivel " . $_POST['nivel'];
        }

        if ($where) {
            $where = " WHERE  " . implode(' AND ', $where);
            $arr['subtitulo'] = implode(' - ', $arr['subtitulo']);
        } else {
            $where = " ";
        }

        $sql = "SELECT mes, count(*) FROM (
                      SELECT date_part('month', data_abertura) as mes, date_part('year', data_abertura) as ano
                      FROM v_saf {$where})
                  as ta
                  WHERE ta.ano = {$_POST['ano']} GROUP BY ta.mes ORDER BY mes";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($this->month as $key=>$value){
            $arr['qtdFalha'][$value] = 0;
        }

        foreach($resultado as $dados => $value){
            $arr['qtdFalha'][$this->month[$value['mes']]] = (int)$value['count'];
        }

        return json_encode( $arr);


    }

    public function getTML($db)
    {
        $where;

        if ($_POST['linha']) {
            $where[] = "vsaf.cod_linha = {$_POST['linha']}";
        }

        if ($_POST['trecho']) {
            $where[] = "vsaf.cod_trecho = {$_POST['trecho']}";
        }

        if ($_POST['grupo']) {
            $where[] = "vsaf.cod_grupo = {$_POST['grupo']}";
        }

        if ($_POST['sistema']) {
            $where[] = "vsaf.cod_sistema = {$_POST['sistema']}";
        }

        if ($_POST['subsistema']) {
            $where[] = "vsaf.cod_subsistema = {$_POST['subsistema']}";
        }

        if ($_POST['veiculo']) {
            $where[] = "vsaf.cod_veiculo = {$_POST['veiculo']}";
        }

        if ($_POST['local']) {
            $where[] = "vsaf.cod_local_grupo = {$_POST['local']}";
        }

        if ($_POST['subLocal']) {
            $where[] = "vsaf.cod_sublocal_grupo = {$_POST['subLocal']}";
        }

        if ($_POST['dataPartir']) {
            $where[] = "vsaf.data_abertura >= '{$_POST['dataPartir']} 00:00:00'";
        }

        if ($_POST['dataRetroceder']) {
            $where[] = "vsaf.data_abertura <= '{$_POST['dataRetroceder']} 23:59:59' ";
        }

        if ($_POST['nivel']) {
            $where[] = "vsaf.nivel = '{$_POST['nivel']}' ";
        }

        if ($where) {
            $where = " AND " . implode(' AND ', $where);
        }else {
            $where = " ";
        }

        $sql = "SELECT
            COUNT(DISTINCT vsaf.cod_saf) as Total_Falhas,
            (
                SELECT SUM(ot.data_exec_termino - vsaf.data_abertura)
                FROM
                    v_saf as vsaf

                    INNER JOIN ssm
                    ON ssm.cod_saf = vsaf.cod_saf

                    INNER JOIN
                    (
                        SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
                        FROM ssm sm
                        INNER JOIN osm_falha o
                        ON sm.cod_ssm = o.cod_ssm
                        INNER JOIN osm_encerramento oe
                        ON o.cod_osm = oe.cod_osm
                        WHERE liberacao = 's'
                        GROUP BY o.cod_ssm
                    ) AS menor_osm
                    ON ssm.cod_ssm = menor_osm.cod_ssm

                    INNER JOIN osm_falha osm
                    ON menor_osm.menorosm = osm.cod_osm

                    INNER JOIN osm_registro osmr
                    ON osmr.cod_osm = osm.cod_osm

                    INNER JOIN osm_tempo ot
                    ON ot.cod_osm = osm.cod_osm

                    WHERE cod_ag_causador NOT IN(16,17,22, 28) {$where}
            ) /COUNT(DISTINCT vsaf.cod_saf) as TML


        FROM

            v_saf as vsaf

        INNER JOIN ssm
        ON ssm.cod_saf = vsaf.cod_saf

        INNER JOIN
        (
            SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
            FROM ssm sm
            INNER JOIN osm_falha o
            ON sm.cod_ssm = o.cod_ssm
            GROUP BY o.cod_ssm
        ) AS menor_osm
        ON ssm.cod_ssm = menor_osm.cod_ssm

        INNER JOIN osm_falha osm
        ON menor_osm.menorosm = osm.cod_osm

        INNER JOIN osm_registro osmr
        ON osmr.cod_osm = osm.cod_osm

        WHERE cod_ag_causador NOT IN(16,17,22, 28) {$where}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return json_encode($resultado);
    }

    public function getMTTR($db)
    {
        $where = [];

        if ($_POST['linha']) {
            $where[] = "vsaf.cod_linha = {$_POST['linha']}";
        }

        if ($_POST['trecho']) {
            $where[] = "vsaf.cod_trecho = {$_POST['trecho']}";
        }

        if ($_POST['grupo']) {
            $where[] = "vsaf.cod_grupo = {$_POST['grupo']}";
        }

        if ($_POST['sistema']) {
            $where[] = "vsaf.cod_sistema = {$_POST['sistema']}";
        }

        if ($_POST['subsistema']) {
            $where[] = "vsaf.cod_subsistema = {$_POST['subsistema']}";
        }

        if ($_POST['dataPartir']) {
            $where[] = "vsaf.data_abertura >= '{$_POST['dataPartir']} 00:00:00'";
        }

        if ($_POST['dataRetroceder']) {
            $where[] = "vsaf.data_abertura <= '{$_POST['dataRetroceder']} 23:59:59' ";
        }

        if ($_POST['nivel']) {
            $where[] = "vsaf.nivel = '{$_POST['nivel']}' ";
        }

        if ($where) {
            $where = " AND " . implode(' AND ', $where);
        } else {
            $where = " ";
        }

        $sql = "SELECT(SUM(ost.data_exec_termino - ost.data_exec_inicio)/count(DISTINCT (vsaf.cod_saf)) ) as mttr, COUNT(vsaf.cod_saf) as total_falha
        FROM v_saf as vsaf

        INNER JOIN
        (
            SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
            FROM saf sa
            INNER JOIN ssm s
            ON sa.cod_saf = s.cod_saf
            GROUP BY s.cod_saf
        ) AS menor_ssm
        ON vsaf.cod_saf = menor_ssm.cod_saf

        INNER JOIN ssm
        ON menor_ssm.menor = ssm.cod_ssm

        INNER JOIN
        (
            SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
            FROM ssm sm
            INNER JOIN osm_falha o
            ON sm.cod_ssm = o.cod_ssm
            GROUP BY o.cod_ssm
        ) AS menor_osm
        ON ssm.cod_ssm = menor_osm.cod_ssm

        INNER JOIN osm_falha osm
        ON menor_osm.menorosm = osm.cod_osm

        INNER JOIN osm_falha osm_total
        ON ssm.cod_ssm = osm_total.cod_ssm

        INNER JOIN osm_tempo ost
        ON osm_total.cod_osm = ost.cod_osm

        INNER JOIN osm_registro osmr
        ON osmr.cod_osm = osm.cod_osm

        WHERE cod_ag_causador NOT IN(16,17,22, 28) {$where}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return json_encode($resultado);
    }

    public function getMTBF($db)
    {
        $where = array();

        if ($_POST['aPartir']) {
            $dataPartir = "{$_POST['aPartir']} 00:00:00";
            $where[] = "s.data_abertura >= '{$dataPartir}'";
        }

        if ($_POST['ate']) {
            $dataAte = "{$_POST['ate']} 23:59:59";
            $where[] = "s.data_abertura <= '{$dataAte}'";
        }

        if (!empty($_POST['linha'])) {
            $where[] = "s.cod_linha = " . $_POST['linha'];
        }

        if (!empty($_POST['trecho'])) {
            $where[] = "s.cod_trecho = " . $_POST['trecho'];
        }

        if (!empty($_POST['pn'])) {
            $where[] = "s.cod_ponto_notavel = " . $_POST['pn'];
        }

        if (!empty($_POST['grupo'])) {
            $where[] = "s.cod_grupo = " . $_POST['grupo'];
            if($_POST['grupo'] == 22 || $_POST['grupo'] == 23){
                $where[] = "osr.cod_ag_causador NOT IN (16, 21, 24, 27, 28, 9, 7, 12, 25, 1, 26)"; // Não
            }else{
                $where[] = "osr.cod_ag_causador NOT IN (16,17,22, 28)";
            }
        }

        if (!empty($_POST['sistema'])) {
            $where[] = "s.cod_sistema = " . $_POST['sistema'];
        }

        if (!empty($_POST['subsistema'])) {
            $where[] = "s.cod_subsistema = " . $_POST['subsistema'];
        }

        if (!empty($_POST['avaria'])) {
            $where[] = "s.cod_avaria = " . $_POST['avaria'];
        }

        if ($where) {
            $where = " AND " . implode(' AND ', $where);
        } else {
            $where = " ";
        }


        $sql = "SELECT
                ((timestamp '{$dataAte}' - timestamp '{$dataPartir}' ) - SUM(tml) )/count(*) as periodo_limpo,
              	count(*),
              	(EXTRACT(EPOCH FROM (timestamp '{$dataAte}' - timestamp '{$dataPartir}' )) - EXTRACT(EPOCH FROM sum(tml)))/count(*) as mtbf,
              	(timestamp '{$dataAte}' - timestamp '{$dataPartir}' ) as periodo,
              	EXTRACT(EPOCH FROM sum(tml)) AS tml_seconds
                FROM
                	(
                		SELECT
                    			DISTINCT ON(cod_saf) cod_saf, cod_ssm, cod_osm,
                    			ost.data_exec_termino, ose.liberacao,
                    			s.data_abertura AS data_saf,
                    			(
                    				ost.data_exec_termino - s.data_abertura
                    			) as tml
                    		FROM ssm
                    		JOIN v_saf s USING(cod_saf)
                    		JOIN osm_falha os USING(cod_ssm)
                    		JOIN osm_encerramento ose USING(cod_osm)
                    		JOIN osm_registro osr USING(cod_osm)
                    		JOIN osm_tempo ost USING(cod_osm)
                    		WHERE cod_saf IN
                    		(
                    			SELECT cod_saf
                    			FROM ssm
                    			GROUP BY cod_saf
                    		) AND ose.liberacao = 's' AND s.data_abertura BETWEEN '{$dataPartir}' AND '{$dataAte}'
                    		{$where}
                    		GROUP BY cod_ssm, cod_osm, os.data_abertura, ost.data_exec_termino, ose.liberacao, s.data_abertura
                    		ORDER BY cod_saf, os.data_abertura ASC
                 	) mtbf
                ";

                //return $sql;
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return json_encode($resultado);
    }

    public function getNaoConstatada($db)
    {
        $arr = [];
        $where = [];

        if ($_POST['linha']) {
            $linha = $this->medoo->select("linha", "nome_linha", ["cod_linha" => (int)$_POST['linha']]);
            $linha = $linha[0];
            $where[] = "vsaf.cod_linha = {$_POST['linha']}";
        }

        if ($_POST['trecho']) {
            $trecho = $this->medoo->select("trecho", "nome_trecho", ["cod_trecho" => (int)$_POST['trecho']]);
            $trecho = $trecho[0];
            $where[] = "vsaf.cod_trecho = {$_POST['trecho']}";
        }


        if ($_POST['grupo']) {
            $grupo = $this->medoo->select("grupo", "nome_grupo", ["cod_grupo" => (int)$_POST['grupo']]);
            $grupo = $grupo[0];
            $where[] = "vsaf.cod_grupo = {$_POST['grupo']}";
        }

        if ($_POST['sistema']) {
            $sistema = $this->medoo->select("sistema", "nome_sistema", ["cod_sistema" => (int)$_POST['sistema']]);
            $sistema = $sistema[0];
            $where[] = "vsaf.cod_sistema = {$_POST['sistema']}";
        }

        if ($_POST['subsistema']) {
            $subsistema = $this->medoo->select("subsistema", "nome_subsistema", ["cod_subsistema" => (int)$_POST['subsistema']]);
            $subsistema = $subsistema[0];
            $where[] = "vsaf.cod_subsistema = {$_POST['subsistema']}";
        }

        if ($_POST['dataPartir']) {
            $where[] = "vsaf.data_abertura >= '{$_POST['dataPartir']} 00:00:00'";
        }

        if ($_POST['dataRetroceder']) {
            $where[] = "vsaf.data_abertura <= '{$_POST['dataRetroceder']} 23:59:59' ";
        }

        if ($_POST['nivel']) {
            $where[] = "vsaf.nivel = '{$_POST['nivel']}' ";
        }

        if ($where) {
            $where = " AND " . implode(' AND ', $where);
        } else {
            $where = " ";
        }

        $sql = "SELECT vsaf.cod_saf, nome_grupo, nome_sistema, nome_avaria, vsaf.data_abertura,usuario, nome
FROM v_saf as vsaf

INNER JOIN
(
	SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
	FROM saf sa
	INNER JOIN ssm s
	ON sa.cod_saf = s.cod_saf
	GROUP BY s.cod_saf
) AS menor_ssm
ON vsaf.cod_saf = menor_ssm.cod_saf

INNER JOIN ssm
ON menor_ssm.menor = ssm.cod_ssm

INNER JOIN
(
	SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
	FROM ssm sm
	INNER JOIN osm_falha o
	ON sm.cod_ssm = o.cod_ssm
	GROUP BY o.cod_ssm
) AS menor_osm
ON ssm.cod_ssm = menor_osm.cod_ssm

INNER JOIN osm_falha osm
ON menor_osm.menorosm = osm.cod_osm

INNER JOIN osm_registro osmr
ON osmr.cod_osm = osm.cod_osm

WHERE cod_ag_causador IN(16,17,22, 28) {$where}
ORDER BY vsaf.cod_saf";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $dados => $value) {
            $arr['tabela'][] = [
                "0" => $value['cod_saf'],
                "1" => $value['data_abertura'],
                "2" => $value['nome_grupo'],
                "3" => $value['nome_sistema'],
                "4" => $value['nome_avaria'],
                "5" => $value['usuario'],
                "6" => $value['nome']
            ];
        }

        return json_encode($arr);
    }

    public function getCategoriasMaterial($codCategoria, $db)
    {
        $arr = [];

        $sql = "SELECT nome_material, cod_material FROM material WHERE cod_categoria = {$codCategoria}";
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultado as $key => $value) {
            $arr += [$value['cod_material'] => $value['nome_material']];
        }

        return json_encode($arr);
    }

    public function getTodosDadosMaterial($codMaterial, $db)
    {
        $arr = [];

        $sql = "SELECT m.quant_min, m.descricao, m.cod_material,m.nome_material,c.cod_categoria,c.nome_categoria,
                u.cod_uni_medida,u.nome_uni_medida,u.sigla_uni_medida,mm.cod_marca,mm.nome_marca

                FROM material m
                LEFT JOIN categoria_material c ON m.cod_categoria = c.cod_categoria
                FULL JOIN unidade_medida u ON m.cod_uni_medida = u.cod_uni_medida
                FULL JOIN marca_material mm ON m.cod_marca = mm.cod_marca
                WHERE cod_material = {$codMaterial}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['cod_material']        = $resultado['cod_material'];
        $arr['nome_material']       = $resultado['nome_material'];
        $arr['descricao']           = $resultado['descricao'];
        $arr['quant_min']           = $resultado['quant_min'];
        $arr['cod_categoria']       = $resultado['cod_categoria'];
        $arr['nome_categoria']      = $resultado['nome_categoria'];
        $arr['cod_uni_medida']      = $resultado['cod_uni_medida'];
        $arr['nome_uni_medida']     = $resultado['nome_uni_medida'];
        $arr['sigla_uni_medida']    = $resultado['sigla_uni_medida'];
        $arr['cod_marca']           = $resultado['cod_marca'];
        $arr['nome_marca']          = $resultado['nome_marca'];

        return json_encode($arr);
    }

    public function getTodosDadosFornecedor($cnpj, $db)
    {
        $arr = [];



        $sql = "SELECT  f.cod_fornecedor, nome_fantasia, razao_social, cnpjcpf, avaliacao,
                        f.descricao, optante_simples, insc_estadual, insc_municipal,

                        nome_contato, fone_fornecedor, email_fornecedor, site,

                        agencia, conta_corrente, banco, num_transferencia, municipio, bairro,

                        logradouro, cep, numero_endereco, complemento, uf

                FROM fornecedor f
                FULL JOIN contato_fornecedor cf ON f.cod_Fornecedor = cf.cod_Fornecedor
                FULL JOIN dados_bancarios_fornecedor dbf ON f.cod_Fornecedor = dbf.cod_Fornecedor
                FULL JOIN endereco_fornecedor ef ON f.cod_Fornecedor = ef.cod_Fornecedor
                WHERE cnpjcpf = '{$cnpj}'";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];


        // Dados obtidos da tabela 'fornecedor'
        $arr['cod_fornecedor']      = $resultado['cod_fornecedor'];
        $arr['nome_fantasia']       = $resultado['nome_fantasia'];
        $arr['razao_social']        = $resultado['razao_social'];
        $arr['cnpjcpf']             = $resultado['cnpjcpf'];
        $arr['avaliacao']           = $resultado['avaliacao'];
        $arr['descricao']           = $resultado['descricao'];
        $arr['optante_simples']     = $resultado['optante_simples'];
        $arr['insc_estadual']       = $resultado['insc_estadual'];
        $arr['insc_municipal']      = $resultado['insc_municipal'];


        // Dados obtidos da tabela 'contato_fornecedor'
        $arr['nome_contato']        = $resultado['nome_contato'];
        $arr['fone_fornecedor']     = $resultado['fone_fornecedor'];
        $arr['email_fornecedor']    = $resultado['email_fornecedor'];
        $arr['site']                = $resultado['site'];

        // Dados obtidos da tabela 'dados_bancarios_fornecedor'
        $arr['agencia']             = $resultado['agencia'];
        $arr['conta_corrente']      = $resultado['conta_corrente'];
        $arr['banco']               = $resultado['banco'];
        $arr['num_transferencia'] = $resultado['num_transferencia'];

        // Dados obtidos da tabela 'endereco_fornecedor'
        $arr['municipio']           = $resultado['municipio'];
        $arr['bairro']              = $resultado['bairro'];
        $arr['logradouro']          = $resultado['logradouro'];
        $arr['cep']                 = $resultado['cep'];
        $arr['numero_endereco']     = $resultado['numero_endereco'];
        $arr['complemento']         = $resultado['complemento'];
        $arr['uf']                  = $resultado['uf'];

        return json_encode($arr);
    }

    public function getAvaria ($cod_avaria, $db) {
        $arr = [];

        $sql = "SELECT * FROM avaria
                WHERE cod_avaria = {$cod_avaria}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['cod_avaria']      = $resultado['cod_avaria'];
        $arr['nome_avaria']     = $resultado['nome_avaria'];
        $arr['sugestao_nivel']  = $resultado['sugestao_nivel'];
        $arr['cod_grupo']       = $resultado['cod_grupo'];
        $arr['ativo']           = $resultado['ativo'];

        return json_encode($arr);
    }

    public function getLocalGrupo ($cod, $db) {
        $arr = [];

        $sql = "SELECT * FROM local_grupo
                WHERE cod_local_grupo = {$cod}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
        $resultado = $resultado[0];

        $arr['cod_local_grupo']      = $resultado['cod_local_grupo'];
        $arr['nome_local_grupo']     = $resultado['nome_local_grupo'];
        $arr['cod_grupo']       = $resultado['cod_grupo'];
        $arr['ativo']           = $resultado['ativo'];

        return json_encode($arr);
    }

    public function getHistoricoOdometro ($dados, $db){
        if($dados['tipo'] == 22){
            $sql = "SELECT ho.odometro_ma, ho.odometro_mb, ho.tracao_ma, ho.tracao_mb, ho.gerador_ma, ho.gerador_mb, u.usuario, ho.data_cadastro, {$dados['tipo']} AS grupo FROM odometro_vlt ho JOIN usuario u USING(cod_usuario) WHERE cod_veiculo = {$dados['veiculo']} AND mes = {$dados['mes']} AND ano = {$dados['ano']}
                ORDER BY data_cadastro ASC";
        }else{
            $sql = "SELECT ho.odometro, u.usuario, ho.data_cadastro, {$dados['tipo']} AS grupo FROM odometro_tue ho JOIN usuario u USING(cod_usuario) WHERE cod_veiculo = {$dados['veiculo']} AND mes = {$dados['mes']} AND ano = {$dados['ano']}
                ORDER BY data_cadastro ASC";
        }

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        return json_encode($resultado);
    }

    public function getRetornoOcorrenciaPorFrequencia($db)
    {
        $where = [];

        $hasUnion = false;

        $prefixo = $_POST['_table'] . ".";

        if ($_POST['_table'] == "v_saf") {
            if($_POST['ocorrencia'] == "nome_agente"){
                $ocorrencia = "agente_causador.{$_POST['ocorrencia']}";

            }else if($_POST['ocorrencia'] == "nome_causa"){
                $ocorrencia = "causa.{$_POST['ocorrencia']}";

            }else if($_POST['ocorrencia'] == "nome_atuacao"){
                $ocorrencia = "atuacao.{$_POST['ocorrencia']}";

            }else{
                $ocorrencia = $prefixo . $_POST['ocorrencia'];
            }

            $where[] = "{$prefixo}cod_status = 14"; // SAF Encerrada

            if (!empty($_POST['grupoSistema'])){
                $where[] = "{$prefixo}cod_grupo = {$_POST['grupoSistema']}";

                if (in_array($_POST['grupoSistema'], ['22','23','26']) )
                {
                    $where[] = "osm_registro.cod_ag_causador NOT IN (16, 21, 24, 27, 28, 9, 7, 12, 25, 1, 26)"; // Não Caracteriza Falha
                }else{
                    $where[] = "osm_registro.cod_ag_causador NOT IN (16, 28)"; // Não Caracteriza Falha
                }

                $_POST['_table'] .= " JOIN (
                                    SELECT p.cod_saf, MIN(p.cod_ssm) As MenorSsm
                                    FROM saf AS c
                                        JOIN ssm AS p
                                        ON c.cod_saf = p.cod_saf
                                        GROUP BY p.cod_saf
                                ) As saf_ssm ON v_saf.cod_saf = saf_ssm.cod_saf

                                JOIN ssm As ssm ON saf_ssm.MenorSsm = ssm.cod_ssm

                                JOIN (
                                    SELECT p.cod_ssm, MIN(p.cod_osm) As MenorOsm
                                    FROM ssm AS c
                                        JOIN osm_falha AS p
                                        ON c.cod_ssm = p.cod_ssm
                                        GROUP BY p.cod_ssm
                                ) As ssm_osm ON ssm.cod_ssm = ssm_osm.cod_ssm

                                JOIN osm_falha As osm ON ssm_osm.MenorOsm = osm.cod_osm
                                JOIN osm_registro ON osm_registro.cod_osm = osm.cod_osm
                                JOIN agente_causador ON osm_registro.cod_ag_causador = agente_causador.cod_ag_causador
                                JOIN causa ON osm_registro.cod_causa = causa.cod_causa";
            }else{
                $hasUnion = true;
            }


            $colunaDataPartitAte = 'data_abertura';

        }else if ($_POST['_table'] == "v_ssp") {
            $ocorrencia = $prefixo . $_POST['ocorrencia'];

            $where[] = "{$prefixo}cod_status = 18"; // SSP Encerrada

            $colunaDataPartitAte = 'data_programada';
        }else{
            $ocorrencia = $prefixo . $_POST['ocorrencia'];
        }


        if (!empty($_POST['grupoSistema'])){
            $where[] = "{$prefixo}cod_grupo = {$_POST['grupoSistema']}";
        }

        if (!empty($_POST['sistema']))
            $where[] = "{$prefixo}cod_sistema = {$_POST['sistema']}";

        if (!empty($_POST['subSistema']))
            $where[] = "{$prefixo}cod_subsistema = {$_POST['subSistema']}";

        if (!empty($_POST['nivel']))
            $where[] = "{$prefixo}nivel = '{$_POST['nivel']}'";

        if (!empty($_POST['linha']))
            $where[] = "{$prefixo}cod_linha = {$_POST['linha']}";

        if (!empty($_POST['trecho']))
            $where[] = "{$prefixo}cod_trecho = {$_POST['trecho']}";

        $dataPartir = '';
        $dataRetroceder = '';
        if (!empty($_POST['dataPartirDatAber']))
            $dataPartir = $this->inverteData($_POST['dataPartirDatAber']);

        if (!empty($_POST['dataRetrocederDatAber']))
            $dataRetroceder = $this->inverteData($_POST['dataRetrocederDatAber']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "{$prefixo}{$colunaDataPartitAte} >= '{$dataPartir}'";
                $where[] = "{$prefixo}{$colunaDataPartitAte} <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "{$prefixo}{$colunaDataPartitAte} >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "{$prefixo}{$colunaDataPartitAte} < '{$dataRetroceder}'";
                }
            }
        }

        if (!empty($_POST['origemProgramacao']))
            $where[] = "{$prefixo}tipo_orissp = {$_POST['origemProgramacao']}";

        $dataPartir = '';
        $dataRetroceder = '';
        if (!empty($_POST['dataPartirPeriodo']))
            $dataPartir = $this->inverteData($_POST['dataPartirPeriodo']);

        if (!empty($_POST['dataRetrocederPeriodo']))
            $dataRetroceder = $this->inverteData($_POST['dataRetrocederPeriodo']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";

                $where[] = "{$prefixo}XXXXXXXX >= '{$dataPartir}'";
                $where[] = "{$prefixo}XXXXXXXX <= '{$dataRetroceder}'";

            } else {
                if (!empty($dataPartir)) {
                    $dataPartir = $dataPartir . " 00:00:00";
                    $where[] = "{$prefixo}XXXXXXXX >= '{$dataPartir}'";
                }

                if (!empty($dataRetroceder)) {
                    $dataRetroceder = $dataRetroceder . " 23:59:59";
                    $where[] = "{$prefixo}XXXXXXXX < '{$dataRetroceder}'";
                }
            }
        }

        if (!empty($_POST['periodicidadePesquisaCronograma']))
            $where[] = "{$prefixo}XXXXXXXX = {$_POST['periodicidadePesquisaCronograma']}";

        if (!empty($_POST['quinzenaCronograma']))
            $where[] = "{$prefixo}quinzena = {$_POST['quinzenaCronograma']}";

        if (!empty($_POST['mes']))
            $where[] = "{$prefixo}mes = {$_POST['mes']}";

        if (!empty($_POST['ano']))
            $where[] = "{$prefixo}ano = {$_POST['ano']}";

        if (!empty($_POST['cod_avaria']))
            $where[] = "{$prefixo}cod_avaria = {$_POST['cod_avaria']}";

        if (!empty($_POST['tipoIntervencao']))
            $where[] = "{$prefixo}cod_tipo_intervencao = {$_POST['tipoIntervencao']}";

        if (!empty($_POST['servico']))
            $where[] = "{$prefixo}cod_servico = {$_POST['servico']}";

        if (!empty($_POST['unidade']))
            $where[] = "{$prefixo}cod_unidade = {$_POST['unidade']}";

        if (!empty($_POST['equipe']))
            $where[] = "{$prefixo}sigla_equipe = '{$_POST['equipe']}'";

        if (!empty($_POST['veiculo']))
            $where[] = "{$prefixo}cod_veiculo = {$_POST['veiculo']}";

        if (!empty($_POST['carro']))
            $where[] = "{$prefixo}cod_carro = {$_POST['carro']}";

        if (!empty($_POST['localGrupo']))
            $where[] = "{$prefixo}cod_local_grupo = {$_POST['localGrupo']}";

        if (!empty($_POST['subLocalGrupo']))
            $where[] = "{$prefixo}cod_sublocal_grupo = {$_POST['subLocalGrupo']}";

        $where[] = "{$ocorrencia} NOTNULL";


        if($hasUnion){
            if (!empty($where)) {
                $allWhere = implode(' AND ', $where);
            }

            $sql = "SELECT ocorrencia, sum(count) as contador
                FROM (
                    SELECT {$ocorrencia} as ocorrencia, count(*)
                    FROM v_saf
                    JOIN (
                        SELECT p.cod_saf, MIN(p.cod_ssm) As MenorSsm
                        FROM saf AS c
                        JOIN ssm AS p ON c.cod_saf = p.cod_saf
                        GROUP BY p.cod_saf
                    ) As saf_ssm ON v_saf.cod_saf = saf_ssm.cod_saf

                    JOIN ssm As ssm ON saf_ssm.MenorSsm = ssm.cod_ssm
                    JOIN (
                        SELECT p.cod_ssm, MIN(p.cod_osm) As MenorOsm
                        FROM ssm AS c
                        JOIN osm_falha AS p ON c.cod_ssm = p.cod_ssm
                        GROUP BY p.cod_ssm
                    ) As ssm_osm ON ssm.cod_ssm = ssm_osm.cod_ssm

                    JOIN osm_falha As osm ON ssm_osm.MenorOsm = osm.cod_osm
                    JOIN osm_registro ON osm_registro.cod_osm = osm.cod_osm
                    JOIN agente_causador ON osm_registro.cod_ag_causador = agente_causador.cod_ag_causador
                    JOIN causa ON osm_registro.cod_causa = causa.cod_causa
                    WHERE {$allWhere} AND v_saf.cod_grupo NOT IN(22,23,26) AND osm_registro.cod_ag_causador NOT IN (16, 28)
                    GROUP BY {$ocorrencia}

                    UNION ALL

                    SELECT {$ocorrencia} as ocorrencia, count(*)
                    FROM v_saf
                    JOIN (
                        SELECT p.cod_saf, MIN(p.cod_ssm) As MenorSsm
                        FROM saf AS c
                        JOIN ssm AS p ON c.cod_saf = p.cod_saf
                        GROUP BY p.cod_saf
                    ) As saf_ssm ON v_saf.cod_saf = saf_ssm.cod_saf

                    JOIN ssm As ssm ON saf_ssm.MenorSsm = ssm.cod_ssm
                    JOIN (
                        SELECT p.cod_ssm, MIN(p.cod_osm) As MenorOsm
                        FROM ssm AS c
                        JOIN osm_falha AS p ON c.cod_ssm = p.cod_ssm
                        GROUP BY p.cod_ssm
                    ) As ssm_osm ON ssm.cod_ssm = ssm_osm.cod_ssm

                    JOIN osm_falha As osm ON ssm_osm.MenorOsm = osm.cod_osm
                    JOIN osm_registro ON osm_registro.cod_osm = osm.cod_osm
                    JOIN agente_causador ON osm_registro.cod_ag_causador = agente_causador.cod_ag_causador
                    JOIN causa ON osm_registro.cod_causa = causa.cod_causa
                    WHERE {$allWhere} AND v_saf.cod_grupo IN(22,23,26) AND osm_registro.cod_ag_causador NOT IN (16, 21, 24, 27, 28)
                    GROUP BY {$ocorrencia}
                ) as v_saf GROUP BY ocorrencia ORDER BY contador DESC";

        }else{
            //SINTAX SQL para pesquisa no PostGres
            $sql = "SELECT {$ocorrencia} as ocorrencia, count(*) as contador FROM {$_POST['_table']}";

            //Campos WHERE ser�o adicionados conforme preenchidos respectivos campos
            //Verifica se h� dados e divide os campos adicionando 'AND'
            if (!empty($where)) {
                $sql = $sql . ' WHERE ' . implode(' AND ', $where);
            }

            $sql = $sql . " GROUP BY {$ocorrencia}";
            $sql = $sql . " ORDER BY contador DESC";
        }

        $arr['sql'] = $sql;
//        $arr['post'] = $_POST;

//        Executa o select a partir do medoo
        $resultado = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
//            return $sql;
        foreach ($resultado as $dados => $value) {
            $arr['valorX'][] = [utf8_encode($value['ocorrencia']), (int)$value['contador']];
        }
        return json_encode($arr);
    }

    public function getServicoMr($dados, $db){
        $sql = "SELECT cod_servico_material_rodante, nome_servico_material_rodante FROM servico_material_rodante WHERE cod_grupo = {$dados}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['cod_servico_material_rodante'] => $dados['nome_servico_material_rodante']
                );
            }
        }

        return json_encode($arr);
    }

    public function getFrota($dados, $db){
        $sql = "SELECT distinct(frota) FROM veiculo WHERE cod_grupo = {$dados}";

        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = Array();
        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                $arr += array(
                    $dados['frota'] => $dados['frota']
                );
            }
        }
        return json_encode($arr);
    }

    public function apiDisponibilidadeMR($dados, $db){

        switch($dados["mode"]){
            case "1":
                $sql = "SELECT *
                        FROM calc_disponibilidade
                        WHERE dia = {$dados['dia']} AND mes= {$dados['mes']} AND ano={$dados['ano']} AND cod_linha={$dados['linha']}";
                $sql = $db->prepare($sql);
                $sql->execute();
                $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

                if(!empty($resultado))
                    return json_encode(["hasDisp" => false]);

                return json_encode(["hasDisp" => true]);
                break;
            case "2":
                $sql = "SELECT *
                        FROM calc_disponibilidade
                        WHERE mes= {$dados['mes']} AND ano={$dados['ano']} AND cod_linha={$dados['linha']}";
                $sql = $db->prepare($sql);
                $sql->execute();
                $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

                if(!empty($resultado))
                    return json_encode($resultado);

                return json_encode(["hasDisp" => false]);;
                break;
        }
    }

    public function getQtdFalhaDia($dados, $db){
        $sql = "SELECT vsaf.dia, COUNT(vsaf.cod_saf) AS valor
		FROM
		(
			SELECT
				date_part('day', data_abertura) AS dia,
		   		date_part('month', data_abertura) AS mes,
		   		date_part('year', data_abertura) AS ano, *
		 	FROM v_saf
		) AS vsaf

		INNER JOIN
		(
			SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
			FROM saf sa
			INNER JOIN ssm s
			ON sa.cod_saf = s.cod_saf
			GROUP BY s.cod_saf
		) AS menor_ssm
		ON vsaf.cod_saf = menor_ssm.cod_saf

		INNER JOIN ssm
		ON menor_ssm.menor = ssm.cod_ssm

		INNER JOIN
		(
			SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
			FROM ssm sm
			INNER JOIN osm_falha o
			ON sm.cod_ssm = o.cod_ssm
			GROUP BY o.cod_ssm
		) AS menor_osm
		ON ssm.cod_ssm = menor_osm.cod_ssm

		INNER JOIN osm_falha osm
		ON menor_osm.menorosm = osm.cod_osm

		INNER JOIN osm_registro osmr
		ON osmr.cod_osm = osm.cod_osm

		WHERE vsaf.cod_status = 14 AND osmr.cod_ag_causador NOT IN (16, 21, 24, 27, 28) {$dados}
		GROUP BY vsaf.dia
		ORDER BY vsaf.dia";

//        $sql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode($resultado);
    }

    public function getQtdFalhaDiaPorNivel($dados, $db){
        $sql = "SELECT vsaf.dia, vsaf.nivel, COUNT(vsaf.cod_saf) AS valor
		FROM
		(
			SELECT
				date_part('day', data_abertura) AS dia,
		   		date_part('month', data_abertura) AS mes,
		   		date_part('year', data_abertura) AS ano, *
		 	FROM v_saf
		) AS vsaf

		INNER JOIN
		(
			SELECT s.cod_saf,  MIN(s.cod_ssm) AS menor
			FROM saf sa
			INNER JOIN ssm s
			ON sa.cod_saf = s.cod_saf
			GROUP BY s.cod_saf
		) AS menor_ssm
		ON vsaf.cod_saf = menor_ssm.cod_saf

		INNER JOIN ssm
		ON menor_ssm.menor = ssm.cod_ssm

		INNER JOIN
		(
			SELECT o.cod_ssm, MIN(o.cod_osm) AS menorosm
			FROM ssm sm
			INNER JOIN osm_falha o
			ON sm.cod_ssm = o.cod_ssm
			GROUP BY o.cod_ssm
		) AS menor_osm
		ON ssm.cod_ssm = menor_osm.cod_ssm

		INNER JOIN osm_falha osm
		ON menor_osm.menorosm = osm.cod_osm

		INNER JOIN osm_registro osmr
		ON osmr.cod_osm = osm.cod_osm

		WHERE vsaf.cod_status = 14 AND osmr.cod_ag_causador NOT IN (16,21,24,27,28) {$dados}
		GROUP BY vsaf.dia, vsaf.nivel
		ORDER BY vsaf.dia, vsaf.nivel";

//        $sql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $sql = $db->prepare($sql);
        $sql->execute();
        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        $arr = [];
        foreach($resultado as $dados => $value)
        {
            $arr[$value['dia']][$value['nivel']] = $value['valor'];
        }

        echo json_encode($arr);
    }

    public function getAppMr($dados, $db) {
        $sql = "SELECT cod_servico_material_rodante, nome_servico_material_rodante, qtd_manutencao,
                (
                    SELECT COUNT(*) AS executadas FROM v_ssp WHERE cod_servico = cod_servico_material_rodante
                )
                FROM pmp_mr
                JOIN servico_material_rodante USING(cod_servico_material_rodante)
                WHERE mes = $dados[0] AND ano = $dados[1]";

//        $sql = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $sql = $db->prepare($sql);
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

//        $arr = [];
//        foreach($resultado as $dados => $value)
//        {
//            $arr[$value['dia']][$value['nivel']] = $value['valor'];
//        }

        echo json_encode($resultado);
    }

    public function getFuncionariosByTipo($dados, $db) {

        $cod_origem = $dados;

        $sql = "SELECT
                f.cod_funcionario,
                fe.matricula,
                f.nome_funcionario,
                fe.cod_tipo_funcionario
                FROM funcionario_empresa fe
                JOIN funcionario f USING(cod_funcionario_empresa)
                WHERE fe.cod_tipo_funcionario = $cod_origem AND ativo='s'";

        $sql = $db->prepare($sql);
        $sql->execute();

        $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode($resultado);
    }

    public function TesteApi(){
        try {
            $db = new PDO("pgsql:host=127.0.0.1; port=5433;  dbname=SIMGBKP", "postgres", "postgres");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        //==================> Início de código

        if(!empty($dados)){
            if ($dados == "3-meses") {
                $ano = date('Y', time());
                $mes = date('m', time());
            } else {
                $mes = substr($dados, 0, 2);
                $ano = substr($dados, 3);
            }
        }else{
            $mes = Date('m');
            $ano = Date('Y');
        }

        if ($mes == 12) {
            $proxMes = 1;
            $proxAno = $ano + 1;

            $antMes = 11;
            $antAno = $ano;
        } else if ($mes == 1) {
            $proxMes = 2;
            $proxAno = $ano;

            $antMes = 12;
            $antAno = $ano - 1;
        } else {
            $proxMes = (int)$mes + 1;
            $proxAno = $ano;

            $antMes = (int)$mes - 1;
            $antAno = $ano;
        }

        try {
            if ($dados = "3-meses")
                $sql = $db->prepare("SELECT * FROM v_ssp WHERE data_programada BETWEEN '".$antAno."-".$antMes."-01 00:00:00' AND '".$proxAno."-".$proxMes."-01 00:00:00'");
            else
                $sql = $db->prepare("SELECT * FROM v_ssp WHERE data_programada BETWEEN '".$ano."-".$mes."-01 00:00:00' AND '".$proxAno."-".$proxMes."-01 00:00:00'");
            $sql->execute();

            $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

            echo var_dump($resultado);
        } catch (PDOException $e) {
            return $e;
        }
        $arr = Array();

        if (!empty($resultado)) {
            foreach ($resultado as $dados) {
                /** Obs.
                 *  utf8_decode() - Transforma toda a string na mesma codificação
                 *  para evitar conflito em  utf8_encode(), pois, a função é chamada pelo javascript que só funciona em utf-8
                 *  ( Informações de Banco já vem em utf-8, enquanto o sistema é ISO-8859-1 )
                 **/

                $title = utf8_decode($dados['cod_ssp'] . " - " . $dados['nome_servico']);
                $descr = '<div class="row">
								<div class="col-md-4">
									<label>Codigo Ssp</label>
									<input disabled type="text" name="codigoSspCallendar" value="' . $dados['cod_ssp'] . '" class="form-control">
								</div>
								<div class="col-md-8">
									<label>Status</label>
									<input disabled type="text" value="' . utf8_decode($dados['nome_status']) . '" class="form-control">
								</div>
							</div>
							<label>Tipo Intervenção</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_tipo_intervencao']) . '" class="form-control">
							<label>Local</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_trecho'] . ' - ' . $dados['descricao_trecho']) . '" class="form-control">
							<label>Equipe</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_equipe']) . '" class="form-control">
							<label>Serviço</label>
							<input disabled type="text" value="' . utf8_decode($dados['nome_servico']) . '" class="form-control">';
                $dataProgramada = $dados['data_programada'];
                $diasServico = $dados['dias_servico'];

                $timestamp = strtotime($dataProgramada);

                $start = date('Y/m/d', $timestamp);
                $end = date('Y/m/d', strtotime('+' . $diasServico . ' days', strtotime($dataProgramada)));

                if ($dados['nome_status'] == "Encerrada" || $dados['nome_status'] == "Cancelada") {
                    $backcolor = "gray";
                } else if ($dados['tipo_orissp'] == "2") {
                    $backcolor = "red";
                } else if ($dados['nome_status'] == "Pendente") {
                    $backcolor = "blue";
                } else if ($dados['nome_status'] == "Autorizada") {
                    $backcolor = "green";
                } else {
                    $backcolor = "default";
                }

                $data = array(
                    "allDay" => true,
                    "title" => utf8_encode($title),
                    "description" => utf8_encode($descr),
                    "start" => $start,
                    "end" => $end,
                    "backgroundColor" => $backcolor
                );
                $arr[] = $data;
            }
            return json_encode($arr);
        }


        //==============> FIM de Código

        echo json_encode($resultado);
    }

    public function filter($var)
    {
        return $var;
    }

    public function returnResult()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();

        try {
            $db = new PDO("pgsql:host={$this->host}; port={$this->port};  dbname={$this->bdname}", "{$this->user}", "{$this->password}");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        if (!empty($parametros[1])) {
            echo $this->retorna($_GET[$parametros[0]], $parametros[1], $db);
        } else {
            echo $this->retorna($_GET[$parametros[0]], $parametros[0], $db);
        }
    }

    public function getResult()
    {

        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        /* só se for enviado o parâmetro, que devolve os dados */

        try {
            $db = new PDO("pgsql:host={$this->host}; port={$this->port};  dbname={$this->bdname}", "{$this->user}", "{$this->password}");
        } catch (PDOException $e) {
            print $e->getMessage();
        }

        echo $this->retorna($parametros[0], $parametros[0], $db);

    }

    public function parameterResult()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        /* só se for enviado o parâmetro, que devolve os dados */

        try {
            $db = new PDO("pgsql:host={$this->host}; port={$this->port};  dbname={$this->bdname}", "{$this->user}", "{$this->password}");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        echo $this->retorna($parametros[0], $parametros[1], $db);
    }


    private function inverteData($data = null)
    {
        $nova_data = null;                                            // Configura uma variável para receber a nova data

        if (!empty($data)){                                                // Se a data for enviada

            $data = preg_split('/\-|\/|\s|:/', $data);    // Explode a data por -, /, : ou espaço
            $data = array_map('trim', $data);                // Remove os espaços do começo e do fim dos valores
            $nova_data .= chk_array($data, 2) . '-';            // Cria a data invertida
            $nova_data .= chk_array($data, 1) . '-';            // Cria a data invertida
            $nova_data .= chk_array($data, 0);                    // Cria a data invertida

            if (chk_array($data, 3)) {
                $nova_data .= ' ' . chk_array($data, 3);        // Configura a hora
            }

            if (chk_array($data, 4)) {
                $nova_data .= ':' . chk_array($data, 4);        // Configura os minutos
            }

            if (chk_array($data, 5)) {
                $nova_data .= ':' . chk_array($data, 5);        // Configura os segundos
            }
        }
        return $nova_data;                                            // Retorna a nova data
    }

}
