<?php

/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 17/03/2016
 * Time: 11:03
 */
class DashboardDiretoriaController extends MainController
{
    public $intervalo = [
        1 => "Quinzenal",
        2 => "Mensal",
        4 => "Bimestral",
        6 => "Trimestral",
        12 => "Semestral",
        24 => "Anual"
    ];

    public $loginNecessario = true;
    public $nivelNecessario = "6";

    public function index()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboard.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function tabela()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";


        $_SESSION['tabela'] = $_POST;

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "scriptTabela.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        if ($_POST['quinzenaPmp'])
            require_once(ABSPATH . "/views/diretoria/pesquisaPmp/tabelaGeral.php");
        else
            require_once(ABSPATH . "/views/diretoria/pesquisaPmp/tabelaQuinzenal.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function gerarRelatorio()
    {
        $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
        $relatorio = $parametros[0];

        $_SESSION['post'] = $_POST;

        $this->titulo = "Relat�rio";
        $this->script = "relatorios/diretoria/script{$relatorio}.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/relatorios/diretoria/rel{$relatorio}.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function moduloEngenharia()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboardEngenharia.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function moduloOrganizacional()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboardOrganizacional.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function moduloMaoObra()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboardMaoObra.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function moduloOs()
    {
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboardCronograma.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function moduloAlmoxarifado(){
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboardAlmoxarifado.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }

    public function moduloEstadoFrota(){
        $this->controleAcesso();

        $_SESSION['direcionamento'] = "Diretoria";
        $_SESSION['navegador'] = "sidebarDiretoria.php";

        $this->pagina = "";

        $this->titulo = "Sistema de Manuten��o - Metro Service / METROFOR";
        $this->script = "dashboardDiretoria.js";

        require_once(ABSPATH . "/views/_includes/_header.php");
        require_once(ABSPATH . "/views/_includes/navegadores/navegador.php");
        require_once(ABSPATH . "/views/_includes/_body.php");
        require_once(ABSPATH . "/views/diretoria/dashboardFrota.php");
        require_once(ABSPATH . "/views/_includes/_footer.php");
    }
    public function printModuloNavigator()
    {
        echo('
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-sitemap fa-5x fa-fw"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div><label>Estrutura Organizacional</label></div>
                            </div>
                        </div>
                    </div>
                    <a href="' . HOME_URI . '/dashboardDiretoria/moduloOrganizacional">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Estrutura Organizacional</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-television fa-5x fa-fw"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div><label>Engenharia</label></div>
                            </div>
                        </div>
                    </div>
                    <a href="' . HOME_URI . '/dashboardDiretoria/moduloEngenharia">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Engenharia</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-gears fa-5x fa-fw"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div><label>Ordens de Servi�o</label></div>
                            </div>
                        </div>
                    </div>
                    <a href="' . HOME_URI . '/dashboardDiretoria/moduloOs">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Ordens de Servi�o</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-cubes fa-5x fa-fw"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div><label>Suprimentos/Almoxarifado</label></div>
                            </div>
                        </div>
                    </div>
                    <a href="' . HOME_URI . '/dashboardDiretoria/moduloAlmoxarifado">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Suprimentos/Almoxarifado</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x fa-fw"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div><label>Gest�o de M�o de Obra</label></div>
                            </div>
                        </div>
                    </div>
                    <a  href="' . HOME_URI . '/dashboardDiretoria/moduloMaoObra">
                        <div class="panel-footer">
                            <span class="pull-left">Ver M�o de Obra</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-train fa-5x fa-fw"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div><label>Estado da Frota</label></div>
                            </div>
                        </div>
                    </div>
                    <a href="' . HOME_URI . '/dashboardDiretoria/moduloEstadoFrota">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Estado da Frota</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        
        <div style="border: 0.1em solid black; margin-bottom: 2em;"></div>
        ');
    }

    public function exibirPdfProcedimento()
    {
        require_once(ABSPATH . "/views/diretoria/exibirPdfProcedimento.php");
    }

    public function exibirPdfTreinamento()
    {
        require_once(ABSPATH . "/views/diretoria/exibirPdfTreinamentoFuncionario.php");
    }

}
