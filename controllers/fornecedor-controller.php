<?php

class FornecedorController extends MainController
{
  public $nivelNecessario = "4";

  public function __construct($parametros = array())
  {
      parent::__construct($parametros);
      
      $this->controleAcesso();
  }

  public function create()
  {
    $this->titulo = "SIMG";                                            # T�tulo da P�gina

    require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina
    require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
        . "navegador.php");
    require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body
    require_once(ABSPATH . "/views/forms/fornecedor/create.php");                 # Carrega o respectivo conte�do
    require_once(ABSPATH . "/views/_includes/_footer.php");                           # Carrega o footer
  }

  public function store()
  {
    $fornecedorModel = $this->carregaModelo('fornecedor-model');

    $fornecedorModel->create();

    header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");

  }

  public function edit()
  {
    $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
    $fornecedor = $parametros[0];

    $this->titulo = "SIMG";

    $dadosFornecedor = $this->medoo->select("v_fornecedor", "*", ["cod_fornecedor" => $fornecedor])[0];

    require_once(ABSPATH . "/views/_includes/_header.php");                           # Carrega o header da p�gina
    require_once(ABSPATH . "/views/_includes/navegadores/"                            # Carrega o respectivo navegador
        . "navegador.php");
    require_once(ABSPATH . "/views/_includes/_body.php");                             # Carrega o Body
    require_once(ABSPATH . "/views/forms/fornecedor/edit.php");                 # Carrega o respectivo conte�do
    require_once(ABSPATH . "/views/_includes/_footer.php");
  }

  public function update()
  {
    $parametros = (func_num_args() >= 1) ? func_get_arg(0) : array();
    $fornecedor = $parametros[0];

    $fornecedorModel = $this->carregaModelo('fornecedor-model');

    $fornecedorModel->update($fornecedor);

    header("Location:{$this->home_uri}/dashboard/{$_SESSION['direcionamento']}");
  }
}
 ?>
