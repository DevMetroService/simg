<?php
/**
 * Created by PhpStorm.
 * User: ricardo.hernandez
 * Date: 10/09/2020
 * Time: 10:14
 */

class UnidadeMedidaController extends MainController
{
    public function get()
    {
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        $obj = $this->medoo->select("unidade_medida", "*", ["cod_uni_medida" => (int)$parametros[0]])[0];

        echo json_encode($obj);
    }

    public function all()
    {
        return $this->medoo->select("unidade_medida", "*");
    }

    public function insert()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

    public function getFromMaterial()
    {
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

        $obj = $this->medoo->select("material",
            [
                "[><]unidade_medida" => "cod_uni_medida"
            ], [
                "cod_uni_medida",
                "nome_uni_medida",
                "sigla_uni_medida"
            ], ["cod_material" => (int)$parametros[0]])[0];

        echo json_encode($obj);
    }

}