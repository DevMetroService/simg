<?php

/**
 * Controller de login para a aplica��o S.I.M.G
 *
 * @package SimgMVC
 * @since 0.1
 *
 * Este c�digo � confidencial.
 * A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 *
 */
class LogoutController extends MainController
{

    public function index()

    {

        $_SESSION ['dadosUsuario'] = array();       # Remove todos os dados de $_SESSION['dadosUsuario']
        unset ($_SESSION ['dadosUsuario']);         # Comando redudante, apenas para ter certeza que os dados foram apagados
        session_regenerate_id();                    # Regeramos o ID da sessao
        $this->redirecionarParaPaginaLogin();       # Redirecionamos o usuario para a p�gina de login

    }
}

?>