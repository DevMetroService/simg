<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;iso-8859-1">

    <!-- BLoco de CSS -->
    <link href='http://fonts.googleapis.com/css?family=Exo+2:700,300,400' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="<?php echo HOME_URI; ?>/views/_css/style.css" />
    <link rel="icon" type="image/jpg" href="<?php echo HOME_URI ?>/views/_images/mini_metroservice_logo.png" />

    <!-- T�tulo -->
    <title>SIMG - Page not found</title>

</head>

<body>

<div id="login" class="form bradius">
    <!-- Login -->

    <!-- Logo -->
    <div class="logo">
        <a href="index.php" title="Metro Service"><img alt="Metro Service" src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo.png" width="250px" height="78px"/></a>
    </div>
    <!-- Fim logo -->

    <!-- Acomodar -->
    <div class="acomodar">
        <br />
        <h2>404 - P�gina n�o encontrada.</h2>
        <br />
        <a href="javascript:window.history.go(-1)">Voltar</a>
    </div>
    <!-- Fim Acomodar -->

</div>
</body>
</html>