<?php
/**
 * Created by PhpStorm.
 * User: ricardo.diego
 * Date: 08/07/2016
 * Time: 14:57
 */

$notificar = new NovidadesModal();
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;ISO-8859-1">

    <!-- BLoco de CSS -->
    <link href='http://fonts.googleapis.com/css?family=Exo+2:700,300,400' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="<?php echo HOME_URI; ?>/views/_css/style.css" />
    <link rel="icon" type="image/jpg" href="<?php echo HOME_URI ?>/views/_images/mini_metroservice_logo.png" />
    <link rel="stylesheet" href="<?php echo HOME_URI ?>/views/_css/bootstrap/bootstrap.min.css">

    <!-- Chamada do css do font-Awesome -->
    <link type="text/css" rel="stylesheet"
          href="<?php echo HOME_URI ?>/views/_css/font-awesome/css/font-awesome.min.css">

    <!-- T�tulo -->
    <title><?php echo $this->titulo; ?></title>

</head>

<body>

<div id="login" class="form bradius" style="width:80%; min-height:78px;">
    <!-- Login -->

    <!-- Logo -->
    <div class="logo">
        <a href="index.php" title="Metro Service"><img alt="Metro Service" src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo.png" width="250px" height="78px"/></a>
    </div>
    <!-- Fim logo -->

    <!-- Acomodar -->
    <div>
        <br />
        <h2>Novidades de <?php echo $notificar->dataNotificacao; ?></h2>
        <hr />
        <br />

        <?php
        foreach($notificar->mensagens as $mensagem){
            echo   "<h4>
                        <span>&#10020;</span> {$mensagem}
                    </h4>";
        }
        ?>

        <hr />
        <br />

        <input id="euLi" type="checkbox" value="aceito" /> <span>Li e compreendi todas as novidades e atualiza��es do Sistema Integrado de Manuten��o e Gest�o (SIMG).</span>
        <br />
        <button id="ir" disabled class="btn btn-info btn-lg" onclick="ir()" title="Acessar Sistema">Acessar Sistema</button>
    </div>
    <!-- Fim Acomodar -->

</div>

<!-- Bloco de BootStrap / Jquery  -->
<script type="text/javascript" src="<?php echo HOME_URI ?>/views/_js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URI ?>/views/_js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo HOME_URI ?>/views/_js/bootstrap.min.js"></script>

<script>

    $('#euLi').on("change", function () {
        if ($(this).is(':checked')){
            $('#ir').removeAttr("disabled");
        }else{
            $('#ir').attr("disabled", "disabled");
        }
    });

</script>
</body>

</html>