<?php
/**
 * User: Valmar J�nior
 * Date: 13/11/2014
 * Time: 14:50
 */
define ( "MENSAGEM_CONTA_N�O_ATIVADA", "Sua conta ainda n�o foi ativada. Favor clicar no link de confirma��o enviado por email." );
define ( "MENSAGEM_CAPTCHA_ERRADO", "Captcha incorreto!" );
define ( "MENSAGEM_COOKIE_INVALID", "Cookie inv�lido" );
define ( "MENSAGEM_DATABASE_ERROR", "Erro de conex�o com o banco de dados." );
define ( "MENSAGEM_EMAIL_ALREADY_EXISTS", "Este e-mail j� est� registrado. Tente usar a \"recupera��o de senha\"." );
define ( "MENSAGEM_EMAIL_CHANGE_FALHA", "Desculpe, a altera��o de e-mail falhou." );
define ( "MENSAGEM_EMAIL_CHANGED_SUCESSO", "Seu e-mail foi alterado com sucesso. Novo e-mail � " );
define ( "MENSAGEM_EMAIL_EMPTY", "Email n�o pode ficar em branco" );
define ( "MENSAGEM_EMAIL_INVALID", "Seu e-mail possui um formato inv�lido" );
define ( "MENSAGEM_EMAIL_SAME_LIKE_OLD_ONE", "Desculpe, este email � o mesmo do atual. Por favor informe outro email." );
define ( "MENSAGEM_EMAIL_TOO_LONG", "Email n�o pode ter mais de 64 caracteres" );
define ( "MENSAGEM_LINK_PARAMETER_EMPTY", "Link vazio." );
define ( "MENSAGEM_LOGGED_OUT", "Voc� saiu.." );

define ( "MENSAGEM_LOGIN_FALHA", "Login falhou." );
define ( "MENSAGEM_SENHA_ANTIGA_ERRADO", "Sua senha antiga est� incorreta." );
define ( "MENSAGEM_SENHA_NAO_COICIDEM", "As senhas informadas n�o coincidem" );
define ( "MENSAGEM_SENHA_ALTERADA_FALHA", "Desculpe, a altera��o de senha falhou." );
define ( "MENSAGEM_SENHA_ALTERADA_SUCESSO", "Senha alterada com sucesso!" );
define ( "MENSAGEM_RESETAR_VAZIA", "Senha est� em branco" );
define ( "MENSAGEM_RESETAR_SENHA_MAIL_FALHA", "Email de recupera��o de senha n�o foi enviado! Erro: " );
define ( "MENSAGEM_RESETAR_SENHA_MAIL_SUCESSO_ENVIADO", "Email de recupera��o de senha enviado!" );
define ( "MENSAGEM_PASSWORD_CURTO", "Tamanho m�nimo da senha � de 6 caracteres" );
define ( "MENSAGEM_PASSWORD_ERRADO", "Senha incorreta. Tente novamente." );
define ( "MENSAGEM_PASSWORD_ERRADO_3_VEZES", "Voc� inseriu uma senha incorreta 3 vezes ou mais. Favor aguardar 30 segundos e tente novamente." );
define ( "MENSAGEM_ATIVACAO_DE_REGISTRO_N�O_SUCEDIDA", "Desculpe, nenhum id encontrado..." );
define ( "MENSAGEM_ATIVACAO_DE_REGISTRO_SUCEDIDA", "Ativa��o bem sucedida! Voc� pode entrar agora!" );
define ( "MENSAGEM_REGISTRATION_FALHA", "Desculpe, seu registro falhou. Volte e tente novamente." );
define ( "MENSAGEM_RESET_LINK_HAS_EXPIRED", "Este link de recupera��o expirou. Use o link sempre em menos de uma hora." );
define ( "MENSAGEM_VERIFICACAO_DE_EMAIL_ERRO", "Desculpe, n�o foi poss�vel enviar um email de verifica��o. Sua conta n�o foi criada." );
define ( "MENSAGEM_VERIFICACAO_DE_EMAIL_N�O_ENVIADO", "Email de verifica��o n�o foi enviado! Erro: " );
define ( "MENSAGEM_VERIFICACAO_DE_EMAIL_ENVIADO", "Sua conta foi criada e enviamos um email. Clique no link de verifica��o deste email." );
define ( "MENSAGEM_USER_DOES_N�O_EXIST", "Este usu�rio n�o existe" );
define ( "MENSAGEM_USERNAME_CURTA", "Usu�rio n�o pode conter menos que 2 caracteres ou mais que 64" );
define ( "MENSAGEM_USERNAME_MUDAR_FALHA", "Desculpe, a altera��o do nome de usu�rio falhou" );
define ( "MENSAGEM_USERNAME_MUDAR_SUCESSO", "Seu nome de usu�rio foi alterado com sucesso. Novo nome de usu�rio � " );
define ( "MENSAGEM_USERNAME_VAZIA", "Campo nome de usu�rio est� vazio" );
define ( "MENSAGEM_USERNAME_EXISTE", "Desculpe, este nome de usu�rio j� foi utilizado. Escolha outro." );
define ( "MENSAGEM_USERNAME_INVALIDO", "Nome de usu�rio fora do padr�o: somente a-Z e n�meros s�o permitidos, 2 a 64 caracteres" );
define ( "MENSAGEM_USERNAME_IGUAL_AO_ANTIGO", "Desculpe, o nome de usu�rio � o mesmo atual. Escolha outro." );

define ( "PALAVRA_VOLTAR_AO_LOGIN", "Voltar ao Login" );
define ( "PALAVRA_MUDAR_EMAIL", "Alterar email" );
define ( "PALAVRA_MUDAR_SENHA", "Alterar senha" );
define ( "PALAVRA_MUDAR_USUARIO", "Alterar nome de usu�rio" );
define ( "PALAVRA_ATUALMENTE", "atualmente" );
define ( "PALAVRA_EDITAR_USUARIO_DADOS", "Editar dados do usu�rio" );
define ( "PALAVRA_EDITAR_SUAS_CREDENCIAIS", "Voc� est� logado e pode editar suas informa��es aqui" );
define ( "PALAVRA_ESQUECI_MINHA_SENHA", "Esqueci minha senha" );
define ( "PALAVRA_LOGIN", "Entrar" );
define ( "PALAVRA_LOGOUT", "Sair" );
define ( "PALAVRA_NOVA_EMAIL", "Novo email" );
define ( "PALAVRA_NOVA_SENHA", "Nova senha" );
define ( "PALAVRA_NOVA_SENHA_REPEAT", "Repetir nova senha" );
define ( "PALAVRA_NOVA_USUARIO", "Novo nome de usu�rio (n�o pode ficar vazio e deve ser azAZ09 e possuir 2-64 caracteres)" );
define ( "PALAVRA_ANTIGA_SENHA", "Sua senha antiga" );
define ( "PALAVRA_SENHA", "Senha" );
define ( "PALAVRA_PERFIL_FOTO", "Sua foto de perfil (do gravatar):" );
define ( "PALAVRA_REGISTRAR", "Registrar" );
define ( "PALAVRA_REGISTRAR_NOVA_CONTA", "Registrar nova conta" );
define ( "PALAVRA_REGISTRO_CAPTCHA", "Digite os caracteres" );
define ( "PALAVRA_REGISTRO_EMAIL", "Email do usu�rio (informe um email real, enviaremos um email de confirma��o com link de ativa��o)" );
define ( "PALAVRA_REGISTRO_SENHA", "Senha (min. 6 caracteres!)" );
define ( "PALAVRA_REGISTRO_SENHA_REPEAT", "Repita a senha" );
define ( "PALAVRA_REGISTRO_USUARIO", "Nome de usu�rio (comente letras e numeros, 2 a 64 caracteres)" );
define ( "PALAVRA_REMEMBER_ME", "Manter logado (por 4 semanas)" );
define ( "PALAVRA_REQUISITAR_SENHA_RESETAR", "Solicitar recupera��o de senha. Informe seu nome de usu�rio e enviaremos um email com instru��es:" );
define ( "PALAVRA_RESETAR_SENHA", "recuperar senha" );
define ( "PALAVRA_SUBMETER_NOVA_SENHA", "Enviar nova senha" );
define ( "PALAVRA_EMAIL", "Email" );
define ( "PALAVRA_VOCE_ESTA_LOGADO_COMO", "Voc� est� logado como " );

/**
 * RH
 */

define ( "PALAVRA_DADOS_PESSOAIS", "Dados Pessoais" );
define ( "PALAVRA_NOME", "Nome Completo" );
define ( "PALAVRA_DATA_NASCIMENTO", "Data de Nascimento" );
define ( "PALAVRA_LOCAL_NASCIMENTO", "Local de Nascimento" );
define ( "PALAVRA_NACIONALIDADE", "Nacionalidade" );
define ( "PALAVRA_CIVIL", "Estado Civil" );
define ( "PALAVRA_ESTADO_CIVIL_SOLTEIRO", "Solteiro" );
define ( "PALAVRA_ESTADO_CIVIL_CASADO", "Casado" );
define ( "PALAVRA_ESTADO_CIVIL_DESQUITADO", "Desquitado" );
define ( "PALAVRA_ESTADO_CIVIL_DIVORCIADO", "Divorciado" );
define ( "PALAVRA_ESTADO_CIVIL_UNIAO_ESTAVEL", "Uni�o Est�vel" );
define ( "PALAVRA_ESTADO_CIVIL_VIUVO", "Vi�vo" );
define ( "PALAVRA_NATURALIDADE", "Naturalizado" );
define ( "PALAVRA_TIPO_VISTO", "Tipo de Visto" );
define ( "PALAVRA_DATA_CHEGADA_BRASIL", "Data de Chegada no Brasil" );
define ( "PALAVRA_ENDERECO", "Endere�o - AV./Rua" );
define ( "PALAVRA_COMPLEMENTO", "Complemento" );
define ( "PALAVRA_BAIRRO", "Bairro" );
define ( "PALAVRA_CEP", "CEP" );
define ( "PALAVRA_MUNICIPIO", "Munic�pio - UF" );
define ( "PALAVRA_UF", "UF" );
define ( "PALAVRA_TELEFONE_RESIDENCIAL", "Telefone Residencial" );
define ( "PALAVRA_TELEFONE_CELULAR", "Telefone Celular" );
define ( "PALAVRA_TELEFONE_RECADOS", "Telefone p/ Recados" );
define ( "PALAVRA_FALAR_COM", "Falar com" );
define ( "PALAVRA_ESCOLARIDADE", "Escolaridade" );
define ( "PALAVRA_MODALIDADE", "Modalidade" );
define ( "PALAVRA_TRACO_FISICO", "Tra�os F�sicos" );
define ( "PALAVRA_COR_OLHO", "Cor dos olhos" );
define ( "PALAVRA_CABELO", "Cabelos" );
define ( "PALAVRA_PELE", "Pele" );
define ( "PALAVRA_TAMANHO_CALCA", "N� Cal�a" );
define ( "PALAVRA_TAMANHO_CAMISA", "N� Camisa" );
define ( "PALAVRA_TAMANHO_CALCADO", "N� Cal�ado" );
define ( "PALAVRA_TAMANHO_CAPACETE", "N� Capacete" );
define ( "PALAVRA_TAMANHO_LUVAS", "N� Luvas" );

/**
 * C.I.M
 */


/**
 * MATERIAIS
 */

    
    
    