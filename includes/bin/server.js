/**
 * Created by ricardo.diego on 25/11/2015.
 */
var express = require('express')
    , app = express()
    , server = require('http').createServer(app).listen(4555)
    , io = require('socket.io').listen(server)
    , bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var port = process.env.PORT || 8091;
/* Socket ir� aqui depois */



var countUser = 0;

var listNotificationCcm = {
    lista: [],
    setLista: function(notification){
        if(this.lista.length >= 10){
            this.lista.pop();               // Se passar do limite retirar o �ltimo da lista
        }
        this.lista.unshift(notification);   // Insere no come�o da lista

        console.log(" Lista ccm - " + this.lista.length);
    },
    getLista: function () {
        return this.lista
    }
};

var listNotificationEquipe = {
    lista: [],
    setLista: function(notification){
        if(this.lista.length >= 500){       // 500 para melhorar a chance de ter 10 itens para cada Uni_Equipe
            this.lista.pop();               // Se passar do limite retirar o �ltimo da lista
        }
        this.lista.unshift(notification);   // Insere no come�o da lista

        console.log(" Lista Equipe - " + this.lista.length);
    },
    getLista: function () {
        return this.lista
    }
};

app.listen(port);
console.log('conectado a porta ' + port);

io.on('connection', function(socket){
    console.log('New User connected');
    countUser ++;

    console.log('Users connected ' + countUser);

    socket.on('notificationCcm', function(msg){
        listNotificationCcm.setLista(msg);

        io.emit('notificationCcm', msg);
        io.emit('listNotificationCcm', listNotificationCcm.getLista() );
    });

    socket.on('listNotificationCcm', function(){
        io.emit('listNotificationCcm', listNotificationCcm.getLista() );
    });

    socket.on('functionCcm', function(msg){
        socket.broadcast.emit('functionCcm', msg);
    });


    socket.on('notificationEquipe', function(msg){
        listNotificationEquipe.setLista(msg);

        io.emit('notificationEquipe', msg);
        io.emit('listNotificationEquipe', listNotificationEquipe.getLista() );
    });

    socket.on('listNotificationEquipe', function(){
        io.emit('listNotificationEquipe', listNotificationEquipe.getLista() );
    });

    socket.on('functionEquipe', function(msg){
        socket.broadcast.emit('functionEquipe', msg);
    });


    socket.on('disconnect', function(){
        console.log('User disconnected');
        countUser --;

        console.log('Users connected ' + countUser);
    });
});