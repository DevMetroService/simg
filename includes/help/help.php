<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 15/07/2016
 * Time: 11:56
 */
?>

<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="modalHelpMatriculaPj" aria-labelledby="modalHelpMatriculaPj" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-question-circle fa-fw iconNavegador"></i> Matr�cula com adi��o de prefixos</h4>
            </div>
            <div class="modal-body">
                <div class="panel">
                    <h5>Matr�culas de funcion�rios externos devem ser precedidas com um prefixo espec�fico.</h5>
                </div>
                <div class="panel panel-warning">
                    <span class="btn-circle btn-info" style="font-size: 25px; margin: 25px; padding: 10px 23px;">PJ</span>
                    <label>Aos funcion�rios PJ<br/>Prefixo: 700 + matr�cula.</label>
                    <div style="padding: 10px !important;"><br />Ex: <br />A matr�cula 1 � 700 + 1 -> <strong>7001</strong><br />Assim como a matr�cula 10 � 700 + 10 -> <strong>70010</strong></div>
                </div>
                <div class="panel panel-danger">
                    <span class="btn-circle btn-primary" style="font-size: 25px; margin: 25px; padding: 10px;">MPE</span>
                    <label>Aos funcion�rios oriundos da MPE<br/>Prefixo: 800 + matr�cula.</label>
                    <div style="padding: 10px !important;"><br />Ex: <br />A matr�cula 1 � 800 + 1 -> <strong>8001</strong><br />Assim como a matr�cula 10 � 800 + 10 -> <strong>80010</strong></div>
                </div>
                <div class="panel panel-success">
                    <span class="btn-circle btn-success" style="font-size: 25px; margin: 25px; padding: 10px 15px;">CRJ</span>
                    <label>Aos funcion�rios oriundos da CRJ<br/>Prefixo: 900 + matr�cula.</label>
                    <div style="padding: 10px !important;"><br />Ex: <br />A matr�cula 1 � 900 + 1 -> <strong>9001</strong><br />Assim como a matr�cula 10 � 900 + 10 -> <strong>90010</strong></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="modalHelpNivel" aria-labelledby="modalHelpNivel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-question-circle fa-fw iconNavegador"></i> Tipos de N�vel</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-danger">
                    <span class="btn-circle btn-danger" style="font-size: 25px; margin: 25px; padding: 10px">A</span>
                    <label class="nivel-a">Atendimento Imediato</label>
                </div>
                <div class="panel panel-success">
                    <span class="btn-circle btn-success" style="font-size: 25px; margin: 25px; padding: 10px">B</span>
                    <label class="nivel-b">Atendimento em at� 6 horas</label>
                </div>
                <div class="panel panel-warning">
                    <span class="btn-circle btn-warning" style="font-size: 25px; margin: 25px; padding: 10px">C</span>
                    <label class="nivel-c">Atendimento em at� 24 horas</label>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="modalHelpProgramada" aria-labelledby="modalHelpProgramada" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-question-circle fa-fw iconNavegador"></i> Data Programada</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-success">
                    <span class="btn-circle btn-success" style="font-size: 25px; margin: 25px; padding: 10px"><i class="fa fa-clock-o fa-fw"></i></span>
                    <div style="font-size: 20px; text-indent:50px; margin: 25px; padding: 10px">
                        <p>A <strong>Data Programada</strong> � uma previs�o do in�cio do servi�o em campo.</p>
                        <p>Ela ser� repassada para o Metrofor com antecedencia para aprova��o, sendo assim n�o � permitido sua altera��o posteriormente.</p>
                        <p>A <em>Ordem de Servi�o</em> dever� ser gerada antes da <strong>Data Programada</strong> para o in�cio do servi�o,
                            pois ela se trata do momento em que a equipe de campo ir� se preparar para atender.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="ajudaModal" tabindex="-1" role="form" aria-labelledby="ajudaModal"
     data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="motivoLabel"><i class="fa fa-question-circle fa-fw iconNavegador"></i> Ajuda</h4>
            </div>

            <div class="modal-body">
                <div class="logo">
                    <img alt="Metro Service" src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo.png" width="250px" height="78px"/>
                </div>

                <div class="tabs">
                    <ul>
                        <li><a href="#docSIMG">Documenta��o SIMG</a></li>
                        <li><a href="#atualizacoes">�ltimas Atualiza��es</a></li>
                    </ul>

                    <div id="docSIMG">
                        <span style="color: red; font-size: 12px;">
                            *Clique no texto para abrir o arquivo desejado
                        </span>

                        <header>
                            <a href="<?php echo HOME_URI . '/includes/help/FluxogramaSIMG.pdf'; ?>" target="_blank">Fluxogramas do SIMG (Completo)</a>
                        </header>

                        <article>
                            <h4><a href="<?php echo HOME_URI . '/includes/help/FluxoGeral.pdf'; ?>" target="_blank">Fluxograma do SIMG:</a></h4>

                            <ul style='text-indent: 30px'>
                                <li><a href="<?php echo HOME_URI . '/includes/help/FluxoFalha.pdf'; ?>" target="_blank">Falha</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/FluxoSSPProgramada.pdf'; ?>" target="_blank">SSP Programada</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/FluxoPMP.pdf'; ?>" target="_blank">PMP</a></li>
                            </ul>
                        </article>

                        <header>
                            <a href="<?php echo HOME_URI . '/includes/help/ProcessosFalha.pdf'; ?>" target="_blank">Detalhamento dos Status nos Processos SIMG (Completo)</a><br />
                        </header>

                        <article>
                            <h4>Detalhamento dos Status caso:</h4>

                            <ul style='text-indent: 30px'>
                                <li><a href="<?php echo HOME_URI . '/includes/help/StatusFalhaNormal.pdf'; ?>" target="_blank">Falha padr�o</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/StatusDuasEquipes.pdf'; ?>" target="_blank">Servi�o para mais de uma Equipes</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/StatusPendente.pdf'; ?>" target="_blank">Servi�o Pendente</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/StatusTransferido.pdf'; ?>" target="_blank">Servi�o Transferido</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/StatusCorretivo.pdf'; ?>" target="_blank">Servi�o com SSP Corretiva</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/StatusProgramado.pdf'; ?>" target="_blank">Servi�o Programado</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/ProcessoDevolucao.pdf'; ?>" target="_blank">Processo de Devolu��o</a></li>
                            </ul>
                        </article>

                        <header>
                            <a href="<?php echo HOME_URI . '/includes/help/GrupoSistema.pdf'; ?>" target="_blank">Grupos Sistema / Sistema / SubSistema</a>
                        </header>

                        <header>
                            <a href="<?php echo HOME_URI . '/includes/help/StatusFormularios.pdf'; ?>" target="_blank">Descri��o dos Status</a><br />
                        </header>

                        <header>
                            Organograma Metro Service:<br />
                        </header>

                        <article>
                            <h4>Organograma:</h4>

                            <ul style='text-indent: 30px'>
                                <li><a href="<?php echo HOME_URI . '/includes/help/CONTRATO.pdf'; ?>" target="_blank">Contrato</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/ENGMANUTENCAOCONT.pdf'; ?>" target="_blank">Engenharia de Manuten��o - Controle</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/ENGMANUTENCAOTI.pdf'; ?>" target="_blank">Engenharia de Manuten��o</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/ENGMANUTENCAOBITE.pdf'; ?>" target="_blank">Bilhetagem e Telecom</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/EDIFICACAO.pdf'; ?>" target="_blank">Edifica��es</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/VPERMANENTE.pdf'; ?>" target="_blank">Via Permanente</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/ELETRICOS.pdf'; ?>" target="_blank">Sistemas El�tricos</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/FINANCEIRO.pdf'; ?>" target="_blank">Financeiro</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/SOBRAL.pdf'; ?>" target="_blank">Sobral</a></li>
                                <li><a href="<?php echo HOME_URI . '/includes/help/CARIRI.pdf'; ?>" target="_blank">Cariri</a></li>
                            </ul>
                        </article>

                    </div>

                    <div id="atualizacoes">
                        <?php
                        $notificar = new NovidadesModal();
                        ?>
                        <h3>Novidades de <?php echo $notificar->dataNotificacao; ?></h3>
                        <hr />
                        <br />

                        <?php
                        foreach($notificar->mensagens as $mensagem){
                            echo   "<h4>
                                        <span>&#10020;</span> {$mensagem}
                                    </h4>";
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-lg" aria-label="right align" data-dismiss="modal" title="Fechar">
                    <i class="fa fa-times fa-2x"></i>
                </button>
            </div>
        </div>
    </div>
</div>


<!-- Modals -->
<!-- Marca  -->
<div class="modal fade" id="metroServiceModal" tabindex="-1" role="form" aria-labelledby="metroServiceModal"
     data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="logo">
                    <img alt="Metro Service" src="<?php echo HOME_URI; ?>/views/_images/metroservice_logo.png" width="250px" height="78px"/>
                </div>

                <h4>&copy; Copyright 2014. Consorcio Metro Service. Todos os direitos reservados.</h4>

                <strong>CNPJ</strong><br />
                20.063.949/0001-48<br />

                <strong>Nome</strong><br />
                Consorcio Metro Service<br />

                <strong>Endere�o</strong><br />
                R Guilherme Vieira Da Costa, 255, Jardim Cearense, Maraponga, Fortaleza, CE, CEP 60712-075, Brasil<br />

                <strong>Telefone</strong><br />
                (85) 9650-1776<br /><br />

                <strong>Atividade Econ�mica Principal</strong><br />

                Manuten��o e Repara��o de M�quinas, Aparelhos e Materiais El�tricos das vias do Metro<br /><br />

                <strong>Atividade Econ�mica Secund�ria</strong><br />
                <ul>
                    <li>Servi�os de engenharia</li>
                    <li>Instala��o e manuten��o el�trica</li>
                </ul>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-lg" aria-label="right align" data-dismiss="modal" title="Fechar">
                    <i class="fa fa-times fa-2x"></i>
                </button>
            </div>
        </div>
    </div>
</div>