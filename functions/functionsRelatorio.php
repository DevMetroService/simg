<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 14/11/2016
 * Time: 09:17
 */

function returnWhere($post)
{
    $where = [];

    if ($post['grafico'] != "LancadasAtendidas") {
        $dataPartir = inverteData($post['dataPartirDatAber']);
        $dataRetroceder = inverteData($post['dataRetrocederDatAber']);
        $dataPartirEnc = inverteData($post['dataPartirDatEnc']);
        $dataRetrocederEnc = inverteData($post['dataRetrocederDatEnc']);

        if ($dataPartir != "" || $dataRetroceder != "") {
            if ($post['form'] == "ssp") {
                $dat_ab = "data_programada";
            } else if ($post['form'] == "osm") {
                $dat_ab = "osm.data_abertura";
            } else if ($post['form'] == "osp") {
                $dat_ab = "osp.data_abertura";
            } else if ($post['form'] == "osmp") {
                $dat_ab = "osmp.data_abertura";
            } else if ($post['form'] == "todos") {
                $dat_ab = "o.data_abertura";
            } else {
                $dat_ab = "data_abertura";
            }

            if ($dataPartir == $dataRetroceder) {
                $dataPartir = $dataPartir . " 00:00:00";
                $dataRetroceder = $dataRetroceder . " 23:59:59";


                $where[] = "{$dat_ab} >= '{$dataPartir}'";
                $where[] = "{$dat_ab} <= '{$dataRetroceder}'";

            } else {
                if ($dataPartir) {
                    $dataPartir = $dataPartir . " 00:00:00";

                    $where[] = "{$dat_ab} >= '{$dataPartir}'";
                }

                if ($dataRetroceder) {
                    $dataRetroceder = $dataRetroceder . " 00:00:00";

                    $where[] = "{$dat_ab} < '{$dataRetroceder}'";
                }
            }
        }


        if ($dataPartirEnc != "" || $dataRetrocederEnc != "") {
            if ($post['form'] == "osm") {
                $dat_ab = "osms.data_status";
            } else if ($post['form'] == "osp") {
                $dat_ab = "osps.data_status";
            } else if ($post['form'] == "osmp") {
                $dat_ab = "osmps.data_status";
            } else if ($post['form'] == "todos") {
                $dat_ab = "ostatus.data_status";
            } else {
                $dat_ab = "data_status";
            }

            if ($dataPartirEnc == $dataRetrocederEnc) {
                $dataPartirEnc = $dataPartirEnc . " 00:00:00";
                $dataRetrocederEnc = $dataRetrocederEnc . " 23:59:59";


                $where[] = "{$dat_ab} >= '{$dataPartirEnc}'";
                $where[] = "{$dat_ab} <= '{$dataRetrocederEnc}'";

            } else {
                if ($dataPartirEnc) {
                    $dataPartirEnc = $dataPartirEnc . " 00:00:00";

                    $where[] = "{$dat_ab} >= '{$dataPartirEnc}'";
                }

                if ($dataRetrocederEnc) {
                    $dataRetrocederEnc = $dataRetrocederEnc . " 00:00:00";

                    $where[] = "{$dat_ab} < '{$dataRetrocederEnc}'";
                }
            }
        }
    }

    switch ($post['form']) {
        case "saf":
            if ($post['solicitanteOrigem']) {
                $where[] = "tipo_orisaf = {$post['solicitanteOrigem']}";
            }
            if ($post['avaria']) {
                $where[] = "cod_avaria = {$post['avaria']}";
            }
            if ($post['nivel']) {
                $where[] = "nivel = '{$post['nivel']}'";
            }

            break;
        case "ssm":
            if ($post['nivel']) {
                $where[] = "nivel = '{$post['nivel']}'";
            }
            if ($post['tipoIntervencao']) {
                $where[] = "cod_tipo_intervencao = '{$post['tipoIntervencao']}'";
            }
            if ($post['servico']) {
                $where[] = "cod_servico = '{$post['servico']}'";
            }
            if ($post['unidade']) {
                $where[] = "cod_unidade = '{$post['unidade']}'";
            }
            if ($post['equipe']) {
                $where[] = "sigla_equipe = '{$post['equipe']}'";
            }
            break;
        case "osm":
            if ($post['nivel']) {
                $where[] = "nivel = '{$post['nivel']}'";
            }
            if ($post['servico']) {
                $where[] = "os.cod_servico = '{$post['servico']}'";
            }
            if ($post['unidade']) {
                $where[] = "cod_unidade = '{$post['unidade']}'";
            }
            if ($post['equipe']) {
                $where[] = "equipe.sigla = '{$post['equipe']}'";
            }
            break;
        case "ssp":
            if ($post['origemProgRel']) {
                $where[] = "tipo_orissp = {$post['origemProgRel']}";
            }
            if ($post['tipoIntervencao']) {
                $where[] = "cod_tipo_intervencao = '{$post['tipoIntervencao']}'";
            }
            if ($post['servico']) {
                $where[] = "cod_servico = '{$post['servico']}'";
            }
            if ($post['unidade']) {
                $where[] = "cod_unidade = '{$post['unidade']}'";
            }
            if ($post['equipe']) {
                $where[] = "sigla_equipe = '{$post['equipe']}'";
            }
            break;
        case "osp":
            if ($post['origemProgRel']) {
                $where[] = "tipo_orissp = {$post['solicitanteOrigem']}";
            }
            if ($post['servico']) {
                $where[] = "os.cod_servico = '{$post['servico']}'";
            }
            if ($post['unidade']) {
                $where[] = "cod_unidade = '{$post['unidade']}'";
            }
            if ($post['equipe']) {
                $where[] = "equipe.sigla = '{$post['equipe']}'";
            }
            break;
    }

    if ($post['form'] == "osm" || $post['form'] == "osp") {
        //###########         Local
        if ($post['linha']) {
            $where[] = "cod_linha_atuado = {$post['linha']}";
        }

        if ($post['trecho']) {
            $where[] = "trecho_atuado = {$post['trecho']}";
        }

        if ($post['pn']) {
            $where[] = "cod_ponto_notavel_atuado = {$post['pn']}";
        }

        //############         Dados Gerais
        if ($post['grupoSistema']) {
            $where[] = "grupo_atuado = {$post['grupoSistema']}";
        }

        if ($post['sistema']) {
            $where[] = "sistema_atuado = {$post['sistema']}";
        }

        if ($post['subSistema']) {
            $where[] = "subsistema_atuado = {$post['subSistema']}";
        }

        //############         Materiais M�quinas e Equipamentos
        if ($post['maquinasEquipamentos']) {
            $where[] = "cod_material = {$post['maquinasEquipamentos']}";
        }

        if ($post['materiais']) {
            $where[] = "cod_material = {$post['materiais']}";
        }
    } else {
        //###########         Local
        if ($post['linha']) {
            $where[] = "cod_linha = {$post['linha']}";
        }

        if ($post['trecho']) {
            $where[] = "cod_trecho = {$post['trecho']}";
        }

        if ($post['pn']) {
            $where[] = "cod_ponto_notavel = {$post['pn']}";
        }

        //############         Dados Gerais
        if ($post['grupoSistema'] && (empty($post['form']) || $post['form'] != "osmp")) {
            if ($post['grupoSistema'] && $post['form'] != "todos") {
                $where[] = "cod_grupo = {$post['grupoSistema']}";
            }
        }

        if ($post['sistema']) {
            $where[] = "cod_sistema = {$post['sistema']}";
        }

        if ($post['subSistema']) {
            $where[] = "cod_subsistema = {$post['subSistema']}";
        }
        //############         Materiais M�quinas e Equipamentos
        if ($post['maquinasEquipamentos']) {
            $where[] = "cod_material = {$post['maquinasEquipamentos']}";
        }

        if ($post['materiais']) {
            $where[] = "cod_material = {$post['materiais']}";
        }

    }

    //Material rodante
    if ($post['veiculo']) {
        $where[] = "cod_veiculo = {$post['veiculo']}";
    }
    if ($post['carro']) {
        $where[] = "cod_carro = {$post['carro']}";
    }
    if ($post['prefixo']) {
        $where[] = "cod_prefixo = {$post['prefixo']}";
    }
    if ($post['odometroPartir']) {
        $where[] = "odometro >= {$post['odometroPartir']}";
    }
    if ($post['odometroAte']) {
        $where[] = "odometro <= {$post['odometroAte']}";
    }

    $sqlWhere = "";
    if (count($where)) {
        $sqlWhere = implode(' AND ', $where);
    }

    return $sqlWhere;
}

function returnCor($status)
{
    switch ($status) {
        case "Aberta":// saf // ssm // ssp
            return "#B0BEC5";
        case "Agendada":// ssm
            return "#ffff33";
        case "Autorizada":// saf // ssm // ssp
            return "#FFEB3B";
        case "Cancelada":// saf // ssm // ssp
            return "#d50000";
        case "Devolvida":// saf // ssm
            return "#B0BEC5";
        case "Duplicado":// os
            return "#FF5722";
        case "Encaminhado":// ssm
            return "#9C27B0";
        case "Encerrada":// saf // ssm // ssp // os
            return "#4CAF50";
        case "Execu��o":// ssp // os
            return "#03A9F4";
        case "N�o Configura falha":// os
            return "#3F51B5";
        case "N�o Executado":// os
            return "#d50000";
        case "Pendente":// ssm // ssp
            return "#FF5722";
        case "Programada":// saf // ssm
            return "#263238";
        case "Programado":// ssp
            return "#263238";
        case "Transferida":// ssm
            return "#e0e0eb";
    }
}

function inverteData($data = null)
{
    $nova_data = null;                                            // Configura uma vari�vel para receber a nova data

    if ($data) {                                                // Se a data for enviada

        $data = preg_split('/\-|\/|\s|:/', $data);    // Explode a data por -, /, : ou espa�o
        $data = array_map('trim', $data);                // Remove os espa�os do come�o e do fim dos valores
        $nova_data .= chk_array($data, 2) . '-';            // Cria a data invertida
        $nova_data .= chk_array($data, 1) . '-';            // Cria a data invertida
        $nova_data .= chk_array($data, 0);                    // Cria a data invertida

        if (chk_array($data, 3)) {
            $nova_data .= ' ' . chk_array($data, 3);        // Configura a hora
        }

        if (chk_array($data, 4)) {
            $nova_data .= ':' . chk_array($data, 4);        // Configura os minutos
        }

        if (chk_array($data, 5)) {
            $nova_data .= ':' . chk_array($data, 5);        // Configura os segundos
        }
    }
    return $nova_data;                                            // Retorna a nova data
}

function mesAno($mes, $ano)
{
    $nomeMes = [
        1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Mar�o', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho',
        7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro'
    ];


    if ($mes == 12) {
        $data['mes']['Apartir'] = $mes;
        $data['mes']['Ate'] = 1;

        $data['ano']['Apartir'] = $ano;
        $data['ano']['Ate'] = $ano + 1;
    } else {
        $data['mes']['Apartir'] = $mes;
        $data['mes']['Ate'] = $mes + 1;

        $data['ano']['Apartir'] = $ano;
        $data['ano']['Ate'] = $ano;
    }
    $return['mesNome'] = $nomeMes[$mes];

    $return['dataApartir'] = caracter2($data['ano']['Apartir']) . "-" . caracter2($data['mes']['Apartir']) . "-01 00:00:00";
    $return['dataAte'] = caracter2($data['ano']['Ate']) . "-" . caracter2($data['mes']['Ate']) . "-01 00:00:00";

    return $return;
}

function caracter2($num)
{
    if ($num < 10)
        return "0" . (int)$num;
    else
        return $num;
}

function mesAnoTresMeses($mes, $ano)
{
    $nomeMes = [
        1 => 'Janeiro', 2 => 'Fevereiro', 3 => 'Mar�o', 4 => 'Abril', 5 => 'Maio', 6 => 'Junho',
        7 => 'Julho', 8 => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro', 12 => 'Dezembro'
    ];


    if ($mes == 12) {
        $data['mes']['terceiro']['Apartir'] = $mes;
        $data['mes']['terceiro']['Ate'] = 1;

        $data['ano']['terceiro']['Apartir'] = $ano;
        $data['ano']['terceiro']['Ate'] = $ano + 1;
    } else {
        $data['mes']['terceiro']['Apartir'] = $mes;
        $data['mes']['terceiro']['Ate'] = $mes + 1;

        $data['ano']['terceiro']['Apartir'] = $ano;
        $data['ano']['terceiro']['Ate'] = $ano;
    }
    $return['mesTres'] = $nomeMes[$mes];

    if ($mes == 1) {
        $data['mes']['segundo']['Apartir'] = 12;
        $data['mes']['segundo']['Ate'] = $mes;

        $data['ano']['segundo']['Apartir'] = $ano - 1;
        $data['ano']['segundo']['Ate'] = $ano;


    } else {
        $data['mes']['segundo']['Apartir'] = $mes - 1;
        $data['mes']['segundo']['Ate'] = $mes;

        $data['ano']['segundo']['Apartir'] = $ano;
        $data['ano']['segundo']['Ate'] = $ano;

        $return['mesDois'] = $nomeMes[($mes - 1)];
    }
    $return['mesDois'] = $nomeMes[$data['mes']['segundo']['Apartir']];

    if ($data['mes']['segundo']['Apartir'] == 1) {
        $data['mes']['primeiro']['Apartir'] = 12;
        $data['mes']['primeiro']['Ate'] = $data['mes']['segundo']['Apartir'];

        $data['ano']['primeiro']['Apartir'] = $data['ano']['segundo']['Apartir'] - 1;
        $data['ano']['primeiro']['Ate'] = $data['ano']['segundo']['Apartir'];
    } else {
        $data['mes']['primeiro']['Apartir'] = $data['mes']['segundo']['Apartir'] - 1;
        $data['mes']['primeiro']['Ate'] = $data['mes']['segundo']['Apartir'];

        $data['ano']['primeiro']['Apartir'] = $data['ano']['segundo']['Apartir'];
        $data['ano']['primeiro']['Ate'] = $data['ano']['segundo']['Apartir'];
    }
    $return['mesUm'] = $nomeMes[$data['mes']['primeiro']['Apartir']];


    $return['dataUmApartir'] = "{$data['ano']['primeiro']['Apartir']}-{$data['mes']['primeiro']['Apartir']}-01 00:00:00";
    $return['dataUmAte'] = "{$data['ano']['primeiro']['Ate']}-{$data['mes']['primeiro']['Ate']}-01 00:00:00";

    $return['dataDoisApartir'] = "{$data['ano']['segundo']['Apartir']}-{$data['mes']['segundo']['Apartir']}-01 00:00:00";
    $return['dataDoisAte'] = "{$data['ano']['segundo']['Ate']}-{$data['mes']['segundo']['Ate']}-01 00:00:00";

    $return['dataTresApartir'] = "{$data['ano']['terceiro']['Apartir']}-{$data['mes']['terceiro']['Apartir']}-01 00:00:00";
    $return['dataTresAte'] = "{$data['ano']['terceiro']['Ate']}-{$data['mes']['terceiro']['Ate']}-01 00:00:00";

    return $return;
}

function returnSqlPmp($grupo)
{
    switch ($grupo) {
        case 21:
            //Edifica��es
            return "SELECT
                        c_ed.cod_cronograma_pmp AS cod_cronograma_grupo,
                        nome_local,
                        cod_linha,
                        mes,
                        ano,
                        c.quinzena,
                        homem_hora,
                        nome_periodicidade,
                        nome_servico_pmp,
                        cod_sistema,
                        cod_subsistema,
                        nome_status,
                        'Edifica��es' AS nome_grupo
                        
                        FROM cronograma c
                        
                        JOIN cronograma_pmp c_ed USING (cod_cronograma)
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_edificacao USING (cod_pmp)
                        JOIN local l USING (cod_local)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN servico_pmp USING (cod_servico_pmp)";
        case 20:
            //Rede A�rea
            return "SELECT
                        c_ra.cod_cronograma_pmp AS cod_cronograma_grupo,
                        nome_local,
                        l.cod_linha,
                        mes,
                        ano,
                        c.quinzena,
                        homem_hora,
                        nome_periodicidade,
                        nome_servico_pmp,
                        cod_sistema,
                        cod_subsistema,
                        nome_status,
                        'Rede A�rea' AS nome_grupo
                        
                        FROM cronograma c
                        
                        JOIN cronograma_pmp c_ra USING (cod_cronograma)
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_rede_aerea USING (cod_pmp)
                        
                        JOIN local USING (cod_local)
                        JOIN linha l USING (cod_linha)
                        
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN servico_pmp USING (cod_servico_pmp)";
        case 24:
            //Via Permanente
            return "SELECT
                        c_vp.cod_cronograma_pmp AS cod_cronograma_grupo,
			            l.nome_estacao || ' - ' || f.nome_estacao AS nome_local,
                        l.cod_linha,
                        mes,
                        ano,
                        c.quinzena,
                        homem_hora,
                        nome_periodicidade,
                        nome_servico_pmp,
                        cod_sistema,
                        cod_subsistema,
                        nome_status,
                        'Via Permanente' AS nome_grupo
                        
                        FROM cronograma c
                        
                        JOIN cronograma_pmp c_vp USING (cod_cronograma)
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_via_permanente USING (cod_pmp)
                        
                        JOIN estacao l ON l.cod_estacao = cod_estacao_inicial
                        JOIN estacao f ON f.cod_estacao = cod_estacao_final
                        
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN servico_pmp USING (cod_servico_pmp)";
        case 25:
            //Subesta��o
            return "SELECT
                        c_su.cod_cronograma_pmp AS cod_cronograma_grupo,
                        nome_local,
                        cod_linha,
                        mes,
                        ano,
                        c.quinzena,
                        homem_hora,
                        nome_periodicidade,
                        nome_servico_pmp,
                        cod_sistema,
                        cod_subsistema,
                        nome_status,
                        'Subesta��o' AS nome_grupo
                        
                        FROM cronograma c
                        
                        JOIN cronograma_pmp c_su USING (cod_cronograma)
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_subestacao USING (cod_pmp)
                        JOIN local l USING (cod_local)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN sub_sistema USING (cod_sub_sistema)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN servico_pmp USING (cod_servico_pmp)";
    }
}