<?php

/**
 * Fun��es globais para a aplica��o S.I.M.G
 *
 * @package SimgMVC
 * @since 0.1
 *
 * Este c�digo � confidencial.
 * A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 *
 * Verifica chaves de arrays
 * 
 * Verifica se a chave existe no arrat e se ela tem algum valor.
 * Obs.: Essa fun��o est� no escopo global, pois, ela ser� muito necess�ria
 * 	
 * @param array $array O array
 * @param string $key A chave do array
 * @return string|null  O valor da chave ou nulo
 * 
 */

function chk_array($array, $key) {

	if (isset ( $array [$key] ) && ! empty ( $array [$key] )) { 					# Verifica se a chave existe no array
		return $array [$key];														# Retorna o valor da Chave
	}

	return null;																	# Retorna null por default
}

/**
 * Fun��o para carregar automaticamente todas as classes padr�o
 *
 * As classes est�o na pastas classes/..
 *
 * O nome do arquivo dever� ser class-NomeDaClasse.php
 *
 * @param $class_name
 *
// */
//function __autoload($class_name) {
//	$file = ABSPATH . '/classes/class-' . $class_name . '.php';
//
//	if (! file_exists ( $file )) {
//		require_once ABSPATH . '/includes/404.php';
//		return;
//	}
//
//	require_once $file;																# Inclui o arquivo da Classe
//}

spl_autoload_register(function ($class) {
    require_once ABSPATH."/classes/class-{$class}.php";
});

?>