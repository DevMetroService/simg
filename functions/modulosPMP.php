<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 27/09/2016
 * Time: 10:57
 */

function moduloIdentificacaoGeral($pmp = null, $sistema, $subsistema, $servico_pmp, $periodicidade)
{
    $modulo = '     <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><label>Identifica��o Geral</label></div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Sistema</label>
                                        <select name="sistema" class="form-control" required>
                                            <option selected disabled>Selecione o Sistema</option>';

    if($sistema) {
        foreach ($sistema as $dados => $value) {
            if (!empty($pmp['sistema']) && $pmp['sistema'] == $value['cod_sistema'])
                $modulo .= '<option selected value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_sistema"] . '">' . $value["nome_sistema"] . '</option>';
        }
    }

    $modulo .= '                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>SubSistema</label>
                                        <select name="subSistema" class="form-control" required>
                                            <option selected disabled>Selecione o Subsistema</option>';

    if (!empty($pmp['sistema']) && $subsistema) {
        foreach ($subsistema as $dados => $value) {
            if (!empty($pmp['subSistema']) && $pmp['subSistema'] == $value['cod_subsistema'])
                $modulo .= '<option selected value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>';
        }
    }
    
    $modulo .= '                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Servi�o</label>
                                        <select name="servicoPmp" class="form-control" required>
                                            <option selected disabled>Selecione um Servi�o</option>';

    if (!empty($pmp['subSistema']) && $servico_pmp) {
        foreach ($servico_pmp as $dados => $value) {
            if (!empty($pmp['servicoPmp']) && $pmp['servicoPmp'] == $value['cod_servico_pmp'])
                $modulo .= '<option selected value="' . $value["cod_servico_pmp"] . '">' . $value["nome_servico_pmp"] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_servico_pmp"] . '">' . $value["nome_servico_pmp"] . '</option>';
        }
    }
    
    $modulo .= '                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Periodicidade</label>
                                        <select name="periodicidade" class="form-control" required>
                                            <option selected disabled>Selecione uma Periodicidade</option>';
    if (!empty($pmp['servicoPmp']) && $periodicidade) {
        foreach ($periodicidade as $dados => $value) {
            if (!empty($pmp['periodicidade']) && $pmp['periodicidade'] == $value['cod_tipo_periodicidade'])
                $modulo .= '<option selected value="' . $value["cod_tipo_periodicidade"] . '">' . $value["nome_periodicidade"] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_tipo_periodicidade"] . '">' . $value["nome_periodicidade"] . '</option>';
        }
    }

    $modulo .= '                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Procedimento</label>';

    if(!empty($pmp['procedimento']))
        $modulo .= '<input type="text" value="'.$pmp['procedimento'].'" name="procedimento" class="form-control" readonly>';
    else
        $modulo .= '<input type="text" name="procedimento" class="form-control" readonly>';

    $modulo .= '                    </div>
                                    <div class="col-md-4">
                                        <label>Quinzena (Inicial)</label>';

    if(!empty($pmp['quinzena']))
        $modulo .= '<input type="text" value="'.$pmp['quinzena'].'" name="quinzena" class="form-control number" required>';
    else
        $modulo .= '<input type="text" name="quinzena" class="form-control number" required>';

    $modulo .= '                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
    
    return $modulo;
}

function moduloRecursosHH($pmp = null, $col = 6)
{
    $modulo = '
    <div class="col-lg-'.$col.'">
        <div class="panel panel-default">
            <div class="panel-heading"><label>Recursos HH</label></div>
            
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <label>Turno</label>
                        <select type="text" name="turno" class="form-control" required>
                            <option selected disabled>Selecione o Turno</option>';

    if(!empty($pmp['turno'])) {
        if ($pmp['turno'] == "N")
            $modulo .= '<option value="D">Diurno</option><option selected value="N">Noturno</option>';
        else
            $modulo .= '<option selected value="D">Diurno</option><option value="N">Noturno</option>';
    }else
        $modulo .= '<option value="D">Diurno</option><option value="N">Noturno</option>';

    $modulo .= '       </select>
                    </div>
                    <div class="col-md-3">
                        <label>M�o de Obra</label>';

    if(!empty($pmp['maoObra']))
        $modulo .= '<input type="text" value="'.$pmp['maoObra'].'" name="maoObra" class="form-control number" required>';
    else
        $modulo .= '<input type="text" name="maoObra" class="form-control number" required>';

    $modulo .= '    </div>
                    <div class="col-md-3">
                        <label>Hr. �teis</label>';

    if(!empty($pmp['horasUteis']))
        $modulo .= '<input type="text" value="'.$pmp['horasUteis'].'" name="horasUteis" class="form-control number" required>';
    else
        $modulo .= '<input type="text" name="horasUteis" class="form-control number" required>';

    $modulo .= '    </div>
                    <div class="col-md-3">
                        <label>Homem/Hora</label>';

    if(!empty($pmp['homemHora']))
        $modulo .= '<input type="text" value="'.$pmp['homemHora'].'" name="homemHora" class="form-control" readonly>';
    else
        $modulo .= '<input type="text" name="homemHora" class="form-control" readonly>';

    $modulo .= '    </div>
                </div>
            </div>
        </div>
    </div>';
    
    return $modulo;
}

function moduloIdentificacaoGeralPesquisa($refill = null, $sistema, $subsistema, $servicoPmp)
{
    $modulo = ' <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Identifica��o</label></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <label>C�digo PMP</label>';

    if(!empty($refill['codPmp']))
        $modulo .= '<input name="codPmp" value="'.$refill["codPmp"].'" class="form-control number" />';
    else
        $modulo .= '<input name="codPmp" class="form-control number" />';

    $modulo .= '             </div>
                            <div class="col-md-6">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <option value="">Todos</option>';

    if(!empty($refill['status']) && $refill['status'] == "A"){
        $modulo .= '<option value="A" selected >Ativo</option>
                    <option value="D">Depreciado</option>
                    <option value="E">Aberto</option>';

    }else if(!empty($refill['status']) && $refill['status'] == "E"){
        $modulo .= '<option value="A">Ativo</option>
                    <option value="D">Depreciado</option>
                    <option value="E" selected >Aberto</option>';

    }else if(!empty($refill['status']) && $refill['status'] == "D"){
        $modulo .= '<option value="A">Ativo</option>
                    <option value="D" selected >Depreciado</option>
                    <option value="E">Aberto</option>';

    }else{
        $modulo .= '<option value="A">Ativo</option>
                    <option value="D">Depreciado</option>
                    <option value="E">Aberto</option>';
    }

    $modulo .= '                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Informa��es Gerais</label></div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <label>Sistema</label>
                                    <select class="form-control" name="sistema">
                                        <option selected value="">Todos</option>';

    if (!empty($sistema)) {
        foreach ($sistema as $dados => $value) {
            if (!empty($refill['sistema']) && $refill['sistema'] == $value["cod_sistema"])
                $modulo .= '<option value="' . $value["cod_sistema"] . '" selected>' . $value['nome_sistema'] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_sistema"] . '">' . $value['nome_sistema'] . '</option>';
        }
    }

    $modulo .= '                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>SubSistema</label>
                                    <select class="form-control" name="subSistema">
                                        <option selected value="">Todos</option>';

    if (!empty($refill['sistema']) && !empty($subsistema)) {
        foreach ($subsistema as $dados => $value) {
            if (!empty($refill['subSistema']) && $refill['subSistema'] == $value["cod_subsistema"])
                $modulo .= '<option value="' . $value["cod_subsistema"] . '" selected>' . $value["nome_subsistema"] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_subsistema"] . '">' . $value["nome_subsistema"] . '</option>';
        }
    }

    $modulo .= '                     </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Servi�o</label>
                                    <select class="form-control" name="servicoPmp">
                                        <option selected value="">Todos</option>';

    if (!empty($refill['subSistema']) && !empty($servicoPmp)){
        foreach ($servicoPmp as $dados => $value) {
            if (!empty($refill['servicoPmp']) && $refill['servicoPmp'] == $value["cod_servico_pmp"])
                $modulo .= '<option value="' . $value["cod_servico_pmp"] . '" selected>' . $value["nome_servico_pmp"] . '</option>';
            else
                $modulo .= '<option value="' . $value["cod_servico_pmp"] . '">' . $value["nome_servico_pmp"] . '</option>';
        }
    }

    $modulo .= '                     </select>
                                </div>
                            </div>
                        </div>
                    </div>';

    return $modulo;
}

function moduloPeriodicidadePesquisa($refill = null, $col = 6, $selectPerio){
    $modulo = ' <div class="col-md-'.$col.'">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Per�odo</label></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <label>Periodicidade</label>
                                <select name="periodicidade" class="form-control">
                                    <option selected value="">Todos</option>';

    if (!empty($selectPerio)){
        foreach ($selectPerio as $dados => $value) {
            if (!empty($refill['periodicidade']) && $refill['periodicidade'] == $dados)
                $modulo .= "<option value='{$dados}' selected >{$value}</option>";
            else
                $modulo .= "<option value='{$dados}' >{$value}</option>";
        }
    }

    $modulo .= '                 </select>
                            </div>
                            <div class="col-md-6">
                                <label>Quinzena Inicial</label>';

    if(!empty($refill['quinzena'])){
        $modulo .= '<input name="quinzena" value="'.$refill['quinzena'].'" class="form-control number" type="text"/>';
    }else{
        $modulo .= '<input name="quinzena" class="form-control number" type="text"/>';
    }

    $modulo .= '            </div>
                         </div>
                    </div>
                </div>';

    return $modulo;
}

function moduloRecursosHHPesquisa($refill = null, $col = 6){
    $modulo = ' <div class="col-md-'.$col.'">
                    <div class="panel panel-default">
                        <div class="panel-heading"><label>Recursos HH</label></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <label>Homem Hora</label>';

    if(!empty($refill['homemHora']))
        $modulo .= '<input name="homemHora" value="'.$refill['homemHora'].'" class="form-control number" type="text"/>';
    else
        $modulo .= '<input name="homemHora" class="form-control number" type="text"/>';

    $modulo .= '             </div>
                            <div class="col-md-6">
                                <label>M�o de Obra</label>';

    if(!empty($refill['maoObra']))
        $modulo .= '<input name="maoObra" value="'.$refill['maoObra'].'" class="form-control number" type="text"/>';
    else
        $modulo .= '<input name="maoObra" class="form-control number" type="text"/>';

    $modulo .= '             </div>
                        </div>
                    </div>
                </div>';

    return $modulo;
}

//================Bot�es PMP================//
function btnSalvarPmp()
{
    $btn = '<div class="btn-group">';
    $btn .= '<button type="button" aria-label="right align" class="btn btn-default btn-lg salvarListaPmp" title="Salvar">
                <i class="fa fa-floppy-o fa-2x"></i>
             </button>';
    $btn .= '</div>';

    return $btn;
}


function btnImprimirPmp()
{
    $btn = '<div class="btn-group">';
    $btn .= '<button type="button" aria-label="right align" id="" class="btn btn-default btn-lg" title="Imprimir">
                <i class="fa fa-print fa-2x"></i>
             </button>';
    $btn .= '</div>';

    return $btn;
}