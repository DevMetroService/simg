<?php
/**
 * Created by PhpStorm.
 * User: josue.marques
 * Date: 26/02/2016
 * Time: 12:01
 */

function modalDevolverCancelar($dados, $form)
{
    $functionController = "";
    switch ($form) {
        case "Saf":
            $functionController = "devolverCancelar";
            if (empty($dados) || $dados['nome_status'] == "Encerrada" || $dados['nome_status'] == "Cancelada" || $dados['nome_status'] == "Autorizada")
                return;
            else
                $codigo = $dados['cod_saf'];
            break;

        case "NegarSaf":
            $functionController = "negar";
            $form = "Saf";
            $codigo = $dados['cod_saf'];
            break;

        case "Ssm":
            $functionController = "devolverCancelar";
            $codigo = $dados['cod_ssm'];
            break;

        case "Ssp":
            $functionController = "cancelar";
            $codigo = $dados['cod_ssp'];
            break;

        case "Osm":
            $functionController = "cancelar";
            $codigo = $dados['cod_osm'];
            break;

        case "Osp":
            $functionController = "cancelar";
            $codigo = $dados['cod_osp'];
            break;

        case "Osmp":
            $functionController = "cancelar";
            $codigo = $dados['cod_osmp'];
            break;
    }

    echo '<!-- Modals -->
    <div class="modal fade" id="motivoModal" tabindex="-1" role="form" aria-labelledby="motivoLabel" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" id="motivoLabel">Motivo da A��o em <span id="formCancelamento">'.$form.'</span></h4>
                </div>

                <form action="' . HOME_URI . '/cadastroGeral/' . $functionController . $form . '" id="form_modal" method="post">
                    <div class="modal-body">';
                    if ($form == "Osm" || $form == "Osp" || $form == "Osmp") {
                        echo '<div class="row">
                            <div class="col-md-4">
                                <label>Motivo</label>
                                <select class="form-control free" required="required" name="motivo">
                                <option value="">Selecione</option>
                                <option value="24">Duplicidade</option>
                                <option value="23">N�o executado</option>';
                        if ($form == "Osm") {
                            echo '<option value="25">N�o Configura Falha</option>';
                        }

                        echo '</select>
                        </div>
                        <div id="campoCodOsDuplicado" class="col-md-3 col-md-offset-5" style="display:none">
                        <label>Cod. Os Duplicada</label>
                        <input type="text" name="codOsDuplicado" value="" class="form-control">
                        </div></div>';
                    }
                    if ($form == "Ssm") {
                        if ($dados['nome_status'] != "Pendente" && $dados['nome_status'] != "Reprovada") {
                            echo '<div class="row" id="motivoCancelamentoSSM">
                                    <div class="col-md-4">
                                        <label>Motivo</label>
                                        <select class="form-control free" required="required" name="motivoCancelamento">
                                            <option value="">Selecione</option>
                                            <option value="1">Cancelada</option>
                                            <option value="2">Programa��o</option>
                                            <option value="3">Servi�o</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" id="numeroSspProgramada" style="display: none" >
                                        <label>N�mero Ssp</label>
                                        <input type="text" name="nSsp" value="" class="form-control free number">
                                    </div>
                                </div>';
                            echo '<div class="row" id="motivoDevolucaoSSM">
                                    <div class="col-md-4">
                                        <label>Motivo</label>
                                        <select class="form-control free" required="required" name="motivoDevolucao">
                                            <option value="">Selecione</option>
                                            <option value="1">Duplicada</option>
                                            <option value="2">N�o Configura Falha</option>
                                            <option value="3">Informa��es Incompletas</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" id="numeroSafDuplicada" style="display: none" >
                                        <label>N�mero Saf</label>
                                        <input type="text" name="nSaf" value="" class="form-control free number">
                                    </div>
                                </div>';
                        } else {
                            echo '<div class="row">
                                    <div class="col-md-4">
                                        <h3>Encerrar Ssm</h3>
                                    </div>
                                </div>';
                        }
                    }
                    echo '<div class="row">
                                                <div class="col-md-12">';

                    if ($form == "Osm" || $form == "Osp" || $form == "Osmp") {
                        echo '<label>Observa��es:</label>';
                    } else {
                        echo '<label>Informe o motivo da a��o:</label>';
                    }

    echo '<textarea name="motivoAcao" class="form-control free" placeholder="informe aqui" required></textarea>
                                    <input type="hidden" value="' . $codigo . '" class="free" name="codigo">
                                    <input type="hidden" id="acao" class="free" name="acaoModal">';
    if ($form == "Osm" || $form == "Osp" || $form == "Osmp")
        echo '<input type="hidden" id="acao" value="' . $form . '" name="tipoOs">';

    echo '</div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submitButton" class="btn btn-success btn-lg" aria-label="right align" title="Aprovar">
                            <i class="fa fa-check fa-2x"></i>
                        </button>

                        <button type="button" class="btn btn-danger btn-lg" aria-label="right align" data-dismiss="modal" title="Cancelar">
                            <i class="fa fa-times fa-2x"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>';
}

// #### Bot�es SAF

function btnAutorizarSaf($saf = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($saf) && ($saf['cod_status'] == 1 || $saf['cod_status'] == 4)) { // Autorizada e Aberta
        $btn .= '<button type="button" class="btn btn-success btn-lg autorizarSaf" aria-label="right align" title="Aprovar / Gerar SSM">
                        <i class="fa fa-check fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button type="button" class="btn btn-lg" disabled aria-label="right align">
                        <i class="fa fa-check fa-2x"></i>
                    </button>';
    }
    $btn .= '</div>';

    return $btn;
}

function btnAutorizarSafView($saf = null)
{
    $btn = '';

    if (!empty($saf) && ($saf['cod_status'] == 1 || $saf['cod_status'] == 4)) { // Autorizada e Aberta
        $btn .= '<button class="btn-verde btnAutorizarSaf" value="'.$saf['cod_saf'].'">Gerar SSM</button>';
    }

    return $btn;
}

function btnDevolverSaf($saf = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($saf) && $saf['nome_status'] == "Aberta") {
        $btn .= '<button type="button" class="btn btn-warning btn-lg" aria-label="right align" title="Devolver" data-toggle="modal" data-action="devolver" data-target="#motivoModal">
                        <i class="fa fa-mail-reply fa-2x"></i>
                </button>';
    } else {
        $btn .= '<button type="button" class="btn btn-lg" disabled aria-label="right align" title="Devolver">
                        <i class="fa fa-mail-reply fa-2x"></i>
                </button>';
    }
    $btn .= '</div>';

    return $btn;
}

function btnDevolverSafView($saf = null)
{
    $btn = '';

    if (!empty($saf) && $saf['nome_status'] == "Aberta") {
        $btn .= '<button class="btn-laranja btnDevolverSaf"  value="'.$saf['cod_saf'].'">Devolver</button>';
    }

    return $btn;
}

function btnCancelarSaf($saf = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($saf) && ($saf['cod_status'] == 3 || $saf['cod_status'] == 4)) { // Devolvida e Aberta
        $btn .= '<button type="button" class="btn btn-danger btn-lg" aria-label="right align" title="Cancelar" data-toggle="modal" data-action="cancelar" data-target="#motivoModal">
                        <i class="fa fa-times fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button type="button" class="btn btn-lg" disabled aria-label="right align" title="Cancelar">
                        <i class="fa fa-times fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnCancelarSafView($saf = null)
{
    $btn = '';

    if (!empty($saf) && ($saf['cod_status'] == 3 || $saf['cod_status'] == 4)) { // Devolvida e Aberta
        $btn .= '<button class="btn-vermelho btnCancelarSaf"  value="'.$saf['cod_saf'].'">Cancelar</button>';
    }

    return $btn;
}

function btnSalvarSaf($saf = null, $nivel = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($saf) && empty($nivel) && ($saf['nome_status'] == "Encerrada" || $saf['nome_status'] == "Cancelada" || $saf['nome_status'] == "Autorizada" || $saf['nome_status'] == "Programada" || $saf['nome_status'] == "Pendente" || $saf['nome_status'] == "N�o Autorizado")) {
        $btn .= '<button type="button" aria-label="right align" class="btn btn-lg" disabled title="Salvar">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>';
    } else {
        $btn .= '<button type="button" aria-label="right align" id="salvarSaf" class="btn btn-default btn-lg" title="Salvar">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>';
    }
    $btn .= '</div>';

    return $btn;
}

function btnVerHistorico()
{
    $btn = '<div class="btn-group">
                <button type="button" class="btn btn-primary btn-lg btnHistorico"
                    data-toggle="modal" data-target=".modal-historico" title="Ver Hist�rico do Formul�rio">
                    <i class="fa fas fa-list-ul fa-2x"></i>
                </button>
            </div>';
    return $btn;
}

function btnVerSolicitacaoNivel()
{
    $btn = '<div class="btn-group">
                <button type="button" class="btn btn-default btn-lg btnSolicitacaoNivel"
                    data-toggle="modal" data-target=".modal-solicitacaoNivel" title="Ver Hist�rico de Solicita��o de Altera��o de N�vel">
                    <i class="fa fas fa-exchange fa-2x"></i>
                </button>
            </div>';
    return $btn;
}

function btnAlterarNivel()
{
    $btn = '<div class="btn-group">
                <button type="button" id="btnAlterarNivel" class="btn btn-default btn-lg"
                    data-toggle="modal" data-target=".modal-AlterarNivel" title="Solicitar Alterar N�vel">
                    <i class="fa fas fa-edit fa-2x"></i>
                </button>
            </div>';
    return $btn;
}

function btnReenviarSaf()
{
    $btn = '<div class="btn-group">
                <input type="hidden" value="reenviar" name="reenviar" />
                <button type="submit" id="salvarSaf" class="btn btn-success btn-lg" aria-label="right align" title="Reenviar">
                    <i class="fa fa-mail-reply fa-2x"></i>
                </button>
            </div>';

    return $btn;
}

function btnAprovarSaf()
{
    $btn = '<div class="btn-group">
                <button type="button" id="aprovarSaf" class="btn btn-success btn-lg aprovarSaf" aria-label="right align" title="Aprovar">
                    <i class="fa fa-check fa-2x"></i>
                </button>
            </div>';

    return $btn;
}

function btnNegarSaf()
{
    $btn = '<div class="btn-group">
                <button type="button" id="negarSaf" class="btn btn-danger btn-lg negarSaf" aria-label="right align" title="Negar" data-toggle="modal" data-action="negar" data-target="#motivoModal">
                    <i class="fa fa-times fa-2x"></i>
                </button>
            </div>';

    return $btn;
}

function btnDisabled()
{
    $btn = '<div class="btn-group">
                <button type="button" class="btn btn-lg" disabled aria-label="right align">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>
            </div>';

    return $btn;
}

function btnImprimir()
{
    $btn = '<div class="btn-group">
                <button type="button" class="btn btn-default btn-lg btnImprimirFormulario" title="Imprimir" aria-label="right align">
                    <i class="fa fa-print fa-2x"></i>
                </button>
            </div>';

    return $btn;
}

function btnImprimirView()
{
    $btn = '<button class="btn-azul" onclick="print()">Imprimir</button>';

    return $btn;
}

// #### Bot�es SSM

function btnGerarOsm($ssm = null)
{
    $btn = '<div class="btn-group">';

    $btn .= '<button aria-label="right align" type="button" class="btn btn-lg ';

    $arrayBlock = [26,28,35,39];//Programada, Agendada, Aguardando Aprova��o, An�lise
    if (!empty($ssm) && !in_array($ssm['cod_status'], $arrayBlock)) {
        $arrayBlock = [12,15,36,42,43]; // Encaminhado, Pendente, Reprovada, Aguardando Material, Compra Reprovada
        if (!in_array($ssm['cod_status'], $arrayBlock)) {
            if ($ssm['cod_status'] == 5 || $ssm['cod_status'] == 16 || $ssm['cod_status'] == 7 || $ssm['cod_status'] == 41) { //Autorizada, Encerrada, Cancelada ou Pend�ncia Material
                $btn .= '" disabled>
                            <i class="fa fa-gears fa-2x"></i>
                        </button>';
            } else {
                $btn .= 'btn-success encaminharEquipeSsm" title="Encaminhar para Equipe">
                            <i class="fa fa-arrow-right fa-2x"></i>
                        </button>';
            }
        } else {
            $btn .= 'btn-success gerarOsm" title="Gerar Osm">
                            <i class="fa fa-gears fa-2x"></i>
                        </button>';
        }
    } else {
        $btn .= '" disabled>
                        <i class="fa fa-gears fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnGerarOsmView($ssm = null)
{
    $btn = '';

    if (!empty($ssm) && ($ssm['cod_status'] != 26 && $ssm['cod_status'] != 28 && $ssm['cod_status'] != 35)) { //Programada, Agendada, Aguardando Aprova��o
        if ($ssm['cod_status'] != 12 && $ssm['cod_status'] != 15 && $ssm['cod_status'] != 36) { // Encaminhado, Pendente e Reprovada
            if ($ssm['cod_status'] == 5 || $ssm['cod_status'] == 16 || $ssm['cod_status'] == 7) { //Autorizada, Encerrada e Cancelada

            } else {

            }
        } else {
            $btn .= '<button class="btn-verde btnGerarOsm"  value="'.$ssm['cod_ssm'].'">Gerar Osm</button>';
        }
    }

    return $btn;
}

function btnDevolverSsm($ssm = null)
{
    $btn = '<div class="btn-group">';

    if(!empty($ssm) && $ssm['cod_status'] == "12") {
        $btn .= '<button type="button" class="btn btn-warning btn-lg btnDevolverSSM"  aria-label="right align" title="Devolver (Sem Altera��es)" data-toggle="modal" data-action="devolver" data-target="#motivoModal">
                        <i class="fa fa-mail-reply fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button class="btn btn-lg" disabled >
                        <i class="fa fa-mail-reply fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnDevolverSsmView($ssm = null)
{
    $btn = '';

    if(!empty($ssm) && $ssm['cod_status'] == "12") {
        $btn .= '<button class="btn-laranja btnDevolverSsm"  value="'.$ssm['cod_ssm'].'">Devolver</button>';
    }

    return $btn;
}

function btnCancelarSsm($ssm = null)
{
    $btn = '<div class="btn-group">';

    /*
    "Autorizada"
    "Cancelada"
    "Encaminhado"
    "Pendente"
    "Encerrada"
    "Programada"
    "N�o Validado"
    "Agendada"
    "Pend�ncia de material"
    "Aguardando material"
    "Pend�ncia (Compra Negada)"
    */
    $arrayBlock = [5, 7, 12, 15, 16, 26, 28, 36, 39, 41, 42, 43];

    if(
        !empty($ssm) && !in_array($ssm['cod_status'], $arrayBlock)
    ) {
        $btn .= '<button type="button" class="btn btn-danger btn-lg btnCancelarSSM" aria-label="right align" title="Cancelar" data-toggle="modal" data-action="cancelar" data-target="#motivoModal">
                    <i class="fa fa-times fa-2x"></i>
                </button>';

    } else {
        $btn .= '<button type="button" class="btn btn-lg" disabled>
                        <i class="fa fa-times fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnCancelarSsmView($ssm = null)
{
    $btn = '';
    /*
    "Autorizada"
    "Cancelada"
    "Encaminhado"
    "Encerrada"
    "Programada"
    "Agendada"
    "N�o Validado"
    */
    $arayBloc = [5,7,12,16,26,28,36];

    if(
        !empty($ssm) && !in_array($ssm['cod_status'], $arrayBlock)
    ) {
        if ($ssm['nome_status'] == "Pendente") {
            //ENCERRAR-------------------------------------
        } else {
            $btn .= '<button class="btn-vermelho btnCancelarSsm"  value="'.$ssm['cod_ssm'].'">Cancelar</button>';
        }
    }

    return $btn;
}

function btnSalvarSsm($ssm = null, $nivel = null)
{
    $btn = '<div class="btn-group">';

    if ((!empty($ssm) && $ssm['nome_status'] == "Encaminhado") || !empty($nivel)) {
        $btn .= '<button type="button" class="btn btn-default btn-lg salvarAlteracao" disabled aria-label="right align" title="Salvar">
                        <i class="fa fa-floppy-o fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button type="button" disabled class="btn btn-lg" aria-label="right align">
                        <i class="fa fa-floppy-o fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnImprimirSsm($ssm = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($ssm)) {
        $btn .= '<button type="button" class="btn btn-default btn-lg" onclick="window.print()" aria-label="right align" title="Imprimir SSM (Sem Altera��es)">
                        <i class="fa fa-print fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button type="button" class="btn btn-lg" disabled aria-label="right align">
                        <i class="fa fa-print fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

// #### Bot�es SSP

function btnGerarOsp($ssp = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($ssp)) {
        if ($ssp['nome_status'] == "Autorizada" || $ssp['nome_status'] == "Encerrada" || $ssp['nome_status'] == "Cancelada") {
            $btn .= '<button type="button" disabled class="btn btn-lg" aria-label="right align">
                    <i class="fa fa-gears fa-2x"></i>
                </button>';
        } else {
            $btn .= '<button type="button" class="btn btn-success btn-lg gerarOsp" aria-label="right align" title="Gerar Osp">
                    <i class="fa fa-gears fa-2x"></i>
                </button>';
        }
    } else {
        $btn .= '<button type="button" disabled class="btn btn-lg" aria-label="right align">
                    <i class="fa fa-gears fa-2x"></i>
                </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnCancelarSsp($ssp = null)
{
    $btn = '<div class="btn-group">';

    if ($ssp['nome_status'] == "Programado") {
        $btn .= '<button type="button" class="btn btn-danger btn-lg" aria-label="right align" title="Cancelar" data-toggle="modal" data-action="cancelar" data-target="#motivoModal">
                        <i class="fa fa-times fa-2x"></i>
                    </button>';
    } else if ($ssp['nome_status'] == "Pendente") {
        //ENCERRAR-------------------------------------
        $btn .= '<button type="button" class="btn btn-danger btn-lg" aria-label="right align" title="Encerrar" data-toggle="modal" data-action="encerrar" data-target="#motivoModal">
                        <i class="fa fa-times fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button disabled class="btn btn-lg" aria-label="right align">
                        <i class="fa fa-times fa-2x"></i>
                    </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnSalvarSsp($ssp = -1)
{
    $btn = '<div class="btn-group">';

    if (empty($ssp) || $ssp['nome_status'] == "Programado" || $ssp['nome_status'] == "Aberto") {
        $btn .= '<button type="button" class="btn btn-default btn-lg salvarSsp" aria-label="right align" title="Salvar">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>';

        $btn .= '</div>';
    } else {
        $btn .= '<button disabled class="btn btn-lg" aria-label="right align">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>';

        $btn .= '</div>';
    }

    return $btn;
}

function btnImprimirSsp($ssp = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($ssp)) {
        $btn .= '<button type="button" class="btn btn-default btn-lg" onclick="window.print()" aria-label="right align" title="Imprimir Ssp (Sem altera��es)">
                                            <i class="fa fa-print fa-2x"></i>
                                        </button>';
    } else {
        $btn .= '<button type="button" class="btn btn-lg" disabled aria-label="right align">
                                            <i class="fa fa-print fa-2x"></i>
                                        </button>';
    }

    $btn .= '</div>';

    return $btn;
}

// #### Bot�es SSMP

function btnGerarOsmp($ssmp = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($ssmp) && ($ssmp['nome_status'] == "Programado" || $ssmp['nome_status'] == "Pendente")) {
        $btn .= '<button type="button" class="btn btn-success btn-lg btnGerarOsmp" aria-label="right align" title="Gerar Osmp">
                <i class="fa fa-gears fa-2x"></i>
            </button>';
    } else {
        $btn .= '<button disabled class="btn btn-lg" aria-label="right align">
                    <i class="fa fa-gears fa-2x"></i>
                </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnSalvarSsmp($ssmp = null){
    $btn = '<div class="btn-group">';

    $btn .= '<button type="button" class="btn btn-default btn-lg salvarSsmp" aria-label="right align" title="Salvar Ssmp">
                <i class="fa fa-floppy-o fa-2x"></i>
            </button>';

    $btn .= '</div>';

    return $btn;
}

function btnProgramarSsmp($ssmp = null, $usuario = null){
    $btn = '<div class="btn-group">';

    if (!empty($ssmp) && $ssmp['nome_status'] == "Aberta" || ($usuario != null && in_array($usuario['nivel'], array(7, 7.1))) ) {
        $btn .= '<button type="button" class="btn btn-success btn-lg programarSsmp" aria-label="right align" title="Programar Ssmp">
                    <i class="fa fa-clock-o fa-2x"></i>
                </button>';

        $btn .= '</div>';
    } else {
        $btn .= '<button disabled class="btn btn-lg" aria-label="right align">
                    <i class="fa fa-clock-o fa-2x"></i>
                </button>';

        $btn .= '</div>';
    }

    return $btn;
}

function btnImprimirSsmp($ssmp = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($ssmp)) {
        $btn .= '<button type="button" class="btn btn-default btn-lg" onclick="window.print()" aria-label="right align" title="Imprimir Ssmp (Sem altera��es)">
                    <i class="fa fa-print fa-2x"></i>
                </button>';
    } else {
        $btn .= '<button disabled class="btn btn-lg" aria-label="right align">
                    <i class="fa fa-print fa-2x"></i>
                </button>';
    }

    $btn .= '</div>';

    return $btn;
}

// #### Bot�es Ti

function btnCriarUser($user = null)
{
    $btn = '<div class="btn-group">';

    if (!empty($user)) {
        $btn .= '<button type="submit" class="btn btn-default btn-lg salvarAlteracao" aria-label="right align" title="Salvar altera��es">
                        <i class="fa fa-floppy-o fa-2x"></i>
                    </button>';
    } else {
        $btn .= '<button type="submit" class="btn btn-lg btn-success criarNova" aria-label="right align" title="Criar Usu�rio">
                        <i class="fa fa-plus fa-2x"></i>
                </button>';
    }

    $btn .= '</div>';

    return $btn;
}

function btnDesativarUsuario($user = null)
{
    $btn = '<div class="btn-group">';
    $btn .= '<button type="button" class="btn btn-danger btn-lg desativar" aria-label="right align" title="Salvar altera��es">
                <i class="fa fa-times fa-2x"></i>
            </button>';

    $btn .= '</div>';

    return $btn;
}
