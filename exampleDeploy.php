<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'simg');

// Project repository
set('repository', 'git@gitlab.com:metrofor/simg.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploysMas foi o usu�rio quem fez essa solicita��o. Por isso que eu digo que essa conversa � improdutiva. N�o depende de n�s isso. 
set('shared_files', []);
set('shared_dirs', ['/vendor', '/views/_images', '/includes/node_modules', '/includes/help', '/includes/bin']);

// Writable dirs by web server
set('writable_dirs', []);
set('allow_anonymous_stats', false);
//set('use_relative_symlinks', false);

//Aux connection to the host and execute commands
set('ssh_multiplexing', false);

// Hosts
inventory('hosts.yml');

// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);
//Ask whats branch is to deploy
task('what_branch', function () {
    $hostName = "hgsimg.metrofor.ce.gov.br";
    $branch = ask('What branch to deploy?');

    on(host($hostName), function ($host) use ($branch) {
        set('branch', $branch);
    });
})->local();


//Return the current url of the app
task('pwd', function () {
    writeln("Current dir: {{hostname}}");
    $result = run('pwd');
    writeln("Current dir: $result");

    // writeln("Current dir: $this->hostname");
});

task('cp_config', function () {
    run('cp /var/www/simg.metrofor.ce.gov.br/simg/current/exampleConfig.php /var/www/simg.metrofor.ce.gov.br/simg/current/config.php ');
    run('chmod 777 /var/www/simg.metrofor.ce.gov.br/simg/current/includes/temp/');
    writeln("Configuration has been applyied");
});

task('cp_confighg', function () {
    run('cp /var/www/hgsimg.metrofor.ce.gov.br/simg/current/exampleConfigHG.php /var/www/hgsimg.metrofor.ce.gov.br/simg/current/config.php ');
    run('chmod 777 /var/www/hgsimg.metrofor.ce.gov.br/simg/current/includes/temp/');
    writeln("Configuration has been applyied");
});

after('deploy', 'cp_confighg');

task('deploy_master', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);
after('deploy_master', 'cp_config');

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
