<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 17/03/2017
 * Time: 09:53
 *
 * Classe que possui fun��es para a cria��o dos Dashboards do sistema, contendo as partes em comum de todos os niveis
 */

require_once(ABSPATH . '/functions/btnForm.php');

class MainForm
{
    public $editorSaf =  array(7,23);

    public $medoo;

    public function __construct($medoo)
    {
        $this->medoo = $medoo;
    }

    public function saf($usuario, $codSaf = null)
    {
        if (!empty($codSaf)) { // Ver / Editar SAF
            $saf = $this->medoo->select('v_saf', '*', ["cod_saf" => (int)$codSaf]);
            $saf = $saf[0];

            $btnsAcao = "";

            if ((in_array($usuario['cod_usuario'], $this->editorSaf) || $saf['usuario'] == $usuario['usuario']) && $saf['cod_status'] == 3) { // Tratar Devolvida
                $btnsAcao .= btnReenviarSaf();
                $btnsAcao .= btnCancelarSaf($saf);
                $btnsAcao .= modalDevolverCancelar($saf, "Saf");
            } else {
                if($saf['cod_status'] == 35 && in_array($usuario['cod_usuario'], AUDITORIA)) {
                    $btnsAcao .= btnAprovarSaf();
                    $btnsAcao .= btnNegarSaf();
                    $btnsAcao .= modalDevolverCancelar($saf, "NegarSaf");
                } else if (in_array($usuario['nivel'], array(2)) && $saf['cod_status'] != 3) { // Edi��o Completa
                    $btnsAcao .= btnAutorizarSaf($saf);
                    $btnsAcao .= btnDevolverSaf($saf);
                    $btnsAcao .= btnCancelarSaf($saf);
                    $btnsAcao .= btnSalvarSaf($saf);
                    $btnsAcao .= modalDevolverCancelar($saf, "Saf");
                } else if (in_array($usuario['nivel'], array(7.1))) { // Alterar
                    $btnsAcao .= btnSalvarSaf($saf, $usuario['nivel']);
                } else { // Apenas Ver
                    $btnsAcao .= btnDisabled();

                    $_SESSION['bloqueio'] = true;
                }
            }

            $btnsAcao .= btnImprimir();
            $btnsAcao .= btnVerHistorico();
        } else { // Criar SAF
            $saf = null;
            $btnsAcao = btnSalvarSaf();
            $btnsAcao .= btnImprimir();
        }

        $historico = "";
        $descricaoDevolvidas = $this->medoo->select('status_saf', 'descricao', [
                'AND' => [
                    "cod_saf"    => (int)$codSaf,
                    "cod_status" => (int)3 // Devolvida
                ]
            ]
        );

        foreach ($descricaoDevolvidas as $dados => $value) {
            $historico .= $value . "\n";
        }

        return [
            "dados"     => $saf,
            "botoes"    => $btnsAcao,
            "historico" => $historico
        ];
    }

    public function viewSafButton($usuario, $codSaf)
    {
        $saf = $this->medoo->select('v_saf', '*', ["cod_saf" => (int)$codSaf]);
        $saf = $saf[0];

        $btnsAcao = "";

        if ((in_array($usuario['cod_usuario'], $this->editorSaf) || $saf['usuario'] == $usuario['usuario']) && $saf['cod_status'] == 3) { // Tratar Devolvida
            //$btnsAcao .= btnReenviarSaf();
            $btnsAcao .= btnCancelarSafView($saf);
        } else {
            if($saf['cod_status'] == 35 && in_array($usuario['cod_usuario'], AUDITORIA)) {
                //$btnsAcao .= btnAprovarSaf();
                $btnsAcao .= btnNegarSaf();
            } else if (in_array($usuario['nivel'], array(2)) && $saf['cod_status'] != 3) { // Edi��o Completa
                $btnsAcao .= btnAutorizarSafView($saf);
                $btnsAcao .= btnDevolverSafView($saf);
                $btnsAcao .= btnCancelarSafView($saf);
            }
        }

        $btnsAcao .= btnImprimirView();

        return [
            "botoes" => $btnsAcao
        ];
    }

    public function botoesPesquisaSaf()
    {
        //data-toggle="modal" data-target=".ExibirSaf"
        return '<button class="btn btn-primary btn-circle btnViewSaf" type="button" title="Visualizar"><i class="fa fa-eye"></i></button>
                <button class="btn btn-default btn-circle btnImprimirSaf" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
                <button class="btn btn-success btn-circle btnAbrirPesquisaSsm" type="button" title="Abrir Pesquisa SSM">SSM</button>';
    }

    public function ssm($usuario, $codSsm = null)
    {
        $ssm = $this->medoo->select('v_ssm', '*', ["cod_ssm" => (int)$codSsm]);
        $ssm = $ssm[0];

        $saf = $this->medoo->select("v_saf", "*", ["cod_saf" => (int)$ssm['cod_saf']]);
        $saf = $saf[0];

        $btnsAcao = "";
        if (
            (in_array($usuario['nivel'], array(2))) ||
            (
                (in_array($usuario['nivel'], array(5, 5.1, 5.2))) &&
                ($ssm['nome_status'] != "Aberta" || $ssm['nome_status'] != "Devolvida" || $ssm['nome_status'] != "Transferida")
            )
        ) { // Edi��o Completa
            $btnsAcao .= btnGerarOsm($ssm);
            $btnsAcao .= btnDevolverSsm($ssm);
            $btnsAcao .= btnCancelarSsm($ssm);
            $btnsAcao .= btnSalvarSsm($ssm);
            $btnsAcao .= modalDevolverCancelar($ssm, "Ssm");
        } else if (in_array($usuario['nivel'], array(7, 7.1))) {
            $btnsAcao .= btnSalvarSsm($ssm, $usuario['nivel']);
        } else {
            $_SESSION['bloqueio'] = true;
            $btnsAcao .= btnDisabled();
        }

        $btnsAcao .= btnImprimir();
        $btnsAcao .= btnVerHistorico();

        $hasAlterNivel = $this->medoo->select('reg_alter_nivel_ssm', '*', ['cod_ssm' => $ssm['cod_ssm']]);
        if(!empty($hasAlterNivel))
          $btnsAcao .= btnVerSolicitacaoNivel();
          
        $btnsAcao .= btnAlterarNivel();

        $historico = "";
        $dataEncaminhamento = "";
        $descricaoStatus = $this->medoo->select('status_ssm', ['descricao', 'data_status', 'cod_status'], [
                "cod_ssm" => (int)$codSsm,
                "ORDER" => "data_status"
            ]
        );

        foreach ($descricaoStatus as $dados => $value) {
            if ($value['cod_status'] == "12")
                $dataEncaminhamento = $value['data_status'];

            if (!empty($value['descricao']))
                $historico .= $value['descricao'] . "\n";
        }

        return [
            "dadosSsm"  => $ssm,
            "dadosSaf"  => $saf,
            "botoes"    => $btnsAcao,
            "historico" => $historico,
            "dataEncaminhamento" => $dataEncaminhamento
        ];
    }

    public function viewSsmButton($usuario, $codSsm)
    {
        $ssm = $this->medoo->select('v_ssm', '*', ["cod_ssm" => (int)$codSsm]);
        $ssm = $ssm[0];

        $btnsAcao = "";
        if (
            (in_array($usuario['nivel'], array(2))) ||
            (
                (in_array($usuario['nivel'], array(5, 5.1, 5.2))) &&
                ($ssm['nome_status'] != "Aberta" || $ssm['nome_status'] != "Devolvida" || $ssm['nome_status'] != "Transferida")
            )
        ) { // Edi��o Completa
            $btnsAcao .= btnGerarOsmView($ssm);
            $btnsAcao .= btnDevolverSsmView($ssm);
            //$btnsAcao .= btnCancelarSsmView($ssm);
        }

        $btnsAcao .= btnImprimirView();

        return [
            "botoes" => $btnsAcao
        ];
    }

    public function botoesPesquisaSsm()
    {
        return '<button class="btn btn-primary btn-circle btnViewSsm" type="button" title="Visualizar"><i class="fa fa-eye"></i></button>
                <button class="btn btn-default btn-circle btnImprimirSsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
                <button class="btn btn-primary btn-circle btnAbrirPesquisaSaf" type="button" title="Abrir Pesquisa SAF">SAF</button>
                <button class="btn btn-success btn-circle btnAbrirPesquisaOsm" type="button" title="Abrir Pesquisa OSM">OSM</button>';
    }

    public function botoesPesquisaOsm()
    {
        return '<button class="btn btn-default btn-circle btnEditarOsm" type="button" title="Ver Dados Gerais / Editar"><i class="fa fa-file-o fa-fw"></i></button>
                <button class="btn btn-default btn-circle btnImprimirOsm" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
                <button class="btn btn-primary btn-circle btnAbrirPesquisaSsm" type="button" title="Abrir Pesquisa SSM">SSM</button>';
    }

    public function ssp($usuario, $codSsp = null)
    {
        if (!empty($codSsp)) { // Ver / Editar SSP
            $ssp = $this->medoo->select('v_ssp', '*', ["cod_ssp" => (int)$codSsp]);
            $ssp = $ssp[0];

            $btnsAcao = "";

            if (in_array($usuario['nivel'], array(2, 5, 5.1, 5.2))) { // Edi��o Completa
                $btnsAcao .= btnGerarOsp($ssp);
                $btnsAcao .= btnCancelarSsp($ssp);
                $btnsAcao .= btnSalvarSsp($ssp);
                $btnsAcao .= modalDevolverCancelar($ssp, "Ssp");
            } else { // Apenas Ver
                if(in_array($usuario['nivel'], array(2.1))){
                    $btnsAcao .= btnSalvarSsp($ssp);
                    $_SESSION['bloqueio'] = false;
                }else{
                    $btnsAcao .= btnDisabled();
                    $_SESSION['bloqueio'] = true;
                }
            }

            $btnsAcao .= btnImprimir();
            $btnsAcao .= btnVerHistorico();
        } else { // Criar SSP
            $ssp = null;
            $btnsAcao = btnSalvarSsp(null);
            $btnsAcao .= btnImprimir();
        }

        return [
            "dados" => $ssp,
            "botoes" => $btnsAcao
        ];
    }

    public function botoesPesquisaSsp()
    {
        return '<button class="btn btn-default btn-circle btnEditarSsp" type="button" title="Ver Dados / Editar"><i class="fa fa-file-o fa-fw"></i></button>
                <button class="btn btn-default btn-circle btnImprimirSsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
                <button class="btn btn-success btn-circle btnAbrirPesquisaOsp" type="button" title="Abrir Pesquisa OSP">OSP</button>';
    }

    public function botoesPesquisaOsp()
    {
        return '<button class="btn btn-default btn-circle btnEditarOsp" type="button" title="Ver Dados Gerais / Editar"><i class="fa fa-file-o fa-fw"></i></button>
                <button class="btn btn-default btn-circle btnImprimirOsp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>
                <button class="btn btn-primary btn-circle btnAbrirPesquisaSsp" type="button" title="Abrir Pesquisa SSP">SSP</button>';
    }

    public function ssmp($usuario, $grupo, $codSsmp)
    {
        $arrayCollumn = [
            'ssmp.cod_ssmp', 'ssmp.data_abertura', 'solicitacao_acesso',
            'dias_servico', 'data_programada', 'usuario.usuario',
            'nome_status', 'un_equipe.cod_unidade', 'ssmp.complemento',
            'linha.nome_linha', 'linha.cod_linha', 'nome_servico_pmp',
        ];

        $arrayCollumnPMP = ['nome_sistema', 'nome_subsistema', 'nome_procedimento', 'nome_periodicidade', 'cod_tipo_periodicidade'];

        switch ($grupo){
            case 'ed':
                $tipo   = "ed";
                $nomeTipo = "Edifica��o";
                array_push($arrayCollumn, "nome_local");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpEdificacao($arrayCollumn);
                break;
            case 'ra':
                $tipo   = "ra";
                $nomeTipo = "Rede Aerea";
                array_push($arrayCollumn, "nome_local", "ssmp_ra.cod_poste", "nome_via", "ssmp_ra.posicao");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpRedeAerea($arrayCollumn);
                break;
            case 'vp':
                $tipo   = "vp";
                $nomeTipo = "Via Permanente";
                array_push($arrayCollumn, "ei.nome_estacao AS estacao_inicial", "ef.nome_estacao AS estacao_final", "nome_via", "ssmp_vp.posicao",
                    "ssmp_vp.km_inicial","ssmp_vp.km_final", "ssmp_vp.km_inicial","ssmp_vp.km_final", "nome_amv");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpViaPermanente($arrayCollumn);
                break;
            case 'su':
                $tipo   = "su";
                $nomeTipo = "Subesta��o";
                array_push($arrayCollumn, "nome_local");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpSubestacao($arrayCollumn);
                break;
            case 'vlt':
                $tipo   = "vlt";
                $nomeTipo = "Material Rodante Vlt";
                array_push($arrayCollumn, "nome_veiculo");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpVlt($arrayCollumn);
                break;
            case 'tue':
                $tipo   = "tue";
                $nomeTipo = "Material Rodante Tue";
                array_push($arrayCollumn, "nome_veiculo");
                $sql = Sql::ssmpTue($arrayCollumn);
                break;
            case 'tl':
                $tipo   = "tl";
                $nomeTipo = "Telecom";
                array_push($arrayCollumn, "nome_local");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpTelecom($arrayCollumn);
                break;
            case 'bl':
                $tipo   = "bl";
                $nomeTipo = "Bilhetagem";
                array_push($arrayCollumn, "nome_estacao");
                $arrayCollumn = array_merge($arrayCollumn, $arrayCollumnPMP);
                $sql = Sql::ssmpBilhetagem($arrayCollumn);
                break;
        }

        $sql .= " WHERE ssmp.cod_ssmp = {$codSsmp}";

        $dados = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        $dados = $dados[0];

        $btnsAcao = "";

        if (in_array($usuario['nivel'], array(2, 5, 5.1))) { // Edi��o Completa
            if($dados['nome_status'] != "Aberta"){
                $_SESSION['bloqueio'] = true;
            }

            $btnsAcao .= btnGerarOsmp($dados);
            $btnsAcao .= btnProgramarSsmp($dados);
        } else { // Apenas Ver
            if(in_array($usuario['nivel'], array(7, 7.1))){
                $_SESSION['bloqueio'] = false;
                $btnsAcao .= btnProgramarSsmp($dados, $usuario);
            }else{
                $_SESSION['bloqueio'] = true;
                $btnsAcao .= btnDisabled();
            }
        }

        $btnsAcao .= btnImprimirSsmp($dados);
        $btnsAcao .= btnVerHistorico();

        return [
            "dados"     => $dados,
            "botoes"    => $btnsAcao,
            "tipo"      => $tipo,
            "nomeTipo"  => $nomeTipo
        ];
    }

    public function osmp($usuario, $tipo, $codOsmp = null)
    {
        $tituloOs = "Ordem de Servi�o de Manuten��o Programada";
        $actionForm = "moduloOsmp";
        $acao = true;
        $pag = "dadosGerais";

        switch ($tipo) {
            case "ed":
                $tipoOS = "Osmp/ed";
                $_SESSION['refillOs']['form'] = "ed";
                $sql = Sql::osmpEd();
                break;
            case "vp":
                $tipoOS = "Osmp/vp";
                $_SESSION['refillOs']['form'] = "vp";
                $sql = Sql::osmpVp();
                break;
            case "su":
                $tipoOS = "Osmp/su";
                $_SESSION['refillOs']['form'] = "su";
                $sql = Sql::osmpSu();
                break;
            case "ra":
                $tipoOS = "Osmp/ra";
                $_SESSION['refillOs']['form'] = "ra";
                $sql = Sql::osmpRa();
                break;
            case "vlt":
                $tipoOS = "Osmp/vlt";
                $_SESSION['refillOs']['form'] = "vlt";
                $sql = Sql::osmpVlt();
                break;
            case "tue":
                $tipoOS = "Osmp/tue";
                $_SESSION['refillOs']['form'] = "tue";
                $sql = Sql::osmpTue();
                break;
            case "tl":
                $tipoOS = "Osmp/tl";
                $_SESSION['refillOs']['form'] = "tl";
                $sql = Sql::osmpTl();
                break;
            case "bl":
                $tipoOS = "Osmp/bl";
                $_SESSION['refillOs']['form'] = "bl";
                $sql = Sql::osmpBl();
                break;
        }
        $sql .= " WHERE osmp.cod_osmp = {$codOsmp}";

        $dadosOs = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC)[0];


        $_SESSION['refillOs']['codigoOsmp'] = $dadosOs['cod_osmp'];

        $maoObra = $this->medoo->select("osmp_mao_de_obra", "cod_osmp_mao_de_obra", ["cod_osmp" => $codOsmp]);
        $tempoTotal = $this->medoo->select("osmp_tempo", "cod_osmp_tempo", ["cod_osmp" => $codOsmp]);
        $regExecucao = $this->medoo->select("osmp_registro", "cod_osmp_registro", ["cod_osmp" => $codOsmp]);

        $selectDescStatus['cod_status'] = $dadosOs['cod_status'];
        $selectDescStatus['nome_status'] = $dadosOs['nome_status'];
        $selectDescStatus['descricao'] = $dadosOs['descricao'];

        if (in_array($usuario['nivel'], array(2, 5, 5.1, 7, 7.1))) { // Edi��o Completa
            //Recebe o status atual para futuras avalias
            $_SESSION['dadosCheck']['statusOs'] = $dadosOs['cod_status'];
            if ($_SESSION['dadosCheck']['codigoOs'] != $_SESSION['refillOs']['codigoOs']) {
                $_SESSION['dadosCheck']['codigoOs'] = $_SESSION['refillOs']['codigoOs'];

                $_SESSION['dadosCheck']['maquinaCheck'] = false;
                $_SESSION['dadosCheck']['materialCheck'] = false;
            }

            //Verifica se a OS est� em execu��o. Quando estiver encerrada n�o h� a necessidade de travar.
            if ($selectDescStatus['cod_status'] == 10) {
                $_SESSION['dadosCheck']['maquina'] = $this->medoo->select("osmp_maquina", "*", ["cod_osmp" => (int)$_SESSION['refillOs']['codigoOs']]);
                $_SESSION['dadosCheck']['material'] = $this->medoo->select("osmp_material", "*", ["cod_osmp" => (int)$_SESSION['refillOs']['codigoOs']]);
            }
            $modal = modalDevolverCancelar($dadosOs, "Osmp");

            $_SESSION['bloqueio'] = $this->bloquearEdicao($dadosOs['data_status'], $dadosOs['nome_status'], $usuario['nivel']);
        } else { // Apenas Ver
            $_SESSION['bloqueio'] = true;
        }

        return [
            "tituloOs" => $tituloOs,
            "actionForm" => $actionForm,
            "acao" => $acao,
            "pag" => $pag,
            "tipoOS" => $tipoOS,
            "dados" => $dadosOs,
            "maoObra" => $maoObra,
            "tempoTotal" => $tempoTotal,
            "regExecucao" => $regExecucao,
            "selectDescStatus" => $selectDescStatus,
            "modal" => $modal
        ];
    }

    public function botoesPesquisaOsmp()
    {
        return '<button class="btn btn-default btn-circle btnImprimirOsmp" type="button" title="Imprimir"><i class="fa fa-print fa-fw"></i></button>';
    }

    private function bloquearEdicao($dataEncerramento, $status, $usuario)
    {
        if ($status == "N�o Executado" || $status == "Duplicado" || $status == "N�o Configura falha")
            return true;

        if (!empty($dataEncerramento) && $status == "Encerrada") {
            switch ($usuario) {
                case 5:  // Equipe
                case '5.1':  // EquipeMr
                case '5.2':  // EquipeMr
                    $dias = "+3 days";
                    break;
                case 2:  // ccm
                    $dias = "+5 days";
                    break;
                case 15: // T.I
                    $dias = "+15 days";
                    break;
                case 7:  // Engenharia
                case '7.1':// Engenharia Supervis�o
                case '7.2':// Engenharia Material Rodante
                    $dias = "+1440 days";
                    break;
                default:
                    $dias = "+0 days";
            }

            $dataLimite = date('d-m-Y', strtotime($dias, strtotime($dataEncerramento)));
            $dataAtual = date('d-m-Y');

            if (strtotime($dataAtual) > strtotime($dataLimite)) {
                return true;
            }
        }
        return false;
    }

    //================================================================================================================//

    // Colocar o Select de N�vel
    public static function getSelectNivel($nivel = null)
    {
        echo "<select name='nivel' class='form-control'>";
        switch ($nivel) {
            case 'A':
                echo "<option value=''>TODOS</option>";
                echo "<option selected value='A'>N�VEL A</option>";
                echo "<option value='B'>N�VEL B</option>";
                echo "<option value='C'>N�VEL C</option>";
                break;
            case 'B':
                echo "<option value=''>TODOS</option>";
                echo "<option value='A'>N�VEL A</option>";
                echo "<option selected value='B'>N�VEL B</option>";
                echo "<option value='C'>N�VEL C</option>";
                break;
            case 'C':
                echo "<option value=''>TODOS</option>";
                echo "<option value='A'>N�VEL A</option>";
                echo "<option value='B'>N�VEL B</option>";
                echo "<option selected value='C'>N�VEL C</option>";
                break;
            default:
                echo "<option value=''>TODOS</option>";
                echo "<option value='A'>N�VEL A</option>";
                echo "<option value='B'>N�VEL B</option>";
                echo "<option value='C'>N�VEL C</option>";
                break;
        }
        echo "</select>";
    }

    // Colocar o Select de Grupo
    public static function getSelectGrupo($grupo, $sql, $nomeSelect = 'grupo', $selecionavel = true)
    {
        if ($selecionavel == true) {
            echo "<select id='{$nomeSelect}' name='{$nomeSelect}' class='form-control'>";
            echo "<option value=''>TODOS</option>";
        } else {
            echo "<select id='{$nomeSelect}' name='{$nomeSelect}' class='form-control' required>";
            echo "<option value='' selected disabled>SELECIONE UM GRUPO</option>";
        }
        foreach ($sql as $dados => $value) {
            if ($grupo == $value['cod_grupo'])
                echo("<option selected value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
            else
                echo("<option value='{$value['cod_grupo']}'>{$value['nome_grupo']}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Sistema
    public static function getSelectSistema($sistema, $sql, $nomeSelect = 'sistema', $selecionavel = true)
    {
        if ($selecionavel == true) {
            echo "<select name='{$nomeSelect}' class='form-control'>";
            echo "<option value=''>TODOS</option>";
        } else {
            echo "<select name='{$nomeSelect}' class='form-control' required>";
            echo "<option value='' selected disabled>SELECIONE UM SISTEMA</option>";
        }

        if ($sql) {
            foreach ($sql as $dados => $value) {
                if ($sistema == $value['cod_sistema'])
                    echo("<option selected value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
                else
                    echo("<option value='{$value['cod_sistema']}'>{$value['nome_sistema']}</option>");
            }
        }

        echo "</select>";
    }

    // Colocar o Select de Subsistema
    public static function getSelectSubsistema($subsistema, $sql, $nomeSelect = 'subsistema', $selecionavel = true)
    {
        if ($selecionavel == true) {
            echo "<select name='{$nomeSelect}' class='form-control'>";
            echo "<option value=''>TODOS</option>";
        } else {
            echo "<select name='{$nomeSelect}' class='form-control' required>";
            echo "<option value='' selected disabled>SELECIONE UM SUBSISTEMA</option>";
        }

        if ($sql) {
            foreach ($sql as $dados => $value) {
                if ($subsistema == $value['cod_subsistema'])
                    echo("<option selected value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
                else
                    echo("<option value='{$value['cod_subsistema']}'>{$value['nome_subsistema']}</option>");
            }
        }
        echo "</select>";
    }

    // Colocar o Select de Ve�culo
    public static function getSelectVeiculo($veiculo, $sql)
    {
        echo "<select name='veiculo' class='form-control'>";
        echo "<option value=''>TODOS</option>";
        foreach ($sql as $dados => $value) {
            if ($veiculo == $value['cod_veiculo'])
                echo("<option value='{$value['cod_veiculo']}' selected>{$value['nome_veiculo']}</option>");
            else
                echo("<option value='{$value['cod_veiculo']}'>{$value['nome_veiculo']}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Carro
    public static function getSelectCarro($carro, $sql)
    {
        echo "<select name='carro' class='form-control'>";
        echo "<option value=''>TODOS</option>";
        foreach ($sql as $dados => $value) {
            if ($carro == $value['cod_carro'])
                echo("<option value='{$value['cod_carro']}' selected>{$value['nome_carro']}</option>");
            else
                echo("<option value='{$value['cod_carro']}'>{$value['nome_carro']}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Prefixo
    public static function getSelectPrefixo($prefixo, $sql)
    {
        echo "<select name='prefixo' class='form-control'>";
        echo "<option value=''>TODOS</option>";
        foreach ($sql as $dados => $value) {
            if ($prefixo == $value['cod_prefixo'])
                echo("<option value='{$value['cod_prefixo']}' selected>{$value['cod_prefixo']}</option>");
            else
                echo("<option value='{$value['cod_prefixo']}'>{$value['cod_prefixo']}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Avaria
    public static function getSelectAvaria($avaria, $sql)
    {
        echo "<select name='avaria' class='form-control'>";
        echo "<option value=''>TODOS</option>";
        foreach ($sql as $dados => $value) {
            if ($avaria == $value['cod_avaria'])
                echo("<option selected value='{$value['cod_avaria']}'>{$value['nome_avaria']}</option>");
            else
                echo("<option value='{$value['cod_avaria']}'>{$value['nome_avaria']}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Linha
    public static function getSelectLinha($linha, $sql, $nomeSelect = "linha", $selecionavel = true)
    {
        if ($selecionavel == true) {
            echo "<select name='{$nomeSelect}' class='form-control'>";
            echo "<option value=''>TODOS</option>";
        } else {
            echo "<select name='{$nomeSelect}' class='form-control' required>";
            echo "<option value='' selected disabled>SELECIONE UMA LINHA</option>";
        }
        foreach ($sql as $dados => $value) {
            if ($linha == $value['cod_linha'])
                echo("<option value='{$value['cod_linha']}' selected>{$value["nome_linha"]}</option>");
            else
                echo("<option value='{$value['cod_linha']}'>{$value["nome_linha"]}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Trecho
    public static function getSelectTrecho($trecho, $sql, $nomeSelect = "trecho", $selecionavel = true)
    {
        if ($selecionavel == true) {
            echo "<select name='{$nomeSelect}' class='form-control'>";
            echo "<option value=''>TODOS</option>";
        } else {
            echo "<select name='{$nomeSelect}' class='form-control' required>";
            echo "<option value='' selected disabled>SELECIONE UM TRECHO</option>";
        }

        foreach ($sql as $dados => $value) {
            if ($trecho == $value['cod_trecho'])
                echo("<option value='{$value['cod_trecho']}' selected>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
            else
                echo("<option value='{$value['cod_trecho']}'>{$value["nome_trecho"]} - {$value['descricao_trecho']}</option>");
        }
        echo "</select>";
    }

    // Colocar o Select de Ponto Not�vel
    public static function getSelectPontoNotavel($pn, $sql)
    {
        echo "<select name='pn' class='form-control'>";
        echo "<option value=''>TODOS</option>";
        foreach ($sql as $dados => $value) {
            if ($pn == $value['cod_ponto_notavel'])
                echo("<option value='{$value['cod_ponto_notavel']}' selected>{$value["nome_ponto"]}</option>");
            else
                echo("<option value='{$value['cod_ponto_notavel']}'>{$value["nome_ponto"]}</option>");
        }
        echo "</select>";
    }

}
