<?php
/**
 * Gerencia Models, Controllers e Views
 *
 * @package SimgMVC
 * @since 0.1
 */
class SimgMVC {
	
	/**
	 * $controlador
	 *
	 * Receber� o valor do controlador (Vindo da URL).
	 * exemplo.com/controlador/
	 *
	 * @access private
	 */
	private $controlador;
	
	/**
	 * $acao
	 *
	 * Receber� o valor da a��o (Tamb�m vem da URL):
	 * exemplo.com/controlador/acao
	 *
	 * @access private
	 */
	private $acao;
	
	/**
	 * $parametros
	 *
	 * Receber� um array dos par�metros (Tamb�m vem da URL):
	 * exemplo.com/controlador/acao/param1/param2/param50
	 *
	 * @access private
	 */
	private $parametros;
	
	/**
	 * $not_found
	 *
	 * Caminho da p�gina n�o encontrada
	 *
	 * @access private
	 */
	private $not_found = '/includes/404.php';
	
	/**
	 * Construtor para essa classe
	 *
	 * Obt�m os valores do controlador, a��o e par�metros. Configura
	 * o controlado e a a��o (m�todo).
	 */
	public function __construct() {
		$this->get_url_data (); 													# Obt�m os valores do controlador, a��o e par�metros da URL. E configura as propriedades da classe.
		
		if (! $this->controlador) { 												# Verifica se o controlador existe. Caso contr�rio, adiciona o controlador padr�o 
																					# e chama o m�todo index().
			require_once ABSPATH . '/controllers/login-controller.php';				# Adiciona o controlador padr�o
			$this->controlador = new LoginController();								# 
			$this->controlador->index (); 											# Executa o m�todo index()
			return;
		}
		
		if (! file_exists ( ABSPATH	. '/controllers/'. $this->controlador
									. '.php' )) { 
			
			require_once ABSPATH . $this->not_found; 								# P�gina n�o encontrada
			return;
		}
		
		require_once ABSPATH . '/controllers/' 
					 . $this->controlador . '.php'; 								# Inclui o arquivo do controlador
		                                                                      
		$this->controlador = preg_replace ( '/[^a-zA-Z]/i', 						# Remove caracteres inv�lidos do nome do controlador para gerar o nome da classe.
						'', $this->controlador );
		
		
		if (! class_exists ( $this->controlador )) { 								# Se a classe do controlador indicado n�o existir, n�o faremos nada
			require_once ABSPATH . $this->not_found; 								# P�gina n�o encontrada
			return;
		}
		
		$this->controlador 	=	new $this->controlador ( $this->parametros ); 		# Cria o objeto da classe do controlador e envia os par�mentros
		$this->acao 		= 	preg_replace ( '/[^a-zA-Z]/i', '', $this->acao ); 	# Remove caracteres inv�lidos do nome da a��o (m�todo)
		
		if (method_exists ( $this->controlador, $this->acao )) { 					# Se o m�todo indicado existir, executa o m�todo e envia os par�metros
			$this->controlador->{$this->acao} ( $this->parametros );
			
			return;
		}
		
		if (! $this->acao && method_exists ( $this->controlador, 'index' )) { 		# Sem a��o, chamamos o m�todo index
			$this->controlador->index ( $this->parametros );
			
			return;
		}
		
		require_once ABSPATH . $this->not_found; 									# P�gina n�o encontrada
		
		return;
	}
	
	/**
	 * Obt�m par�metros de $_GET['path']
	 *
	 * Obt�m os par�metros de $_GET['path'] e configura as propriedades
	 * $this->controlador, $this->acao e $this->parametros
	 *
	 * A URL dever� ter o seguinte formato:
	 * http:#www.example.com/controlador/acao/parametro1/parametro2/etc...
	 */
	public function get_url_data() {
		
		if (isset ( $_GET ['path'] )) {											# Verifica se o par�metro path foi enviado

			$path = $_GET ['path'];												# Captura o valor de $_GET['path']

            $path = explode ( '#', $path );                                     # Limpeza de Ancoras de link <a>
            $path = $path[0];

			$path = rtrim ( $path, '/' );										# Limpa os dados
			$path = filter_var ( $path, FILTER_SANITIZE_URL );
			$path = explode ( '/', $path );										# Cria um array de par�metros

			$this->controlador = chk_array ( $path, 0 );						# Configura as propriedades
			$this->controlador .= '-controller';
			$this->acao = chk_array ( $path, 1 );
			#
			if (chk_array ( $path, 2 )) {										# Configura os par�metros
				unset ( $path [0] );
				unset ( $path [1] );
				$this->parametros = array_values ( $path );						# Os par�metros sempre vir�o ap�s a a��o
			}
			
			 # 	DEBUG
 			 #	echo $this->controlador . '<br>';
			 #	echo $this->acao . '<br>';
			 #	echo '<pre>'; 			
			 #	print_r( $this->parametros ); 		
			 # 	echo '</pre>';
		}
	}
}

?>