<?php
/**
 *
 * Manipula os dados de usu�rios, faz login e logout, verifica permiss�es e redireciona p�gina para usu�rios logados.
 *
 * @package SimgMVC
 * @since 0.1
 * 
 * Este c�digo � confidencial. 
 * A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 * 
 */
class UsuarioLogin {
	/**
	 * Vari�vel para verificar se o usu�rio est� logado.
	 * Recebe true em caso afirmativo.
	 *
	 * @public
	 *
	 * @access public
	 * @var bol
	 */
	public $logado;
	
	/**
	 * Vari�vel a qual recebe todos os dados do usu�rio na base de dados.
	 *
	 * @public
	 *
	 * @access public
	 * @var array
	 */
	public $dadosUsuario;
	
	/**
	 * Mensagem de erro para o formul�rio de login
	 *
	 * @public
	 *
	 * @access public
	 * @var string
	 */
	public $erroLogin;
	
	/**
	 * $loginLiberado
	 *
	 * Vari�vel para verificar se o usu�rio est� liberado ou n�o.
	 * Recebe true, caso positivo.
	 *
	 * @access public
	 * @var boolean
	 */
	public $usuarioLiberado;
	
	/**
	 * $nivelUsuario
	 *
	 * Vari�vel a qual recebe o n�vel do usu�rio oriundo da base de dados.
	 *
	 * @access public
	 * @var string
	 */
	public $nivelUsuario;
	
	
	/**
	 * Verifica o login
	 *
	 * Configura as propriedades $logado e $erroLogin. Tamb�m configura o array do usu�rio em $dadosUsuario
	 */
	public function checarUsuario() {

		if (isset ( $_SESSION ['dadosUsuario'] ) 											# Verifica se existe uma sess�o com a chave dadosUsuario. 
				&& ! empty ( $_SESSION ['dadosUsuario'] ) 									# E esse array n�o pode est� vazio, ou seja, ele tem que existir.
				&& is_array ( $_SESSION ['dadosUsuario'] ) 									# E dadosUsuarios tem que ser um array.
				&& ! isset ( $_POST ['dadosUsuario'] )) { 									# E esse array n�o pode ser passado via POST.
			
			$dadosUsuario 			= $_SESSION ['dadosUsuario'];							# Configura os dados do usu�rio.
			$dadosUsuario ['post'] 	= false;												# Garante que n�o � HTTP POST.
		}
		
		
		if (isset ( $_POST ['dadosUsuario'] )  												# Verifica se existe um $_POST com a chave dadosUsuario.
				&& ! empty ( $_POST ['dadosUsuario'] ) 										# E esse POST n�o est� vazio.
				&& is_array ( $_POST ['dadosUsuario'] )) {									# E o POST � um array.
			
			$dadosUsuario			= $_POST ['dadosUsuario'];								# Configura os dados do usu�rio.
			$dadosUsuario ['post'] 	= true;													# Garante que � HTTP POST.
		}
		
		if (! isset ( $dadosUsuario ) || ! is_array ( $dadosUsuario)) {						# Verifica se existe algum dado de usu�rio para conferir.
			$this->logout ();																# Desconfigura qualquer sess�o que possa existir sobre o usu�rio.
			return;
		}
		
		if ($dadosUsuario ['post'] === true) {												# Passa os dados do post para uma vari�vel.
			$post = true;
		} else {
			$post = false;
		}
		
		unset ( $dadosUsuario ['post'] );													# Remove a chave post do array userdata.
		
		
		if (empty ( $dadosUsuario )) {														# Verifica se existe algo a conferir.
			$this->logado 		= false;													# Em caso negativo, o Usu�rio n�o ser� logado.
			$this->erroLogin 	= "Nenhum dado foi passado.";								# � atribu�do o erro correspondente.
			$this->logout ();																# Desconfigura qualquer sess�o que possa existir sobre o usu�rio.
			return;
		}
		
		extract ( $dadosUsuario );															# Extrai vari�veis dos dados do usu�rio.
		
		if ( ! isset ( $usuario )															# Verifica se existe um usu�rio e senha. Os campos foram preenchidos? 
				||! isset ( $senha )) {
			$this->logado 		= false;													# Em caso negativo, o usu�rio n�o ser� logado.
			$this->erroLogin 	= "N�o h� informa��es suficientes.";						# � atribu�do o erro correspondente.
			$this->logout ();																# Desconfigura qualquer sess�o que possa existir sobre o usu�rio.
			return;
		}
		
		$query = $this->bancoDados->consulta ( "SELECT * FROM usuario WHERE "				# Verifica se o usu�rio existe na base de dados.
				."usuario = ? LIMIT 1" 	
				, array( $usuario ));
		
		
		if (! $query) {																		# Verifica a consulta. A consulta retornou com sucesso?
			$this->logado 		= false;													# Em caso negativo, o Usu�rio n�o ser� logado.
			$this->erroLogin 	= '[001] Erro interno. Favor notificar o setor de TI.';		# � atribu�do o erro correspondente.
			$this->logout ();																# Desconfigura qualquer sess�o que possa existir sobre o usu�rio.
			return;
		}
		
		$fetch 		 	= $query->fetch ( PDO::FETCH_ASSOC );								# Obt�m os dados da base de usu�rio.
		$userId 	 	= ( int ) $fetch ['cod_usuario'];									# Obt�m o ID do usu�rio.
		$usuario 		= ( string ) $fetch['usuario'];
		$loginAtivo  	= ( string ) $fetch ['permissao'];									# Obt�m a ativa��o do login.
		$nivelAcesso 	= ( string ) $fetch ['nivel'];
		
		if (empty ( $userId )) {															# Verifica se o ID existe. O usu�rio existe?
			$this->logado 		= false;													# Em caso negativo, o usu�rio n�o ser� logado.
			$this->erroLogin 	= 'Combina��o Usuario/Senha n�o existente.';				# � atribu�do o erro correspondente.
			$this->logout ();																# Desconfigura qualquer sess�o que possa existir sobre o usu�rio.
			return;
		}

		if ($loginAtivo == 'n' ){															# Verifica se o usu�rio est� ativo. O Usu�rio est� ativo?
			$this->logado 		= false;													# Em caso negativo, o usu�rio n�o ser� logado.
			$this->erroLogin 	= 'Usu�rio "'. $usuario .'" n�o est� ativado.<br>'
							.' Favor, notificar a TI.';										# � atribu�do o erro correspondente.
			$this->logout();																# Desconfigura qualquer sess�o que possa existir sobre o usu�rio
			return ;
		}
		
		if ($this->phpass->CheckPassword ( $senha, $fetch ['senha'] )) {				  	# Compara a senha do banco (sob hash) com a senha passada pelo usu�rio
			
			if (session_id () != $fetch ['id_sessao'] && ! $post) {							# Se for uma sess�o, compara as chaves de sess�o no banco e armazenada localmente
				$this->logado 		= false;												# Caso seja negativo, o usu�rio n�o ser� logado
				$this->erroLogin 	= 'ID de Sess�o inv�lida.'; 							# O erro correspondente � atribu�do
				$this->logout ();															# Desconfigura qualquer sess�o que possa existir sobre o usu�rio
				return;
			}

			if ($post) {																	# Se for um post
				session_regenerate_id ();													# Recria o ID da sess�o		
				$sessionId = session_id ();													# Atribuimos uma key para a sess�o
				$horaLogin = date('H:i:s - D, d M Y ');										# Atribu�mos o hor�rio local (hora/timezone do servidor) para o usu�rio.
																							# Obs.: Atualmente o servidor est� com o Timezone de Fortaleza.	
				$_SESSION ['dadosUsuario'] = $fetch;										# Envia os dados de usu�rio para a sess�o.
				$_SESSION ['dadosUsuario'] ['senha'] = $senha;								# Atualiza a senha.
				$_SESSION ['dadosUsuario'] ['id_sessao'] = $sessionId;						# Atualiza o ID da sess�o.
				
				$query = $this->bancoDados-> consulta ( 'UPDATE usuario SET '				# Atualiza o ID da sess�o na base de dados.
						.	'id_sessao = ?, ultimo_login = ? '
						.	'WHERE cod_usuario  = ?'
						, array ( $sessionId, $horaLogin, $userId) );
			}
			
			$_SESSION['dadosUsuario']['nivel'] 	= $fetch['nivel'];							# Obt�m um array com as permiss�es de usu�rio
			$this->logado 						= true;										# Configura a propriedade dizendo que o usu�rio est� logado
			$this->nivelUsuario 				= $fetch['nivel'];
			$this->dadosUsuario 				= $_SESSION['dadosUsuario'];
			

			
			return;
			
		} else {																		# Caso a senha passada pelo usu�rio n�o seja a mesma que est� salva na base de dados
			$this->logado = false;														# O usu�rio n�o est� logado
			$this->erroLogin = 'Senha inv�lida.';										# O erro correspodente � atribu�do
			$this->logout ();															# Desconfigura qualquer sess�o que possa existir sobre o usu�rio.
			return;
		}
	}

	/**
	 *
	 * Desconfigura tudo do usu�rio.
	 *
	 * @param bool $redirecionar
	 * $redirect Se verdadeiro, redireciona para a p�gina de login*
	 * Se verdadeiro, redireciona para a p�gina de login
	 * @final
	 */
	 protected function logout($redirecionar = false) {
		
		$_SESSION ['dadosUsuario'] = array ();										# Remove todos os dados de $_SESSION['userdata']
		unset ( $_SESSION ['dadosUsuario'] );										# Comando redudante, apenas para ter certeza que os dados foram apagados
		session_regenerate_id ();													# Regeramos o ID da sessao
		if ($redirecionar === true) {
			$this->redirecionarParaPaginaLogin ();									# Redirecionamos o usuario para a p�gina de login
		}
         session_unset();
	}
	
	/**
	 * Vai para a p�gina de login
	 */
	protected function redirecionarParaPaginaLogin() {
		
		if (defined ( 'HOME_URI' )) {												# Verifica se a URL da HOME est� configurada
			
			$login_uri = HOME_URI;													# Configura a URL de login
			
			$_SESSION ['goto_url'] = urlencode ( $_SERVER ['REQUEST_URI'] );		# A p�gina em que o usu�rio estava
			
			echo '<meta http-equiv="Refresh" content="0; url=' 
					. $login_uri . '">';											# Redireciona
			echo '<script type="text/javascript">window.location.href = "' 
					. $login_uri . '";</script>';
			#header('location: ' . $login_uri);
		}
		
		return;
	}
	
	/**
	 *
	 * Envia para uma p�gina qualquer
	 *
	 * @final
	 *
	 * @access protected
	 */
	final protected function vaiParaPagina($page_uri = null) {
		if (isset ( $_GET ['url'] ) 
				&& ! empty ( $_GET ['url'] ) 
				&& ! $page_uri) {
			
			$page_uri = urldecode ( $_GET ['url'] );						# Configura a URL
		}
		
		if ($page_uri) {
			# Redireciona
			echo( "<script type='text/javascript'>
						function ir(){
							window.location.href = '{$page_uri}';
						}
        			</script>");

			$novidade = new NovidadesModal();
			
			return $novidade->mostrarNovidades($_SESSION['dadosUsuario']['ultimo_login']);
		}
	}
	
	/**
	 * Verifica o nivel
	 *
	 * @param string $nivelNecessario
	 *        	O n�vel necess�rio
	 * @param array $nivelUsuario
	 *        	O n�vel do usu�rio
	 * @return False / True
	 *
	 */
	final protected function verificarNivel($nivelNecessario = 'any', $nivelUsuario = "any") {

        if(is_array($nivelNecessario)){
            if ( in_array($nivelUsuario , $nivelNecessario)) {					# Se o n�vel oriundo da base de dados n�o for identico ao do usuario
                return true;												# Retorna falso
            } else {														# Caso Contr�rio
                return false;												# Retorna verdadeiro
            }
        }

		if ($nivelNecessario == $nivelUsuario) {					# Se o n�vel oriundo da base de dados n�o for identico ao do usuario
            return true;												# Retorna falso
        } else {														# Caso Contr�rio
            return false;												# Retorna verdadeiro
        }
	}
}
?>