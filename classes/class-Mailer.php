<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require ABSPATH.'/vendor/autoload.php';

class Mailer
{
    private static $mail;

    public static function sendEmail($to, $bodyMail, $dados)
    {
      if(MAIL_ACTIVE)
        try {
            self::$mail = new PHPMailer(true);

            try{
              //Server settings
              if(MAIL_DEBUG)
                self::$mail->SMTPDebug = 4;                                 // Enable verbose debug output

              self::$mail->SMTPOptions = SMTP_CONF;                         //Seta configura��es de SMTP

              self::$mail->isSMTP();                                        // Send using SMTP
              self::$mail->Host       = SMTP_HOST;                          // Set the SMTP server to send through
              self::$mail->SMTPAuth   = true;                               // Enable SMTP authentication
              self::$mail->Username   = MAIL_USERNAME;                      // SMTP username
              self::$mail->Password   = MAIL_PASSWORD;                      // SMTP password
              self::$mail->SMTPSecure = 'PHPMailer::ENCRYPTION_STARTTLS';   // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
              self::$mail->Port       = SMTP_PORT;                          // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            }catch (Exception $e) {
                echo "Message could not be sent. Mailer Error:". self::$mail->ErrorInfo;
            }
            //Recipients
            self::$mail->setFrom(MAIL_USERNAME, 'Sistema SIMG');

            self::setAdress($to);
            self::$mail->addReplyTo('suporte.simg@metrofor.ce.gov.br');

            // Content
            self::$mail->isHTML(true);                                  // Set email format to HTML

            if(!self::getBodyMessage($bodyMail, $dados))
              throw new MyException('foo!');

            self::$mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error:" . self::$mail->ErrorInfo;
        }
    }

    private static function setAdress($address, $name="")
    {
        if(is_array($address))
        {
          $address = array_filter($address);
          foreach($address as $key=>$value)
          {
            self::$mail->addAddress($value);
          }
        }else{
          self::$mail->addAddress($address);
        }
    }

    private static function getBodyMessage($mailBody, $dados)
    {

      $path = ABSPATH."/views/mails/{$mailBody}.php";

      if(is_file($path))
      {
        //Envia imagens que ser�o utilizadas no corpo do e-mail
        self::$mail->AddEmbeddedImage(ABSPATH."/views/_images/metroservice_logo.png", "simgLogo");
        self::$mail->AddEmbeddedImage(ABSPATH."/views/_images/metrofor.png", "metroforLogo");

        //Cabe�alho e rodap� s�o definidos pelo sistema
        $header = "<img src='cid:metroforLogo' alt=''> <img src='cid:simgLogo' alt=''>";
        // $footer = "Assinatura";

        //Chama o arquivo com a informa��o da mensagem e assunto do e-mail
        require_once ($path);
        //Monta o corpo de e-mail
        $body = $header.$message.$footer;

        //Seta no e-mail as informa��es e retorna positivo.
        self::$mail->Subject  = $subject;
        self::$mail->Body     = $body;
        self::$mail->AltBody  = $AltBody;
        return true;
      }else
      {
        return false;
      }

    }
}

?>
