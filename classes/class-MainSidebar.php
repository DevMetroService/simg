<?php

/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 17/03/2017
 * Time: 09:53
 *
 * Classe que possui fun??es para a cria??o dos Dashboards do sistema, contendo as partes em comum de todas os usuarios
 */
class MainSidebar
{
    private $caminhoDashboard;
    private $nivel;
    private $usuario;
    private $home_uri = HOME_URI;

    public function printSidebar($usuario, $selecao)
    {
        $this->caminhoDashboard = "dashboard{$_SESSION['direcionamento']}";
        $this->nivel = $usuario['nivel'];
        $this->usuario = $usuario['usuario'];

        $permissao = $this->permissoesAcesso($this->nivel);

        //Verifica n�veis que j� se enquandram no novo processo de sidebar
        $medidaProvisoria = in_array($this->nivel, array(2, 2.1, 4, 5, 5.1, 5.2, 6.1, 6.2, 7, 7.1, 7.2)); // Niveis atendidos pela fun??o apagar apos 100% atendidos

        if ($medidaProvisoria) {
            echo '<div class="navbar-default sidebar navbar-fixed-top" role="navigation" style=" overflow-y: auto; height: 100vh">
                    <div class="sidebar-nav navbar-collapse collapse" aria-expanded="false">
                        <ul id="side-menu" class="nav">';

            if (in_array($this->nivel, array(5, 5.1, 5.2))) {
                $this->divisoria();
                $this->sidebarControleEquipe($selecao, $permissao);
            } else {
                if ($permissao['engenharia']) {
                    $this->divisoria();
                    $this->sidebarControleEngenharia($selecao);
                }

                if($permissao['engenhariaMr'])
                {
                    $this->sidebarControleEngenhariaMr();
                }

                if ($permissao['corretivas'] || $permissao['preventivas'] || $permissao['programacao'] || $permissao['equipe']) {
                    $this->divisoria();
                    $this->sidebarGerencia($selecao, $permissao);
                }

                if ($selecao == "pmp") {
                    $this->divisoria();
                    $this->sidebarGeradorPMP();
                }
            }

            if ($permissao['criarSaf'] && $permissao['criarSsp']) {
                $this->divisoria();
                $this->sidebarCriarSafSsp();
            } else if ($selecao == "corretiva" && $permissao['criarSaf']) {
                $this->divisoria();
                $this->sidebarCriarSaf();
            }else if($permissao['criarSaf']){
                $this->sidebarCriarSaf();
            }

            if($permissao['gerenciamento']['crud'])
            {
                $this->divisoria();
                $this->sidebarCrud();   
            }

            if ($permissao['gerenciamento']['avaria']) {
                $this->divisoria();
                $this->sidebarCrudTabelas();
            }

            if($permissao['gerenciamento']['userCRUD'])
            {
                $this->userCRUD();
            }

            if($permissao['material'])
              $this->sidebarMaterial();

            $this->divisoria(); // Pesquisar
            $this->sidebarPesquisa();

            $this->divisoria(); // Relat?rios
            if($permissao['relatorio'])
              $this->sidebarRelatorio($permissao);

            echo '          </ul>
                        </div>
                </div>';
        }


        $this->modalPesquisar($permissao);
        $this->modalFormularios($permissao);
        $this->modalIndicadoresdeDesempenho($this->nivel);
        $this->modalRelatoriosFalha($this->nivel);
        $this->modalAlterarSenha($this->usuario);

        return $medidaProvisoria; // Deletar apos fun??o estar 100% em todos os niveis
    }

    public function permissoesAcesso($nivel)
    {
        $permissoes['nivel'] = $nivel;

        $permissoes['material'] = false;

        $permissoes['diretoria'] = false;

        $permissoes['relatorio'] = true;

        $permissoes['engenhariaMr'] = false;

        $permissoes['engenharia'] = false;
        $permissoes['criarSaf'] = true;
        $permissoes['criarSsp'] = false;

        $permissoes['gerenciamento']['avaria'] = false;
        $permissoes['gerenciamento']['userCRUD'] = false;

        $permissoes['corretivas'] = false;
        $permissoes['saf'] = true;
        $permissoes['ssm'] = true;
        $permissoes['osm'] = true;

        $permissoes['preventivas'] = false;
        $permissoes['cronograma'] = true;
        $permissoes['ssmp'] = true;
        $permissoes['osmp'] = true;

        $permissoes['programacao'] = false;
        $permissoes['ssp'] = true;
        $permissoes['osp'] = true;

        switch ($nivel) {
            case 1://   RH
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                break;

            case 2://   ccm
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;

                $permissoes['criarSsp'] = true;
                break;

            case 2.1:// supervisao
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                break;

            case 2.3:// Usuario
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                break;

            case 3://   TI
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                break;

            case 4://   Materiais
                $permissoes['corretivas'] = false;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['criarSaf'] = false;
                $permissoes['relatorio'] = false;
                $permissoes['material'] = true;
                break;

            case 5://   Equipe
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                $permissoes['saf'] = true;
                $permissoes['criarSsp'] = true;
                break;

            case 5.1:// Equipe Material Rodante
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                $permissoes['saf'] = true;
                $permissoes['criarSsp'] = true;
                $permissoes['gerenciamento']['avaria'] = true;
                break;

            case 5.2:// Equipe Material Rodante
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;

                $permissoes['criarSsp'] = true;
                break;

            case 6://   Diretoria
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                break;

            case 6.1:// GESIV
                $permissoes['gerenciamento']['crud'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                $permissoes['relatorioFormulario'] = true;
                break;

            case 6.2:// ASTIG
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['pesquisaProgramacao'] = true;
                $permissoes['relatorioFormulario'] = true;
                $permissoes['gerenciamento']['userCRUD'] = true;
                
                break;

            case 7://   Engenharia
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;

                $permissoes['engenharia'] = true;
                $permissoes['gerenciamento']['avaria'] = true;
                break;

            case '7.1':// EngenhariaSupervisao
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['preventivas'] = true;
                $permissoes['pesquisaPreventiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;

                $permissoes['engenharia'] = true;
                $permissoes['gerenciamento']['avaria'] = true;
                break;

            case '7.2':// EngenhariaMaterialRodante
                $permissoes['engenhariaMr'] = true;
                $permissoes['equipe'] = true;
                $permissoes['corretivas'] = true;
                $permissoes['pesquisaCorretiva'] = true;
                $permissoes['programacao'] = true;
                $permissoes['pesquisaProgramacao'] = true;

                $permissoes['gerenciamento']['avaria'] = true;
                break;
        }

        return $permissoes;
    }

    private function sidebarControleEquipe($selecao, $permissao)
    {
        echo '<li>
                <a class="styleLink">
                    <i class="fa fa-dashboard fa-fw"></i> Equipe de Manuten��o
                </a>
                <ul class="nav nav-second-level collapse in">';

        $criaSaf = false;

        $texto="Corretiva e Programa��o";

        if ($permissao['programacao']) {
            if (in_array($this->nivel, array(5))) {
                if ($selecao == "preventiva") {
                    echo '<li style="background-color: #cacaca">
                        <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/preventiva">Controle de PMP</a>
                      </li>';
                } else {
                    echo '<li>
                        <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/preventiva">Controle de PMP</a>
                      </li>';
                }
            }
        }

        if ($selecao == "correProg") {
            echo "<li style='background-color: #cacaca'>
                     <a href='{$this->home_uri}/{$this->caminhoDashboard}/correProg'>{$texto}</a>
                    </li>";
        } else {
            echo "<li>
                     <a href='{$this->home_uri}/{$this->caminhoDashboard}/correProg'>{$texto}</a>
                    </li>";
        }

        echo '</ul></li>';

        if($criaSaf){
            $this->divisoria();
            $this->sidebarCriarSaf();
        }

        if (in_array($this->nivel, array(5.1))) {
            $this->divisoria();

            echo '<li>
                    <a class="styleLink">
                        <i class="fa fa-dashboard fa-fw"></i> Material Rodante
                    </a>
                    <ul class="nav nav-second-level collapse in">
                        <li><a href="' . $this->home_uri . '/dashboardGeral/configComposicao">Configura��o de Composi��o</a></li>
                    </ul>
                </li>';

            $this->divisoria(); // Pesquisar
            $this->sidebarMR();

        }
    }

    private function sidebarControleEngenharia($selecao)
    {
        echo '<li>
                <a class="styleLink">
                    <i class="fa fa-dashboard fa-fw"></i> Controle Engenharia
                </a>
                <ul class="nav nav-second-level collapse out">';

        if ($selecao == "cronograma") {
            echo '<li style="background-color: #cacaca">
                    <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/cronograma">Cronograma</a>
                  </li>';
        } else {
            echo '<li>
                    <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/cronograma">Cronograma</a>
                  </li>';
        }

        if ($selecao == "pmp") {
            echo '<li style="background-color: #cacaca">
                     <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/pmp">PMP</a>
                    </li>';
        } else {
            echo '<li>
                     <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/pmp">PMP</a>
                   </li>';
        }

        echo '<li style="background-color: #cacaca">
                    <a href="' . $this->home_uri . '/usuario/create">Cadastro Usu�rios</a>
              </li>';

        echo '</ul></li>';
    }

    private function userCRUD()
    {
        echo "<li style='background-color: #cacaca'>
                    <a href='{$this->home_uri}/usuario/create'>Cadastro Usu�rios</a>
              </li>";

    }

    private function sidebarControleEngenhariaMr(){
        $this->divisoria();

        echo '<li>
                <a class="styleLink">
                    <i class="fa fa-dashboard fa-fw"></i> Material Rodante
                </a>
                <ul class="nav nav-second-level collapse in">
                    <li><a href="' . $this->home_uri . '/dashboardGeral/configComposicao">Configura��o de Composi��o</a></li>
                    <li><a href="' . $this->home_uri . '/usuario/create">Cadastro Usu�rios</a></li>
                </ul>
            </li>';

        $this->divisoria(); // Pesquisar
        $this->sidebarMR();

    }

    private function sidebarGerencia($selecao, $permissao)
    {
        echo '<li>
                <a class="styleLink">
                    <i class="fa fa-dashboard fa-fw"></i> Controle Ger�ncia
                </a>
                <ul class="nav nav-second-level collapse out">';

        if ($permissao['corretivas'] && $this->nivel != '7.2') {
            if ($selecao == "corretiva") {
                echo '<li style="background-color: #cacaca">
                      <a href="' . $this->home_uri . "/". $this->caminhoDashboard . '/corretiva">Corretiva</a>
                  </li>';
            } else {
                echo '<li>
                      <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/corretiva">Corretiva</a>
                  </li>';
            }
        }

        if ($permissao['programacao'] && $this->nivel != '7.2') {
            if ($selecao == "programacao") {
                echo '<li style="background-color: #cacaca">
                    <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/programacao">Programa��o</a>
                </li>';
            } else {
                echo '<li>
                    <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/programacao">Programa��o</a>
                </li>';
            }
        }

        if ($permissao['preventivas']) {
            if ($selecao == "preventiva") {
                echo '<li style="background-color: #cacaca">
                    <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/preventiva">Preventiva</a>
                </li>';
            } else {
                echo '<li>
                    <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/preventiva">Preventiva</a>
                </li>';
            }
        }

        if($permissao['equipe'])
        {
          if ($selecao == "equipe") {
              echo '<li style="background-color: #cacaca">
                  <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/equipe">Manuten��o</a>
              </li>';
          } else {
              echo '<li>
                  <a href="' . $this->home_uri . "/".$this->caminhoDashboard . '/equipe">Manuten��o</a>
              </li>';
          }
        }

        echo '</ul></li>';
    }

    private function sidebarGeradorPMP()
    {
        echo <<<HTML
<li>
    <a class="styleLink">
        <i class="fa fa-calendar-plus-o"></i> Gerador PMP
    </a>
    <ul class="nav nav-second-level collapse in">
        <li>
            <a href="#">
                <i class="fa fa-university fa-fw"></i> Edifica��es<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/edificacao">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}//dashboardPmp/procedimento/E">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa icon-iconRa fa-fw"></i> Rede A�rea<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/redeAerea">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}//dashboardPmp/procedimento/R">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-bolt fa-fw"></i> Subesta��o<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/subestacao">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}//dashboardPmp/procedimento/S">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-road fa-fw"></i> Via Permanente<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/viaPermanente">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}//dashboardPmp/procedimento/V">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-phone fa-fw"></i> Telecom<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/telecom">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}//dashboardPmp/procedimento/T">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-ticket fa-fw"></i> Bilhetagem<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/bilhetagem">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}/dashboardPmp/procedimento/B">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-arrow-up fa-fw"></i> Transportes Verticais<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/transportes">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}/dashboardPmp/procedimento/Tv">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-pagelines fa-fw"></i> Jardins e �reas Verdes<span class="fa arrow"></span>
            </a>
            <ul class="nav nav-second-level collapse" style="text-indent: 10%">
                <li>
                    <a href="{$this->home_uri}/pmp/jardins">Criar Item PMP</a>
                </li>
                <li>
                    <a href="{$this->home_uri}/dashboardPmp/procedimento/J">Cadastro de Procedimento</a>
                </li>
            </ul>
        </li>
HTML;

        echo <<<HTML
    </ul>
</li>
HTML;
    }

    private function sidebarCrudTabelas () {
        echo '<li>
                    <a class="styleLink">
                        <i class="fa fa-dashboard fa-fw"></i> Inserir Dados
                    </a>
                    <ul class="nav nav-second-level collapse out">
                        <li><a href="' . $this->home_uri . '/cadastroCampos/avaria">Avaria</a></li>
                    </ul>
                    <ul class="nav nav-second-level collapse out">
                        <li><a href="' . $this->home_uri . '/cadastroCampos/localGrupo">Local Grupo de Sistema</a></li>
                    </ul>
                </li>';
    }

    private function sidebarCrud () {
        echo '<li>
            <a class="styleLink">
                <i class="fa fa-dashboard fa-fw"></i> Inserir Dados
            </a>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/amv/create">Cadastro de AMVs</a></li>
                <li><a href="'. $this->home_uri . '/servicoPmp/create">Cadastro de Servi�os de PMP</a></li>
            </ul>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/linha/create">Cadastro de Linhas</a></li>
            </ul>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/procedimento/create">Cadastro de Procedimentos</a></li>
            </ul>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/pontoNotavel/create">Cadastro de Pontos Not�veis</a></li>
            </ul>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/estacao/create">Cadastro de Esta��es</a></li>
            </ul>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/local/create">Cadastro de Locais</a></li>
            </ul>
            <ul class="nav nav-second-level collapse out">
                <li><a href="'. $this->home_uri . '/material/create">Cadastro de Materiais</a></li>
            </ul>
        </li>';
    }

    private function sidebarCriarSaf()
    {
        $cancelSaf =  "";
        if($this->nivel == 2.1) {
            $cancelSaf = '<ul class="nav nav-second-level collapse">
                            <li><a href="' . $this->home_uri . '/saf/cancelSaf">Cancelar</a></li>
                        </ul>';
        }
        
        echo '<li>
                    <a href="#"> <i class="fa fa-edit fa-fw">
                        </i> Solicita��o de Abertura de Falha<span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="' . $this->home_uri . '/saf/create">Criar</a></li>
                    </ul>
                    '.$cancelSaf.'
                </li>';
    }

    private function sidebarCriarSafSsp()
    {
        echo '  <li>
                    <a href="#"> <i class="fa fa-edit fa-fw">
                        </i> Criar<span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="' . $this->home_uri . '/saf/create">SAF</a></li>
                        <li><a href="' . $this->home_uri . '/dashboardGeral/ssp">SSP</a></li>
                    </ul>
                </li>';
    }

    private function sidebarPesquisa()
    {
        echo '<li>
                    <a href="#" data-toggle="modal" data-target="#modalPesquisas">
                        <i class="fa fa-search fa-fw"></i> Pesquisar</span>
                    </a>
                </li>';
    }

    private function sidebarMR(){

        echo ($this->nivel) != '5.1' ? "<li>
                <a href='{$this->home_uri}//dashboardEngenhariaMr/cadastroMateriais'><i class='fa fa-list-ol fa-fw'></i> Cadastro Materiais</a>
            </li>" : "";

        echo "<li>
                <a href='#'>
                    <i class='fa fa-list-ol fa-fw'></i> Registros Gerais<span class='fa arrow'>
                </a>

                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}//dashboardGeral/registroDisponibilidade'>Registro Disponibilidade</a></li>
                </ul>
                 <ul class='nav nav-second-level collapse'>
                    <li>
                        <a href='#'>
                            <i class='fa fa-file fa-fw'></i>Registro Od�metro<span class='fa arrow'>
                        </a>
                       <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}//dashboardGeral/registroOdometroTue'> - TUE</a></li>
                        </ul>
                        <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}//dashboardGeral/registroOdometroVlt'> - VLT</a></li>
                        </ul>
                    </li>
                </ul>


                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/registroFrota'>Registro de Frota</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/registroProcedimento'>Registro de Procedimento</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/registroFicha'>Registro de Ficha</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li>
                        <a href='#'>
                            <i class='fa fa-train fa-fw'></i>Disponibilidade Di�rias<span class='fa arrow'>
                        </a>
                        <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/DisponibilidadeMr/5'>Linha Sul</a> </li>
                        </ul>
                        <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/DisponibilidadeMr/1'>Linha Oeste</a> </li>
                        </ul>
                        <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/DisponibilidadeMr/2'>Linha Cariri</a> </li>
                        </ul>
                        <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/DisponibilidadeMr/7'>Linha Sobral</a> </li>
                        </ul>
                        <ul class='nav nav-third-level collapse'>
                            <li><a href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/DisponibilidadeMr/6'>Linha Parangaba-Mucuripe</a> </li>
                        </ul>
                    </li>
                </ul>

            </li>";

        echo "<li>
                <a href='#'>
                    <i class='fa fa-list-ol fa-fw'></i> Consultas Gerais<span class='fa arrow'>
                </a>

                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/consultaISMRTUE'>Consulta IS TUE</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/consultaISMRVLT'>Consulta IS VLT</a></li>
                </ul>
                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/consultaOcorrencia/2019/*/*/*/*/*'>Consulta Por Frota/Veiculo</a></li>
                </ul>

                <ul class='nav nav-second-level collapse'>
                    <li><a href='{$this->home_uri}/dashboardGeral/consultaOcorrenciaSistema/2019'>Consulta Por Frota/Sistema</a></li>
                </ul>

            </li>";

    }

    private function sidebarMaterial()
    {
      echo("
        <li>
            <a href='#'> <i class='fa fa-edit fa-fw'></i> Cadastrar<span class='fa arrow'></span></a>

            <ul class='nav nav-second-level collapse'>
    					<li><a href='{$this->home_uri}/material/create'>Material</a></li>
    					<li><a href='{$this->home_uri}/fornecedor/create'>Fornecedor</a></li>
    					<li><a href='{$this->home_uri}/categoria/create'>Categoria</a></li>
    				</ul>
        </li>
          ");
    }

    private function sidebarRelatorio($permissoes)
    {
        echo '<li>
                    <a href="#">
                        <i class="fa fa-pie-chart fa-fw"></i> Relat�rios<span class="fa arrow">
                    </a>

                    <ul class="nav nav-second-level collapse">
                        <li><a href="' . $this->home_uri . '/dashboardGeral/relatorio">Relat�rios de Controle</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modalRelatorioPesquisas"> Relat�rio de Formul�rios</a></li>
                        <li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/GeradorRelatorio"> Gerador de Relat�rio</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modalRelatorioFalha"> Relat�rios de Falha</a></li>
                        <li><a href="' . $this->home_uri . '/report/solicitacaoAlterNivelSSM"> Hist�rico de Solicita��o de Altera��o de N�vel da SSM</a></li>';

        //Para Material Rodante
        if(in_array($permissoes['nivel'], array(5.1, 7.2))){
            echo '<li><a href="#" data-toggle="modal" data-target="#modalRelatoriosContratuais"> Indicadores de Desempenho</a></li>';
            echo '<li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/KmRodadosPorVeiculo">Km Rodados por Ve�culo</a></li>';
            echo '<li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosKmRodadosPorVeiculo">Dados Km Rodados por Ve�culo</a></li>
                      <li><a href="' . $this->home_uri . '/report/indicadorRetrabalhoMR"> Indicador de Retrabalho MR</a></li>';
        }else{
          echo "<li><a href='{$this->home_uri}/report/indicadorRetrabalho'> Indicador de Retrabalho</a></li>";
        }

        //Engenharia e Supervis�o
        if (in_array($permissoes['nivel'], array(2.1, 7, 7.1, 6.1))) {
            echo '
                        <li><a href="#" data-toggle="modal" data-target="#modalRelatoriosContratuais"> Indicadores de Desempenho</a></li>
                        <li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/CronogramaSimples"> Cronograma Simples</a></li>
                        <li><a href="' . $this->home_uri . '/dashboardGeral/controleEstoque"> Controle de Estoque</a></li>

            <li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/PmpCronograma"> Pmp/Cronograma por servi�o</a></li>';
//            href="' . $this->home_uri .  'dashboardGeral/relatoriosDiversos/PreventivaCorretiva"
        } else if (in_array($permissoes['nivel'], array(2))) {
            echo '<li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/Corretivas60Dias"> SAF\'s acima de 60 dias</a></li>
                  <li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/HistoricoSaf"> Hist�rico da SAF</a></li>';
        }

        //Equipe de Manuten��o e GESIV
        if (in_array($permissoes['nivel'], array(5, 6.1))) {
            echo '<li><a href="#" data-toggle="modal" data-target="#modalRelatoriosContratuais"> Indicadores de Desempenho</a></li>';
        }

        echo '          <li><a href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/HistoricoStatus"> Hist�rico de Formul�rios</a></li>
                    </ul>
                </li>';

        echo ('<div style="width: 100%; height: 80px"></div>');
    }

    private function modalPesquisar($permissoes)
    {
        echo '<div class="modal fade" id="modalPesquisas" tabindex="-1" role="form" aria-labelledby="modalPesquisas" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title"><i class="fa fa-search fa-fw iconNavegador"></i> Pesquisas</h4>
                        </div>
                        <div class="modal-body">
                            <div class="list-group">';

        if ($permissoes['pesquisaCorretiva']) {
            echo '                  <a class="list-group-item list-group-item-info">
                                        <label>Corretivas</label>
                                    </a>
                                    <div class="row">
                                        <div class="col-md-12" >';

            if ($permissoes['saf']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaSaf">SAF</a>';
            }
            if ($permissoes['ssm']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaSsm">SSM</a>';
            }
            if ($permissoes['osm']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaOsm">OSM</a>';
            }

            echo '                      </div>
                                    </div>

                                    <div style="margin:5%"></div>';
        }

        if ($permissoes['pesquisaPreventiva']) {
            echo '                  <a class="list-group-item list-group-item-info">
                                        <label>Preventivas</label>
                                    </a>
                                    <div class="row">
                                        <div class="col-md-12" >';

            if ($permissoes['cronograma']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaCronograma">Cronograma</a>';
            }
            if ($permissoes['ssmp']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaSsmp">SSMP</a>';
            }
            if ($permissoes['osmp']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaOsmp">OSMP</a>';
            }

            echo '                      </div>
                                    </div>

                                    <div style="margin:5%"></div>';
        }

        if ($permissoes['pesquisaProgramacao']) {
            echo '                  <a class="list-group-item list-group-item-info">
                                        <label>Programa��o</label>
                                    </a>
                                    <div class="row">
                                        <div class="col-md-12" >';

            if ($permissoes['ssp']) {
                echo '<a class="col-md-offset-4 col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaSsp">SSP</a>';
            }
            if ($permissoes['osp']) {
                echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/pesquisaOsp">OSP</a>';
            }

            echo '                      </div>
                                    </div>
                                    <div style="margin:5%"></div>';
        }

        if (in_array($permissoes['nivel'], array(7, 7.1))) {
            echo <<<HTML
                                    <a class="list-group-item list-group-item-info">
                                        <label>PMP</label>
                                    </a>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <a class="col-md-4 btn btn-default" href="{$this->home_uri}/dashboardPmp/pesquisaEdificacao">Edifica��es</a>
                                            <a class="col-md-4 btn btn-default" href="{$this->home_uri}/dashboardPmp/pesquisaSubestacao">Subesta��o</a>
                                            <a class="col-md-4 btn btn-default" href="{$this->home_uri}/dashboardPmp/pesquisaViaPermanente">Via Permanente</a>
                                            <a class="col-md-4 btn btn-default" href="{$this->home_uri}/dashboardPmp/pesquisaRedeAerea">Rede A�rea</a>
                                            <a class="col-md-4 btn btn-default" href="{$this->home_uri}/dashboardPmp/pesquisaTelecom">Telecom</a>
                                            <a class="col-md-4 btn btn-default" href="{$this->home_uri}/dashboardPmp/pesquisaBilhetagem">Bilhetagem</a>
                                        </div>
                                    </div>

                                    <div style="margin:5%"></div>
HTML;
        }

        echo '                  </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }

    private function modalFormularios($permissoes)
    {
        echo '<div class="modal fade" id="modalRelatorioPesquisas" tabindex="-1" role="form" aria-labelledby="modalPesquisas" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title"><i class="fa fa-search fa-fw iconNavegador"></i>Relat�rio de Formul�rios</h4>
                        </div>
                        <div class="modal-body">
                            <div class="list-group">';
        if($permissoes['relatorioFormulario'])
        {
            echo '<a class="list-group-item list-group-item-info">
                    <label>Corretivas</label>
                </a>
                <div class="row">
                    <div class="col-md-12" >
                        <a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSaf">SAF</a>
                        <a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSsm">SSM</a>
                        <a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosOsm">OSM</a>
                    </div>
                </div>
                <div style="margin:5%"></div>
                <a class="list-group-item list-group-item-info">
                    <label>Preventivas</label>
                </a>
                <div class="row">
                    <div class="col-md-12" >
                        <a class=" col-md-offset-4 col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSsmp">SSMP</a>
                        <a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosOsmp">OSMP</a>
                    </div>
                </div>
                <div style="margin:5%"></div>
                <a class="list-group-item list-group-item-info">
                    <label>Programa��o</label>
                </a>
                <div class="row">
                    <div class="col-md-12" >
                        <a class="col-md-offset-4 col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSsp">SSP</a>
                        <a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosOsp">OSP</a>
                    </div>
                </div>
                <div style="margin:5%"></div>';
        }else{
            if ($permissoes['corretivas']) {
                echo '                  <a class="list-group-item list-group-item-info">
                                            <label>Corretivas</label>
                                        </a>
                                        <div class="row">
                                            <div class="col-md-12" >';

                if ($permissoes['saf']) {
                    echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSaf">SAF</a>';
                }
                if ($permissoes['ssm']) {
                    echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSsm">SSM</a>';
                }
                if ($permissoes['osm']) {
                    echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosOsm">OSM</a>';
                }

                echo '                      </div>
                                        </div>

                                        <div style="margin:5%"></div>';
            }

            if ($permissoes['preventivas']) {
                echo '                  <a class="list-group-item list-group-item-info">
                                            <label>Preventivas</label>
                                        </a>
                                        <div class="row">
                                            <div class="col-md-12" >';
                if ($permissoes['ssmp']) {
                    echo '<a class=" col-md-offset-4 col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSsmp">SSMP</a>';
                }
                if ($permissoes['osmp']) {
                    echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosOsmp">OSMP</a>';
                }
                echo '                      </div>
                                        </div>

                                        <div style="margin:5%"></div>';
            }

            if ($permissoes['programacao']) {
                echo '                  <a class="list-group-item list-group-item-info">
                                            <label>Programa��o</label>
                                        </a>
                                        <div class="row">
                                            <div class="col-md-12" >';

                if ($permissoes['ssp']) {
                    echo '<a class="col-md-offset-4 col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosSsp">SSP</a>';
                }
                if ($permissoes['osp']) {
                    echo '<a class="col-md-4 btn btn-default" href="' . $this->home_uri . '/dashboardGeral/relatoriosDiversos/DadosOsp">OSP</a>';
                }

                echo '                      </div>
                                        </div>
                                        <div style="margin:5%"></div>';
            }
        }
        echo '                  </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }

    private function modalIndicadoresdeDesempenho($nivel)
    {
        if (in_array($nivel, array(5.1, 7.2))) {
            $linkRelPn = "
                <a class='list-group-item' href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/MTBFMR'>
                    <strong>MTBF FROTA</strong><br />M�dia de Tempo Entre Falhas
                </a>
                <a class='list-group-item list-group-item-info' href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/MKBFFrota'>
                    <strong>MKBF FROTA</strong><br />M�dia de Quilometragem Entre Falhas das Frotas
                </a>
                <a class='list-group-item' href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/APPTUE'>
                        <label>APP TUE</label><br />Aproveitamento de Programa��es Preventivas TUE
                </a>
                <a class='list-group-item list-group-item-info' href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/APPVLT'>
                        <label>APP VLT</label><br />Aproveitamento de Programa��es Preventivas VLT
                </a>
            ";
        }else{
            $linkRelPn = "
            <a class='list-group-item' href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/APP'>
                <label>APP</label><br />Aproveitamento de Programa��es Preventivas
            </a>
            ";
        }

        echo <<<HTML
<div class="modal fade" id="modalRelatoriosContratuais" tabindex="-1" role="form" aria-labelledby="modalRelatoriosContratuais" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-pie-chart fa-fw iconNavegador"></i> Indicadores de Desempenho {$nivel}</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/AcompanhamentoAnual">
                        <label>Acompanhamento Anual</label>
                    </a>
                    <a class="list-group-item list-group-item-info" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/AproveitamentoOS">
                        <label>Aproveitamento de Ordens de Servi�os</label>
                    </a>
                    <a class="list-group-item"  href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/IndiceDeRetrabalho">
                        <label>�ndice de Retrabalho</label>
                    </a>
                    <a class="list-group-item list-group-item-info" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/DisponibilidadeMRMensal">
                        <label>Disponibilidade Material Rodante Mensal</label>
                    </a>
                    {$linkRelPn}
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
//        href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/CorretivasAbertasEncerradas"
//        href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/PreventivasAbertasEncerradas"

//        <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/APPMR">
//                        <label>APP - MR</label><br />Aproveitamento de Programa��es Preventivas de Material Rodante
//    </a>
    }

    public function modalRelatoriosFalha($nivel)
    {
        if (in_array($nivel, array(7, 7.1))) {
            $linkRelPn = "
                   <a class='list-group-item list-group-item-info' href='{$this->home_uri}/dashboardGeral/relatoriosDiversos/PassagemNivel'>
                        <strong>Atua��o Passagem de N�vel</strong><br>Relat�rio de Gr�fico de Causas em atua��es de passagem de n�vel.
                    </a>
            ";
        }else{
            $linkRelPn = "";
        }
        echo <<<HTML
<div class="modal fade" id="modalRelatorioFalha" tabindex="-1" role="form" aria-labelledby="modalRelatoriosContratuais" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-pie-chart fa-fw iconNavegador"></i> Relat�rio de Falhas</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/Corretivas07Dias">
                        <strong>Relat�rio de SAF's Acima de 07 Dias</strong>
                    </a>
                    <a class="list-group-item list-group-item-info" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/HistoricoSaf">
                        <strong>Hist�rico Saf</strong>
                    </a>
                    <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/FalhasNaoConstatadas">
                        <strong>Gr�fico de Falhas N�o Constatada</strong>
                    </a>
                    <a class="list-group-item list-group-item-info" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/Corretivas60Dias">
                        <strong>SAF's 60 dias</strong><br />Relat�rio de SAF's Acima de 60 Dias
                    </a>
                    <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/MTTR">
                        <strong>MTTR</strong><br />Tempo M�dio de Reparo
                    </a>
                    <a class="list-group-item list-group-item-info" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/TML">
                        <strong>TML</strong><br />Tempo M�dio de Libera��o
                    </a>
                    <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/MTBF">
                        <strong>MTBF</strong><br />M�dia de Tempo Entre Falhas
                    </a>
                    <a class="list-group-item" href="{$this->home_uri}/dashboardGeral/relatoriosDiversos/ValidacaoSSM">
                        <strong>Relatorio de Validacao SSM</strong>
                    </a>
                    {$linkRelPn}
                </div>
            </div>
        </div>
    </div>
</div>
HTML;

    }

    private function modalAlterarSenha($usuario)
    {
        echo '<div class="modal fade" id="alteraSenhaModal" tabindex="-1" role="form" aria-labelledby="alteraSenhaModal" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <h4 class="modal-title" id="motivoLabel">Alterar Senha</h4>
                        </div>

                        <form class="form-horizontal formAlterarSenha" method="post">

                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="senhaAntiga" class="col-sm-3 control-label">Senha Antiga</label>
                                    <div class="col-sm-9">
                                        <input required type="password" class="form-control" name="senhaAntiga" id="senhaAntiga" placeholder="Digite sua Senha Atual">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="novaSenha" class="col-sm-3 control-label">Nova Senha</label>
                                    <div class="col-sm-9">
                                        <input required type="password" class="form-control" name="novaSenha" id="novaSenha" placeholder="Digite sua nova Senha">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confSenha" class="col-sm-3 control-label">Confirmar Nova Senha</label>
                                    <div class="col-sm-9">
                                        <input required type="password" class="form-control" name="confirmarSenha" id="confSenha" placeholder="Confirme sua nova Senha">
                                    </div>
                                </div>

                                <input type="hidden" value="' . $usuario . '" name="nomeUsuario">
                                <p id="erro"></p>

                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success btn-lg" aria-label="right align" title="Alterar">
                                    <i class="fa fa-check fa-2x"></i>
                                </button>

                                <button type="button" class="btn btn-danger btn-lg" aria-label="right align" data-dismiss="modal" title="Cancelar">
                                    <i class="fa fa-times fa-2x"></i>
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>';
    }

    private function divisoria()
    {
        echo '<li class="nav-divider"></li>';
    }
}
