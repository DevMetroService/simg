<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 17/03/2017
 * Time: 11:25
 *
 * Armazenamentos de Sqls pre-praparados
 */

class Sql{
    public static function getCronogramaSql($tipoCron, $array){
        switch ($tipoCron){
            case 'Ed':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "nome_local");

                $sql['sql'] = Sql::cronogramaEdificacao($array);
                $sql['siglaGrupo'] = 'ed';
                $sql['nomeGrupo']  = 'Edifica��o';
                $sql['nameLocal']  = 'Local';
                $sql['codGrupo']   = 21;
                break;
            case 'Su':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "nome_local");

                $sql['sql'] = Sql::cronogramaSubestacao($array);
                $sql['siglaGrupo'] = 'su';
                $sql['nomeGrupo']  = 'Subesta��o';
                $sql['nameLocal']  = 'Local';
                $sql['codGrupo']   = 25;
                break;
            case 'Vp':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "ei.nome_estacao || ' - ' || ef.nome_estacao AS nome_local");

                $sql['sql'] = Sql::cronogramaViaPermanente($array);
                $sql['siglaGrupo'] = 'vp';
                $sql['nomeGrupo']  = 'Via Permanente';
                $sql['nameLocal']  = 'Esta��o Inicial / Final';
                $sql['codGrupo']   = 24;
                break;
            case 'Ra':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "nome_local", "nome_poste");

                $sql['sql'] = Sql::cronogramaRedeAerea($array);
                $sql['siglaGrupo'] = 'ra';
                $sql['nomeGrupo']  = 'Rede A�rea';
                $sql['nameLocal']  = 'Local / Poste';
                $sql['codGrupo']   = 20;
                break;
            case 'Vlt':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "nome_veiculo AS nome_local");

                $sql['sql'] = Sql::cronogramaVLT($array);
                $sql['siglaGrupo'] = 'vlt';
                $sql['nomeGrupo']  = 'Material Rodante VLT';
                $sql['nameLocal']  = 'Veiculo';
                $sql['codGrupo']   = 22;
                break;
            case 'Tl':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "nome_local");

                $sql['sql'] = Sql::cronogramaTelecom($array);
                $sql['siglaGrupo'] = 'tl';
                $sql['nomeGrupo']  = 'Telecom';
                $sql['nameLocal']  = 'Local';
                $sql['codGrupo']   = 27;
                break;
            case 'Bl':
                array_push($array, "cronograma_pmp.cod_cronograma_pmp", "nome_estacao AS nome_local");

                $sql['sql'] = Sql::cronogramaBilhetagem($array);
                $sql['siglaGrupo'] = 'bl';
                $sql['nomeGrupo']  = 'Bilhetagem';
                $sql['nameLocal']  = 'Esta��o';
                $sql['codGrupo']   = 28;
                break;
        }

        return $sql;
    }

    public static function getSsmpSql($tipoForm, $array){
        switch ($tipoForm){
            case 'Ed':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "nome_local");

                $sql['sql']  = Sql::ssmpEdificacao($array);
                $sql['nameLocal']  = 'Local';
                $sql['siglaGrupo'] = 'ed';
                break;
            case 'Su':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "nome_local");

                $sql['sql']  = Sql::ssmpSubestacao($array);
                $sql['nameLocal']  = 'Local';
                $sql['siglaGrupo'] = 'su';
                break;
            case 'Vp':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "ei.nome_estacao || ' - ' || ef.nome_estacao AS nome_local");

                $sql['sql']  = Sql::ssmpViaPermanente($array);
                $sql['nameLocal']  = 'Esta��o Inicial / Final';
                $sql['siglaGrupo'] = 'vp';
                break;
            case 'Ra':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "nome_local", "nome_poste");

                $sql['sql']  = Sql::ssmpRedeAerea($array);
                $sql['nameLocal']  = 'Local / Poste';
                $sql['siglaGrupo'] = 'ra';
                break;
            case 'Vlt':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "nome_veiculo AS nome_local");

                $sql['sql']  = Sql::ssmpVlt($array);
                $sql['nameLocal']  = 'Veiculo';
                $sql['siglaGrupo'] = 'vlt';
                break;
            case 'Tue':
                $array = ["ssmp.cod_ssmp", "nome_veiculo", "nome_servico_pmp","data_programada"];

                $sql['sql']  = Sql::ssmpTue($array);
                $sql['nameLocal']  = 'Veiculo';
                $sql['siglaGrupo'] = 'tue';
                break;
            case 'Tl':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "nome_local");

                $sql['sql']  = Sql::ssmpTelecom($array);
                $sql['nameLocal']  = 'Local';
                $sql['siglaGrupo'] = 'tl';
                break;
            case 'Bl':
                array_push($array, "ssmp.cod_ssmp", "cod_cronograma_pmp", "nome_estacao AS nome_local");

                $sql['sql']  = Sql::ssmpBilhetagem($array);
                $sql['nameLocal']  = 'Esta��o';
                $sql['siglaGrupo'] = 'bl';
                break;
        }

        return $sql;
    }
    
    public static function getOsmpSql($tipoForm, $array){
        switch ($tipoForm){
            case 'Ed':
                array_push($array, "cod_ssmp", "nome_local");

                $sql['sql']  = Sql::osmpEdificacao($array);
                $sql['nameLocal']  = 'Local';
                $sql['siglaGrupo'] = 'ed';
                break;
            case 'Su':
                array_push($array, "cod_ssmp", "nome_local");

                $sql['sql']  = Sql::osmpSubestacao($array);
                $sql['nameLocal']  = 'Local';
                $sql['siglaGrupo'] = 'su';
                break;
            case 'Vp':
                array_push($array, "cod_ssmp", "ei.nome_estacao || ' - ' || ef.nome_estacao AS nome_local");

                $sql['sql']  = Sql::osmpViaPermanente($array);
                $sql['nameLocal']  = 'Esta��o Inicial / Final';
                $sql['siglaGrupo'] = 'vp';
                break;
            case 'Ra':
                array_push($array, "cod_ssmp", "nome_local", "nome_poste");

                $sql['sql']  = Sql::osmpRedeAerea($array);
                $sql['nameLocal']  = 'Local / Poste';
                $sql['siglaGrupo'] = 'ra';
                break;
            case 'Vlt':
                array_push($array, "cod_ssmp", "nome_veiculo AS nome_local");

                $sql['sql']  = Sql::osmpMrVlt($array);
                $sql['nameLocal']  = 'Veiculo';
                $sql['siglaGrupo'] = 'vlt';
                break;
            case 'Tue':
                array_push($array, "cod_ssmp", "nome_veiculo AS nome_local");

                $sql['sql']  = Sql::osmpMrTue($array);
                $sql['nameLocal']  = 'Veiculo';
                $sql['siglaGrupo'] = 'tue';
                break;
            case 'Tl':
                array_push($array, "cod_ssmp", "nome_local");

                $sql['sql']  = Sql::osmpTelecom($array);
                $sql['nameLocal']  = 'Local';
                $sql['siglaGrupo'] = 'tl';
                break;
            case 'Bl':
                array_push($array, "cod_ssmp", "nome_estacao AS nome_local");

                $sql['sql']  = Sql::osmpBilhetagem($array);
                $sql['nameLocal']  = 'Esta��o';
                $sql['siglaGrupo'] = 'bl';
                break;
        }

        return $sql;
    }

    public static function getUnidadesUserSql($equipeUsuario){
        $unidadesUserSql = '';

        if (!empty($equipeUsuario)) {
            $unidadesUser = array();
            foreach ($equipeUsuario as $dados) {
                $unidadesUser[] = $dados['cod_unidade'];
            }
            $unidadesUserSql = implode(',', $unidadesUser);
            $unidadesUserSql = "linha.cod_unidade IN({$unidadesUserSql}) AND ";
        }

        return $unidadesUserSql;
    }

    // -- Cronograma
    // -- Cronograma
    // -- Cronograma

    public static function cronogramaEdificacao($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_edificacao USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN local USING (cod_local)
                        JOIN linha USING (cod_linha)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }
    
    public static function cronogramaSubestacao($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_subestacao USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN local USING (cod_local)
                        JOIN linha USING (cod_linha)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }
    
    public static function cronogramaViaPermanente($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_via_permanente USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN estacao ei ON pmp_via_permanente.cod_estacao_inicial = ei.cod_estacao
                        JOIN linha USING (cod_linha)
                        JOIN estacao ef ON pmp_via_permanente.cod_estacao_final = ef.cod_estacao
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }

    public static function cronogramaRedeAerea($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_rede_aerea USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN local USING(cod_local)
                        JOIN linha USING (cod_linha)
                        LEFT JOIN poste USING(cod_poste)
                        LEFT JOIN via USING(cod_via)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }

    public static function cronogramaVLT($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_vlt USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN veiculo USING(cod_veiculo)
                        JOIN linha USING (cod_linha)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }

    public static function cronogramaTelecom($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_telecom USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN local USING (cod_local)
                        JOIN linha USING (cod_linha)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }

    public static function cronogramaBilhetagem($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                      FROM cronograma_pmp
                        JOIN pmp USING (cod_pmp)
                        JOIN pmp_bilhetagem USING (cod_pmp)
                        JOIN cronograma USING (cod_cronograma)
                        JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                        JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                        JOIN servico_pmp USING (cod_servico_pmp)
                        JOIN estacao USING (cod_estacao)
                        JOIN linha USING (cod_linha)
                        JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                        JOIN status_cronograma_pmp USING (cod_status_cronograma_pmp)
                        JOIN status USING (cod_status)";
    }

    // -- Ssmp
    // -- Ssmp
    // -- Ssmp

    public static function ssmpEdificacao($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_ed USING (cod_ssmp)
                                JOIN local USING (cod_local)
                                JOIN linha USING (cod_linha)
                                
                                JOIN pmp_edificacao USING (cod_pmp_edificacao)
                                JOIN pmp USING (cod_pmp)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)
                                
                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpSubestacao($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_su USING (cod_ssmp)
                                JOIN local USING (cod_local)
                                JOIN linha USING (cod_linha)
                                
                                JOIN pmp_subestacao USING (cod_pmp_subestacao)
                                JOIN pmp USING (cod_pmp)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                
                             
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)

                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpViaPermanente($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_vp USING (cod_ssmp)
                                JOIN estacao ei ON ssmp_vp.cod_estacao_inicial = ei.cod_estacao
                                JOIN linha USING (cod_linha)
                                JOIN estacao ef ON ssmp_vp.cod_estacao_final = ef.cod_estacao
                                LEFT JOIN via USING(cod_via)
                                
                                JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                                JOIN pmp USING (cod_pmp)
                                
                                LEFT JOIN amv USING(cod_amv)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                
                          
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)

                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpRedeAerea($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_ra USING (cod_ssmp)
                                JOIN local USING(cod_local)
                                JOIN linha USING (cod_linha)
                                LEFT JOIN poste USING(cod_poste)
                                LEFT JOIN via USING(cod_via)
                                
                                JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                                JOIN pmp USING (cod_pmp)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                                
                             
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)

                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpVlt($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_vlt USING (cod_ssmp)
                                JOIN veiculo USING(cod_veiculo)
                                JOIN linha USING (cod_linha)
                                
                                JOIN pmp_vlt USING (cod_pmp_vlt)
                                JOIN pmp USING (cod_pmp)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                
                              
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)

                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpTue($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_tue USING (cod_ssmp)
                                JOIN veiculo USING(cod_veiculo)
                                JOIN linha USING (cod_linha)
                                
                                JOIN servico_pmp USING (cod_servico_pmp)
                                
                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpTelecom($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_te USING (cod_ssmp)
                                JOIN local USING (cod_local)
                                JOIN linha USING (cod_linha)
                                
                                JOIN pmp_telecom USING (cod_pmp_telecom)
                                JOIN pmp USING (cod_pmp)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                
                             
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)
                                
                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    public static function ssmpBilhetagem($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                              FROM ssmp
                                LEFT JOIN un_equipe USING (cod_un_equipe)
                                JOIN ssmp_bi USING (cod_ssmp)
                                JOIN estacao USING (cod_estacao)
                                JOIN linha USING (cod_linha)
                                
                                JOIN pmp_bilhetagem USING (cod_pmp_bilhetagem)
                                JOIN pmp USING (cod_pmp)
                                
                                JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                                JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                                JOIN sub_sistema USING (cod_sub_sistema)
                                JOIN sistema USING (cod_sistema)
                                JOIN subsistema USING (cod_subsistema)
                                JOIN servico_pmp USING (cod_servico_pmp)
                                JOIN procedimento USING (cod_procedimento)
                                JOIN tipo_periodicidade USING(cod_tipo_periodicidade)
                                
                                
                                JOIN cronograma_pmp USING (cod_cronograma_pmp)
                                JOIN cronograma USING (cod_cronograma)
                                
                                JOIN status_ssmp USING (cod_status_ssmp)
                                JOIN status USING(cod_status)
                                JOIN usuario USING(cod_usuario)";
    }

    // -- Osmp
    // -- Osmp
    // -- Osmp

    public static function osmpEdificacao($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_ed
                            JOIN osmp USING (cod_osmp)
                            JOIN local USING (cod_local)
                            JOIN linha USING (cod_linha)
                            JOIN ssmp_ed USING (cod_ssmp)
                            JOIN pmp_edificacao USING (cod_pmp_edificacao)
                            JOIN pmp USING (cod_pmp)
                            JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                            JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                            JOIN servico_pmp USING (cod_servico_pmp)
                            JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpSubestacao($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_su
                            JOIN osmp USING (cod_osmp)
                            JOIN local USING (cod_local)
                            JOIN linha USING (cod_linha)
                            JOIN ssmp_su USING (cod_ssmp)
                            JOIN pmp_subestacao USING (cod_pmp_subestacao)
                            JOIN pmp USING (cod_pmp)
                            JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                            JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                            JOIN servico_pmp USING (cod_servico_pmp)
                            JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpViaPermanente($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_vp
                               JOIN osmp USING (cod_osmp)
                               JOIN estacao ei ON osmp_vp.cod_estacao_inicial = ei.cod_estacao
                               JOIN linha USING (cod_linha)
                               JOIN estacao ef ON osmp_vp.cod_estacao_final = ef.cod_estacao
                               JOIN ssmp_vp USING (cod_ssmp)
                               JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                               JOIN pmp USING (cod_pmp)
                               JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                               JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                               JOIN servico_pmp USING (cod_servico_pmp)
                               JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpRedeAerea($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_ra
                               JOIN osmp USING (cod_osmp)
                               JOIN local USING(cod_local)
                               JOIN linha USING (cod_linha)
                               LEFT JOIN poste USING(cod_poste)
                               LEFT JOIN via USING(cod_via)
                               JOIN ssmp_ra USING (cod_ssmp)
                               JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                               JOIN pmp USING (cod_pmp)
                               JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                               JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                               JOIN servico_pmp USING (cod_servico_pmp)
                               JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpMrVlt($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_vlt
                               JOIN osmp USING (cod_osmp)
                               JOIN veiculo USING(cod_veiculo)
                               JOIN linha USING (cod_linha)
                               JOIN ssmp_vlt USING (cod_ssmp)
                               JOIN pmp_vlt USING (cod_pmp_vlt)
                               JOIN pmp USING (cod_pmp)
                               JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                               JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                               JOIN servico_pmp USING (cod_servico_pmp)
                               JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpMrTue($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_tue
                               JOIN osmp USING (cod_osmp)
                               JOIN veiculo USING(cod_veiculo)
                               JOIN linha USING (cod_linha)
                               JOIN ssmp_tue USING (cod_ssmp)
                               JOIN servico_pmp USING (cod_servico_pmp)
                               JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpTelecom($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_te
                            JOIN osmp USING (cod_osmp)
                            JOIN local USING (cod_local)
                            JOIN linha USING (cod_linha)
                            JOIN ssmp_te USING (cod_ssmp)
                            JOIN pmp_telecom USING (cod_pmp_telecom)
                            JOIN pmp USING (cod_pmp)
                            JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                            JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                            JOIN servico_pmp USING (cod_servico_pmp)
                            JOIN status_osmp USING (cod_status_osmp) ";
    }

    public static function osmpBilhetagem($arrayCollumn = null){
        if($arrayCollumn)
            $stringCollumn = implode(',', $arrayCollumn);
        else
            $stringCollumn = '*';

        return "SELECT {$stringCollumn}
                          FROM osmp_bi
                            JOIN osmp USING (cod_osmp)
                            JOIN estacao USING (cod_estacao)
                            JOIN linha USING (cod_linha)
                            JOIN ssmp_bi USING (cod_ssmp)
                            JOIN pmp_bilhetagem USING (cod_pmp_bilhetagem)
                            JOIN pmp USING (cod_pmp)
                            JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                            JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                            JOIN servico_pmp USING (cod_servico_pmp)
                            JOIN status_osmp USING (cod_status_osmp) ";
    }

    
    public static function osmpEd(){
        return "SELECT
                  osmp.cod_osmp,
                  equipe.sigla,
                  nome_equipe,
                  un_equipe.cod_unidade,
                  nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_ed,
                  nome_linha,
                  nome_local,
                  osmp.descricao AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_ed.cod_ssmp
                FROM osmp_ed
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
                  JOIN local USING (cod_local)
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade USING (cod_unidade)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_ed USING (cod_ssmp)
                  JOIN pmp_edificacao USING (cod_pmp_edificacao)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }
    
    public static function osmpVp(){
        return "SELECT
                  osmp.cod_osmp,
                  equipe.sigla,
                  nome_equipe,
                  un_equipe.cod_unidade,
                  nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_vp,
                  ei.nome_estacao AS estacao_inicial,
                  ef.nome_estacao AS estacao_final,
                  nome_via,
                  osmp_vp.posicao,
                  osmp_vp.km_inicial,
                  osmp_vp.km_final,
                  nome_linha,
                  nome_amv,
                  osmp.descricao  AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_vp.cod_ssmp
                FROM osmp_vp
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
                
                  JOIN estacao ei ON osmp_vp.cod_estacao_inicial = ei.cod_estacao
                  JOIN linha USING (cod_linha)
                  JOIN estacao ef ON osmp_vp.cod_estacao_final = ef.cod_estacao
                  LEFT JOIN via USING (cod_via)
                
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade ON un_equipe.cod_unidade = unidade.cod_unidade
                  JOIN ssmp_vp USING (cod_ssmp)
                  JOIN pmp_via_permanente USING (cod_pmp_via_permanente)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN amv USING (cod_amv)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }
    
    public static function osmpSu(){
        return "SELECT
                  osmp.cod_osmp,
                  equipe.sigla,
                  nome_equipe,
                  un_equipe.cod_unidade,
                  nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_su,
                  nome_linha,
                  nome_local,
                  osmp.descricao AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_su.cod_ssmp
                FROM osmp_su
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
                  JOIN local USING (cod_local)
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade USING (cod_unidade)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_su USING (cod_ssmp)
                  JOIN pmp_subestacao USING (cod_pmp_subestacao)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }

    public static function osmpRa(){
        return "SELECT
                  osmp.cod_osmp, equipe.sigla, nome_equipe, un_equipe.cod_unidade, nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_ra,
                  
                  lo.nome_local,
                  nome_via,osmp_ra.posicao, p.nome_poste , l.nome_linha,
                  
                  osmp.descricao AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_ra.cod_ssmp
                FROM osmp_ra
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
        
                  JOIN local lo USING(cod_local)
                  JOIN linha l USING(cod_linha)
                  LEFT JOIN via USING(cod_via)
                  LEFT JOIN poste p USING(cod_poste)
                  
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade ON un_equipe.cod_unidade = unidade.cod_unidade
                  JOIN ssmp_ra USING (cod_ssmp)
                  JOIN pmp_rede_aerea USING (cod_pmp_rede_aerea)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }

    public static function osmpVlt(){
        return "SELECT
                  osmp.cod_osmp, equipe.sigla, nome_equipe, un_equipe.cod_unidade, nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_vlt,
                  
                  nome_veiculo , l.nome_linha,
                  
                  osmp.descricao AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_vlt.cod_ssmp
                FROM osmp_vlt
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
        
                  JOIN veiculo USING(cod_veiculo)
                  JOIN linha l USING(cod_linha)
                  
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade ON un_equipe.cod_unidade = unidade.cod_unidade
                  JOIN ssmp_vlt USING (cod_ssmp)
                  JOIN pmp_vlt USING (cod_pmp_vlt)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }
    
    public static function osmpTue(){
        return "SELECT
                  osmp.cod_osmp, equipe.sigla, nome_equipe, un_equipe.cod_unidade, nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_tue,
                  
                  nome_veiculo , l.nome_linha,
                  
                  osmp.descricao AS complemento,
                  
                  nome_servico_pmp,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_tue.cod_ssmp
                FROM osmp_tue
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
        
                  JOIN veiculo USING(cod_veiculo)
                  JOIN linha l USING(cod_linha)
                  
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade ON un_equipe.cod_unidade = unidade.cod_unidade
                  JOIN ssmp_tue USING (cod_ssmp)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }

    public static function osmpTl(){
        return "SELECT
                  osmp.cod_osmp,
                  equipe.sigla,
                  nome_equipe,
                  un_equipe.cod_unidade,
                  nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_te,
                  nome_linha,
                  nome_local,
                  osmp.descricao AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_te.cod_ssmp
                FROM osmp_te
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
                  JOIN local USING (cod_local)
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade USING (cod_unidade)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_te USING (cod_ssmp)
                  JOIN pmp_telecom USING (cod_pmp_telecom)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }

    public static function osmpBl(){
        return "SELECT
                  osmp.cod_osmp,
                  equipe.sigla,
                  nome_equipe,
                  un_equipe.cod_unidade,
                  nome_unidade,
                  osmp.data_abertura,
                  cod_osmp_bi,
                  nome_linha,
                  nome_estacao,
                  osmp.descricao AS complemento,
                  nome_sistema,
                  nome_subsistema,
                  nome_servico_pmp,
                  nome_procedimento,
                  nome_periodicidade,
                  mao_obra,
                  horas_uteis,
                  homem_hora,
                  usuario.usuario,
                  cod_status_osmp,
                  cod_status,
                  nome_status,
                  status_osmp.descricao,
                  data_status,
                  osmp_encerramento.liberacao,
                  ssmp_bi.cod_ssmp
                FROM osmp_bi
                  JOIN osmp USING (cod_osmp)
                  LEFT JOIN usuario ON (usuario.cod_usuario = osmp.usuario_responsavel)
                  JOIN estacao USING (cod_estacao)
                  JOIN ssmp USING (cod_ssmp)
                  JOIN status_osmp USING (cod_status_osmp)
                  JOIN status USING (cod_status)
                  JOIN un_equipe USING (cod_un_equipe)
                  JOIN equipe USING (cod_equipe)
                  JOIN unidade USING (cod_unidade)
                  JOIN linha USING (cod_linha)
                  JOIN ssmp_bi USING (cod_ssmp)
                  JOIN pmp_bilhetagem USING (cod_pmp_bilhetagem)
                  JOIN pmp USING (cod_pmp)
                  JOIN servico_pmp_periodicidade USING (cod_servico_pmp_periodicidade)
                  JOIN servico_pmp_sub_sistema USING (cod_servico_pmp_sub_sistema)
                  JOIN servico_pmp USING (cod_servico_pmp)
                  JOIN sub_sistema USING (cod_sub_sistema)
                  JOIN sistema USING (cod_sistema)
                  JOIN subsistema USING (cod_subsistema)
                  JOIN procedimento USING (cod_procedimento)
                  JOIN tipo_periodicidade USING (cod_tipo_periodicidade)
                  LEFT JOIN osmp_encerramento ON (osmp_encerramento.cod_osmp = osmp.cod_osmp)";
    }
}