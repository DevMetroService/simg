<?php
/**
 * Todos os controllers dever�o estender essa classe
 *
 * @package SimgMVC
 * @since 0.1
 *
 *
 * Este c�digo � confidencial. A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 */
class MainController extends UsuarioLogin
{

    public $month = Array(1 => "Jan", 2 => "Fev", 3 => "Mar", 4 => "Abr", 5 => "Mai", 6 => "Jun", 7 => "Jul", 8 => "Ago", 9 => "Set", 10 => "Out", 11 => "Nov", 12 => "Dez");
    static public $monthComplete = Array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Mar�o", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
    /**
     * $db
     *
     * Nossa conex�o com a base de dados. Manter� o objeto PDO
     *
     * @access public
     */
    public $bancoDados;

    /**
     * $phpass
     *
     * Classe phpass
     *
     * @see http:#www.openwall.com/phpass/
     * @access public
     */
    public $phpass;

    /**
     * $titulo
     *
     * T�tulo das p�ginas
     *
     * @access public
     */
    public $titulo;

    /**
     * $navegador
     *
     * Navegador (Sidebar) da p�ginas
     *
     * @access public
     */
    //public $navegador; --> $_SESSION['navegador']

    /**
     * $pagina
     *
     * P�gina para contoler de reload
     *
     * @access public
     */
    public $pagina;

    /**
     * $pagina
     *
     * P�gina para direcionar o sidebar
     *
     * @access public
     */
    public $paginaDashboard;

    /**
     * $script
     *
     * Variavel para script selecionado
     *
     * @access public
     */
    public $script;

    /**
     * $bloquei
     *
     * Variavel para bloqueio de p�gina
     *
     * @access public
     */
    public $bloqueio;

    /**
     * $loginNecessario
     *
     * Se a p�gina precisa de login
     *
     * @access public
     */
    public $loginNecessario = false;

    /**
     * $parametros
     *
     * @access public
     */
    public $parametros = array();

    /**
     *  Nossa conex�o com a base de dados via Medoo.
     *
     * @var public
     */
    public $medoo;

    /**
     * Nossa classe de cria��o do Dashboard
     */
    public $dashboard;

    /**
     * Nossa classe de cria��o de formularios
     */
    public $form;

    /**
     * Nossa classe de cria��o do sidebar
     */
    public $sidebar;

    /**
     * Quantitativo de SAF, SSMS e OSM para referencia
     */
    public $quantidadeOSMfechamento;
    public $quantidadeOSM;
    public $quantidadeSSM;
    public $quantidadeSAF;

    /**
     * Canal de comunicacao websocket para notificacao
     */
    public $canalNotificacao = '';

    /**
     * Vari�veis de ambiente
    */
    public $home_uri = HOME_URI;
    public $sistemas_fixos = SISTEMAS_FIXOS;

    /**
     * Construtor da classe
     *
     * Configura as propriedades e metodos da classe.
     *
     * @since 0.1
     * @access public
     */
    public function __construct($parametros = array())
    {
        $this->bancoDados = new SimgBD ();                  # Instancia a classe de base de dados
        $this->phpass = new PasswordHash (8, false);        # Instancia o Phpass
        $this->medoo = new Medoo(array(PDO::ATTR_PERSISTENT => true));                         # Iniciar o Medoo
        $this->dashboard = new MainDashboard($this->medoo, $this->home_uri);                 # Iniciar o mainDashboard
        $this->form = new MainForm($this->medoo);           # Iniciar o mainForm
        $this->sidebar = new MainSidebar();                 # Iniciar o mainSidebar
        $this->parametros = $parametros;                    # Parametros
        $this->checarUsuario();                             # Verifica o login

        $this->sistemas_fixos = $this->medoo->select('grupo', 'cod_grupo', ['cod_tipo_grupo' => 5]);
    }

    /**
     * Carrega os modelos presentes na pasta /models/.
     * usar essa barra => /
     *
     * @since 0.1
     * @access public
     */
    public function carregaModelo($nomeModelo = false)
    {
        if (!$nomeModelo) {                                                            # Um arquivo dever� ser enviado
            return;                                                                    #
        }

        // $nomeModelo = strtolower($nomeModelo);                                  # Garante que o nome do modelo tenha letras min�sculas
        $caminhoModelo = ABSPATH . '/models/' . $nomeModelo . '.php';           # Inclui o arquivo

        if (file_exists($caminhoModelo)) {                                      # Verifica se o arquivo existe

            require_once $caminhoModelo;                                        # Inclui o arquivo
            $nomeModelo = explode('/', $nomeModelo);                            # Remove os caminhos do arquivo (se tiver algum)
            $nomeModelo = end($nomeModelo);                                     # Pega s� o nome final do caminho
            $nomeModelo = preg_replace('/[^a-zA-Z0-9]/is', '', $nomeModelo);    # Remove caracteres inv�lidos do nome do arquivo

            if (class_exists($nomeModelo)) {                                            # Verifica se a classe existe
                return new $nomeModelo ($this->bancoDados, $this, $this->medoo, $this->phpass, $this->dadosUsuario);    # Retorna um objeto da classe

            }
            return;
        }
    }

    public function controle()
    {
        switch (strtolower($_SESSION['direcionamento'])) {
            case "rh":
                $this->nivelNecessario = "1";
                break;

            case "ccm":
                $this->nivelNecessario = "2";
                break;

            case "supervisao":
                $this->nivelNecessario = "2.1";
                break;

            case "usuario":
                $this->nivelNecessario = "2.3";
                break;

            case "ti":
                $this->nivelNecessario = "3";
                break;

            case "material":
                $this->nivelNecessario = "4";
                break;

            case "equipe":
                $this->nivelNecessario = ["5", "5.2"];
                break;

            case "equipemr":
                $this->nivelNecessario = "5.1";
                break;

            case "diretoria":
                $this->nivelNecessario = "6";
                break;

            case "gesiv":
                $this->nivelNecessario = "6.1";
                break;

            case "astig":
                $this->nivelNecessario = "6.2";
                break;

            case "engenharia":
                $this->nivelNecessario = "7";
                break;

            case "engenhariasupervisao":
                $this->nivelNecessario = "7.1";
                break;

            case "engenhariamr":
                $this->nivelNecessario = "7.2";
                break;
        }

        $this->controleAcesso();
    }

    //Fun��o de valida��o de login.
    public function controleAcesso($unsetBloqueio = true)
    {
        if($unsetBloqueio)
            unset($_SESSION['bloqueio']);

        if (!$this->logado) {
            $this->logout();                                                             #
            $this->redirecionarParaPaginaLogin();                                        #
            return;                                                                      #
        }

        if (!$this->verificarNivel($this->nivelNecessario, $this->nivelUsuario)) {
            echo("Voc� n�o tem permiss�o para acessar essa p�gina");                    # Caso n�o tenha, exibe uma mensagem
            $this->logout();                                                            #
            $this->redirecionarParaPaginaLogin();                                       #
            return;
        }

    }

    //Transforma data e hora retirada do bd em formato americano para formato padr�o pt-Br
    public function parse_timestamp($timestamp, $format = 'd-m-Y H:i:s')
    {
        if ($timestamp == null) return "";

        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }

    //Transforma data retirada do bd em formato americano para formato padr�o pt-Br
    public function parse_timestamp_date($timestamp, $format = 'd-m-Y')
    {
        if ($timestamp == null) return "";

        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }

    //Transforma data retirada do bd em formato americano para formato padr�o pt-Br
    public static function parse_timestamp_static($timestamp, $format = 'd-m-Y H:i:s')
    {
        if ($timestamp == null) return "";

        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }

    //Fun��o de preenchimento dos bot�es de download em pesquisas.
    public function inputButtonDownDocFile($caminhoXlsCompleta = null, $caminhoTxtMaoObra = null, $caminhoTxtMateriais = null, $form = 'pesquisa')
    {

        echo("<div class='row'><div class='col-md-12'>");

        if (!empty($caminhoTxtMaoObra)) {
            echo('
                    <button onclick="changeActionForm(\'#' . $form . '\',\'' . $caminhoXlsCompleta . '\')" class="btn btn-primary btn-lg" type="button" ><i class="fa fa-file-excel-o fa-fw"></i>Excel Completo</button>
                    <button onclick="changeActionForm(\'#' . $form . '\',\'' . $caminhoTxtMaoObra . '\')" class="btn btn-primary btn-lg" type="button" ><i class="fa fa-file-excel-o fa-fw"></i>Excel M�o de Obra</button>
                    <button onclick="changeActionForm(\'#' . $form . '\',\'' . $caminhoTxtMateriais . '\')" class="btn btn-primary btn-lg" type="button" ><i class="fa fa-file-excel-o fa-fw"></i>Excel Materiais</button>');
        } else {
            echo('      <button class="btn btn-primary btn-lg" type="submit" ><i class="fa fa-file-excel-o fa-fw"></i>Excel Completo</button> ');
        }

        echo(' </div></div>');
    }

    //Fun��o de exibi���o do modal de altera��o de senha do usu�rio.
    public function alterarSenha($nomeUsuario, $senhaAtual, $novaSenha)
    {
        $senhaAntiga = $this->medoo->select("usuario", "senha", ["usuario" => $nomeUsuario]);
        $senhaAntiga = $senhaAntiga[0];

        if ($this->phpass->CheckPassword($senhaAtual, $senhaAntiga)) {
            $novaSenha = $this->phpass->HashPassword($novaSenha);
            $this->medoo->update("usuario", ["senha" => $novaSenha], ["usuario" => $nomeUsuario]);
        } else {
            return false;
        }

        return true;
    }

    //Fun��o de bloqueio de edi��o.
    public function bloquearEdicaoOsm($dataEncerramento, $status, $codStatus = null)
    {
        //Status de Bloqueio pelo codigo
        //Duplicado, N�o Configura Falha, Cancelada
        $statusOff = [24, 25, 38];
        if(in_array($codStatus, $statusOff)) return true;

        //Se pode ou n�o editar
        $canEdit = false;

        //Analisa cada usu�rio e seu tempo para edi��o
        switch ($this->dadosUsuario['nivel']) {
            case '5':  // Equipe
            case '5.1':  // equipeMr
            case '5.2':  // equipeMr
                $canEdit = true;
                $dias = "+0 days";
                break;
            case '7':  // Engenharia
            case '7.1':  // Engenharia
                $canEdit = true;
                $dias = "+0 days";
                break;
            case '2':  // ccm
                $canEdit = true;
                $dias = "+0 days";
                break;
            case '6.2':  // ASTIG
                $canEdit = true;
                $dias = "+360 days";
                break;
        }

        //verifica se tem a data de encerramento
        if(!empty($dataEncerramento))
        {
            //calcula diferente da data limite pela data atual e checa se ela est� encerrada e dentro do prazo.
            $dataLimite = date('d-m-Y', strtotime($dias, strtotime($dataEncerramento)));
            $dataAtual = date('d-m-Y');

            if ((strtotime($dataAtual) >= strtotime($dataLimite)) && $status == "Encerrada" && $dias == "+0 days") {
                return true;
            }
        }

        //Se puder editar, n�o bloqueia.
        if($canEdit)
            return false;
        else
            return true;
    }

    public function bloquearEdicao($dataEncerramento, $status, $codStatus = null)
    {
        //Se estiver cancelado, n�o pode editar n�o importa como.
        if (($status == "N�o Executado" || $status == "Duplicado" || $status == "N�o Configura falha"))
            return true;

       if (!empty($dataEncerramento) && $status == "Encerrada") {
           switch ($this->dadosUsuario['nivel']) {
               case '5':  // Equipe
               case '5.1':  // equipeMr
               case '5.2':  // equipeMr
                   $dias = "+3 days";
                   break;
               case '2':  // ccm
                   $dias = "+5 days";
                   break;
               case '15': // T.I
                   $dias = "+0 days";
                   break;
               case '7':  // Engenharia
               case '7.1':// Engenharia Supervis�oo
               case '7.2':// Engenharia Material Rodante
                   $dias = "+30 days";
                   break;
               case '6.2':  // ASTIG
                   $canEdit = true;
                   $dias = "+360 days";
                   break;
               default:
                   $dias = "+0 days";
           }

           $dataLimite = date('d-m-Y', strtotime($dias, strtotime($dataEncerramento)));
           $dataAtual = date('d-m-Y');

           if (strtotime($dataAtual) > strtotime($dataLimite)) {
               return true;
           }
       }
    }

    //Fun��o em que retorna as OSM em que estouram o tempo limite de execu��o.
    public function contarOSMTempoLimite($arryOSM)
    {
        $contador = 0;

        $dataAtual = new DateTime();
        foreach ($arryOSM as $dados) {
            $data = date('d-m-Y H:i', strtotime('+12 hour', strtotime($dados['data_status'])));
            $dataLimite = new DateTime($data);

            if ($dataLimite < $dataAtual)
                $contador += 1;
        }
        return $contador;
    }

    //Fun��o de download do formul�rio de preenchimento das OSM
    public function btnDownloadFormOsm($getArchive)
    {
        $caminhoDownload = HOME_URI . '/views/_includes/download.php?arquivo=' . $getArchive;
        echo('<a href="' . $caminhoDownload . '"><button class="btn btn-default"><i class="fa fa-print fa-fw fa-1x"></i> Imprimir Formul�rio OS em Excel</button></a>');
    }

    //Fun�o de grava��o de arquivos TXT.
    public function salvaTxt($arquivo, $conteudo)
    {
        $path = TEMP_PATH.$arquivo;

        $r = fopen($path, "w");
        if ($r) {
            $escreve = fwrite($r, $conteudo);
            fclose($r);
            return true;
        } else {
            return false;
        }
    }

    //Fun��o que retorna quinzena atual.
    public function getQzn()
    {
        $month = date('n');
        $day = date('j');
        $qznAtual = $month + ($month - 1);
        if ($day > 15) {
            $qznAtual += 1;
        }

        return $qznAtual;
    }

    public function getQznPerData($data)
    {
        $day = date('j', strtotime($data));
        $month = date('n', strtotime($data));

        if ($day > 15) {
            $qzn = $month * 2;
        } else {
            $qzn = ($month * 2) - 1;
        }

        return $qzn;
    }

    //Fun��o que retorna pr�xima quinzena.
    public function getNextQzn()
    {
        $qznAtual = $this->getQzn();

        if ($qznAtual + 1 > 24)
            $proxQzn = 1;
        else
            $proxQzn = $qznAtual + 1;

        return $proxQzn;
    }

    //Fun��o que retorna quinzenas referente ao pr�ximo mas e o nome.
    public function getQznNextMes()
    {
        $dados = [];

        $mes = date('n');
        if ($mes == 12) {
            $proxMes = 1;
            $dados['ano'] = date('Y') + 1;
        } else {
            $proxMes = $mes + 1;
            $dados['ano'] = date('Y');
        }
        $dados['primQ'] = $proxMes + $proxMes - 1;
        $dados['segQ'] = $proxMes + $proxMes;

        $dados['nome'] = MainController::$monthComplete[$proxMes];

        return $dados;
    }

    public function getMesPorQzn($qzn, $ano)
    {
        if ($qzn % 2 > 0) {
            $mes = ($qzn + 1) / 2;

            if ($mes < 10)
                $mes = "0" . $mes;

            $periodo = "01/{$mes}/{$ano} 00:00:00 a 15/{$mes}/{$ano} 23:59:59";
        } else {
            $mes = ($qzn) / 2;

            if ($mes < 10) {
                $mesAtual = "0" . $mes;
                $mes += 1;
                if ($mes < 10)
                    $mes = "0" . $mes;
                $periodo = "16/{$mesAtual}/{$ano} 00:00:00 a 01/{$mes}/{$ano} 00:00:00";
            } else {
                $mesAtual = $mes;
                $mes += 1;

                $periodo = "16/{$mesAtual}/{$ano} 00:00:00 a 01/{$mes}/{$ano} 00:00:00";
            }
        }

        return $periodo;

    }

    //Fun��es de valida��o de dados para MODEL.
    public function validarDadosSsp($session)
    {

        $dados = $session;

        if (!(int)$dados['servicoSsp'])
            return false;

        if (!(int)$dados['tipoIntervencao'])
            return false;

        if (!(int)$dados['diasServico'])
            return false;

        if (!empty($dados['numeroSsmPendente']) && !(int)$dados['numeroSsmPendente'])
            return false;

        if (!(int)$dados['grupoSistemaSsp'])
            return false;

        if (!(int)$dados['sistemaSsp'])
            return false;

        if (!empty($dados['subSistemaSsp']) && !(int)$dados['subSistemaSsp'])
            return false;

        if (!(int)$dados['linhaSsp'])
            return false;

        if (!(int)$dados['trechoSsp'])
            return false;

        if ($this->validateDate($dados['dataHoraSsp']))
            return false;

        return true;
    }

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('d-m-Y', $date);
        return $d && $d->format('d-m-Y') === $date;
    }

    function retiraAcentos($texto)
    {
        return strtr($texto, "??????????????????????????", "aaaaeeiooouucAAAAEEIOOOUUC");
    }

    //fun��o de filtrarQzn necess�ria para constru��o das planilhas de cronograma.
    function filtrarQzn($dados, $quinzena)
    {
        foreach ($dados as $dado) {

            $qznInicial = $dado['quinzena'];
            $perio = $dado['cod_tipo_periodicidade'];

            $qzn = $qznInicial;

            while ($qzn <= $quinzena) {

                if ($quinzena == $qzn) {
                    $dadosQzn[] = $dado;
                }

                switch ($perio) { // Ajuste conforme a periodicidade
                    case 9: // Anual // a cada 12 meses
                        $qzn += 24;
                        break;
                    case 8: // Semestral // a cada 6 meses
                        $qzn += 12;
                        break;
                    case 7: // Quadrimestral // a cada 4 meses
                        $qzn += 8;
                        break;
                    case 6: // Trimestral // a cada 3 meses
                        $qzn += 6;
                        break;
                    case 5: // Bimestral // a cada 2 meses
                        $qzn += 4;
                        break;
                    case 4: // Mensal // a cada m�s
                        $qzn += 2;
                        break;
                    case 3: // Quinzenal // uma vez na quinzena
                        $qzn += 1;
                        break;
                    case 2: // Semanal // duas vezes na quinzena
                        $qzn += 1;
                        break;
                    case 1: // Diario // uma vez na quinzena com quantidade de dias de servi�o em 15
                        $qzn += 1;
                        break;
                }
            }
        }

        return $dadosQzn;
    }

    //Fun��o emq ue retira somente a data em um dado de data e hora.
    public function getOnlyData($timestamp, $format = 'd-m-Y')
    {
        if ($timestamp == null) return "";

        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }

    //Fun��o de preenchimento do modal de impress�o para engenharia supervis�o. Modal de gera��o de Cronograma.
    public function modalFiltroCronograma($grupoSistema, $tagGrupo, $titulo)
    {
        echo "<button class='btn btn-primary' data-toggle='modal' data-target='#imprimirPMP{$grupoSistema}ModalCronograma'>
                    <i class='fa fa-print fa-fw fa-1x'></i> Imprimir
                </button> ";

        $this->modalCronograma($grupoSistema, $tagGrupo, $titulo);
    }

    public function modalCronograma($grupoSistema, $tagGrupo, $titulo, $codGrupo)
    {
        echo("<div class='modal fade' id='imprimirPMP{$grupoSistema}ModalCronograma' tabindex='-1' role='form'
                 aria-labelledby='imprimirPMP{$grupoSistema}ModalQzn'
                 data-backdrop='static' aria-hidden='true'>
                <div class='modal-dialog'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>x</span></button>
                            <h4 class='modal-title'><i class='fa fa-university fa-fw'></i>
                                {$titulo} ");
        echo date('Y');
        echo("</h4></div>
                        <form action='" . HOME_URI ."dashboardPmp/pdf{$grupoSistema}Crono' method='post' target='_blank'>
                            <div class='modal-body'>

                                <div class='row'>
                                    <div class='col-md-6'>
                                        <label>Selecione o M�s</label>
                                        <select name='mes' class='form-control'>
                                            <option value='' selected>Selecione o M�s</option>");
        foreach (MainController::$monthComplete as $key => $value) {
            echo("<option value='{$key}'>$value</option>");
        }

        echo("</select></div><div class='col-md-6'>
                                        <label>Selecione a Linha</label>
                                        <select name='linha' class='form-control'>
                                            <option value='' selected>Todas</option>");
        $linha = $this->medoo->select("linha", "*", ["ORDER" => "cod_linha"]);
        foreach ($linha as $dados => $value) {
            echo("<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>");
        }
        echo("</select></div></div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <label>Servi�o</label>
                                        <select name='servico' class='form-control'>
                                            <option value=''>Todos</option>");

        $servico = $this->medoo->select("servico_pmp", "*", ["cod_grupo" => $codGrupo]);
        foreach ($servico as $dados => $value) {
            echo("<option value='{$value['cod_servico_pmp']}'>{$value['nome_servico_pmp']}</option>");
        }
        echo(" </select ></div ></div ></div ><div class='modal-footer' >
                                <button class='btn btn-primary btnImprimir{$tagGrupo}Crono' type = 'submit'
                                        aria-label = 'right align' title = 'Gerar' >
                                    <i class='fa fa-check' ></i > Gerar
                                </button>
                                <button type='button' class='btn btn-danger' aria-label='right align' data-dismiss='modal' title='Fechar'>
                                    <i class='fa fa-times'></i> Fechar
                                </button>
                            </div >
                        </form >
                    </div >
                </div >
            </div >");
    }

    //Fun�ao de preenchimendo do modal de impress�o para engenharia supervis�o. Modal de gera��ao de Cronograma Quinzenal
    public function modalQznCronograma($tagGrupo, $titulo)
    {
        echo "<button class='btn btn-success' data-toggle='modal' data-target='#imprimirPMP{$tagGrupo}ModalQzn'>
                    <i class='fa fa-print fa-fw fa-1x'></i> Imprimir por Quinzena
                </button> ";

        echo("<div class='modal fade' id='imprimirPMP{$tagGrupo}ModalQzn' tabindex='-1' role='form' aria-labelledby='imprimirPMP{$tagGrupo}ModalQzn' data-backdrop='static' aria-hidden='true'>
                    <div class='modal-dialog'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                    <span aria-hidden='true'>x</span></button>
                                <h4 class='modal-title'><i class='fa fa-university fa-fw'></i> {$titulo} -
                                    Escolha a quinzena:</h4>
                            </div>
                            <div class='modal-body'>");
        for ($i = 1; $i <= 24; $i++) {
            echo("<input type='radio' value='{$i}' class='btnImprimirPMP{$tagGrupo}Qzn' data-dismiss='modal' aria-label='Close'/> - {$i} <br/>");
        }

        echo("<input type='radio' value='' class='btnImprimirPMP{$tagGrupo}Qzn'
                                       data-dismiss='modal' aria-label='Close'/> - <strong>Todas</strong>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>");
    }

    public function modalExibirCronograma($osmp)
    {
        echo '<div class="modal fade ExibirPmp" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">x</button>
                                <h3 class="modal-title">Cronograma - PMP</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Cod. Cronograma</label>
                                        <input class="form-control" readonly name="cronogramaModal" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Grupo</label>
                                        <input class="form-control" readonly name="grupoModal" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>Sistema</label>
                                        <input class="form-control" readonly name="sistemaModal" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>SubSistema</label>
                                        <input class="form-control" readonly name="subsistemaModal" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <label>Servi�o</label>
                                        <input class="form-control" readonly name="servicoModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Procedimento</label>
                                        <input class="form-control" readonly name="procedimentoModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Periodicidade</label>
                                        <input class="form-control" readonly name="periodicidadeModal" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Linha</label>
                                        <input class="form-control" readonly name="linhaModal" />
                                    </div>
                                    <div class="col-md-8">
                                        <label>Local</label>
                                        <textarea class="form-control" readonly name="localModal"></textarea>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-2">
                                        <label>Quinzena Prevista</label>
                                        <input class="form-control" readonly name="quinzenaModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Homem - Hora</label>
                                        <input class="form-control" readonly name="hhModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Situa��o</label>
                                        <input class="form-control" readonly name="statusModal" />
                                    </div>';
        if ($osmp) {
            echo '<div class="col-md-3">
                                        <label>In�cio</label>
                                        <input class="form-control" readonly name="inicioModal" />
                                    </div>
                                    <div class="col-md-3">
                                        <label>T�rmino</label>
                                        <input class="form-control" readonly name="terminoModal" />
                                    </div>
                                </div>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="tableRelModal table table-bordered table-responsive">
                                            <thead>
                                            <th>Cod. Osmp</th>
                                            <th>Cod. Ssmp</th>
                                            <th>Data de Abertura</th>
                                            <th>Status</th>
                                            <th>A��es</th>
                                            </thead>

                                            <tbody></tbody>
                                        </table>
                                    </div>';
        }

        echo '</div>
                            </div>
                        </div>
                    </div>
                </div>';
    }

    public function modalExibirPmp()
    {
        echo '<div class="modal fade ExibirPmp" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">x</button>
                                <h3 class="modal-title">PMP - Cronograma</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Cod. PMP</label>
                                        <input class="form-control" readonly name="pmpModal" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Grupo</label>
                                        <input class="form-control" readonly name="grupoModal" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>Sistema</label>
                                        <input class="form-control" readonly name="sistemaModal" />
                                    </div>
                                    <div class="col-md-4">
                                        <label>SubSistema</label>
                                        <input class="form-control" readonly name="subsistemaModal" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <label>Servi�o</label>
                                        <input class="form-control" readonly name="servicoModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Procedimento</label>
                                        <input class="form-control" readonly name="procedimentoModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Periodicidade</label>
                                        <input class="form-control" readonly name="periodicidadeModal" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Linha</label>
                                        <input class="form-control" readonly name="linhaModal" />
                                    </div>
                                    <div class="col-md-8">
                                        <label>Local</label>
                                        <textarea class="form-control" readonly name="localModal"></textarea>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-2">
                                        <label>Quinzena Inicial</label>
                                        <input class="form-control" readonly name="quinzenaModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Homem - Hora</label>
                                        <input class="form-control" readonly name="hhModal" />
                                    </div>
                                    <div class="col-md-2">
                                        <label>Situa��o Atual</label>
                                        <input class="form-control" readonly name="statusModal" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="tableRelModal table table-bordered table-responsive">
                                            <thead>
                                            <th>Cod. Cronograma</th>
                                            <th>Quinzena</th>
                                            <th>M�s</th>
                                            <th>Ano</th>
                                            <th>Status</th>
                                            </thead>

                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
    }

    public function getOptionSelectByVariable($variable, $indexHtml, $indexValue, $valueSelected=null, $subIndex = null)
    {
        $options = "";

        foreach($variable as $dados=>$value)
        {
            if($subIndex != null)
                $labelOption = "{$value[$indexHtml]} - {$value[$subIndex]}";
            else
                $labelOption = $value[$indexHtml];


            if($value[$indexValue] == $valueSelected){
                $options .= "<option value='$value[$indexValue]' selected>$labelOption</option>";
            }
            else{
                $options .= "<option value='$value[$indexValue]'>$labelOption</option>";
            }
        }

        return $options;
    }

    protected function validacaoHorario($horaEnviada, $form, $codigo)
    {

        switch ($form) {
            case 'ssp':
                $horaValida = $this->medoo->select("v_ssp", "data_programada", ["cod_ssp" => (int)$codigo]);
                $horaValida = $horaValida[0];
                if ($horaEnviada . ":00" != $horaValida) {
                    return true;
                }
                break;
            case 'ssmp':
                $horaValida = $this->medoo->select("ssmp", "data_programada", ["cod_ssmp" => (int)$codigo]);
                $horaValida = $this->parse_timestamp($horaValida[0]);
                if ($horaEnviada . ":00" != $horaValida) {
                    return true;
                }
                break;
        }

        return false;
    }

    function imprimirAlteracaoStatus($status, $descricao, $data, $codigo)
    {
        echo "<hr style='border-top: 1px dashed #cacaca; margin-top: 20px; margin-bottom: 20px' />
            <div class='row'>
                <div class='col-md-3'>{$data}</div>
                <div class='col-md-6'>
                    <label style='font-size: large;'>{$status}</label>
                    <br />
                    <span style='font-size: medium;'>{$descricao}</span>
                </div>
                <div class='col-md-3'>
                    <label>{$codigo}</label>
                </div>
            </div>";
    }
}
