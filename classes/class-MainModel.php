<?php

/**
 * Todos os modelss dever�o estender essa classe
 *
 * @package SimgMVC
 * @since 0.1
 */
class MainModel
{
    /**
     * Os dados de formul�rios de envio.
     *
     * @access public
     */
    public $dadosFormulario;

    /**
     * $feedbackMensagem
     * As mensagens de feedback para formul�rios.
     *
     * @access public
     */
    public $feedbackMensagem;

    /**
     * $form_confirma
     * Mensagem de confirma��o para apagar dados de formul�rios
     *
     * @access public
     */
    public $form_confirma;

    /**
     * $db
     * O objeto da nossa conex�o PDO
     *
     * @access public
     */
    public $bancoDados;


    /**
     *  Nossa conex�o com a base de dados via Medoo.
     *
     * @access public
     */
    public $medoo;
    /**
     *  Nossa conex�o com PHPpass.
     *
     * @access public
     */
    public $phpass;

    /**
     * $controller
     * O controller que gerou esse modelo
     *
     * @access public
     */
    public $controller;

    /**
     * $parametros
     * Par�metros da URL
     *
     * @access public
     */
    public $parametros;

    /**
     * $userdata
     * Dados do usu�rio
     *
     * @access public
     */
    public $dadosUsuario;

    /**
     * Inverte datas
     * Obt�m a data e inverte seu valor.
     * De: d-m-Y H:i:s para Y-m-d H:i:s ou vice-versa.
     *
     * @since 0.1
     * @access public
     * @param string $data
     *            A data
     */
    public function inverteData($data = null)
    {
        $nova_data = null;                                            // Configura uma vari�vel para receber a nova data

        if (!empty($data)){                                                // Se a data for enviada

            $data = preg_split('/\-|\/|\s|:/', $data);    // Explode a data por -, /, : ou espa�o
            $data = array_map('trim', $data);                // Remove os espa�os do come�o e do fim dos valores
            $nova_data .= chk_array($data, 2) . '-';            // Cria a data invertida
            $nova_data .= chk_array($data, 1) . '-';            // Cria a data invertida
            $nova_data .= chk_array($data, 0);                    // Cria a data invertida

            if (chk_array($data, 3)) {
                $nova_data .= ' ' . chk_array($data, 3);        // Configura a hora
            }

            if (chk_array($data, 4)) {
                $nova_data .= ':' . chk_array($data, 4);        // Configura os minutos
            }

            if (chk_array($data, 5)) {
                $nova_data .= ':' . chk_array($data, 5);        // Configura os segundos
            }
        }
        return $nova_data;                                            // Retorna a nova data
    }

    /*  Fun��o que ir� gravar a��o que desencadeou a notifica��o
     *  Receber� o canal, ao qual a notifica��o ir� passar,
     *  as informa��es que ir�o
     *  e o usu�rio respons�vel.
     *  Tamb�m ir� realizar a chamada do envio da fun��o!
     */

    function osResponsavel($usuario, $numOs, $os){
        switch ($os){
            case "osp":
                $coluna = "cod_osp";
                break;
            case "osmp":
                $coluna = "cod_osmp";
                break;
            default:
                $coluna = "cod_osm";
                break;
        }

        $responsavel = $this->medoo->select($os,"usuario_responsavel",[$coluna => $numOs]);
        $responsavel = $responsavel[0];

        if(empty($responsavel)){
            $updateOsm = $this->medoo->update($os,[
                "usuario_responsavel"       => (int) $usuario
            ],[
                $coluna                     => (int) $numOs
            ]);
        }
    }

    function getCodUsuario($nomeUsuario){
        $codUsuario = $this->medoo->select("usuario", "cod_usuario",
            [
                "usuario"     =>  $nomeUsuario,
            ]
        );

        return $codUsuario[0];
    }

    function getCodEquipe($sigla){
        $codigoEquipe = $this->medoo->select("equipe", "cod_equipe",
            [
                "sigla"     =>  $sigla,
            ]
        );

        return $codigoEquipe[0];
    }

    function getCodGrupoSistema($sigla){
        $sigla = $this->medoo->select("grupo", "cod_grupo",
            [
                "sigla"     =>  $sigla,
            ]
        );

        return $sigla[0];
    }

    function getCodSistema($sigla){
        $codSistema = $this->medoo->select("sistema", "cod_sistema",
            [
                "sigla"     =>  $sigla,
            ]
        );

        return $codSistema[0];
    }

    function alterarStatusSsmReferente($codSsp, $time, $dadosUsuario){
        $vSsp = $this->medoo->select("v_ssp", ["cod_ssm","tipo_orissp","cod_saf"], ["cod_ssp" => (int)$codSsp]);
        $vSsp = $vSsp[0];

        if($vSsp["tipo_orissp"] == "2"){ // Corretiva
            $codSsm = $vSsp["cod_ssm"];  // A SSM dever� esta no status Agendada

            $this->alterarStatusSsm($codSsm, 16, $time, $dadosUsuario);

            $_SESSION['notificacao']['mensag'] .= "<br />Ssm n. {$codSsm} encerrada.";
            $this->encerrarSaf($vSsp["cod_saf"], $time, $dadosUsuario);
        }
    }

    function encerrarSaf($codSaf, $time, $dadosUsuario, $status = 14, $descricao = ""){
        //Por meio da OS n�o ser� possivel encerrar a SAF, deixando-a para analise
        if( $this->verificarSsm($codSaf)){
            $this->alterarStatusSaf($codSaf, $status, $time, $dadosUsuario, $descricao);

            if($status == 14)
                $_SESSION['notificacao']['mensag'] .= "<br />Saf n. {$codSaf} encerrada.";
        }
    }

    function encerrarSsm($codSsm, $time, $dadosUsuario, $status = 7, $descricao = ""){
        $statusSaf = 2;

        if( !$this->verificarOsm($codSsm))
        {
            $status = 35;
            $statusSaf = 37;
        }

        $this->alterarStatusSsm($codSsm, $status, $time, $dadosUsuario, $descricao);

        //Por meio da OS n�o ser� possivel encerrar a SAF, deixando-a para analise
        $codSaf = $this->medoo->select("ssm", "cod_saf", ["cod_ssm" => $codSsm]);
        $codSaf = $codSaf[0];

        $this->encerrarSaf($codSaf, $time, $dadosUsuario, $statusSaf, $descricao);
    }

    function verificarSsm($codSaf){
        $ssms = $this->medoo->select("v_ssm","cod_status",["cod_saf" => $codSaf]);

        if(count($ssms)== 1)
            return true;

        $status = [6,7,16, 26, 35]; // DEVOLVIDA, CANCELADA, ENCERRADA, PROGRAMADA, AGUARDANDO VALIDA��O

        foreach($ssms as $dados => $value){
            //Se for diferente de DEVOLVIDA, CANCELADA, PROGRAMADA, ENCERRADA, AGUARDANDO VALIDA��O
            //N�O altera a SAF
            if(!in_array($value, $status)){
                return false;
            }
        }
        return true;
    }

    function verificarOsm($codSsm){
        $osms = $this->medoo->select("v_osm","cod_status",["cod_ssm" => $codSsm]);

        if(count($osms) == 1 || empty($osms)){
            return true;
        }

        $status = [23,24,25,38]; // N�O EXECUTADO, DUPLICADO, N�O CONFIGURA FALHA, CANCELADA

        foreach($osms as $dados => $value){
            //Se for diferente de N�O EXECUTADO, DUPLICADO, N�O CONFIGURA FALHA, CANCELADA
            if(!in_array($value, $status)){
                return false;
            }
        }
        return true;
    }

    /**
    * Alter Status Saf
    * 
    *  Altera o status da SAF de acordo com l�gica;
    *
    * @access   public
    */
    function alterarStatusSaf($codSaf, $status, $time, $dadosUsuario, $descricao = null){
        //FIM DE FLUXO
        $statusSaf=[2,14,27];

        if(in_array($status, $statusSaf))
          $newStatusSaf = $this->medoo->insert("status_saf", [
              "cod_saf"     => (int)$codSaf,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "data_termino_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);
        else
          //insere novo status sem data t�rmino
          $newStatusSaf = $this->medoo->insert("status_saf", [
              "cod_saf"     => (int)$codSaf,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);

        //recebe status atual
        $cod_ssaf = $this->medoo->select("saf", "cod_ssaf", ["cod_saf" => $codSaf]);

        //Atualiza data de t�rmino do status atual
        $updateLastStatus = $this->medoo->update("status_saf",[
          "data_termino_status" => $time
        ], [
          "cod_ssaf"            => (int)$cod_ssaf[0]
        ]);

        //Atualiza ssm para NOVO status
        $updateSaf = $this->medoo->update("saf", [
            "cod_ssaf"    => (int)$newStatusSaf
        ] ,[
            "cod_saf"     => (int)$codSaf
        ]);
    }

    function alterarStatusSsm($codSsm, $status, $time, $dadosUsuario, $descricao = null){
        //FIM DE FLUXO
        $statusSsm=[7,16,26,28];

        if(in_array($status, $statusSsm))
          $ssmPendente = $this->medoo->insert("status_ssm", [
              "cod_ssm"     => (int)$codSsm,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "data_termino_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);
        else
          //insere novo status sem data t�rmino
          $ssmPendente = $this->medoo->insert("status_ssm", [
              "cod_ssm"     => (int)$codSsm,
              "cod_status"  => (int)$status,
              "descricao"   => $descricao,
              "data_status" => $time,
              "usuario"     => (int)  $dadosUsuario['cod_usuario']
          ]);

        //recebe status atual
        $cod_mstatus = $this->medoo->select("ssm", "cod_mstatus", ["cod_ssm" => $codSsm]);

        //Atualiza data de t�rmino do status atual
        $updateLastStatus = $this->medoo->update("status_ssm",[
          "data_termino_status" => $time
        ], [
          "cod_mstatus" => (int)$cod_mstatus[0]
        ]);

        //Atualiza ssm para NOVO status
        $updateSsm = $this->medoo->update("ssm", [
            "cod_mstatus" => (int)$ssmPendente
        ] ,[
            "cod_ssm"     => (int)$codSsm
        ]);
    }

    function alterarStatusOsm($codOsm, $status, $time, $dadosUsuario, $descricao = null, $codOsmDuplicado = null)
    {
        //FIM DE FLUXO
        $statusOsm=[11,24,25,23,38];

        if(in_array($status, $statusOsm))
          //Insere o novo status
          $insertStatusOs = $this->medoo->insert("status_osm",
              [
                  "cod_osm"           => (int)$codOsm,
                  "cod_status"        => (int)$status,
                  "cod_osm_duplicado" => (int)$codOsmDuplicado,
                  "descricao"         => $descricao,
                  "data_status"       => $time,
                  "data_termino_status" => $time,
                  "usuario"           => $dadosUsuario['cod_usuario']
              ]
          );
        else
          //Insere o novo status
          $insertStatusOs = $this->medoo->insert("status_osm",
              [
                  "cod_osm"           => (int)$codOsm,
                  "cod_status"        => (int)$status,
                  "cod_osm_duplicado" => (int)$codOsmDuplicado,
                  "descricao"         => $descricao,
                  "data_status"       => $time,
                  "usuario"           => $dadosUsuario['cod_usuario']
              ]
          );


        //recebe status atual
        $cod_ostatus = $this->medoo->select("osm_falha", "cod_ostatus", ["cod_osm" => $codOsm]);

        //Atualiza data de t�rmino do status atual
        $updateLastStatus = $this->medoo->update("status_osm",[
          "data_termino_status" => $time
        ], [
          "cod_ostatus" => (int)$cod_ostatus[0]
        ]);

        //Atualiza o status atual da OSM
        $updateOsm = $this->medoo->update("osm_falha",
            [
                "cod_ostatus"       => (int) $insertStatusOs
            ],[
                "cod_osm"           => (int)$codOsm
            ]
        );
    }

    function alterarStatusSsp($codSsp, $status, $time, $dadosUsuario){
        $sspPendente = $this->medoo->insert("status_ssp", [
            "cod_ssp"     => (int)$codSsp,
            "cod_status"  => (int)$status,
            "data_status" => $time,
            "usuario"     => (int)  $dadosUsuario['cod_usuario']
        ]);

        $updateSsp = $this->medoo->update("ssp", [
            "cod_pstatus" => (int)$sspPendente
        ], [
            "cod_ssp"     => (int)$codSsp
        ]);
    }

    function parse_timestamp($timestamp, $format = 'd-m-Y H:i:s')
    {
        if($timestamp == null) return "";

        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }

    function filtrarQzn($dados, $quinzena) {
        foreach ($dados as $dado) {

            $qznInicial = $dado['quinzena'];
            $perio = $dado['cod_tipo_periodicidade'];

            $qzn = $qznInicial;

            while ($qzn <= $quinzena) { // Enquanto a qzn for inferior a Quinzena pedida

                if ($quinzena == $qzn) {
                    $dadosQzn[] = $dado; // Adicionado os dados da pmp para ser adicionano ao cronograma desta quinzena
                }

                switch ($perio) { // Ajuste conforme a periodicidade
                    case 9: // Anual // a cada 12 meses
                        $qzn += 24;
                        break;
                    case 8: // Semestral // a cada 6 meses
                        $qzn += 12;
                        break;
                    case 7: // Quadrimestral // a cada 4 meses
                        $qzn += 8;
                        break;
                    case 6: // Trimestral // a cada 3 meses
                        $qzn += 6;
                        break;
                    case 5: // Bimestral // a cada 2 meses
                        $qzn += 4;
                        break;
                    case 4: // Mensal // a cada m�s
                        $qzn += 2;
                        break;
                    case 3: // Quinzenal // uma vez na quinzena
                        $qzn += 1;
                        break;
                    case 2: // Semanal // duas vezes na quinzena
                        $qzn += 1;
                        break;
                    case 1: // Diario // uma vez na quinzena com quantidade de dias de servi�o em 15
                        $qzn += 1;
                        break;
                }
            }
        }

        return $dadosQzn;
    }

    public function feedBack($tipo, $titulo, $mensagem){
        $_SESSION['feedBackBD']['tipo']     = $tipo;
        $_SESSION['feedBackBD']['titulo']   = $titulo;
        $_SESSION['feedBackBD']['mensagem'] = $mensagem;
    }
}
