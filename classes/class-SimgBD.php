<?php
/** 
 * Classe respons�vel pelo gerenciamento e conexao � base de dados
 * @since 0.1
 * @package SimgMVC/classes
 * 
 * Este c�digo � confidencial. A c�pia parcial ou integral de qualquer parte do texto abaixo poder� implicar em encargos judiciais.
 */
class SimgBD {
	
	/* Propriedades do Banco de Dados */
	private	$host 		= HOSTNAME, //'10.0.0.5', 				# Host da base de dados
            $port       = PORTNUMBER,
			$db_name 	= DB_NAME, //'TESTE', //'SIMG', 				    # Nome do banco de dados
			$senha 		= DB_SENHA, //'joao123',			# Senha do usu�rio da base de dados
			$usuario 	= DB_USER,  //'johanes', 				    # Usu�rio da base de dados
			$charset 	= DB_CHARSET, //'latin1', 				# Charset da base de dados
			$pdo 		= null, 					# Nossa conex�o com o BD
			$error 		= null, 					# Configura o erro
			$debug 		= true, 					# Mostra todos os erros
			$last_id 	= null; 					# �ltimo ID inserido
	
	/**
	 * Contrutor da classe
	 *
	 * @since 0.1
	 * @access public
	 * @param string $host        	
	 * @param string $db_name        	
	 * @param string $senha        	
	 * @param string $usuario        	
	 * @param string $debug        	
	 */
	public function __construct($host = null, $db_name = null, $senha = null, $usuario = null, $debug = null) {
		
		$this->host 	= defined ( 'HOSTNAME' ) 			# Configura as propriedades novamente.
									? HOSTNAME 	: $this->host;
		$this->port 	= defined ( 'PORTNUMBER' ) 			# Configura as propriedades novamente.
									? PORTNUMBER 	: $this->port;
		$this->db_name 	= defined ( 'DB_NAME' ) 			# Configura as propriedades novamente.
									? DB_NAME 	: $this->db_name;
		$this->senha 	= defined ( 'DB_SENHA' ) 			# Configura as propriedades novamente.
									? DB_SENHA 	: $this->senha;
		$this->usuario 	= defined ( 'DB_USER' )				# Configura as propriedades novamente.
									? DB_USER 	: $this->usuario;
		$this->debug 	= defined ( 'DEBUG' )				# Configura as propriedades novamente.
									? DEBUG 	: $this->debug;
		
		$this->conectarBanco();								# Conecta � base de dados.
		
	}
	
	/**
	 * Estabelece a conex�o com a base de dados
	 *
	 * @since 0.1
	 * @final
	 *
	 * @access protected
	 */
	final protected function conectarBanco() {
        $is_port = isset($this->port);
		$pdo_details 	= "pgsql:host={$this->host}; port=5433;"; 			# String com as configura��es de conex�o � base de dados
        $pdo_details    .= ($is_port ? 'port=' . $this->port.';' : '');
		$pdo_details 	.= "dbname={$this->db_name};";				# String com as configura��es de conex�o � base de dados

		try {
			$this->pdo 	= new PDO($pdo_details, $this->usuario,
								  $this->senha);
			
			if ($this->debug === true) { 							# A aplica��o est� em modo de desenvolvimento?
				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, 
										PDO::ERRMODE_WARNING);		# Configuramos o PDO ERR
			}
			
			#unset ( $this->host ); 								# Descartamos a propriedade 
			#unset ( $this->db_name );								# Descartamos a propriedade
			#unset ( $this->senha );								# Descartamos a propriedade
			#unset ( $this->usuario );								# Descartamos a propriedade
			
			} catch ( PDOException $e ) {
				if ($this->debug === true) {
					print_r ($e->getMessage());
				}
			
				die ();
			}
	}
	/**
	 * Faz a consulta via PDO
	 *
	 * @since 0.1
	 * @access public
	 * @param string $statement        	
	 * @param string $arrayDados        	
	 *
	 *
	 * @return object|bool Retorna uma consulta ou falso caso a query n�o executar com sucesso
	 */
	public function consulta($statement, $arrayDados = null) {
		
		if (!$statement) {												# String n�o pode ser vazia
			$this->error = "Erro: Query vazia!";
			return false ;
		}
		try {
			$query 				= 	$this->pdo->prepare($statement);	# Prepara��o para a consulta;
			$checar_execucao	= 	$query->execute($arrayDados); 		# Executa a consulta;
			
			if ($checar_execucao) {										# A consulta foi executada com sucesso?
				return $query;											# Em caso positivo, retorna a query;
			} else {													# Caso n�o, inicia o tratamento de erro;
				$error			= 	$query->errorInfo();				# Informa o erro
				$this->error	= 	$error[2];							#
				var_dump($error[2]);
				return false;											# Retorna False
			}
			
		} catch (PDOException $erro ) {
				echo("Erro:" . $erro->getMessage());
				
		}
		
	}
	
	/**
	 * Insere uma tupla � uma tabela e retorna o �ltimo id enviado
	 *
	 * @since 0.1
	 * @param string $table
	 *        	- Nome da tabela
	 * @param
	 *        	array ...
	 */
	public function inserirTupla($tabela, $dados = array()) {
		
		$ultimoIdInserido = array();
		
		if(!isset($dados[0])){
			$dados = array($dados);
		}
		
		foreach ($dados as $dado){
			$chave 		= array_keys($dado);
			$value 		= array();
			$colunas 	= array();
			
			
			
		}

	}
	
	/**
	 * Atualiza uma tupla na base de dados
	 *
	 * @since 0.1
	 * @param string $table        	
	 * @param string $where_field        	
	 * @param string $where_field_value        	
	 * @param string $values        	
	 * @return object|bool
	 */
	public function atualizarTupla($table, $where_field, $where_field_value, $values) {
		
		if (empty ( $table ) 	
				|| empty ( $where_field ) 
				|| empty ( $where_field_value )) {			# Se os par�metros vierem vazios, retornamos false
			return;
		}
		
		$stmt 	= 	"UPDATE `$table` SET "; 				# Inicia a Query
		$set 	= 	array ();								# Configura o array de valores 
		$where 	= 	" WHERE `$where_field` = ? ";			# Configura a declara��o do WHERE campo=valor (PDO)
		
		
		if (! is_array ( $values )) {						# Se n�o for um array de valor, retorna false
			return;
		}
		
		
		foreach ( $values as $column => $value ) { 			# Configura as colunas a atualizar
			$set [] = " `$column` = ?";
		}
		
		
		$set 		= 	implode ( ', ', $set ); 			# Separa as colunas por v�rgula
		$stmt 		.= 	$set . $where;						# Concatena a declara��o 
		$values [] 	= 	$where_field_value;					# Configura o valor do campo que vamos buscar
		$values 	= 	array_values ( $values );			# Garante apenas n�meros nas chaves do array
		$update 	= 	$this->query ( $stmt, $values ); 	# Atualiza 
		
		
		if ($update) {										# Verifica se a consulta est� OK
			
			return $update;									# Retorna a consulta
		}
		
		return;
	}
	
	/**
	 *
	 * @param unknown $table        	
	 * @param unknown $where_field        	
	 * @param unknown $where_field_value        	
	 * @return void|unknown
	 */
	public function apagarTupla($table, $where_field, $where_field_value) {
		
		if (empty ( $table ) 	
				|| empty ( $where_field ) 
				|| empty ( $where_field_value )) {			# Se os par�metros vierem vazios, retornamos false
			return;
		}
		
		
		$stmt 		= 		" DELETE FROM `$table` ";		 # Inicia a declara��o
		$where 		= 		" WHERE `$where_field` = ? ";	 # Configura a declara��o WHERE campo=valor
		$stmt 		.= 		$where; 						 # Concatena tudo
		$values 	= 		array ($where_field_value);		 # O valor que vamos buscar para apagar
		$delete 	= 		$this->query ( $stmt, $values ); # Apaga
		
		if ($delete) {										 # Verifica se a consulta est� OK
			return $delete;									 # Retorna a consulta
		}
		
		return;
	}

	public function fecharConexao() {
		
		try {
			
		} catch (PDOException $e) {
		}
	}
}

?>