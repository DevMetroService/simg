<?php

class NovidadesModal{

    public $dataNotificacao = "01-12-2020 17:30:00"; // "dd-mm-yyyy hh:mm:ss"

    public $mensagens = array(

       "<strong>Atualiza��es:</strong>
           <ul style='text-indent: 35px; margin-top: 5px'>
               <li>&raquo; Sugest�o de n�vel por av�rias.</li>
               <li>&raquo; Filtro de DATA em relat�rio de <storng>RETRABALHO</storng>.</li>
               <li>&raquo; Novas op��es na gera��o do <strong>CRONOGRAMA</strong>.<small>Somente para Engenharia de Sistemas Fixos.</small>.</li>
               <li>&raquo; Reformula��o da visuala��o de <strong>N�VEL MATERIAL</strong>.</li>
               <li>&raquo; Novas colunas em exporta��o de excel de materiais utilizados.</li>
               <small>Caso n�o esteja recebendo e-mail, verifique com o respons�vel pela cria��o de usu�rios se seu e-mail est� cadastrado corretamente.</small></li>
           </ul><br/>",
        "<strong>Corre��es:</strong>
            <ul style='text-indent: 35px; margin-top: 5px'>
                <li>&raquo; Corre��o da exporta��o de excel de Materiais Utilizado.Acrescentado Descri��o.</li>
                <li>&raquo; Retifica��o do relat�rio de <strong>CRONOGRAMA SIMPLES</strong> para <strong>BILHETAGEM E TELECOM</strong>.</li>
            </ul><br/>",
//        "Alterado relat�rios de corretivas para material rodante. Adicionado l�gica de <strong>Agente Causador</strong>. ",
//        "Removido op��o de Sistema 'TODOS' na cria��o de SAF's, SSM's e OSM's referentes ao grupo de sistema de Material Rodante, conforme solicitado.",
//        "Adicionado nova funcionalidade para os n�veis de engenharia: <strong>Cadastro e altera��o</strong> de usu�rios do SIMG.",

//        "Em complemento a informa��o dos novos grupos adicionados, foi itamb�m adicionado dois novos campos espec�ficos:
//        <ul style='text-indent: 35px; margin-top: 5px'>
//                <li>&raquo; <strong>Local</strong></li>
//                <li>&raquo; <strong>SubLocal</strong></li>
//            </ul><br />",
//
//        "CCM
//            <ul style='text-indent: 35px; margin-top: 5px'>
//                <li>&raquo; Adicionado nova tabela indicando SAF's devolvidas que n�o obtiveram resposta a mais de 5 dias.</li>
//            </ul><br />",
//
//        "Implementado novo m�todo de exporta��o de dados em todas as pesquisas. As exporta��es Completa ser� no formato '.txt'.
//            <ul style='text-indent: 35px; margin-top: 5px'>
//                <li><em>� possivel a importa��o de um arquivo '.txt' para uma planilha Excel, em caso de d�vidas entre em contato com nossa equipe.</em></li>
//            </ul>",
//
        "<em>O <strong>SIMG</strong> � um sistema de uma �nica aba, para evitar comportamentos inesperados utilize-o sem abrir novas abas.</em>",

        "<em>Em caso de comportamento estranho dos formul�rios do Sistema pressione 'F5' para atualizar a p�gina. Caso n�o funcione, entre em contato com a ASTIG atrav�s do e-mail suporte.simg@metrofor.ce.gov.br ou nos ramais 7189 / 7278 / 7222.</em>"

    );

    public function mostrarNovidades($ultimoLogin){
        //$ultimoLogin = "00:00:01 - Mon, 01 Aug 2016"; // Para testes Formato que vem na variavel

        if(!$ultimoLogin) return "WellCome";

        $logHora = explode(' - ', $ultimoLogin);    // Separar a Hora
        $logDat = explode(',', $logHora[1]);        // Separar Data de dia da semana

        $logHoraDat = $logHora[0] . $logDat[1];     // Unir Hora Data
        $arrayDat = date_parse($logHoraDat);        // Transformar em Array Data

        $datUltimoLogin = $arrayDat['day'].'-'.$arrayDat['month'].'-'.$arrayDat['year'].' '.$arrayDat['hour'].':'.$arrayDat['minute'].':'.$arrayDat['second'];

        $datUlt = new DateTime($datUltimoLogin);
        $dataUltimoLogin = $datUlt->getTimestamp(); // Transforma a data em Integer

        $datNov = new DateTime($this->dataNotificacao);
        $dataNovidade = $datNov->getTimestamp();    // Transforma a data em Integer

        if($dataUltimoLogin < $dataNovidade)
            return "WellCome";
        else
            return "DashBoard / index";
    }
}
