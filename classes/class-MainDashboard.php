<?php
/**
 * Created by PhpStorm.
 * User: josue.santos
 * Date: 17/03/2017
 * Time: 09:53
 *
 * Classe que possui fun��es para a cria��o dos Dashboards do sistema, contendo as partes em comum de todos os niveis
 */

class MainDashboard{
    public $medoo;

    public $usuarioValidacao;
    public $home_uri;

    public function __construct($medoo, $home_uri){
        $this->medoo = $medoo;
        $this->home_uri = $home_uri;

        $this->usuarioValidacao = $this->medoo->select('usuario', 'cod_usuario',['validater' => 's']);
    }

    public function modalUserForm($dadosUsuario)
    {
        $usuario = $this->medoo->select("usuario", "*", ['cod_usuario' => $dadosUsuario['cod_usuario']])[0];
        echo <<<HTML

        <script>
        $(document).ready(function()
        {
            var modalUserForm = $('#updateUser');
            $.get("{$this->home_uri}/usuario/hasCompleteRegister/{$usuario['cod_usuario']}", function(dados)
            {
                console.log(dados);
                if(dados == "false")
                {
                    alertaJquery("Dados de Cadastro Incompletos", 
                    "Identificamos que seu cadastro de usu�rio est� com algumas informa��es ausentes. "+
                    "Para utilizar todas as funcionalidades do sistema, solicitamos que complete seu cadastro."+
                    "</br> Gostaria de fazer agora?", 
                    "alert", 
                    [{ value: "SIM" },{ value: "N�O" }], 
                    function(result){
                        if(result == "SIM")
                        {
                            modalUserForm.modal('show');
                        }
                    });
                }
            });

            $('#btnUpdateDataUser').click(function()
            {
                var nomeCompleto = $('#nomeCompletoFormUpdate');
                var email = $('#emailFormUpdate');

                var message = "";

                if(nomeCompleto.val() == "")
                {
                    message += "- A informa��o de <strong>Nome Completo</strong> � obrigat�ria.</br>";
                    nomeCompleto.parent('div').addClass('has-error');
                }

                if(email.val() == "")
                {
                    message += "- A informa��o de <strong>Email</strong> � obrigat�ria.</br>";
                    email.parent('div').addClass('has-error');
                }

                if(message != "")
                {
                    alertaJquery('Campos Obrigat�rios', message, 'alert');
                    return;
                }

                $.post("{$this->home_uri}/usuario/update/{$usuario['cod_usuario']}",
                $('#userDataUpdateForm').serialize(), 
                function(data)
                {
                    console.log(data);
                }).success(function()
                {
                    modalUserForm.modal('hide');
                    alertaJquery('Dados Registrados', 'Dados registrado com sucesso!', 'info');
                });
            });
        })
        </script>

<div id="updateUser" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dados de Cadastro de Usu�rio</h4>
      </div>
      <div class="modal-body">
        <form id="userDataUpdateForm" action="" method="post">
            <div class="row">
                <div class='col-md-12'>
                    <label>USU�RIO</label>
                    <input class="form-control" readonly name="usuario" value="{$usuario['usuario']}" type="text">
                    <input class="form-control" readonly name="cod_usuario" value="{$usuario['cod_usuario']}" type="hidden">
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>NOME COMPLETO*</label>
                    <input id="nomeCompletoFormUpdate" class="form-control" name="nome_completo" value="{$usuario['nome_completo']}" type="text">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>EMAIL*</label>
                    <input id="emailFormUpdate" class="form-control" name="email" value="{$usuario['email']}" type="text">
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
        <button type="button" class="btn btn-primary" id="btnUpdateDataUser">SALVAR</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

HTML;
    }

    public function cabecalho($titulo, $tamanho = 1){
        echo ("<div class='page-header'><h{$tamanho}>{$titulo}</h{$tamanho}></div>");
    }

    public function auditoriaSaf($cod_usuario){
        if(in_array($cod_usuario, $this->usuarioValidacao)) {
            $query = "SELECT ss.cod_ssm, ss.cod_saf,ss.nome_grupo, ss.nome_sistema, ss.nome_servico, ss.data_abertura, ss.nome_grupo, ss.nome_sistema,
                      ss.nome_subsistema, ss.nome_linha, ss.descricao_trecho, ss.nome_ponto, ss.nome_veiculo, ss.nome_avaria FROM v_ssm ss
                      JOIN saf s USING(cod_saf) WHERE cod_status = 35";
            $ssmEspera = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);

            $contador = count($ssmEspera);
            if ($contador > 0) {
                $this->htmlTabelaInicio("AuditoriaSaf", "SSM's Aguardando Valida��o", $contador);

                echo "<thead>
                    <tr>
                        <th>SSM</th>
                        <th>SAF</th>
                        <th>Grupo de Sistemas</th>
                        <th>Local</th>
                        <th>Data de Abertura</th>
                        <th>Avaria</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
                $home_uri = HOME_URI;

                echo "<div class='modal fade' id='modal-validador' tabindex='-1' role='dialog'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close -align-right' data-dismiss='modal' aria-label='Close'>
                    <i aria-hidden='true' class='fa fa-close'></i>
                </button>
                <h4 class='modal-title' id='tituloValidacao'>Dados do Servi�o(SSM) N� <span id='numServicoValidacao'></span> </h4>
            </div>
            <div class='modal-body'>
                <form id='formModalSsm' class='form-group' method='post' action='". HOME_URI ."cadastroGeral/aprovarSsm'>
                    <div class='col-md-12'>
                        <div class='panel panel-primary'>
                            <div class='panel-heading' style='background-color: #337ab7 !important;'>
                                <label>Resumo das atua��es</label>
                            </div>
                            <div class='panel-body'>
                                <div id='infosModal'></div>
                            </div>
                        </div>
                        <div class='panel panel-primary'>
                            <div class='panel-heading' style='background-color: #337ab7 !important;'>
                                <label>Observa��es</label>
                            </div>
                            <div class='panel-body'>
                                <label id='observacoes'>Registre as observa��es</label>
                                <div>
                                    <textarea id='observacaoModal' class='form-control' type='text' value='observacao' placeholder='Observa��es'></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-success' id='btnFinalizarValidacao'>
                    <i class='fa fa-check'></i><span id='spanValidar'>Validar</span>
                </button>
                <button type='button' class='btn btn-danger' id='btnCancelaValidacao' data-dismiss='modal'>
                    <i class='fa fa-close'></i> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>";

                foreach ($ssmEspera as $dados => $value) {
                    $dataAbertura = MainController::parse_timestamp_static($value['data_abertura']);

                    echo "<tr>
                            <td>{$value['cod_ssm']}</td>
                            <td>{$value['cod_saf']}</td>
                            <td><span class='primary-info'>{$value['nome_grupo']}</span><br /><span class='sub-info'>{$value['nome_sistema']}</span></td>
                            <td><span class='primary-info'>{$value['nome_linha']}</span><br /><span class='sub-info'>{$value['descricao_trecho']}</span></td>
                            <td>{$dataAbertura}</td>
                            <td>{$value['nome_avaria']}</td>";
                    if(!empty($value['nome_veiculo'])){
                        echo "<td><span class='primary-info'>{$value['nome_servico']}</span><br /><span class='sub-info'>{$value['nome_veiculo']}</span></td>";
                    }else{
                        echo "<td>{$value['nome_servico']}</td>";
                    }

                    echo "<td>
                                <a href='{$home_uri}/dashboardGeral/aprovarSsm/{$value['cod_ssm']}'>
                                    <button class='btn btn-primary btn-circle' type='button' title='Ver Dados'>
                                        <i class='fa fa-eye fa-fw'></i>
                                    </button>
                                </a>
                                <br>

                                <button class='btn btn-success btnValidacaoOpenModal' type='button' title='Aprovar SSM' data-toggle='modal'
                                data-target='#modal-validador' data-status='16' data-ssm='{$value['cod_ssm']}'>
                                    <i class='fa fa-thumbs-up'></i>
                                </button>

                                <button class='btn btn-danger btnValidacaoOpenModal' type='button' title='Devolver SSM' data-toggle='modal'
                                data-target='#modal-validador' data-status='36' data-ssm='{$value['cod_ssm']}'>
                                    <i class='fa fa-close'></i>
                                </button>
                            </td>
                        </tr>";
                }

                $this->htmlTabelaFim();

            }
        }
    }


    // -- Cronograma
    // -- Cronograma
    // -- Cronograma

    public function graficoAcompanhamentoAnoAtual(){
        $ano = date('Y');

        echo <<<HTML
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Acompanhamento do Cronograma $ano</h4>
                        <input type="hidden" name="title" value="<?php echo date('Y') ?>">
                    </div>
                    <div class="col-md-2" style="float: right">
                        <select name="mesBarDashboard" class="form-control">
HTML;

        foreach (MainController::$monthComplete as $key => $value) {
            echo (date('n') == $key) ? "<option value='$key' selected='selected'>$value</option>" : "<option value='$key'>$value</option>";
        }

        echo <<<HTML
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="cronograma"></div>
            </div>
        </div>
    </div>
</div>
HTML;
    }

    public function painelCronograma($tipoCron, $imprimirCrono = false, $acao = false, $proxMes = null, $whereFiltroLocal = null, $pendente = false, $marcarTodos = false, $cor = 'primary'){
        $arrayCollumn = ["nome_servico_pmp", "cronograma.quinzena", "nome_periodicidade", "nome_status"];

        $returnGetSql = Sql::getCronogramaSql($tipoCron, $arrayCollumn);

        $sql        = $returnGetSql['sql'];
        $siglaGrupo = $returnGetSql['siglaGrupo'];
        $nomeGrupo  = $returnGetSql['nomeGrupo'];
        $nameLocal  = $returnGetSql['nameLocal'];
        $codGrupo   = $returnGetSql['codGrupo'];

        if(!empty($proxMes)){
            $ano              = $proxMes['ano'];
            $proxMesPrimQuinz = $proxMes['primQ'];
            $proxMesSegQuinz  = $proxMes['segQ'];
            $nomeMes          = $proxMes['nome'];

            $wherePeriodo = " ano = {$ano} AND cronograma.quinzena IN({$proxMesPrimQuinz},{$proxMesSegQuinz})";
        }else{
            $wherePeriodo = " ano = " . date('Y');
        }

        // Tabela Cronograma Pendente
        if($pendente){
            if(!empty($whereFiltroLocal)){
                $cronogramaPendente = $this->medoo->query($sql . ' WHERE ' . $whereFiltroLocal . ' status.cod_status = 22')->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $cronogramaPendente = $this->medoo->query($sql . ' WHERE status.cod_status = 22')->fetchAll(PDO::FETCH_ASSOC);
            }

            if (!empty($cronogramaPendente)){
                $contadorPendente = count($cronogramaPendente);

                $subTitulo = "<div class='col-md-4'><label>{$contadorPendente} - Cronograma(s) Pendente(s)</label></div>";

                $this->htmlTabelaInicio("CronogramaPendente{$tipoCron}", "Cronogramas Pendentes", $contadorPendente, 'red', $subTitulo);

                echo "<thead>
                            <tr>
                                <th>N�</th>
                                <th>Servi�o</th>
                                <th>{$nameLocal}</th>
                                <th>Quinzena Prevista</th>
                                <th>Periodicidade</th>
                                <th>Status</th>
                                <th>A��o</th>
                            </tr>
                        </thead>
                        <tbody>";

                foreach ($cronogramaPendente as $dados => $value) {
                    $local = $value['nome_local'];
                    if($codGrupo == 20 && $value['nome_poste']){
                        $local .= " - {$value['nome_poste']}";
                    }
                    echo <<<HTML
                            <tr>
                                <td>{$value['cod_cronograma_pmp']}<input type='hidden' value='{$nomeGrupo}' ></td>
                                <td>{$value['nome_servico_pmp']}</td>
                                <td>{$local}</td>
                                <td>{$value['quinzena']}</td>
                                <td>{$value['nome_periodicidade']}</td>
                                <td>{$value['nome_status']}</td>
                                <td>
HTML;
                    if($acao){
                        echo "<button value='{$siglaGrupo}' type='button' class='btnReprogramar btn btn-circle btn-success' title='Reprogramar'><i class='fa fa-cogs'></i></button>";
                    }

                    echo <<<HTML
                                    <button type='button' class='abrirModalCronograma btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirCronograma'><i class='fa fa-eye'></i></button>
                                </td>
                            </tr>
HTML;
                }

                $this->htmlTabelaFim();
            }
        }

        $where = $whereFiltroLocal . $wherePeriodo;

        if ($pendente) { // Para n�o exibir cronogramas pendentes em duplicidade
            $cronograma = $this->medoo->query($sql . ' WHERE ' . $where . ' AND status.cod_status <> 22')->fetchAll(PDO::FETCH_ASSOC);
        } else{
            $cronograma = $this->medoo->query($sql . ' WHERE ' . $where)->fetchAll(PDO::FETCH_ASSOC);
        }

        echo <<<HTML
<div class='row'>
    <div class='col-sm-12'>
        <div class='panel panel-{$cor}'>
            <div class='panel-heading' role='tab'>
                <div class='row'>
                    <div class='col-md-4'>
                        <a class='collapsed' role='button' data-toggle='collapse' href='#tabelaCronograma{$tipoCron}' aria-expanded='false'>
                            <button class='btn btn-default'><label>Cronograma - {$nomeGrupo}</label></button>
                        </a>
                    </div>
HTML;

        if(!empty($proxMes)){
            echo("    <div class='col-md-4'><label>{$nomeMes} de {$ano}</label></div>");
        }

        echo <<<HTML
                </div>
            </div>
            <div id='tabelaCronograma{$tipoCron}' class='panel-collapse collapse out' role='tabpanel'>
HTML;

        if($imprimirCrono){
            echo <<<HTML
                <div class='panel-body'>
                    <button class='btn btn-primary' data-toggle='modal' data-target='#ModalCronograma{$tipoCron}'>
                        <i class='fa fa-eye fa-fw fa-1x'></i> Visualizar Cronograma
                    </button>
                </div>
HTML;

        }else if($marcarTodos){
            echo <<<HTML
                <div class='panel-body'>
                    <div class='col-md-offset-6 col-md-2'>
                        <button value='{$siglaGrupo}' rel='tooltip-wrapper' data-title='Apenas da P�gina em exibi��o' class='btn btn-primary btn-block btnTodasOpn'><i class='fa fa-check-square'></i> <span>Marcar Todas</span></button>
                    </div>
                    <div class='col-md-2'>
                        <button value='{$siglaGrupo}' rel='tooltip-wrapper' data-title='Apenas da P�gina em exibi��o' class='btn btn-warning btn-block btnNenhumaOpn'><i class='fa fa-square'></i> <span>Desmarcar Todas</span></button>
                    </div>
                    <div class='col-md-2'>
                        <button value='{$siglaGrupo}' class='btn btn-success btn-block btnGerarSsmp'><i class='fa fa-gears'></i> Gerar Ssmp</button>
                    </div>
                </div>
HTML;
        }


        $contadorCro = count($cronograma);

        echo <<<HTML
                <div class='panel-body'>
                    <strong>Total de Itens da tabela: {$contadorCro}<br/><br/></strong>
                    <table id='indicadorCronograma{$tipoCron}' class='table table-striped table-bordered table-hover dataTable no-footer'>
                        <thead>
                            <tr>
                                <th>N�</th>
                                <th>Servi�o</th>
                                <th>{$nameLocal}</th>
                                <th>Quinzena Prevista</th>
                                <th>Periodicidade</th>
                                <th>Status</th>
                                <th>A��o</th>
                            </tr>
                        </thead>
                        <tbody>
HTML;

        if (!empty($cronograma)){
            foreach ($cronograma as $dados => $value) {
                $local = $value['nome_local'];
                if($codGrupo == 20 && $value['nome_poste']){
                    $local .= " - {$value['nome_poste']}";
                }
                echo <<<HTML
                <tr>
                    <td>{$value['cod_cronograma_pmp']}<input type='hidden' value='{$nomeGrupo}' ></td>
                    <td>{$value['nome_servico_pmp']}</td>
                    <td>{$local}</td>
                    <td>{$value['quinzena']}</td>
                    <td>{$value['nome_periodicidade']}</td>
                    <td>{$value['nome_status']}</td>

                    <td>
HTML;

                if($acao) {
                    if ($value['nome_status'] == 'Pendente' && !$pendente) {
                        echo("<button value='{$siglaGrupo}' type='button' class='btnReprogramar btn btn-circle btn-success' title='Reprogramar'><i class='fa fa-cogs'></i></button>");
                    }

                    if ($value['nome_status'] == 'Aberta') {
                        if ($marcarTodos) {
                            echo("<input type='checkbox' value='{$value['cod_cronograma_pmp']}' class='checkBoxGerarSsmp-{$siglaGrupo}' id='gerarSsmp{$value['cod_cronograma_pmp']}'><label for='gerarSsmp{$value['cod_cronograma_pmp']}'>Gerar Ssmp </label>");
                        } else {
                            echo("<button type='button' class='btn btn-circle btn-success btnGerarSsmp' title='Gerar Ssmp'><i class='fa fa-check fa-fw'></i></button>");
                        }
                    }
                }

                echo(" <button type='button' class='abrirModalCronograma btn btn-circle btn-primary' data-toggle='modal' data-target='.ExibirCronograma'><i class='fa fa-eye'></i></button>");

                echo("</td>");
                echo('</tr>');
            }
        }
        echo("</tbody></table></div></div></div></div></div>");

        $this->modalCronograma($tipoCron, $nomeGrupo, $codGrupo);
    }

    public function painelCronogramaTue($acao = false){
        $preventivaTUE = [];

        $sql = "SELECT cod_veiculo, nome_veiculo, odometro FROM veiculo JOIN carro USING(cod_veiculo) WHERE carro_lider = 's'";
        $tue = $this->medoo->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        foreach ($tue as $dados => $value) {
            $cod_servico = $this->periodTuePmp($value['odometro']);

            if ($cod_servico != 0) {
                $odometro = $this->medoo->query("SELECT odometro FROM ssmp_tue WHERE cod_servico_pmp = {$cod_servico} AND cod_veiculo = {$value['cod_veiculo']} ORDER BY odometro DESC")->fetchAll(PDO::FETCH_ASSOC);
                $odometro = $odometro[0]['odometro'];

                if($value['odometro'] > ($odometro + 3000)){ // 3000 � uma margem de seguran�a
                    $servico = $this->medoo->query("SELECT nome_servico_pmp FROM servico_pmp WHERE cod_servico_pmp = {$cod_servico}")->fetchAll(PDO::FETCH_ASSOC);
                    $servico = $servico[0]['nome_servico_pmp'];

                    array_push($preventivaTUE, [
                        "cod_veiculo" => $value['cod_veiculo'],
                        "nome_veiculo" => $value['nome_veiculo'],
                        "odometro" => $value['odometro'],
                        "cod_servico_pmp" => $cod_servico,
                        "nome_servico_pmp" => $servico
                    ]);
                }
            }
        }

        if (!empty($preventivaTUE)){
            $contador = count($preventivaTUE);

            $subTitulo = "<div class='col-md-4'><label>{$contador} - Preventiva(s) Previstas</label></div>";

            $this->htmlTabelaInicio("PreventivaTue", "Preventiva Tue", $contador, 'red', $subTitulo);

            echo "<thead>
                        <tr>
                            <th>Veiculo</th>
                            <th>Servi�o</th>
                            <th>Odometro</th>";

            if($acao) echo "<th>A��o</th>";

            echo "      </tr>
                    </thead>
                    <tbody>";

            foreach ($preventivaTUE as $dados => $value) {
                echo <<<HTML
                        <tr>
                            <td>{$value['nome_veiculo']}<input type="hidden" value="*{$value['cod_veiculo']}*{$value['cod_servico_pmp']}*{$value['odometro']}*"></td>
                            <td>{$value['nome_servico_pmp']}</td>
                            <td>{$value['odometro']}</td>

HTML;
                if($acao){
                    echo("<td><button type='button' class='btn btn-circle btn-success btnGerarSsmpTue' title='Gerar Ssmp'><i class='fa fa-check fa-fw'></i></button></td>");
                }

                echo <<<HTML
                        </tr>
HTML;
            }

            $this->htmlTabelaFim();
        }
    }

    // -- Ssmp
    // -- Ssmp
    // -- Ssmp

    public function painelSsmpAbertas($tipoForm, $whereFiltroLocal = '', $acao = false){
        $arrayCollumn = [
            "cronograma.quinzena",
            "nome_sistema",
            "nome_subsistema",
            "nome_servico_pmp"
        ];

        $returnGetSql = Sql::getSsmpSql($tipoForm, $arrayCollumn);

        $table      = "Ssmp{$tipoForm}Ab";
        $titulo     = 'SSMP - Abertas';
        $siglaGrupo = $returnGetSql['siglaGrupo'];
        $query = "{$returnGetSql['sql']} WHERE {$whereFiltroLocal} cod_status = 32";

        $ssmpAbertas = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);

        $contador = count($ssmpAbertas);

        $this->htmlTabelaInicio($table, $titulo, $contador);

        if($tipoForm == 'Tue'){
            echo "<thead>
                    <tr>
                        <th>N� SSMP</th>
                        <th>Veiculo</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
        }else {
            echo "<thead>
                    <tr>
                        <th>N� SSMP</th>
                        <th>N� Cronograma</th>
                        <th>Quinzena Prevista</th>
                        <th>{$returnGetSql['nameLocal']}</th>
                        <th>Sistema</th>
                        <th>SubSistema</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
        }

        if (!empty($ssmpAbertas)) {
            foreach ($ssmpAbertas as $dados => $value) {
                if($tipoForm == 'Tue') {
                    echo "<tr>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$value['nome_veiculo']}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";
                }else{
                    $local = $value['nome_local'];
                    if ($siglaGrupo == "ra" && $value['nome_poste']) {
                        $local .= " - {$value['nome_poste']}";
                    }
                    echo "<tr>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$value["cod_cronograma_pmp"]}</td>
                        <td>{$value['quinzena']}</td>
                        <td>{$local}</td>
                        <td>{$value['nome_sistema']}</td>
                        <td>{$value['nome_subsistema']}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";
                }

                if($acao) {
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarSsmp' type='button' title='Ver Dados/Editar'><i class='fa fa-file-o fa-fw'></i></button>");
                }else{
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarSsmp' type='button' title='Ver Dados'><i class='fa fa-eye fa-fw'></i></button>");
                }
                echo("<button value='{$siglaGrupo}' class='btn btn-default btn-circle btnImprimirSsmp' type='button' title='Imprimir'>
                            <i class='fa fa-print fa-fw'></i>
                      </button>
                      </td>
                      </tr>");
            }
        }

        $this->htmlTabelaFim();
    }

    public function painelSsmpProgramadas($tipoForm, $whereFiltroLocal = '', $acao = false){
        $arrayCollumn = [
            "data_programada",
            "cronograma.quinzena",
            "nome_sistema",
            "nome_subsistema",
            "nome_servico_pmp"
        ];

        $returnGetSql = Sql::getSsmpSql($tipoForm, $arrayCollumn);

        $table      = "Ssmp{$tipoForm}Prog";
        $titulo     = 'SSMP - Programadas';
        $siglaGrupo = $returnGetSql['siglaGrupo'];

        $query = "{$returnGetSql['sql']} WHERE {$whereFiltroLocal} cod_status = 19";
        $ssmpProgramada = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);

        $contador = count($ssmpProgramada);

        $this->htmlTabelaInicio($table, $titulo, $contador);

        if($tipoForm == 'Tue'){
            echo "<thead>
                    <tr>
                        <th>N� SSMP</th>
                        <th>Veiculo</th>
                        <th>Data Programada</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
        }else {
            echo "<thead>
                    <tr>
                        <th>N�</th>
                        <th>{$returnGetSql['nameLocal']}</th>
                        <th>Data Programada</th>
                        <th>Quinzena Prevista</th>
                        <th>Sistema</th>
                        <th>SubSistema</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
        }

        if (!empty($ssmpProgramada)) {
            foreach ($ssmpProgramada as $dados => $value) {
                $dataProgramada = MainController::parse_timestamp_static($value['data_programada']);

                if($tipoForm == 'Tue'){
                    echo "<tr>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$value['nome_veiculo']}</td>
                        <td>{$dataProgramada}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";
                }else {
                    $local = $value['nome_local'];
                    if ($siglaGrupo == "ra" && $value['nome_poste']) {
                        $local .= " - {$value['nome_poste']}";
                    }
                    echo "<tr>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$local}</td>
                        <td>{$dataProgramada}</td>
                        <td>{$value['quinzena']}</td>
                        <td>{$value['nome_sistema']}</td>
                        <td>{$value['nome_subsistema']}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";
                }

                if($acao) {
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarSsmp' type='button' title='Ver Dados/Editar'><i class='fa fa-file-o fa-fw'></i></button>");
                }else{
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarSsmp' type='button' title='Ver Dados'><i class='fa fa-eye fa-fw'></i></button>");
                }
                echo("<button value='{$siglaGrupo}' class='btn btn-default btn-circle btnImprimirSsmp' type='button' title='Imprimir'>
                            <i class='fa fa-print fa-fw'></i>
                      </button>
                      </td>
                      </tr>");
            }
        }

        $this->htmlTabelaFim();
    }

    public function painelSsmpPendentes($tipoForm, $whereFiltroLocal = '', $acao = false){
        $arrayCollumn = [
            "nome_sistema",
            "nome_subsistema",
            "nome_servico_pmp"
        ];

        $returnGetSql = Sql::getSsmpSql($tipoForm, $arrayCollumn);

        $table    = "Ssmp{$tipoForm}Pen";
        $titulo   = 'SSMP - Pendentes';
        $siglaGrupo = $returnGetSql['siglaGrupo'];

        $query = "{$returnGetSql['sql']} WHERE {$whereFiltroLocal} cod_status = 22";
        $ssmpPendentes = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);

        $contador = count($ssmpPendentes);

        $this->htmlTabelaInicio($table, $titulo, $contador);

        if($tipoForm == 'Tue'){
            echo "<thead>
                    <tr>
                        <th>N� SSMP</th>
                        <th>Veiculo</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
        }else {
            echo "<thead>
                    <tr>
                        <th>N�</th>
                        <th>{$returnGetSql['nameLocal']}</th>
                        <th>Sistema</th>
                        <th>SubSistema</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";
        }

        if (!empty($ssmpPendentes)) {
            foreach ($ssmpPendentes as $dados => $value) {
                if($tipoForm == 'Tue'){
                    echo "<tr>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$value['nome_veiculo']}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";
                }else {
                    $local = $value['nome_local'];
                    if ($siglaGrupo == "ra" && $value['nome_poste']) {
                        $local .= " - {$value['nome_poste']}";
                    }
                    echo "<tr>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$local}</td>
                        <td>{$value['nome_sistema']}</td>
                        <td>{$value['nome_subsistema']}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";
                }

                if($acao) {
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarSsmp' type='button' title='Ver Dados/Editar'><i class='fa fa-file-o fa-fw'></i></button>");
                }else{
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarSsmp' type='button' title='Ver Dados'><i class='fa fa-eye fa-fw'></i></button>");
                }
                echo("<button value='{$siglaGrupo}' class='btn btn-default btn-circle btnImprimirSsmp' type='button' title='Imprimir'>
                            <i class='fa fa-print fa-fw'></i>
                      </button>
                      </td>
                      </tr>");
            }
        }

        $this->htmlTabelaFim();
    }

    // -- Osmp
    // -- Osmp
    // -- Osmp

    public function painelOsmp($tipoForm, $whereFiltroLocal = '', $acao = false){
        $arrayCollumn = [
            "osmp.cod_osmp",
            "osmp.data_abertura",
            "nome_servico_pmp"
        ];

        $returnGetSql = Sql::getOsmpSql($tipoForm, $arrayCollumn);

        $table    = "Osmp{$tipoForm}Exec";
        $titulo   = 'OSMP em Execu��o';
        $siglaGrupo = $returnGetSql['siglaGrupo'];

        $query = "{$returnGetSql['sql']} WHERE {$whereFiltroLocal} cod_status = 10";
        $osmp = $this->medoo->query($query)->fetchAll(PDO::FETCH_ASSOC);

        $contador = count($osmp);

        $this->htmlTabelaInicio($table, $titulo, $contador);

        echo "<thead>
                    <tr>
                        <th>N� OSMP</th>
                        <th>N� SSMP</th>
                        <th>Data de Abertura</th>
                        <th>{$returnGetSql['nameLocal']}</th>
                        <th>Servi�o</th>
                        <th>A��o</th>
                    </tr>
                </thead>
                <tbody>";

        if (!empty($osmp)) {
            foreach ($osmp as $dados => $value) {
                $dt = MainController::parse_timestamp_static($value['data_abertura']);
                echo "<tr>
                        <td>{$value['cod_osmp']}</td>
                        <td>{$value['cod_ssmp']}</td>
                        <td>{$dt}</td>
                        <td>{$value['nome_local']}</td>
                        <td>{$value['nome_servico_pmp']}</td>
                        <td>";

                if($acao) {
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarOsmp' type='button' title='Ver Dados/Editar'><i class='fa fa-file-o fa-fw'></i></button>");
                }else{
                    echo("<button value='{$siglaGrupo}' class='btn btn-primary btn-circle btnEditarOsmp' type='button' title='Ver Dados'><i class='fa fa-eye fa-fw'></i></button>");
                }
                echo("<button value='{$siglaGrupo}' class='btn btn-default btn-circle btnImprimirOsmp' type='button' title='Imprimir'>
                            <i class='fa fa-print fa-fw'></i>
                      </button>
                      </td>
                      </tr>");
            }
        }

        $this->htmlTabelaFim();
    }

    // -- Utilitario
    // -- Utilitario
    // -- Utilitario

    private function htmlTabelaInicio($table, $titulo, $contador, $cor = 'default', $subTitulo = ''){
        $corBtn = "default";
        if($cor == "red") $corBtn = $cor;

        echo <<<HTML
<div class='row'>
    <div class='col-sm-12'>
        <div class='panel panel-{$cor}'>
            <div class='panel-heading' role='tab' id='heading'>
                <div class='row'>
                    <div class='col-md-4'>
                        <a class='collapsed' role='button' data-toggle='collapse' href='#tabela{$table}' aria-expanded='false'>
                            <button class='btn btn-{$corBtn}'><label>{$titulo}</label></button>
                        </a>
                    </div>
                    {$subTitulo}
                </div>
            </div>
            <div id='tabela{$table}' class='panel-collapse collapse out' role='tabpanel'>
                <div class='panel-body'>
                    <strong>Total de Itens da tabela: {$contador}</strong>
                    <table id='indicador{$table}' class='table table-bordered table-striped indicador{$table}'>
HTML;
    }

    private function htmlTabelaFim(){
        echo <<<HTML
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
    }

    private function periodTuePmp($km){
        $periodicidade = [1600000,800000,400000,80000,40000,20000,10000]; // Periodicidade por Km
        $codPeriod = [185,184,183,182,181,180,179];

        for($i = 0; $i < count($periodicidade); $i++){
            // Margem de 1000 km pra cima ou para baixo da quilometragem da periodicidade
            if( ( ($km % $periodicidade[$i]) < 1000 ) || ( ($km % $periodicidade[$i]) > ($periodicidade[$i] - 1000) )){
                return $codPeriod[$i];
            }
        }

        return 0;
    }

    // -- Modais
    // -- Modais
    // -- Modais

    public function modalExibirPmp()
    {
        echo <<<HTML
<div class="modal fade ExibirPmp" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3 class="modal-title">Item do PMP</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>Cod. PMP</label>
                        <input class="form-control" readonly name="pmpModal" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo</label>
                        <input class="form-control" readonly name="grupoModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input class="form-control" readonly name="sistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>SubSistema</label>
                        <input class="form-control" readonly name="subsistemaModal" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label>Servi�o</label>
                        <input class="form-control" readonly name="servicoModal" />
                    </div>
                    <div class="col-md-2">
                        <label>Procedimento</label>
                        <input class="form-control" readonly name="procedimentoModal" />
                    </div>
                    <div class="col-md-2">
                        <label>Periodicidade</label>
                        <input class="form-control" readonly name="periodicidadeModal" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Linha</label>
                        <input class="form-control" readonly name="linhaModal" />
                    </div>
                    <div class="col-md-8">
                        <label>Local</label>
                        <textarea class="form-control" readonly name="localModal"></textarea>
                    </div>
                </div>

                 <div class="row">
                    <div class="col-md-2">
                        <label>Quinzena Inicial</label>
                        <input class="form-control" readonly name="quinzenaModal" />
                    </div>
                    <div class="col-md-2">
                        <label>Homem - Hora</label>
                        <input class="form-control" readonly name="hhModal" />
                    </div>
                    <div class="col-md-2">
                        <label>Situa��o Atual</label>
                        <input class="form-control" readonly name="statusModal" />
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="tableRelModal table table-bordered table-responsive">
                            <thead>
                            <th>Cod. Cronograma</th>
                            <th>Quinzena</th>
                            <th>M�s</th>
                            <th>Ano</th>
                            <th>Status</th>
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
    }

    private function modalCronograma($grupoSistema, $titulo, $codGrupo)
    {
        echo("<div class='modal fade' id='ModalCronograma{$grupoSistema}' tabindex='-1' role='form' aria-labelledby='ModalCronograma{$grupoSistema}' data-backdrop='static' aria-hidden='true'>
                <div class='modal-dialog'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>x</span></button>
                            <h4 class='modal-title'><i class='fa fa-university fa-fw'></i>
                                {$titulo} ");
        echo date('Y');
        echo("</h4></div>

      <!-- FORMUL�RIO DE PESQUISA DO CRONOGRAMA -->

          <form action='" . HOME_URI . "/dashboardPmp/pdf{$grupoSistema}Crono' method='post' target='_blank'>
            <div class='modal-body'>
              <div class='row'>
                <div class='col-md-4'>


                  <label>Selecione o Ano</label>
                      <select name='ano' class='form-control'>
                        <option value='' selected>Selecione o Ano</option>");
                        $ano = Date("Y");
                        $aux = $ano -2;
                        echo("<option value='{$aux}'>{$aux}</option>");
                        $aux = $ano -1;
                        echo("<option value='{$aux}'>{$aux}</option>");
                        echo("<option value='{$ano}' selected>{$ano}</option>");
                        $aux = $ano +1;
                        echo("<option value='{$aux}'>{$aux}</option>");
        echo("</select> </div><div class='col-md-4'>
                        <label>Selecione o M�s</label>
                           <select name='mes' class='form-control'>
                              <option value='' selected>Selecione o M�s</option>");
                    foreach (MainController::$monthComplete as $key => $value) {
                          echo("<option value='{$key}'>$value</option>");
                    }
        echo("</select>  </div><div class='col-md-4'>

                                        <label>Selecione a Linha</label>
                                        <select name='linha' class='form-control'>
                                            <option value='' selected>Todas</option>");
        $linha = $this->medoo->select("linha", "*", ["ORDER" => "cod_linha"]);
        foreach ($linha as $dados => $value) {
            echo("<option value='{$value['cod_linha']}'>{$value['nome_linha']}</option>");
        }
        echo("</select></div></div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <label>Servi�o</label>
                                        <select name='servico' class='form-control'>
                                            <option value=''>Todos</option>");

        $servico = $this->medoo->select("servico_pmp", "*", ["cod_grupo" => $codGrupo]);
        foreach ($servico as $dados => $value) {
            echo("<option value='{$value['cod_servico_pmp']}'>{$value['nome_servico_pmp']}</option>");
        }
        echo(" </select ></div ></div ></div ><div class='modal-footer' >
                                <button class='btn btn-primary btnImprimir{$grupoSistema}Crono' type = 'submit'
                                        aria-label = 'right align' title = 'Gerar' >
                                    <i class='fa fa-check' ></i > Gerar
                                </button>
                                <button type='button' class='btn btn-danger' aria-label='right align' data-dismiss='modal' title='Fechar'>
                                    <i class='fa fa-times'></i> Fechar
                                </button>
                            </div >
                        </form>
                    </div >
                </div >
            </div >");

    }

    private function modalPrevisaoCronograma($grupoSistema, $titulo)
    {
        echo <<<HTML
<div class='modal fade' id='ModalCronograma{$grupoSistema}Qzn' tabindex='-1' role='form' aria-labelledby='ModalCronograma{$grupoSistema}Qzn' data-backdrop='static' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>x</span>
                </button>
                <h4 class='modal-title'>{$titulo} - Escolha a quinzena:</h4>
            </div>
            <div class='modal-body'>
HTML;

        for ($i = 1; $i <= 24; $i++) {
            echo("<input type='radio' value='{$i}' class='btnImprimirCronograma{$grupoSistema}Qzn' data-dismiss='modal' aria-label='Close'/> - {$i} <br/>");
        }

        echo <<<HTML
                <input type='radio' value='' class='btnImprimirPMP{$grupoSistema}Qzn'
                       data-dismiss='modal' aria-label='Close'/> - <strong>Todas</strong>
                <br/>
            </div>
        </div>
    </div>
</div>
HTML;
    }

    public function modalExibirCronograma()
    {
        echo <<<HTML
<div class="modal fade ExibirCronograma" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h3 class="modal-title">Item do Cronograma</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>Cronograma</label>
                        <input class="form-control" readonly name="cronogramaModal" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo</label>
                        <input class="form-control" readonly name="grupoModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input class="form-control" readonly name="sistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>SubSistema</label>
                        <input class="form-control" readonly name="subsistemaModal" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label>Servi�o</label>
                        <input class="form-control" readonly name="servicoModal" />
                    </div>
                    <div class="col-md-2">
                        <label>Procedimento</label>
                        <input class="form-control" readonly name="procedimentoModal" />
                    </div>
                    <div class="col-md-2">
                        <label>Periodicidade</label>
                        <input class="form-control" readonly name="periodicidadeModal" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Linha</label>
                        <input class="form-control" readonly name="linhaModal" />
                    </div>
                    <div class="col-md-8">
                        <label>Local</label>
                        <textarea class="form-control" readonly name="localModal"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <label>Quinzena Prevista</label>
                        <input class="form-control" readonly name="quinzenaModal" />
                    </div>
                    <div class="col-md-3">
                        <label>Homem - Hora</label>
                        <input class="form-control" readonly name="hhModal" />
                    </div>
                    <div class="col-md-3">
                        <label>Situa��o</label>
                        <input class="form-control" readonly name="statusModal" />
                    </div>
                </div>
                <div class="dadosSsmp" style="margin-top: 2em"></div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="tableRelModal table table-bordered table-responsive">
                            <thead>
                                <th>Osmp</th>
                                <th>Ssmp</th>
                                <th>Data de Abertura</th>
                                <th>Status</th>
                                <th>A��es</th>
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
    }

    public function modalExibirSaf()
    {
        echo <<<HTML
<div class="modal fade ExibirSaf" id="areaImpressao" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close hidden-print" data-dismiss="modal">x</button>
                <h3 class="modal-title">SAF - Solicita��o de Abertura de Falha</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-2">
                        <label>SAF</label>
                        <input class="form-control" readonly name="safModal" />
                    </div>
                    <div class="col-xs-6">
                        <label>Solicitante</label>
                        <input class="form-control" readonly name="solicitanteModal" />
                    </div>
                    <div class="col-xs-4">
                        <label>Data de Abertura</label>
                        <input class="form-control" readonly name="dataAberturaModal" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Grupo</label>
                        <input class="form-control" readonly name="grupoModal" />
                    </div>
                    <div class="col-md-4">
                        <label>Sistema</label>
                        <input class="form-control" readonly name="sistemaModal" />
                    </div>
                    <div class="col-md-4">
                        <label>SubSistema</label>
                        <input class="form-control" readonly name="subsistemaModal" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10">
                        <label>Avaria</label>
                        <input class="form-control" readonly name="avariaModal" />
                    </div>
                    <div class="col-xs-2">
                        <label>N�vel</label>
                        <input class="form-control" readonly name="nivelModal" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Avaria</label>
                        <textarea class="form-control" readonly name="complementoAvariaModal"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <label>Linha</label>
                        <input class="form-control" readonly name="linhaModal"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Trecho</label>
                        <input class="form-control" readonly name="trechoModal"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Ponto Not�vel</label>
                        <input class="form-control" readonly name="pontoNotavelModal"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Complemento Local</label>
                        <textarea class="form-control" readonly name="complementoLocalModal"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <label>Status SAF</label>
                        <input class="form-control" readonly name="statusSafModal"/>
                    </div>
                    <div class="col-md-8">
                        <label>Motivo Status</label>
                        <textarea class="form-control" readonly name="descricaoStatusModal"></textarea>
                    </div>
                </div>

                <div class="dadosSsm" style="margin-top: 2em"></div>
            </div>

            <div class="modal-body divTableRelModal">
                <div class="row">
                    <div class="col-md-12">
                        <table class="tableRelModal table table-bordered table-responsive">
                            <thead>
                                <th>Osm</th>
                                <th>Ssm</th>
                                <th>Data de Abertura</th>
                                <th>Status</th>
                                <th class="hidden-print">A��es</th>
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer hidden-print">
                <button type="button" onclick="printAreaImpressao()" class="btn btn-primary btn-lg" title="Imprimir">
                    <i class="fa fa-print fa-2x"></i>
                </button>
                <button type="button" class="btn btn-danger btn-lg" aria-label="right align" data-dismiss="modal" title="Fechar">
                    <i class="fa fa-times fa-2x"></i>
                </button>
            </div>
        </div>
    </div>
</div>
HTML;
    }

}
